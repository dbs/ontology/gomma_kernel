package org.gomma.rmi;

import java.rmi.AccessException;
import java.rmi.Naming;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

/**
 * The class represents an abstract RMI server which creates and binds the service.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since   JDK1.5
 */
public abstract class AbstractRemoteServer {

	/**
	 * The constant defines the default service URI. The default URI is used 
	 * whenever no URI has been specified by the user at start time.
	 */
	public static final String LOCALHOST = "localhost";
	
	/**
	 * The constant defines the default service name. The default service name
	 * is used when no service name has been specified by the user at start time.
	 */
	public static String SERVICE_NAME; 
	
	/**
	 * The constant represents the logger to save information and exceptions occurring during 
	 * start and stop of services and service registry.
	 */
	protected static final Logger rmiLogger = Logger.getLogger("org.gomma");
	

	/**
	 * The constructor initializes the class.
	 * @param defaultServiceName default service name
	 */
	protected AbstractRemoteServer(String defaultServiceName) {
		SERVICE_NAME = defaultServiceName;
	}
	
	/**
	 * The method returns the usage string, i.e., how to call the class on the command line..
	 * @return usage string
	 */
	public abstract String getUsage();
	
	/**
	 *  The method binds the default service using to the 
	 *  localhost using the default RMI port 1099.
	 */
	public void start() {
		this.register(LOCALHOST,Registry.REGISTRY_PORT,SERVICE_NAME);
	}
	
	/**
	 * The method binds the specified GOMMA service. 
	 * @param uri URI of the GOMMA service, e.g., rmi:/localhost/
	 * @param port port of the GOMMA service, e.g., 1099 (default)
	 * @param serviceName service name of the GOMMA service, e.g., GommaService
	 */
	public void start(String hostName, int port, String serviceName) {
		this.register(hostName,port,serviceName);
	}
	
	public void stop() {
		this.unregister(LOCALHOST,Registry.REGISTRY_PORT,SERVICE_NAME);
	}
	
	public void stop(String hostName, int port, String serviceName) {
		this.unregister(hostName,port,serviceName);
	}

	
	/**
	 * The method returns a stub reference to the service registry.
	 * It creates a service registry if one is available.
	 * @param hostName name of the host for that the service registry will be created
	 * @param port port on that the registry should be listen
	 * @return registry stub reference
	 */
	protected Registry getRegistryStub(String hostName, int port) {
		
		Registry registryStub = null;
		RegistryRepeaterThread registryRepeaterThread = null;
		String[] serviceNames = null;
		
		System.setProperty("java.rmi.server.hostname",hostName);
		
		try {
			/**
			 * creating a stub to the registry at the specified location;
			 * The stub is basically a reference to the registry but it
			 * is not connected to the registry, i.e., the stub will be also
			 * created when the registry is not available. The remote exception
			 * is only thrown when the stub creation process fails.
			 */
			
			registryStub = LocateRegistry.getRegistry(hostName,port);
			serviceNames = registryStub.list();
			rmiLogger.info("Reuse registry at '//"+hostName+":"+port+"'.");
			
		} catch (RemoteException p) {
			
			/**
			 * Since there is no registry available lets create a new registry.
			 */
			try {
				Registry registry = LocateRegistry.createRegistry(port);
				registryRepeaterThread = new RegistryRepeaterThread(registry);
				
				registryStub = LocateRegistry.getRegistry(hostName,port);
				serviceNames = registryStub.list();
				rmiLogger.info("Registry created at '//"+hostName+":"+port+"'.");
				
			} catch (RemoteException e) {
				rmiLogger.info(e.getMessage());
			}
		}
		
		// register the dummy shutdown service
		if (serviceNames!=null && !this.containsService(serviceNames, ShutdownService.SERVICE_NAME)) {
			try {
				rmiLogger.info("Adding shutdown service.");
				ShutdownService service = new ShutdownServiceImpl(hostName,port,rmiLogger);
				ShutdownService serviceStub = (ShutdownService) UnicastRemoteObject.exportObject(service,0);
				registryStub.bind(ShutdownService.SERVICE_NAME,serviceStub);
			} catch (Exception e) {
				rmiLogger.info(e.getMessage());
			}
		}
		
		if (registryRepeaterThread!=null) registryRepeaterThread.start();
		
		return registryStub;
	}

	/**
	 * The method builds and returns the host URL.
	 * @param hostName host name
	 * @param port port on which the service is listening
	 * @param serviceName service name
	 * @return host URL as string
	 */
	protected String getURL(String hostName, int port, String serviceName) {
		return "//"+hostName+":"+port+"/"+serviceName;
	}

	
	/**
	 * The method returns true if the specified service name is already registered,
	 * i.e., it is available within the given array of registered service names; 
	 * otherwise it returns false. 
	 * @param serviceNames array of already registered service names.
	 * @param serviceName service name that should be check for
	 * @return
	 */
	protected boolean containsService(String[] serviceNames, String serviceName) {
		for (String a : serviceNames)
			if (a != null && a.equals(serviceName))
				return true;
		return false;
	}
	
	
	/**
	 * The method binds the service at given host name, port and service name.
	 * @param hostName host on that the service will run
	 * @param port port of the service
	 * @param serviceName service name used by the service
	 */
	protected abstract void register(String hostName, int port, String serviceName);
	
	/**
	 * The method unbinds the service.
	 * @param hostName name of the host where the service should run
	 * @param port port of the service, e.g., 1099 (default)
	 * @param serviceName service name of the service
	 */
	private void unregister(String hostName, int port, String serviceName) {
		try {
			System.setProperty("java.rmi.server.hostname",hostName);
			System.setProperty("sun.rmi.server.execptionTrace","true");
			
			Registry registryStub = LocateRegistry.getRegistry(hostName,port);
			String[] services = registryStub.list();
			
			if (!this.containsService(services, serviceName)) {
				rmiLogger.info("The registry at '//"+hostName+":"+port+
						"' does not contain any service using the name '"+serviceName+"'.");
				return;
			}
			
			Naming.unbind(this.getURL(hostName, port, serviceName));
			rmiLogger.info("Service detached from '"+hostName+":"+port+"/"+serviceName+"'.");
			
			if (services.length==2) {
				ShutdownService shutdownService = (ShutdownService) registryStub.lookup(ShutdownService.SERVICE_NAME);
				shutdownService.shutdown();
			}
			
			registryStub=null;
		} catch (Exception e) {
			rmiLogger.info(e.getMessage());
		}
	}

	
	
	
	
	
	/**
	 * The thread class observes the service registry
	 * and performs a registry shutdown whenever the shutdown
	 * service is removed from the registry.
	 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
	 * @version 1.0
	 * @since JDK1.5
	 */
	private static final class RegistryRepeaterThread extends Thread {

		/**
		 * The item represents the registry that have been created.
		 */
		private final Registry registry;
		
		/**
		 * The constructor initializes the class.
		 * @param reg created registry that should be observed
		 */
		public RegistryRepeaterThread(Registry reg) {
			this.registry = reg;
		}
		
		public void run() {
			try {
				rmiLogger.info("The registry is saved now.");

				while (this.registry!=null && this.containsShutdownService(this.registry.list())) {
				
					// wait for 5 seconds
					try {
						sleep(5000);
					} catch (InterruptedException e) {
						rmiLogger.info("The registry thread is waiting.");
					}
					// do nothing, just waiting
				}
			
				rmiLogger.info("The registry is going down.");
			
				// unbind all registered services
				try {
					for (String serviceName : this.registry.list())
						this.registry.unbind(serviceName);
				} catch (AccessException e) {
					rmiLogger.info(e.getMessage());
				} catch (NotBoundException e) {
					rmiLogger.info(e.getMessage());
				} catch (RemoteException e) {
					rmiLogger.info(e.getMessage());
				}
			
				// shutdown the registry
				try {
					UnicastRemoteObject.unexportObject(this.registry,true);
					rmiLogger.info("The registry is down.");
				} catch (NoSuchObjectException e) {
					rmiLogger.info(e.getMessage());
				}
				
			} catch (Exception e) {
				rmiLogger.info("The registry could not be finished.\n"+e.getMessage());
			}
			
			System.exit(0);
		}
		
		/**
		 * The method returns true if the registry already contains the shutdown service;
		 * otherwise it returns false.
		 * @param serviceNames list of services that the registry currently manages  
		 * @return true/false
		 */
		private boolean containsShutdownService(String[] serviceNames) {
			for (String a : serviceNames)
				if (a != null && a.equals(ShutdownService.SERVICE_NAME))
					return true;
			return false;
		}
	} // end of the thread class
}
