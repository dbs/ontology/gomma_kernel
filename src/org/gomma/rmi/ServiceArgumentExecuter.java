package org.gomma.rmi;

/**
 * The class parses command line arguments and executes 
 * start and stop actions of the remote server.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class ServiceArgumentExecuter<S extends AbstractRemoteServer> extends AbstractArgumentExecuter {
	
	/**
	 * The item represents an initialized remote server instance.
	 */
	private S remoteService;
	
	/**
	 * The constructor initializes the class.
	 * @param args array of service arguments
	 * @param service remote server instance
	 */
	public ServiceArgumentExecuter(String[] args, S service) {
		if (service==null)
			throw new NullPointerException("Could not resolve the remote service.");
		this.remoteService = service;
		
		super.parseArguments(args);
		this.executeRemoteServiceAction();
	}
	

	
	/**
	 * The method executes the start/stop action of the remote service.
	 */
	private void executeRemoteServiceAction() {
		if (this.hasInvalidArguments) {
			this.remoteService.getUsage();
			return;
		}
		
		if (this.isStart && this.isStop) {
			System.out.println("Could not resolve the action (start|stop) that should be carried out.");
			this.remoteService.getUsage();
			return;
		}
		
		if (!this.isStart && !this.isStop) {
			System.out.println("Confused with multiple start stop actions,");
			this.remoteService.getUsage();
			return;
		}
		
		if (this.isStart) {
			if (this.hostName!=null && this.serviceName!=null)
				this.remoteService.start(this.hostName, this.port, this.serviceName);
			else this.remoteService.start();
			return;
		}
		
		if (this.isStop) {
			if (this.hostName!=null && this.serviceName!=null)
				this.remoteService.stop(this.hostName, this.port, this.serviceName);
			else this.remoteService.stop();
			return;
		}
	}
}
