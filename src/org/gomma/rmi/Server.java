/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;

/**
 * The interface represents the GOMMA administration interface comprising 
 * administration methods, such as methods for initialization and shutdown 
 * the GOMMA kernel. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface Server extends Remote {

	/**
	 * The method initializes the kernel using the standard configuration.
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public void initialize() throws GommaException, RemoteException;
	
	/**
	 * The method initializes the kernel standalone, i.e., without repository connection.
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public void initializeStandalone() throws GommaException, RemoteException;
	
	/**
	 * The method initializes the kernel using the configuration as 
	 * defined within the specified file. 
	 * @param confFileNameLoc configuration file name
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public void initialize(String confFileNameLoc) throws GommaException, RemoteException;
	
	/**
	 * The method performs a shutdown of the kernel.
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public void shutdown() throws GommaException, RemoteException;
	
}
