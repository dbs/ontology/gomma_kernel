/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/04/23
 * 
 * changes:
 * 
 **/
package org.gomma.rmi;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.gomma.Gomma;
import org.gomma.exceptions.GommaException;

/**
 * The class represents the GOMMA administration client which used to
 * initialize and shutdown the GOMMA kernel remotely.  
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class GommaAdminClient extends GommaClient implements Gomma {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7856699225016933143L;


	/**
	 * The constructor initializes the class.
	 */
	public GommaAdminClient() {
		super("../log");
		this.gommaService=super.gommaService;
	}
	
	
	/**
	 * @see Server#initialize()
	 */
	public void initialize() throws GommaException, RemoteException {
		super.gommaService.initialize();
	}
	
	/**
	 * @see Server#initializeStandalone()
	 */
	public void initializeStandalone() throws GommaException, RemoteException {
		super.gommaService.initialize();
	}
	
	/**
	 * @see Server#initialize(String)
	 */
	public void initialize(String confFileNameLoc) throws GommaException, RemoteException {
		super.gommaService.initialize(confFileNameLoc);
	}
	
	/**
	 * @see Server#shutdown()
	 */
	public void shutdown() throws GommaException, RemoteException {
		super.gommaService.shutdown();
	}
		
	
	public static String usage() {
		return "GommaAdminClient\n"+
        		"IZBI/IMISE Leipzig, Version 0.8, 2008-2009\n"+
        		"Toralf Kirsten\n\n"+
        		"Start and stop the Gomma-Server (not the RMI-Service!)\n\n"+
        		"Start: GommaAdminClient start -h=<hostName> -p=<port> -s=<serviceName> -c=<Gomma-Config-FileName>\n"+
        		"Stop:  GommaAdminClient stop -h=<hostName> -p=<port> -s=<serviceName>";
	}
	
	
	public static void main(String[] args) {
		if (args!=null) {
			try {
				String hostName=null, serviceName=null, configFileName=null;
				int port=0;
				
				for (String arg : args) {
					if (arg.startsWith("start") || arg.startsWith("stop")) continue;
					else if (arg.startsWith("-h=")) hostName=arg.substring(3);
					else if (arg.startsWith("-p=")) port=Integer.parseInt(arg.substring(3));
					else if (arg.startsWith("-s=")) serviceName=arg.substring(3);
					else if (arg.startsWith("-c=")) configFileName=arg.substring(3);
					else System.out.println("Could not resolve the unknown parameter '"+arg+"'.");
				}
				
				if (args[0].equalsIgnoreCase("stop")) {
					if (args.length==4 && hostName!=null && serviceName!=null) {
						GommaAdminClient client = new GommaAdminClient();
						client.connect(hostName,port,serviceName);
						client.shutdown();
						return;
					}
					
					System.out.println("Exception: Not enough parameters have been specified.");
					System.out.println(GommaAdminClient.usage());
					return;
					
				} else
					
				if (args[0].equalsIgnoreCase("start")) {
					if (args.length==5 && hostName!=null && serviceName!=null && configFileName!=null) {
						GommaAdminClient client = new GommaAdminClient();
						client.connect(hostName,port,serviceName);
						client.initialize(configFileName);
						return;
					}
					
					System.out.println("Exception: Not enough parameters have been specified.");
					System.out.println(GommaAdminClient.usage());
					
				} else {
					System.out.println("Exception: Please, use keywords 'start' or 'stop' to start or stop the Gomma RMI server.");
					System.out.println(GommaAdminClient.usage());
				}
				
				return;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			} catch (GommaException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Exception: No parameters have been specified.");
		System.out.println(GommaAdminClient.usage());
	}
	
}
