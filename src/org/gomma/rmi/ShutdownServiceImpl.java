package org.gomma.rmi;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

public class ShutdownServiceImpl implements ShutdownService {
	
	private final String hostName;
	private final int port;
	private final Logger rmiLogger;
	
	public ShutdownServiceImpl(String hostName, int port, Logger log) {
		this.hostName = hostName;
		this.port = port;
		this.rmiLogger = log;
	}
	
	public void shutdown() throws RemoteException {
		
		try {
			// delete the shutdown service from the registry
			Registry registryStub = LocateRegistry.getRegistry(this.hostName,this.port);
			registryStub.unbind(ShutdownService.SERVICE_NAME);
			registryStub = null;
			
		} catch (AccessException e) {
			this.rmiLogger.info(e.getMessage());
		} catch (RemoteException e) {
			this.rmiLogger.info(e.getMessage());
		} catch (NotBoundException e) {
			this.rmiLogger.info(e.getMessage());
		}
	}
}
