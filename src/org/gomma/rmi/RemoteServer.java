package org.gomma.rmi;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public interface RemoteServer extends Server {

	/**
	 * The method creates a connection to the locally installed RMI server
	 * using the standard port 1099 and the service name 'GommaService'. 
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public void connect() throws MalformedURLException, RemoteException, NotBoundException;
	
	/**
	 * The method creates a connection to the specified service on the given 
	 * host using the specified port.
	 * @param hostName host name
	 * @param port port
	 * @param serviceName service name
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public void connect(String hostName, int port, String serviceName) throws MalformedURLException, RemoteException, NotBoundException;
	
	/**
	 * The method disconnects from the currently used RMI server.
	 */
	public void disconnect();
}
