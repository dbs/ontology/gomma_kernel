package org.gomma.rmi;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;

public final class ServerArgumentExecuter extends AbstractArgumentExecuter {

	private RemoteServer remoteServer;
	
	public ServerArgumentExecuter(String[] args, RemoteServer server) throws MalformedURLException, RemoteException, NotBoundException, GommaException {
		if (server==null)
			throw new NullPointerException("Could not resolve the remote service.");
		this.remoteServer = server;
		
		super.parseArguments(args);
		this.executeRemoteServerAction();
	}
	
	private void executeRemoteServerAction() throws MalformedURLException, RemoteException, NotBoundException, GommaException {
		if (this.hasInvalidArguments) {
			return;
		}
		
		if (this.isStart && this.isStop) {
			System.out.println("Could not resolve the action (start|stop) that should be carried out.");
			return;
		}
		
		if (!this.isStart && !this.isStop) {
			System.out.println("Confused with multiple start stop actions,");
			return;
		}
		
		if (this.isStart) {
			if (this.hostName!=null && this.serviceName!=null)
				this.remoteServer.connect(hostName, port, serviceName);
			else this.remoteServer.connect();
			
			if (super.configFileNameLoc!=null)
				this.remoteServer.initialize(super.configFileNameLoc);
			else this.remoteServer.initialize();
			
			this.remoteServer.disconnect();
		}
		
		if (this.isStop) {
			if (this.hostName!=null && this.serviceName!=null)
				this.remoteServer.connect(hostName, port, serviceName);
			else this.remoteServer.connect();
			
			this.remoteServer.shutdown();
			
			
			this.remoteServer.disconnect();
		}
	}
}
