package org.gomma.rmi;

public abstract class AbstractArgumentExecuter {

	/**
	 * The item represents the host name.
	 */
	protected String hostName=null;
	
	/**
	 * The item represents the service name.
	 */
	protected String serviceName=null;
	
	/**
	 * The item represents the port number. 
	 */
	protected int port=0;
	
	protected String configFileNameLoc=null;
	
	/**
	 * The item is false iff no parse errors have been occurred; otherwise it is true.
	 */
	protected boolean hasInvalidArguments=false;
	
	/**
	 * The item is true if the remote server should be started; otherwise it is false or invalid.
	 */
	protected boolean isStart = false;
	
	/**
	 * The item is true if the remote server should be stopped; otherwise it is false or invalid.
	 */
	protected boolean isStop = false;
	
	/**
	 * The method parses the given arguments and extracts the host and service name
	 * as well as the port number and action (start/stop) that should be executed. 
	 * @param args array of arguments
	 */
	protected void parseArguments(String[] args) {
		if (args==null) {
			this.hasInvalidArguments = true;
			System.out.println("No parameters have been specified.");
			return;
		}
		
		for (String arg : args) {
			if (arg.equalsIgnoreCase("start")) this.isStart = true;
			else if (arg.equalsIgnoreCase("stop")) this.isStop = true; 
			else if (arg.startsWith("-h=")) this.hostName=arg.substring(3);
			else if (arg.startsWith("-p=")) this.port=Integer.parseInt(arg.substring(3));
			else if (arg.startsWith("-s=")) this.serviceName=arg.substring(3);
			else if (arg.startsWith("-c=")) this.configFileNameLoc=arg.substring(3);
			else System.out.println("Could not resolve the unknown parameter '"+arg+"'.");
		}
	}
}
