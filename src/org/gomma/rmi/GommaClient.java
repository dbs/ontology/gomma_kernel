/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/04/23
 * 
 * changes:
 * 
 **/
package org.gomma.rmi;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.gomma.GommaManager;
import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.ManagerServiceFactory;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;

/**
 * The class represents the GOMMA client which provides the
 * functionality of the kernel. The client can remotely access 
 * the kernel such as installed RMI server. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class GommaClient implements GommaManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6114386084945406731L;
	
	/**
	 * The item represents the GOMMA service.
	 */
	protected GommaService gommaService;
	
	/**
	 * The item represents the service factory used to group kernel functions.
	 */
	private ManagerServiceFactory serviceFactory;
	
	/**
	 * The constructor initializes the class.
	 */
	public GommaClient(String logDir) {
		this.serviceFactory = null;
		this.gommaService=null;
		
//		GommaLogFactory.createRMIClientLoggers(
//				new GommaLogFactory.RMIClientLogCongig()
//					.logTransportTCP()
//					.logTransportProxy()
//					.logTransportMisc(),logDir
//			);
	}
	
	/**
	 * The method creates a connection to the locally installed RMI server
	 * using the standard port 1099 and the service name 'GommaService'. 
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public void connect() throws MalformedURLException, RemoteException, NotBoundException {
		this.connect(GommaServer.LOCALHOST,Registry.REGISTRY_PORT,GommaServer.SERVICE_NAME);
	}
	
	/**
	 * The method creates a connection to the specified service on the given 
	 * host using the specified port.
	 * @param hostName host name
	 * @param port port
	 * @param serviceName service name
	 * @throws MalformedURLException
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public void connect(String hostName, int port, String serviceName) throws MalformedURLException, RemoteException, NotBoundException {
//		if (System.getSecurityManager()==null)
//			System.setSecurityManager(new RMISecurityManager());
		
		Registry registry = LocateRegistry.getRegistry(hostName, port);
		this.gommaService = (GommaService)registry.lookup(serviceName);
		
		if (this.gommaService!=null) { 
			this.serviceFactory = new ManagerServiceFactory(this.gommaService);
			System.out.println("Connection established to '"+hostName+":"+port+"/"+serviceName+"'.");
		} else System.out.println("Connection to "+hostName+":"+port+"/"+serviceName+"' could not be established.");
	}
	
	/**
	 * The method disconnects from the currently used RMI server.
	 */
	public void disconnect() {
		this.gommaService=null;
	}
	
	/**
	 * The method returns true if there is a valid connection to a RMI server;
	 * otherwise it returns false.
	 * @return true/false
	 */
	public boolean isConnected() {
		return this.gommaService!=null;
	}
	
	/**
	 * @see GommaManager#getSourceManager()
	 */
	public SourceManager getSourceManager() throws RemoteException {
		return this.serviceFactory.getSourceManager();
	}
	
	/**
	 * @see GommaManager#getMappingManager()
	 */
	public MappingManager getMappingManager() throws RemoteException {
		return this.serviceFactory.getMappingManager();
	}
	
	/**
	 * @see GommaManager#getMappingOperationManager()
	 */
	public MappingOperationManager getMappingOperationManager() throws RemoteException {
		return this.serviceFactory.getMappingOperationManager();
	}
	
	/**
	 * @see GommaManager#getMatchManager()
	 */
	public MatchManager getMatchManager() throws RemoteException {
		return this.serviceFactory.getMatchManager();
	}
	
	/**
	 * @see GommaManager#getWorkManager()
	 */
	public WorkManager getWorkManager() throws RemoteException {
		return this.serviceFactory.getWorkManager();
	}
	
	/**
	 * @see GommaManager#getSourceEvolutionManager()
	 */
	public SourceEvolutionManager getSourceEvolutionManager() throws RemoteException {
		return this.serviceFactory.getSourceEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getMappingObjectEvolutionManager()
	 */
	public MappingObjectEvolutionManager getMappingObjectEvolutionManager() throws RemoteException {
		return this.serviceFactory.getMappingObjectEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getMappingCorrespondenceEvolutionManager()
	 */
	public MappingCorrespondenceEvolutionManager getMappingCorrespondenceEvolutionManager() throws RemoteException {
		return this.serviceFactory.getMappingCorrespondenceEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getEvolutionStatisticsManager()
	 */
	public EvolutionStatisticsManager getEvolutionStatisticsManager() throws RemoteException {
		return this.serviceFactory.getEvolutionStatisticsManager();
	}
	
	/**
	 * @see GommaManager#getStatisticsManager()
	 */
	public StatisticsManager getStatisticsManager() throws RemoteException {
		return this.serviceFactory.getStatisticsManager();
	}
	
	/**
	 * @see GommaManager#getStabilityManager()
	 */
	public StabilityManager getStabilityManager() throws RemoteException {
		return this.serviceFactory.getStabilityManager();
	}
	
	/**
	 * @see GommaManager#getDistributedMatchManager()
	 */
	public DistributedMatchManager getDistributedMatchManager()
			throws RemoteException {
		return this.serviceFactory.getDistributedMatchManager();
	}
	
	public String toString() { return "GommaService"; }
	
	/**
	 * The method checks whether the GOMMA service is already initialized and yet active.
	 * @throws GommaException
	 */
	protected void checkInitialization() throws GommaException {
		if (!this.isConnected()) 
			throw new GommaException("Operation failed: The Gomma service is currently not initialized.");
	}

	

}
