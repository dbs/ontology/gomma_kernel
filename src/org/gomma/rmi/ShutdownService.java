package org.gomma.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ShutdownService extends Remote {

	public static final String SERVICE_NAME = "shutdown_service";
	
	public void shutdown() throws RemoteException;
}
