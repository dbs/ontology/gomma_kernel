/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/04/23
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.gomma.GommaService;
import org.gomma.GommaServiceImpl;


/**
 * The class represents the GOMMA RMI server which creates and binds the GOMMA service.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since   JDK1.5
 */
public final class GommaServer extends AbstractRemoteServer {
	
	/**
	 * The constructor initializes the class.
	 */
	public GommaServer() {
		super("GommaService");
//		GommaLogFactory.createRMIServerLoggers(
//			new GommaLogFactory.RMIServerLogCongig()
//				.logTransportTCP()
//				.logTransportProxy()
//				.logTransportMisc(),"../log"
//			);
//		GommaLogFactory.createGommaServerLogger("../log");
	}
	
	
	
	/**
	 * @see AbstractRemoteServer#register(String, int, String)
	 */
	protected void register(String hostName, int port, String serviceName) {
		
//		if (System.getSecurityManager()==null)
//			System.setSecurityManager(new RMISecurityManager());
		
		try {
//			String udir = System.getProperty("user.dir");
//			System.setProperty("java.rmi.server.codebase", "file:" + udir + "../jar/gomma-kernel.jar");
//			System.setProperty("java.security.policy",udir+"/java.policy");
			
//			System.setProperty("sun.rmi.server.execptionTrace","true");
//			System.setProperty("javax.net.debug","all");
			
			
			try {
				
				Registry registryStub = super.getRegistryStub(hostName, port);
				String[] serviceNames = registryStub.list();
				
				// check whether the service name is already in use
				if (this.containsService(serviceNames, serviceName)) {
					rmiLogger.info("GOMMA service is already attached. "+
							"Please use another port or service name.");
					return;
				}
				
				// create the service stub and bind it within the registry
				GommaService gommaService = new GommaServiceImpl();
				Remote gommaServiveStub = UnicastRemoteObject.exportObject(gommaService,0);
				registryStub.rebind(serviceName, gommaServiveStub);
				rmiLogger.info("GOMMA service attached at '//"+hostName+":"+port+"/"+serviceName+"'.");
				
			} catch (RemoteException e) {
				rmiLogger.info("GOMMA service could not be attached: No registry found.");
				rmiLogger.info(e.toString());
			}
		} catch (Exception e) {
			rmiLogger.info(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractRemoteServer#getUsage()
	 */
	public String getUsage() {
		return "GommaServer\n"+
		"IZBI/IMISE Leipzig, Version 0.8, 2008-2009\n"+
		"Toralf Kirsten\n\n"+
		"Start and stop the Gomma-RMI-Service (no initialization of the Gomma-Server!)\n\n"+
		"Start: GommaServer start -h=<hostName> -p=<port> -s=<serviceName>\n"+
		"Stop:  GommaServer stop -h=<hostName> -p=<port> -s=<serviceName>";
	}
	
	
	/**
	 * The static method can be called on os shells and call the class above 
	 * @param args list of arguments
	 */
	public static void main(String[] args) {
		new ServiceArgumentExecuter<GommaServer>(args, new GommaServer());
	}

}
