/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.match;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.matcher.Matcher;
import org.gomma.match.matcher.date.DateIntervalMatcher;
import org.gomma.match.matcher.date.SingleDateMatcher;
import org.gomma.match.matcher.name.PathMatcher;
import org.gomma.match.matcher.name.StringCompleteMatcher;
import org.gomma.match.matcher.name.StringMapMatcher;
import org.gomma.match.matcher.optimized.FastExactMatcher;
import org.gomma.match.matcher.optimized.FastListTrigramIndexMatcher;
import org.gomma.match.matcher.optimized.FastListTrigramMatcher;
import org.gomma.match.matcher.optimized.FastSetTrigramIndexMatcher;
import org.gomma.match.matcher.optimized.FastSetTrigramMatcher;
import org.gomma.match.matcher.optimized.ThreadedListTrigramMatcher;
import org.gomma.match.matcher.spatial.LatLongCosineMatcher;
import org.gomma.match.matcher.spatial.LatLongExactMatcher;
import org.gomma.match.matcher.structure.NodeStatisticsMatcher;
import org.gomma.match.matcher.structure.ObjStatisticsMatcher;
import org.gomma.match.matcher.structure.PageRankMatcher;
import org.gomma.match.matcher.structure.SimilarityFloodingMatcher;
import org.gomma.match.matcher.structure.StructureLabelMatcher;
import org.gomma.match.matcher.structure.StructurePureMatcher;
import org.gomma.match.matcher.token.ThreadedEncLCSMatcher;
import org.gomma.match.matcher.token.TF_IDF_Matcher;
import org.gomma.match.matcher.token.ThreadedSoftTfidfEncMatcher;
import org.gomma.match.matcher.token.ThreadedTF_IDFEncMatcher;
import org.gomma.match.matcher.token.ThreadedTF_IDFMatcher;
import org.gomma.match.metrics.SimilarityFunctionConfiguration;

/**
 * The class implements a factory instantiating different matcher.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class MatcherFactory {

	/**
	 * The method returns a matcher referring to the specified matcher type.
	 * @param type matcher type
	 * @return matcher referring to specified matcher type
	 * @throws OperatorException
	 */
	public static Matcher getMatcher(SimilarityFunctionConfiguration conf) throws OperatorException {
		switch (conf.getMatcherType()) {
			case NAME : return getStringMatcher(conf.getExecutionClassType());
			case DATE : return getDateMatcher(conf.getExecutionClassType());
			case STRUCTURE : return getStructureMatcher(conf.getExecutionClassType());
			case OPTIMIZED : return getOptimizedMatcher(conf.getExecutionClassType());
			case SPATIAL : return getSpatialMatcher(conf.getExecutionClassType());
			case TOKEN : return getTokenMatcher(conf.getExecutionClassType());	
			default: throw new OperatorException("Could not resolve a valid matcher.");
		}
	}
	
	private static Matcher getStringMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
			case STRING_COMPLETE: return getStringCompleteMatcher();
			case STRING_MAP: return getStringMapMatcher();
			case PATH: return getPathMatcher();
			default: throw new OperatorException("Could not resolve a valid name or path matcher.");
		}
	}
	
	private static Matcher getDateMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
			case DATE_SINGLE: return getSingleDateMatcher();
			case DATE_INTERVAL: return getDateIntervalMatcher();
			default: throw new OperatorException("Could not resolve a valid date matcher.");
		}
	}
	
	private static Matcher getOptimizedMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
			case OPTIMIZED_SET_TRIGRAM: return getOptimizedSetTrigramMatcher();
			case OPTIMIZED_SET_INDEX_TRIGRAM: return getOptimizedSetIndexTrigramMatcher();
			case OPTIMIZED_LIST_INDEX_TRIGRAM: return getOptimizedListIndexTrigramMatcher();
			case OPTIMIZED_LIST_TRIGRAM: return getOptimizedListTrigramMatcher();
			case OPTIMIZED_EXACT: return getOptimizedExactMatcher();
			case OPTIMIZED_THREADED_LIST_TRIGRAM: return getThreadedListTrigramMatcher();
			default: throw new OperatorException("Could not resolve a valid optimized matcher.");
		}
	}private static Matcher getTokenMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
		case TFIDF: return getTFIDFMatcher();
		case THREADED_TFIDF: return getThreadedTFIDFMatcher();
		case THREADED_ENC_TFIDF: return getThreadedTFIDFEncMatcher();
		case THREADED_ENC_SOFT_TFIDF : return getThreadedSoftTFIDFEncMatcher();
		case THREADED_ENC_LCS: return getThreadedEncLCSMatcher();
			default: throw new OperatorException("Could not resolve a valid optimized matcher.");
		}
	}
	

	
	

	
	private static Matcher getSpatialMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
			case SPATIAL_LAT_LONG_COSINE: return getSpatialLatLongCosineMatcher();
			case SPATIAL_LAT_LONG_EXACT: return getSpatialLatLongExactMatcher();
			default: throw new OperatorException("Could not resolve a valid date matcher.");
		}
	}
	
	/**
	 * The method returns the structure matcher taking structure-based
	 * similarity methods into account.
	 * @param type class type for the structure matcher
	 * @return structure matcher
	 */
	private static Matcher getStructureMatcher(ExecutionClassTypes type) throws OperatorException {
		switch (type) {
			case STATISTICS_NODE: return getNodeStatisticsMatcher();
			case STATISTICS_OBJECT: return getObjectStatisticsMatcher();
			case STRUCTURE_LABEL: return getStructureLabelMatcher();
			case STRUCTURE_PURE: return getStructurePureGraphMatcher();
			case STRUCTURE_PAGERANK: return getPageRankMatcher();
			case STRUCTURE_SIMILARITY_FLOODING: return getSimilarityFloodingMatcher();
			default: throw new OperatorException("Could not resolve a valid structure matcher.");
		}
	}
	
	/**
	 * The method returns the name matcher using 
	 * character-based similarity methods into account.
	 * @return name matcher
	 */
	private static StringCompleteMatcher getStringCompleteMatcher() {
		return new StringCompleteMatcher();
	}
	
	private static StringMapMatcher getStringMapMatcher() {
		return new StringMapMatcher();
	}
	
	/**
	 * The method returns the path matcher using path-based
	 * similarity methods into account.
	 * @return path matcher
	 */
	private static PathMatcher getPathMatcher() {
		return new PathMatcher();
	}
	

	/**
	 * The method returns the single date matcher taking calendar-date-based
	 * similarity methods into account.
	 * @return single date matcher
	 */
	private static SingleDateMatcher getSingleDateMatcher() {
		return new SingleDateMatcher();
	}
	
	/**
	 * The method returns the date interval matcher taking calendar-date-interval-based
	 * similarity methods into account.
	 * @return date interval matcher
	 */
	private static DateIntervalMatcher getDateIntervalMatcher() {
		return new DateIntervalMatcher();
	}
	
	/**
	 * The method returns an instance of the node statistics matcher. The instance
	 * is only initialized; match properties, similarity threshold etc. needs to be 
	 * set separately.
	 * @return node statistics matcher
	 */
	public static NodeStatisticsMatcher getNodeStatisticsMatcher() {
		return new NodeStatisticsMatcher();
	}
	
	/**
	 * The method returns an instance of the object statistics matcher. The instance
	 * is only instantiated; match properties, similarity threshold etc. needs to be
	 * set separately.
	 * @return object statistics matcher
	 */
	private static ObjStatisticsMatcher getObjectStatisticsMatcher() {
		return new ObjStatisticsMatcher();
	}
	
	/**
	 * The methods returns an instance of PageRank matcher. The instance
	 * is only initialized; match properties, similarity threshold etc. needs to be 
	 * set separately.
	 * @return PageRank matcher
	 */
	private static PageRankMatcher getPageRankMatcher() {
		return new PageRankMatcher();
	}
	
	/**
	 * The methods returns an instance of Similarity Flooding matcher. The instance
	 * is only initialized; match properties, similarity threshold etc. needs to be 
	 * set separately.
	 * @return Similarity Flooding matcher
	 */
	private static SimilarityFloodingMatcher getSimilarityFloodingMatcher() {
		return new SimilarityFloodingMatcher();
	}
	
	/**
	 * The methods returns an instance of structure label matcher. The instance
	 * is only initialized; match properties, similarity threshold etc. needs to be 
	 * set separately.
	 * @return structure label matcher
	 */
	private static StructureLabelMatcher getStructureLabelMatcher() {
		return new StructureLabelMatcher();
	}
	
	/**
	 * The methods returns an instance of structure pure graph matcher. The instance
	 * is only initialized; match properties, similarity threshold etc. needs to be 
	 * set separately.
	 * @return structure pure graph matcher
	 */
	private static StructurePureMatcher getStructurePureGraphMatcher() {
		return new StructurePureMatcher();
	}
	
	/**
	 * The methods returns an instance of an optimized Trigram Matcher. 
	 * @return optimized Trigram matcher
	 */
	private static Matcher getOptimizedSetTrigramMatcher() {
		return new FastSetTrigramMatcher();
	}
	private static Matcher getOptimizedSetIndexTrigramMatcher() {
		return new FastSetTrigramIndexMatcher();
	}
	private static Matcher getOptimizedListIndexTrigramMatcher() {
		return new FastListTrigramIndexMatcher();
	}
	private static Matcher getOptimizedListTrigramMatcher() {
		return new FastListTrigramMatcher();
	}
	private static Matcher getThreadedListTrigramMatcher() {
		return new ThreadedListTrigramMatcher();
	}
	private static Matcher getOptimizedExactMatcher() {
		return new FastExactMatcher();
	}
	
	/**
	 * The methods returns the token based tf-idf matcher. 
	 * @return tf-idf matcher
	 */
	private static Matcher getTFIDFMatcher() {
		return new TF_IDF_Matcher();
	}
	
	private static Matcher getThreadedTFIDFMatcher() {
		// TODO Auto-generated method stub
		return new ThreadedTF_IDFMatcher();
	}
	
	private static Matcher getThreadedTFIDFEncMatcher() {
		// TODO Auto-generated method stub
		return new ThreadedTF_IDFEncMatcher();
	}
	
	private static Matcher getThreadedSoftTFIDFEncMatcher() {
		// TODO Auto-generated method stub
		return new ThreadedSoftTfidfEncMatcher();
	}

	private static Matcher getThreadedEncLCSMatcher() {
		// TODO Auto-generated method stub
		return new ThreadedEncLCSMatcher();
	}

		
	private static Matcher getSpatialLatLongCosineMatcher() {
		return new LatLongCosineMatcher();
	}
	private static Matcher getSpatialLatLongExactMatcher() {
		return new LatLongExactMatcher();
	}
}
