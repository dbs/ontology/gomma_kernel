package org.gomma.match.metrics;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The enumeration provides a set of structure-based simialrity functions.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum StructureSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {

	/**
	 * The element represents the structure-based similarity function
	 * taking the labels of parents of the considered node into account. 
	 */
	LABELLED_PARENTS(ExecutionClassTypes.STRUCTURE_LABEL) {
		public String getLabel() {
			return "parent structure matcher using node labels";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the labels of children of the considered node into account. 
	 */
	LABELLED_CHILDREN(ExecutionClassTypes.STRUCTURE_LABEL) {
		public String getLabel() {
			return "children structure matcher using node labels";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the labels of parents and children of the considered node into account. 
	 */
	LABELLED_PARENT_CHILDREN(ExecutionClassTypes.STRUCTURE_LABEL) {
		public String getLabel() {
			return "parent-children structure matcher using node labels";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the labels of parents and siblings of the considered node into account. 
	 */
	LABELLED_PARENT_SIBLINGS(ExecutionClassTypes.STRUCTURE_LABEL) {
		public String getLabel() {
			return "parent-sibling structure matcher using node labels";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking into account the connected sub-graph where the considered node is part of. 
	 */
	LABELLED_FAMILY_GRAPH(ExecutionClassTypes.STRUCTURE_LABEL) {
		public String getLabel() {
			return "family graph structure matcher using node labels";
		}
	},
	
	
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the labels of parents, children and siblings of the considered node into account. 
	 */
	PURE_PARENT_CHILDREN_SIBLING_GRAPH(ExecutionClassTypes.STRUCTURE_PURE) {
		public String getLabel() {
			return "parent-children-sibling graph matcher using pure grap structure";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking into account the structure of the sub-graph where the the 
	 * considered node is part of. 
	 */
	PURE_FAMILY_GRAPH(ExecutionClassTypes.STRUCTURE_PURE) {
		public String getLabel() {
			return "family graph structure matcher using pure graph structure";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking into account node statistics of the considered node, 
	 * such as number of parents, children, siblings, etc. 
	 */
	NODE_STATISTICS(ExecutionClassTypes.STATISTICS_NODE) {
		public String getLabel() {
			return "node statistics matcher taking number of parents, siblings etc. into account";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking into account the statistics of specific object properties of the considered node. 
	 */
	OBJECT_STATISTICS(ExecutionClassTypes.STATISTICS_OBJECT) {
		public String getLabel() {
			return "object statistics matcher taking number of parents, siblings etc. separated "+
					"by available values of a specifiec attribute into account";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the connection from and to other nodes into account. 
	 */
	SIMILARITY_FLOODING(ExecutionClassTypes.STRUCTURE_SIMILARITY_FLOODING) {
		public String getLabel() {
			return "Similarity Flooding";
		}
	},
	
	/**
	 * The element represents the structure-based similarity function
	 * taking the connection from and to other nodes into account. 
	 */
	PAGERANK(ExecutionClassTypes.STRUCTURE_PAGERANK) {
		public String getLabel() {
			return "PageRank";
		}
	};
	
	
	
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private StructureSimilarityFunctions(ExecutionClassTypes type) {
		this.matcherType = MatcherTypes.STRUCTURE;
		this.executionClassType = type;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(String, String)
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
	}

	/**
	 * @see SimilarityFunction#computeConfidence(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of date matcher.");
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static StructureSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (StructureSimilarityFunctions f : StructureSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the structure similarity function '"+label+"'.");
	}
}
