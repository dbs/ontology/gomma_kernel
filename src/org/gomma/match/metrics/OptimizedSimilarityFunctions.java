package org.gomma.match.metrics;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.math.SetSimilarityFunctions;
import org.gomma.util.string.StringSetComparer;

/**
 * The enumeration provides a set of optimized string similarity functions.
 * @author Michael Hartung
 */

public enum OptimizedSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {
	
	/**
	 * The element represents the optimized Trigram similarity function
	 * based on preprocessing and sorting of the ngrams. 
	 * Set-based = duplicate trigrams are considered ONCE.
	 */
	OPTIMIZED_SET_TRIGRAM(ExecutionClassTypes.OPTIMIZED_SET_TRIGRAM) {
		public String getLabel() {
			return "optimized trigram based on sorting of ngrams (set-based)";
		}
	},
	/**
	 * The element represents the optimized Trigram similarity function
	 * based on preprocessing and sorting of the ngrams using the filling technique.
	 * Set-based = duplicate trigrams are considered ONCE.
	 */
	OPTIMIZED_SET_TRIGRAM_FILLING(ExecutionClassTypes.OPTIMIZED_SET_TRIGRAM) {
		public String getLabel() {
			return "optimized trigram based on sorting of ngrams including filling (set-based)";
		}
	},
	/**
	 * The element represents the optimized Trigram similarity function
	 * based on preprocessing.
	 * List-based = duplicate trigrams are FULLY considered.
	 */
	OPTIMIZED_LIST_TRIGRAM(ExecutionClassTypes.OPTIMIZED_LIST_TRIGRAM) {
		public String getLabel() {
			return "optimized trigram (list-based)";
		}
	},
	/**
	 * The element represents the optimized Trigram similarity function
	 * based on preprocessing using the filling technique.
	 * List-based = duplicate trigrams are FULLY considered.
	 */
	OPTIMIZED_LIST_TRIGRAM_FILLING(ExecutionClassTypes.OPTIMIZED_LIST_TRIGRAM) {
		public String getLabel() {
			return "optimized trigram including filling (list-based)";
		}
	},
	/**
	 * The element represents the optimized set-based trigram similarity function based on indexing.
	 */
	OPTIMIZED_SET_TRIGRAM_INDEX_FILLING(ExecutionClassTypes.OPTIMIZED_SET_INDEX_TRIGRAM) {
		public String getLabel() {
			return "optimized set trigram matcher based on indexing";
		}
	},
	/**
	 * The element represents the optimized list-based trigram similarity function based on indexing.
	 */
	OPTIMIZED_LIST_TRIGRAM_INDEX_FILLING(ExecutionClassTypes.OPTIMIZED_LIST_INDEX_TRIGRAM) {
		public String getLabel() {
			return "optimized list trigram matcher based on indexing";
		}
	},
	/**
	 * The element represents the optimized exact similarity function.
	 */
	OPTIMIZED_EXACT(ExecutionClassTypes.OPTIMIZED_EXACT) {
		public String getLabel() {
			return "optimized exact matcher";
		}
	},
	
	/**
	 * The element represents the optimized threaded list trigram similarity function.
	 */
	OPTIMIZED_THREADED_LIST_TRIGRAM_FILLING(ExecutionClassTypes.OPTIMIZED_THREADED_LIST_TRIGRAM) {
		public String getLabel() {
			return "optimized threaded list trigram matcher";
		}
		public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
			if (isEmptyCondition(m1, m2)) return 0F;
			return StringSetComparer.compare(m1, m2, SetSimilarityFunctions.DICE);
		}
	};
	
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private OptimizedSimilarityFunctions(ExecutionClassTypes type) {
		this.matcherType = MatcherTypes.OPTIMIZED;
		this.executionClassType = type;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(String, String)
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
	}

	/**
	 * @see SimilarityFunction#computeConfidence(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of date matcher.");
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static OptimizedSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (OptimizedSimilarityFunctions f : OptimizedSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the structure similarity function '"+label+"'.");
	}
	protected boolean isEmptyCondition(Map<String,Integer> m1, Map<String,Integer> m2) {
		return m1.size()==0 && m2.size()==0;
	}
}
