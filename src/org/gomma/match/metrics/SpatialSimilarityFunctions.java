package org.gomma.match.metrics;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The enumeration provides a set of structure-based simialrity functions.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum SpatialSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {

	/**
	 * The element represents a spatial matcher for latitude / longitude coordinates
	 * based on their similarity in a vector space using their cosine similarity
	 * (http://en.wikipedia.org/wiki/Cosine_similarity)
	 */
	LAT_LONG_BASED_ON_COSINE(ExecutionClassTypes.SPATIAL_LAT_LONG_COSINE) {
		public String getLabel() {
			return "latitude/longitude similarity using the cosine similarity as basis";
		}
	},
	
	/**
	 * The element represents a spatial matcher for latitude / longitude coordinates
	 * based on their exact similarity (= equality)
	 */
	LAT_LONG_EXACT(ExecutionClassTypes.SPATIAL_LAT_LONG_COSINE) {
		public String getLabel() {
			return "latitude/longitude equality";
		}
	};
	
	
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private SpatialSimilarityFunctions(ExecutionClassTypes type) {
		this.matcherType = MatcherTypes.SPATIAL;
		this.executionClassType = type;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(String, String)
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
	}

	/**
	 * @see SimilarityFunction#computeConfidence(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of date matcher.");
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static SpatialSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (SpatialSimilarityFunctions f : SpatialSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the structure similarity function '"+label+"'.");
	}
}
