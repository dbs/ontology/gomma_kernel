package org.gomma.match.metrics;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The enumeration provides a set of structure-based simialrity functions.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum TokenSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {

	/**
	 * The element represents the optimized Trigram similarity function
	 * based on preprocessing and sorting of the ngrams. 
	 * Set-based = duplicate trigrams are considered ONCE.
	 */
	TFIDF(ExecutionClassTypes.TFIDF) {
		public String getLabel() {
			return "tf-idf based on lucene tokenization and Lars Kolb's optimization";
		}
	},
	THREADED_ENC_TFIDF(ExecutionClassTypes.THREADED_ENC_TFIDF) {
		public String getLabel() {
			return "tf-idf based on lucene tokenization with encoding and victor threading";
		}
	},
	THREADED_ENC_SOFT_TFIDF(ExecutionClassTypes.THREADED_ENC_SOFT_TFIDF) {
		public String getLabel() {
			return "soft tf-idf based on lucene tokenization with encoding and victor threading";
		}
	},
	THREADED_ENC_LCS(ExecutionClassTypes.THREADED_ENC_LCS) {
		public String getLabel() {
			return "lcs";
		}
	},
	THREADED_TFIDF(ExecutionClassTypes.THREADED_TFIDF) {
		public String getLabel() {
			return "tf-idf based on lucene tokenization and Lars Kolb's optimization and victor threading";
		}
	};
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private TokenSimilarityFunctions(ExecutionClassTypes type) {
		this.matcherType = MatcherTypes.TOKEN;
		this.executionClassType = type;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(String, String)
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
	}

	/**
	 * @see SimilarityFunction#computeConfidence(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of date matcher.");
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static TokenSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (TokenSimilarityFunctions f : TokenSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the structure similarity function '"+label+"'.");
	}
}
