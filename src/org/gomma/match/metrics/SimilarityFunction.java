/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.metrics;

import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The interface defines methods allowing to calculate confidence. 
 * Each method represents a specific context, e.g., the similarity 
 * (confidence) between two given string.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface SimilarityFunction {

	/**
	 * The method returns the similarity (confidence) of two given 
	 * strings s1 and s2 taking the selected similarity function 
	 * into account.
	 * @param s1 string s1
	 * @param s2 string s2
	 * @return similarity between string s1 and s2
	 * @throws OperatorException
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException;
	
	
	/**
	 * The method returns the similarity between two given date intervals 
	 * taking their start and end date into account.
	 * @param t1Start start date of the date interval 1
	 * @param t1End end date of the date interval 1
	 * @param t2Start start date of the date interval 2
	 * @param t2End end date of the date interval 2
	 * @return similarity between two date intervals
	 * @throws OperatorException
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException;
	
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException;
	
	public String getLabel();
}
