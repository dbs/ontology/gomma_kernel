package org.gomma.match.metrics;

import java.util.Calendar;
import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.DateHandler;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.MyMath;

public enum DateSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {

	/**
	 * similarity based on interval congruency
	 */
	COMPLETE(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval complete";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (t1Start.equals(t2Start) && t1End.equals(t2End)) return 1F;
			return 0F;
		}
	},
	
	/**
	 * similarity of including calendar intervals
	 */
	INCLUSION(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval inclusion";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if ((t1Start.equals(t2Start) || t1Start.before(t2Start)) && 
					(t1End.equals(t2End) || t1End.after(t2End))) return 1F;
			else 
			if ((t1Start.equals(t2Start) || t1Start.after(t2Start)) &&
					(t1End.equals(t2End) || t1End.before(t2End))) return 1F;
			return 0F;
			
//			return SimilarityFunctionImpl.date_interval(t1Start,t1End,t2Start,t2End,false);
		}
	},
	
	/**
	 * similarity of overlapping calendar intervals
	 */
	OVERLAP_BOTH(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap both";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (t1Start.equals(t2Start) && t1End.equals(t2End)) return 1F;
			
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			
			return MyMath.round(0.5F*
			(AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
			AggregationFunction.MAXIMUM.aggregate((DateHandler.getDifferenceInDays(minEnd,minStart)),1) +
			AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
					AggregationFunction.MAXIMUM.aggregate(DateHandler.getDifferenceInDays(maxEnd,maxStart),1)), 3
			);
		}
	},
	
	/**
	 * dice similarity of overlapping calendar intervals
	 */
	OVERLAP_DICE(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap dice";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (t1Start.equals(t2Start) && t1End.equals(t2End)) return 1F;
			
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			
			return MyMath.round(
			2 * AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
					(AggregationFunction.MAXIMUM.aggregate((DateHandler.getDifferenceInDays(minEnd,minStart)),1) +
					AggregationFunction.MAXIMUM.aggregate(DateHandler.getDifferenceInDays(maxEnd,maxStart),1)), 3
			);
		}
	},
	
	/**
	 * normalized dice similarity of overlapping calendar intervals
	 */
	OVERLAP_DICE_SIGMOID_NORMALIZED(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap dice sigmoid";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			int minDiffInDays = DateHandler.getDifferenceInDays(minEnd,minStart);
			int maxDiffInDays = DateHandler.getDifferenceInDays(maxEnd,maxStart);
			
			return MyMath.round(
			(2 * AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
					(AggregationFunction.MAXIMUM.aggregate(minDiffInDays,1) +
					AggregationFunction.MAXIMUM.aggregate(maxDiffInDays,1))
			) * (float)MyMath.sigmoid(-0.025F,minDiffInDays+maxDiffInDays,-10.0F), 3);
		}
	},
	
	/**
	 * similarity of overlapping calendar intervals
	 */
	OVERLAP_MIN(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap min";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (t1Start.equals(t2Start) && t1End.equals(t2End)) return 1F;
			
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			
			return MyMath.round(
					AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
						AggregationFunction.MINIMUM.aggregate(
							AggregationFunction.MAXIMUM.aggregate((DateHandler.getDifferenceInDays(minEnd,minStart)),1),
							AggregationFunction.MAXIMUM.aggregate(DateHandler.getDifferenceInDays(maxEnd,maxStart),1)), 3
			);
		}
	},
	
	/**
	 * similarity of overlapping calendar intervals
	 */
	OVERLAP_MIN_SIGMOID_NORMALIZED(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap min sigmoid";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			int minDiffInDays = DateHandler.getDifferenceInDays(minEnd,minStart);
			int maxDiffInDays = DateHandler.getDifferenceInDays(maxEnd,maxStart);
			
			return MyMath.round(
				(AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
					AggregationFunction.MINIMUM.aggregate(
							AggregationFunction.MAXIMUM.aggregate(minDiffInDays,1),
							AggregationFunction.MAXIMUM.aggregate(maxDiffInDays,1))
			) * (float)MyMath.sigmoid(-0.025F, minDiffInDays+maxDiffInDays, -10.0F), 3);
		}
	},
	
	/**
	 * similarity of overlapping calendar intervals
	 */
	OVERLAP_MAX(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap max";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (t1Start.equals(t2Start) && t1End.equals(t2End)) return 1F;
			
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			
			return MyMath.round(
					AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
							AggregationFunction.MAXIMUM.aggregate(
									AggregationFunction.MAXIMUM.aggregate((DateHandler.getDifferenceInDays(minEnd,minStart)),1),
									AggregationFunction.MAXIMUM.aggregate(DateHandler.getDifferenceInDays(maxEnd,maxStart),1)), 3
			);
		}
	},
	
	/**
	 * similarity of overlapping calendar intervals
	 */
	OVERLAP_MAX_SIGMOID_NORMALIZED(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval overlap max sigmoid";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if ((t1Start.before(t2Start) && t1End.before(t2Start)) ||
					(t1Start.after(t2Start) && t1Start.after(t2End))) return 0F;
			
			SimplifiedGregorianCalendar minStart = t1Start.before(t2Start)?t1Start:t2Start;
			SimplifiedGregorianCalendar minEnd   = t1Start.before(t2Start)?t1End:t2End;
			SimplifiedGregorianCalendar maxStart = t1Start.before(t2Start)?t2Start:t1Start;
			SimplifiedGregorianCalendar maxEnd   = t1Start.before(t2Start)?t2End:t1End;
			
			// overlap == number of days (earliest end date, latest start date)
			int diffInDays = DateHandler.getDifferenceInDays(minEnd.before(maxEnd)?minEnd:maxEnd,maxStart);
			int minDiffInDays = DateHandler.getDifferenceInDays(minEnd,minStart);
			int maxDiffInDays = DateHandler.getDifferenceInDays(maxEnd,maxStart);
			
			return MyMath.round(
				(AggregationFunction.MAXIMUM.aggregate(diffInDays,1) /
					AggregationFunction.MAXIMUM.aggregate(
							AggregationFunction.MAXIMUM.aggregate(minDiffInDays,1),
							AggregationFunction.MAXIMUM.aggregate(maxDiffInDays,1))
			) * (float)MyMath.sigmoid(-0.025F,minDiffInDays+maxDiffInDays,-10.0F), 3);
		}
	},
	
	/**
	 * similarity of year variants
	 */
	YEAR_VARIANTS(ExecutionClassTypes.DATE_INTERVAL) {
		public String getLabel() {
			return "date interval year variants";
		}
		public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, 
				SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
			if (!(t1Start.get(Calendar.DAY_OF_MONTH)==t2Start.get(Calendar.DAY_OF_MONTH) &&
					t1Start.get(Calendar.MONTH)==t2Start.get(Calendar.MONTH) &&
					t1End.get(Calendar.DAY_OF_MONTH)==t2End.get(Calendar.DAY_OF_MONTH) &&
					t1End.get(Calendar.MONTH)==t2End.get(Calendar.MONTH))) 
				return 0F;
			float avgYearDistance = AggregationFunction.AVERAGE.aggregate(
					Math.abs(t1Start.get(Calendar.YEAR)-t2Start.get(Calendar.YEAR)),
					Math.abs(t1End.get(Calendar.YEAR)-t2End.get(Calendar.YEAR)));
			if (avgYearDistance>100) return 0F;
			return MyMath.round(1F-avgYearDistance/100, 3);
		}
	};
	
	
	
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private DateSimilarityFunctions(ExecutionClassTypes eType) {
		this.matcherType = MatcherTypes.DATE;
		this.executionClassType = eType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(String, String)
	 */
	public float computeConfidence(String s1, String s2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
	}

	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static DateSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (DateSimilarityFunctions f : DateSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the date similarity function '"+label+"'.");
	}
}
