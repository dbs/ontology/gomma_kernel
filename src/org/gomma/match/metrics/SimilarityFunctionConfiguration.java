package org.gomma.match.metrics;

import org.gomma.match.MatcherTypes;
import org.gomma.match.ExecutionClassTypes;

public interface SimilarityFunctionConfiguration {

	public MatcherTypes getMatcherType();
	
	public ExecutionClassTypes getExecutionClassType();
}
