package org.gomma.match.metrics;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.ExecutionClassTypes;
import org.gomma.match.MatcherTypes;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.DistanceFunctionImpl;
import org.gomma.util.math.SetSimilarityFunctions;
import org.gomma.util.string.StringSetComparer;

import uk.ac.shef.wit.simmetrics.similaritymetrics.Jaro;
import uk.ac.shef.wit.simmetrics.similaritymetrics.JaroWinkler;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;
import uk.ac.shef.wit.simmetrics.similaritymetrics.MongeElkan;
import uk.ac.shef.wit.simmetrics.similaritymetrics.NeedlemanWunch;
import uk.ac.shef.wit.simmetrics.similaritymetrics.SmithWaterman;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Soundex;

/**
 * The enumeration provides a set of string-based similarity function
 * and is used as wrapper for the similarity function implementation.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum StringSimilarityFunctions implements SimilarityFunction, SimilarityFunctionConfiguration {

	
	EXACT(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "no similarity function defined";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1,s2)) return 0F;
			return s1.trim().equals(s2.trim())?1:0;
		}
	},
	
	BIGRAM_DICE(ExecutionClassTypes.STRING_MAP) {
		public String getLabel() {
			return "bigram-dice";
		}
		/**
		 * @see SimilarityFunction#computeConfidence(String, String)
		 */
		public float computeConfidence(String s1, String s2) throws OperatorException {
			throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
		}
		
		/**
		 * @see SimilarityFunction#computeConfidence(Map, Map)
		 */
		public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
			if (isEmptyCondition(m1, m2)) return 0F;
			return StringSetComparer.compare(m1, m2, SetSimilarityFunctions.DICE);
		}
	},
	TRIGRAM_DICE(ExecutionClassTypes.STRING_MAP) {
		public String getLabel() {
			return "trigram-dice";
		}

		/**
		 * @see SimilarityFunction#computeConfidence(String, String)
		 */
		public float computeConfidence(String s1, String s2) throws OperatorException {
			throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
		}
		/**
		 * @see SimilarityFunction#computeConfidence(Map, Map)
		 */
		public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
			if (isEmptyCondition(m1, m2)) return 0F;
			return StringSetComparer.compare(m1, m2, SetSimilarityFunctions.DICE);
		}
	},
	TRIGRAM_DICE_FILLING(ExecutionClassTypes.STRING_MAP) {
		public String getLabel() {
			return "trigram filled tail";
		}

		/**
		 * @see SimilarityFunction#computeConfidence(String, String)
		 */
		public float computeConfidence(String s1, String s2) throws OperatorException {
			throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
		}
		/**
		 * @see SimilarityFunction#computeConfidence(Map, Map)
		 */
		public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
			if (isEmptyCondition(m1, m2)) return 0F;
			return StringSetComparer.compare(m1, m2, SetSimilarityFunctions.DICE);
		}
	},
	
	FULLWORD_DICE(ExecutionClassTypes.STRING_MAP) {
		public String getLabel() {
			return "fullword-dice";
		}

		/**
		 * @see SimilarityFunction#computeConfidence(String, String)
		 */
		public float computeConfidence(String s1, String s2) throws OperatorException {
			throw new OperatorException("The selected similarity function doesn't work with any kind of string matcher.");
		}
		/**
		 * @see SimilarityFunction#computeConfidence(Map, Map)
		 */
		public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
			if (isEmptyCondition(m1, m2)) return 0F;
			return StringSetComparer.compare(m1, m2, SetSimilarityFunctions.DICE);
		}
	},
	AFFIX(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "affix";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return AggregationFunction.MAXIMUM.aggregate(
					StringSimilarityFunctions.PREFIX.computeConfidence(s1,s2),
					StringSimilarityFunctions.SUFFIX.computeConfidence(s1,s2));
		}
	},
	PREFIX(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "prefix";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return s1.startsWith(s2)?1:(s2.startsWith(s1)?1:0);
		}
	},
	PREFIX_OVERLAP(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "prefix_overlap";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			int pos = 0;
			String longString = null;
			String smallString = null;
			if (s2.startsWith(s1)) {
				smallString = s1;
				longString = s2;
				if (smallString!=null&&longString!=null) {
					return ((float)smallString.length())/((float)longString.length());
				}
			}else if (s1.startsWith(s2)) {
				smallString = s2;
				longString = s1;
				if (smallString!=null&&longString!=null) {
					return ((float)smallString.length())/((float)longString.length());
				}
			}else{
				
				if(s1.length()<=s2.length()){
					smallString= s1;
					longString = s2;
				}else{
					smallString= s2;
					longString = s1;
				}
				for(pos=0; pos<smallString.length();pos++){
					if(smallString.toCharArray()[pos]==longString.toCharArray()[pos]){
						pos++;
					}else{
						return (2*(float)pos/(smallString.length()+(float)longString.length()));
					}
				}
				
			}
			return (2*(float)pos/(smallString.length()+(float)longString.length()));
		}
	},
	
	PREFIX_LEVENSHTEIN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "suffix levenshtein";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			String str_small = s1.length()<s2.length()?s1:s2;
			String str_large = s1.length()>=s2.length()?s1:s2;
			System.out.println(str_small);
			System.out.println(str_large);
			
			return  StringSimilarityFunctions.FAST_LEVENSHTEIN.computeConfidence(str_small,str_large.substring(1,str_small.length()));
		}
	},
	SUFFIX(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "suffix";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return s1.endsWith(s2)?1:(s2.endsWith(s1)?1:0);
		}
	},
	SUFFIX_LEVENSHTEIN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "suffix levenshtein";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			String str_small = s1.length()<s2.length()?s1:s2;
			String str_large = s1.length()>=s2.length()?s1:s2;
			
			return  StringSimilarityFunctions.FAST_LEVENSHTEIN.computeConfidence(str_small,str_large.substring(str_large.length()-str_small.length()));
		}
	},
	LEVENSHTEIN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "levenshtein";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new Levenshtein().getSimilarity(s1,s2);
		}
	},
	FAST_LEVENSHTEIN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "fast_levenshtein";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			/*
		    The difference between this impl. and the previous is that, rather 
		     than creating and retaining a matrix of size s.length()+1 by t.length()+1, 
		     we maintain two single-dimensional arrays of length s.length()+1.  The first, d,
		     is the 'current working' distance array that maintains the newest distance cost
		     counts as we iterate through the characters of String s.  Each time we increment
		     the index of String t we are comparing, d is copied to p, the second int[].  Doing so
		     allows us to retain the previous cost counts as required by the algorithm (taking 
		     the minimum of the cost count to the left, up one, and diagonally up and to the left
		     of the current cost count being calculated).  (Note that the arrays aren't really 
		     copied anymore, just switched...this is clearly much better than cloning an array 
		     or doing a System.arraycopy() each time  through the outer loop.)

		     Effectively, the difference between the two implementations is this one does not 
		     cause an out of memory condition when calculating the LD over two very large strings.  		
		  */		
				
		  int n = s1.length(); // length of s
		  int m = s2.length(); // length of t
				
		  if (n == 0) {
		    return 0;
		  } else if (m == 0) {
		    return 0;
		  }

		  int p[] = new int[n+1]; //'previous' cost array, horizontally
		  int d[] = new int[n+1]; // cost array, horizontally
		  int _d[]; //placeholder to assist in swapping p and d

		  // indexes into strings s and t
		  int i; // iterates through s
		  int j; // iterates through t

		  char t_j; // jth character of t

		  int cost; // cost

		  for (i = 0; i<=n; i++) {
		     p[i] = i;
		  }
				
		  for (j = 1; j<=m; j++) {
		     t_j = s2.charAt(j-1);
		     d[0] = j;
				
		     for (i=1; i<=n; i++) {
		        cost = s1.charAt(i-1)==t_j ? 0 : 1;
		        // minimum of cell to the left+1, to the top+1, diagonally left and up +cost				
		        d[i] = Math.min(Math.min(d[i-1]+1, p[i]+1),  p[i-1]+cost);  
		     }

		     // copy current distance counts to 'previous row' distance counts
		     _d = p;
		     p = d;
		     d = _d;
		  } 
				
		  // our last action in the above loop was to switch d and p, so p now 
		  // actually has the most recent cost counts
		  return 1.0f-((float)p[n]/(float)Math.max(m, n));
		}
	},
	LAST_THREE_LEVEL(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "last three levels";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			//input: two paths (strings separated by "/")
			String threeLevel_s1 = getThreeLevelString(s1);
			String threeLevel_s2 = getThreeLevelString(s2);
			//System.out.println(threeLevel_s1 + "\t" + s1);
			//System.out.println(threeLevel_s2 + "\t" + s2);
			//System.out.println("\n");

			return StringSimilarityFunctions.FAST_LEVENSHTEIN.computeConfidence(threeLevel_s1,threeLevel_s2);
		}
		private String getThreeLevelString(String s) {
			
			String[] s_array = s.split("/"); // array laenge ist immer eins mehr - da ein Feld am path aussen extra angelegt wird wegen separator "/"
			String threeLevel_s = "";

			if(s_array.length==2){			//eine Ebene
				threeLevel_s = "/"+ s_array[1];
			}else if(s_array.length==3){	//zwei Ebenen
				threeLevel_s = "/"+s_array[1] + "/"+s_array[2];
			}else if(s_array.length>=4){	//drei Ebenen
				threeLevel_s = "/"+ s_array[s_array.length-3] +"/"+s_array[s_array.length-2] +"/"+ s_array[s_array.length-1];
			}	                                                               
			return threeLevel_s;
		}
	},
	WEIGHTED_NAME_PATH(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "aggregated name path";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			//input: two paths (strings separated by "/")
			
			int pos1 = s1.lastIndexOf("/");
			String s1_path = s1.substring(0, pos1);
			String s1_name = s1.substring(pos1+1);

			int pos2 = s2.lastIndexOf("/");	
			String s2_path = s2.substring(0, pos2);
			String s2_name = s2.substring(pos2+1);	
			
			float sim_path = StringSimilarityFunctions.SUFFIX_LEVENSHTEIN.computeConfidence(s1_path,s2_path);
			float sim_name = StringSimilarityFunctions.FAST_LEVENSHTEIN.computeConfidence(s1_name,s2_name);
			
			//System.out.println("\n"+s1_path + "\t" + s2_path + "\t" + sim_path);
			//System.out.println(s1_name + "\t" + s2_name + "\t" + sim_name);
			//System.out.println("\t\t"+(float) (0.3*sim_path + 0.7*sim_name));
			return (float) (0.2*sim_path + 0.8*sim_name);
		}
	},

	STR_LENGTH_RATIO(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "string length ratio";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			if(s1.length()>=s2.length()){
				return (float)s2.length()/(float)s1.length();
			} else {
				return (float)s1.length()/(float)s2.length();				
			}
		}
	},
	SOUNDEX(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "soundex";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new Soundex().getSimilarity(s1,s2);
		}
	},
	
	
	HAMMING(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "hamming";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return 1-DistanceFunctionImpl.normalizedHammingDistance(s1,s2);
		}
	},
	
	/**
	 * LOOM stands for 'Lexical OWL Ontology Matcher as introduced by Ghazvinian et al.
	 * For further reading see also: 
	 * Ghazvinian, Noy, Musen: Creating mappings for ontologies in biomedicin: Simple methods work.
	 * as of July, 27th, 2009 it is in submission.
	 * 
	 * (min(|s1|,|s2|)<4)?(exact(s1,s2)=1?sim=1:sim=0):(hamming-distance(s1,s2)<2?sim=1:sim=0)
	 */
	LOOM(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "loom";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			
			if (AggregationFunction.MINIMUM.aggregate(s1.length(),s2.length())<4)
				return EXACT.computeConfidence(s1, s2);
			else if (DistanceFunctionImpl.hammingDistance(s1,s2)<2) return 1F;
			else return 0F;
		}
	},
	JARO(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "jaro";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new Jaro().getSimilarity(s1,s2);
		}
	},
	JARO_WINKLER(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "jaro winkler";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new JaroWinkler().getSimilarity(s1,s2);
		}
	},
	
	NEEDLEMAN_WUNCH(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "needleman wunch";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new NeedlemanWunch().getSimilarity(s1,s2);
		}
	},
	SMITH_WATERMAN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "smith waterman";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new SmithWaterman().getSimilarity(s1,s2);
		}
	},
	MONGE_ELKAN(ExecutionClassTypes.STRING_COMPLETE) {
		public String getLabel() {
			return "monge elkan";
		}
		public float computeConfidence(String s1, String s2) throws OperatorException {
			if (isEmptyCondition(s1, s2)) return 0F;
			return new MongeElkan().getSimilarity(s1,s2);
		}
	};
	
	/**
	 * The item represents the matcher type.
	 */
	private final MatcherTypes matcherType;
	
	/**
	 * The item represents the execution class type.
	 */
	private final ExecutionClassTypes executionClassType;
	
	/**
	 * The constructor initializes the class.
	 */
	private StringSimilarityFunctions(ExecutionClassTypes types) {
		this.matcherType = MatcherTypes.NAME;
		this.executionClassType = types;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getMatcherType()
	 */
	public MatcherTypes getMatcherType() {
		return this.matcherType;
	}
	
	/**
	 * @see SimilarityFunctionConfiguration#getExecutionClassType()
	 */
	public ExecutionClassTypes getExecutionClassType() {
		return this.executionClassType;
	}
	
	protected boolean isEmptyCondition(String s1, String s2) {
		return s1.length()==0 && s2.length()==0;
	}
	
	protected boolean isEmptyCondition(Map<String,Integer> m1, Map<String,Integer> m2) {
		return m1.size()==0 && m2.size()==0;
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float computeConfidence(SimplifiedGregorianCalendar t1Start, SimplifiedGregorianCalendar t1End, SimplifiedGregorianCalendar t2Start, SimplifiedGregorianCalendar t2End) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of date matcher.");
	}
	
	/**
	 * @see SimilarityFunction#computeConfidence(Map, Map)
	 */
	public float computeConfidence(Map<String,Integer> m1, Map<String,Integer> m2) throws OperatorException {
		throw new OperatorException("The selected similarity function doesn't work with any kind of structure matcher.");
	}
	
	public static StringSimilarityFunctions resolveSimilarityFunction(String label) throws NoSuchElementException {
		for (StringSimilarityFunctions f : StringSimilarityFunctions.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not find the string similarity function '"+label+"'.");
	}
}
