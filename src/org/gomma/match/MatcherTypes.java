/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.match;

/**
 * The enumeration holds all implemented types of matcher.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum MatcherTypes {

	/**
	 * The elements represents a name-based matcher.
	 */
	NAME {
		public String toString() {
			return "name matcher (metadata)";
		}
	},
	
	/**
	 * The element represents a date matcher.
	 */
	DATE {
		public String toString() {
			return "single date matcher (metadata)";
		}
	},
	
	/**
	 * The element represents a structure matcher taking labeled nodes or
	 * the pure structure only into account. 
	 */
	STRUCTURE {
		public String toString() {
			return "labelled node and pure structure matcher (metadata)";
		}
	},
	
	/**
	 * The element represents a optimized matcher taking focusing on the
	 * efficient computation of correspondences. 
	 */
	OPTIMIZED {
		public String toString() {
			return "optimized matcher (metadata)";
		}
	},
	
	/**
	 * The element represents a spatial matcher taking coordinates or positions
	 * in a vector space into account. 
	 */
	SPATIAL {
		public String toString() {
			return "spatial matcher (metadata)";
		}
	},
	
	TOKEN {
		public String toString() {
			return "token matcher (metadata)";
		}
	}
	;
}
