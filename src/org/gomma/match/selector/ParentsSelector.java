/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.selector;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements methods allowing to select all
 * parent objects and the corresponding relationships 
 * (to parents) w.r.t. a given object.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ParentsSelector implements SourceVersionSelector {

	/**
	 * The constructor initializes the class.
	 */
	public ParentsSelector() {}
	
	/**
	 * @see SourceVersionSelector#getObjectSelection(SourceVersionStructure, Obj)
	 */
	public ObjSet getObjectSelection(SourceVersionStructure s, Obj o) throws WrongSourceException {
		return s.getSuccessorObjects(o);
	}
	
	/**
	 * @see SourceVersionSelector#getRelationshipSelection(SourceVersionStructure, Obj)
	 */
	public ObjRelationshipSet getRelationshipSelection(SourceVersionStructure s, Obj o) throws WrongSourceException {
		return s.getInRelationships(o);
	}
}
