/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.selector;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements methods allowing to select all
 * parent, children and sibling objects and the corresponding 
 * relationships (to parents, siblings and children) w.r.t. a given object.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ParentSiblingsChildrenSelector implements SourceVersionSelector {

	private ParentsSelector parentsSelector;
	private SiblingsSelector siblingsSelector;
	private ChildrenSelector childrenSelector;
	
	/**
	 * The constructor initializes the class.
	 */
	public ParentSiblingsChildrenSelector() {
		this.parentsSelector = new ParentsSelector();
		this.siblingsSelector= new SiblingsSelector(); 
		this.childrenSelector= new ChildrenSelector(); 
	}
	
	/**
	 * @see SourceVersionSelector#getObjectSelection(SourceVersionStructure, Obj)
	 */
	public ObjSet getObjectSelection(SourceVersionStructure s, Obj o) throws WrongSourceException, GraphInitializationException {
		ObjSet objs = this.parentsSelector.getObjectSelection(s,o);
		for (Obj sibling : this.siblingsSelector.getObjectSelection(s,o))
			objs.addObj(sibling);
		for (Obj child : this.childrenSelector.getObjectSelection(s,o))
			objs.addObj(child);
		return objs;
	}
	
	/**
	 * @see SourceVersionSelector#getRelationshipSelection(SourceVersionStructure, Obj)
	 */
	public ObjRelationshipSet getRelationshipSelection(SourceVersionStructure s, Obj o) throws WrongSourceException, GraphInitializationException {
		ObjRelationshipSet objRels = this.parentsSelector.getRelationshipSelection(s,o);
		for (ObjRelationship siblingRel : this.siblingsSelector.getRelationshipSelection(s,o).getCollection())
			objRels.addObjectRelationship(siblingRel);
		for (ObjRelationship childRel : this.childrenSelector.getRelationshipSelection(s,o).getCollection())
			objRels.addObjectRelationship(childRel);
		return objRels;
	}

}
