/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.selector;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The interface defines methods allowing to select relevant sets of
 * objects and relationships.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface SourceVersionSelector {

	/**
	 * The method returns a set of objects of the given source version structure 
	 * which are in any way included in the surroundings of the specified object o.
	 * @param s source version structure
	 * @param o object o as part of the source version structure s
	 * @return set of relevant objects
	 * @throws WrongSourceException
	 * @throws GraphInitializationException
	 */
	public ObjSet getObjectSelection(SourceVersionStructure s, Obj o) throws WrongSourceException, GraphInitializationException;
	
	/**
	 * The method returns a set of relationships of the given source version structure
	 * which are in any way included in the surroundings of the specified object o.
	 * @param ssource version structure
	 * @param o object o as part of the source version structure s
	 * @return set of relevant objects
	 * @throws WrongSourceException
	 * @throws GraphInitializationException
	 */
	public ObjRelationshipSet getRelationshipSelection(SourceVersionStructure s, Obj o) throws WrongSourceException, GraphInitializationException;
}
