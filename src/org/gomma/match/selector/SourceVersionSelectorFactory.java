/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.selector;

import java.util.NoSuchElementException;

/**
 * The class implements factory methods returning 
 * specific selector instances.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SourceVersionSelectorFactory {

	/**
	 * The constructor initializes the class.
	 */
	private SourceVersionSelectorFactory() {}
	
	/**
	 * The method returns an selector instance w.r.t. the specified type.
	 * @param type source version selector type
	 * @return instance of the selector
	 */
	public static SourceVersionSelector getSourceVersionSelector(SourceVersionSelectorTypes type) {
		switch (type) {
			case CHILDREN: return getChildrenSelector();
			case PARENTS:  return getParentsSelector();
			case SIBLINGS: return getSiblingsSelector();
			case PARENTS_CHILDREN: return getParentsChildrenSelector();
			case PARENTS_SIBLINGS: return getParentsSiblingsSelector();
			case PARENTS_SIBLINGS_CHILDREN: return getParentsSiblingsChildrenSelector();
			case CONNECTED_SUBGRAPH: return getConnectedSubGraphSelector();
			default: throw new NoSuchElementException("Could not find any implementation for element selection.");
		}
	}
	
	/**
	 * The method returns an instance of the children selector.
	 * @return children selector
	 */
	public static ChildrenSelector getChildrenSelector() {
		return new ChildrenSelector();
	}
	
	/**
	 * The method returns an instance of the parent selector.
	 * @return parent selector
	 */
	public static ParentsSelector getParentsSelector() {
		return new ParentsSelector();
	}
	
	/**
	 * The method returns an instance of the sibling selector.
	 * @return sibling selector
	 */
	public static SiblingsSelector getSiblingsSelector() {
		return new SiblingsSelector();
	}
	
	/**
	 * The method returns an instance of the parent-children selector.
	 * @return parent-children selector
	 */
	public static ParentsChildrenSelector getParentsChildrenSelector() {
		return new ParentsChildrenSelector();
	}
	
	/**
	 * The method returns an instance of the parent-sibling selector.
	 * @return parent-sibling selector
	 */
	public static ParentsSiblingsSelector getParentsSiblingsSelector() {
		return new ParentsSiblingsSelector();
	}
	
	/**
	 * The method returns an instance of the parent-children-sibling selector.
	 * @return parent-children-sibling selector
	 */
	public static ParentSiblingsChildrenSelector getParentsSiblingsChildrenSelector() {
		return new ParentSiblingsChildrenSelector();
	}
	
	/**
	 * The method returns an instance of the connected sub-graph selector.
	 * @return connected sub-graph selector
	 */
	public static ConnectedSubGraphSelector getConnectedSubGraphSelector() {
		return new ConnectedSubGraphSelector();
	}
}