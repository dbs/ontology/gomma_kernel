/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.selector;

/**
 * The enumeration contains selector types allowing to 
 * select objects and relationships in the surroundings 
 * of the source structure w.r.t. a specified object.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum SourceVersionSelectorTypes {

	/**
	 * children objects and relationships
	 */
	CHILDREN, 
	/**
	 * parent objects and relationships
	 */
	PARENTS,
	/**
	 * sibling objects and relationships
	 */
	SIBLINGS,
	/**
	 * parent and children objects and relationships
	 */
	PARENTS_CHILDREN,
	/**
	 * parent and sibling objects and relationships
	 */
	PARENTS_SIBLINGS,
	/**
	 * parent,sibling and children objects and relationships
	 */
	PARENTS_SIBLINGS_CHILDREN,
	/**
	 * objects and relationships of the greatest connected subgraph
	 */
	CONNECTED_SUBGRAPH
}
