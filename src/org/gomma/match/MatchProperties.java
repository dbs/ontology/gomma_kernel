package org.gomma.match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MatchProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3795575028433389580L;
	
	private static final String DOMAIN_SOURCEVERSION_NAME_PATTERN = "[domain.sourceVersion]";
	private static final String RANGE_SOURCEVERSION_NAME_PATTERN  = "[domain.sourceVersion]";
	
	private static final String DOMAIN_ATTRIBUTE_NAME_PATTERN = "[domain.attribute]";
	private static final String RANGE_ATTRIBUTE_NAME_PATTERN  = "[range.attribute]";
	
	private static final String DOMAIN_START_DATE_ATTRIBUTE_NAME_PATTERN = "[domain.startDate]";
	private static final String DOMAIN_END_DATE_ATTRIBUTE_NAME_PATTERN   = "[domain.endDate]";
	private static final String RANGE_START_DATE_ATTRIBUTE_NAME_PATTERN  = "[range.startDate]";
	private static final String RANGE_END_DATE_ATTRIBUTE_NAME_PATTERN    = "[range.endDate]";
	
	private static final String SEPARATE_SOURCE_IDF_PATTERN    = "[idfSeparateSourceMode]";
	
	private static final String INIT_RANGE_IDS = "[initRangeIds]";
	private static final String INIT_DOMAIN_IDS = "[initDomainIds]";
	
	private static final String METRIC_WEIGHTING_PATTERN = "[metricWeighting]";
	
	private Map<String,Set<String>> propMap = null;
	
	public MatchProperties() {
		this.propMap = new HashMap<String,Set<String>>();		
	}
	public void setDomainSourceVersion(String sourceVersionName) {
		if (sourceVersionName==null) 
			throw new NullPointerException("The domain source version name could not be resolved.");
		this.addSingleProperty(DOMAIN_SOURCEVERSION_NAME_PATTERN, sourceVersionName);
	}
	
	public void setRangeSourceVersion(String sourceVersionName) {
		if (sourceVersionName==null) 
			throw new NullPointerException("The range source version name could not be resolved.");
		this.addSingleProperty(RANGE_SOURCEVERSION_NAME_PATTERN, sourceVersionName);
	}
	
	public boolean containsDomainSourceVersion() {
		return this.propMap.containsKey(DOMAIN_SOURCEVERSION_NAME_PATTERN);
	}
	
	public boolean containsRangeSourceVersion() {
		return this.propMap.containsKey(RANGE_SOURCEVERSION_NAME_PATTERN);
	}
	
	public String getDomainSourceVersionName() {
		if (this.containsDomainSourceVersion())
			return this.propMap.get(DOMAIN_SOURCEVERSION_NAME_PATTERN).iterator().next();
		else return "";
	}
	
	public String getRangeSourceVersionName() {
		if (this.containsRangeSourceVersion())
			return this.propMap.get(RANGE_SOURCEVERSION_NAME_PATTERN).iterator().next();
		else return "";
	}
	
	public void addDomainAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The domain attribute name could not be resolved.");
		this.addMultipleProperty(DOMAIN_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public void addRangeAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The range attribute name could not be resolved.");
		this.addMultipleProperty(RANGE_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public boolean containsDomainAttributeNames() {
		return this.propMap.containsKey(DOMAIN_ATTRIBUTE_NAME_PATTERN);
	}
	
	public boolean containsRangeAttributeNames() {
		return this.propMap.containsKey(RANGE_ATTRIBUTE_NAME_PATTERN);
	}
	
	public List<String> getDomainAttributeNameList() {
		List<String> list = new ArrayList<String>();
		
		if (this.containsDomainAttributeNames()) {
			for (String name : this.propMap.get(DOMAIN_ATTRIBUTE_NAME_PATTERN))
				list.add(name);
		}
		return list;
	}
	
	public List<String> getRangeAttributeNameList() {
		List<String> list = new ArrayList<String>();
		
		if (this.containsDomainAttributeNames()) {
			for (String name : this.propMap.get(RANGE_ATTRIBUTE_NAME_PATTERN))
				list.add(name);
		}
		return list;
	}
	public void setDomainStartDateAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The domain start date attribute name could not be resolved.");
		this.addSingleProperty(DOMAIN_START_DATE_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public void setRangeStartDateAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The range start date attribute name could not be resolved.");
		this.addSingleProperty(RANGE_START_DATE_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public void setDomainEndDateAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The domain end date attribute name could not be resolved.");
		this.addSingleProperty(DOMAIN_END_DATE_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public void setRangeEndDateAttributeName(String name) {
		if (name==null) 
			throw new NullPointerException("The range end date attribute name could not be resolved.");
		this.addSingleProperty(RANGE_END_DATE_ATTRIBUTE_NAME_PATTERN, name);
	}
	
	public boolean containsDomainStartDateAttributeName() {
		return this.propMap.containsKey(DOMAIN_START_DATE_ATTRIBUTE_NAME_PATTERN);
	}
	
	public boolean containsRangeStartDateAttributeName() {
		return this.propMap.containsKey(RANGE_START_DATE_ATTRIBUTE_NAME_PATTERN);
	}
	
	public boolean containsDomainEndDateAttributeName() {
		return this.propMap.containsKey(DOMAIN_END_DATE_ATTRIBUTE_NAME_PATTERN);
	}
	
	public boolean containsRangeEndDateAttributeName() {
		return this.propMap.containsKey(RANGE_END_DATE_ATTRIBUTE_NAME_PATTERN);
	}
	
	public String getDomainStartDateAttributeName() {
		if (this.containsDomainStartDateAttributeName())
			return this.propMap.get(DOMAIN_START_DATE_ATTRIBUTE_NAME_PATTERN).iterator().next();
		else return "";
	}
	
	public String getRangeStartDateAttributeName() {
		if (this.containsRangeStartDateAttributeName())
			return this.propMap.get(RANGE_START_DATE_ATTRIBUTE_NAME_PATTERN).iterator().next();
		else return "";
	}
	
	public String getDomainEndDateAttributeName() {
		if (this.containsDomainEndDateAttributeName())
			return this.propMap.get(DOMAIN_END_DATE_ATTRIBUTE_NAME_PATTERN).iterator().next();
		else return "";
	}
	
	public String getRangeEndDateAttributeName() {
		if (this.containsRangeEndDateAttributeName())
			return this.propMap.get(RANGE_END_DATE_ATTRIBUTE_NAME_PATTERN).iterator().next();
		else return "";
	}
	public int size() {
		return this.propMap.size();
	}	
	private void addSingleProperty(String pattern, String value) {
		Set<String> set = new HashSet<String>();
		set.add(value);
		this.propMap.put(pattern, set);
	}
	
	private void addMultipleProperty(String pattern, String value) {
		Set<String> set;
		if (this.propMap.containsKey(pattern))
			set = this.propMap.get(pattern);
		else set = new HashSet<String>();
		
		set.add(value);
		this.propMap.put(pattern, set);
	}
	public void clear() {
		this.propMap.clear();
	}
	public String getSeparateSourceIdf() {
		if (this.containsSeparateSourceIDF()){
			return this.propMap.get(SEPARATE_SOURCE_IDF_PATTERN).iterator().next();}
		else {
			System.out.println("WARNING! You must provide the boolean property "+SEPARATE_SOURCE_IDF_PATTERN+"! The default is \"false\". Use MatchProperties.addIdfForSingleSources!");
			return "false";
		}
	}
	public boolean containsSeparateSourceIDF() {
		return this.propMap.containsKey(SEPARATE_SOURCE_IDF_PATTERN);
	}
	public void setSeparateSourceIDF(String separateSourceIDF) {		
		if (separateSourceIDF==null) 
			throw new NullPointerException("weighting metric name could not be resolved.");
		this.addIdfForSeparateSources(separateSourceIDF);
	}
	public void addIdfForSeparateSources(String name) {
		if (name==null) 
			throw new NullPointerException("The weighting metric  name could not be resolved.");
		this.addSingleProperty(SEPARATE_SOURCE_IDF_PATTERN, name);
	}
	
	public String getMetricWeighting() {
		if (this.containsMetricWeighting()){
			return this.propMap.get(METRIC_WEIGHTING_PATTERN).iterator().next();}
		else {
			System.out.println("WARNING! You must provide the string property "+METRIC_WEIGHTING_PATTERN+"! The default is \"normal\". Use MatchProperties.addMetricWeighting!");
			return "normal";
		}
	}
	public boolean containsMetricWeighting() {
		return this.propMap.containsKey(METRIC_WEIGHTING_PATTERN);
	}
	
	public void setMetricWeighting(String weighting) {		
		if (weighting==null) 
			throw new NullPointerException("Metric Weight  name could not be resolved.");
		this.addMetricWeighting(weighting);
	}
	public void addMetricWeighting(String name) {
		if (name==null) 
			throw new NullPointerException("The metric weight attribute name could not be resolved.");
		this.addSingleProperty(METRIC_WEIGHTING_PATTERN, name);
	}
	
	
	
	public String getInitRange() {
		if (this.containsInitRange()){
			return this.propMap.get(INIT_RANGE_IDS).iterator().next();}
		else {
			return "true";
		}
	}
	public boolean containsInitRange() {
		return this.propMap.containsKey(INIT_RANGE_IDS);
	}
	
	public void setInitRange(String weighting) {		
		if (weighting==null) 
			throw new NullPointerException("init range  name could not be resolved.");
		this.addInitRange(weighting);
	}
	public void addInitRange(String name) {
		if (name==null) 
			throw new NullPointerException("The init range attribute name could not be resolved.");
		this.addSingleProperty(INIT_RANGE_IDS, name);
	}
	
	public String getInitDomain() {
		if (this.containsInitDomain()){
			return this.propMap.get(INIT_DOMAIN_IDS).iterator().next();}
		else {
			return "true";
		}
	}
	public boolean containsInitDomain() {
		return this.propMap.containsKey(INIT_DOMAIN_IDS);
	}
	
	public void setInitDomain(String weighting) {		
		if (weighting==null) 
			throw new NullPointerException("Metric Weight  name could not be resolved.");
		this.addInitDomain(weighting);
	}
	public void addInitDomain(String name) {
		if (name==null) 
			throw new NullPointerException("The metric weight attribute name could not be resolved.");
		this.addSingleProperty(INIT_DOMAIN_IDS, name);
	}
}
