/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.spatial;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements a matcher to match objects w.r.t. 
 * single calendar dates.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class LatLongCosineMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public LatLongCosineMatcher() {}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			Map<Integer,List<Float>> domainToMatch = new HashMap<Integer, List<Float>>();
			Map<Integer,List<Float>> rangeToMatch = new HashMap<Integer, List<Float>>();
			
			//Pre-Processing
			for (Obj domainObj : domain) {
				List<String> domainValuesAsString = super.getAttributeValues(domainObj, true);
				if (domainValuesAsString!=null&&domainValuesAsString.size()>0) {
					List<Float> domainValuesAsFloat = this.convertToFloat(domainValuesAsString);
					domainToMatch.put(domainObj.getID(), domainValuesAsFloat);
				}
			}
			for (Obj rangeObj : range) {
				List<String> rangeValuesAsString = super.getAttributeValues(rangeObj, false);
				if (rangeValuesAsString!=null&&rangeValuesAsString.size()>0) {
					List<Float> rangeValuesAsFloat = this.convertToFloat(rangeValuesAsString);
					rangeToMatch.put(rangeObj.getID(), rangeValuesAsFloat);
				}
			}
			
			//Matching
			for (int domainObjID : domainToMatch.keySet()) {
				List<Float> domainValuesAsFloat = domainToMatch.get(domainObjID);
				for (int rangeObjID : rangeToMatch.keySet()) {
					List<Float> rangeValuesAsFloat = rangeToMatch.get(rangeObjID);
					
					result = this.addCorrespondence(domain.getObj(domainObjID),range.getObj(rangeObjID),
							this.computeMatch(domainValuesAsFloat,rangeValuesAsFloat),result,true);
				}
			}
			
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (NumberFormatException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			Obj domainObj, rangeObj;
				
			for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
				if (!(domain.contains(initCorr.getDomainObjID()) &&
						range.contains(initCorr.getRangeObjID()))) continue;
					
				domainObj = domain.getObj(initCorr.getDomainObjID());
				rangeObj = range.getObj(initCorr.getRangeObjID());
					
				List<String> domainValuesAsString = super.getAttributeValues(domainObj, true);
				List<Float> domainValuesAsFloat = this.convertToFloat(domainValuesAsString);
				List<String> rangeValuesAsString = super.getAttributeValues(rangeObj, false);
				List<Float> rangeValuesAsFloat = this.convertToFloat(rangeValuesAsString);
				
				if (domainValuesAsFloat.size()==rangeValuesAsFloat.size()) {
					result = this.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(domainValuesAsFloat,rangeValuesAsFloat),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (NumberFormatException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private MatchResult computeMatch(List<Float> domainCoordinates, List<Float> rangeCoordinates) throws OperatorException {
		if (domainCoordinates.size()!=rangeCoordinates.size()) {
			throw new OperatorException("Dimension of the coordinates differ, coordinates should be in the same vector space !!");
		}
		/*
		float lengthDomain = 0;
		float lengthRange = 0;
		float scalarProduct = 0;
		
		for (float domainValue : domainCoordinates) {
			lengthDomain += domainValue*domainValue;
		}
		lengthDomain = (float)Math.sqrt(lengthDomain);
		
		for (float rangeValue : rangeCoordinates) {
			lengthRange += rangeValue*rangeValue;
		}
		lengthRange = (float)Math.sqrt(lengthRange);
		
		for (int i=0;i<domainCoordinates.size();i++) {
			scalarProduct += domainCoordinates.get(i)*rangeCoordinates.get(i);
		}
		float cosineSimilarity = scalarProduct / (lengthDomain*lengthRange);
		*/
		float lat1 = (float)(domainCoordinates.get(0)*Math.PI/180F);
		float lat2 = (float)(rangeCoordinates.get(0)*Math.PI/180F);
		float long1 = (float)(domainCoordinates.get(1)*Math.PI/180F);
		float long2 = (float)(rangeCoordinates.get(1)*Math.PI/180F);
		
		
		float dist = (float) (Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(long2 - long1)));
		dist = (float)(dist / (2*Math.PI));
		
		return new MatchResult(1-dist, 1, "N/A");
		//return new MatchResult((1.0F+cosineSimilarity)/2.0F,1,"N/A");
	}
	
	private List<Float> convertToFloat(List<String> vectorAsString) {
		List<Float> result = new Vector<Float>();
		for (String vectorValue : vectorAsString) {
			result.add(Float.valueOf(vectorValue));
		}
		return result;
	}
}
