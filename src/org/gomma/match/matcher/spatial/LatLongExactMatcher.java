/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.spatial;

import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements a matcher to match objects w.r.t. 
 * single calendar dates.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class LatLongExactMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public LatLongExactMatcher() {}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : domain) {
				List<String> domainValuesAsString = super.getAttributeValues(domainObj, true);
				for (Obj rangeObj : range) {
					List<String> rangeValuesAsString = super.getAttributeValues(rangeObj, false);
					
					if (domainValuesAsString.size()==rangeValuesAsString.size()) {
						result = this.addCorrespondence(domainObj,rangeObj,
								this.computeMatch(domainValuesAsString,rangeValuesAsString),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			Obj domainObj, rangeObj;
				
			for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
				if (!(domain.contains(initCorr.getDomainObjID()) &&
						range.contains(initCorr.getRangeObjID()))) continue;
					
				domainObj = domain.getObj(initCorr.getDomainObjID());
				rangeObj = range.getObj(initCorr.getRangeObjID());
					
				List<String> domainValuesAsString = super.getAttributeValues(domainObj, true);
				List<String> rangeValuesAsString = super.getAttributeValues(rangeObj, false);
				
				if (domainValuesAsString.size()==rangeValuesAsString.size()) {
					result = this.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(domainValuesAsString,rangeValuesAsString),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private MatchResult computeMatch(List<String> domainCoordinates, List<String> rangeCoordinates) throws OperatorException {
		if (domainCoordinates.size()!=rangeCoordinates.size()) {
			throw new OperatorException("Dimension of the coordinates differ, coordinates should be in the same vector space !!");
		}
		
		
		boolean exactTheSame = true;
		for (int i=0;i<domainCoordinates.size();i++) {
			if (!domainCoordinates.get(i).equalsIgnoreCase(rangeCoordinates.get(i))) {
				exactTheSame = false;
			}
		}
		if (exactTheSame) {
			return new MatchResult(1,1,"N/A");
		} else {
			return new MatchResult(0,0,"N/A");
		}
		
		
	}

}
