package org.gomma.match.matcher;

/**
 * The class is a helper class to return match result 
 * for an object correspondence.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class MatchResult {

	private float confidence;
	private int support;
	private String type;
	
	public MatchResult(float conf, int sup, String type) {
		this.confidence=conf;
		this.support=sup;
		this.type=type;
	}
	
	public float getConfidence() {
		return this.confidence;
	}
	
	public int getSupport() {
		return this.support;
	}
	
	public String getType() {
		return this.type;
	}
}
