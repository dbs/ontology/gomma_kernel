/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.structure;

import java.util.Set;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.selector.SourceVersionSelector;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.algorithm.InducedSubGraph;
import org.gomma.util.math.SimilarityFunctionImpl;

/**
 * The class implements a matcher to match objects taking
 * their surrounding structure of the source version into 
 * account. In particular, the matcher compares the graph 
 * structure between two selected sub-graphs induced by
 * relevant objects of a source version.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class StructurePureMatcher extends AbstractStructureMatcher {

	/**
	 * The item represents the selector which extracts relevant objects 
	 * from a source version.
	 */
	private SourceVersionSelector selector;
	
	/**
	 * The constructor initializes the class.
	 */
	public StructurePureMatcher() {}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	public ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		this.selector=super.resolveSourceVersionSelector();
		
		Graph<Obj,ObjRelationship> domainGraph,rangeGraph;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : s1.getAllObjects()) {
				domainGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
						this.selector.getObjectSelection(s1,domainObj).toNodeSet());
				
				for (Obj rangeObj : s2.getAllObjects()) {
					rangeGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
							this.selector.getObjectSelection(s2,rangeObj).toNodeSet());
					
					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeConfidence(domainGraph,rangeGraph),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure) 
	 */
	public ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		Graph<Obj,ObjRelationship> domainGraph,rangeGraph;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			Set<Integer> domainObjIds = s1.getAllObjects().getIDSet(),
						 rangeObjIds  = s2.getAllObjects().getIDSet();

			if (super.initialMap.size()<s1.getNumberOfObjects()*s2.getNumberOfObjects()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap) {
					if (!(domainObjIds.contains(initCorr.getDomainObjID()) &&
							rangeObjIds.contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = s1.getObject(initCorr.getDomainObjID());
					rangeObj = s2.getObject(initCorr.getRangeObjID());
					
					domainGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
							this.selector.getObjectSelection(s1,domainObj).toNodeSet());
					rangeGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
							this.selector.getObjectSelection(s2,rangeObj).toNodeSet());
					
					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeConfidence(domainGraph,rangeGraph),result,true);
				}
			} else {
				for (Obj domainObj : s1.getAllObjects()) {
					domainGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
							this.selector.getObjectSelection(s1,domainObj).toNodeSet());
					
					for (Obj rangeObj : s2.getAllObjects()) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						
						rangeGraph = InducedSubGraph.getInducedSubGraph(s1.getStructure(),
								this.selector.getObjectSelection(s2,rangeObj).toNodeSet());
						
						result = super.addCorrespondence(domainObj,rangeObj,
								this.computeConfidence(domainGraph,rangeGraph),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		}			
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	private MatchResult computeConfidence(Graph<Obj,ObjRelationship> g1, Graph<Obj,ObjRelationship> g2) throws OperatorException {
		try {
			return new MatchResult(
					SimilarityFunctionImpl.normalizedSymmetricDifference(g1,g2),
					1,"N/A");
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphOperationExecutionException e) {
			throw new OperatorException(e.getMessage());
		}
	}
}
