/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.structure;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.selector.SourceVersionSelector;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.SimilarityFunctionImpl;

/**
 * The class implements a matcher to match objects taking
 * their surrounding structure of the source version into 
 * account. In particular, the matcher compares sets of 
 * adjacent objects and relationships.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class StructureLabelMatcher extends AbstractStructureMatcher {

	/**
	 * The item represents the selector which extracts relevant objects 
	 * from a source version.
	 */
	private SourceVersionSelector selector;
	
	/**
	 * The constructor initializes the class.
	 */
	public StructureLabelMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	public ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		this.selector=super.resolveSourceVersionSelector();
		
		ObjSet domainObjs=null, rangeObjs=null;
		ObjRelationshipSet domainRels=null, rangeRels=null;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : s1.getAllObjects()) {
				domainObjs = this.selector.getObjectSelection(s1,domainObj);
				domainRels = this.selector.getRelationshipSelection(s1,domainObj);
				
				for (Obj rangeObj : s2.getAllObjects()) {
					rangeObjs = this.selector.getObjectSelection(s2,rangeObj);
					rangeRels = this.selector.getRelationshipSelection(s2,rangeObj);
					
					result = super.addCorrespondence(domainObj,rangeObj,
								this.computeConfidence(domainObjs,domainRels,rangeObjs,rangeRels),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure) 
	 */
	public ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		this.selector=super.resolveSourceVersionSelector();
		
		ObjSet domainObjs=null, rangeObjs=null;
		ObjRelationshipSet domainRels=null, rangeRels=null;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size()<s1.getNumberOfObjects()*s2.getNumberOfObjects()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(s1.getAllObjects().contains(initCorr.getDomainObjID()) &&
							s2.getAllObjects().contains(initCorr.getRangeObjID()))) continue;
				
					domainObj = s1.getObject(initCorr.getDomainObjID());
					rangeObj = s2.getObject(initCorr.getDomainObjID());
					
					domainObjs = this.selector.getObjectSelection(s1,domainObj);
					domainRels = this.selector.getRelationshipSelection(s1,domainObj);
					rangeObjs  = this.selector.getObjectSelection(s2,rangeObj);
					rangeRels  = this.selector.getRelationshipSelection(s2,rangeObj);
					
					result = super.addCorrespondence(domainObj,rangeObj,
								this.computeConfidence(domainObjs,domainRels,rangeObjs,rangeRels),result,true);
				}
			} else {
				for (Obj domainObj : s1.getAllObjects()) {
					domainObjs = this.selector.getObjectSelection(s1,domainObj);
					domainRels = this.selector.getRelationshipSelection(s1,domainObj);
					
					for (Obj rangeObj : s2.getAllObjects()) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						
						rangeObjs = this.selector.getObjectSelection(s2,rangeObj);
						rangeRels = this.selector.getRelationshipSelection(s2,rangeObj);
						
						result = super.addCorrespondence(domainObj,rangeObj,
									this.computeConfidence(domainObjs,domainRels,rangeObjs,rangeRels),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	private MatchResult computeConfidence(ObjSet objs1, ObjRelationshipSet objRels1, ObjSet objs2, ObjRelationshipSet objRels2) throws OperatorException {
		try {
			return new MatchResult(
					SimilarityFunctionImpl.normalizedSymmetricDifference(objs1,objRels1,objs2,objRels2),
					1,"N/A"); 
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
		
	}
}
