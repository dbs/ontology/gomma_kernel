package org.gomma.match.matcher.structure;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.metrics.StructureSimilarityFunctions;
import org.gomma.match.selector.SourceVersionSelector;
import org.gomma.match.selector.SourceVersionSelectorFactory;
import org.gomma.match.selector.SourceVersionSelectorTypes;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.algorithm.GraphProduct;
import org.gomma.util.graph.algorithm.RandomSurferModel;

public abstract class AbstractStructureMatcher extends AbstractMatcher {

	protected void setInitialNodeConfiguration(Graph<Node,Edge> g, GraphProduct<Obj,ObjRelationship> gp, RandomSurferModel<Node,Edge> model) {
		int nodeID;
		model.setInitialNodeSimilarities(0F);
		for (ObjCorrespondence corr : super.getInitialMapping().getCollection()) {
			nodeID = gp.getNodeID(corr.getDomainObjID(),corr.getRangeObjID()); 
			if (!g.containsNode(nodeID)) continue;
			model.setInitialNodeSimilarity(g.getNode(nodeID),corr.getConfidence());
		}
	}
	
	
	protected SourceVersionSelector resolveSourceVersionSelector() throws OperatorException {
		if (!(super.simFunc instanceof StructureSimilarityFunctions))
			throw new OperatorException("The similarity function is not a structure-based similarity function");
		
		StructureSimilarityFunctions f = (StructureSimilarityFunctions)super.simFunc;
		
		switch (f) {
			case LABELLED_CHILDREN:        return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.CHILDREN);
			case LABELLED_PARENTS:         return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.PARENTS);
			case LABELLED_PARENT_CHILDREN: return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.PARENTS_CHILDREN);
			case LABELLED_PARENT_SIBLINGS: return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.PARENTS_SIBLINGS);
			case LABELLED_FAMILY_GRAPH   : return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.CONNECTED_SUBGRAPH);
			
			case PURE_PARENT_CHILDREN_SIBLING_GRAPH : return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.PARENTS_SIBLINGS_CHILDREN);
			case PURE_FAMILY_GRAPH:        return SourceVersionSelectorFactory.getSourceVersionSelector(SourceVersionSelectorTypes.CONNECTED_SUBGRAPH);

			default: throw new OperatorException("Could not resolve a valid source version selector of the structure label matcher.");
		}
	}
	
}
