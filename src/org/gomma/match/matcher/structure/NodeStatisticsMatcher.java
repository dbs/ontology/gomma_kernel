/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/03
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.structure;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.graph.NodeStatistics;
import org.gomma.util.graph.algorithm.NodeStatistician;
import org.gomma.util.math.DistanceFunctionImpl;

/**
 * The class implements a matcher to match objects taking
 * statistics of their adjacencies into account.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class NodeStatisticsMatcher extends AbstractMatcher {

	private float minConfidence, maxConfidence;
	
	/**
	 * The constructor initializes the class.
	 */
	public NodeStatisticsMatcher() {}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	public ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		NodeStatistics domainObjStat=null, rangeObjStat=null;
		NodeStatistician<Obj,ObjRelationship> domainStat = new NodeStatistician<Obj,ObjRelationship>(s1.getStructure());
		NodeStatistician<Obj,ObjRelationship> rangeStat  = new NodeStatistician<Obj,ObjRelationship>(s2.getStructure());
		
		this.minConfidence=Float.MAX_VALUE;
		this.maxConfidence=0F;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : s1.getAllObjects()) {
				domainObjStat = domainStat.getNodeStatistics(domainObj);
				
				for (Obj rangeObj : s2.getAllObjects()) {
					rangeObjStat = rangeStat.getNodeStatistics(rangeObj);
					
					result = super.addCorrespondence(domainObj,rangeObj,
								this.computeConfidence(domainObjStat,rangeObjStat),result,false);
				}
			}
			return this.normalizeAndFilterConfidence(result);
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure) 
	 */
	public ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		NodeStatistics domainObjStat=null, rangeObjStat=null;
		NodeStatistician<Obj,ObjRelationship> domainStat = new NodeStatistician<Obj,ObjRelationship>(s1.getStructure());
		NodeStatistician<Obj,ObjRelationship> rangeStat  = new NodeStatistician<Obj,ObjRelationship>(s2.getStructure());
		
		this.minConfidence=Float.MAX_VALUE;
		this.maxConfidence=0F;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size()<s1.getNumberOfObjects()*s2.getNumberOfObjects()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(s1.getAllObjects().contains(initCorr.getDomainObjID()) &&
							s2.getAllObjects().contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = s1.getObject(initCorr.getDomainObjID());
					rangeObj = s2.getObject(initCorr.getRangeObjID());
					
					domainObjStat = domainStat.getNodeStatistics(domainObj);
					rangeObjStat  = rangeStat.getNodeStatistics(rangeObj);

					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeConfidence(domainObjStat,rangeObjStat),result,false);
				}
			} else {
				for (Obj domainObj : s1.getAllObjects()) {
					domainObjStat = domainStat.getNodeStatistics(domainObj);
					
					for (Obj rangeObj : s2.getAllObjects()) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						
						rangeObjStat = rangeStat.getNodeStatistics(rangeObj);
						
						result = super.addCorrespondence(domainObj,rangeObj,
									this.computeConfidence(domainObjStat,rangeObjStat),result,false);
					}
				}
			}
			return this.normalizeAndFilterConfidence(result);
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}


	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	
	private MatchResult computeConfidence(NodeStatistics obj1, NodeStatistics obj2) {
		float distance = DistanceFunctionImpl.euclidicDistance(obj1.getValueVector(),obj2.getValueVector()); 
		if (this.minConfidence>distance) this.minConfidence=distance;
		if (this.maxConfidence<distance) this.maxConfidence=distance;
		return new MatchResult(distance,1,"N/A");
	}
	
	private ObjCorrespondenceSet normalizeAndFilterConfidence(ObjCorrespondenceSet objCorrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();

		for (ObjCorrespondence objCorr : objCorrs.getCollection()) {
			objCorr.resetConfidence(this.minConfidence==this.maxConfidence?
					(this.minConfidence==0?1:1 - objCorr.getConfidence() / this.maxConfidence):
					1 - (objCorr.getConfidence() - this.minConfidence) / (this.maxConfidence - this.minConfidence));

			if (objCorr.getConfidence()>=super.simThreshold) result.addCorrespondence(objCorr);
		}

		return result;
	}
}
