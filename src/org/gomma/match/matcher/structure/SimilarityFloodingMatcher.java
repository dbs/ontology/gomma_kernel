/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.structure;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.Matcher;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.EdgeImpl;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.algorithm.GraphProduct;
import org.gomma.util.graph.algorithm.SimilarityFlooding;
import org.gomma.util.graph.algorithm.TensorGraphProduct;
import org.gomma.util.graph.algorithm.SimilarityFlooding.FixPointFormulaVariants;

/**
 * The class implements match methods taking context information into account.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SimilarityFloodingMatcher extends AbstractStructureMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public SimilarityFloodingMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.executeMatch(s1,s2);
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.executeMatch(s1,s2);
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see Matcher#match(SourceVersionStructure, SourceVersionStructure)
	 */
	private ObjCorrespondenceSet executeMatch(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		
		try {
			int objID1, objID2;
			Graph<Obj,ObjRelationship> g1 = s1.getStructure();
			Graph<Obj,ObjRelationship> g2 = s2.getStructure();
			
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			ObjCorrespondence corr;
			float confidence;
			
			GraphProduct<Obj,ObjRelationship> sfGP= new TensorGraphProduct<Obj,ObjRelationship>(g1,g2);
			Graph<Node,Edge> sfG = sfGP.getGraphProduct(true);
			sfG = this.addOppositeEdges(sfG);
//			System.out.print(sfG.getEdgeSetSize());
					
			SimilarityFlooding<Node,Edge> sf = new SimilarityFlooding<Node,Edge>(sfG,0.001F,100);
			
			if (super.getInitialMapping().size()!=0) this.setInitialNodeConfiguration(sfG,sfGP,sf);
			sf.compute(FixPointFormulaVariants.ZERO_PLUS_I_PLUS_PHI_OF_ZERO_AND_I);
//			System.out.print(","+sf.getLastIterationNumber());
					
			for (Node node : sfG.getNodeCollection()) {
				objID1 = sfGP.getG1Node(node.getID()).getID();
				objID2 = sfGP.getG2Node(node.getID()).getID();
						
				confidence = sf.getNodeSimilarity(sf.getLastIterationNumber(),node);
				if (confidence<super.simThreshold) continue;
						
				corr = new ObjCorrespondence.Builder(objID1,s1.getObject(objID1).getLDSID(),objID2,s2.getObject(objID2).getLDSID())
							.domainAccessionNumber(s1.getObject(objID1).getAccessionNumber())
							.rangeAccessionNumber(s2.getObject(objID2).getAccessionNumber())
							.confidence(confidence)
							.support(1)
							.numberOfUserChecks(0)
							.build();
				corr.addDomainSourceVersionID(s1.getSourceVersion().getID());
				corr.addRangeSourceVersionID(s2.getSourceVersion().getID());
						
				result.addCorrespondence(corr);
			}
			return result;
				
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphOperationExecutionException e) {
			throw new OperatorException(e.getMessage());
		} 
	}
	
	private Graph<Node,Edge> addOppositeEdges(Graph<Node,Edge> g) throws GraphInitializationException {
		if (!g.isDirected()) return g;
		Edge oppositeEdge;
		 
		for (Edge e : g.getEdgeCollection()) {
			oppositeEdge = new EdgeImpl(e.getToID(),e.getFromID());
			oppositeEdge.setRelationshipType("inverse of "+e.getRelationshipType());
			g.addEdge(oppositeEdge);
		}
		return g;
	}
}