package org.gomma.match.matcher.structure;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.Matcher;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.algorithm.GraphProduct;
import org.gomma.util.graph.algorithm.PageRank;
import org.gomma.util.graph.algorithm.TensorComplementGraphProduct;

public class PageRankMatcher extends AbstractStructureMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public PageRankMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.executeMatch(s1,s2);
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.executeMatch(s1,s2);
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		throw new OperatorException("The method could not be applied for this structure-based matcher.");
	}
	
	/**
	 * @see Matcher#match(SourceVersionStructure, SourceVersionStructure)
	 */
	private ObjCorrespondenceSet executeMatch(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {

		try {
			int objID1, objID2;
			Graph<Obj,ObjRelationship> g1 = s1.getStructure();
			Graph<Obj,ObjRelationship> g2 = s2.getStructure();
			
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			ObjCorrespondence corr;
			float confidence;
			
			GraphProduct<Obj,ObjRelationship> prGP= new TensorComplementGraphProduct<Obj,ObjRelationship>(g1,g2);
			Graph<Node,Edge> prG = prGP.getGraphProduct(true);

			PageRank<Node,Edge> pr = new PageRank<Node,Edge>(prG,0.0001F,100);
			if (super.getInitialMapping().size()!=0) super.setInitialNodeConfiguration(prG,prGP,pr);
			pr.compute(true);
			
			for (Node node : prG.getNodeCollection()) {
				objID1 = prGP.getG1Node(node.getID()).getID();
				objID2 = prGP.getG2Node(node.getID()).getID();
				
				confidence = pr.getNodeSimilarity(pr.getLastIterationNumber(),node);
				if (confidence<super.simThreshold) continue;
				
				corr = new ObjCorrespondence.Builder(objID1,s1.getObject(objID1).getLDSID(),objID2,s2.getObject(objID2).getLDSID())
							.domainAccessionNumber(s1.getObject(objID1).getAccessionNumber())
							.rangeAccessionNumber(s2.getObject(objID2).getAccessionNumber())
							.confidence(confidence)
							.support(1)
							.numberOfUserChecks(0)
							.build();
				corr.addDomainSourceVersionID(s1.getSourceVersion().getID());
				corr.addRangeSourceVersionID(s2.getSourceVersion().getID());
				
				result.addCorrespondence(corr);
			}
			
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new OperatorException(e.getMessage());
		} catch (GraphOperationExecutionException e) {
			throw new OperatorException(e.getMessage());
		} 
	}
}
