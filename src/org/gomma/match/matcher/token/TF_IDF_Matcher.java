/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.token;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.string.StringDecomposer;


public class TF_IDF_Matcher extends AbstractMatcher {

	//private static final String IDF_FOR_SINGLE_SOURCES = "[idfForSingleSources]";
	private int multiAttributeIndex;
	private IDFIndexReader idfReaderDomain;
	private IDFIndexReader idfReaderRange;
	
	/**
	 * The constructor initializes the class.
	 */
	public TF_IDF_Matcher() {
	}
	
	public TF_IDF_Matcher(int multiAttributeIndex, IDFIndexReader idfReaderDomain, IDFIndexReader idfReaderRange)
	{
		setAttribute(multiAttributeIndex);
		setidfReader(idfReaderDomain,idfReaderRange);
	}
	
	public void setAttribute(int multiAttributeIndex)
	{
		this.multiAttributeIndex= multiAttributeIndex;
	}
	
	public void setidfReader(IDFIndexReader idfReaderDomain,IDFIndexReader idfReaderRange)
	{
		this.idfReaderDomain= idfReaderDomain;
		this.idfReaderRange= idfReaderRange;
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
		try {
			
			String separateSourceIdfString	= this.getMatchProperties().getSeparateSourceIdf();			
			boolean idfForSeparateSources		= Boolean.valueOf(separateSourceIdfString);
						
			
			HashMap<String, Float> globalTokenMapDomain, globalTokenMapRange;
			
			if(idfForSeparateSources){//wenn idfForSeparateSources==true: domain und range separater idf
				
				globalTokenMapDomain	= getGlobalTokenMap(domain, range, idfForSeparateSources, true);
				globalTokenMapRange		= getGlobalTokenMap(domain, range, idfForSeparateSources, false);
				
			}else{//falls ==false, dann wird eine TokenMap f�r beide berechnet, welche die gleiche ist 
				
				HashMap<String, Float> globalTokenMap = getGlobalTokenMap(domain, range, idfForSeparateSources, true);
				globalTokenMapDomain	= globalTokenMap;
				globalTokenMapRange		= globalTokenMap;
			}
			
			IDFIndexReader idfReaderDomain	= new IDFIndexReader(globalTokenMapDomain);
			IDFIndexReader idfReaderRange	= new IDFIndexReader(globalTokenMapRange);
			
			this.setidfReader(idfReaderDomain,idfReaderRange);
			
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : domain) {				
				for (Obj rangeObj : range) {
					
				
					List<Float> confidenceList	= new ArrayList<Float>();
					List<String> attValuesDomain= super.getAttributeValues(domainObj, true);
					List<String> attValuesRange	= super.getAttributeValues(rangeObj, false);
					
					for(String attValueDomain: attValuesDomain){
						for(String attValueRange: attValuesRange){
							confidenceList.add(computeConfidence(attValueDomain,attValueRange));
						}
					}
				
					result = super.addCorrespondence(domainObj,rangeObj,new MatchResult(
																		super.aggFunc.aggregateFloatList(confidenceList),
																		confidenceList.size(), "N/A"),result, true);
				}
					
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException { //Muss noch implementiert werden!
		return initialMap;
	}
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}*/
		
	public float computeConfidence(String s1, String s2)
	{
		Map<String,Integer> wordFreqs1= StringDecomposer.decomposeWordFrequenciesLucene(s1);
		Map<String,Integer> wordFreqs2= StringDecomposer.decomposeWordFrequenciesLucene(s2);
		
		return computeConfidence(wordFreqs1, wordFreqs2);
	}	

	
	public float computeConfidence(Map<String,Integer> wordFreqs1, Map<String,Integer> wordFreqs2)
	{
		Set<String> commonWords = new HashSet<String>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		if(commonWords.size()==0)
			return 0f;
		
		Set<String> wordsOnlyInFirst= new HashSet<String>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		
		Set<String> wordsOnlyInSecond= new HashSet<String>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		double dotProduct= 0, length1= 0, length2= 0;
		
		for(String commonWord : commonWords)
		{
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			
			double tf_idf1= Math.sqrt(tf1) * idf_word_domain;
			double tf_idf1_squared= tf_idf1 * tf_idf1;
			length1+= tf_idf1_squared;
			
			double tf_idf2;
			//if(tf1==tf2) OPTIMIERUNG GEHT NICHT WENN QUELLEN GETRENNT BETRACHTET WERDEN
			//{
			//	tf_idf2= tf_idf1;
			//	length2+= tf_idf1_squared;
			//}
			//else
			//{
				tf_idf2= Math.sqrt(tf2) * idf_word_range;
				length2+= tf_idf2 * tf_idf2;
			//}
			
			dotProduct+= tf_idf1 * tf_idf2;
		}
		
		for(String wordOnlyInFirst : wordsOnlyInFirst)
		{
			double tf_idf1= Math.sqrt(wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
			length1+= tf_idf1 * tf_idf1;
		}
		
		for(String wordOnlyInSecond : wordsOnlyInSecond)
		{
			double tf_idf2= Math.sqrt(wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
			length2+= tf_idf2 * tf_idf2;
		}
		
		length1= Math.sqrt(length1);
		length2= Math.sqrt(length2);
		double productOfLengths= length1 * length2;
		
		 if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
			 productOfLengths= dotProduct;
		
		double cosine= dotProduct / productOfLengths;
		return Double.isNaN(cosine) ? 0f : (float) cosine;
	}
	
	public HashMap<String, Float> getGlobalTokenMap(ObjSet domain, ObjSet range, boolean idfForSeparateSources, boolean isDomain){
		HashMap<String, Float> globalTokenMap = new HashMap<String, Float>();
		int sourceSize;
		if(!idfForSeparateSources){//idf for domain and range
			
			sourceSize = domain.size()+range.size();
			
			for (Obj domainObj : domain) {
				List<String> attValues = super.getAttributeValues(domainObj, true);
				globalTokenMap = extendGlobalTokenMap(attValues,globalTokenMap);
			}
			for (Obj rangeObj : range) {
				List<String> attValues = super.getAttributeValues(rangeObj, false);
				globalTokenMap = extendGlobalTokenMap(attValues,globalTokenMap);
			}
			
		}else{
			if(isDomain){//idf for domain
				
				sourceSize = domain.size();
				for (Obj domainObj : domain) {
					List<String> attValues = super.getAttributeValues(domainObj, true);
					globalTokenMap = extendGlobalTokenMap(attValues,globalTokenMap);
				}
			}else{//idf for range
				
				sourceSize = range.size();
				for (Obj rangeObj : range) {
					List<String> attValues = super.getAttributeValues(rangeObj, false);
					globalTokenMap = extendGlobalTokenMap(attValues,globalTokenMap);
				}
			}
		}
		for(String token : globalTokenMap.keySet()){
			Float wordFrequ = globalTokenMap.get(token);
			//System.out.println(token+","+wordFrequ+","+source.length+" --> "+Math.log((float)(source.length)/(wordFrequ+1.0)));
			float idf= (float)(Math.log((float)(sourceSize)/(wordFrequ+1.0)) + 1.0F);
			globalTokenMap.put(token, idf);
			//System.out.println(token+"\t"+wordFrequ+"\t"+idf);
		}
		return globalTokenMap;
	}

	private HashMap<String, Float> extendGlobalTokenMap(List<String> attValues,HashMap<String, Float> globalTokenMap) {
		
		for(String s:attValues){
			Map<String,Integer> wordFreqs= StringDecomposer.decomposeWordFrequenciesLucene(s);
			
			for(String word:wordFreqs.keySet()){
				
				if(globalTokenMap.containsKey(word)){
					
					float newFreq = globalTokenMap.get(word)+(float)wordFreqs.get(word);
					globalTokenMap.put(word, newFreq);
				}else{
					globalTokenMap.put(word, (float)wordFreqs.get(word));
				}
			}
		}
		
		return globalTokenMap;
	}
}
