package org.gomma.match.matcher.token;

import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentInvertedTrigramReader {
	ConcurrentHashMap<Integer, HashSet<Integer>> invertTrigramTokenMap;
	ConcurrentHashMap<Integer,HashSet<Integer>> trigramIndexMap;

	public ConcurrentInvertedTrigramReader (){
		invertTrigramTokenMap = new ConcurrentHashMap<Integer, HashSet<Integer>> ();
		trigramIndexMap =new ConcurrentHashMap<Integer, HashSet<Integer>> ();
	}
	
	public ConcurrentHashMap<Integer, HashSet<Integer>> getTrigramIndexMap() {
		return trigramIndexMap;
	}

	public ConcurrentHashMap<Integer, HashSet<Integer>> getInvertTrigramTokenMap() {
		return invertTrigramTokenMap;
	}

	public void addTrigrams (HashSet<Integer> triIds, Integer tokenId){
		for (Integer tri:triIds){
			this.addTrigram(tri, tokenId);
		}
		trigramIndexMap.put(tokenId, triIds);
	}
	
	
	public void addTrigram (Integer triId, Integer tokenId){
		HashSet<Integer> tokenSet = invertTrigramTokenMap.get(triId);
		if (tokenSet ==null){
			tokenSet = new HashSet<Integer>();
			invertTrigramTokenMap.put(triId, tokenSet);
		}
		tokenSet.add(tokenId);
	}
	
	public void clear (){
		this.invertTrigramTokenMap.clear();
		this.trigramIndexMap.clear();
	}
}
