package org.gomma.match.matcher.token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.util.string.StringDecomposer;

public class TF_IDFThread extends Thread{

	private ObjSet domain;
	private ObjSet range;
	private ThreadedTF_IDFMatcher matcher;
	private ConcurrentIDFIndexReader idfReaderDomain;
	

	private ConcurrentIDFIndexReader idfReaderRange;
	private int multiAttributeIndex;
	
	private ObjCorrespondenceSet result;
	
	public TF_IDFThread(ObjSet domain, ObjSet range,
			ThreadedTF_IDFMatcher matcher, ConcurrentIDFIndexReader idfReaderRange2,
			ConcurrentIDFIndexReader idfReaderRange3, int multiAttributeIndex) {
		super();
		this.domain = domain;
		this.range = range;
		this.matcher = matcher;
		this.idfReaderDomain = idfReaderRange2;
		this.idfReaderRange = idfReaderRange3;
		this.multiAttributeIndex = multiAttributeIndex;
	}
	
	@Override
	public void run() {
		result = new ObjCorrespondenceSet();
		for (Obj domainObj : domain) {				
			for (Obj rangeObj : range) {
				List<Float> confidenceList	= new ArrayList<Float>();
				List<String> attValuesDomain= getAttributeValues(domainObj, true);
				List<String> attValuesRange	= getAttributeValues(rangeObj, false);
				
				for(String attValueDomain: attValuesDomain){
					for(String attValueRange: attValuesRange){
						confidenceList.add(computeConfidence(attValueDomain,attValueRange));
					}
				}
			
				result = addCorrespondence(domainObj,rangeObj,new MatchResult(
																matcher.getAggregationFunction().aggregateFloatList(confidenceList),
																confidenceList.size(), "N/A"),result, true);
			}
				
		}
		
	}
	
	
	private List<String> getAttributeValues(Obj obj, boolean isDomain) {
		String sourceVersionName=null;
		List<String> attNameList = null;
		
		if (this.matcher.getMatchProperties()!=null && this.matcher.getMatchProperties().size()!=0)  {
			
			if (isDomain?this.matcher.getMatchProperties().containsDomainSourceVersion():this.matcher.getMatchProperties().containsRangeSourceVersion())
				sourceVersionName = isDomain?
						this.matcher.getMatchProperties().getDomainSourceVersionName():
						this.matcher.getMatchProperties().getRangeSourceVersionName();
			if (isDomain?this.matcher.getMatchProperties().containsDomainAttributeNames():this.matcher.getMatchProperties().containsRangeAttributeNames()) 
				attNameList = isDomain?
						this.matcher.getMatchProperties().getDomainAttributeNameList():
						this.matcher.getMatchProperties().getRangeAttributeNameList();	
		}

		return this.extractAttributeValues(obj, sourceVersionName, attNameList);
	}
	
	
	private List<String> extractAttributeValues(Obj obj, String sourceVersionName, List<String> attNameList) {
		List<String> valueList = new ArrayList<String>();
		AttributeSet attSet;
		
		if (sourceVersionName!=null && attNameList!=null) {
			
			SourceVersion s = obj.getSourceVersionSet().getSourceVersion(sourceVersionName);
			attSet = obj.getAttributeValues(s.getID()).getAttributeSet();
			
			for (String attName : attNameList) {
				if (!attSet.contains(attName)) continue;
				for (Attribute att : attSet.getAttributeSet(attName)) 
					for (AttributeValueVersion avv : obj.getAttributeValues(s.getID(),att))
						if (!valueList.contains(avv.toString())) 
							valueList.add(avv.toString());
			}
				
		} else
			
		if (attNameList!=null) {
			
			for (SourceVersion s : obj.getSourceVersionSet()) {
				attSet = obj.getAttributeValues(s.getID()).getAttributeSet();
			
				for (String attName : attNameList) {
					if (!attSet.contains(attName)) continue;
					for (Attribute att : attSet.getAttributeSet(attName)) 
						for (AttributeValueVersion avv : obj.getAttributeValues(s.getID(),att))
							if (!valueList.contains(avv.toString())) 
								valueList.add(avv.toString());
				}
			}
		}
		
		return valueList;
	}
	
	public float computeConfidence(String s1, String s2)
	{
		Map<String,Integer> wordFreqs1= StringDecomposer.decomposeWordFrequenciesLucene(s1);
		Map<String,Integer> wordFreqs2= StringDecomposer.decomposeWordFrequenciesLucene(s2);
		
		return computeConfidence(wordFreqs1, wordFreqs2);
	}	

	public float computeConfidence(Map<String,Integer> wordFreqs1, Map<String,Integer> wordFreqs2)
	{
		Set<String> commonWords = new HashSet<String>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		if(commonWords.size()==0)
			return 0f;
		
		Set<String> wordsOnlyInFirst= new HashSet<String>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		
		Set<String> wordsOnlyInSecond= new HashSet<String>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		double dotProduct= 0, length1= 0, length2= 0;
		
		for(String commonWord : commonWords)
		{
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			
			double tf_idf1= Math.sqrt(tf1) * idf_word_domain;
			double tf_idf1_squared= tf_idf1 * tf_idf1;
			length1+= tf_idf1_squared;
			
			double tf_idf2;
			//if(tf1==tf2) OPTIMIERUNG GEHT NICHT WENN QUELLEN GETRENNT BETRACHTET WERDEN
			//{
			//	tf_idf2= tf_idf1;
			//	length2+= tf_idf1_squared;
			//}
			//else
			//{
				tf_idf2= Math.sqrt(tf2) * idf_word_range;
				length2+= tf_idf2 * tf_idf2;
			//}
			
			dotProduct+= tf_idf1 * tf_idf2;
		}
		
		for(String wordOnlyInFirst : wordsOnlyInFirst)
		{
			double tf_idf1= Math.sqrt(wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
			length1+= tf_idf1 * tf_idf1;
		}
		
		for(String wordOnlyInSecond : wordsOnlyInSecond)
		{
			double tf_idf2= Math.sqrt(wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
			length2+= tf_idf2 * tf_idf2;
		}
		
		length1= Math.sqrt(length1);
		length2= Math.sqrt(length2);
		double productOfLengths= length1 * length2;
		
		 if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
			 productOfLengths= dotProduct;
		
		double cosine= dotProduct / productOfLengths;
		return Double.isNaN(cosine) ? 0f : (float) cosine;
	}
	
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) {
		
		if (checkThreshold && matchResult.getConfidence()<matcher.getSimilarityThreshold()) return set;
			
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		try {
			set.addCorrespondence(cor);
		} catch (WrongSourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return set;
	}

	public ObjCorrespondenceSet getResult() {
		return result;
	}

	
}
