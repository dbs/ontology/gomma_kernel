package org.gomma.match.matcher.token;

import it.unimi.dsi.fastutil.ints.Int2IntMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.string.StringDecomposer;

public class ThreadedTF_IDFEncMatcher extends AbstractMatcher{

	//private static final String IDF_FOR_SINGLE_SOURCES = "[idfForSingleSources]";
		private int multiAttributeIndex;
		private ConcurrentIDFEncIndexReader idfReaderDomain;
		private ConcurrentIDFEncIndexReader idfReaderRange;
		private Integer[] objDomainIds;
		private Integer[] objRangeIds;
		
		/**
		 * first field is obj index ,second attribute, third token ids;
		 */
		private int[][][] domEncodingIds;
		private int[][][] ranEncodingIds;
		private WordIntDictionary dict;
		Int2IntMap wordInDocMapDom;
		Int2IntMap wordInDocMapRange;
		/**
		 * The constructor initializes the class.
		 */
		public ThreadedTF_IDFEncMatcher() {
			
		}
		
		public ThreadedTF_IDFEncMatcher(int multiAttributeIndex, ConcurrentIDFEncIndexReader idfReaderDomain, ConcurrentIDFEncIndexReader idfReaderRange)
		{
			setAttribute(multiAttributeIndex);
			setidfReader(idfReaderDomain,idfReaderRange);
		}
		
		public void setAttribute(int multiAttributeIndex)
		{
			this.multiAttributeIndex= multiAttributeIndex;
		}
		
		public void setidfReader(ConcurrentIDFEncIndexReader idfReaderDomain,ConcurrentIDFEncIndexReader idfReaderRange)
		{
			this.idfReaderDomain= idfReaderDomain;
			this.idfReaderRange= idfReaderRange;
		}

		@Override
		/**
		 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
		 */
		protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
				SourceVersionStructure s2) throws OperatorException {
			return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
		}

		@Override
		/**
		 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
		 */
		protected ObjCorrespondenceSet createMappingUsingInitialMapping(
				SourceVersionStructure s1, SourceVersionStructure s2)
				throws OperatorException {
			return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
		}

		/**
		 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
		 */
		protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
				throws OperatorException {
			dict = WordIntDictionary.getInstance();
			
			EncodingManager encodingManger = EncodingManager.getInstance();
			String domainName = "domain";
			String rangeName = "range";
			
			objDomainIds = (Integer[]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ID_PREF);
			if (objDomainIds ==null){
				encodingManger.encodeObjWithAttValues(domain, domainName, this.getMatchProperties().getDomainAttributeNameList(), true);
			}
			objDomainIds = (Integer[]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ID_PREF);
			domEncodingIds = (int[][][]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ATT_PREF);
			wordInDocMapDom = (Int2IntMap) encodingManger.getEncodedObject(domainName, EncodingManager.IDF_COUNTS);
			Int2IntMap posDomainMap = (Int2IntMap) encodingManger.getEncodedObject(domainName, EncodingManager.POS_MAP);
			
			
			objRangeIds = (Integer[]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ID_PREF);
			if (objRangeIds ==null){
				encodingManger.encodeObjWithAttValues(range, rangeName, this.getMatchProperties().getRangeAttributeNameList(), true);
			}
			objRangeIds =(Integer[]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ID_PREF);
			ranEncodingIds = (int[][][]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ATT_PREF);
			wordInDocMapRange = (Int2IntMap) encodingManger.getEncodedObject(rangeName, EncodingManager.IDF_COUNTS);
			Int2IntMap posRangeMap = (Int2IntMap) encodingManger.getEncodedObject(rangeName, EncodingManager.POS_MAP);
			
			String separateSourceIdfString	= this.getMatchProperties().getSeparateSourceIdf();	
			boolean idfForSeparateSources		= Boolean.valueOf(separateSourceIdfString);
			//System.out.println("separateSourceIdfString: "+separateSourceIdfString);
			ConcurrentHashMap<Integer, Float> globalTokenMapDomain, globalTokenMapRange;
			//this.wordInDocMapDom = new HashMap<Integer,Integer>();
			//this.wordInDocMapRange = new HashMap<Integer,Integer>();
			if(idfForSeparateSources){//wenn idfForSeparateSources==true: domain und range separater idf
				globalTokenMapDomain = this.generateTFIDFMap(objDomainIds.length, wordInDocMapDom, wordInDocMapRange, idfForSeparateSources, true);
				globalTokenMapRange = this.generateTFIDFMap(objRangeIds.length, wordInDocMapDom, wordInDocMapRange, idfForSeparateSources, false);
				//globalTokenMapDomain	= this.getGlobalTokenMap(domain, range, idfForSeparateSources, true);
				//globalTokenMapRange		= this.getGlobalTokenMap(domain, range, idfForSeparateSources, false);
			}else{//falls ==false, dann wird eine TokenMap f�r beide berechnet, welche die gleiche ist 
				
				ConcurrentHashMap<Integer, Float> globalTokenMap = this.generateTFIDFMap(objDomainIds.length+objRangeIds.length, wordInDocMapDom, wordInDocMapRange, idfForSeparateSources, false);
				//ConcurrentHashMap<Integer, Float> globalTokenMap = this.getGlobalTokenMap(domain, range, idfForSeparateSources, true);
				globalTokenMapDomain	= globalTokenMap;
				globalTokenMapRange		= globalTokenMap;
			}
			
			ConcurrentIDFEncIndexReader idfReaderDomain	= new ConcurrentIDFEncIndexReader(globalTokenMapDomain);
			ConcurrentIDFEncIndexReader idfReaderRange	= new ConcurrentIDFEncIndexReader(globalTokenMapRange);
			
			this.setidfReader(idfReaderDomain,idfReaderRange);
			int numberOfProcessros = Runtime.getRuntime().availableProcessors();
			int threadNumber;
			if (domain.size()<1000&&range.size()<1000) {
				threadNumber = 4;
			} else {
				threadNumber = Math.max(numberOfProcessros, 16);
			}
			
			//Gr��ere Ontologie f�r Threading aufspalten (Modulo-Ansatz!!) Ziel: Menge von k Match-Threads
			List<TFIDFEncodedThread> threadList = new ArrayList<TFIDFEncodedThread>();
			if (domain.size()>range.size()) {
				//Domain splitten
				ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
				ArrayList<List<int[][]>> objEncodingParts = new ArrayList<List<int[][]>>();
				for (int i = 0; i<threadNumber;i++){
					objIdParts.add(new ArrayList<Integer>());
					objEncodingParts.add(new ArrayList<int[][]>());
				}
				
				
				for (int id =0; id<domain.size();id++){
					objIdParts.get(id%threadNumber).add(objDomainIds[id]);
					objEncodingParts.get(id%threadNumber).add(domEncodingIds[id]);
				}
				/*
				int domID=0;
				for (Obj o: domain){
					int id = posDomainMap.get(o.getID());
					objIdParts.get(domID%threadNumber).add(objDomainIds[id]);
					objEncodingParts.get(domID++%threadNumber).add(domEncodingIds[id]);
				}
				Integer[] objRangeIds2 = new Integer[range.size()];
				int ranEncodingIds2 [][][] = new int [range.size()][][];
				int id2=0;
				for (Obj o: range){
					int id = posRangeMap.get(o.getID());
					objRangeIds2[id2] = objRangeIds[id];
					ranEncodingIds2[id2++] = ranEncodingIds[id];
				}*/
				for (int t= 0; t<threadNumber;t++){
					Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
					int [][][] encodingPart= objEncodingParts.get(t).toArray(new int[][][]{});
					TFIDFEncodedThread thread = new TFIDFEncodedThread(partObjIds,objRangeIds , encodingPart, ranEncodingIds, this, idfReaderDomain,
							idfReaderRange, domain, range, multiAttributeIndex);
					threadList.add(thread);
				}
			} else {
				ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
				ArrayList<List<int[][]>> objEncodingParts = new ArrayList<List<int[][]>>();
				/*
				 * init lists of obj Id parts and value parts
				 */
				for (int i = 0; i<threadNumber;i++){
					objIdParts.add(new ArrayList<Integer>());
					objEncodingParts.add(new ArrayList<int[][]>());
				}
				/*
				 * distribute data via round robin 
				 */
				
				for (int id =0; id<objRangeIds.length;id++){
					objIdParts.get(id%threadNumber).add(objRangeIds[id]);
					objEncodingParts.get(id%threadNumber).add(ranEncodingIds[id]);
				}
				//long startTime = System.currentTimeMillis();
				/*
				int ranID=0;
				for (Obj o: range){
					int id = posRangeMap.get(o.getID());
					objIdParts.get(ranID%threadNumber).add(objRangeIds[id]);
					objEncodingParts.get(ranID++%threadNumber).add(ranEncodingIds[id]);
				}
				
				
				Integer[] objDomainIds2 = new Integer[domain.size()];
				int domEncodingIds2 [][][] = new int [domain.size()][][];
				int id2 =0;
				for (Obj o: domain){
					int id = posDomainMap.get(o.getID());
					objDomainIds2[id2] = objDomainIds[id];
					domEncodingIds2[id2++] = domEncodingIds[id];
				}*/
				//long endTime = System.currentTimeMillis();
				//System.out.println("build matcher parts: "+(endTime-startTime));
				/*
				 * create threads with their parts
				 */
				for (int t= 0; t<threadNumber;t++){
					Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
					int [][][] encodingPart= objEncodingParts.get(t).toArray(new int[][][]{});
					TFIDFEncodedThread thread = new TFIDFEncodedThread(objDomainIds,partObjIds , domEncodingIds, encodingPart, this, idfReaderDomain,
							idfReaderRange, domain, range, multiAttributeIndex);
					threadList.add(thread);
				}
			}
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			for (TFIDFEncodedThread thread:threadList){
				thread.start();
			}
			
			for (TFIDFEncodedThread thread:threadList){
				try {
					thread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			for (TFIDFEncodedThread thread : threadList) {
				try {
					result = SetOperators.union(result, thread.getResult());
				} catch (WrongSourceException e) {
					e.printStackTrace();
				}
			}
			//this.idfReaderDomain.clear();
			//this.idfReaderRange.clear();
			//this.wordInDocMapDom.clear();
			//this.wordInDocMapRange.clear();
			//this.dict.clear();
			boolean isInitDomain = Boolean.parseBoolean(matchProps.getInitDomain());
			boolean isInitRange = Boolean.parseBoolean(matchProps.getInitRange());
			if (isInitDomain){
				EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_ID_PREF);
				EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_ATT_PREF);
				EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.IDF_COUNTS);
				EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_TRIGRAMS);
				EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.POS_MAP);
				
			}
			
			if (isInitRange){
				EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_ID_PREF);
				EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_ATT_PREF);
				EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.IDF_COUNTS);
				EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_TRIGRAMS);
				EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.POS_MAP);
			}
			return result;
		}

		protected ObjCorrespondenceSet createMappingUsingInitialMapping(
				ObjSet domain, ObjSet range) throws OperatorException { //Muss noch implementiert werden!
			return initialMap;
		}
		/**
		 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
		 
		protected ObjCorrespondenceSet createMappingUsingInitialMapping(
				ObjSet domain, ObjSet range) throws OperatorException {
			List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

			try {
				ObjCorrespondenceSet result = new ObjCorrespondenceSet();

				if (super.initialMap.size() < domain.size() * range.size()) {
					Obj domainObj, rangeObj;

					for (ObjCorrespondence initCorr : super.initialMap
							.getCollection()) {
						if (!(domain.contains(initCorr.getDomainObjID()) && range
								.contains(initCorr.getRangeObjID())))
							continue;

						domainObj = domain.getObj(initCorr.getDomainObjID());
						rangeObj = range.getObj(initCorr.getRangeObjID());

						domainAttrValueList = this.createStringPortion(super
								.getAttributeValues(domainObj, true));

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj, this
								.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				} else {
					for (Obj domainObj : domain) {
						domainAttrValueList = this.createStringPortion(super
								.getAttributeValues(domainObj, true));

						for (Obj rangeObj : range) {
							if (!super.initialMap.containsCorrespondence(domainObj
									.getID(), rangeObj.getID()))
								continue;

							rangeAttrValueList = this.createStringPortion(super
									.getAttributeValues(rangeObj, false));

							result = this.addCorrespondence(domainObj, rangeObj,
									this.computeMatch(domainAttrValueList,
											rangeAttrValueList), result, true);
						}
					}
				}
				return result;
			} catch (WrongSourceException e) {
				throw new OperatorException(e.getMessage());
			}
		}*/
			
		
		
		private ConcurrentHashMap<Integer, Float> getGlobalTokenMap(ObjSet domain, ObjSet range, boolean idfForSeparateSources, boolean isDomain){
			ConcurrentHashMap<Integer, Float> globalTokenMap = new ConcurrentHashMap<Integer, Float>();
			int sourceSize;
			
			
			if(!idfForSeparateSources){//idf for domain and range
				
				sourceSize = domain.size()+range.size();
				objDomainIds = new Integer[domain.size()];
				this.domEncodingIds = new int [domain.size()][][];
				int domCount=0;
				for (Obj domainObj : domain) {
					objDomainIds[domCount] = domainObj.getID();
					List<String> attValues = super.getAttributeValues(domainObj, true);
					globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,domCount,true);
					domCount++;
				}
				int rangeCount = 0;
				this.objRangeIds = new Integer[range.size()];
				this.ranEncodingIds = new int [range.size()][][];
				for (Obj rangeObj : range) {
					List<String> attValues = super.getAttributeValues(rangeObj, false);
					objRangeIds[rangeCount] = rangeObj.getID();
					globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,rangeCount,false);
					rangeCount++;
				}
				
			}else{
				if(isDomain){//idf for domain
					int domCount=0;
					sourceSize = domain.size();
					for (Obj domainObj : domain) {
						objDomainIds[domCount] = domainObj.getID();
					
						List<String> attValues = super.getAttributeValues(domainObj, true);
						globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,domCount,true);
						domCount++;
					}
				}else{//idf for range
					int rangeCount=0;
					sourceSize = range.size();
					for (Obj rangeObj : range) {
						objRangeIds[rangeCount] = rangeObj.getID();
						List<String> attValues = super.getAttributeValues(rangeObj, false);
						globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,rangeCount,false);
						rangeCount++;
					}
				}
			}
			HashSet<Integer> allTokens = new HashSet<Integer>();
			allTokens.addAll(this.wordInDocMapDom.keySet());
			allTokens.addAll(this.wordInDocMapRange.keySet());
			
			for(Integer token : allTokens){
				Float wordFrequ=0F;
				if (!idfForSeparateSources){
					wordFrequ = (float) (((this.wordInDocMapDom.get(token)!=null)?this.wordInDocMapDom.get(token):0)+
							((this.wordInDocMapRange.get(token)!=null)?this.wordInDocMapRange.get(token):0));
				}else {
					if (isDomain){
						wordFrequ = (float) (((this.wordInDocMapDom.get(token)!=null)?this.wordInDocMapDom.get(token):0));
					}else{
						wordFrequ = (float) (((this.wordInDocMapRange.get(token)!=null)?this.wordInDocMapRange.get(token):0));
					}
				}
				//Float wordFrequ = globalTokenMap.get(token);
				if (sourceSize<wordFrequ)
					System.out.println(token+","+wordFrequ+","+sourceSize+" --> "+Math.log((float)(sourceSize)/(wordFrequ)));
				//float idf= (float)(Math.log((float)(sourceSize)/(wordFrequ+1.0)) + 1.0F);
				float idf= (float)((Math.log((float)((float)sourceSize)/(wordFrequ)))/(float)Math.log(2));
				globalTokenMap.put(token, idf);
				//System.out.println(token+"\t"+wordFrequ+"\t"+idf);
			}
			return globalTokenMap;
		}
		
		private ConcurrentHashMap<Integer, Float>  generateTFIDFMap (int size,Int2IntMap idfDomainMap, Int2IntMap idfRangeMap,
				boolean isSeparated, boolean isDomain){
			ConcurrentHashMap<Integer, Float> tokenIdfMap = new ConcurrentHashMap<Integer, Float>();
			HashSet<Integer> allWord = new HashSet<Integer>();
			
			if (isSeparated){
				if (isDomain){
					allWord.addAll(idfDomainMap.keySet());
				}else {
					allWord.addAll(idfRangeMap.keySet());
				}
				for(Integer token : allWord){
					Float wordFrequ=0F;
					if (isDomain){
						wordFrequ = (float) (((idfDomainMap.get(token)!=null)?idfDomainMap.get(token):0));
					}else{
						wordFrequ = (float) (((idfRangeMap.get(token)!=null)?idfRangeMap.get(token):0));
					}
					float idf= (float)((Math.log((float)((float)size)/(wordFrequ)))/(float)Math.log(2));
					tokenIdfMap.put(token, idf);
				}
			}else {
				allWord.addAll(idfDomainMap.keySet());
				allWord.addAll(idfRangeMap.keySet());
				for(Integer token : allWord){
					Float wordFrequ=0F;
					wordFrequ = (float) (((idfDomainMap.get(token)!=null)?idfDomainMap.get(token):0)+
							((idfRangeMap.get(token)!=null)?idfRangeMap.get(token):0));
					float idf= (float)((Math.log((float)((float)size)/(wordFrequ)))/(float)Math.log(2));
					tokenIdfMap.put(token, idf);
				}
			}
			return tokenIdfMap;
		}


		
		private ConcurrentHashMap<Integer, Float> extendGlobalTokenMapWithIds(List<String> attValues,ConcurrentHashMap<Integer,Float> globalTokenMap,
				int objIndex, boolean isDomain) {
			int valueIndex = 0;
			if (isDomain)
				this.domEncodingIds[objIndex] = new int[attValues.size()][];
			else
				this.ranEncodingIds[objIndex] = new int[attValues.size()][];
			HashSet <Integer> idsPerConcept = new HashSet<Integer>();
			for(String s:attValues){
				List<Integer> tokenIds = new ArrayList<Integer>();
				Map<String,Integer> wordFreqs= StringDecomposer.decomposeWordFrequenciesLucene(s);
				String [] tokens = s.split("[^A-Za-z0-9]");
				for(String word:tokens){
					Integer id = dict.check(word);
					//for (int i = 0;i<wordFreqs.get(word);i++){
					tokenIds.add(id);
					idsPerConcept.add(id);
					//}
					/*
					if(globalTokenMap.containsKey(id)){
						float newFreq = globalTokenMap.get(id)+(float)wordFreqs.get(word);
						globalTokenMap.put(id, newFreq);
					}else{
						globalTokenMap.put(id, (float)wordFreqs.get(word));
					}*/
				}
				
				//Collections.sort(tokenIds);
				if (isDomain){
					this.domEncodingIds[objIndex][valueIndex] = new int[tokenIds.size()];
					for (int i=0;i<tokenIds.size();i++){
						this.domEncodingIds[objIndex][valueIndex][i]=tokenIds.get(i);
					}
					
				}
				else {
					this.ranEncodingIds[objIndex][valueIndex] = new int[tokenIds.size()];
					for (int i=0;i<tokenIds.size();i++){
						this.ranEncodingIds[objIndex][valueIndex][i]=tokenIds.get(i);
					}
				}
				valueIndex++;
			}
			
			for (int id: idsPerConcept){
				if (isDomain){
					Integer presentCount = this.wordInDocMapDom.get(id);
					if(presentCount ==null)
						wordInDocMapDom.put(id, 1);
					else
						wordInDocMapDom.put(id, (int)presentCount+1);
				}else{
					Integer presentCount = this.wordInDocMapRange.get(id);
					if(presentCount ==null)
						wordInDocMapRange.put(id, 1);
					else
						wordInDocMapRange.put(id, (int)presentCount+1);
				}
			}
			
			return globalTokenMap;
		}
}

