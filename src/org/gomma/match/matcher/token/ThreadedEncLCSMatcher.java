package org.gomma.match.matcher.token;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.string.StringDecomposer;

public class ThreadedEncLCSMatcher extends AbstractMatcher{
	//private static final String IDF_FOR_SINGLE_SOURCES = "[idfForSingleSources]";
			
			private Integer[] objDomainIds;
			private Integer[] objRangeIds;
			
			/**
			 * first field is obj index ,second attribute, third token ids;
			 */
			private int[][][] domEncodingAtts;
			private int[][][] ranEncodingAtts;
			private WordIntDictionary dict;
			/**
			 * The constructor initializes the class.
			 */
			public ThreadedEncLCSMatcher() {
				
			}
			
			
			
			
			
			

			@Override
			/**
			 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
			 */
			protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
					SourceVersionStructure s2) throws OperatorException {
				return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
			}

			@Override
			/**
			 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
			 */
			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					SourceVersionStructure s1, SourceVersionStructure s2)
					throws OperatorException {
				return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
			}

			/**
			 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
			 */
			protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
					throws OperatorException {
				dict = WordIntDictionary.getInstance();
				String domainName = "domain";
				String rangeName = "range";
				
				EncodingManager encodingManger =  EncodingManager.getInstance();
				objDomainIds = (Integer[]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ID_PREF);
				if (objDomainIds ==null){
					encodingManger.encodeObjWithAttValues(domain, domainName, this.getMatchProperties().getDomainAttributeNameList(), true);
				}
				objDomainIds = (Integer[]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ID_PREF);
				domEncodingAtts = (int[][][]) encodingManger.getEncodedObject(domainName, EncodingManager.OBJ_ATT_PREF);
				Int2IntMap posDomainMap = (Int2IntMap) encodingManger.getEncodedObject(domainName, EncodingManager.POS_MAP);
				
				
				objRangeIds = (Integer[]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ID_PREF);
				if (objRangeIds ==null){
					encodingManger.encodeObjWithAttValues(range, rangeName, this.getMatchProperties().getRangeAttributeNameList(), true);
				}
				objRangeIds =(Integer[]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ID_PREF);
				ranEncodingAtts = (int[][][]) encodingManger.getEncodedObject(rangeName, EncodingManager.OBJ_ATT_PREF);
				Int2IntMap posRangeMap = (Int2IntMap) encodingManger.getEncodedObject(rangeName, EncodingManager.POS_MAP);
				
				/*
					objDomainIds = new Integer[domain.size()];
					domEncodingAtts = new int[domain.size()][][];
					objRangeIds = new Integer[range.size()];
					ranEncodingAtts = new int[range.size()][][];
					this.getGlobalTokenMap(domain, range, true);
					this.getGlobalTokenMap(domain, range, false);
				*/	
				//idfs yet calculated
				//this.dict.clear();
				int numberOfProcessros = Runtime.getRuntime().availableProcessors();
				int threadNumber;
				if (domain.size()<1000&&range.size()<1000) {
					threadNumber = 4;
				} else {
					threadNumber = Math.max(numberOfProcessros, 16);
				}
				
				//Gr��ere Ontologie f�r Threading aufspalten (Modulo-Ansatz!!) Ziel: Menge von k Match-Threads
				List<LCSThread> threadList = new ArrayList<LCSThread>();
				if (domain.size()>range.size()) {
					//Domain splitten
					ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
					ArrayList<List<int[][]>> objEncodingParts = new ArrayList<List<int[][]>>();
					for (int i = 0; i<threadNumber;i++){
						objIdParts.add(new ArrayList<Integer>());
						objEncodingParts.add(new ArrayList<int[][]>());
					}
					
					/*
					for (int id =0; id<objDomainIds.length;id++){
						objIdParts.get(id%threadNumber).add(objDomainIds[id]);
						objEncodingParts.get(id%threadNumber).add(domEncodingAtts[id]);
					}*/
					int domID=0;
					for (Obj o: domain){
						int id = posDomainMap.get(o.getID());
						objIdParts.get(domID%threadNumber).add(objDomainIds[id]);
						objEncodingParts.get(domID++%threadNumber).add(domEncodingAtts[id]);
					}
					Integer[] objRangeIds2 = new Integer[range.size()];
					int ranEncodingIds2 [][][] = new int [range.size()][][];
					int id2=0;
					for (Obj o: range){
						int id = posRangeMap.get(o.getID());
						objRangeIds2[id2] = objRangeIds[id];
						ranEncodingIds2[id2++] = ranEncodingAtts[id];
					}
					
					for (int t= 0; t<threadNumber;t++){
						Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
						int[][][] encodingPart= objEncodingParts.get(t).toArray(new int[][][]{});
						LCSThread thread = new LCSThread(partObjIds,objRangeIds2 , encodingPart, ranEncodingIds2,
								this, domain, range);
						threadList.add(thread);
					}
				} else {
					ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
					ArrayList<List<int[][]>> objEncodingParts = new ArrayList<List<int[][]>>();
					/*
					 * init lists of obj Id parts and value parts
					 */
					for (int i = 0; i<threadNumber;i++){
						objIdParts.add(new ArrayList<Integer>());
						objEncodingParts.add(new ArrayList<int[][]>());
					}
					/*
					 * distribute data via round robin 
					 */
					/*
					for (int id =0; id<objRangeIds.length;id++){
						objIdParts.get(id%threadNumber).add(objRangeIds[id]);
						objEncodingParts.get(id%threadNumber).add(ranEncodingAtts[id]);
					}*/
					int ranID=0;
					for (Obj o: range){
						int id = posRangeMap.get(o.getID());
						objIdParts.get(ranID%threadNumber).add(objRangeIds[id]);
						objEncodingParts.get(ranID++%threadNumber).add(ranEncodingAtts[id]);
					}
					
					Integer[] objDomainIds2 = new Integer[domain.size()];
					int domEncodingIds2 [][][] = new int [domain.size()][][];
					int id2 =0;
					for (Obj o: domain){
						int id = posDomainMap.get(o.getID());
						objDomainIds2[id2] = objDomainIds[id];
						domEncodingIds2[id2++] = domEncodingAtts[id];
					}
					/*
					 * create threads with their parts
					 */
					for (int t= 0; t<threadNumber;t++){
						Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
						int [][][] encodingPart= objEncodingParts.get(t).toArray(new int[][][]{});
						LCSThread thread = new LCSThread(objDomainIds2,partObjIds , domEncodingIds2, encodingPart, this,
								domain, range);
						threadList.add(thread);
					}
				}
				ObjCorrespondenceSet result = new ObjCorrespondenceSet();
				for (LCSThread thread:threadList){
					thread.start();
				}
				
				for (LCSThread thread:threadList){
					try {
						thread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				for (LCSThread thread : threadList) {
					try {
						result = SetOperators.union(result, thread.getResult());
					} catch (WrongSourceException e) {
						e.printStackTrace();
					}
				}
				//dict.clear();
				//idfReaderDomain.clear();
				//idfReaderRange.clear();
				boolean isInitDomain = Boolean.parseBoolean(matchProps.getInitDomain());
				boolean isInitRange = Boolean.parseBoolean(matchProps.getInitRange());
				if (isInitDomain){
					EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_ID_PREF);
					EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_ATT_PREF);
					EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.IDF_COUNTS);
					EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.OBJ_TRIGRAMS);
					EncodingManager.getInstance().deleteEncodings(domainName, EncodingManager.POS_MAP);
				}
				
				if (isInitRange){
					EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_ID_PREF);
					EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_ATT_PREF);
					EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.IDF_COUNTS);
					EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.OBJ_TRIGRAMS);
					EncodingManager.getInstance().deleteEncodings(rangeName, EncodingManager.POS_MAP);
				}
				return result;
			}

			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					ObjSet domain, ObjSet range) throws OperatorException { //Muss noch implementiert werden!
				return initialMap;
			}
			/**
			 * @param b 
			 * @return 
			 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
			 
			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					ObjSet domain, ObjSet range) throws OperatorException {
				List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

				try {
					ObjCorrespondenceSet result = new ObjCorrespondenceSet();

					if (super.initialMap.size() < domain.size() * range.size()) {
						Obj domainObj, rangeObj;

						for (ObjCorrespondence initCorr : super.initialMap
								.getCollection()) {
							if (!(domain.contains(initCorr.getDomainObjID()) && range
									.contains(initCorr.getRangeObjID())))
								continue;

							domainObj = domain.getObj(initCorr.getDomainObjID());
							rangeObj = range.getObj(initCorr.getRangeObjID());

							domainAttrValueList = this.createStringPortion(super
									.getAttributeValues(domainObj, true));

							rangeAttrValueList = this.createStringPortion(super
									.getAttributeValues(rangeObj, false));

							result = this.addCorrespondence(domainObj, rangeObj, this
									.computeMatch(domainAttrValueList,
											rangeAttrValueList), result, true);
						}
					} else {
						for (Obj domainObj : domain) {
							domainAttrValueList = this.createStringPortion(super
									.getAttributeValues(domainObj, true));

							for (Obj rangeObj : range) {
								if (!super.initialMap.containsCorrespondence(domainObj
										.getID(), rangeObj.getID()))
									continue;

								rangeAttrValueList = this.createStringPortion(super
										.getAttributeValues(rangeObj, false));

								result = this.addCorrespondence(domainObj, rangeObj,
										this.computeMatch(domainAttrValueList,
												rangeAttrValueList), result, true);
							}
						}
					}
					return result;
				} catch (WrongSourceException e) {
					throw new OperatorException(e.getMessage());
				}
			}*/
				
			
			
			
			
			public void getGlobalTokenMap(ObjSet domain, ObjSet range,  boolean isDomain){
				
				if(isDomain){//idf for domain
					int domCount=0;
					
					for (Obj domainObj : domain) {
						objDomainIds[domCount] = domainObj.getID();
					
						List<String> attValues = super.getAttributeValues(domainObj, true);
						extendGlobalTokenMapWithIds(attValues,domCount,true);
						domCount++;
					}
				}else{//idf for range
					int rangeCount=0;
					
					for (Obj rangeObj : range) {
						objRangeIds[rangeCount] = rangeObj.getID();
						List<String> attValues = super.getAttributeValues(rangeObj, false);
						extendGlobalTokenMapWithIds(attValues,rangeCount,false);
						rangeCount++;
					}
				}
			}

			
			
			private void extendGlobalTokenMapWithIds(List<String> attValues,
					int objIndex, boolean isDomain) {
				int valueIndex = 0;
				if (isDomain)
					this.domEncodingAtts[objIndex] = new int[attValues.size()][];
				else
					this.ranEncodingAtts[objIndex] = new int[attValues.size()][];
				for(String s:attValues){
					List<Integer> tokenIds = new ArrayList<Integer>();
					String [] a = s.split("[^A-Za-z0-9]"); 
					for(String word:a){
						Integer id = dict.check(word);
						//for (int i = 0;i<wordFreqs.get(word);i++){
						tokenIds.add(id);
						//}
					}
					if (isDomain){
						this.domEncodingAtts[objIndex][valueIndex] = new int[tokenIds.size()];
						for (int i=0;i<tokenIds.size();i++){
							this.domEncodingAtts[objIndex][valueIndex][i]=tokenIds.get(i);
						}
						
					}
					else{
						this.ranEncodingAtts[objIndex][valueIndex] = new int[tokenIds.size()];
						for (int i=0;i<tokenIds.size();i++){
							this.ranEncodingAtts[objIndex][valueIndex][i]=tokenIds.get(i);
						}
						
					}
						valueIndex++;
				}
			}
}
