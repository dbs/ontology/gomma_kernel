package org.gomma.match.matcher.token;


import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentIDFIndexReader {
	ConcurrentHashMap<String,Float> globalTokenMap;
	
	

	public ConcurrentIDFIndexReader(ConcurrentHashMap<String,Float> globalTokenMap){
		this.globalTokenMap = globalTokenMap;
	}
	
	
	public float getIDF(int attributeIndex, String token) { 
		return globalTokenMap.get(token);
		//nutze attribute index im Moment nicht, weil einfache TokenMap fuer nur ein Attribut
	}
}
