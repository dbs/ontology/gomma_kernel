package org.gomma.match.matcher.token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.string.StringDecomposer;

public class ThreadedSoftTfidfEncMatcher extends AbstractMatcher{
	//private static final String IDF_FOR_SINGLE_SOURCES = "[idfForSingleSources]";
			private int multiAttributeIndex;
			private ConcurrentIDFEncIndexReader idfReaderDomain;
			private ConcurrentIDFEncIndexReader idfReaderRange;
			private ConcurrentInvertedTrigramReader invertTrigramTokenReader;
			private Integer[] objDomainIds;
			private Integer[] objRangeIds;
			
			/**
			 * first field is obj index ,second attribute, third token ids;
			 */
			private Integer[][][] domEncodingIds;
			private Integer[][][] ranEncodingIds;
			private WordIntDictionary dict;
			private TrigramIntDictionary trigramDict;
			HashMap<Integer,Integer> wordInDocMapDom;
			HashMap<Integer,Integer> wordInDocMapRange;
			/**
			 * The constructor initializes the class.
			 */
			public ThreadedSoftTfidfEncMatcher() {
				
			}
			
			public ThreadedSoftTfidfEncMatcher(int multiAttributeIndex, ConcurrentIDFEncIndexReader idfReaderDomain, ConcurrentIDFEncIndexReader idfReaderRange)
			{
				setAttribute(multiAttributeIndex);
				setidfReader(idfReaderDomain,idfReaderRange);
			}
			
			public void setAttribute(int multiAttributeIndex)
			{
				this.multiAttributeIndex= multiAttributeIndex;
			}
			
			public void setidfReader(ConcurrentIDFEncIndexReader idfReaderDomain,ConcurrentIDFEncIndexReader idfReaderRange)
			{
				this.idfReaderDomain= idfReaderDomain;
				this.idfReaderRange= idfReaderRange;
			}

			@Override
			/**
			 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
			 */
			protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
					SourceVersionStructure s2) throws OperatorException {
				return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
			}

			@Override
			/**
			 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
			 */
			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					SourceVersionStructure s1, SourceVersionStructure s2)
					throws OperatorException {
				return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
			}

			/**
			 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
			 */
			protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
					throws OperatorException {
				dict = WordIntDictionary.getInstance();
				invertTrigramTokenReader = new ConcurrentInvertedTrigramReader();
				trigramDict = TrigramIntDictionary.getInstance();
				objDomainIds = new Integer[domain.size()];
				objRangeIds = new Integer[range.size()];
				
				domEncodingIds = new Integer[domain.size()][][];
				ranEncodingIds = new Integer[range.size()][][];
				wordInDocMapDom = new HashMap<Integer,Integer>();
				wordInDocMapRange = new HashMap<Integer,Integer>();
				String separateSourceIdfString	= this.getMatchProperties().getSeparateSourceIdf();	
				boolean idfForSeparateSources		= Boolean.valueOf(separateSourceIdfString);
				//System.out.println("separateSourceIdfString: "+separateSourceIdfString);
				ConcurrentHashMap<Integer, Float> globalTokenMapDomain, globalTokenMapRange;
				if(idfForSeparateSources){//wenn idfForSeparateSources==true: domain und range separater idf
					
					globalTokenMapDomain	= getGlobalTokenMap(domain, range, idfForSeparateSources, true);
					globalTokenMapRange		= getGlobalTokenMap(domain, range, idfForSeparateSources, false);
					
				}else{//falls ==false, dann wird eine TokenMap f�r beide berechnet, welche die gleiche ist 
					
					ConcurrentHashMap<Integer, Float> globalTokenMap = getGlobalTokenMap(domain, range, idfForSeparateSources, true);
					globalTokenMapDomain	= globalTokenMap;
					globalTokenMapRange		= globalTokenMap;
				}
				
				ConcurrentIDFEncIndexReader idfReaderDomain	= new ConcurrentIDFEncIndexReader(globalTokenMapDomain);
				ConcurrentIDFEncIndexReader idfReaderRange	= new ConcurrentIDFEncIndexReader(globalTokenMapRange);
				
				this.wordInDocMapDom.clear();
				this.wordInDocMapRange.clear();
				this.trigramDict.clear();
				this.dict.clear();
				System.gc();
				this.setidfReader(idfReaderDomain,idfReaderRange);
				int numberOfProcessros = Runtime.getRuntime().availableProcessors();
				int threadNumber;
				if (domain.size()<1000&&range.size()<1000) {
					threadNumber = 4;
				} else {
					threadNumber = Math.max(numberOfProcessros, 16);
				}
				
				//Gr��ere Ontologie f�r Threading aufspalten (Modulo-Ansatz!!) Ziel: Menge von k Match-Threads
				List<SoftTfidfThread> threadList = new ArrayList<SoftTfidfThread>();
				
				if (domain.size()>range.size()) {
					//Domain splitten
					ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
					ArrayList<List<Integer[][]>> objEncodingParts = new ArrayList<List<Integer[][]>>();
					for (int i = 0; i<threadNumber;i++){
						objIdParts.add(new ArrayList<Integer>());
						objEncodingParts.add(new ArrayList<Integer[][]>());
					}
					
					
					for (int id =0; id<objDomainIds.length;id++){
						objIdParts.get(id%threadNumber).add(objDomainIds[id]);
						objEncodingParts.get(id%threadNumber).add(domEncodingIds[id]);
					}
					
					for (int t= 0; t<threadNumber;t++){
						Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
						Integer [][][] encodingPart= objEncodingParts.get(t).toArray(new Integer[][][]{});
						SoftTfidfThread thread = new SoftTfidfThread(partObjIds,objRangeIds , encodingPart, ranEncodingIds, this, idfReaderDomain,
								idfReaderRange,this.invertTrigramTokenReader, domain, range, multiAttributeIndex);
						threadList.add(thread);
					}
				} else {
					ArrayList<List<Integer>> objIdParts = new ArrayList<List<Integer>>();
					ArrayList<List<Integer[][]>> objEncodingParts = new ArrayList<List<Integer[][]>>();
					/*
					 * init lists of obj Id parts and value parts
					 */
					for (int i = 0; i<threadNumber;i++){
						objIdParts.add(new ArrayList<Integer>());
						objEncodingParts.add(new ArrayList<Integer[][]>());
					}
					/*
					 * distribute data via round robin 
					 */
					for (int id =0; id<objRangeIds.length;id++){
						objIdParts.get(id%threadNumber).add(objRangeIds[id]);
						objEncodingParts.get(id%threadNumber).add(ranEncodingIds[id]);
					}
					/*
					 * create threads with their parts
					 */
					for (int t= 0; t<threadNumber;t++){
						Integer [] partObjIds = objIdParts.get(t).toArray(new Integer[]{});
						Integer [][][] encodingPart= objEncodingParts.get(t).toArray(new Integer[][][]{});
						SoftTfidfThread thread = new SoftTfidfThread(objDomainIds,partObjIds , domEncodingIds, encodingPart, this, idfReaderDomain,
								idfReaderRange, invertTrigramTokenReader, domain, range, multiAttributeIndex);
						threadList.add(thread);
					}
				}
				ObjCorrespondenceSet result = new ObjCorrespondenceSet();
				for (SoftTfidfThread thread:threadList){
					thread.start();
				}
				
				for (SoftTfidfThread thread:threadList){
					try {
						thread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				for (SoftTfidfThread thread : threadList) {
					try {
						result = SetOperators.union(result, thread.getResult());
					} catch (WrongSourceException e) {
						e.printStackTrace();
					}
				}
				this.idfReaderDomain.clear();
				this.idfReaderRange.clear();
				this.invertTrigramTokenReader.clear();
				System.gc();
				//this.dict.clear();
				return result;
			}

			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					ObjSet domain, ObjSet range) throws OperatorException { //Muss noch implementiert werden!
				return initialMap;
			}
			/**
			 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
			 
			protected ObjCorrespondenceSet createMappingUsingInitialMapping(
					ObjSet domain, ObjSet range) throws OperatorException {
				List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

				try {
					ObjCorrespondenceSet result = new ObjCorrespondenceSet();

					if (super.initialMap.size() < domain.size() * range.size()) {
						Obj domainObj, rangeObj;

						for (ObjCorrespondence initCorr : super.initialMap
								.getCollection()) {
							if (!(domain.contains(initCorr.getDomainObjID()) && range
									.contains(initCorr.getRangeObjID())))
								continue;

							domainObj = domain.getObj(initCorr.getDomainObjID());
							rangeObj = range.getObj(initCorr.getRangeObjID());

							domainAttrValueList = this.createStringPortion(super
									.getAttributeValues(domainObj, true));

							rangeAttrValueList = this.createStringPortion(super
									.getAttributeValues(rangeObj, false));

							result = this.addCorrespondence(domainObj, rangeObj, this
									.computeMatch(domainAttrValueList,
											rangeAttrValueList), result, true);
						}
					} else {
						for (Obj domainObj : domain) {
							domainAttrValueList = this.createStringPortion(super
									.getAttributeValues(domainObj, true));

							for (Obj rangeObj : range) {
								if (!super.initialMap.containsCorrespondence(domainObj
										.getID(), rangeObj.getID()))
									continue;

								rangeAttrValueList = this.createStringPortion(super
										.getAttributeValues(rangeObj, false));

								result = this.addCorrespondence(domainObj, rangeObj,
										this.computeMatch(domainAttrValueList,
												rangeAttrValueList), result, true);
							}
						}
					}
					return result;
				} catch (WrongSourceException e) {
					throw new OperatorException(e.getMessage());
				}
			}*/
				
			
			
			public ConcurrentHashMap<Integer, Float> getGlobalTokenMap(ObjSet domain, ObjSet range, boolean idfForSeparateSources, boolean isDomain){
				ConcurrentHashMap<Integer, Float> globalTokenMap = new ConcurrentHashMap<Integer, Float>();
				int sourceSize;
				
				
				if(!idfForSeparateSources){//idf for domain and range
					
					sourceSize = domain.size()+range.size();
					
					int domCount=0;
					for (Obj domainObj : domain) {
						objDomainIds[domCount] = domainObj.getID();
						List<String> attValues = super.getAttributeValues(domainObj, true);
						globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,domCount,true);
						domCount++;
					}
					int rangeCount = 0;
					for (Obj rangeObj : range) {
						List<String> attValues = super.getAttributeValues(rangeObj, false);
						objRangeIds[rangeCount] = rangeObj.getID();
						globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,rangeCount,false);
						rangeCount++;
					}
					
				}else{
					if(isDomain){//idf for domain
						int domCount=0;
						sourceSize = domain.size();
						for (Obj domainObj : domain) {
							objDomainIds[domCount] = domainObj.getID();
						
							List<String> attValues = super.getAttributeValues(domainObj, true);
							globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,domCount,true);
							domCount++;
						}
					}else{//idf for range
						int rangeCount=0;
						sourceSize = range.size();
						for (Obj rangeObj : range) {
							objRangeIds[rangeCount] = rangeObj.getID();
							List<String> attValues = super.getAttributeValues(rangeObj, false);
							globalTokenMap = extendGlobalTokenMapWithIds(attValues,globalTokenMap,rangeCount,false);
							rangeCount++;
						}
					}
				}
				
				for(Integer token : globalTokenMap.keySet()){
					Float wordFrequ=0F;
					if (!idfForSeparateSources){
						wordFrequ = (float) (((this.wordInDocMapDom.get(token)!=null)?this.wordInDocMapDom.get(token):0)+
								((this.wordInDocMapRange.get(token)!=null)?this.wordInDocMapRange.get(token):0));
					}else {
						if (isDomain){
							wordFrequ = (float) (((this.wordInDocMapDom.get(token)!=null)?this.wordInDocMapDom.get(token):0));
						}else{
							wordFrequ = (float) (((this.wordInDocMapRange.get(token)!=null)?this.wordInDocMapRange.get(token):0));
						}
					}
					//Float wordFrequ = globalTokenMap.get(token);
					if (sourceSize<wordFrequ)
						System.out.println(token+","+wordFrequ+","+sourceSize+" --> "+Math.log((float)(sourceSize)/(wordFrequ)));
					//float idf= (float)(Math.log((float)(sourceSize)/(wordFrequ+1.0)) + 1.0F);
					float idf= (float)((Math.log((float)((float)sourceSize)/(wordFrequ)))/(float)Math.log(2));
					globalTokenMap.put(token, idf);
					//System.out.println(token+"\t"+wordFrequ+"\t"+idf);
				}
				return globalTokenMap;
			}


			
			private ConcurrentHashMap<Integer, Float> extendGlobalTokenMapWithIds(List<String> attValues,ConcurrentHashMap<Integer,Float> globalTokenMap,
					int objIndex, boolean isDomain) {
				int valueIndex = 0;
				if (isDomain)
					this.domEncodingIds[objIndex] = new Integer[attValues.size()][];
				else
					this.ranEncodingIds[objIndex] = new Integer[attValues.size()][];
				HashSet <Integer> idsPerConcept = new HashSet<Integer>();
				for(String s:attValues){
					List<Integer> tokenIds = new ArrayList<Integer>();
					Map<String,Integer> wordFreqs= StringDecomposer.decomposeWordFrequenciesLucene(s);
					for(String word:wordFreqs.keySet()){
						Integer id = dict.check(word);
						//for (int i = 0;i<wordFreqs.get(word);i++){
						HashSet<Integer> trigramIds = this.generateTrigram(word, 2);
						this.invertTrigramTokenReader.addTrigrams(trigramIds, id);
						tokenIds.add(id);
						idsPerConcept.add(id);
						//}
						if(globalTokenMap.containsKey(id)){
							float newFreq = globalTokenMap.get(id)+(float)wordFreqs.get(word);
							globalTokenMap.put(id, newFreq);
						}else{
							globalTokenMap.put(id, (float)wordFreqs.get(word));
						}
					}
					
					Collections.sort(tokenIds);
					if (isDomain){
						this.domEncodingIds[objIndex][valueIndex] = tokenIds.toArray(new Integer[]{});
						
					}
					else 
						this.ranEncodingIds[objIndex][valueIndex] = tokenIds.toArray(new Integer[]{});
					valueIndex++;
				}
				
				for (Integer id: idsPerConcept){
					if (isDomain){
						Integer presentCount = this.wordInDocMapDom.get(id);
						if(presentCount ==null)
							wordInDocMapDom.put(id, 1);
						else
							wordInDocMapDom.put(id, presentCount+1);
					}else{
						Integer presentCount = this.wordInDocMapRange.get(id);
						if(presentCount ==null)
							wordInDocMapRange.put(id, 1);
						else
							wordInDocMapRange.put(id, presentCount+1);
					}
				}
				
				return globalTokenMap;
			}
			
			private HashSet<Integer> generateTrigram (String token, int ngram){
				HashSet<Integer> trigramIds = new HashSet<Integer>();
				for (int i=0;i<=token.length()-ngram;i++){
					String trigram = token.substring(i,i+ngram);
					int tid = this.trigramDict.check(trigram);
					trigramIds.add(tid);
				}
				return trigramIds;
			}
}
