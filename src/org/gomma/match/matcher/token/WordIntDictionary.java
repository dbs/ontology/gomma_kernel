package org.gomma.match.matcher.token;

import java.util.concurrent.ConcurrentHashMap;

public class WordIntDictionary {

	
	private ConcurrentHashMap<String,Integer> dictionary;
	
	private static WordIntDictionary instance;
	
	static int id =0;
	public WordIntDictionary (){
		id = 0;
		dictionary = new ConcurrentHashMap<String,Integer>();
		
	}
	
	
	public Integer check(String s){
		Integer sid = this.dictionary.get(s);
		if (sid ==null){
			this.dictionary.put(s, new Integer(id));
			id++;
		}
		return this.dictionary.get(s);
	}
	
	public static WordIntDictionary getInstance(){
		if (instance ==null){
			instance = new WordIntDictionary();
		}
		return instance;
	}


	public void clear() {
		this.dictionary.clear();
		
	}
}
