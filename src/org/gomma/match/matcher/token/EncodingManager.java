package org.gomma.match.matcher.token;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntMaps;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.gomma.match.metrics.OptimizedSimilarityFunctions;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.util.string.StringDecomposer;

public class EncodingManager {
	
	public static final String OBJ_ID_PREF ="objIDs_";
	
	public static final String OBJ_ATT_PREF ="objAtts_";
	
	public static final String IDF_COUNTS = "idfCounts_";
	
	public static final String OBJ_TRIGRAMS = "objTrigrams_";
	
	
	public static final String POS_MAP = "objPositions_";

	private static EncodingManager instance;
	
	private WordIntDictionary dict;

	private Map<String, Object> encodedObjectMap;
	
	private int [][][] objEncodingIds ;
	
	private Int2IntMap wordInDocMap;
	private Int2IntMap posMap;
	
	private EncodingManager (){
		encodedObjectMap = new HashMap<String,Object>();
		dict = WordIntDictionary.getInstance();
	}
	
	
	
	
	public Object getEncodedObject(String source, String verifier){
		return encodedObjectMap.get(verifier+source);
	}
	
	
	public void encodeObjWithAttValues (ObjSet set, String sourceName, List<String> list, boolean save){
		HashSet<String> attsSet = new HashSet<String>();
		Integer[] objRangeIds = new Integer[set.size()];
		this.objEncodingIds = new int[set.size()][][];
		this.wordInDocMap =  new Int2IntOpenHashMap ();
		this.posMap =new Int2IntOpenHashMap ();
		ArrayList<ArrayList<int[][]>> objGrams = new ArrayList<ArrayList<int[][]>>();
		for (String a : list){
			attsSet.add(a);
		}
		int rangeCount = 0;
		for (Obj rangeObj : set) {
			List<String> attValues = this.getAllValues(rangeObj, attsSet);
			objRangeIds[rangeCount] = rangeObj.getID();
			posMap.put(rangeObj.getID(), rangeCount);
			objGrams.add(this.generateTrigrams(attValues, rangeCount));
			extendGlobalTokenMapWithIds(attValues,rangeCount);
			rangeCount++;
		}
		this.encodedObjectMap.put(OBJ_ID_PREF+sourceName, objRangeIds);
		this.encodedObjectMap.put(OBJ_ATT_PREF+sourceName, objEncodingIds);
		this.encodedObjectMap.put(IDF_COUNTS+sourceName, wordInDocMap);
		this.encodedObjectMap.put(OBJ_TRIGRAMS+sourceName, objGrams);
		this.encodedObjectMap.put(POS_MAP+sourceName, posMap);
	}
	
	public void deleteEncodings (String source,String verifier){
		this.encodedObjectMap.remove(verifier+source);
	}
	
	private ArrayList<int[][]> generateTrigrams (List<String> attValues, int objIndex){
		ArrayList<int[][]> objGrams = new ArrayList<int[][]>();
		for (String domainObjString : attValues) {
			int[][] domainObjGramsID = generateNGramId(this.generateNGrams(domainObjString, 3));					
			objGrams.add(domainObjGramsID);
		}
		return objGrams;
	}
	
	private Map<String,Integer> generateNGrams(String str, int gramlength) {
		str = "$$" + str + "$$";
		return StringDecomposer.decomposeOverlappingLength(str, gramlength);
	}

	//private HashSet<String> grams = new HashSet<String>();
	//private HashSet<Integer> codes = new HashSet<Integer>();
	private int[][] generateNGramId(

			Map<String, Integer> ngrams) {

		int[][] result = new int[2][];
		result[0] = new int[ngrams.size()];
		result[1] = new int[ngrams.size()];
		
		
	    TreeMap<Integer,Integer> tmpResult = new TreeMap<Integer,Integer>();
	    
	    for (String ngram : ngrams.keySet())
	    {
	    	//grams.add(ngram);
	    	//codes.add(grams.hashCode());
	        tmpResult.put(ngram.hashCode(), ngrams.get(ngram));
	    }
	     
	    int i=0;
	    for (Entry<Integer,Integer> entry : tmpResult.entrySet()) {
	    	result[0][i] = entry.getKey();
	    	result[1][i++] = entry.getValue();
	    }
	    
	    return result;
	}
	
	private  void extendGlobalTokenMapWithIds(List<String> attValues,int objIndex) {
		int valueIndex = 0;
		this.objEncodingIds[objIndex] = new int[attValues.size()][];
		HashSet <Integer> idsPerConcept = new HashSet<Integer>();
		for(String s:attValues){
			
			List<Integer> tokenIds = new ArrayList<Integer>();
			String [] a = s.split("[^A-Za-z0-9]"); 
			for(String word:a){
				Integer id = dict.check(word);
				//for (int i = 0;i<wordFreqs.get(word);i++){
				idsPerConcept.add(id);
				tokenIds.add(id);
				//}
			}
			this.objEncodingIds[objIndex][valueIndex] = new int[tokenIds.size()];
			for (int i=0;i<tokenIds.size();i++){
				this.objEncodingIds[objIndex][valueIndex][i]=tokenIds.get(i);
			}
			valueIndex++;
		}
		
		for (int id: idsPerConcept){
			Integer presentCount = this.wordInDocMap.get(id);
			if(presentCount ==null)
				wordInDocMap.put(id, 1);
			else
				wordInDocMap.put(id, presentCount+1);
			
		}
		
		
	}
	
	
	
	
	
	private List<String> getAllValues (Obj o ,HashSet<String> atts){
		List <String> values = new ArrayList<String>();
		for(Attribute a : o.getAttributeValues().getAttributeSet()){
			if (atts.contains(a.getName())){
				values.addAll(getValues(a, o));
			}
		}
		return values;
	}
	private List <String> getValues (Attribute a , Obj o ){
		List <String> values = new ArrayList<String>();
		for (AttributeValueVersion avv:o.getAttributeValues(a)){
			values.add(avv.toString());
		}
		return values;
	}
	
	public static EncodingManager getInstance(){
		if (instance ==null){
			instance = new EncodingManager();
		}
		return instance;
	}
}
