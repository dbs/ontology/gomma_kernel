package org.gomma.match.matcher.token;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

public class SoftTfidfThread extends Thread{
	Integer[] domObjIds;
	Integer[] rangeObjIds;
	Integer[][][] encodingDom;
	Integer[][][] encodingRange;
	private ThreadedSoftTfidfEncMatcher matcher;
	private ConcurrentIDFEncIndexReader idfReaderDomain;
	private ConcurrentIDFEncIndexReader idfReaderRange;
	private int multiAttributeIndex;
	private  int weightType; 
	private ObjCorrespondenceSet result;
	private ObjSet domain;
	private ObjSet range;
	private String weightedType ;
	private ConcurrentInvertedTrigramReader trigramIndexReader;
	
	public SoftTfidfThread(Integer[] domObjIds, Integer[] rangeObjIds,Integer[][][] encodingPart, Integer[][][] ranEncodingIds,
			ThreadedSoftTfidfEncMatcher threadedTF_IDFEnclMatcher, ConcurrentIDFEncIndexReader idfReaderDomain2,
			ConcurrentIDFEncIndexReader idfReaderRange2,ConcurrentInvertedTrigramReader invertTrigramTokenReader, ObjSet domain2, ObjSet range2, int multiAttributeIndex) {
		super();
		this.domObjIds = domObjIds;
		this.rangeObjIds = rangeObjIds;
		this.encodingDom = encodingPart;
		this.encodingRange = ranEncodingIds;
		this.matcher = threadedTF_IDFEnclMatcher;
		this.idfReaderDomain = idfReaderDomain2;
		this.idfReaderRange = idfReaderRange2;
		this.domain = domain2;
		this.range = range2;
		this.trigramIndexReader = invertTrigramTokenReader;
		this.weightedType = threadedTF_IDFEnclMatcher.getMatchProperties().getMetricWeighting();
		if (weightedType.equals("normal")){
			weightType =0;
		}else if (weightedType.equals("weighted")){
			weightType = 1;
		}else if (weightedType.equals("min")){
			weightType =2;
		}else if (weightedType.equals("hamming")){
			weightType =3;
		}else if (weightedType.equals("minHamming")){
			weightType = 4;
		}
		this.multiAttributeIndex = multiAttributeIndex;
	}
	
	@Override
	public void run() {
		result = new ObjCorrespondenceSet();
		for (int domIndex =0; domIndex<domObjIds.length; domIndex++) {				
			for (int ranIndex=0; ranIndex<rangeObjIds.length; ranIndex++) {
				
				List<Float> confidenceList	= new ArrayList<Float>();
				Integer[][] attValuesDomain = this.encodingDom[domIndex];
				Integer[][] attValuesRange = this.encodingRange[ranIndex]; 
				for(int attDomIndex =0;attDomIndex<attValuesDomain.length;attDomIndex++){
					Integer[] encDomValue = attValuesDomain[attDomIndex];
					for(int attRanIndex =0; attRanIndex<attValuesRange.length;attRanIndex++){
						Integer[] encRangeValue = attValuesRange[attRanIndex];
						confidenceList.add(computeConfidence(encDomValue,encRangeValue));
					}
				}
				
				
				result = addCorrespondence(domain.getObj(domObjIds[domIndex]),range.getObj(rangeObjIds[ranIndex]),new MatchResult(
																matcher.getAggregationFunction().aggregateFloatList(confidenceList),
																confidenceList.size(), "N/A"),result, true);
				//map hinzufügen
			}
			//	Auswahl aus Map
		}
		
	}
	
	

	
	private float computeConfidence(Integer[] encDomValue, Integer[] encRangeValue)
	{
		Map<Integer,Integer> wordFreqs1 = this.getFrequencies(encDomValue);
		Map<Integer,Integer> wordFreqs2 = this.getFrequencies(encRangeValue);
		if (weightType == 1){
			return computeWeightedConfidence(wordFreqs1, wordFreqs2);
		}else if (weightType ==2){
			return computeMinConfidence(wordFreqs1, wordFreqs2);
		}else if (weightType==3){
			return computeHammingConfidence(wordFreqs1, wordFreqs2);
		}else if (weightType == 4){
			return computeMinHammingConfidence(wordFreqs1, wordFreqs2);
		}else{
			return computeConfidence(wordFreqs1, wordFreqs2);
		}
		
	}	
	
	private Map<Integer,Integer> getFrequencies(Integer[] values){
		Map<Integer,Integer> wordFreqs1 = new HashMap<Integer,Integer>();
		for (int i =0; i<values.length;i++){
			if (wordFreqs1.get(values[i])==null){
				wordFreqs1.put(values[i], 1);
			}else {
				wordFreqs1.put(values[i], 1+wordFreqs1.get(values[i]));
			}
		}
		return wordFreqs1;
	}
	
	private float computeMinHammingConfidence(Map<Integer,Integer> wordFreqs1, Map<Integer,Integer> wordFreqs2){
		Set<Integer> commonWords = new HashSet<Integer>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		if(commonWords.size()==0)
			return 0f;
		
		Set<Integer> wordsOnlyInFirst= new HashSet<Integer>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		
		Set<Integer> wordsOnlyInSecond= new HashSet<Integer>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		
		double simpleLength1 =0,simpleLength2=0;
		double weightedSum1=0;
		double weightedSum2 =0;
		for(Integer commonWord : commonWords)
		{
			
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			double tf_idf1=((double)tf1) * idf_word_domain;
			simpleLength1 += tf_idf1;
			
			double tf_idf2= ((double)tf2) * idf_word_range;
			simpleLength2 += tf_idf2;
		}
		
		
		
		for(Integer wordOnlyInFirst : wordsOnlyInFirst){
				double tf_idf1 = ((double)wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
				weightedSum1 += tf_idf1;
				simpleLength1+=tf_idf1;
		}
	
		
		for(Integer wordOnlyInSecond : wordsOnlyInSecond){
				double tf_idf2 = ((double)wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
				
				weightedSum2 += tf_idf2;
				simpleLength2+=tf_idf2;
		}
		
		
		//System.out.println(hammingDistance);
		
		double hamConfidence = 1/(1+(weightedSum1/simpleLength1+weightedSum2/simpleLength2));
		 //OUT OF MEMORY
		if (wordFreqs1.size()<wordFreqs2.size()){
			hamConfidence = 1/(1+(weightedSum1));
		}else {
			hamConfidence = 1/(1+(weightedSum2));
		}
		//System.out.println("hamming:"+hamConfidence);
		return Double.isNaN(hamConfidence) ? 0f : (float) hamConfidence;
	}
	
	private float computeHammingConfidence(Map<Integer,Integer> wordFreqs1, Map<Integer,Integer> wordFreqs2){
		Set<Integer> commonWords = new HashSet<Integer>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		
		Set<Integer> wordsOnlyInFirst= new HashSet<Integer>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		Set<Integer> wordsOnlyInSecond= new HashSet<Integer>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		HashSet<Integer> toRemoveFromFirst = new HashSet<Integer>();
		HashMap<Integer,Integer> rangeDomainSoftMap = new HashMap<Integer,Integer>();
		for (Integer wf : wordsOnlyInFirst){
			Integer matchRangeToken = this.getClosureForDomainToken(wf, wordsOnlyInSecond, 0.8F);
			if (matchRangeToken!=null){
				commonWords.add(matchRangeToken);
				rangeDomainSoftMap.put(matchRangeToken, wf);
				wordsOnlyInSecond.remove(matchRangeToken);
				toRemoveFromFirst.add(wf);
			}
		}
		wordsOnlyInFirst.removeAll(toRemoveFromFirst);
		
		if(commonWords.size()==0)
			return 0f;
		Float idf_word_domain,idf_word_range;
		
		double simpleLength1 =0,simpleLength2=0;
		double weightedSum1=0;
		double weightedSum2 =0;
		for(Integer commonWord : commonWords)
		{
			int domainTokenId= -1;
			if (wordFreqs1.containsKey(commonWord))
				idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			else {
				domainTokenId =rangeDomainSoftMap.get(commonWord);
				idf_word_domain = idfReaderDomain.getIDF(multiAttributeIndex,domainTokenId );
			}
			
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1;
			if (domainTokenId==-1){
				tf1= wordFreqs1.get(commonWord);
			}else{
				tf1= wordFreqs1.get(domainTokenId);
			}
			int tf2= wordFreqs2.get(commonWord);
			double tf_idf1=((double)tf1) * idf_word_domain;
			simpleLength1 += tf_idf1;
			
			double tf_idf2= ((double)tf2) * idf_word_range;
			simpleLength2 += tf_idf2;
			//if (idfReaderRange.getIDF(multiAttributeIndex, commonWord)<0)
			//	System.out.println(idfReaderRange.getIDF(multiAttributeIndex, commonWord));
		}
		
		
		
		for(Integer wordOnlyInFirst : wordsOnlyInFirst){
				double tf_idf1=((double)wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
				weightedSum1 += tf_idf1;
				simpleLength1+=tf_idf1;
		}
	
		
		
		for(Integer wordOnlyInSecond : wordsOnlyInSecond){
				double tf_idf2= ((double)wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
			//	if (idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond)<0)
			//			System.out.println(idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond));
				weightedSum2 += tf_idf2;
				simpleLength2+=tf_idf2;
		}
		
		
		//System.out.println(hammingDistance);
		
		double hamConfidence = 1/(1d+(weightedSum1/simpleLength1+weightedSum2/simpleLength2));
		if (simpleLength1==0&& simpleLength2==0){
			hamConfidence =1;
		}
		if (hamConfidence>1){
			System.out.println(weightedSum1+"\t"+simpleLength1);
			System.out.println(weightedSum2+"\t"+simpleLength2);
			System.out.println("hamming:"+hamConfidence);
		}//System.out.println("hamming:"+hamConfidence);
		//
		return Double.isNaN(hamConfidence) ? 0f : (float) hamConfidence;
	}

	private float computeWeightedConfidence(Map<Integer,Integer> wordFreqs1, Map<Integer,Integer> wordFreqs2){
		Set<Integer> commonWords = new HashSet<Integer>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		if(commonWords.size()==0)
			return 0f;
		
		Set<Integer> wordsOnlyInFirst= new HashSet<Integer>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		
		Set<Integer> wordsOnlyInSecond= new HashSet<Integer>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		double dotProduct= 0, length1= 0, length2= 0;
		double simpleLength1 =0,simpleLength2 =0;
		for(Integer commonWord : commonWords)
		{
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			
			double tf_idf1=((double)tf1/(double)wordFreqs1.size()) * idf_word_domain;
			double tf_idf1_squared= tf_idf1 * tf_idf1;
			length1+= tf_idf1_squared;
			simpleLength1+=tf_idf1;
			
			double tf_idf2;
			//if(tf1==tf2) OPTIMIERUNG GEHT NICHT WENN QUELLEN GETRENNT BETRACHTET WERDEN
			//{
			//	tf_idf2= tf_idf1;
			//	length2+= tf_idf1_squared;
			//}
			//else
			//{
				tf_idf2=((double)tf2/(double)wordFreqs2.size()) * idf_word_range;
				length2+= tf_idf2 * tf_idf2;
				simpleLength2+=tf_idf2;
			//}
			
			dotProduct+= tf_idf1 * tf_idf2;
		}
		double hamming1 =0;
		for(Integer wordOnlyInFirst : wordsOnlyInFirst)
		{
			double tf_idf1=((double)wordFreqs1.get(wordOnlyInFirst)/(double)wordFreqs1.size()) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
			hamming1 += tf_idf1;
			simpleLength1+=tf_idf1;
			length1+= tf_idf1 * tf_idf1;
		}
		double hamming2=0;
		for(Integer wordOnlyInSecond : wordsOnlyInSecond)
		{
			double tf_idf2= ((double)wordFreqs2.get(wordOnlyInSecond)/(double)wordFreqs2.size()) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
			hamming2 += tf_idf2;
			length2+= tf_idf2 * tf_idf2;
			simpleLength2+=tf_idf2;
		}
		double hammingDistance = hamming1/simpleLength1+hamming2/simpleLength2;
		//System.out.println(hammingDistance);
		length1= Math.sqrt(length1);
		length2= Math.sqrt(length2);
		double productOfLengths= length1 * length2;
		
		 if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
			 productOfLengths= dotProduct;
		
		double cosine= dotProduct / productOfLengths;
		double weightCosine = /*(cosine);*/cosine/(Math.pow(hammingDistance, 4)+cosine);
		return Double.isNaN(weightCosine) ? 0f : (float) weightCosine;
	}
	
	private float computeMinConfidence(Map<Integer,Integer> wordFreqs1, Map<Integer,Integer> wordFreqs2)
	{
		Set<Integer> commonWords = new HashSet<Integer>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		
		if(commonWords.size()==0)
			return 0f;
		
		Set<Integer> wordsOnlyInFirst= new HashSet<Integer>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		
		Set<Integer> wordsOnlyInSecond= new HashSet<Integer>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		double dotProduct= 0, length1= 0, length2= 0;
		
		for(Integer commonWord : commonWords)
		{
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			
			double tf_idf1= Math.sqrt(tf1) * idf_word_domain;
			double tf_idf1_squared= tf_idf1 * tf_idf1;
			length1+= tf_idf1_squared;
			
			
			double tf_idf2;
			//if(tf1==tf2) OPTIMIERUNG GEHT NICHT WENN QUELLEN GETRENNT BETRACHTET WERDEN
			//{
			//	tf_idf2= tf_idf1;
			//	length2+= tf_idf1_squared;
			//}
			//else
			//{
				tf_idf2= Math.sqrt(tf2) * idf_word_range;
				length2+= tf_idf2 * tf_idf2;
				
			//}
			
			dotProduct+= tf_idf1 * tf_idf2;
		}
		double productOfLengths=0;
		
		double cosine= 0;
		if (wordFreqs1.size()<wordFreqs2.size()){
			for(Integer wordOnlyInFirst : wordsOnlyInFirst)
			{
				double tf_idf1= Math.sqrt(wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
				length1+= tf_idf1 * tf_idf1;
			}
			productOfLengths= length1 * length1;
			if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
				 productOfLengths= dotProduct;
			cosine= dotProduct / productOfLengths;
		}else {
			for(Integer wordOnlyInSecond : wordsOnlyInSecond)
			{
				double tf_idf2= Math.sqrt(wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
				length2+= tf_idf2 * tf_idf2;
			}
			productOfLengths= length2 * length2;
			if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
				 productOfLengths= dotProduct;
			cosine= dotProduct / productOfLengths;
		}
		return Double.isNaN(cosine) ? 0f : (float) cosine;
	}
	
	private float computeConfidence(Map<Integer,Integer> wordFreqs1, Map<Integer,Integer> wordFreqs2)
	{
		Set<Integer> commonWords = new HashSet<Integer>(wordFreqs1.keySet());
		commonWords.retainAll(wordFreqs2.keySet());
		if(commonWords.size()==0){
			return 0f;
			
		}
		Set<Integer> wordsOnlyInFirst= new HashSet<Integer>(wordFreqs1.keySet());
		wordsOnlyInFirst.removeAll(commonWords);
		Set<Integer> wordsOnlyInSecond= new HashSet<Integer>(wordFreqs2.keySet());
		wordsOnlyInSecond.removeAll(commonWords);
		
		
		float idf_word_domain,idf_word_range;
		double dotProduct= 0, length1= 0, length2= 0;
		
		for(Integer commonWord : commonWords)
		{
			idf_word_domain= idfReaderDomain.getIDF(multiAttributeIndex, commonWord);
			idf_word_range = idfReaderRange.getIDF(multiAttributeIndex, commonWord);
			
			int tf1= wordFreqs1.get(commonWord);
			int tf2= wordFreqs2.get(commonWord);
			
			double tf_idf1= ((double)tf1) * idf_word_domain;
			double tf_idf1_squared= tf_idf1 * tf_idf1;
			length1+= tf_idf1_squared;
			
			
			double tf_idf2;
			//if(tf1==tf2) OPTIMIERUNG GEHT NICHT WENN QUELLEN GETRENNT BETRACHTET WERDEN
			//{
			//	tf_idf2= tf_idf1;
			//	length2+= tf_idf1_squared;
			//}
			//else
			//{
				tf_idf2= ((double)tf2) * idf_word_range;
				length2+= tf_idf2 * tf_idf2;
				
			//}
			
			dotProduct+= tf_idf1 * tf_idf2;
		}
		for(Integer wordOnlyInFirst : wordsOnlyInFirst)
		{
			double tf_idf1= ((double)wordFreqs1.get(wordOnlyInFirst)) * idfReaderDomain.getIDF(multiAttributeIndex, wordOnlyInFirst);
			length1+= tf_idf1 * tf_idf1;
		}
		
		for(Integer wordOnlyInSecond : wordsOnlyInSecond)
		{
			double tf_idf2=(wordFreqs2.get(wordOnlyInSecond)) * idfReaderRange.getIDF(multiAttributeIndex, wordOnlyInSecond);
			length2+= tf_idf2 * tf_idf2;
		}
		
		length1= Math.sqrt(length1);
		length2= Math.sqrt(length2);
		double productOfLengths= length1 * length2;
		
		 if(productOfLengths<dotProduct)	// correct for floating-point rounding errors
			 productOfLengths= dotProduct;
		
		double cosine= dotProduct / productOfLengths;
		return Double.isNaN(cosine) ? 0f : (float) cosine;
	}

	
	
	
	private Integer getClosureForDomainToken (int tokenId1, Set<Integer> wordsOnlyInSecond,float threshold){
		float maxDice = 0;
		float dice = 0;
		Integer maxSimToken = null;
		int numberTrigramsDomain = this.trigramIndexReader.getTrigramIndexMap().get(tokenId1).size();
		for (Integer rt: wordsOnlyInSecond){
			dice =0;
			Set<Integer> trigramRange = this.trigramIndexReader.getTrigramIndexMap().get(rt);
			float common=0 ;
			for (Integer trigram : this.trigramIndexReader.getTrigramIndexMap().get(tokenId1)){
				Set <Integer> containValues = this.trigramIndexReader.getInvertTrigramTokenMap().get(trigram);
				if (containValues.contains(rt)){
					common++;
				}
			}
			dice = 2*common/(numberTrigramsDomain+trigramRange.size());
			if (dice>maxDice&&dice>threshold){
				maxDice = dice;
				maxSimToken = rt;
			}
		}
		return maxSimToken;
	}
	
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) {
		if (checkThreshold && matchResult.getConfidence()<matcher.getSimilarityThreshold()) return set;
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		try {
			set.addCorrespondence(cor);
		} catch (WrongSourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return set;
	}

	public ObjCorrespondenceSet getResult() {
		return result;
	}

}
