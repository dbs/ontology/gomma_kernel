package org.gomma.match.matcher.token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

public class LCSThread extends Thread{
	
	private ThreadedEncLCSMatcher matcher;
	private ObjCorrespondenceSet result;
	private Integer[] domObjIds;
	private Integer[] rangeObjIds;
	private int[][][] encodingDom;
	private int[][][] encodingRange;
	private ObjSet domain;
	private ObjSet range;
	
	
	public LCSThread(Integer[] domObjIds, Integer[] rangeObjIds,int[][][] encodingPart, int[][][] ranEncodingIds,
			ThreadedEncLCSMatcher matcher,ObjSet domain2, ObjSet range2, ConcurrentIDFEncIndexReader idfReaderDomain, ConcurrentIDFEncIndexReader idfReaderRange){
		super();
		this.matcher = matcher;
		
		this.domObjIds = domObjIds;
		this.rangeObjIds = rangeObjIds;
		this.encodingDom = encodingPart;
		this.encodingRange = ranEncodingIds;
		this.domain = domain2;
		this.range = range2;
		
	}
	
	public LCSThread(Integer[] domObjIds, Integer[] rangeObjIds,int[][][] domAttIds, int[][][] ranAttIds,
			ThreadedEncLCSMatcher matcher,ObjSet domain2, ObjSet range2){
		super();
		this.matcher = matcher;
		
		this.domObjIds = domObjIds;
		this.rangeObjIds = rangeObjIds;
		this.encodingDom = domAttIds;
		this.encodingRange = ranAttIds;
		this.domain = domain2;
		this.range = range2;
		
	}
	public void run(){
		result = new ObjCorrespondenceSet();
		for (int domIndex =0; domIndex<domObjIds.length; domIndex++) {
			if (domain.contains(domObjIds[domIndex])){
				for (int ranIndex=0; ranIndex<rangeObjIds.length; ranIndex++) {
					List<Float> confidenceList	= new ArrayList<Float>();
					int[][] attValuesDomain = this.encodingDom[domIndex];
					int[][] attValuesRange = this.encodingRange[ranIndex]; 
					for(int attDomIndex =0;attDomIndex<attValuesDomain.length;attDomIndex++){
						int[] encDomValue = attValuesDomain[attDomIndex];
						for(int attRanIndex =0; attRanIndex<attValuesRange.length;attRanIndex++){
							int[] encRangeValue = attValuesRange[attRanIndex];
							confidenceList.add(computeConfidence(encDomValue,encRangeValue));
						}
					}
					result = addCorrespondence(domain.getObj(domObjIds[domIndex]),range.getObj(rangeObjIds[ranIndex]),new MatchResult(
																		matcher.getAggregationFunction().aggregateFloatList(confidenceList),
																		confidenceList.size(), "N/A"),result, true);
				}
			}
		}
		
	}
	private Float computeConfidence(int[] encDomValue,
			int[] encRangeValue) {
		HashSet<Integer> set1 = new HashSet<Integer> ();
		for (Integer i : encDomValue)
			set1.add(i);
		HashSet<Integer> set2 = new HashSet<Integer> ();
		for (Integer i :encRangeValue){
			set2.add(i);
		}
		set1.retainAll(set2);
		float longestMatch =0F;
		
		
		if (set1.size()>1){
			for (Integer c : set1){
				int current =0;
				int index1 = 0;
				while (index1<encDomValue.length){
					if (encDomValue[index1]==c)
						break;
					else
						index1++;
				}
				int index2 =0;
				while (index2<encRangeValue.length){
					if (encRangeValue[index2]==c)
						break;
					else
						index2++;
				}
				boolean nextMatch =true;
						int matchedPos=0;
				while (nextMatch){
					if (index2<encRangeValue.length&&index1<encDomValue.length){
						if (encRangeValue[index2]==encDomValue[index1]){
							//current+= this.idfReaderDomain.getIDF(0, encDomValue[index1])*this.idfReaderRange.getIDF(0, encRangeValue[index2]);
							matchedPos++;
						}/*else if ((index2+1)<encRangeValue.length||
								(index1+1)<encDomValue.length){
							int oldIndex2 = index2;
							try{
								if (encRangeValue[index2+1]==encDomValue[index1]){
									matchedPos++;
									index2++;
								}
							}catch (IndexOutOfBoundsException e){}
							try{
								if (encRangeValue[oldIndex2]==encDomValue[index1+1]){
									matchedPos++;
									index1++;
								}
							}catch (IndexOutOfBoundsException e){}
						}*/else{
							nextMatch =false;
						}
						index2++;index1++;
					}else nextMatch = false;
				}
				if (matchedPos>longestMatch&&matchedPos>1){
					longestMatch =matchedPos;
				}
			}
			/*
			Integer[] shortArray = (encRangeValue.length<encDomValue.length)? encRangeValue:encDomValue;
			float length = 0;
			for (Integer id: shortArray){
				float idf = (shortArray ==encRangeValue)? this.idfReaderRange.getIDF(0, id):this.idfReaderDomain.getIDF(0, id);
				length += idf*idf;	
			}
			length = length*length;
			length = (float) Math.sqrt(length);
			System.out.println (set1+"\t"+Arrays.toString(encDomValue)+"\t"
			+Arrays.toString(encRangeValue)+"\t"+longestMatch);
			float sim = (float) longestMatch/length;
			return sim;*/
			//return (longestMatch)/((float)Math.min(encDomValue.length, encRangeValue.length));
			return (1.25F*longestMatch)/((float)Math.min(encDomValue.length, encRangeValue.length)+0.25F*Math.max(encDomValue.length, encRangeValue.length));
		}else return 0F;
		
	}
	public ObjCorrespondenceSet getResult() {
		return result;
	}
	
	
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) {
		if (checkThreshold && matchResult.getConfidence()<matcher.getSimilarityThreshold()) return set;
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		try {
			set.addCorrespondence(cor);
		} catch (WrongSourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return set;
	}


}
