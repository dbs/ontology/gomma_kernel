package org.gomma.match.matcher.token;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentIDFEncIndexReader {
ConcurrentHashMap<Integer, Float> globalTokenMap;
	
	

	public ConcurrentIDFEncIndexReader(ConcurrentHashMap<Integer,Float> globalTokenMapDomain){
		this.globalTokenMap = globalTokenMapDomain;
	}
	
	
	public float getIDF(int attributeIndex, Integer token) { 
		return globalTokenMap.get(token);
		//nutze attribute index im Moment nicht, weil einfache TokenMap fuer nur ein Attribut
	}


	public void clear() {
		this.globalTokenMap.clear();
	}
}
