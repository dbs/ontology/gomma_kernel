package org.gomma.match.matcher.token;

import java.util.concurrent.ConcurrentHashMap;

public class TrigramIntDictionary {

private ConcurrentHashMap<String,Integer> dictionary;
	
	private static TrigramIntDictionary instance;
	
	static int id =0;
	public TrigramIntDictionary (){
		id = 0;
		dictionary = new ConcurrentHashMap<String,Integer>();
		
	}
	
	
	public Integer check(String s){
		Integer sid = this.dictionary.get(s);
		if (sid ==null){
			this.dictionary.put(s, new Integer(id));
			id++;
		}
		return this.dictionary.get(s);
	}
	
	public static TrigramIntDictionary getInstance(){
		if (instance ==null){
			instance = new TrigramIntDictionary();
		}
		return instance;
	}


	public void clear() {
		this.dictionary.clear();
		
	}

}
