/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.wdimatchlibrary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.string.StringDecomposer;


public class WDIMatchLibraryMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public WDIMatchLibraryMatcher() {}
	

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		/*try {
			GOMMAObjectInstanceProvider domainOIP = new GOMMAObjectInstanceProvider(domain,this.matchProps.getDomainAttributeNameList());
			GOMMAObjectInstanceProvider rangeOIP = new GOMMAObjectInstanceProvider(range,this.matchProps.getDomainAttributeNameList());
			
			ArrayListMainMemoryMapping resultStore = new ArrayListMainMemoryMapping();
			
			//Hier case einbauen - aktuell hart Trigram
			LuceneTFIDFFullyCachedAlternative trigram = new LuceneTFIDFFullyCachedAlternative(this.matchProps.getDomainAttributeNameList().get(0),this.matchProps.getRangeAttributeNameList().get(0),this.simThreshold);
			try {
				trigram.match(domainOIP,rangeOIP,resultStore);
			} catch (MappingStoreException e) {
				e.printStackTrace();
			}
			
			Iterator<IMappingEntry> resultStoreIterator = resultStore.iterator();
			while (resultStoreIterator.hasNext()) {
				IMappingEntry mappingEntry = resultStoreIterator.next();
				int domainObjID = Integer.valueOf(mappingEntry.getLeft().getId());
				int rangeObjID = Integer.valueOf(mappingEntry.getRight().getId());
				float similarity = (float)mappingEntry.getSimilarity().getSim();
				
				this.addCorrespondence(domain.getObj(domainObjID), range.getObj(rangeObjID), 
						new MatchResult(similarity,0,"N/A"), result, true);
			}
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}*/
		return result;
		
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String,Integer>> domainAttrValueList, rangeAttrValueList;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size()<domain.size()*range.size()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) &&
							range.contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());
					
					domainAttrValueList = this.createStringPortion(
							super.getAttributeValues(domainObj,true));
		
					rangeAttrValueList = this.createStringPortion(
							super.getAttributeValues(rangeObj,false));
					
					result = this.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(domainAttrValueList,rangeAttrValueList),result,true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(
							super.getAttributeValues(domainObj,true));
					
					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
					
						rangeAttrValueList = this.createStringPortion(
								super.getAttributeValues(rangeObj,false));
						
						result = this.addCorrespondence(domainObj,rangeObj,
									this.computeMatch(domainAttrValueList,rangeAttrValueList),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private List<Map<String,Integer>> createStringPortion(List<String> list) throws OperatorException {
		List<Map<String,Integer>> resultList = new ArrayList<Map<String,Integer>>();
		
		for (String s : list) {
			switch ((StringSimilarityFunctions)this.simFunc) {
				case BIGRAM_DICE: 
					resultList.add(StringDecomposer.decomposeOverlappingLength(s,2));
					break;
				case TRIGRAM_DICE:
					resultList.add(StringDecomposer.decomposeOverlappingLength(s,3));
					break;
				case TRIGRAM_DICE_FILLING:
					resultList.add(StringDecomposer.fillAndDecomposeOverlappingLength(s,3));
					break;
				case FULLWORD_DICE:
					Map<String, Integer> wordMap;
					if(resultList.isEmpty()){
						wordMap  = new HashMap<String, Integer>();							
					}else{
						wordMap = resultList.get(0);
					}
					Integer cnt = wordMap.get(s);
					if(cnt!=null){
						wordMap.put(s,cnt++);
					}else{
						wordMap.put(s,1);
					}
					resultList.clear();
					resultList.add(wordMap);	
					break;
				default: throw new OperatorException("Could not resolve the string decomposer function.");
			}			
		}		
		return resultList;
	}

	private MatchResult computeMatch(List<Map<String,Integer>> domainAttrValueList, List<Map<String,Integer>> rangeAttrValueList) throws OperatorException {
		List<Float> confidenceList  = new ArrayList<Float>();
		
		for (Map<String,Integer> domainMap : domainAttrValueList) 
			for (Map<String,Integer> rangeMap : rangeAttrValueList) 
				confidenceList.add(super.simFunc.computeConfidence(domainMap, rangeMap));
		
		return new MatchResult(super.aggFunc.aggregateFloatList(confidenceList),confidenceList.size(),"N/A");
	}
	
	private void computeNGramSimilarity(String domainObjString, Map<String, Integer> domainObjGrams, String rangeObjString, Map<String, Integer> rangeObjGrams, List<Float> confidenceList) throws OperatorException {
		Map<String, Integer> shortGrams, longGrams;
		float testThreshold = 0.5F*super.simThreshold;
		if (domainObjGrams.size()>rangeObjGrams.size()) {
			shortGrams = rangeObjGrams;
			longGrams = domainObjGrams;
		} else {
			shortGrams = domainObjGrams;
			longGrams = rangeObjGrams;
		}
		float numberOfGramsShort = Math.max(1, shortGrams.size());
		float numberOfGramsLong = Math.max(1, longGrams.size());
		float maxPossibleGrams = Math.min(numberOfGramsShort, numberOfGramsLong)+1;
		
		if (maxPossibleGrams<testThreshold*(numberOfGramsShort+numberOfGramsLong)) {
			confidenceList.add(0.0F);
			return;
		}
		
		float count = 0.0F;
		
		for (String gram : shortGrams.keySet()) {
			if (longGrams.containsKey(gram)) {
				count++;
			} else {
				maxPossibleGrams--;
				if (maxPossibleGrams<testThreshold*(numberOfGramsShort+numberOfGramsLong)) {
					confidenceList.add(2.0F*count/((float)(domainObjGrams.size()+rangeObjGrams.size())));
					return;
				}
			}
		}
		confidenceList.add(2.0F*count/((float)(domainObjGrams.size()+rangeObjGrams.size())));
	}
	
	private void computeNGramSimilarity2(String domainObjString, Map<String, Integer> domainObjGrams, String rangeObjString, Map<String, Integer> rangeObjGrams, List<Float> confidenceList) throws OperatorException {
		String shortString, longString;
		Map<String, Integer> shortGrams, longGrams;
		float testThreshold = 0.5F*super.simThreshold;
		if (domainObjGrams.size()>rangeObjGrams.size()) {
			shortString = rangeObjString;
			shortGrams = rangeObjGrams;
			longString = domainObjString;
			longGrams = domainObjGrams;
		} else {
			shortString = domainObjString;
			shortGrams = domainObjGrams;
			longString = rangeObjString;
			longGrams = rangeObjGrams;
		}
		float numberOfGramsShort = Math.max(1, shortString.length()-2);
		float numberOfGramsLong = Math.max(1, longString.length()-2);
		float maxPossibleGrams = Math.min(numberOfGramsShort, numberOfGramsLong);
		
		if (maxPossibleGrams<testThreshold*(numberOfGramsShort+numberOfGramsLong)) {
			confidenceList.add(0.0F);
			return;
		}
		
		float count = 0.0F;
		
		for (String gram : shortGrams.keySet()) {
			if (longGrams.containsKey(gram)) {
				//Max oder Min?
				Math.min(shortGrams.get(gram), longGrams.get(gram));
			} else {
				maxPossibleGrams -= shortGrams.get(gram);
				if (maxPossibleGrams<testThreshold*(numberOfGramsShort+numberOfGramsLong)) {
					confidenceList.add(0.0F);
					return;
				}
			}
		}
		confidenceList.add(2.0F*count/(numberOfGramsShort+numberOfGramsLong));
	}
	  private float computeSortedNGramSimilarity(int[] gramsId1, int[] gramsId2) {
		    //for (int i=0; i<grams2.length; i++) System.out.print(grams2[i] + ";"); System.out.println();
		    /*int count = 0;
		    if (gramsId1.length<gramsId2.length) {
		    	for (int i = 0; i < gramsId1.length; i++) {
		    		for (int j = 0; j < gramsId2.length; j++) {
		    			if (gramsId1[i] == gramsId2[j]) {
				          //System.out.println("Common gram: " + grams1[i]);
				          count++;
				          break;
				        } else if (gramsId1[i] < gramsId2[j]) {
				        	break;
				        }
		    		}
		    	}
		    } else {
		    	for (int i = 0; i < gramsId2.length; i++) {
		    		for (int j = 0; j < gramsId1.length; j++) {
				        if (gramsId2[i] == gramsId1[j]) {
				          //System.out.println("Common gram: " + grams1[i]);
				          count++;
				          break;
				        } else if (gramsId2[i] < gramsId1[j]) {
				        	break;
				        }
		    		}
		    	}
		    }*/
		    int count = 0;
		    int left = 0, right = 0;
		    while (left<gramsId1.length&&right<gramsId2.length) {
		    	if (gramsId1[left]==gramsId2[right]) {
		    		count++;
		    		left++;
		    	} else if (gramsId1[left]<gramsId2[right]) {
		    		left++;
		    	} else {
		    		right++;
		    	}
		    }
		    if (right==gramsId2.length) {
			    left++;
		    	while (left<gramsId1.length-1) {
			    	if (gramsId1[left]==gramsId2[gramsId2.length-1]) {
			    		count++;
			    	}
			    	left++;
			    }
		    }
		    if (left==gramsId1.length) {
			    right++;
		    	while (right<gramsId2.length) {
				    if (gramsId2[right]==gramsId1[gramsId1.length-1]) {
				    	count++;
				    }
				    right++;
			    }
		    }
	  		
		    float sim = (float) 2 * count / (gramsId1.length + gramsId2.length); // Dice-Coefficient
		    return sim;
	}
	  private float computeNGramSimilarity(int[] gramsId1, int[] gramsId2) {
		    //for (int i=0; i<grams2.length; i++) System.out.print(grams2[i] + ";"); System.out.println();
		  int count = 0;
		    for (int i = 0; i < gramsId1.length; i++)
		      for (int j = 0; j < gramsId2.length; j++) {
		        if (gramsId1[i] == gramsId2[j]) {
		          //System.out.println("Common gram: " + grams1[i]);
		          count++;
		          break;
		        }
		      }
		    //System.out.println("Common grams: " + count);
		    //float sim = (float) 1 / (1+grams1.length +grams2.length - 2 * count);
		    //float sim = 1 - 0.5 * ((float)(grams1.length-count)/grams1.length +
		    //		       (float)(grams2.length-count )/grams2.length);
		    float sim = (float) 2 * count / (gramsId1.length + gramsId2.length); // Dice-Coefficient
		    return sim;
	  }

	  private String[] generateNGrams(String str, int gramlength) {
		    Map<String,Integer> grams = StringDecomposer.decomposeOverlappingLength(str, gramlength);
		    return (String[]) grams.keySet().toArray(new String[0]);
		  }

}
