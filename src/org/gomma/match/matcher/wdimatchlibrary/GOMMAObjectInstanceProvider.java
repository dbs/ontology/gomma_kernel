package org.gomma.match.matcher.wdimatchlibrary;


public class GOMMAObjectInstanceProvider /*extends AbstractObjectInstanceProvider*/ {
	/*
	private String  uid = "GOMMAObjectInstanceProvider";
	private Map<String,IObjectInstance> objects;
	
	public GOMMAObjectInstanceProvider(ObjSet gommaObjects, List<String> attributeNames) {
		super(EOIProviderCapabilityHint.MEMORY);
		this.convertObjects(gommaObjects, attributeNames);
		super.size = this.objects.size();
	}
	
	@Override
	public void close() throws ObjectInstanceProviderException {
		// TODO Auto-generated method stub
	}
	
	private void convertObjects(ObjSet gommaObjects, List<String> attributeNames) {
		this.objects = new HashMap<String, IObjectInstance>();
		for (Obj gommaObj : gommaObjects.getCollection()) {
			Map<String,Object> attributeValues = new HashMap<String, Object>();
			attributeValues.put("objID", gommaObj.getID());
			attributeValues.put("accession", gommaObj.getAccessionNumber());
			
			SourceVersion s = gommaObj.getSourceVersionSet().getLatestSourceVersion();
			AttributeSet attSet = gommaObj.getAttributeValues(s.getID()).getAttributeSet();
			
			for (String attName : attributeNames) {
				if (!attSet.contains(attName)) continue;
				for (Attribute att : attSet.getAttributeSet(attName)) {
					Iterator<AbstractAttributeValueVersion> it = gommaObj.getAttributeValues(s.getID(),att).iterator();
					if (it.hasNext()) {
						AttributeValueVersion avv = gommaObj.getAttributeValues(s.getID(),att).iterator().next();
						attributeValues.put(attName, avv.toString());
					}
				}
			}
			
			IObjectInstance wdiObj = new ObjectInstance(attributeValues,"objID");
			this.objects.put(String.valueOf(gommaObj.getID()), wdiObj);
		}	
	}
	
	@Override
	public IObjectInstance getInstance(String id) {
		return this.objects.get(id);
	}

	@Override
	public IObjectInstanceMetaData getMetaData() {
		return new ObjectInstanceMetaData("objID");
	}

	@Override
	public String getObjectInstanceProviderUID() {
		return this.uid;
	}

	@Override
	public Iterator<IObjectInstance> iterator() {
		return this.objects.values().iterator();
	}

	@Override
	public void open() throws ObjectInstanceProviderException {
		// TODO Auto-generated method stub
	}
	
	public int size() {
		return this.objects.size();
	}
	*/
}
