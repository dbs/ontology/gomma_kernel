/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher;

import java.util.HashMap;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

/**
 * The interface defines some general methods 
 * setting and getting matcher properties and executes a matcher.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface Matcher {

	/**
	 * The method computes and returns a mapping between 
	 * both source version structures s1 and s2 using the
	 * previously specified match properties.  
	 * @param domain domain source version structure
	 * @param range range source version structure
	 * @return set of correspondences between objects of s1 and s2
	 * @throws OperatorException
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure domain, SourceVersionStructure range) throws OperatorException;
	
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range) throws OperatorException;
	
	/**
	 * The method sets the similarity function. 
	 * @param simFunc similarity function
	 */
	public void setSimilarityFunction(SimilarityFunction simFunc);
	
	/**
	 * The method sets the aggregation function.
	 * @param aggFunc aggregation function
	 */
	public void setAggregationFunction(AggregationFunction aggFunc);
	
	/**
	 * The method sets the similarity threshold.
	 * @param simThreshold similarity threshold
	 */
	public void setSimilarityThreshold(float simThreshold);
	
	/**
	 * The method sets the set of match properties.
	 * @param props set of match properties
	 */
	public void setMatchProperties(MatchProperties matchProperties);
	/**
	 * The method returns the similarity function.
	 * @return similarity function
	 */
	public SimilarityFunction getSimilarityFunction();
	
	/**
	 * The method returns the aggregation function.
	 * @return aggregation function
	 */
	public AggregationFunction getAggregationFunction();
	
	/**
	 * The method returns the similarity threshold.
	 * @return similarity threshold
	 */
	public float getSimilarityThreshold();
	
	/**
	 * The method returns the set of match properties.
	 * @return set of match properties 
	 */
	public MatchProperties getMatchProperties();
	
	/**
	 * The method sets the initial mapping that can be 
	 * considered by the matcher.
	 * @param initMap initial mapping
	 */
	public void setInitialMapping(ObjCorrespondenceSet initMap);
	
	/**
	 * The method returns the initial mapping that can be 
	 * considered by the matcher.
	 * @return initial mapping
	 */
	public ObjCorrespondenceSet getInitialMapping();

	
}
