/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.OptimizedSimilarityFunctions;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.string.StringDecomposer;

public class ThreadedListTrigramMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public ThreadedListTrigramMatcher() {
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2
				.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
			//Zerlegung in Trigramme fuer Domain und Range fuer jeden Multi-Value-Attribut-Wert		
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			ArrayList<Integer> domainObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[][]>> domainGrams = new ArrayList<ArrayList<int[][]>>();
			for (Obj domainObj : domain) {
				List<String> domainObjStrings = super.getAttributeValues(
						domainObj, true);
				ArrayList<int[][]> domainObjGrams = new ArrayList<int[][]>();
				for (String domainObjString : domainObjStrings) {
					int[][] domainObjGramsID = generateNGramId(this.generateNGrams(domainObjString, 3));					
					domainObjGrams.add(domainObjGramsID);
				}
				domainObjIDs.add(domainObj.getID());
				domainGrams.add(domainObjGrams);
			}
			ArrayList<Integer> rangeObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[][]>> rangeGrams = new ArrayList<ArrayList<int[][]>>();
			for (Obj rangeObj : range) {
				List<String> rangeObjStrings = super.getAttributeValues(
						rangeObj, false);
				ArrayList<int[][]> rangeObjGrams = new ArrayList<int[][]>();
				for (String rangeObjString : rangeObjStrings) {
					int[][] rangeObjGramsID = generateNGramId(this.generateNGrams(rangeObjString, 3));					
					rangeObjGrams.add(rangeObjGramsID);
				}
				rangeObjIDs.add(rangeObj.getID());
				rangeGrams.add(rangeObjGrams);
			}
			
			//Threadanzahl ermitteln
			int numberOfProcessros = Runtime.getRuntime().availableProcessors();
			int threadNumber;
			if (domainObjIDs.size()<1000&&rangeObjIDs.size()<1000) {
				threadNumber = 4;
			} else {
				threadNumber = Math.max(numberOfProcessros, 16);
			}
			
			//Gr��ere Ontologie f�r Threading aufspalten (Modulo-Ansatz!!) Ziel: Menge von k Match-Threads
			List<ListTrigramThread> threadList = new ArrayList<ListTrigramThread>();
			if (domainObjIDs.size()>rangeObjIDs.size()) {
				//Domain splitten
				ArrayList<ArrayList<Integer>> domObjIDsParts = new ArrayList<ArrayList<Integer>>();
				ArrayList<ArrayList<ArrayList<int[][]>>> domainObjGramsParts = new ArrayList<ArrayList<ArrayList<int[][]>>>();
				for (int i=0;i<threadNumber;i++) {
					domObjIDsParts.add(new ArrayList<Integer>());
					domainObjGramsParts.add(new ArrayList<ArrayList<int[][]>>());
				}
				for (int i=0;i<domainObjIDs.size();i++) {
					int domainObjID = domainObjIDs.get(i);
					domObjIDsParts.get(domainObjID%threadNumber).add(domainObjID);
					domainObjGramsParts.get(domainObjID%threadNumber).add(domainGrams.get(i));
				}
				for (int i=0;i<threadNumber;i++) {
					ListTrigramThread tmpThread = new ListTrigramThread(domObjIDsParts.get(i), domainObjGramsParts.get(i), domain, rangeObjIDs, rangeGrams, range, this);
					threadList.add(tmpThread);
				}
			} else {
				//Range splitten
				ArrayList<ArrayList<Integer>> rangeObjIDsParts = new ArrayList<ArrayList<Integer>>();
				ArrayList<ArrayList<ArrayList<int[][]>>> rangeObjGramsParts = new ArrayList<ArrayList<ArrayList<int[][]>>>();
				for (int i=0;i<threadNumber;i++) {
					rangeObjIDsParts.add(new ArrayList<Integer>());
					rangeObjGramsParts.add(new ArrayList<ArrayList<int[][]>>());
				}
				for (int i=0;i<rangeObjIDs.size();i++) {
					int rangeObjID = rangeObjIDs.get(i);
					rangeObjIDsParts.get(rangeObjID%threadNumber).add(rangeObjID);
					rangeObjGramsParts.get(rangeObjID%threadNumber).add(rangeGrams.get(i));
				}
				for (int i=0;i<threadNumber;i++) {
					ListTrigramThread tmpThread = new ListTrigramThread(domainObjIDs, domainGrams, domain, rangeObjIDsParts.get(i), rangeObjGramsParts.get(i), range, this);
					threadList.add(tmpThread);
				}
			}
			
			//Alle Threads starten / gemeinsames Warten auf letzten Thread
			for (ListTrigramThread thread : threadList) {
				thread.start();
			}
			for (ListTrigramThread thread : threadList) {
				try {
					thread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			//Fusionierung der Teilergebnisse zu Gesamtergebnis
			for (ListTrigramThread thread : threadList) {
				try {
					result = SetOperators.union(result, thread.result);
				} catch (WrongSourceException e) {
					e.printStackTrace();
				}
			}
			return result;
	}

	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	private List<Map<String, Integer>> createStringPortion(List<String> list)
			throws OperatorException {
		List<Map<String, Integer>> resultList = new ArrayList<Map<String, Integer>>();

		for (String s : list) {
			switch ((StringSimilarityFunctions) this.simFunc) {
			case BIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 2));
				break;
			case TRIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 3));
				break;
			case TRIGRAM_DICE_FILLING:
				resultList.add(StringDecomposer
						.fillAndDecomposeOverlappingLength(s, 3));
				break;
			case FULLWORD_DICE:
				Map<String, Integer> wordMap;
				if (resultList.isEmpty()) {
					wordMap = new HashMap<String, Integer>();
				} else {
					wordMap = resultList.get(0);
				}
				Integer cnt = wordMap.get(s);
				if (cnt != null) {
					wordMap.put(s, cnt++);
				} else {
					wordMap.put(s, 1);
				}
				resultList.clear();
				resultList.add(wordMap);
				break;
			default:
				throw new OperatorException(
						"Could not resolve the string decomposer function.");
			}
		}
		return resultList;
	}

	private MatchResult computeMatch(
			List<Map<String, Integer>> domainAttrValueList,
			List<Map<String, Integer>> rangeAttrValueList)
			throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();

		for (Map<String, Integer> domainMap : domainAttrValueList)
			for (Map<String, Integer> rangeMap : rangeAttrValueList)
				confidenceList.add(super.simFunc.computeConfidence(domainMap,
						rangeMap));

		return new MatchResult(
				super.aggFunc.aggregateFloatList(confidenceList),
				confidenceList.size(), "N/A");
	}

	private Map<String,Integer> generateNGrams(String str, int gramlength) {

		if (this.simFunc==OptimizedSimilarityFunctions.OPTIMIZED_THREADED_LIST_TRIGRAM_FILLING) {
			str = "$$" + str + "$$";
		}
		return StringDecomposer.decomposeOverlappingLength(str, gramlength);
	}

	private int[][] generateNGramId(

			Map<String, Integer> ngrams) {

		int[][] result = new int[2][];
		result[0] = new int[ngrams.size()];
		result[1] = new int[ngrams.size()];
		
		
	    TreeMap<Integer,Integer> tmpResult = new TreeMap<Integer,Integer>();
	    
	    for (String ngram : ngrams.keySet())
	    {
	        tmpResult.put(ngram.hashCode(), ngrams.get(ngram));
	    }
	     
	    int i=0;
	    for (Entry<Integer,Integer> entry : tmpResult.entrySet()) {
	    	result[0][i] = entry.getKey();
	    	result[1][i++] = entry.getValue();
	    }
	    
	    return result;
	}

	
}