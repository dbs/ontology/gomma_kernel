/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.string.StringDecomposer;

public class FastExactMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public FastExactMatcher() {
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2
				.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			HashMap<String, List<Set<Integer>>> exactMatches = new HashMap<String, List<Set<Integer>>>();
			
			for (Obj domainObj : domain) {
				List<String> domainObjStrings = super.getAttributeValues(domainObj, true);
				for (String domainObjString : domainObjStrings) {
					List<Set<Integer>> currentMatches = exactMatches.get(domainObjString);
					if (currentMatches==null) {
						Set<Integer> setDomain = new HashSet<Integer>();
						Set<Integer> setRange = new HashSet<Integer>();
						setDomain.add(domainObj.getID());
						currentMatches = new Vector<Set<Integer>>();
						currentMatches.add(setDomain);
						currentMatches.add(setRange);
						exactMatches.put(domainObjString, currentMatches);
					} else {
						currentMatches.get(0).add(domainObj.getID());
					}
				}
			}
			for (Obj rangeObj : range) {
				List<String> rangeObjStrings = super.getAttributeValues(rangeObj, false);
				for (String rangeObjString : rangeObjStrings) {
					List<Set<Integer>> currentMatches = exactMatches.get(rangeObjString);
					if (currentMatches==null) {
						//Nichts machen, da in der Domain nichts gefunden wurde und daher eh kein Match entstehen kann
						/*Set<Integer> setDomain = new HashSet<Integer>();
						Set<Integer> setRange = new HashSet<Integer>();
						setRange.add(rangeObj.getID());
						currentMatches = new Vector<Set<Integer>>();
						currentMatches.add(setDomain);
						currentMatches.add(setRange);
						exactMatches.put(domainObjString, currentMatches);*/
					} else {
						currentMatches.get(1).add(rangeObj.getID());
					}
				}
			}
			
			for (String exactString : exactMatches.keySet()) {
				List<Set<Integer>> currentMatches = exactMatches.get(exactString);
				Set<Integer> domainObjIDs = currentMatches.get(0);
				Set<Integer> rangeObjIDs = currentMatches.get(1);
				for (int domainObjID : domainObjIDs) {
					for (int rangeObjID : rangeObjIDs) {
						result = super.addCorrespondence(domain.getObj(domainObjID), range.getObj(rangeObjID), new MatchResult(1.0F, 1, "N/A"), result, false);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	private List<Map<String, Integer>> createStringPortion(List<String> list)
			throws OperatorException {
		List<Map<String, Integer>> resultList = new ArrayList<Map<String, Integer>>();

		for (String s : list) {
			switch ((StringSimilarityFunctions) this.simFunc) {
			case BIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 2));
				break;
			case TRIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 3));
				break;
			case TRIGRAM_DICE_FILLING:
				resultList.add(StringDecomposer
						.fillAndDecomposeOverlappingLength(s, 3));
				break;
			case FULLWORD_DICE:
				Map<String, Integer> wordMap;
				if (resultList.isEmpty()) {
					wordMap = new HashMap<String, Integer>();
				} else {
					wordMap = resultList.get(0);
				}
				Integer cnt = wordMap.get(s);
				if (cnt != null) {
					wordMap.put(s, cnt++);
				} else {
					wordMap.put(s, 1);
				}
				resultList.clear();
				resultList.add(wordMap);
				break;
			default:
				throw new OperatorException(
						"Could not resolve the string decomposer function.");
			}
		}
		return resultList;
	}

	private MatchResult computeMatch(
			List<Map<String, Integer>> domainAttrValueList,
			List<Map<String, Integer>> rangeAttrValueList)
			throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();

		for (Map<String, Integer> domainMap : domainAttrValueList)
			for (Map<String, Integer> rangeMap : rangeAttrValueList)
				confidenceList.add(super.simFunc.computeConfidence(domainMap,
						rangeMap));

		return new MatchResult(
				super.aggFunc.aggregateFloatList(confidenceList),
				confidenceList.size(), "N/A");
	}

}
