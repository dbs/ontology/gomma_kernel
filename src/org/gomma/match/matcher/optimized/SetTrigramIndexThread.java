package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

public class SetTrigramIndexThread extends Thread {

	public ObjCorrespondenceSet result;
	
	private Map<Integer, Set<Integer>> mainIndex;
	private Map<Integer, int[]> representationIndex;
	private Map<Integer, Integer> attributeToObjMap;
	private ObjSet domain;
	
	private ArrayList<Integer> rangeObjIDs;
	private ArrayList<ArrayList<int[]>> rangeGrams;
	private ObjSet range;
	private FastSetTrigramIndexMatcher matcher;
	
	public SetTrigramIndexThread(Map<Integer, Set<Integer>> mainIndex, Map<Integer, int[]> representationIndex, Map<Integer, Integer> attributeToObjMap, ObjSet domain, ArrayList<Integer> rangeObjIDs, ArrayList<ArrayList<int[]>> rangeGrams, ObjSet range, FastSetTrigramIndexMatcher matcher) {
		this.mainIndex = mainIndex;
		this.representationIndex = representationIndex;
		this.attributeToObjMap = attributeToObjMap;
		this.domain = domain;
		this.rangeObjIDs = rangeObjIDs;
		this.rangeGrams = rangeGrams;
		this.range = range;
		this.matcher = matcher;
	}

	public void run() {
		result = new ObjCorrespondenceSet();
		
		for (int j = 0; j < rangeObjIDs.size(); j++) {
			Map<Integer,List<Float>> confidenceListByDomainObj = new HashMap<Integer,List<Float>>();
			
			for (int[] rangeObjGrams : rangeGrams.get(j)) {
				Set<Integer> domainCandidates = new HashSet<Integer>();
				for (int rangeObjGram : rangeObjGrams) {
					Set<Integer> currentCandidates = mainIndex.get(rangeObjGram);
					if (currentCandidates!=null) {
						domainCandidates.addAll(currentCandidates);
					}
				}
				for (int domainAttrID : domainCandidates) {
					int[] domainAttrGrams = representationIndex.get(domainAttrID);
					float simValue = this.computeSortedNGramSimilarity(domainAttrGrams, rangeObjGrams);
					int domainObjID = attributeToObjMap.get(domainAttrID);
					List<Float> currentConfidenceValues = confidenceListByDomainObj.get(domainObjID);
					if (currentConfidenceValues==null) {
						currentConfidenceValues = new Vector<Float>();
					}
					currentConfidenceValues.add(simValue);
					confidenceListByDomainObj.put(domainObjID, currentConfidenceValues);
				}
			}
			
			for (int domainObjID : confidenceListByDomainObj.keySet()) {
				result = this
				.addCorrespondence(
						domain.getObj(domainObjID),
						range.getObj(rangeObjIDs.get(j)),
						new MatchResult(
								matcher.getAggregationFunction()
									.aggregateFloatList(confidenceListByDomainObj.get(domainObjID)),
								confidenceListByDomainObj.get(domainObjID).size(), "N/A"),
								result, true);
			}
		}
	}
	
	private float computeSortedNGramSimilarity(int[] gramsId1, int[] gramsId2) {
		// merge-sort-aehnlicher Vergleich der Trigramlisten fuer
		// Overlapberechnung (Dice)
		int count = 0;
		int left = 0, right = 0;
		while (left < gramsId1.length && right < gramsId2.length) {
			if (gramsId1[left] == gramsId2[right]) {
				count++;
				left++;
				right++;
			} else if (gramsId1[left] < gramsId2[right]) {
				left++;
			} else {
				right++;
			}
		}

		float sim = (float) 2 * count / (gramsId1.length + gramsId2.length); // Dice-Coefficient
		return sim;
	}
	
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) {
		
		if (checkThreshold && matchResult.getConfidence()<this.matcher.getSimilarityThreshold()) return set;
			
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		try {
			set.addCorrespondence(cor);
		} catch (WrongSourceException e) {
			e.printStackTrace();
		}
		
		return set;
	}
}
