/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.string.StringDecomposer;

public class FastListTrigramIndexMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public FastListTrigramIndexMatcher() {
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2
				.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			//Erstmal wird immer domain indexiert
			/*boolean isDomainIndexed;
			if (domain.size()>range.size()) {
				isDomainIndexed = false;
			} else {
				isDomainIndexed = true;
			}*/
			
			Map<Integer, Set<Integer>> mainIndex = new HashMap<Integer, Set<Integer>>();
			Map<Integer, int[][]> representationIndex = new HashMap<Integer, int[][]>();
			Map<Integer, Integer> attributeToObjMap = new HashMap<Integer, Integer>();
			
			int attributeCounter = 0;
			
			for (Obj domainObj : domain) {
				List<String> domainObjStrings = super.getAttributeValues(
						domainObj, true);
				for (String domainObjString : domainObjStrings) {
					int[][] domainObjGramsID = generateNGramId(this
							.generateNGrams(domainObjString, 3));
					attributeCounter++;
					representationIndex.put(attributeCounter, domainObjGramsID);
					attributeToObjMap.put(attributeCounter, domainObj.getID());
					for (int gram : domainObjGramsID[0]) {
						Set<Integer> currentAttributes = mainIndex.get(gram);
						if (currentAttributes==null) {
							currentAttributes = new HashSet<Integer>();
						}
						currentAttributes.add(attributeCounter);
						mainIndex.put(gram, currentAttributes);
					}
					
				}
			}	
			
			ArrayList<Integer> rangeObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[][]>> rangeGrams = new ArrayList<ArrayList<int[][]>>();
			for (Obj rangeObj : range) {
				List<String> rangeObjStrings = super.getAttributeValues(
						rangeObj, false);
				ArrayList<int[][]> rangeObjGrams = new ArrayList<int[][]>();
				for (String rangeObjString : rangeObjStrings) {
					int[][] rangeObjGramsID = generateNGramId(this
							.generateNGrams(rangeObjString, 3));
					rangeObjGrams.add(rangeObjGramsID);
				}
				rangeObjIDs.add(rangeObj.getID());
				rangeGrams.add(rangeObjGrams);
			}
			
			//Threadanzahl ermitteln
			int numberOfProcessros = Runtime.getRuntime().availableProcessors();
			int threadNumber;
			if (rangeObjIDs.size()<1000) {
				threadNumber = 4;
			} else {
				threadNumber = Math.max(numberOfProcessros, 16);
			}
			List<ListTrigramIndexThread> threadList = new Vector<ListTrigramIndexThread>();
			
			//Range splitten
			ArrayList<ArrayList<Integer>> rangeObjIDsParts = new ArrayList<ArrayList<Integer>>();
			ArrayList<ArrayList<ArrayList<int[][]>>> rangeObjGramsParts = new ArrayList<ArrayList<ArrayList<int[][]>>>();
			for (int i=0;i<threadNumber;i++) {
				rangeObjIDsParts.add(new ArrayList<Integer>());
				rangeObjGramsParts.add(new ArrayList<ArrayList<int[][]>>());
			}
			for (int i=0;i<rangeObjIDs.size();i++) {
				int rangeObjID = rangeObjIDs.get(i);
				rangeObjIDsParts.get(rangeObjID%threadNumber).add(rangeObjID);
				rangeObjGramsParts.get(rangeObjID%threadNumber).add(rangeGrams.get(i));
			}
			for (int i=0;i<threadNumber;i++) {
				ListTrigramIndexThread tmpThread = new ListTrigramIndexThread(mainIndex,representationIndex, attributeToObjMap, domain, rangeObjIDsParts.get(i), rangeObjGramsParts.get(i), range, this);
				threadList.add(tmpThread);
			}
			
			//Alle Threads starten / gemeinsames Warten auf letzten Thread
			for (ListTrigramIndexThread thread : threadList) {
				thread.start();
			}
			for (ListTrigramIndexThread thread : threadList) {
				try {
					thread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			//Fusionierung der Teilergebnisse zu Gesamtergebnis
			for (ListTrigramIndexThread thread : threadList) {
				try {
					result = SetOperators.union(result, thread.result);
				} catch (WrongSourceException e) {
					e.printStackTrace();
				}
			}
			
			return result;
	}

	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	private List<Map<String, Integer>> createStringPortion(List<String> list)
			throws OperatorException {
		List<Map<String, Integer>> resultList = new ArrayList<Map<String, Integer>>();

		for (String s : list) {
			switch ((StringSimilarityFunctions) this.simFunc) {
			case BIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 2));
				break;
			case TRIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 3));
				break;
			case TRIGRAM_DICE_FILLING:
				resultList.add(StringDecomposer
						.fillAndDecomposeOverlappingLength(s, 3));
				break;
			case FULLWORD_DICE:
				Map<String, Integer> wordMap;
				if (resultList.isEmpty()) {
					wordMap = new HashMap<String, Integer>();
				} else {
					wordMap = resultList.get(0);
				}
				Integer cnt = wordMap.get(s);
				if (cnt != null) {
					wordMap.put(s, cnt++);
				} else {
					wordMap.put(s, 1);
				}
				resultList.clear();
				resultList.add(wordMap);
				break;
			default:
				throw new OperatorException(
						"Could not resolve the string decomposer function.");
			}
		}
		return resultList;
	}

	private MatchResult computeMatch(
			List<Map<String, Integer>> domainAttrValueList,
			List<Map<String, Integer>> rangeAttrValueList)
			throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();

		for (Map<String, Integer> domainMap : domainAttrValueList)
			for (Map<String, Integer> rangeMap : rangeAttrValueList)
				confidenceList.add(super.simFunc.computeConfidence(domainMap,
						rangeMap));

		return new MatchResult(
				super.aggFunc.aggregateFloatList(confidenceList),
				confidenceList.size(), "N/A");
	}

	private Map<String,Integer> generateNGrams(String str, int gramlength) {
		str = "$$" + str + "$$";
		return StringDecomposer.decomposeOverlappingLength(str, gramlength);
	}

	private int[][] generateNGramId(Map<String, Integer> ngrams) {

		int[][] result = new int[2][];
		result[0] = new int[ngrams.size()];
		result[1] = new int[ngrams.size()];
		
		
	    TreeMap<Integer,Integer> tmpResult = new TreeMap<Integer,Integer>();
	    
	    for (String ngram : ngrams.keySet())
	    {
	        tmpResult.put(ngram.hashCode(), ngrams.get(ngram));
	    }
	     
	    int i=0;
	    for (Entry<Integer,Integer> entry : tmpResult.entrySet()) {
	    	result[0][i] = entry.getKey();
	    	result[1][i++] = entry.getValue();
	    }
	    
	    return result;
	}

}
