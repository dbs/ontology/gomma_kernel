/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.OptimizedSimilarityFunctions;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.string.StringDecomposer;

public class FastListTrigramMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public FastListTrigramMatcher() {
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2
				.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
		try {//Zerlegung in Trigramme fuer Domain und Range fuer jeden Multi-Value-Attribut-Wert		
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			ArrayList<Integer> domainObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[][]>> domainGrams = new ArrayList<ArrayList<int[][]>>();
			for (Obj domainObj : domain) {
				List<String> domainObjStrings = super.getAttributeValues(
						domainObj, true);
				ArrayList<int[][]> domainObjGrams = new ArrayList<int[][]>();
				for (String domainObjString : domainObjStrings) {
					int[][] domainObjGramsID = generateNGramId(this.generateNGrams(domainObjString, 3));					
					domainObjGrams.add(domainObjGramsID);
				}
				domainObjIDs.add(domainObj.getID());
				domainGrams.add(domainObjGrams);
			}
			ArrayList<Integer> rangeObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[][]>> rangeGrams = new ArrayList<ArrayList<int[][]>>();
			for (Obj rangeObj : range) {
				List<String> rangeObjStrings = super.getAttributeValues(
						rangeObj, false);
				ArrayList<int[][]> rangeObjGrams = new ArrayList<int[][]>();
				for (String rangeObjString : rangeObjStrings) {
					int[][] rangeObjGramsID = generateNGramId(this.generateNGrams(rangeObjString, 3));					
					rangeObjGrams.add(rangeObjGramsID);
				}
				rangeObjIDs.add(rangeObj.getID());
				rangeGrams.add(rangeObjGrams);
			}
			// Vergleich der trigramme fuer multi-value-attribute
			for (int i = 0; i < domainObjIDs.size(); i++) {
				for (int j = 0; j < rangeObjIDs.size(); j++) {
					List<Float> confidenceList = new ArrayList<Float>();
					for (int[][] domainObjGrams : domainGrams.get(i)) {
						for (int[][] rangeObjGrams : rangeGrams.get(j)) {
							// if (2*Math.min(domainObjGrams.length,
							// rangeObjGrams.length)+1>this.simThreshold*(domainObjGrams.length+rangeObjGrams.length))
							confidenceList.add(this.computeNGramSimilarity(domainObjGrams, rangeObjGrams));
						}
					}
					result = super
							.addCorrespondence(
									domain.getObj(domainObjIDs.get(i)),
									range.getObj(rangeObjIDs.get(j)),
									new MatchResult(
											super.aggFunc
													.aggregateFloatList(confidenceList),
											confidenceList.size(), "N/A"),
									result, true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	private List<Map<String, Integer>> createStringPortion(List<String> list)
			throws OperatorException {
		List<Map<String, Integer>> resultList = new ArrayList<Map<String, Integer>>();

		for (String s : list) {
			switch ((StringSimilarityFunctions) this.simFunc) {
			case BIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 2));
				break;
			case TRIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 3));
				break;
			case TRIGRAM_DICE_FILLING:
				resultList.add(StringDecomposer
						.fillAndDecomposeOverlappingLength(s, 3));
				break;
			case FULLWORD_DICE:
				Map<String, Integer> wordMap;
				if (resultList.isEmpty()) {
					wordMap = new HashMap<String, Integer>();
				} else {
					wordMap = resultList.get(0);
				}
				Integer cnt = wordMap.get(s);
				if (cnt != null) {
					wordMap.put(s, cnt++);
				} else {
					wordMap.put(s, 1);
				}
				resultList.clear();
				resultList.add(wordMap);
				break;
			default:
				throw new OperatorException(
						"Could not resolve the string decomposer function.");
			}
		}
		return resultList;
	}

	private MatchResult computeMatch(
			List<Map<String, Integer>> domainAttrValueList,
			List<Map<String, Integer>> rangeAttrValueList)
			throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();

		for (Map<String, Integer> domainMap : domainAttrValueList)
			for (Map<String, Integer> rangeMap : rangeAttrValueList)
				confidenceList.add(super.simFunc.computeConfidence(domainMap,
						rangeMap));

		return new MatchResult(
				super.aggFunc.aggregateFloatList(confidenceList),
				confidenceList.size(), "N/A");
	}

	private float computeNGramSimilarity(int[][]  leftMap, int[][]  rightMap)
	{
		int gramsLeft= 0, gramsRight=0, gramsCommon= 0;
				
		int[] leftGrams= leftMap[0];
		int[] rightGrams= rightMap[0];
		
		int[] leftOccs= leftMap[1];
		int[] rightOccs= rightMap[1];
		
		
		int leftIndex=0, leftGram=-1;
		int rightIndex= 0, rightGram= -1;
		int leftOccTmp = 0, rightOccTmp = 0;
		
		while(leftIndex<leftGrams.length && rightIndex<rightGrams.length)
		{
			leftGram= leftGrams[leftIndex];
			rightGram= rightGrams[rightIndex];
			
			if(leftGram==rightGram)
			{
				leftOccTmp = leftOccs[leftIndex++];
				gramsLeft+= leftOccTmp;
				
				rightOccTmp= rightOccs[rightIndex++];
				gramsRight+= rightOccTmp;
				
				gramsCommon+= Math.min(leftOccTmp, rightOccTmp);
			}
			else if(leftGram<rightGram)
			{
				gramsLeft+= leftOccs[leftIndex++];
			}
			else
			{
				gramsRight+= rightOccs[rightIndex++];
			}
		}
		
		while(leftIndex<leftGrams.length)
		{
			gramsLeft+= leftOccs[leftIndex++];
		}
		
		while(rightIndex<rightGrams.length)
		{
			gramsRight+= rightOccs[rightIndex++];
		}
		
		return 2f * gramsCommon / (gramsLeft+gramsRight); // Dice-Coefficient
	}



	
	
	private Map<String,Integer> generateNGrams(String str, int gramlength) {

		if (this.simFunc==OptimizedSimilarityFunctions.OPTIMIZED_LIST_TRIGRAM_FILLING) {
			str = "$$" + str + "$$";
		}
		return StringDecomposer.decomposeOverlappingLength(str, gramlength);
	}

	private int[][] generateNGramId(

			Map<String, Integer> ngrams) {

		int[][] result = new int[2][];
		result[0] = new int[ngrams.size()];
		result[1] = new int[ngrams.size()];
		
		
	    TreeMap<Integer,Integer> tmpResult = new TreeMap<Integer,Integer>();
	    
	    for (String ngram : ngrams.keySet())
	    {
	        tmpResult.put(ngram.hashCode(), ngrams.get(ngram));
	    }
	     
	    int i=0;
	    for (Entry<Integer,Integer> entry : tmpResult.entrySet()) {
	    	result[0][i] = entry.getKey();
	    	result[1][i++] = entry.getValue();
	    }
	    
	    return result;
	}

	
}
