package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

public class ListTrigramThread extends Thread {

	public ObjCorrespondenceSet result;
	private ArrayList<Integer> domainObjIDs;
	private ArrayList<ArrayList<int[][]>> domainGrams;
	private ObjSet domain;
	private ArrayList<Integer> rangeObjIDs;
	private ArrayList<ArrayList<int[][]>> rangeGrams;
	private ObjSet range;
	private ThreadedListTrigramMatcher matcher;
	
	public ListTrigramThread(ArrayList<Integer> domainObjIDs, ArrayList<ArrayList<int[][]>> domainGrams, ObjSet domain, ArrayList<Integer> rangeObjIDs, ArrayList<ArrayList<int[][]>> rangeGrams, ObjSet range, ThreadedListTrigramMatcher matcher) {
		this.domainObjIDs = domainObjIDs;
		this.domainGrams = domainGrams;
		this.domain = domain;
		this.rangeObjIDs = rangeObjIDs;
		this.rangeGrams = rangeGrams;
		this.range = range;
		this.matcher = matcher;
	}

	public void run() {
		result = new ObjCorrespondenceSet();
		for (int i = 0; i < domainObjIDs.size(); i++) {
			for (int j = 0; j < rangeObjIDs.size(); j++) {
				List<Float> confidenceList = new ArrayList<Float>();
				for (int[][] domainObjGrams : domainGrams.get(i)) {
					for (int[][] rangeObjGrams : rangeGrams.get(j)) {
						confidenceList.add(this.computeNGramSimilarity(domainObjGrams, rangeObjGrams));
					}
				}
				result = this
						.addCorrespondence(
								domain.getObj(domainObjIDs.get(i)),
								range.getObj(rangeObjIDs.get(j)),
								new MatchResult(
										matcher.getAggregationFunction()
												.aggregateFloatList(confidenceList),
										confidenceList.size(), "N/A"),
								result, true);
			}
		}
	}
	
	private float computeNGramSimilarity(int[][]  leftMap, int[][]  rightMap)
	{
		int gramsLeft= 0, gramsRight=0, gramsCommon= 0;
		
		int[] leftGrams= leftMap[0];
		int[] rightGrams= rightMap[0];
		
		int[] leftOccs= leftMap[1];
		int[] rightOccs= rightMap[1];
		
		
		int leftIndex=0, leftGram=-1;
		int rightIndex= 0, rightGram= -1;
		int leftOccTmp = 0, rightOccTmp = 0;
		
		while(leftIndex<leftGrams.length && rightIndex<rightGrams.length)
		{
			leftGram= leftGrams[leftIndex];
			rightGram= rightGrams[rightIndex];
			
			if(leftGram==rightGram)
			{
				leftOccTmp = leftOccs[leftIndex++];
				gramsLeft+= leftOccTmp;
				
				rightOccTmp= rightOccs[rightIndex++];
				gramsRight+= rightOccTmp;
				
				gramsCommon+= Math.min(leftOccTmp, rightOccTmp);
			}
			else if(leftGram<rightGram)
			{
				gramsLeft+= leftOccs[leftIndex++];
			}
			else
			{
				gramsRight+= rightOccs[rightIndex++];
			}
		}
		
		while(leftIndex<leftGrams.length)
		{
			gramsLeft+= leftOccs[leftIndex++];
		}
		
		while(rightIndex<rightGrams.length)
		{
			gramsRight+= rightOccs[rightIndex++];
		}
		float sim = 2f * gramsCommon / (gramsLeft+gramsRight);
		return sim; // Dice-Coefficient
	}
	
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) {
		
		if (checkThreshold && matchResult.getConfidence()<this.matcher.getSimilarityThreshold()) return set;
			
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		try {
			set.addCorrespondence(cor);
		} catch (WrongSourceException e) {
			e.printStackTrace();
		}
		
		return set;
	}
}
