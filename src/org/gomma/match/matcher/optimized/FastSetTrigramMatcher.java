/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/10/13
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.optimized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.match.metrics.OptimizedSimilarityFunctions;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.string.StringDecomposer;

public class FastSetTrigramMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public FastSetTrigramMatcher() {
	}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1,
			SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			SourceVersionStructure s1, SourceVersionStructure s2)
			throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2
				.getAllObjects());
	}

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range)
			throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			ArrayList<Integer> domainObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[]>> domainGrams = new ArrayList<ArrayList<int[]>>();
			for (Obj domainObj : domain) {
				List<String> domainObjStrings = super.getAttributeValues(
						domainObj, true);
				ArrayList<int[]> domainObjGrams = new ArrayList<int[]>();
				for (String domainObjString : domainObjStrings) {
					int[] domainObjGramsID = generateNGramId(this
							.generateNGrams(domainObjString, 3));
					Arrays.sort(domainObjGramsID);
					domainObjGrams.add(domainObjGramsID);
				}
				domainObjIDs.add(domainObj.getID());
				domainGrams.add(domainObjGrams);
			}
			ArrayList<Integer> rangeObjIDs = new ArrayList<Integer>();
			ArrayList<ArrayList<int[]>> rangeGrams = new ArrayList<ArrayList<int[]>>();
			for (Obj rangeObj : range) {
				List<String> rangeObjStrings = super.getAttributeValues(
						rangeObj, false);
				ArrayList<int[]> rangeObjGrams = new ArrayList<int[]>();
				for (String rangeObjString : rangeObjStrings) {
					int[] rangeObjGramsID = generateNGramId(this
							.generateNGrams(rangeObjString, 3));
					Arrays.sort(rangeObjGramsID);
					rangeObjGrams.add(rangeObjGramsID);
				}
				rangeObjIDs.add(rangeObj.getID());
				rangeGrams.add(rangeObjGrams);
			}
			// Vergleich der trigramme fuer multi-value-attribute
			for (int i = 0; i < domainObjIDs.size(); i++) {
				for (int j = 0; j < rangeObjIDs.size(); j++) {
					List<Float> confidenceList = new ArrayList<Float>();
					for (int[] domainObjGrams : domainGrams.get(i)) {
						for (int[] rangeObjGrams : rangeGrams.get(j)) {
							// if (2*Math.min(domainObjGrams.length,
							// rangeObjGrams.length)+1>this.simThreshold*(domainObjGrams.length+rangeObjGrams.length))
							confidenceList.add(this
									.computeSortedNGramSimilarity(
											domainObjGrams, rangeObjGrams));
						}
					}
					result = super
							.addCorrespondence(
									domain.getObj(domainObjIDs.get(i)),
									range.getObj(rangeObjIDs.get(j)),
									new MatchResult(
											super.aggFunc
													.aggregateFloatList(confidenceList),
											confidenceList.size(), "N/A"),
									result, true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(
			ObjSet domain, ObjSet range) throws OperatorException {
		List<Map<String, Integer>> domainAttrValueList, rangeAttrValueList;

		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();

			if (super.initialMap.size() < domain.size() * range.size()) {
				Obj domainObj, rangeObj;

				for (ObjCorrespondence initCorr : super.initialMap
						.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range
							.contains(initCorr.getRangeObjID())))
						continue;

					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());

					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					rangeAttrValueList = this.createStringPortion(super
							.getAttributeValues(rangeObj, false));

					result = this.addCorrespondence(domainObj, rangeObj, this
							.computeMatch(domainAttrValueList,
									rangeAttrValueList), result, true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = this.createStringPortion(super
							.getAttributeValues(domainObj, true));

					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj
								.getID(), rangeObj.getID()))
							continue;

						rangeAttrValueList = this.createStringPortion(super
								.getAttributeValues(rangeObj, false));

						result = this.addCorrespondence(domainObj, rangeObj,
								this.computeMatch(domainAttrValueList,
										rangeAttrValueList), result, true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}

	private List<Map<String, Integer>> createStringPortion(List<String> list)
			throws OperatorException {
		List<Map<String, Integer>> resultList = new ArrayList<Map<String, Integer>>();

		for (String s : list) {
			switch ((StringSimilarityFunctions) this.simFunc) {
			case BIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 2));
				break;
			case TRIGRAM_DICE:
				resultList.add(StringDecomposer
						.decomposeOverlappingLength(s, 3));
				break;
			case TRIGRAM_DICE_FILLING:
				resultList.add(StringDecomposer
						.fillAndDecomposeOverlappingLength(s, 3));
				break;
			case FULLWORD_DICE:
				Map<String, Integer> wordMap;
				if (resultList.isEmpty()) {
					wordMap = new HashMap<String, Integer>();
				} else {
					wordMap = resultList.get(0);
				}
				Integer cnt = wordMap.get(s);
				if (cnt != null) {
					wordMap.put(s, cnt++);
				} else {
					wordMap.put(s, 1);
				}
				resultList.clear();
				resultList.add(wordMap);
				break;
			default:
				throw new OperatorException(
						"Could not resolve the string decomposer function.");
			}
		}
		return resultList;
	}

	private MatchResult computeMatch(
			List<Map<String, Integer>> domainAttrValueList,
			List<Map<String, Integer>> rangeAttrValueList)
			throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();

		for (Map<String, Integer> domainMap : domainAttrValueList)
			for (Map<String, Integer> rangeMap : rangeAttrValueList)
				confidenceList.add(super.simFunc.computeConfidence(domainMap,
						rangeMap));

		return new MatchResult(
				super.aggFunc.aggregateFloatList(confidenceList),
				confidenceList.size(), "N/A");
	}

	private float computeSortedNGramSimilarity(int[] gramsId1, int[] gramsId2) {
		// merge-sort-aehnlicher Vergleich der Trigramlisten fuer
		// Overlapberechnung (Dice)
		int count = 0;
		int left = 0, right = 0;
		while (left < gramsId1.length && right < gramsId2.length) {
			if (gramsId1[left] == gramsId2[right]) {
				count++;
				left++;
				right++;
			} else if (gramsId1[left] < gramsId2[right]) {
				left++;
			} else {
				right++;
			}
		}

		float sim = (float) 2 * count / (gramsId1.length + gramsId2.length); // Dice-Coefficient
		return sim;
	}

	private float computeNGramSimilarity(int[] gramsId1, int[] gramsId2) {
		int count = 0;
		for (int i = 0; i < gramsId1.length; i++)
			for (int j = 0; j < gramsId2.length; j++) {
				if (gramsId1[i] == gramsId2[j]) {
					count++;
					break;
				}
			}
		
		float sim = (float) 2 * count / (gramsId1.length + gramsId2.length); // Dice-Coefficient
		return sim;
	}

	private String[] generateNGrams(String str, int gramlength) {
		if (this.simFunc==OptimizedSimilarityFunctions.OPTIMIZED_SET_TRIGRAM_FILLING) {
			str = "$$" + str + "$$";
		}
		Map<String, Integer> grams = StringDecomposer
				.decomposeOverlappingLength(str, gramlength);
		return (String[]) grams.keySet().toArray(new String[0]);
	}

	private int[] generateNGramId(String[] ngram) {

		int[] result = new int[ngram.length];
		for (int count = 0; count < ngram.length; count++) {
			result[count] = ngram[count].hashCode();
		}
		return result;
	}

}
