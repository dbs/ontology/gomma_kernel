/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.name;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements a matcher to match objects w.r.t. 
 * structural information, especially paths.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class PathMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public PathMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 * TODO: NEEDS TO BE IMPLEMENTED!!!
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		return result;
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		return result;
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		// TODO: needs to be implemented
		return new ObjCorrespondenceSet();
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		// TODO: needs to be implemented
		return new ObjCorrespondenceSet();
	}
}
