/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.name;

import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements a matcher to match objects taking 
 * character-based values into account.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class StringCompleteMatcher extends AbstractMatcher {
	
	/**
	 * The constructor initializes the class.
	 */
	public StringCompleteMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		List<String> domainAttrValueList, rangeAttrValueList;
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : domain) {
				domainAttrValueList = super.getAttributeValues(domainObj,true);
				for (Obj rangeObj : range) { 
					rangeAttrValueList = super.getAttributeValues(rangeObj,false);
					
					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(domainAttrValueList,rangeAttrValueList),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		List<String> domainAttrValueList, rangeAttrValueList;
		
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			if (super.initialMap.size()<domain.size()*range.size()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) &&
							range.contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());
					
					domainAttrValueList = super.getAttributeValues(domainObj,true);
					rangeAttrValueList = super.getAttributeValues(rangeObj,false);
					
					result = this.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(domainAttrValueList,rangeAttrValueList),result,true);
				}
			} else {
				for (Obj domainObj : domain) {
					domainAttrValueList = super.getAttributeValues(domainObj,true);
					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						rangeAttrValueList = super.getAttributeValues(rangeObj,false);
						
						result = this.addCorrespondence(domainObj,rangeObj,
									this.computeMatch(domainAttrValueList,rangeAttrValueList),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private MatchResult computeMatch(List<String> domainAttrValueList, List<String> rangeAttrValueList) throws OperatorException {
		List<Float> confidenceList  = new ArrayList<Float>();
		
		for (String domainStr : domainAttrValueList) 
			for (String rangeStr : rangeAttrValueList) 
				confidenceList.add(super.simFunc.computeConfidence(domainStr,rangeStr));
				
		return new MatchResult(super.aggFunc.aggregateFloatList(confidenceList),confidenceList.size(),"N/A");
	}
}