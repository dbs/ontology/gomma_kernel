/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.date;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.date.DateHandler;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements a matcher to match objects w.r.t. 
 * single calendar dates.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SingleDateMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public SingleDateMatcher() {}

	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	
	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}
	

	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		List<SimplifiedGregorianCalendar> t1List=null, t2List=null;
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			for (Obj domainObj : domain) {
				try {
					t1List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,false,false,true));
				} catch (ParseException e) { continue; }
				
				for (Obj rangeObj : range) {
					try {
						t2List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,false,false,false));
					} catch (ParseException e) { continue; }
					
					result = this.addCorrespondence(domainObj,rangeObj,
								this.computeMatch(t1List,t2List),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		List<SimplifiedGregorianCalendar> t1List=null, t2List=null;
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
			if (super.initialMap.size()<domain.size()*range.size()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) &&
							range.contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());
					
					try {
						t1List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,false,false,true));
						t2List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,false,false,false));
					} catch (ParseException e) { continue; }
					
					result = this.addCorrespondence(domainObj,rangeObj,
							this.computeMatch(t1List,t2List),result,true);
				}
			} else {
				for (Obj domainObj : domain) {
					try {
						t1List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,false,false,true));
					} catch (ParseException e) { continue; }
					
					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						
						try {
							t2List = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,false,false,false));
						} catch (ParseException e) { continue; }
						
						result = this.addCorrespondence(domainObj,rangeObj,
									this.computeMatch(t1List,t2List),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private MatchResult computeMatch(List<SimplifiedGregorianCalendar> t1List, List<SimplifiedGregorianCalendar> t2List) throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();
		
		for (SimplifiedGregorianCalendar t1 : t1List) {
			for (SimplifiedGregorianCalendar t2 : t2List) 
				confidenceList.add(this.simFunc.computeConfidence(t1,t1,t2,t2));
		}
		return new MatchResult(super.aggFunc.aggregateFloatList(confidenceList),1,"N/A");
	}
}
