/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher.date;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.matcher.AbstractMatcher;
import org.gomma.match.matcher.MatchResult;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.date.DateHandler;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements a matcher to match objects taking
 * calendar date intervals into account.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DateIntervalMatcher extends AbstractMatcher {

	/**
	 * The constructor initializes the class.
	 */
	public DateIntervalMatcher() {}
	
	@Override
	/**
	 * @see AbstractMatcher#createMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMapping(s1.getAllObjects(), s2.getAllObjects());
	}

	@Override
	/**
	 * @see AbstractMatcher#createMappingUsingInitialMapping(SourceVersionStructure, SourceVersionStructure)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return this.createMappingUsingInitialMapping(s1.getAllObjects(), s2.getAllObjects());
	}


	/**
	 * @see AbstractMatcher#createMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			List<SimplifiedGregorianCalendar> t1StartList=null, t1EndList=null, t2StartList=null, t2EndList=null;
			
			for (Obj domainObj: domain) {
				try {
					t1StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,true,true));
					t1EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,false,true));
				} catch (ParseException e) { continue; }
				
				for (Obj rangeObj : range) {
					try {
						t2StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,true,false));
						t2EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,false,false));
					} catch (ParseException e) { continue; }
					
					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeConfidence(t1StartList,t1EndList,t2StartList,t2EndList),result,true);
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * @see AbstractMatcher#createMappingWithInitialMapping(ObjSet, ObjSet)
	 */
	protected ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException {
		try {
			ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			List<SimplifiedGregorianCalendar> t1StartList=null, t1EndList=null, t2StartList=null, t2EndList=null;
			
			if (super.initialMap.size()<domain.size()*range.size()) {
				Obj domainObj, rangeObj;
				
				for (ObjCorrespondence initCorr : super.initialMap.getCollection()) {
					if (!(domain.contains(initCorr.getDomainObjID()) && range.contains(initCorr.getRangeObjID()))) continue;
					
					domainObj = domain.getObj(initCorr.getDomainObjID());
					rangeObj = range.getObj(initCorr.getRangeObjID());
					
					try {
						t1StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,true,true));
						t1EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,false,true));
						t2StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,true,false));
						t2EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,false,false));
					} catch (ParseException e) { continue; }
					
					result = super.addCorrespondence(domainObj,rangeObj,
							this.computeConfidence(t1StartList,t1EndList,t2StartList,t2EndList),result,true);
				}
			} else {
				for (Obj domainObj : domain) {
					try {
						t1StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,true,true));
						t1EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(domainObj,true,false,true));
					} catch (ParseException e) { continue; }
					
					for (Obj rangeObj : range) {
						if (!super.initialMap.containsCorrespondence(domainObj.getID(),rangeObj.getID())) continue;
						
						try {
							t2StartList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,true,false));
							t2EndList = DateHandler.toSimplifiedGregorianCalendarList(super.getDateAttributeValues(rangeObj,true,false,false));
						} catch (ParseException e) { continue; }
						
						result = super.addCorrespondence(domainObj,rangeObj,
								this.computeConfidence(t1StartList,t1EndList,t2StartList,t2EndList),result,true);
					}
				}
			}
			return result;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	private MatchResult computeConfidence(List<SimplifiedGregorianCalendar> t1StartList, List<SimplifiedGregorianCalendar> t1EndList, List<SimplifiedGregorianCalendar> t2StartList, List<SimplifiedGregorianCalendar> t2EndList) throws OperatorException {
		List<Float> confidenceList = new ArrayList<Float>();
		
		for (SimplifiedGregorianCalendar t1Start : t1StartList) {
			for (SimplifiedGregorianCalendar t1End : t1EndList) {
				for (SimplifiedGregorianCalendar t2Start : t2StartList) {
					for (SimplifiedGregorianCalendar t2End : t2EndList)
						confidenceList.add(super.simFunc.computeConfidence(t1Start,t1End,t2Start,t2End));
				}
			}
		}
		
		return new MatchResult(super.aggFunc.aggregateFloatList(confidenceList),1,"N/A");
	}
}