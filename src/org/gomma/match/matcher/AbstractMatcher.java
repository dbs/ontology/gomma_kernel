/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.match.matcher;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

/**
 * The abstract class implements some general methods 
 * setting and getting matcher properties.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public abstract class AbstractMatcher implements Matcher {

	/**
	 * The item represents the similarity function.
	 */
	protected SimilarityFunction simFunc;
	
	/**
	 * The item represents the aggregation function. 
	 */
	protected AggregationFunction aggFunc;
	
	/**
	 * The item represents the similarity threshold.
	 */
	protected float simThreshold;
	
	/**
	 * The item represents the set of match properties. 
	 */
	protected MatchProperties matchProps;
	
	/**
	 * The item represents the initial mapping that can be used by the matcher.
	 */
	protected ObjCorrespondenceSet initialMap;	
	
	/**
	 * @see Matcher#match(SourceVersionStructure, SourceVersionStructure)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		if (this.initialMap.size()==0) return createMapping(s1,s2);
		else return createMappingUsingInitialMapping(s1,s2);
	}
	
	/**
	 * @see Matcher#match(ObjSet, ObjSet)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range) throws OperatorException {
		if (this.initialMap.size()==0) return createMapping(domain,range);
		else return createMappingUsingInitialMapping(domain,range);
	}
	
	
	/**
	 * The method creates a new mapping taking the objects of the two given 
	 * source versions and their structure into account. The mapping creation 
	 * is independently from a given initial mapping. The method is abstract 
	 * and needs to be implemented by each matcher.
	 * @param s1 source version structure 1
	 * @param s2 source version structure 2
	 * @return set of object correspondences between objects of s1 and s2
	 * @throws OperatorException
	 */
	protected abstract ObjCorrespondenceSet createMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException;
	
	/**
	 * The method creates a new mapping taking the objects of the two given 
	 * source versions and their structure into account. The mapping creation 
	 * depends on a given initial mapping, i.e., only the object correspondences
	 * of the initial mapping are used to compute the confidence. Therefore,
	 * the generated mapping consists of the same set or a portion of object 
	 * correspondences of the initial mapping. The method is abstract 
	 * and needs to be implemented by each matcher.
	 * @param s1 source version structure 1
	 * @param s2 source version structure 2
	 * @return set of object correspondences between objects of s1 and s2
	 * @throws OperatorException
	 */
	protected abstract ObjCorrespondenceSet createMappingUsingInitialMapping(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException;
	
	
	protected abstract ObjCorrespondenceSet createMapping(ObjSet domain, ObjSet range) throws OperatorException;
	
	protected abstract ObjCorrespondenceSet createMappingUsingInitialMapping(ObjSet domain, ObjSet range) throws OperatorException;
	
	
	/**
	 * @see Matcher#setSimilarityFunction(SimilarityFunction)
	 */
	public void setSimilarityFunction(SimilarityFunction simFunc) {
		this.simFunc=simFunc;
	}
	
	/**
	 * @see Matcher#setAggregationFunction(AggregationFunction)
	 */
	public void setAggregationFunction(AggregationFunction aggFunc) {
		this.aggFunc=aggFunc;
	}
	
	/**
	 * @see Matcher#setSimilarityThreshold(float)
	 */
	public void setSimilarityThreshold(float simThreshold) {
		this.simThreshold=simThreshold;
	}
	
	/**
	 * @see Matcher#setMatchProperties(MatchProperties)
	 */
	public void setMatchProperties(MatchProperties props) {
		this.matchProps=props;
	}
	/**
	 * @see Matcher#getSimilarityFunction()
	 */
	public SimilarityFunction getSimilarityFunction() {
		return this.simFunc;
	}
	
	/**
	 * @see Matcher#getAggregationFunction()
	 */
	public AggregationFunction getAggregationFunction() {
		return this.aggFunc;
	}
	
	/**
	 * @see Matcher#getSimilarityThreshold()
	 */
	public float getSimilarityThreshold() {
		return this.simThreshold;
	}
	
	/**
	 * @see Matcher#getMatchProperties()
	 */
	public MatchProperties getMatchProperties() {
		return this.matchProps;
	}
	/**
	 * @see Matcher#setInitialMapping(ObjCorrespondenceSet)
	 */
	public void setInitialMapping(ObjCorrespondenceSet initialMap) {
		this.initialMap=initialMap;
	}
	
	/**
	 * @see Matcher#getInitialMapping()
	 */
	public ObjCorrespondenceSet getInitialMapping() {
		return this.initialMap;
	}
	
	/**
	 * The method returns a list of attribute values of 
	 * the given object. The attribute names for which 
	 * the values will be captured are part of the match
	 * properties.  
	 * @param obj object from which attribute values will be returned
	 * @param isDomain true if the attribute is part of the
	 * mapping domain; otherwise it is false
	 * @return list of attribute values
	 */
	protected List<String> getAttributeValues(Obj obj, boolean isDomain) {
		String sourceVersionName=null;
		List<String> attNameList = null;
		
		if (this.matchProps!=null && this.matchProps.size()!=0)  {
			
			if (isDomain?this.matchProps.containsDomainSourceVersion():this.matchProps.containsRangeSourceVersion())
				sourceVersionName = isDomain?
						this.matchProps.getDomainSourceVersionName():
						this.matchProps.getRangeSourceVersionName();
			if (isDomain?this.matchProps.containsDomainAttributeNames():this.matchProps.containsRangeAttributeNames()) 
				attNameList = isDomain?
						this.matchProps.getDomainAttributeNameList():
						this.matchProps.getRangeAttributeNameList();	
		}

		return this.extractAttributeValues(obj, sourceVersionName, attNameList);
	}
	
	/**
	 * The method returns a list of calendar dates extracted from the given 
	 * object. The attributes for which the values will be captured are
	 * defined as match properties. 
	 * @param obj object from which the calendar dates will be extracted
	 * @param isDateInterval true if the parameter value is a date interval;
	 * otherwise it is false
	 * @param isStartDate true if the extracted calendar dates are start dates 
	 * of an interval
	 * @param isDomain true if the attribute is part of the
	 * mapping domain; otherwise it is false
	 * @return list of calendar dates
	 * @throws ParseException
	 */
	protected List<String> getDateAttributeValues(Obj obj, boolean isDateInterval, boolean isStartDate, boolean isDomain) {
		String sourceVersionName=null, attName=null;
		
		if (this.matchProps!=null && this.matchProps.size()!=0) {
			
			if (isDomain?this.matchProps.containsDomainSourceVersion():this.matchProps.containsRangeSourceVersion())
				sourceVersionName = isDomain?
						this.matchProps.getDomainSourceVersionName():
						this.matchProps.getRangeSourceVersionName();
					
			if (!isDateInterval && (
					isDomain?this.matchProps.containsDomainAttributeNames():this.matchProps.containsRangeAttributeNames())) 
				attName = isDomain?
						this.matchProps.getDomainAttributeNameList().iterator().next():
						this.matchProps.getRangeAttributeNameList().iterator().next();
			
			else {
				if (isDateInterval && isStartDate && (
						isDomain?
							this.matchProps.containsDomainStartDateAttributeName():
							this.matchProps.containsRangeStartDateAttributeName())) 
					attName = isDomain?
							this.matchProps.getDomainStartDateAttributeName():
							this.matchProps.getRangeStartDateAttributeName();
				
				if (isDateInterval && !isStartDate && (
						isDomain?
							this.matchProps.containsDomainEndDateAttributeName():
							this.matchProps.containsRangeEndDateAttributeName())) 
					attName = isDomain?
							this.matchProps.getDomainEndDateAttributeName():
							this.matchProps.getRangeEndDateAttributeName();
			}
		}
		
		List<String> list = new ArrayList<String>();
		list.add(attName);
		return this.extractAttributeValues(obj, sourceVersionName, list);
	}

	
	
	
	
	/**
	 * The method creates a correspondences between the objects identified by the object 
	 * identifiers objID1 and objID2 of two given source versions s1 and s2. The object
	 * correspondence is only creates if the computed confidence (@see MatchResult) exceeds
	 * the confidence threshold. At the end, the new object correspondence is added to the 
	 * given set of object correspondences which then returned. 
	 * @param s1 source version structure 1
	 * @param s2 source version structure 2
	 * @param objID1 object identifier 1 of source version structure 1
	 * @param objID2 object identifier 2 of source version structure 2
	 * @param matchResult match result comparing object 1 and 2
	 * @param objCorrs set of currently managed object correspondences to which the new
	 * generated object correspondence can be added
	 * @return set of object correspondences
	 * @throws WrongSourceException
	 * @throws OperatorException
	 */
	protected ObjCorrespondenceSet addCorrespondence(Obj domainObj, Obj rangeObj, MatchResult matchResult, ObjCorrespondenceSet set, boolean checkThreshold) throws WrongSourceException, OperatorException {
		
		if (checkThreshold && matchResult.getConfidence()<this.simThreshold) return set;
			
		ObjCorrespondence cor = new ObjCorrespondence.Builder(
				domainObj.getID(),domainObj.getLDSID(),
				rangeObj.getID(),rangeObj.getLDSID())
					.domainAccessionNumber(domainObj.getAccessionNumber())
					.rangeAccessionNumber(rangeObj.getAccessionNumber())
					.confidence(matchResult.getConfidence())
					.support(matchResult.getSupport())
					.numberOfUserChecks(0)
					.typeName(matchResult.getType())
					.build();
		for (SourceVersion v : domainObj.getSourceVersionSet()) cor.addDomainSourceVersionID(v.getID());
		for (SourceVersion v : rangeObj.getSourceVersionSet()) cor.addRangeSourceVersionID(v.getID());
		set.addCorrespondence(cor);
		
		return set;
	}
	
	
	
	private List<String> extractAttributeValues(Obj obj, String sourceVersionName, List<String> attNameList) {
		List<String> valueList = new ArrayList<String>();
		AttributeSet attSet;
		
		if (sourceVersionName!=null && attNameList!=null) {
			
			SourceVersion s = obj.getSourceVersionSet().getSourceVersion(sourceVersionName);
			attSet = obj.getAttributeValues(s.getID()).getAttributeSet();
			
			for (String attName : attNameList) {
				if (!attSet.contains(attName)) continue;
				for (Attribute att : attSet.getAttributeSet(attName)) 
					for (AttributeValueVersion avv : obj.getAttributeValues(s.getID(),att))
						if (!valueList.contains(avv.toString())) 
							valueList.add(avv.toString());
			}
				
		} else
			
		if (attNameList!=null) {
			
			for (SourceVersion s : obj.getSourceVersionSet()) {
				attSet = obj.getAttributeValues(s.getID()).getAttributeSet();
			
				for (String attName : attNameList) {
					if (!attSet.contains(attName)) continue;
					for (Attribute att : attSet.getAttributeSet(attName)) 
						for (AttributeValueVersion avv : obj.getAttributeValues(s.getID(),att))
							if (!valueList.contains(avv.toString())) 
								valueList.add(avv.toString());
				}
			}
		}
		
		return valueList;
	}
}
