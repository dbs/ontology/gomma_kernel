package org.gomma.model;

import java.util.Calendar;

import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents he default end date of a version. The end date can 
 * be used for logical sources, objects, attribute values, and relationships 
 * as well. The class acts as singleton. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class DefaultVersionEndDate {

	/**
	 * The item represents the singleton instance.
	 */
	private static volatile DefaultVersionEndDate singleton = new DefaultVersionEndDate();
	
	/**
	 * The item represents the default end date.
	 */
	private final SimplifiedGregorianCalendar date;
	
	/**
	 * The constructor initializes the class.
	 */
	private DefaultVersionEndDate() {
		this.date = new SimplifiedGregorianCalendar(2099,Calendar.DECEMBER,31);
	}
	
	/**
	 * The static method returns an instance of the class. The class acts as singleton.
	 * @return instance of the class
	 */
	public static DefaultVersionEndDate getInstance() {
		return singleton;
	}
	
	/**
	 * The method returns the default end date of a version.
	 * @return default end date
	 */
	public SimplifiedGregorianCalendar getDate() {
		return this.date;
	}
}
