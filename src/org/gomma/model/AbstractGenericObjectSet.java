package org.gomma.model;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ReferenceOpenHashMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.model.attribute.AbstractAttributeValueVersion;

/**
 * The abstract class implements some general methods for managing generic object sets.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public abstract class AbstractGenericObjectSet<T extends GenericObject> implements GenericObjectSet<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 441969013034951188L;
	
	/**
	 * The item represents the generic object map. 
	 */
	private Map<Integer,T> objectMap;
	
	/**
	 * The enumeration represents the concrete implementation of the 
	 * generic object map that should be used 
	 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
	 * @version: 1.0
	 * @since:   JDK1.5
	 */
	protected enum MapTypes {
		/**
		 * Standard Java HashMap
		 */
		STANDARD,
		
		/**
		 * FastUtils Int2ObjectOpenHashMap
		 */
		INT_OBJECT,
		/**
		 * FastUtils Int2ReferenceOpenHashMap
		 */
		INT_REFERENCE;
	}
	
	/**
	 * The constructor initializes the class.
	 */
	protected AbstractGenericObjectSet() {
		this(MapTypes.STANDARD);
	}
	
	/**
	 * The constructor initializes the class.
	 * @param type map type
	 */
	protected AbstractGenericObjectSet(MapTypes type) {
		this.objectMap = this.factoryMap(type);
	}
	
	/**
	 * @see GenericObjectSet#getIDSet()
	 */
	public Set<Integer> getIDSet() {
		return this.objectMap.keySet();
	}
	
	/**
	 * @see GenericObjectSet#getCollection()
	 */
	public Collection<T> getCollection() {
		return this.objectMap.values();
	}
	
	/**
	 * @see GenericObjectSet#contains(int)
	 */
	public boolean contains(int id) {
		return this.objectMap.containsKey(id);
	}
	
	/**
	 * @see GenericObjectSet#contains(GenericObject)
	 */
	public boolean contains(T element) {
		return this.contains(element.getID());
	}
	
	/**
	 * @see GenericObjectSet#addElement(GenericObject)
	 */
	public void addElement(T element) {
		if (element==null) throw new NullPointerException("Could not resolve the element that should be added.");
		this.objectMap.put(element.getID(),element);
	}
	
	public void removeElement(T element) {
			if (element==null) throw new NullPointerException("Could not resolve the element that should be added.");
			this.objectMap.remove(element.getID());
		}

	/**
	 * @see GenericObjectSet#getElement(int)
	 */
	public T getElement(int id) throws NoSuchElementException {
		if (this.contains(id))
			return this.objectMap.get(id);
		throw new NoSuchElementException("Could not find element for ID '"+id+"'.");
	}
	
	/**
	 * @see GenericObjectSet#size()
	 */
	public int size() {
		return this.objectMap.size();
	}
	
	/**
	 * @see GenericObjectSet#equals(Object)
	 */
	@SuppressWarnings("unchecked")
	public boolean equals(Object o) {
		if (!(o instanceof GenericObjectSet<?>)) return false;
		
		GenericObjectSet<T> set = (GenericObjectSet<T>)o;
		
		if (this.size()!=set.size()) return false;
		
		for (T element : set)
			if (!this.contains(element.getID())) return false;
		
		return true;
	}
	
	public String getSeparatedIDString(String separator) {
		StringBuilder buf = new StringBuilder(this.size()*2);
		
		for (long i : this.objectMap.keySet())
			buf.append(i).append(separator);
		
		return buf.length()>0?buf.substring(0,buf.length()-separator.length()):"";
			
	}
	
	/**
	 * @see Iterable#iterator()
	 */
	public Iterator<T> iterator() {
		return this.objectMap.values().iterator();
	}
	
	/**
	 * The method return an initialized map to store the generic object sets.
	 * The map implementation depends on the specified map type.
	 * @param type type of the map implementation
	 * @return initialized map
	 */
	private Map<Integer,T> factoryMap(MapTypes type) {
		switch (type) {
			case STANDARD: return new HashMap<Integer,T>();
			case INT_OBJECT: return new Int2ObjectOpenHashMap<T>();
			case INT_REFERENCE: return new Int2ReferenceOpenHashMap<T>();
			default: return new HashMap<Integer,T>();
		}
	}
}
