package org.gomma.model;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * The enumeration defines data types of attributes for which 
 * values can be specified and managed within the framework.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public enum DataTypes {

	/**
	 * String attribute values
	 */
	STRING {
		/**
		 * @see DataTypes#getID()
		 */
		public int getID() { return 1; }
		/**
		 * @see Enumeration#toString()
		 */
		public String toString() { return "string"; }
	},
	
	/**
	 * Integer attribute values
	 */
	INTEGER {
		/**
		 * @see DataTypes#getID()
		 */
		public int getID() { return 2; }
		/**
		 * @see Enumeration#toString()
		 */
		public String toString() { return "integer"; }
	},
	
	/**
	 * Float attribute values
	 */
	FLOAT {
		/**
		 * @see DataTypes#getID()
		 */
		public int getID() { return 3; }
		/**
		 * @see Enumeration#toString()
		 */
		public String toString() { return "float"; }
	};
	
	/**
	 * The method returns an internal unique identifier of the element.
	 * @return internal unique identifier
	 */
	public abstract int getID();
	
	/**
	 * The method returns the data type for the specified internal unique data type identifier. 
	 * @param id internal unique data type identifier
	 * @return data type
	 * @throws NoSuchElementException
	 */
	public synchronized static DataTypes resolveDataType(int id) throws NoSuchElementException {
		for (DataTypes type : DataTypes.values()) 
			if (type.getID()==id) return type;
		throw new NoSuchElementException("Could not resolve the data type for the id '"+id+"'.");
	}
}
