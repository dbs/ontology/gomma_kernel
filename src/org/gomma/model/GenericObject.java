package org.gomma.model;

import java.io.Serializable;

/**
 * The interface defines some general methods for managing a generic object.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface GenericObject extends Serializable {

	/**
	 * The method returns the internal unique object identifier.
	 * @return internal unique object identifier
	 */
	public int getID();
	
	/**
	 * The method returns the name of the object.
	 * @return name of the object
	 */
	public String getName();
	
	/**
	 * The method sets the name of the object.
	 * @param name name of the object
	 */
	public void setName(String name);
	
	/**
	 * The method return a string representation of the object
	 * @return string representation of the object
	 * @see Object#toString()
	 */
	public String toString();
	
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o);
}
