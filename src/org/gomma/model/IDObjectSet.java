package org.gomma.model;

/**
 * The class represents a generic set of object which include only an internal unique identifier.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class IDObjectSet extends AbstractGenericObjectSet<IDObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4079835431135595276L;

	/**
	 * The constructor initializes the class.
	 */
	public IDObjectSet() {
		super();
	}
}
