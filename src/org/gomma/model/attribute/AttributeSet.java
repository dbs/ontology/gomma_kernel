package org.gomma.model.attribute;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class AttributeSet extends AbstractGenericObjectSet<Attribute> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7366841702328140833L;

	public AttributeSet() {
		super();
	}
	
	public void addAttribute(Attribute a) {
		if (a==null)
			throw new NullPointerException("Could not resolve the attribute that should be added.");
		super.addElement(a);
	}
	
	public Attribute getAttribute(int id) throws NoSuchElementException {
		if (!super.contains(id))
			throw new NoSuchElementException("Could not find an attribute using the ID '"+id+"'.");
		return super.getElement(id);
	}
	
	public boolean contains(String attName) {
		for (Attribute att : this)
			if (att.getName().equals(attName)) return true;
		return false;
	}
	
	public AttributeSet getAttributeSet(String attName) {
		AttributeSet set = new AttributeSet();
		
		for (Attribute att : this)
			if (att.getName().equals(attName))
				set.addAttribute(att);
		
		return set;
	}
}
