package org.gomma.model.attribute;

import org.gomma.util.date.SimplifiedGregorianCalendar;

public class AttributeValueFactory {

	private AttributeValueFactory() {}
	
	public static AttributeValueInteger getValue(int id, Attribute att, int value) {
		return new AttributeValueInteger.Builder(id,att).value(value).build();
	}
	
	public static AttributeValueFloat getValue(int id, Attribute att, float value) {
		return new AttributeValueFloat.Builder(id,att).value(value).build();
	}
	
	public static AttributeValueString getValue(int id, Attribute att, String value) {
		return new AttributeValueString.Builder(id,att).value(value).build();
	}
	
	public static AttributeValueVersionInteger getValue(int id, Attribute att, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate, int value) {
		return new AttributeValueVersionInteger.Builder(id, att)
				.fromDate(fromDate)
				.toDate(toDate)
				.value(value).build();
	}
	
	public static AttributeValueVersionFloat getValue(int id, Attribute att, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate, float value) {
		return new AttributeValueVersionFloat.Builder(id, att)
				.fromDate(fromDate)
				.toDate(toDate)
				.value(value).build();
	}
	
	public static AttributeValueVersionString getValue(int id, Attribute att, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate, String value) {
		return new AttributeValueVersionString.Builder(id, att)
				.fromDate(fromDate)
				.toDate(toDate)
				.value(value).build();
	}
}
