package org.gomma.model.attribute;

import org.gomma.util.date.SimplifiedGregorianCalendar;


public class AttributeValueVersionString extends AbstractAttributeValueVersion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7892817033862968626L;
	
	private String value;
		
	private AttributeValueVersionString(Builder b) {
		super(b);
		this.value = b.value;
	}
		
	public String getValue() {
		return this.value;
	}
		
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueVersionString)) return false;
		AttributeValueVersionString v = (AttributeValueVersionString)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
		
	public String toString() {
		return this.value;
	}
		
		
	public static class Builder extends AbstractVersionBuilder {
			
		/**
		 * 
		 */
		private static final long serialVersionUID = 6333095756249663026L;
		
		private String value = "N/A";
			
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		@Override
		public Builder fromDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.fromDate(c);
		}
		
		@Override
		public Builder toDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.toDate(c);
		}
		
		public Builder value(String v) {
			this.value = v;
			return this;
		}
			
		public AttributeValueVersionString build() {
			return new AttributeValueVersionString(this);
		}
	}
}
