package org.gomma.model.attribute;

import org.gomma.model.DefaultVersionEndDate;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public abstract class AbstractAttributeValueVersion extends AbstractAttributeValue implements AttributeValueVersion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7565791118558595373L;
	
	
	private SimplifiedGregorianCalendar fromDate;
	private SimplifiedGregorianCalendar toDate;
	
	
	public AbstractAttributeValueVersion(AbstractVersionBuilder b) {
		super(b);
		
		this.fromDate = b.fromDate;
		this.toDate = b.toDate;
	}
	
	public SimplifiedGregorianCalendar getFromDate() {
		return this.fromDate;
	}
	
	public SimplifiedGregorianCalendar getToDate() {
		return this.toDate;
	}
	
	
	public abstract static class AbstractVersionBuilder extends AbstractAttributeValue.AbstractBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 685900009219801020L;
		
		private SimplifiedGregorianCalendar fromDate = DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar toDate = DefaultVersionEndDate.getInstance().getDate();
		
		public AbstractVersionBuilder(int id, Attribute att) {
			super(id,att);	
		}
		
		public AbstractVersionBuilder fromDate(SimplifiedGregorianCalendar c) {
			this.fromDate = c;
			return this;
		}
		
		public AbstractVersionBuilder toDate(SimplifiedGregorianCalendar c) {
			this.toDate = c;
			return this;
		}
	}
}
