package org.gomma.model.attribute;


public final class AttributeValueString extends AbstractAttributeValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1474566902396474129L;
	
	private String value;
	
	private AttributeValueString(Builder b) {
		super(b);
		this.value = b.value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueInteger)) return false;
		AttributeValueInteger v = (AttributeValueInteger)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
	
	public String toString() {
		return this.value;
	}
	
	
	public static class Builder extends AbstractBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -314906576244961025L;
		
		private String value = "N/A";
		
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		public Builder value(String v) {
			this.value = v;
			return this;
		}
		
		public AttributeValueString build() {
			return new AttributeValueString(this);
		}
	}
}
