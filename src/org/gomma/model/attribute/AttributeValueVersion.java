package org.gomma.model.attribute;

import org.gomma.util.date.SimplifiedGregorianCalendar;

public interface AttributeValueVersion extends AttributeValue {

	public SimplifiedGregorianCalendar getFromDate();
	
	public SimplifiedGregorianCalendar getToDate();
}
