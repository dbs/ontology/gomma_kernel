package org.gomma.model.attribute;

import org.gomma.util.date.SimplifiedGregorianCalendar;


public class AttributeValueVersionInteger extends AbstractAttributeValueVersion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1655703614752513357L;

	private int value;
	
	private AttributeValueVersionInteger(Builder b) {
		super(b);
		this.value = b.value;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueVersionInteger)) return false;
		AttributeValueVersionInteger v = (AttributeValueVersionInteger)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
	
	public String toString() {
		return Integer.toString(this.value);
	}
	
	
	public static class Builder extends AbstractVersionBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 5719268485250615036L;
		
		private int value = 0;
		
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		public Builder value(int v) {
			this.value = v;
			return this;
		}
		
		@Override
		public Builder fromDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.fromDate(c);
		}
		
		@Override
		public Builder toDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.toDate(c);
		}
		
		public AttributeValueVersionInteger build() {
			return new AttributeValueVersionInteger(this);
		}
	}
}
