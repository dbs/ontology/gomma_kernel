package org.gomma.model.attribute;

import java.io.Serializable;

public interface AttributeValue extends Serializable {

	public int getID();
	
	public Attribute getAttribute();
	
	public String toString();
}
