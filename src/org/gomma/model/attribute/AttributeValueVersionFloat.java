package org.gomma.model.attribute;

import org.gomma.util.date.SimplifiedGregorianCalendar;


public class AttributeValueVersionFloat extends AbstractAttributeValueVersion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1854223196225670796L;
	
	private float value;
	
	private AttributeValueVersionFloat(Builder b) {
		super(b);
		this.value = b.value;
	}
	
	public float getValue() {
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueVersionFloat)) return false;
		AttributeValueVersionFloat v = (AttributeValueVersionFloat)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
	
	public String toString() {
		return Float.toString(this.value);
	}
	
	
	public static class Builder extends AbstractVersionBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1590052031495561836L;
		
		private float value = 0F;
		
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		@Override
		public Builder fromDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.fromDate(c);
		}
		
		@Override
		public Builder toDate(SimplifiedGregorianCalendar c) {
			return (Builder)super.toDate(c);
		}
		
		public Builder value(float v) {
			this.value = v;
			return this;
		}
		
		public AttributeValueVersionFloat build() {
			return new AttributeValueVersionFloat(this);
		}
	}

}
