package org.gomma.model.attribute;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class AttributeValueVersionSet extends AbstractGenericObjectSet<AbstractAttributeValueVersion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1684443863948173508L;

	public AttributeValueVersionSet() {
		super();
	}
	
	public void addAttributeValue(AttributeValueVersion v) {
		if (v==null)
			throw new NullPointerException("Could not resolve the attribute value that should be added.");
		super.addElement((AbstractAttributeValueVersion) v);
	}
	public void removeAttributeValue(AttributeValueVersion v) {
		if (v==null)
			throw new NullPointerException("Could not resolve the attribute value that should be added.");
		super.removeElement((AbstractAttributeValueVersion) v);
	}
	public AttributeValueVersion getValue(int id) throws NoSuchElementException {
		if (!super.contains(id))
			throw new NoSuchElementException("Could not find attribute value for ID '"+id+"'.");
		return super.getElement(id);
	}

	public AttributeSet getAttributeSet() {
		AttributeSet set = new AttributeSet();
		
		for (AttributeValueVersion avv : this)
			if (!set.contains(avv.getAttribute()))
				set.addAttribute(avv.getAttribute());
				
		return set;
	}
}
