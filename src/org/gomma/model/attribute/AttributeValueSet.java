package org.gomma.model.attribute;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class AttributeValueSet extends AbstractGenericObjectSet<AbstractAttributeValue> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3928944538911776534L;

	public AttributeValueSet() {
		super();
	}
	
	public void addAttributeValue(AttributeValue v) {
		if (v==null)
			throw new NullPointerException("Could not resolve the attribute value that should be added.");
		super.addElement((AbstractAttributeValue) v);
	}
	
	public AttributeValue getValue(int id) throws NoSuchElementException {
		if (super.contains(id))
			throw new NoSuchElementException("Could not find attribute value for ID '"+id+"'.");
		return super.getElement(id);
	}
	
	public AttributeSet getAttributeSet() {
		AttributeSet set = new AttributeSet();
		
		for (AttributeValue av : this)
			if (!set.contains(av.getAttribute()))
				set.addAttribute(av.getAttribute());
				
		return set;
	}
}
