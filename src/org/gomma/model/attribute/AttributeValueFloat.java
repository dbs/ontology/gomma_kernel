package org.gomma.model.attribute;


public final class AttributeValueFloat extends AbstractAttributeValue {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6279643636817870147L;
	
	private float value;
	
	private AttributeValueFloat(Builder b) {
		super(b);
		this.value = b.value;
	}
	
	public float getValue() {
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueInteger)) return false;
		AttributeValueInteger v = (AttributeValueInteger)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
	
	public String toString() {
		return Float.toString(this.value);
	}
	
	
	public static class Builder extends AbstractBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 144044092475100234L;
		
		private float value = 0F;
		
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		public Builder value(float v) {
			this.value = v;
			return this;
		}
		
		public AttributeValueFloat build() {
			return new AttributeValueFloat(this);
		}
	}

}
