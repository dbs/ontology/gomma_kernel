package org.gomma.model.attribute;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DataTypes;

public class Attribute extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710383299676438086L;
	
	private String scope;
	private DataTypes dataType;
	
	private Attribute(Builder b) {
		super(b.id);
		super.setName(b.name);
		this.scope = b.scope;
		this.dataType = b.dataType;
	}
	
	public String getScope() {
		return this.scope;
	}
	
	public DataTypes getDataType() {
		return this.dataType;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Attribute)) return false;
		return ((Attribute)o).getID()== this.getID();
	}
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -6548632045487590978L;

		private final int id;
		
		private String name  = "N/A";
		private String scope = "N/A";
		private DataTypes dataType = DataTypes.STRING;
		
		public Builder (int id) {
			this.id = id;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder scope(String scope) {
			this.scope = scope;
			return this;
		}
		
		public Builder dataType(DataTypes type) {
			this.dataType = type;
			return this;
		}
		
		public Attribute build() {
			return new Attribute(this);
		}
	}
}
