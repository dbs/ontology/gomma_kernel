package org.gomma.model.attribute;

public final class AttributeValueInteger extends AbstractAttributeValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3679036665852833289L;
	
	private int value;
	
	private AttributeValueInteger(Builder b) {
		super(b);
		this.value = b.value;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof AttributeValueInteger)) return false;
		AttributeValueInteger v = (AttributeValueInteger)o;
		if (super.getID()!=v.getID()) return false;
		return true;
	}
	
	public String toString() {
		return Integer.toString(this.value);
	}
	
	
	public static class Builder extends AbstractBuilder {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -5354642072503755171L;
		
		private int value = 0;
		
		public Builder(int id, Attribute att) {
			super(id,att);
		}
		
		public Builder value(int v) {
			this.value = v;
			return this;
		}
		
		public AttributeValueInteger build() {
			return new AttributeValueInteger(this);
		}
	}
}
