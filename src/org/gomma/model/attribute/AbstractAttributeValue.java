package org.gomma.model.attribute;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;

public abstract class AbstractAttributeValue extends AbstractGenericObject implements AttributeValue {

	/**
	 * 
	 */
	private static final long serialVersionUID = 850815679447741234L;
	
	
	private final Attribute attr;
	
	
	
	protected AbstractAttributeValue(AbstractBuilder b) {
		super(b.id);
		this.attr = b.attribute;
	}
	
	public Attribute getAttribute() {
		return this.attr;
	}
	
	
	
	public abstract static class AbstractBuilder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -2362814191012218246L;
		
		private final int id;
		private final Attribute attribute;
		
		public AbstractBuilder(int id, Attribute att) {
			this.id=id;
			this.attribute = att;
		}
		
		
	}
}
