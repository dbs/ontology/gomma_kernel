/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.model.mapping;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.attribute.AttributeValue;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.source.Source;

/**
 * The class Mapping represents mapping metadata. The mapping is associated with a semantic mapping type,
 * such as isEquivalentTo and isInvolvedIn. Similarly, the mapping is part of a specific mapping class, 
 * i.e., it classifies the mapping into instance-instance mappings, annotation mappings and ontology 
 * mappings. Although, a mapping interrelates objects of two source versions, set-based operators also 
 * allows merging mappings using different source version. Therefore, a mapping can interrelate objects 
 * of two sources which are of different versions. The object correspondences of the mapping are not 
 * included in this class; use the class ObjectCorrespondences ({@link org.gomma.model.mapping.ObjCorrespondenceSet})
 * instead.  
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class Mapping extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3334175313931516785L;

	/**
	 * The constant represents the mapping identifier for mappings that have been produced on the fly and 
	 * are not saved within the repository. 
	 */
	public static final int ARTIFICAL_MAPPING_ID = -1; 
	
	public static final String MAP_TOOL_GOMMA = "Gomma";
	
	
	private Source domainSource;
	
	private Source rangeSource;
	
	/**
	 * The isInstanceMapping is true if the current mapping is build based on instance objects. Otherwise,
	 * it is false.
	 */
	private boolean isInstanceMapping;
	
	/**
	 * The isDerived is true if the mapping is built based on other mappings, e.g., by using set-based 
	 * operators, such as union, intersect, and majority.
	 */
	private boolean isDerived;
	
	/**
	 * The mapClass represents the mapping class name, such as instance-instance mapping, annotation 
	 * mapping, and ontology mapping.
	 */
	private String mapClass;
	
	/**
	 * The mapClassDescr represents the mapping class description, such as for instance-instance mapping, 
	 * annotation mapping, and ontology mapping.
	 */
	private String mapClassDescr;
	
	/**
	 * The mapType represents the mapping type name showing the semantics of the mapping, e.g., 
	 * isEquivalentTo, and isInvolvedIn.
	 */
	private String mapType;
	
	/**
	 * The mapTypeDescr represents the mapping type description such as for isEquivalentTo, and isInvolvedIn. 
	 */
	private String mapTypeDescr;
	
	/**
	 * The mapMethod represents the method that was used for generation of the mapping, e.g., dice as a form for 
	 * instance-based or n-gram as a form of string-based. Default value is 'N/A', i.e., the method is unknown.
	 */
	private String mapMethod;
	
	/**
	 * The mapMethodDescr represents further information about the used mapMethod.
	 */
	private String mapMethodDescr;
	
	/**
	 * The mapTool represents the tool, that was used for mapping generation, e.g., COMMA++ or iFuice.
	 * Default value is 'N/A', i.e., the used tool is unknown.
	 */
	private String mapTool;
	
	/**
	 * The mapToolDescr represents further information about the used mapping/matching tool.
	 */
	private String mapToolDescr;
	
	
	private AttributeValueSet attributeValueSet;
		

	
	
		
	/**
	 * The constructor initializes the class Mapping. The mapping is identified by 
	 * the unique identifier which is typically generated by the GOMMA system. All 
	 * other data is set with default values or empty sets. 
	 * @param id unique identifier
	 */
	private Mapping(Builder b) {
		super(b.id);
		super.setName(b.name);
		
		this.domainSource = b.domainSource;
		this.rangeSource = b.rangeSource;
		
		this.isInstanceMapping = b.isInstanceMapping;
		this.isDerived = b.isDerived;
		
		this.mapClass = b.mapClass;
		this.mapClassDescr = b.mapClassDescr;
		this.mapType = b.mapType;
		this.mapTypeDescr = b.mapTypeDescr;
		this.mapMethod = b.mapMethod;
		this.mapMethodDescr = b.mapMethodDescr;
		this.mapTool = b.mapTool;
		this.mapToolDescr = b.mapToolDescr;
		
		this.attributeValueSet = new AttributeValueSet();
	}
	
	public Source getDomainSource() {
		return this.domainSource;
	}
	
	public Source getRangeSource() {
		return this.rangeSource;
	}
	
	public boolean isInstanceMapping() {
		return this.isInstanceMapping;
	}
	
	public boolean isDerived() {
		return this.isDerived;
	}
	
	public String getMappingClassName() {
		return this.mapClass;
	}
	
	public String getMappingClassDescription() {
		return this.mapClassDescr;
	}
	
	public String getMappingTypeName() {
		return this.mapType;
	}
	
	public String getMappingTypeDescription() {
		return this.mapTypeDescr;
	}
	
	public String getMappingMethodName() {
		return this.mapMethod;
	}
	
	public String getMappingMethodDescription() {
		return this.mapMethodDescr;
	}
	
	public String getMappingToolName() {
		return this.mapTool;
	}
	
	public String getMappingToolDescription() {
		return this.mapToolDescr;
	}
	
	public AttributeValueSet getAttributeValues() {
		return this.attributeValueSet;
	}
	
	public void addAttrbuteValue(AttributeValue av) {
		this.attributeValueSet.addAttributeValue(av);
	}
	
	/**
	 * The method compares the current with an external Mapping. It returns true if both mappings 
	 * use the same identifier. Otherwise, it returns false.
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Mapping)) return false;
		return ((Mapping)o).getID()==super.getID();
	}
	
	/**
	 * @see Object#hashCode()
	 */
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + this.domainSource.hashCode();
		result = 31 * result + this.rangeSource.hashCode();
		
		return result;
	}
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -8385637590289490585L;
		
		private final int id;
		private final Source domainSource, rangeSource;
		
		
		private String name = "N/A";
		
		private boolean isInstanceMapping = false;
		private boolean isDerived = false;
		
		private String mapClass = "N/A";
		private String mapClassDescr = "N/A";
		private String mapType = "N/A";
		private String mapTypeDescr = "N/A";
		private String mapMethod = "N/A";
		private String mapMethodDescr ="N/A";
		private String mapTool = "N/A";
		private String mapToolDescr = "N/A";
		
		public Builder(int id, Source domainSource, Source rangeSource) {
			this.id = id;
			this.domainSource = domainSource;
			this.rangeSource = rangeSource;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder mappingClassName(String name) {
			this.mapClass = name;
			return this;
		}
		
		public Builder mappingClassDescription(String descr) {
			this.mapClassDescr = descr;
			return this;
		}
		
		public Builder mappingTypeName(String name) {
			this.mapType = name;
			return this;
		}
		
		public Builder mappingTypeDescription(String descr) {
			this.mapTypeDescr = descr;
			return this;
		}
		
		public Builder mappingMethodName(String name) {
			this.mapMethod = name;
			return this;
		}
		
		public Builder mappingMethodDescription(String descr) {
			this.mapMethodDescr = descr;
			return this;
		}
		
		public Builder mappingToolName(String name) {
			this.mapTool = name;
			return this;
		}
		
		public Builder mappingToolDescription(String descr) {
			this.mapToolDescr = descr;
			return this;
		}
		
		public Builder isInstanceMapping(boolean isInstanceMap) {
			this.isInstanceMapping = isInstanceMap;
			return this;
		}
		
		public Builder isDerived(boolean isDerived) {
			this.isDerived = isDerived;
			return this;
		}
		
		public Mapping build() {
			return new Mapping(this);
		}
	}
}
