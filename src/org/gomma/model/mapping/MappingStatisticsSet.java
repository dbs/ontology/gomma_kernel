package org.gomma.model.mapping;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class MappingStatisticsSet extends AbstractGenericObjectSet<MappingStatistics> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4923614248170933024L;

	public MappingStatisticsSet() {
		super();
	}
	
	public void addMappingStatistics(MappingStatistics stat) {
		if (stat == null)
			throw new NullPointerException("Could not resolve the mapping statistics that should be added."); 
		super.addElement(stat);
	}
	
	public MappingSet getMappingSet() {
		MappingSet set = new MappingSet();
		
		for (MappingStatistics stat : this)
			set.addMapping(stat.getMapping());
		
		return set;
	}
	
	public MappingStatistics getMappingStatistics(Mapping m) {
		if (m == null)
			throw new NullPointerException("Could not resolve the mapping "+
					"for that the statistics should be retrieved.");
		
		if (!super.contains(m.getID()))
				throw new NoSuchElementException("Could not find any statistics for "+
						"mapping '"+m.getName()+"'.");
		
		return super.getElement(m.getID());
	}
}
