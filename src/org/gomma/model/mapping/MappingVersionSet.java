package org.gomma.model.mapping;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;
import org.gomma.util.MappingVersionDateComparator;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public final class MappingVersionSet extends AbstractGenericObjectSet<MappingVersion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5177069619441549318L;

	public MappingVersionSet() {
		super();
	}
	
	public void addMappingVersion(MappingVersion v) {
		if (v == null)
			throw new NullPointerException("Could not resolve the mapping version that should be added.");
		super.addElement(v);
	}
	
	public MappingVersion getMappingVersion(int id) throws NoSuchElementException {
		if (super.contains(id))
			return super.getElement(id);
		throw new NoSuchElementException("Could not find a mapping version using the internal ID '"+id+"'.");
	}
	
	/**
	 * The method returns the mapping version using the version string.
	 * @param version version string of that the resulting mapping version should be
	 * @return mapping version of the specified version string
	 * @throws NoSuchElementException
	 */
	public MappingVersion getMappingVersion(String version) throws NoSuchElementException {
		for (MappingVersion v : this)
			if (v.getName().equals(version)) return v;
		throw new NoSuchElementException("Could not find a mapping version using the version '"+version+"'.");
	}
	
	/**
	 * The method returns the set of mapping versions which belong to the specified mapping.
	 * @param m mapping to which the resulting set of mapping version should belong
	 * @return set of mapping versions belonging to the specified mapping
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) {
		if (m==null) 
			throw new NullPointerException("Could not resolve the mapping.");
		
		MappingVersionSet set = new MappingVersionSet();
		
		for (MappingVersion v : this) 
			if (v.getMapping().equals(m)) set.addMappingVersion(v);
		
		return set;
	}
	
	public MappingVersionSet getMappingVersionSet(String fromVersion, String toVersion) {
		if (fromVersion == null)
			throw new NullPointerException("Could not resolve the from version");
		if (toVersion == null)
			throw new NullPointerException("Could not resolve the to version");
		
		List<MappingVersion> sortedList = this.sort();
		MappingVersionSet set = new MappingVersionSet();
		boolean isInVersionRange = false;
		
		for (MappingVersion v : sortedList) {
			if (!isInVersionRange && v.getName().equals(fromVersion))
				isInVersionRange = true;
			
			if (isInVersionRange) {
				set.addMappingVersion(v);
				
				if (v.getName().equals(toVersion)) {
					isInVersionRange=false;
					break;
				}
			}
		}
		
		return set;
	}
	
	public MappingVersionSet getMappingVersionSet(SimplifiedGregorianCalendar startTime, SimplifiedGregorianCalendar endTime) {
		if (startTime==null)
			throw new NullPointerException("Could not resolve the start time.");
		if (endTime==null)
			throw new NullPointerException("Could not resolve the end time.");
		
		List<MappingVersion> sortedList = this.sort();
		MappingVersionSet set = new MappingVersionSet();
		boolean isInVersionRange = false;
		
		for (MappingVersion v : sortedList) {
			if (!isInVersionRange && 
					(v.getCreationDate().get(Calendar.YEAR)==startTime.get(Calendar.YEAR) &&
					 v.getCreationDate().get(Calendar.MONTH)==startTime.get(Calendar.MONTH) &&
					 v.getCreationDate().get(Calendar.DAY_OF_MONTH)==startTime.get(Calendar.DAY_OF_MONTH)))
				isInVersionRange = true;
			
			if (isInVersionRange) {
				if (v.getCreationDate().get(Calendar.YEAR)==endTime.get(Calendar.YEAR) &&
						 v.getCreationDate().get(Calendar.MONTH)==endTime.get(Calendar.MONTH) &&
						 v.getCreationDate().get(Calendar.DAY_OF_MONTH)==endTime.get(Calendar.DAY_OF_MONTH)) {
					isInVersionRange=false;
					set.addMappingVersion(v);
					break;
				} else set.addMappingVersion(v);
				
/*				if (v.getCreationDate().after(endTime)) {
					isInVersionRange=false;
					break;
				} else set.addMappingVersion(v);
*/				
			}
		}		  
		return set;
	}
	
	public MappingVersion getMappingVersion(String version, Mapping m) {
		return this.getMappingVersionSet(m).getMappingVersion(version);
	}
	
	public MappingVersionSet getMappingVersionSet(String fromVersion, String toVersion, Mapping m) {
		return this.getMappingVersionSet(m).getMappingVersionSet(fromVersion, toVersion);
	}
	
	public MappingVersionSet getMappingVersionSet(SimplifiedGregorianCalendar startTime, SimplifiedGregorianCalendar endTime, Mapping m) {
		return this.getMappingVersionSet(m).getMappingVersionSet(startTime, endTime);
	}
	
	// get n previous Mapping version of mapping m  
	public MappingVersionSet getPreviouslyCreatedMappingVersionSet(int nLastVersion, Mapping m) {
		MappingVersionSet versionSet = this.getMappingVersionSet(m);
		
		if (versionSet.size()<=nLastVersion){
			throw new IllegalArgumentException("The requested number of mapping versions is greater "+
					"than the number of available mapping versions."); 
		}
		int k = nLastVersion;
		List<MappingVersion> versionList = versionSet.sort();
		List<MappingVersion> newList= new ArrayList<MappingVersion>();
		newList=versionList.subList(versionList.size()-k-1, versionList.size()-1);
		MappingVersionSet resultSet = new MappingVersionSet();
		
		for(int i=0;i<newList.size();i++) 
			resultSet.addMappingVersion(newList.get(i));
		
		return resultSet;
		

	}
	
//	/**
//	 * The method returns the mapVersions in the map between the dateInterval.
//	 * @return mapVersions from dateFrom to dateTo
//	 */
//   public MappingVersionSet getMappingVersionSet(Date dateFrom, Date dateTo, Mapping m) {
//    	// sollte spaeter abgeloest werden
//    	SimplifiedGregorianCalendar from = new SimplifiedGregorianCalendar(dateFrom.getTime());
//    	SimplifiedGregorianCalendar to = new SimplifiedGregorianCalendar(dateTo.getTime());
//    	
//    	MappingVersionSet mvs = new MappingVersionSet();
//    	
//    	if (this.isOfMapping(m)) {
//    		for (MappingVersion mv: this) {
//			
//    			if ((mv.getCreationDate().before(from) || mv.getCreationDate().equals(from)) && 
//    					(mv.getCreationDate().before(to) || mv.getCreationDate().equals(to)))
//    					mvs.addMappingVersion(mv);
//    		}
//    		
//    		// FIXME: warum soll hier eine Excepeption geworfen werden 
//    		if (mvs.size()==0) 
//    			throw new NoSuchElementException(
//					"could not find such mapVersions during this Date from'"+dateFrom+"'to'"+dateTo+"'.");
//    		
//    		
//    		// FIXME: warum soll hier eine Exception geworfen werden
//    	} //else throw new GommaException(
//    		//	"map versions do not belong to same mapping'"+m+"'.");
//		
//		return mvs;
//    }
    
//    /**
//	 * The method returns the mapVersions in the map between the VersionNameInterval.
//	 * @return mapVersions during the VersionNameInterval
//	 */
//    // FIXME: Warum Vergleich ueber Date?
//    public MappingVersionSet getMappingVersionSet(String firstVersionName, String lastVersionName,Mapping m){
//    
//		return this.getMappingVersionSet(this.getMappingVersionDate(firstVersionName,m), this.getMappingVersionDate(lastVersionName,m),m);
//    }
    
//    /**
//	 * The method returns the timeStamp of the MapVersion which is named "versionName" 
//	 * @return time stamp of the mapVersion
//	 */
//    // FIXME: Die Methode verstehe ich nicht. Was macht die? Es gibt/gab viele Fehler in ihr!!!
//    // warum benoetoigt man eine Method die das Datu zurueckliefert?
//    public Date getMappingVersionDate(String versionName, Mapping m) {
//    	SimplifiedGregorianCalendar date = SimplifiedGregorianCalendar.getInstance();
//    	SimplifiedGregorianCalendar tempDate = date;
//    	
//    	if (this.isOfMapping(m)) {
//    		for (MappingVersion mv : this) {
//    			
//    			if(mv.getName().equals(versionName)) {
//    				date = mv.getCreationDate();
//    				break;
//    			}
//    		}
//    		// FIXME: warum soll hier eine Exception geworfen werden
//    		if (date.equals(tempDate)) 
//    			throw new NoSuchElementException(
//    					"could not find such MapVersion with this versionName:'"+versionName+"'.");
//    		// FIXME: warum soll hier eine Exception geworfen werden
//    	} //else throw new GommaException(
//    	  //		"could not calculate the map versions that come from different mappings.");
//    	
//    	return date.getTime();
//    }
    
    
/*    public Date getMappingVersionDate(String versionName,Mapping m){
    	Date date=new Date();
    	Date tempDate=date;
    	if(this.isBelongto(m)){
    	for(MappingVersion mv : this){
    		if(mv.getName().equals(versionName)){
    			 date=mv.getCreationDate().getTime();
    			break;
    		}
    	}
    	if(date.equals(tempDate))throw new NoSuchElementException("could not find such MapVersion with this versionName:'"+versionName+"'.");
    	}else throw new GommaException("could not calculate the map versions that come from different Mapping.");
    	return date;
    }
	
	
//    public MappingVersionSet getPreviousMappingVersions(int k, Mapping m) throws IllegalArgumentException{
//		
//		if(super.size()>k && this.isOfMapping(m)){
//			MappingVersionSet mvs=new MappingVersionSet();
//			    List<MappingVersion> list=this.sort();
//			    List<MappingVersion> newList= new ArrayList<MappingVersion>();
//				newList=list.subList(list.size()-k-1, list.size()-1);
//				//Iterator it= newList.listIterator();
//				//while(it.hasNext()){
//			    for(int i=0;i<newList.size();i++){
//					mvs.addMappingVersion(newList.get(i));
//				//}
//			    }
//			    return mvs;
//			
//		    }else throw new IllegalArgumentException("wrong input number of Versions or the map verisons belong to different mapping");
//			
//		}
 */   
    public MappingVersion getFirstMappingVersion(){

		if (this.size() == 0)
			throw new NoSuchElementException("Could not find the latest mapping version, "+
					"since the currently managed set is empty.");
		
		List<MappingVersion> list=this.sort();
		MappingVersion mv=list.get(0);
		return mv;
    }
    /**
	 * The method returns the latest mapping version of the currently managed set.
	 * @return mapping version with the latest creation date in the set
	 */
    
	public MappingVersion getLastMappingVersion(){

		if (this.size() == 0)
			throw new NoSuchElementException("Could not find the latest mapping version, "+
					"since the currently managed set is empty.");
		
		List<MappingVersion> list=this.sort();
		MappingVersion mv=list.get(list.size()-1);
		return mv;
    }
    
	
    /**
	 * The method sort sorts the mapping version set by creation date in ascending order.
	 * @return sortedList of mapVersion
	 * @see gomma.util.MappingVersionDateComparator
	 */
	public List<MappingVersion> sort() {
    	
    	List<MappingVersion> list=new ArrayList<MappingVersion>();
    	list.addAll(super.getCollection()); 
    	
    	Comparator<MappingVersion> comp= new MappingVersionDateComparator();
    	Collections.sort(list, comp);
    	return list;
	}
    
//	// FIXME: ist das richtig? was soll bei einer leeren Menge passieren
//	private boolean isOfMapping(Mapping m) {
//    	boolean result=true;
//    	
//    	for(MappingVersion mv : this) {
//    		if (!mv.getMapping().equals(m))
//    		result=false;
//    		
//    	}
//    	return result;
//    }
    
}
