package org.gomma.model.mapping;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;

public final class MappingStatistics extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6777295118524703424L;
	
	private final Mapping mapping;
	private final int numVersions;	
	private final float avgNumCorrs;
	private final float avgNumDomainObjs;
	private final float avgNumRangeObjs;
	
	
	private MappingStatistics(Builder b) {
		super(b.m.getID());
		
		this.mapping = b.m;
		this.numVersions = b.numVersions;
		this.avgNumCorrs = b.avgNumCorrs;
		this.avgNumDomainObjs = b.avgNumDomainObjs;
		this.avgNumRangeObjs  = b.avgNumRangeObjs;
	}
	
	public Mapping getMapping() {
		return this.mapping;
	}
	
	public int getNumberOfVersions() {
		return this.numVersions;
	}
	
	public float getAverageNumberOfCorrespondences() {
		return this.avgNumCorrs;
	}
	
	public float getAverageNumberOfDomainObjects() {
		return this.avgNumDomainObjs;
	}
	
	public float getAverageNumberOfRangeObjects() {
		return this.avgNumRangeObjs;
	}
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -4865354594043928788L;
		
		private final Mapping m;
		private int numVersions = 0;
		private float avgNumCorrs = 0F;
		private float avgNumDomainObjs = 0F;
		private float avgNumRangeObjs = 0F;
		
		public Builder(Mapping m) {
			this.m = m;
		}
		
		public Builder numberOfVersions(int n) {
			this.numVersions = n;
			return this;
		}
		
		public Builder averageNumberOfCorrespondences(float avg) {
			this.avgNumCorrs = avg;
			return this;
		}
		
		public Builder averageNumberOfDomainObjects(float avg) {
			this.avgNumDomainObjs = avg;
			return this;
		}
		
		public Builder averageNumberOfRangeObjects(float avg) {
			this.avgNumRangeObjs = avg;
			return this;
		}
		
		public MappingStatistics build() {
			return new MappingStatistics(this);
		}
	}
}
