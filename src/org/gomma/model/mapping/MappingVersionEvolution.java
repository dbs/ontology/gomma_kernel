package org.gomma.model.mapping;

import java.io.Serializable;


public class MappingVersionEvolution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4342320746811158060L;

	private MappingVersion fromMappingVersion;
	private MappingVersion toMappingVersion;
	
	private int numAddedCorrs;
	private int numDeletedCorrs;
	private int numSameCorrs;
	private float addedCorrFraction;
	private float deletedCorrFraction;
	private int numAddedDomainObjs;
	private int numDeletedDomainObjs;
	private int numSameDomainObjs;
	private float addedDomainObjsFraction;
	private float deletedDomainObjsFraction;
	private int numAddedRangeObjs;
	private int numDeletedRangeObjs;
	private int numSameRangeObjs;
	private float addedRangeObjsFraction;
	private float deletedRangeObjsFraction;
	
	
	public MappingVersionEvolution(Builder b) {
		this.fromMappingVersion = b.fromVersion;
		this.toMappingVersion = b.toVersion;
		
		this.numAddedCorrs = b.numAddedCorrs;
		this.numDeletedCorrs = b.numDeletedCorrs;
		this.numSameCorrs = b.numSameCorrs;
		this.addedCorrFraction = b.addFrac;
		this.deletedCorrFraction=b.delFrac;
		this.numAddedDomainObjs = b.numAddedDomainObjs;
		this.numDeletedDomainObjs = b.numDeletedDomainObjs;
		this.numSameDomainObjs = b.numSameDomainObjs;
		this.addedDomainObjsFraction = b.addDomainObjsFrac;
		this.deletedDomainObjsFraction=b.delDomainObjsFrac;
		this.numAddedRangeObjs = b.numAddedRangeObjs;
		this.numDeletedRangeObjs = b.numDeletedRangeObjs;
		this.numSameRangeObjs = b.numSameRangeObjs;
		this.addedRangeObjsFraction = b.addRangeObjsFrac;
		this.deletedRangeObjsFraction=b.delRangeObjsFrac;
	}
	
	public MappingVersion getFromMappingVersion() {
		return this.fromMappingVersion;
	}
	
	public MappingVersion getToMappingVersion() {
		return this.toMappingVersion;
	}
	
	public int getNumberOfAddedCorrs() {
		return this.numAddedCorrs;
	}
	
	public int getNumberOfDeletedCorrs() {
		return this.numDeletedCorrs;
	}
	
	public int getNumberOfSameCorrs() {
		return this.numSameCorrs;
	}
	
	public float getAddedCorrFraction() {
		return this.addedCorrFraction;
	}
	
	public float getDeletedCorrFraction() {
		return this.deletedCorrFraction;
	}
	
	public int getNumberOfAddedDomainObjs() {
		return this.numAddedDomainObjs;
	}
	
	public int getNumberOfDeletedDomainObjs() {
		return this.numDeletedDomainObjs;
	}
	
	public int getNumberOfSameDomainObjs() {
		return this.numSameDomainObjs;
	}
	
	public float getAddedDomainObjsFraction() {
		return this.addedDomainObjsFraction;
	}
	
	public float getDeletedDomainObjsFraction() {
		return this.deletedDomainObjsFraction;
	}
	
	public int getNumberOfAddedRangeObjs() {
		return this.numAddedRangeObjs;
	}
	
	public int getNumberOfDeletedRangeObjs() {
		return this.numDeletedRangeObjs;
	}
	
	public int getNumberOfSameRangeObjs() {
		return this.numSameRangeObjs;
	}
	
	public float getAddedRangeObjsFraction() {
		return this.addedRangeObjsFraction;
	}
	
	public float getDeletedRangeObjsFraction() {
		return this.deletedRangeObjsFraction;
	}
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 6035248902488264586L;
		
		private final MappingVersion fromVersion;
		private final MappingVersion toVersion;
		
		private int numAddedCorrs = 0;
		private int numDeletedCorrs = 0;
		private int numSameCorrs = 0;
		private float addFrac = 0F;
		private float delFrac=0F;
		private int numAddedDomainObjs = 0;
		private int numDeletedDomainObjs = 0;
		private int numSameDomainObjs = 0;
		private float addDomainObjsFrac = 0F;
		private float delDomainObjsFrac=0F;
		private int numAddedRangeObjs = 0;
		private int numDeletedRangeObjs = 0;
		private int numSameRangeObjs = 0;
		private float addRangeObjsFrac = 0F;
		private float delRangeObjsFrac=0F;
		
		public Builder(MappingVersion from, MappingVersion to) {
			this.fromVersion = from;
			this.toVersion = to;
		}
		
		public Builder numberOfAddedCorrs(int n) {
			this.numAddedCorrs = n;
			return this;
		}
		
		public Builder numberOfDeletedCorrs(int n) {
			this.numDeletedCorrs = n;
			return this;
		}
		
		public Builder numberOfSameCorrs(int n) {
			this.numSameCorrs= n;
			return this;
		}
		
		public Builder addedCorrFraction(float n) {
			this.addFrac = n;
			return this;
		}
		
		public Builder deletedCorrFraction(float n) {
			this.delFrac = n;
			return this;
		}
		
		public Builder numberOfAddedDomainObjs(int n) {
			this.numAddedDomainObjs = n;
			return this;
		}
		
		public Builder numberOfDeletedDomainObjs(int n) {
			this.numDeletedDomainObjs = n;
			return this;
		}
		
		public Builder numberOfSameDomainObjs(int n) {
			this.numSameDomainObjs= n;
			return this;
		}
		
		public Builder addedDomainObjsFraction(float n) {
			this.addDomainObjsFrac = n;
			return this;
		}
		
		public Builder deletedDomainObjsFraction(float n) {
			this.delDomainObjsFrac = n;
			return this;
		}
		
		public Builder numberOfAddedRangeObjs(int n) {
			this.numAddedRangeObjs = n;
			return this;
		}
		
		public Builder numberOfDeletedRangeObjs(int n) {
			this.numDeletedRangeObjs = n;
			return this;
		}
		
		public Builder numberOfSameRangeObjs(int n) {
			this.numSameRangeObjs= n;
			return this;
		}
		
		public Builder addedRangeObjsFraction(float n) {
			this.addRangeObjsFrac = n;
			return this;
		}
		
		public Builder deletedRangeObjsFraction(float n) {
			this.delRangeObjsFrac = n;
			return this;
		}
		
		
		public MappingVersionEvolution build() {
			return new MappingVersionEvolution(this);
		}
	}
}
