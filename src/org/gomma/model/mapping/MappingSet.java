/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * changes: --
 * 
 **/

package org.gomma.model.mapping;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;
import org.gomma.model.source.SourceSet;

/**
 * The class Mappings collects mapping metadata of different mappings. The mappings are indexed by 
 * their unique identifiers.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingSet extends AbstractGenericObjectSet<Mapping> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6204602163688107581L;
	
	
	/**
	 * The constructor initializes the class Mappings.
	 */
	public MappingSet() {
		super();
	}

	/**
	 * The method adds a mapping to the map.
	 * @param m mapping to be added
	 * @see org.gomma.model.mapping.Mapping
	 */
	public void addMapping(Mapping m) {
		if (m==null) 
			throw new NullPointerException("Could not resolve the mapping that should be added.");
		super.addElement(m);
	}
	
	/**
	 * The method returns the mapping object for the specified mapping identifier.
	 * @param mapID unique mapping identifier
	 * @return mapping object
	 * @throws NoSuchElementException
	 */
	public Mapping getMapping(int mapID) throws NoSuchElementException {
		if (super.contains(mapID)) return super.getElement(mapID);
		throw new NoSuchElementException("Could not find a mapping using the unique identifier '"+mapID+"'.");
	}
	
	public Mapping getMapping(String name) throws NoSuchElementException {
		for (Mapping m : this)
			if (m.getName().equals(name)) return m;
		throw new NoSuchElementException("Could not find a mapping using the name '"+name+"'.");
	}
	
	/**
	 * The method returns a set of all source versions used in the domain of the mappings in the map.
	 * @return set of domain sources
	 */
	public SourceSet getDomainSourceSet() {
		SourceSet set = new SourceSet();
		for (Mapping map : this)
			if (!set.contains(map.getDomainSource()))
				set.addSource(map.getDomainSource());
		return set;
	}
	
	/**
	 * The method returns a set of all source versions used in the range of the mappings in the map.
	 * @return set of range sources
	 */
	public SourceSet getRangeSourceSet() {
		SourceSet set = new SourceSet();
		for (Mapping map : this)
			if (!set.contains(map.getRangeSource()))
				set.addSource(map.getRangeSource());
		return set;
	}
	
	/**
	 * @see Object#hashCode()
	 */
	public int hashCode() {
		int result = 0;
		for (Mapping m : this)
			result += m.hashCode();
		return result;
	}
}
