package org.gomma.model.mapping;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public final class MappingVersion extends AbstractGenericObject {

	public static final int DEFAULT_MAPPING_VERSION_ID = 1;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2322858696090234736L;

	private Mapping mapping;
	
	private SimplifiedGregorianCalendar creationDate;
	
	/**
	 * The domainSourceVersions represents the set of source version of the domain of the mapping.
	 */
	public SourceVersionSet domainSourceVersionSet;
	
	/**
	 * The rangeSourceVersions represents the set of source version of the range of the mapping.
	 */
	public SourceVersionSet rangeSourceVersionSet;
	
	/**
	 * The domainInstanceSourceVersions represents the set of versions of instance sources which 
	 * are used to generate the mapping. These instance sources versions are associated with the 
	 * source versions of mapping domain (see domainSourceVersions).
	 */
	public SourceVersionSet domainInstanceSourceVersionSet;
	
	/**
	 * The rangeInstanceSourceVersions represents the set of versions of instance sources which 
	 * are used to generate the mapping. These instance sources versions are associated with the 
	 * source versions of mapping range (see rangeSourceVersions).
	 */
	public SourceVersionSet rangeInstanceSourceVersionSet;
	
	/**
	 * The minSupport variable stores the minimum support, i.e., the minimum number of shared objects of
	 * a correspondence in that mapping. Default value is 0, i.e., all correspondences of that mapping
	 * have >=0 support or don't use the support variable.
	 */
	private int minSupport;
	
	/**
	 * The minConfidence variable stores the minimum confidence, i.e., correspondences in that mapping show
	 * at least a confidence of minConfidence. Default value is 0, i.e., all correspondences are of confidence
	 * >=0
	 */
	private float minConfidence;
	/**
	 * The number of correspondences that are stored in the mapping with respect to minConfidence
	 */
	private int numberOfCorrs;
	
	/**
	 * The number of domain objects that participate in the mapping
	 */
	private int numberOfDomainObjs;
	
	/**
	 * The number of range objects that participate in the mapping
	 */
	private int numberOfRangeObjs;

	

	private MappingVersion(Builder b) {
		super(b.id);
		super.setName(b.name);
		
		this.mapping = b.map;
		this.creationDate = b.creationDate;
		this.domainSourceVersionSet = new SourceVersionSet();
		this.rangeSourceVersionSet = new SourceVersionSet();
		this.domainInstanceSourceVersionSet = new SourceVersionSet();
		this.rangeInstanceSourceVersionSet = new SourceVersionSet();
		
		// statistics of the mapping version 
		this.minConfidence = b.minConf;
		this.minSupport = b.minSup;
		this.numberOfCorrs = b.nCorrs;
		this.numberOfDomainObjs = b.nDomainObjs;
		this.numberOfRangeObjs = b.nRangeObjs;
	}
	
	public Mapping getMapping() {
		return this.mapping;
	}
	
	public SimplifiedGregorianCalendar getCreationDate() {
		return this.creationDate;
	}
	
	public SourceVersionSet getDomainSourceVersionSet() {
		return this.domainSourceVersionSet;
	}
	
	public SourceVersionSet getRangeSourceVersionSet() {
		return this.rangeSourceVersionSet;
	}
	
	public SourceVersionSet getDomainInstanceSourceVersionSet() {
		return this.domainSourceVersionSet;
	}
	
	public SourceVersionSet getRangeInstanceSourceVersionSet() {
		return this.rangeSourceVersionSet;
	}
	
	public void addDomainSourceVersion(SourceVersion v) {
		this.domainSourceVersionSet.addSourceVersion(v);
	}
	
	public void addRangeSourceVersion(SourceVersion v) {
		this.rangeSourceVersionSet.addSourceVersion(v);
	}
	
	
	
	public float getLowestConfidenceValue() {
		return this.minConfidence;
	}
	
	public int getLowestSupportValue() {
		return this.minSupport;
	}
	
	public int getNumberOfCorrespondences() {
		return this.numberOfCorrs;
	}
	
	public int getNumberOfDomainObjects() {
		return this.numberOfDomainObjs;
	}
	
	public int getNumberOfRangeObjects() {
		return this.numberOfRangeObjs;
	}
	
	
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -6898276346152260431L;
		
		private final int id;
		private final Mapping map;
		
		private String name = "N/A";
		private SimplifiedGregorianCalendar creationDate = DefaultVersionStartDate.getInstance().getDate();
		
		private float minConf = 0F;
		private int minSup = 0;
		private int nCorrs = 0;
		private int nDomainObjs = 0;
		private int nRangeObjs  = 0;
		
		public Builder(int id, Mapping map) {
			this.id=id;
			this.map = map;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder creationDate(SimplifiedGregorianCalendar cal) {
			this.creationDate = cal;
			return this;
		}
		
		public Builder lowestConfidence(float conf) {
			this.minConf = conf;
			return this;
		}
		
		public Builder lowestSupport(int sup) {
			this.minSup = sup;
			return this;
		}
		
		public Builder numberOfCorrespondences(int n) {
			this.nCorrs = n;
			return this;
		}
		
		public Builder numberOfDomainObjects(int n) {
			this.nDomainObjs = n;
			return this;
		}
		
		public Builder numberOfRangeObjects(int n) {
			this.nRangeObjs = n;
			return this;
		}
		
		public MappingVersion build() {
			return new MappingVersion(this);
		}
	}
}
