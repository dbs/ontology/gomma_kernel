/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * changes: --
 * 
 **/

package org.gomma.model.mapping;

//import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.IDObject;
import org.gomma.model.IDObjectSet;
import org.gomma.util.ObjCorrespondenceSorter;
import org.gomma.util.math.CantorDecoder;

/**
 * The class ObjCorrespondences represents a set of object correspondences, typically of an 
 * available mapping.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class ObjCorrespondenceSet extends Object implements Serializable, Iterable<ObjCorrespondence> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2006220662126129637L;

	private static final int DEFAULT_LDSID = -1;
	
	/**
	 * The correspondenceMap represents the set of object correspondences. The correspondences 
	 * are indexed by a generated number.
	 * @see java.util.HashMap
	 * @see org.gomma.model.mapping.ObjCorrespondence
	 */
	private Map<Long,ObjCorrespondence> correspondenceMap;
	
	/**
	 * The domainCorrMap represents the set of domain objects and their usage in the set 
	 * of object correspondences, i.e., the id of each domain object is associated with
	 * the set of correspondence IDs.
	 * @see java.util.Map
	 * @see java.util.Set 
	 */
	private Map<Integer,Set<Long>> domainCorrMap;
	
	/**
	 * The rangeCorrMap represents the set of range objects and their usage in the set 
	 * of object correspondences, i.e., the id of each range object is associated with
	 * the set of correspondence IDs.
	 * @see java.util.Map
	 * @see java.util.Set
	 */
	private Map<Integer,Set<Long>> rangeCorrMap;
	
	/**
	 * The domainSourceVersions represents the source versions of the domain objects.
	 * @see org.gomma.model.source.SourceVersionSet
	 * 
	 */
	private Set<Integer> domainSourceVersionIdSet;
	
	/**
	 * The domainLogicalSourceId uniquely identifies the logical source of domain objects.
	 */
	private int domainLogicalSourceId;
	
	/**
	 * The rangeSourceVersions represents the source versions of the range objects.
	 * @see org.gomma.model.source.SourceVersionSet
	 * 
	 */
	private Set<Integer> rangeSourceVersionIdSet;
	
	/**
	 * The rangeLogicalSourceId uniquely identifies the logical source of range objects.
	 */
	private int rangeLogicalSourceId;
	

	
	
	/**
	 * The constructor initializes the class ObjCorrespondences.
	 */
	public ObjCorrespondenceSet() {
		this.correspondenceMap = new Object2ObjectOpenHashMap<Long,ObjCorrespondence>();
		this.domainCorrMap= new Object2ObjectOpenHashMap<Integer,Set<Long>>();
		this.rangeCorrMap = new Object2ObjectOpenHashMap<Integer,Set<Long>>();
		this.domainLogicalSourceId=DEFAULT_LDSID;
		this.rangeLogicalSourceId=DEFAULT_LDSID;
		this.domainSourceVersionIdSet=new HashSet<Integer>();
		this.rangeSourceVersionIdSet=new HashSet<Integer>();
	}
	
	/**
	 * The method adds a object correspondence to the map.
	 * @param o object correspondence to be added
	 * @see org.gomma.model.mapping.ObjCorrespondence
	 */
	public void addCorrespondence(ObjCorrespondence corr) throws WrongSourceException {
		
		if (corr == null) throw new NullPointerException("Could not resolve the correspondence that should be added.");
		
		if (this.domainLogicalSourceId==DEFAULT_LDSID) this.domainLogicalSourceId = corr.getDomainLDSID();
		if (this.rangeLogicalSourceId==DEFAULT_LDSID)  this.rangeLogicalSourceId = corr.getRangeLDSID();
		
		if (this.domainLogicalSourceId!=corr.getDomainLDSID())
			throw new WrongSourceException("Could not add object correspondence since the logical source of domain objects are different.");
		
		if (this.rangeLogicalSourceId!=corr.getRangeLDSID())
			throw new WrongSourceException("Could not add object correspondence since the logical source of range objects are different.");

		for (int sourceVersionID : corr.getDomainSourceVersionIDSet()) this.domainSourceVersionIdSet.add(sourceVersionID);
		for (int sourceVersionID : corr.getRangeSourceVersionIDSet())  this.rangeSourceVersionIdSet.add(sourceVersionID);
		
		long corrID = this.getCorrespondenceID(corr.getDomainObjID(),corr.getRangeObjID());
		this.correspondenceMap.put(corrID,corr);
		
		Set<Long> set;
		if (this.domainCorrMap.containsKey(corr.getDomainObjID())) set = this.domainCorrMap.get(corr.getDomainObjID());
		else set = new HashSet<Long>();
		set.add(corrID);
		this.domainCorrMap.put(corr.getDomainObjID(),set);
		
		if (this.rangeCorrMap.containsKey(corr.getRangeObjID())) set = this.rangeCorrMap.get(corr.getRangeObjID());
		else set = new HashSet<Long>();
		set.add(corrID);
		this.rangeCorrMap.put(corr.getRangeObjID(),set);
	}
	
	/**
	 * The method adds a set of object correspondences to the currently managed set. 
	 * @param objCorrs set of object correspondences that should be added
	 * @throws WrongSourceException
	 */
	public void addCorrespondences(ObjCorrespondenceSet set) throws WrongSourceException {
		for (ObjCorrespondence corr : set.getCollection())
			this.addCorrespondence(corr);
	}
	/**
	 * The method removes an object correspondence from the map.
	 * @param o object correspondence to be added
	 * @see org.gomma.model.mapping.ObjCorrespondence
	 */
	public void removeCorrespondence(ObjCorrespondence corr) throws WrongSourceException {
		
		if (corr == null) throw new NullPointerException("Could not resolve the correspondence that should be removed.");
		
		if (this.domainLogicalSourceId==DEFAULT_LDSID) this.domainLogicalSourceId = corr.getDomainLDSID();
		if (this.rangeLogicalSourceId==DEFAULT_LDSID)  this.rangeLogicalSourceId = corr.getRangeLDSID();
		
		if (this.domainLogicalSourceId!=corr.getDomainLDSID())
			throw new WrongSourceException("Could not remove object correspondence since the logical source of domain objects are different.");
		
		if (this.rangeLogicalSourceId!=corr.getRangeLDSID())
			throw new WrongSourceException("Could not remove object correspondence since the logical source of range objects are different.");

		for (int sourceVersionID : corr.getDomainSourceVersionIDSet()) this.domainSourceVersionIdSet.remove(sourceVersionID);
		for (int sourceVersionID : corr.getRangeSourceVersionIDSet())  this.rangeSourceVersionIdSet.remove(sourceVersionID);
		
		long corrID = this.getCorrespondenceID(corr.getDomainObjID(),corr.getRangeObjID());
		this.correspondenceMap.remove(corrID);
		
		if (this.domainCorrMap.containsKey(corr.getDomainObjID())) this.domainCorrMap.remove(corr.getDomainObjID());		
		if (this.rangeCorrMap.containsKey(corr.getRangeObjID())) this.rangeCorrMap.remove(corr.getRangeObjID());
	}
	/**
	 * This method returns the object correspondence for the specified unique identifiers.
	 * @param domainID unique domain object identifier
	 * @param rangeID unique range object identifier
	 * @return object correspondence
	 */
	public ObjCorrespondence getCorrespondence(int domainID, int rangeID) throws NoSuchElementException {
		long corrID = this.getCorrespondenceID(domainID,rangeID);
		if (this.correspondenceMap.containsKey(corrID)) return this.correspondenceMap.get(corrID);
		else throw new NoSuchElementException("Could not find a correspondence between both objects ("+domainID+","+rangeID+").");
	}
	
	public ObjCorrespondence getCorrespondence(long corrID) throws NoSuchElementException {		
		if (this.correspondenceMap.containsKey(corrID)) return this.correspondenceMap.get(corrID);
		else throw new NoSuchElementException("Could not find a correspondence with corrID "+corrID+".");
	}
	
	
	/**
	 * The method returns true if a correspondence between both objects exists;
	 * otherwise the method returns false.
	 * @param domainID unique domain object identifier
	 * @param rangeID unique range object identifier
	 * @return true/false
	 */
	public boolean containsCorrespondence(int domainID, int rangeID) {
		return this.correspondenceMap.containsKey(this.getCorrespondenceID(domainID,rangeID));
	}
	
	public boolean containsCorrespondences(int objID, boolean isDomain) {
		if(this.getCorrespondenceSet(objID, isDomain).size()>0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * The method returns a subset of correspondences for the given domain/range object identifier.
	 * @param objID object identifier
	 * @param isDomain true if the object identifier belongs to an object in the domain; false if the 
	 * object identifier belongs to an object in the range 
	 * @return correspondence subset
	 */
	public ObjCorrespondenceSet getCorrespondenceSet(int objID, boolean isDomain) {
		ObjCorrespondenceSet resultSet = new ObjCorrespondenceSet();
		//System.out.println(this.domainCorrMap.get(objID));
		//System.out.println(this.rangeCorrMap.get(objID));
		try {
			if (this.domainCorrMap.get(objID)!=null || this.rangeCorrMap.get(objID)!=null) {
				for (long corrID : isDomain?this.domainCorrMap.get(objID):this.rangeCorrMap.get(objID))
				resultSet.addCorrespondence(this.correspondenceMap.get(corrID));
			}
			
		} catch (WrongSourceException e) {
			// should not happen since we create a sub list
		}
		return resultSet;
	}
	
	/**
	 * The method compares the current with an external object correspondence set. It returns true
	 * if both sets contains the correspondences. Otherwise, it returns false.
	 * @param o object correspondence set to compare with
	 * @return true/false
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjCorrespondenceSet)) return false;
		if (((ObjCorrespondenceSet)o).correspondenceMap.size()!=this.correspondenceMap.size()) return false;
		for (ObjCorrespondence corr : ((ObjCorrespondenceSet)o).correspondenceMap.values())
			if (!this.correspondenceMap.containsValue(corr)) return false;
		return true;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString() {
		StringBuilder buf = new StringBuilder(this.size()*2);
		for (ObjCorrespondence c : this.getCollection())
			buf.append(c.toString()).append("\n");
		return buf.toString();
	}
	
	public Set<Integer> getCorrespondingDomainObjIDSet(int rangeObjID) {
		Set<Integer> set = new HashSet<Integer>();
		
		if(this.rangeCorrMap.get(rangeObjID)==null){
			return set;
		}	
		
		for (long corID : this.rangeCorrMap.get(rangeObjID))
			set.add(this.correspondenceMap.get(corID).getDomainObjID());
		
		return set;
	}
	
	public Set<Integer> getCorrespondingRangeObjIDSet(int domainObjID) {
		Set<Integer> set = new HashSet<Integer>();
		
		if(this.domainCorrMap.get(domainObjID)==null){
			return set;
		}		
		for (long corID : this.domainCorrMap.get(domainObjID))
			set.add(this.correspondenceMap.get(corID).getRangeObjID());
		
		return set;
	}
	
	/**
	 * The method returns the set of object identifiers of the domain of the correspondence set.
	 * @return ID objects for the domain of the correspondence set
	 */
	public IDObjectSet getDomainIDObjectSet() {
		IDObjectSet set = new IDObjectSet();
		
		for (int id : this.domainCorrMap.keySet())
			set.addElement(new IDObject(id));
		
		return set;
	}
	
	/**
	 * The method returns the set of object identifiers of the range of the correspondence set.
	 * @return ID objects for the range of the correspondence set
	 */
	public IDObjectSet getRangeIDObjectSet() {
		IDObjectSet set = new IDObjectSet();
		
		for (int id : this.rangeCorrMap.keySet())
			set.addElement(new IDObject(id));
		
		return set;
	}
	
	public Set<Integer> getDomainSourceVersionIDSet() {
		return this.domainSourceVersionIdSet;
	}
	
	public Set<Integer> getRangeSourceVersionIDSet() {
		return this.rangeSourceVersionIdSet;
	}
	
	/**
	 * The method returns the current set of object correspondences.
	 * @return current set of object correspondences
	 */
	public Collection<ObjCorrespondence> getCollection() {
		return this.correspondenceMap.values();
	}
	
	/**
	 * The method returns a set of object correspondences for the specified domain object identifier
	 * @param domainObjID domain object identifier
	 * @return set of object correspondences for the specified domain object identifier
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet filterByDomainObj(int domainObjID) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		for (long corrID : this.domainCorrMap.get(domainObjID))
			result.addCorrespondence(this.correspondenceMap.get(corrID));
		
		return result;
	}
	
	/**
	 * The method returns a set of object correspondences for the specified range object identifier
	 * @param rangeObjID range object identifier
	 * @return set of object correspondences for the specified range object identifier
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet filterByRangeObj(int rangeObjID) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		for (long corrID : this.domainCorrMap.get(rangeObjID))
			result.addCorrespondence(this.correspondenceMap.get(corrID));
		
		return result;
	}
	
	/**
	 * The method returns a list of correspondences that is sorted by 
	 * similarity value in ascending order. 
	 * @return sorted list of object correspondences 
	 */
	public ObjCorrespondence[] getAscendingSimilarityOrderedArray() {
		ObjCorrespondenceSorter sorter = new ObjCorrespondenceSorter(this);
		return sorter.getAscendingSimilarityOrderedArray();
	}
	/**
	 * The method returns a list of correspondences that is sorted by 
	 * similarity value in descending order. 
	 * @return sorted list of object correspondences 
	 */
	public ObjCorrespondence[] getDescendingSimilarityOrderedArray() {
		ObjCorrespondenceSorter sorter = new ObjCorrespondenceSorter(this);
		return sorter.getDescendingSimilarityOrderedArray();
	}
	/**
	 * The method returns the set of domain object identifiers. 
	 * @return set of domain object identifiers
	 */
	public Set<Integer> getDomainObjIds() {
		Set<Integer> s = new HashSet<Integer>();
		for (int id : this.domainCorrMap.keySet())
			s.add(id);
		return s;
	}
	
	/**
	 * The method returns the set of range object identifiers. 
	 * @return set of range object identifiers
	 */
	public Set<Integer> getRangeObjIds() {
		Set<Integer> s = new HashSet<Integer>();
		for (int id : this.rangeCorrMap.keySet())
			s.add(id);
		return s;
	}
	
	public int getDomainLDSID() {
		return this.domainLogicalSourceId;
	}
	
	public int getRangeLDSID() {
		return this.rangeLogicalSourceId;
	}
	
	/**
	 * The method returns the number of correspondences within the set.
	 * @return number of correspondences within the set
	 */
	public int size() {
		return this.correspondenceMap.size();
	}
	
	/**
	 * The method returns the lowest support value that a correspondence 
	 * within the current set of object correspondences has. 
	 * @return lowest support value
	 */
	public int getLowestSupport() {
		int result = Integer.MAX_VALUE;
		
		for (ObjCorrespondence c : this.correspondenceMap.values()) {
			if (result>c.getSupport()) result = c.getSupport();
		}
		
		return result;
	}
	
	/**
	 * The method returns the lowest confidence value that a correspondence 
	 * within the current set of object correspondences has. 
	 * @return lowest confidence value
	 */
	public float getLowestConfidence() {
		float result = Float.MAX_VALUE;
		
		for (ObjCorrespondence c : this.correspondenceMap.values()) {
			if (result>c.getConfidence()) result = c.getConfidence();
		}
		return result;
	}
	/**
	 * The method returns the highest confidence value that a correspondence 
	 * within the current set of object correspondences has. 
	 * @return highest confidence value
	 */
	public float getHighestConfidence() {
		float result = Float.MIN_VALUE;
		
		for (ObjCorrespondence c : this.correspondenceMap.values()) {
			if (result<c.getConfidence()) result = c.getConfidence();
		}
		return result;
	}
	/**
	 * The method returns the average confidence value 
	 * for the current set of object correspondences. 
	 * @return average confidence value
	 */
	public float getAverageConfidence() {
		return getSumConfidence()/((float)this.correspondenceMap.size());
	}	
	/**
	 * The method returns the sum of confidences  
	 * within the current set of object correspondences. 
	 * @return sum confidence value
	 */
	public float getSumConfidence() {
		float result = 0.0F;	
		for (ObjCorrespondence c : this.correspondenceMap.values()) {
			result += c.getConfidence();
		}
		return result;
	}	
	/**
	 * The method returns the correspondence(s) with confidence value higher than the threshold 
	 * within the current set of object correspondences. 
	 * @return filtered ObjCorrespondenceSet
	 * @throws WrongSourceException 
	 */
	public ObjCorrespondenceSet filterByConfidence(float threshold) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();	
		for (ObjCorrespondence c : this.correspondenceMap.values()) {
			if(c.getConfidence() >= threshold){
				result.addCorrespondence(c);
			}
		}
		return result;
	}	

	/**
	 * The method returns the unique correspondence identifier. 
	 * @param domainID unique domain object identifier
	 * @param rangeID unique range object identifier
	 * @return unique correspondence identifier
	 */
	public long getCorrespondenceID(int domainID, int rangeID) {
		return CantorDecoder.code(domainID,rangeID);
	}

	public Iterator<ObjCorrespondence> iterator() {
		return this.correspondenceMap.values().iterator();
	}
}
