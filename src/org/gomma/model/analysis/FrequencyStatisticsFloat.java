package org.gomma.model.analysis;

import java.io.Serializable;

public class FrequencyStatisticsFloat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3493299690542436063L;
	
	private final float rank;
	private final int freq;
	
	private FrequencyStatisticsFloat(Builder b) {
		this.rank = b.rank;
		this.freq = b.freq;
	}
	
	public float getRank() {
		return rank;
	}

	public int getFreq() {
		return freq;
	}




	public static class Builder {
		
		private final float rank;
		private final int freq;
		
		public Builder(float rank, int frequency) {
			this.rank = rank;
			this.freq = frequency;
		}
		
		public FrequencyStatisticsFloat build() {
			return new FrequencyStatisticsFloat(this);
		}
	}
}
