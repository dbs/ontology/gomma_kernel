package org.gomma.model.analysis;

import java.io.Serializable;

public class FrequencyStatisticsInteger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1719585493482805534L;
	
	private final int rank;
	private final int freq;
	
	private FrequencyStatisticsInteger(Builder b) {
		this.rank = b.rank;
		this.freq = b.freq;
	}
	
	public int getRank() {
		return rank;
	}

	public int getFreq() {
		return freq;
	}




	public static class Builder {
		
		private final int rank;
		private final int freq;
		
		public Builder(int rank, int frequency) {
			this.rank = rank;
			this.freq = frequency;
		}
		
		public FrequencyStatisticsInteger build() {
			return new FrequencyStatisticsInteger(this);
		}
	}
}
