/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * changes: --
 **/

package org.gomma.model.source;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The class SourceVersion represents a specific version of a logical source. The version is specified by 
 * a version number, such as 3.4.1, and a version date at which the source was valid. A logical source
 * is portion of a physical source that represents a data of semantic object type, such as function, 
 * processes, genes, and proteins. A physical sources can be any available data source, such as files, 
 * relational and XML databases.   
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceVersion extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6707969386538666727L;

	/**
	 * The source identifies the logical source of the version.
	 */
	private Source source;
	
	/**
	 * The sourceVersion represents the version of a logical source, such as 3.4.1
	 */
	private String version;
	
	/**
	 * The sourceVersionDate represents the date of the version of a logical source.
	 */
	private SimplifiedGregorianCalendar versionDate;
	
	/**
	 * The item represents the structural graph type of the source version.
	 */
	private GraphTypes sourceVersionStructureType;

		
	/**
	 * The constructor initializes the class. The sourceVersionDate is set to 2000-01-01. 
	 * @param id source version id
	 */
	private SourceVersion(Builder b) {
		super(b.id);
		this.source = b.source;
		this.version = b.version;
		this.versionDate = b.creationDate;
		this.sourceVersionStructureType=b.graphType;
		
		super.setName(this.source.getName()+"["+this.getVersion()+"]");
	}
	
	public Source getSource() {
		return this.source;
	}
	
	public SimplifiedGregorianCalendar getCreationVersionDate() {
		return this.versionDate;
	}
	
	
	public String getVersion() {
		return this.version;
	}
	
	/**
	 * The method returns the structural type of the source version.
	 * @return structural type of source version
	 */
	public GraphTypes getStructuralType() {
		return this.sourceVersionStructureType;
	}
	
	
	/**
	 * The method compares the current with an external source version. It returns true
	 * if the identifiers of both source versions are the same. Otherwise, it returns false.
	 * @param o source version to compare with
	 * @return true/false
	 */
	public boolean equals(Object o) {
		if (!(o instanceof SourceVersion)) return false;
		return ((SourceVersion)o).getID()==super.getID();
	}
	
	public String toString() {
		return "Source version '"+this.getName()+"' ("+super.getID()+")";
	}
	

	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 3668825783164050975L;
		
		private final int id;
		private final Source source;
		
		private String version = "N/A";
		private SimplifiedGregorianCalendar creationDate = DefaultVersionStartDate.getInstance().getDate();
		private GraphTypes graphType = GraphTypes.UNDIRECTED;
		
		public Builder(Source source, int versionID) {
			this.source = source;
			this.id = versionID;
		}
		
		public Builder version(String v) {
			this.version = v;
			return this;
		}
		
		public Builder creationDate(SimplifiedGregorianCalendar date) {
			this.creationDate = date;
			return this;
		}
		
		public Builder graphType(GraphTypes type) {
			this.graphType = type;
			return this;
		}
		
		public SourceVersion build() {
			return new SourceVersion(this);
		}
	}
}
