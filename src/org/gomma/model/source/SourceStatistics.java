package org.gomma.model.source;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;

public final class SourceStatistics extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4937167458181280403L;
	
	private Source source;
	private int numVersions;
	private float avgNumConcepts;
	private float avgNumRelations;
	private String firstVersionName;
	private int numConceptsFirstVersion;
	private int numRelationsFirstVersion;
	private String lastVersionName;
	private int numConceptsLastVersion;
	private int numRelationsLastVersion;
	
	private SourceStatistics(Builder b) {
		super(b.source.getID());
		
		this.source = b.source;
		
		this.numVersions = b.numVersions;
		this.avgNumConcepts = b.avgNumConcepts;
		this.avgNumRelations = b.avgNumRelations;
		
		this.firstVersionName = b.firstVersionName;
		this.numConceptsFirstVersion = b.numConceptsFirstVersion;
		this.numRelationsFirstVersion= b.numRelationsFirstVersion;
		
		this.lastVersionName = b.lastVersionName;
		this.numConceptsLastVersion  = b.numConceptsLastVersion;
		this.numRelationsLastVersion = b.numRelationsLastVersion;
	}
	
	public Source getSource() {
		return this.source;
	}
	
	public int getNumberOfVersions() {
		return this.numVersions;
	}
	
	public float getAverageNumberOfConcepts() {
		return this.avgNumConcepts;
	}
	
	public float getAverageNumberOfRelations() {
		return this.avgNumRelations;
	}
	
	public String getVersionOfFirstVersion() {
		return this.firstVersionName;
	}
	
	public int getNumberOfConceptsOfFirstVersion() {
		return this.numConceptsFirstVersion;
	}
	
	public int getNumberOfRelationsOfFirstVersion() {
		return this.numRelationsFirstVersion;
	}
	
	public String getVersionOfLastVersion() {
		return this.lastVersionName;
	}
	
	public int getNumberOfConceptsOfLastVersion() {
		return this.numConceptsLastVersion;
	}
	
	public int getNumberOfRelationsOfLastVersion() {
		return this.numRelationsLastVersion;
	}
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 6008915981064164362L;

		private final Source source;
		
		private int numVersions = 0;
		private float avgNumConcepts = 0F;
		private float avgNumRelations= 0F;
		
		private String firstVersionName = "N/A";
		private int numConceptsFirstVersion = 0;
		private int numRelationsFirstVersion= 0;
		
		private String lastVersionName = "N/A";
		private int numConceptsLastVersion  = 0;
		private int numRelationsLastVersion = 0;
		
		public Builder(Source s) {
			this.source = s;
		}
		
		public Builder numberOfVersions(int n) {
			this.numVersions = n;
			return this;
		}
		
		public Builder averageNumberOfConcepts(float avgNum) {
			this.avgNumConcepts = avgNum;
			return this;
		}
		
		public Builder averageNumberOfRelations(float avgNum) {
			this.avgNumRelations = avgNum;
			return this;
		}
		
		public Builder firstVersionName(String name) {
			this.firstVersionName = name;
			return this;
		}
		
		public Builder numberOfConceptsInFirstVersion(int n) {
			this.numConceptsFirstVersion = n;
			return this;
		}
		
		public Builder numberOfRelatonsFirstVersion(int n) {
			this.numRelationsFirstVersion = n;
			return this;
		}
		
		public Builder lastVersionName(String name) {
			this.lastVersionName = name;
			return this;
		}
		
		public Builder numberOfConceptsInLastVersion(int n) {
			this.numConceptsLastVersion = n;
			return this;
		}
		
		public Builder numberOfRelatonsLastVersion(int n) {
			this.numRelationsLastVersion = n;
			return this;
		}
		
		public SourceStatistics build() {
			return new SourceStatistics(this);
		}
	}
}
