/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/
package org.gomma.model.source;

import java.io.Serializable;

import org.gomma.model.DefaultVersionEndDate;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.EdgeImpl;

/**
 * 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class ObjRelationship extends EdgeImpl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2301557175663016535L;

	/**
	 * The fromDate represents the date from which the relationship is valid.
	 */
	private SimplifiedGregorianCalendar fromDate;
	
	/**
	 * The toDate represents the date to which the relationship is valid.
	 */
	private SimplifiedGregorianCalendar toDate;
	
	/**
	 * The isDirected represents the relationship directness. It is true if the 
	 * relationship is directed; otherwise it is false.   
	 */
	private boolean isDirected;

	/**
	 * The relationshipType represents the relationship type, e.g. is_a, part_of ...
	 */

	//private String relationshipType;
	
	/**
	 * The constructor initializes the class SourceVersionStructureRelationship. The fromDate is set 
	 * to 1900-01-10 while the toDate is set to 2999-01-01. Per default a relationships is not 
	 * bidirectional. 
	 */
	private ObjRelationship(Builder b) {
		super(b.fromID,b.toID);
		
		this.fromDate = b.fromDate;
		this.toDate = b.toDate;
		this.isDirected = b.isDirected;
		this.setRelationshipType(b.relationshipType);
	}

	public SimplifiedGregorianCalendar getFromDate() {
		return this.fromDate;
	}
	
	public SimplifiedGregorianCalendar getToDate() {
		return this.toDate;
	}
	
	public boolean isDirected() {
		return this.isDirected;
	}
	
	/**
	 * The method compares the current with an external relationship. It returns true if both 
	 * relationships connect the same objects and are of the same type.
	 * @param o relationship to be compared with
	 * @return true/false
	 */
	public boolean equals(Object o) {
		if (!(o instanceof ObjRelationship)) return false;
		ObjRelationship rel = (ObjRelationship)o;
		return super.getFromID() == rel.getFromID() &&
			   super.getToID()   == rel.getToID() &&
			   super.getRelationshipType().equals(rel.getRelationshipType());
	}
	
	
	
	public static class Builder implements Serializable {
		public String relationshipType;

		/**
		 * 
		 */
		private static final long serialVersionUID = -1591493844847017154L;

		private final int fromID, toID;
		
		private SimplifiedGregorianCalendar fromDate = DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar toDate   = DefaultVersionEndDate.getInstance().getDate();
		private boolean isDirected = false;
		
		public Builder(int fromObjID, int toObjID) {
			this.fromID = fromObjID;
			this.toID = toObjID;
		}
		
		public Builder fromDate(SimplifiedGregorianCalendar date) {
			this.fromDate=date;
			return this;
		}
		
		public Builder toDate(SimplifiedGregorianCalendar date) {
			this.toDate=date;
			return this;
		}
		
		public Builder isDirected(boolean isDirected) {
			this.isDirected=isDirected;
			return this;
		}
		public Builder relationshipType(String relationshipType) {
			this.relationshipType=relationshipType;
			return this;
		}
		public ObjRelationship build() {
			return new ObjRelationship(this);
		}
	}
}
