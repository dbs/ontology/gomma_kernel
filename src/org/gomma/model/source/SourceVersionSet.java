/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 * 
 * creation date: 2007/11/10
 * changes:  -- 
 * 
 **/

package org.gomma.model.source;

import java.io.Serializable;
import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class SourceVersions collects source versions. The source versions are indexed using 
 * the version IDs.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceVersionSet extends AbstractGenericObjectSet<SourceVersion> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3978961309936111991L;
	
	
	/**
	 * The constructor of the SourceVersions class initializes the map.
	 */
	public SourceVersionSet() {
		super();
	}
	
	/**
	 * The method adds the specified source version to the map.
	 * @param s source version to be added
	 * @see org.gomma.model.source.SourceVersion
	 */
	public void addSourceVersion(SourceVersion s) {
		if (s==null) throw new NullPointerException("Could not resolve source version.");
		super.addElement(s);
	}
	
	/**
	 * The method returns the first (earliest) source version object. If there is no 
	 * source version within the version set the method then returns null.
	 * @return earliest source version object
	 */
	public SourceVersion getEarliestSourceVersion() {
		SourceVersion result = null;
		for (SourceVersion v : this) {
			if (result==null) {
				result=v;
			} else {
				if (v.getCreationVersionDate().before(result.getCreationVersionDate())) 
					result=v;
			}
		}
		return result;
	}
	
	/**
	 * The method returns the last (latest) source version object. If there is no 
	 * source version within the version set the method then returns null. 
	 * @return latest source version object
	 */
	public SourceVersion getLatestSourceVersion() {
		SourceVersion result = null;
		for (SourceVersion v : this) {
			if (result==null) {
				result=v;
			} else {
				if (v.getCreationVersionDate().after(result.getCreationVersionDate())) 
					result=v;
			}
		}
		return result;
	}
	
	/**
	 * The method returns the source version those creation date is next older
	 * than the given calendar date. If there is no older source version within 
	 * the version set the method then returns null.
	 * @param cal
	 * @return
	 */
	public SourceVersion getNextOlderSourceVersion(SimplifiedGregorianCalendar cal) {
		SourceVersion result = null;
		
		for (SourceVersion v : this) {
			if (v.getCreationVersionDate().after(cal)) {
				
				if (result==null) {
					result = v;
					continue;
				}
				
				if (result.getCreationVersionDate().after(v.getCreationVersionDate()))
					result = v; // overwrite result only iff v is older than result
			}
		}
		return result;
	}
	
	/**
	 * The method returns the source version object by its name. The name parameter
	 * utilizes the following form 
	 *     <object type>@<physical data source name>[<version>]
	 * 
	 * @param name complete name of the source version
	 * @return source version object meeting the specified name
	 * @throws NoSuchElementException
	 */
	public SourceVersion getSourceVersion(String name) throws NoSuchElementException {
		for (SourceVersion s : this) {
			if (s.getName().equals(name)) return s;
		}
		throw new NoSuchElementException("Could not find source version with name '"+name+"'.");
	}

	public SourceVersion getSourceVersion(int sourceVersionID) throws NoSuchElementException {
		if (!super.contains(sourceVersionID))
			throw new NoSuchElementException("Could not find source version of ID '"+sourceVersionID+"'.");
		return super.getElement(sourceVersionID);
	}
	
	public Source getSource(int ldsID) throws NoSuchElementException {
		for (SourceVersion s : this)
			if (s.getSource().getID()==ldsID) return s.getSource();
		throw new NoSuchElementException("Could not resolve the logical data source for id '"+ldsID+"'.");
	}
	
	
//	/**
//	 * The method returns the logical source name for specified logical source id.
//	 * @param ldsID logical source identifier
//	 * @return logical source name
//	 * @throws NoSuchElementException
//	 */
//	public String getLogicalSourceName(int ldsID) throws NoSuchElementException {
//		for (SourceVersion s : super.getCollection())
//			if (s.getSource().getID()==ldsID) return s.getSource().getName();
//		throw new NoSuchElementException("Could not resolve the logical data source for id "+ldsID+".");
//	}
//	
//	/**
//	 * The method returns the semantic object type for the specified LDS identifier. 
//	 * @param ldsID logical source identifier
//	 * @return semantic object type
//	 * @throws NoSuchElementException
//	 */
//	public String getObjectType(int ldsID) throws NoSuchElementException {
//		for (SourceVersion s : super.getCollection())
//			if (s.getLDSID()==ldsID) return s.getObjectType();
//		throw new NoSuchElementException("Could not resolve the logical data source for id "+ldsID+".");
//	}
//	
//	/**
//	 * The method returns the physical source name for the specified LDS identifier.
//	 * @param ldsID logical source identifier
//	 * @return physical source name
//	 * @throws NoSuchElementException
//	 */
//	public String getPhysicalSourceName(int ldsID) throws NoSuchElementException {
//		for (SourceVersion s : super.getCollection())
//			if (s.getLDSID()==ldsID) return s.getPhysicalSourceName();
//		throw new NoSuchElementException("Could not resolve the logical data source for id "+ldsID+".");
//	}
}
