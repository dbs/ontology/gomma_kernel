package org.gomma.model.source;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;

public final class SourceVersionStatistics extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3437200022560602795L;
	
	private SourceVersion version;
	private int numObjects;
	private int numRelations;
	
	private SourceVersionStatistics(Builder b) {
		super(b.version.getID());
		
		this.version = b.version;
		this.numObjects = b.numObjects;
		this.numRelations = b.numRelations;
	}
	
	public SourceVersion getSourceVersion() {
		return this.version;
	}
	
	public int getNumberOfObjects() {
		return this.numObjects;
	}
	
	public int getNumberOfrelationships() {
		return this.numRelations;
	}
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 784759667586630424L;
		
		private final SourceVersion version;
		private int numObjects = 0;
		private int numRelations = 0;
		
		public Builder(SourceVersion v) {
			this.version = v;
		}
		
		public Builder numberOfObjects(int n) {
			this.numObjects = n;
			return this;
		}
		
		public Builder numberOfRelations(int n) {
			this.numRelations = n;
			return this;
		}
		
		public SourceVersionStatistics build() {
			return new SourceVersionStatistics(this);
		}
	}
}
