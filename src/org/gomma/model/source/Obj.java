/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.model.source;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DefaultVersionEndDate;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.attribute.AttributeValueVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.Node;

/**
 * The class represents an object which is identified by a unique object id and described 
 * by a set of attributes. The natural key of the object is its object accession. Moreover,
 * the object is valid for the specified period (from,to).
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class Obj extends AbstractGenericObject implements Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3129476262051892640L;

	/**
	 * The item represents the natural key of the object that is typically used outside the system.
	 */
	private final String accession;
	
	/**
	 * The item represents the identifier of the logical source to which the object belongs. 
	 */
	private final int ldsID;

	
	/**
	 * The fromDate holds the begin of the object validity. 
	 */
	private final SimplifiedGregorianCalendar fromDate;
	
	/**
	 * The toDate holds the end of the object validity.
	 */
	private final SimplifiedGregorianCalendar toDate;
	
	/**
	 * 
	 * @see org.gomma.model.source.SourceVersion
	 */
	private SourceVersionSet sourceVersions;

	/**
	 * The item 'attributeVersions' holds all available attributes describing the object for a specific source version. 
	 * The attributes are indexed by the used source version.
	 * @see org.gomma.model.old.ObjAttributes
	 */
	private AttributeValueVersionSet attributeValueSet;
	
	private Map<Integer,Set<Integer>> sourceVersionAttributeValueMap;
	
	/**
	 * The constructor initializes the class by using the unique object id. The object validity 
	 * period is initially set from 1900-01-01 to 2099-12-31 using the gregorian calendar. 
	 * @param id unique object id
	 */
	public Obj(Builder b) {
		super(b.id);
		this.ldsID = b.ldsId;
		this.accession = b.accession;
		this.fromDate = b.fromDate;
		this.toDate = b.toDate;
		
		this.attributeValueSet = new AttributeValueVersionSet();
		this.sourceVersions = b.sourceVersionSet;
		this.sourceVersionAttributeValueMap = new HashMap<Integer,Set<Integer>>();
	}

	public int getLDSID() {
		return this.ldsID;
	}
	
	public String getAccessionNumber() {
		return this.accession;
	}
	
	public SimplifiedGregorianCalendar getFromDate() {
		return this.fromDate;
	}
	
	public SimplifiedGregorianCalendar getToDate() {
		return this.toDate;
	}
	
	public void addAttributeValue(AttributeValueVersion avv) {
		this.attributeValueSet.addAttributeValue(avv);
		
		for (SourceVersion v : this.sourceVersions)
			this.associateSourceVersionAttribute(v, avv);
	}
	public void removeAttributeValue(AttributeValueVersion avv) {
		this.attributeValueSet.removeAttributeValue(avv);
		
		for (SourceVersion v : this.sourceVersions)
			this.disassociateSourceVersionAttribute(v, avv);
	}
	
	/**
	 * The method sets all attributes of the object.
	 * @param atts attribute map
	 * @see org.gomma.model.old.ObjAttributes
	 */
	public void setAttributeValueSet(AttributeValueVersionSet set) {
		if (set==null) throw new NullPointerException("Could not resolve the attribute value set that should be set to the object.");
		
		for (AttributeValueVersion avv : set)
			this.addAttributeValue(avv);
	}
	
	public AttributeValueVersionSet getAttributeValues() {
		return this.attributeValueSet;
	}
	
	public AttributeValueVersionSet getAttributeValues(int sourceVersionId) {
		AttributeValueVersionSet set = new AttributeValueVersionSet();
		
		if (!this.sourceVersionAttributeValueMap.containsKey(sourceVersionId))
			return set;
		
		for (int id : this.sourceVersionAttributeValueMap.get(sourceVersionId))
			set.addAttributeValue(this.attributeValueSet.getValue(id));
		
		return set;
	}
	
	public AttributeValueVersionSet getAttributeValues(Attribute att) {
		AttributeValueVersionSet set = new AttributeValueVersionSet();
		
		for (AttributeValueVersion avv : this.attributeValueSet) {
			if (avv.getAttribute().equals(att)) 
				set.addAttributeValue(avv);
		}
		
		return set;
	}
	
	public AttributeValueVersionSet getAttributeValues(int sourceVersionId, Attribute att) {
		AttributeValueVersionSet set = new AttributeValueVersionSet();
		AttributeValueVersion avv;
		
		for (int id : this.sourceVersionAttributeValueMap.get(sourceVersionId)) {
			avv = this.attributeValueSet.getValue(id);
			if (avv.getAttribute().getID()==att.getID()) 
				set.addAttributeValue(avv);
		}
		
		return set;
	}
	
	public void addSourceVersion(SourceVersion v) throws WrongSourceException {
		if (v==null) throw new NullPointerException();
		if (this.ldsID!=v.getSource().getID())
			throw new WrongSourceException("Could not add new source version since it does not share the curent logical source.");
		
		this.sourceVersions.addSourceVersion(v);
		
		for (AttributeValueVersion avv : this.attributeValueSet)
			this.associateSourceVersionAttribute(v, avv);
	}
	
	public SourceVersionSet getSourceVersionDifferenceTo(Obj o) {
		SourceVersionSet result = new SourceVersionSet();
		for (SourceVersion s : this.sourceVersions) {
			if (o.getSourceVersionSet().contains(s)) continue;
			result.addSourceVersion(s);
		}
		return result;
	}
	
//	public AttributeValueVersionSet getAttributeDifferenceTo(Obj o) {
//		return .getDifferenceTo(getUnifiedAttributes(o));
//	}
	
	public boolean hasSameAttributes(Obj o) {
		AttributeValueVersionSet oAvvSet = o.getAttributeValues();
		
		if (oAvvSet.size()!=this.attributeValueSet.size()) return false;
			
		for (AttributeValueVersion avv : this.getAttributeValues())
			if (!oAvvSet.contains(avv.getID())) return false;
		
		return true;
	}
	
	/**
	 * The method compares the current with an external object. It returns true if the object identifiers
	 * (objID) are same. Otherwise it returns false.
	 * @param o object to compare with
	 * @return true/false
	 */
	public boolean equals(Object o) {
		if(!(o instanceof Obj)) return false;
		Obj obj = (Obj)o;
		return obj.getID()==super.getID();
	}
	
	public SourceVersionSet getSourceVersionSet() {
		return this.sourceVersions;
	}
	
	private void associateSourceVersionAttribute(SourceVersion v, AttributeValueVersion avv) {
		SimplifiedGregorianCalendar from = new SimplifiedGregorianCalendar(avv.getFromDate().getTimeInMillis()-1);
		SimplifiedGregorianCalendar to = new SimplifiedGregorianCalendar(avv.getToDate().getTimeInMillis()+1);
		
		if (from.before(v.getCreationVersionDate()) && to.after(v.getCreationVersionDate())) {
		
			Set<Integer> set;
			if (this.sourceVersionAttributeValueMap.containsKey(v.getID()))
				set = this.sourceVersionAttributeValueMap.get(v.getID());
			else set = new HashSet<Integer>();
		
			set.add(avv.getID());
			this.sourceVersionAttributeValueMap.put(v.getID(),set);
		}
		from=null;
		to=null;
	}
	private void disassociateSourceVersionAttribute(SourceVersion v, AttributeValueVersion avv) {
		//SimplifiedGregorianCalendar from = new SimplifiedGregorianCalendar(avv.getFromDate().getTimeInMillis()-1);
		//SimplifiedGregorianCalendar to = new SimplifiedGregorianCalendar(avv.getToDate().getTimeInMillis()+1);
		Set<Integer> set;
		if (this.sourceVersionAttributeValueMap.containsKey(v.getID()))
			set = this.sourceVersionAttributeValueMap.get(v.getID());
		else set = new HashSet<Integer>();
			
		set.remove(avv.getID());		
	}
//	private static AttributeValueVersionSet getUnifiedAttributeValues(Obj o) {
//		
//		AttributeValueVersionSet set = new AttributeValueVersionSet();
//		
//		for (AttributeValueVersion avv : o.getAttributeValues().getCollection()) 
//			set.addAttributeValue(avv);
//		
//		
//		return set;
//		if (o.attributeVersions.size()==0) return new ObjAttributes();
//		if (o.attributeVersions.size()==1) return o.attributeVersions.get(o.attributeVersions.keySet().iterator().next());
//		
//		ObjAttributes result = new ObjAttributes();
//		
//		for (ObjAttributes atts : o.attributeVersions.values()) {
//			for (String attName : atts.getAttributeNames()) {
//				if (!result.getAttributeNames().contains(attName)) {
//					for (ObjAttribute att : atts.getAttributeList(attName))
//						result.addObjAttribute(att);
//				} else {
//					for (ObjAttribute att : atts.getAttributeList(attName)) {
//						for (ObjAttribute resultAtt : result.getAttributeList(attName)) {
//							if (att.attributeName.equals(resultAtt.attributeName) &&
//									att.attributeScope.equals(resultAtt.attributeScope) &&
//									att.valueString.equals(resultAtt.valueString)) continue;
//							result.addObjAttribute(att);
//						}
//					}
//				}
//			}
//		}
//		return result;
//	}
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -4455297040169329312L;
	
		private final int id;
		private final int ldsId;
		private final String accession;
		
		private SimplifiedGregorianCalendar fromDate = DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar toDate   = DefaultVersionEndDate.getInstance().getDate();
		
		private SourceVersionSet sourceVersionSet;
		
		public Builder(int objID, int ldsID, String acc) {
			this.id = objID;
			this.ldsId = ldsID;
			this.accession = acc;
			
			this.sourceVersionSet = new SourceVersionSet();
		}
		
		public Builder fromDate(SimplifiedGregorianCalendar c) {
			this.fromDate = c;
			return this;
		}
		
		public Builder toDate(SimplifiedGregorianCalendar c) {
			this.toDate = c;
			return this;
		}
		
		public Builder sourceVersion(SourceVersion s) {
			this.sourceVersionSet.addSourceVersion(s);
			return this;
		}
		
		public Obj build() {
			return new Obj(this);
		}
	}
}
