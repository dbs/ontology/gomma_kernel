/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * changes: --
 * 
 **/
package org.gomma.model.source;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.math.CantorDecoder;

public class ObjRelationshipSet extends Object implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7366730314581497774L;
	
	private Map<Long,ObjRelationship> map;
	
	public ObjRelationshipSet() {
		this.map = new HashMap<Long,ObjRelationship>();
	}
	
	public long addObjectRelationship(Obj fromObject, Obj toObject, String type, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) {
		return this.addObjectRelationship(fromObject,toObject,type,false,fromDate,toDate);
	}
	
	public long addObjectRelationship(Obj fromObject, Obj toObject, String type, boolean isBidirectional, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) {
		if (fromObject!=null && toObject!=null) {
			ObjRelationship e = 
				new ObjRelationship.Builder(fromObject.getID(),toObject.getID())
						.fromDate(fromDate)
						.toDate(toDate)
						.isDirected(!isBidirectional)
						.build();
			e.setRelationshipType(type);
			this.map.put(e.getID(),e);
			return e.getID();
		} else throw new NullPointerException("Could not resolve the objects of the inner source relationship.");
	}
	
	public void addObjectRelationship(ObjRelationship o) {
		if (o!=null) this.map.put(o.getID(),o);
		else throw new NullPointerException("Could not resolve the inner source object relationship.");
	}
	
	public ObjRelationship getObjectRelationship(int domainID, int rangeID) {
		return getObjectRelationship(CantorDecoder.code(domainID,rangeID));
	}
	
	public ObjRelationship getObjectRelationship(long correspondenceID) throws NoSuchElementException {
		if (this.map.containsKey(correspondenceID)) return this.map.get(correspondenceID);
		else throw new NoSuchElementException("Could not find object relationship for ("+correspondenceID+")");
	}
	
	public Collection<ObjRelationship> getCollection() {
		return this.map.values();
	}
	
	public Set<Long> getIDs() {
		return this.map.keySet();
	}
	
	public int size() {
		return this.map.size();
	}
	
	public boolean containsRelationship(long correspondenceID) {
		return this.map.containsKey(correspondenceID);
	}
}
