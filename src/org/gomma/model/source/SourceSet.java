package org.gomma.model.source;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public final class SourceSet extends AbstractGenericObjectSet<Source> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4130900010849500758L;

	public SourceSet() {
		super();
	}

	public void addSource(Source s) {
		if (s==null)
			throw new NullPointerException("Could not resolve the source that should be added.");
		super.addElement(s);
	}
	
	public Source getSource(int id) throws NoSuchElementException {
		if (super.contains(id))
			return super.getElement(id);
		throw new NoSuchElementException("Could not find the source using the lds id '"+id+"'.");
	}
}
