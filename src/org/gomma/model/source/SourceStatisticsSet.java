package org.gomma.model.source;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class SourceStatisticsSet extends AbstractGenericObjectSet<SourceStatistics> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5684706706621557370L;

	public SourceStatisticsSet() {
		super();
	}
	
	public void addSourceStatictics(SourceStatistics stat) {
		if (stat==null)
			throw new NullPointerException("Could not resolve the source statictics that should be added.");
		super.addElement(stat);
	}
	
	public SourceStatistics getSourceStatistics(Source s) throws NoSuchElementException {
		if (s == null)
			throw new NullPointerException("Could not resolve the source for that the statistics should be retrieved.");
		
		if (!super.contains(s.getID()))
			throw new NoSuchElementException("Could not find any statistics for source '"+s.getName()+"'.");
		
		return super.getElement(s.getID());
	}
}
