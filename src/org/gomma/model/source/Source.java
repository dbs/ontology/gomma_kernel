package org.gomma.model.source;

import java.util.List;
import java.util.Vector;

import org.gomma.model.AbstractGenericObject;

public final class Source extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4476552530748640532L;

	/**
	 * The objectType represents the semantic object type of source.
	 */
	private String objectType;
	
	/**
	 * The physicalSourceName represents the name of used physical source.
	 */
	private String physicalSourceName;
	
	/**
	 * The sourceURL represents the web link (URL) of the logical source in order to 
	 * link the source objects with further information of the object.
	 */
	private String sourceURL;
	
	/**
	 * The isOntology is true if the logical source is an ontology; otherwise it is false.
	 */
	private boolean isOntology;
	
	/**
	 * languagesUsed stores the used languages in the data source using the international abbreviations like "en" or "de" 
	 */
	private List<String> languagesUsed = new Vector<String>();
	
	
	
	public Source(Builder b) {
		super(b.id);
		
		this.objectType = b.objectType;
		this.physicalSourceName = b.physicalSourceName;
		this.sourceURL = b.sourceURL;
		this.isOntology = b.isOntology;
		this.languagesUsed = b.languagesUsed;
		
		super.setName(this.objectType+"@"+this.physicalSourceName);
	}
	
	public String getObjectType() {
		return this.objectType;
	}
	
	public String getPhysicalSourceName() {
		return this.physicalSourceName;
	}
	
	public String getSourceURL() {
		return this.sourceURL;
	}
	
	public boolean isOntology() {
		return this.isOntology;
	}
	
	public List<String> getUsedLanguages() {
		return this.languagesUsed;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Source)) return false;
		return ((Source)o).getID() == super.getID();
	}
	
	
	
	public static class Builder {
		
		private final int id;
		
		private String objectType = "N/A";
		private String physicalSourceName = "N/A";
		private String sourceURL ="N/A";
		private boolean isOntology = false;
		private List<String> languagesUsed = new Vector<String>();
		
		public Builder(int id) {
			this.id = id;
		}
		
		public Builder objectType(String type) {
			this.objectType = type;
			return this;
		}
		
		public Builder physicalSourceName(String name) {
			this.physicalSourceName = name;
			return this;
		}
		
		public Builder sourceURL(String url) {
			this.sourceURL = url;
			return this;
		}
		
		public Builder isOntology(boolean isOntology) {
			this.isOntology = isOntology;
			return this;
		}
		
		public Builder languagesUsed(List<String> languagesUsed) {
			this.languagesUsed = languagesUsed;
			return this;
		}
		
		public Source build() {
			return new Source(this);
		}
	}
}
