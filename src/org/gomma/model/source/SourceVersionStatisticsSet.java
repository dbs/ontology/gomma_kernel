package org.gomma.model.source;

import java.util.NoSuchElementException;

import org.gomma.model.AbstractGenericObjectSet;

public class SourceVersionStatisticsSet extends AbstractGenericObjectSet<SourceVersionStatistics> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4147339787643828695L;

	public SourceVersionStatisticsSet() {
		super();
	}
	
	public void addSourceVersionStatistics(SourceVersionStatistics stat) {
		if (stat == null)
			throw new NullPointerException("Could not resolve source version statistics that should be added.");
		
		super.addElement(stat);
	}
	
	public SourceVersionStatistics getSourceVersionStatistics(SourceVersion v) {
		if (v == null)
			throw new NullPointerException("Could not resolve the source for that the statistics should be retrieved.");
		
		if (!super.contains(v.getID()))
			throw new NoSuchElementException("Could not find any statistics for source '"+v.getName()+"'.");
		
		return super.getElement(v.getID());
	}
}
