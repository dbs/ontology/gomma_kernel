/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.model.source;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;

import org.gomma.Gomma;
import org.gomma.GommaManager;
import org.gomma.GommaService;
import org.gomma.GommaServiceImpl;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.manager.AbstractManagerFactory;
import org.gomma.manager.ManagerModuleFactory;
import org.gomma.manager.source.SourceManager;
import org.gomma.model.IDObject;
import org.gomma.model.IDObjectSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.operator.MappingOperators;
import org.gomma.operator.SetOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.algorithm.GreatestConnectedSubGraph;
import org.gomma.util.graph.algorithm.GreatestConnectedSuperGraph;
import org.gomma.util.graph.algorithm.SubGraph;

/**
 * The class SourceVersionStructure represents the graph structure of a source version.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @param <r>
 * @since:   JDK1.5
 */
public class SourceVersionStructure extends Object implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3261037298983963943L;

	/**
	 * The item represents the source version for which the structure should be managed.
	 */
	private final SourceVersion sourceVersion;
	
	/**
	 * The item represents the graph / structure of the source version that should be managed.
	 */
	private Graph<Obj,ObjRelationship> g;
	
	/**
	 * The item is true if the structure graph is a directed graph; otherwise it false.
	 */
	private boolean isDirectedGraph;
	
	
	/**
	 * The constructor initializes the class.
	 * @param s source version for that the structure should be managed
	 * @throws GraphInitializationException
	 */
	public SourceVersionStructure(SourceVersion s) throws GraphInitializationException {
		if (s==null) throw new NullPointerException();
		this.sourceVersion = s;
		
		this.isDirectedGraph = s.getStructuralType().isDirected();
		this.g = s.getStructuralType().generateGraph();		
	}
	
	/**
	 * The method adds the specified object to the currently managed
	 * structure of the source version.
	 * @param o object of the source version that should be added
	 * @throws WrongSourceException
	 */
	public void addObject(Obj o) throws WrongSourceException {
		if (o==null) throw new NullPointerException();
		
		this.g.addNode(o);
	}

	/**
	 * The method adds the specified set of objects to the currently managed 
	 * structure of the source version.
	 * @param objs set of objects of the source version
	 * @throws WrongSourceException
	 */
	public void addObjects(ObjSet objs) throws WrongSourceException {
		if (objs==null) throw new NullPointerException();
		
		for (Obj o : objs) this.g.addNode(o); 
		
	}
	
	/**
	 * The method creates and adds a relationship between the specified objects. 
	 * @param fromObj parent object
	 * @param toObj child object
	 * @param type type of the relationship
	 * @param isDirected true iff the relationship is directed; otherwise false
	 * @param fromDate start date for that the relationship is valid 
	 * @param toDate end date for that the relationship is valid
	 * @throws GraphInitializationException
	 */
	public void addObjectRelationship(Obj fromObj, Obj toObj, String type, boolean isDirected, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) throws GraphInitializationException {
		this.addObjectRelationship(fromObj.getID(),toObj.getID(),type,isDirected,fromDate,toDate);		
	}
	
	/**
	 * The method creates and adds a relationship between the objects for that 
	 * the identifier is specified. 
	 * @param fromObjID identifier of the parent object
	 * @param toObjID identifier of the child object
	 * @param typetype of the relationship
	 * @param isDirected true iff the relationship is directed; otherwise false
	 * @param fromDate start date for that the relationship is valid 
	 * @param toDate end date for that the relationship is valid
	 * @throws GraphInitializationException
	 */
	public void addObjectRelationship(int fromObjID, int toObjID, String type, boolean isDirected, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) throws GraphInitializationException {
		ObjRelationship rel = 
			new ObjRelationship.Builder(fromObjID,toObjID)
					.fromDate(fromDate)
					.toDate(toDate)
					.isDirected(isDirected)
					.build();
		rel.setRelationshipType(type);
		
		this.addObjectRelationship(rel);
		
//		this.addObjectRelationship(this.objs.getObj(fromObjID),this.objs.getObj(toObjID),type,isDirected,fromDate,toDate);
	}
	
	/**
	 * The method adds the specified relationship between two objects to the 
	 * currently managed structure of the source version. The objects need 
	 * to be available within the structure before the relationship can be added. 
	 * @param rel relationship between two objects
	 * @throws GraphInitializationException
	 */
	public void addObjectRelationship(ObjRelationship rel) throws GraphInitializationException {
		this.g.addEdge(rel);
	}
	
	/**
	 * The method adds the specified set of relationships to the currently managed structure.
	 * @param set set of object relationships
	 * @throws GraphInitializationException
	 */
	public void addObjectRelationships(Set<ObjRelationship> set) throws GraphInitializationException {
		if (set==null) 
			throw new NullPointerException("Could not resolve the inner source object relationship set.");
		
		for (Iterator<ObjRelationship> i=set.iterator(); i.hasNext();) {
			this.addObjectRelationship(i.next());
		}
	}
	/**
	 * The method adds the specified set of relationships to the currently managed structure.
	 * @param set set of object relationships
	 * @throws GraphInitializationException
	 */
	public void addObjectRelationships(ObjRelationshipSet set) throws GraphInitializationException {
		if (set==null) 
			throw new NullPointerException("Could not resolve the inner source object relationship set.");
		
		for (ObjRelationship rel: set.getCollection()) {
			this.addObjectRelationship(rel);
		}
	}
	/**
	 * The method returns the set of successor objects for the specified object. 
	 * @param o object for which the successor objects will be retrieved
	 * @return set of successor objects
	 * @throws WrongSourceException
	 */
	public ObjSet getSuccessorObjects(Obj o) throws WrongSourceException {
		ObjSet objs = new ObjSet();
		
		if (this.isDirectedGraph) {
			for (ObjRelationship e : ((DirectedGraph<Obj,ObjRelationship>)this.g).getOutgoingEdges(o))
				objs.addObj(this.g.getNode(e.getToID()));
		} else {
			for (ObjRelationship e : this.g.getEdgeCollection(o))
				if (o.getID()==e.getFromID()) objs.addObj(this.g.getNode(e.getToID()));
				else objs.addObj(this.g.getNode(e.getFromID()));
		}
		
		return objs;
	}
	
	/**
	 * The method returns the set of predecessor objects for the specified object. 
	 * @param o object for which the predecessor objects will be retrieved
	 * @return set of predecessor objects
	 * @throws WrongSourceException
	 */
	public ObjSet getPredecessorObjects(Obj o) throws WrongSourceException {
		ObjSet objs = new ObjSet();
		
		if (this.isDirectedGraph) {
			for (ObjRelationship e : ((DirectedGraph<Obj,ObjRelationship>)this.g).getIngoingEdges(o))
				objs.addObj(this.g.getNode(e.getFromID()));
		} else {
			for (ObjRelationship e : this.g.getEdgeCollection(o))
				if (o.getID()==e.getFromID()) objs.addObj(this.g.getNode(e.getToID()));
				else objs.addObj((Obj)this.g.getNode(e.getFromID()));
		}
		
		return objs;
	}
	/**
	 * The method returns the set of "is_a"-predecessor objects for the specified object. 
	 * @param o object for which the predecessor objects will be retrieved
	 * @return set of predecessor objects
	 * @throws WrongSourceException
	 */
	public ObjSet getIsAPredecessorObjects(Obj o) throws WrongSourceException {
		ObjSet objs = new ObjSet();
		
		if (this.isDirectedGraph) {
			for (ObjRelationship e : ((DirectedGraph<Obj,ObjRelationship>)this.g).getIngoingEdges(o)){
				if(e.getRelationshipType().equals("is_a")){
					objs.addObj(this.g.getNode(e.getFromID()));
				}
			}
				
		} else {
			for (ObjRelationship e : this.g.getEdgeCollection(o)){
				if(e.getRelationshipType().equals("is_a")){
					if (o.getID()==e.getFromID()) objs.addObj(this.g.getNode(e.getToID()));
					else objs.addObj((Obj)this.g.getNode(e.getFromID()));
				}
			}				
		}		
		return objs;
	}
	
	/**
	 * The method returns the set of out-going relationships for the specified object. 
	 * @param o object for which the out-going relationships will be retrieved
	 * @return set of out-going relationships
	 */
	public ObjRelationshipSet getOutRelationships(Obj o) {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		
		if (this.isDirectedGraph) {
			for (ObjRelationship e : ((DirectedGraph<Obj,ObjRelationship>)this.g).getOutgoingEdges(o))
				rels.addObjectRelationship((ObjRelationship)e);
		} else {
			for (ObjRelationship e : this.g.getEdgeCollection(o))
				if (o.getID()!=e.getFromID()) rels.addObjectRelationship(e); 
		}

		return rels;
	}
	
	/**
	 * The method returns the set of in-going relationships for the specified object. 
	 * @param o object for which the in-going relationships will be retrieved
	 * @return set of in-going relationships
	 */
	public ObjRelationshipSet getInRelationships(Obj o) {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		
		if (this.isDirectedGraph) {
			for (ObjRelationship e : ((DirectedGraph<Obj,ObjRelationship>)this.g).getIngoingEdges(o))
				rels.addObjectRelationship((ObjRelationship)e);
		} else {
			for (ObjRelationship e : this.g.getEdgeCollection(o))
				if (o.getID()!=e.getToID()) rels.addObjectRelationship(e); 
		}
		
		return rels;
	}
	
	
	/**
	 * The method returns the number of objects within the source structure.
	 * @return number of objects
	 */
	public int getNumberOfObjects() {
		return this.g.getNodeSetSize();
	}
	
	/**
	 * The method returns the number of relationships within the source structure.
	 * @return number of relationships
	 */
	public int getNumberOfRelationships() {
		return this.g.getEdgeSetSize();
	}
	
	/**
	 * The method returns the object using the specified internal identifier.
	 * @param ID internal identifier
	 * @return object from the currently managed object set of the source version 
	 * @throws NoSuchElementException
	 */
	public Obj getObject(int ID) throws NoSuchElementException {
		return this.g.getNode(ID);
	}
	
	/**
	 * The method returns true iff the object is part of the currently managed 
	 * source version structure; otherwise it returns false. The object we are looking
	 * for is identified by the given unique object identifier. 
	 * @param objID unique object identifier
	 * @return true/false
	 */
	public boolean containsObject(int objID) {
		return this.g.containsNode(objID);
	}
	
	/**
	 * The method returns the set of managed objects of the source version.
	 * @return set of managed objects of the source version
	 */
	public ObjSet getAllObjects() {
		ObjSet objs = new ObjSet();
		
		try {
			for (Obj n : this.g.getNodeCollection()) 
				objs.addObj((Obj)n);
		} catch (WrongSourceException e) {
			// should not happen
		}
		
		return objs;
	}

	/**
	 * The method returns the set of managed objects of the source version.
	 * @return set of managed objects of the source version
	 */
	public IDObjectSet getObjIDSet() throws WrongSourceException {
		IDObjectSet objIDs = new IDObjectSet();
		
		for (Obj n : this.g.getNodeCollection()) {
			IDObject IDObj = new IDObject(n.getID());
			objIDs.addElement(IDObj);
		}
		return objIDs;
	}
	/**
	 * The method returns the managed set of relationships between the objects 
	 * of the source version building the structure of the source version. 
	 * @return set of relationships between objects of the source version
	 */
	public ObjRelationshipSet getAllRelationships() {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		for (Edge e : this.g.getEdgeCollection()) rels.addObjectRelationship((ObjRelationship)e);
		return rels;
	}
	/**
	 * The method returns the managed set of "is_a" relationships between the objects 
	 * of the source version building the structure of the source version. 
	 * @return set of relationships between objects of the source version
	 */
	public ObjRelationshipSet getIsARelationships() {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		for (Edge e : this.g.getEdgeCollection()) rels.addObjectRelationship((ObjRelationship)e);
		
		ObjRelationshipSet isaRels = new ObjRelationshipSet();
		for(ObjRelationship r:rels.getCollection()){
			if(r.getRelationshipType().equals("is_a")){
				isaRels.addObjectRelationship(r);
			}
		}
		return isaRels;
	}
	public ObjRelationshipSet getPartOfRelationships() {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		for (Edge e : this.g.getEdgeCollection()) rels.addObjectRelationship((ObjRelationship)e);
		
		ObjRelationshipSet partofRels = new ObjRelationshipSet();
		for(ObjRelationship r:rels.getCollection()){
			if(r.getRelationshipType().equals("part_of")){ //manchmal gibt es auch andere part-of typen, hier alle nutzen?
				partofRels.addObjectRelationship(r);
			}
		}
		return partofRels;
	}
	/**
	 * The method returns the managed set of "is_a" and "part_of" relationships between the objects 
	 * of the source version building the structure of the source version. 
	 * @return set of relationships between objects of the source version
	 */
	public ObjRelationshipSet getIsAandPartOfRelationships() {
		ObjRelationshipSet rels = new ObjRelationshipSet();
		for (Edge e : this.g.getEdgeCollection()) rels.addObjectRelationship((ObjRelationship)e);
		
		ObjRelationshipSet isaPartofRels = new ObjRelationshipSet();
		for(ObjRelationship r:rels.getCollection()){
			if(r.getRelationshipType().equals("is_a") || r.getRelationshipType().equals("part_of")){
				isaPartofRels.addObjectRelationship(r);
			}
		}
		return isaPartofRels;
	}
	/**
	 * The method returns true if there is a relationship between the 
	 * specified parent and child object; otherwise it returns false.
	 * @param parent parent object
	 * @param child child object
	 * @return true/false
	 */
	public boolean isDirectlyConnected(Obj parent, Obj child) {
		return this.g.isDirectlyConnected(parent, child);
	}
	
	/**
	 * The method returns the source version for which the structure is managed. 
	 * @return source version
	 */
	public SourceVersion getSourceVersion() {
		return this.sourceVersion;
	}
	
	/**
	 * The method returns the graph including all objects and relationships 
	 * of the source version that is managed.
	 * @return structure graph of the source version
	 */
	public Graph<Obj,ObjRelationship> getStructure() {
		return this.g;
	}
	/**
	 * The method returns the same graph but without the listed objects and without relationships to this object.
	 * @return number of objects
	 * @throws GraphInitializationException 
	 * @throws WrongSourceException 
	 * @throws OperatorException 
	 */
	public SourceVersionStructure reduceGraphBySomeObjects(List<String> accessionList) throws GraphInitializationException, 
																								WrongSourceException, OperatorException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		ObjSet svsObjects = this.getAllObjects();
		ObjSet oSet = new ObjSet();
			for(String acc:accessionList){
				Obj o = svsObjects.getObj(acc);			
				oSet.addObj(o);
			}
			ObjSet reducedObjSet = SetOperators.diff(svsObjects,oSet);
			ObjRelationshipSet relSet = this.getAllRelationships();
			svs.addObjects(reducedObjSet);
		try {
			svs.addObjectRelationships(relSet);
		} catch (GraphInitializationException e) {
			System.out.println(e);
		}
		return svs;

	}
	public SourceVersionStructure getGreatestConnectedSubGraph(Obj o) throws WrongSourceException, GraphInitializationException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		Graph<Obj,ObjRelationship> g; 
		if (this.isDirectedGraph) 
			g = GreatestConnectedSubGraph.getGreatestConnectedSubGraph((DirectedGraph<Obj,ObjRelationship>)this.g,o);
		else g = GreatestConnectedSubGraph.getGreatestConnectedSubGraph(this.g,o);
		
		for (Obj n : g.getNodeCollection())
			svs.addObject((Obj)n);
		
		for (ObjRelationship e : g.getEdgeCollection())
			svs.addObjectRelationship((ObjRelationship)e);

		return svs;
	}
	
	public SourceVersionStructure getGreatestConnectedSuperGraph(Obj o) throws GraphInitializationException, WrongSourceException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		Graph<Obj,ObjRelationship> g; 
		if (this.isDirectedGraph) 
			g = GreatestConnectedSuperGraph.getGreatestConnectedSuperGraph((DirectedGraph<Obj,ObjRelationship>)this.g,o);
		else g = GreatestConnectedSuperGraph.getGreatestConnectedSuperGraph(this.g,o);
		
		for (Obj n : g.getNodeCollection())
			svs.addObject((Obj)n);
		
		for (ObjRelationship e : g.getEdgeCollection())
			svs.addObjectRelationship((ObjRelationship)e);

		return svs;
		
		/*SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		ObjSet allPredecessorObjects = new ObjSet();
		getAllPredecessors(o, this, allPredecessorObjects);

		for (Obj p : allPredecessorObjects){
			svs.addObject(p);
		}
		for (Obj r : allPredecessorObjects){
			ObjRelationshipSet relSet = this.getInRelationships(r);
			svs.addObjectRelationships(relSet);
		}

		return svs;*/
	}
	public void getAllPredecessors(Obj currentObj,SourceVersionStructure src, ObjSet allPredecessorObjects) throws WrongSourceException, NoSuchElementException {

		if(!allPredecessorObjects.contains(currentObj.getID())){
			allPredecessorObjects.addObj(currentObj);			
		}
		ObjSet predecessorObjects	= src.getPredecessorObjects(currentObj);
		for(Obj o:predecessorObjects){
			//System.out.println(o.getAccessionNumber());
			getAllPredecessors(o,src,allPredecessorObjects);
		}
	}
	public SourceVersionStructure getSubGraph(Obj o) throws WrongSourceException, GraphInitializationException, GraphOperationExecutionException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		Graph<Obj,ObjRelationship> g = null; 
		if (this.isDirectedGraph) {
			g = SubGraph.getSubGraph((DirectedGraph<Obj,ObjRelationship>)this.g,o);
		} else {
			throw new GraphOperationExecutionException("getSubGraph() not allowed for non-directed graphs");
		}
				
		for (Obj n : g.getNodeCollection())
			svs.addObject((Obj)n);
		
		for (ObjRelationship e : g.getEdgeCollection())
			svs.addObjectRelationship((ObjRelationship)e);

		return svs;
	}	
	public SourceVersionStructure getIsAStructure() throws WrongSourceException, GraphInitializationException, GraphOperationExecutionException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		
		for(ObjRelationship rel: this.getIsARelationships().getCollection()){
			if(!svs.containsObject(rel.getFromID())){
				svs.addObject(this.getObject(rel.getFromID()));
			}
			if(!svs.containsObject(rel.getToID())){
				svs.addObject(this.getObject(rel.getToID()));
			}
			svs.addObjectRelationship(rel);
		}				
		return svs;
	}	
	
	public SourceVersionStructure getIsAPartOfStructure() throws WrongSourceException, GraphInitializationException, GraphOperationExecutionException {
		SourceVersionStructure svs = new SourceVersionStructure(this.sourceVersion);
		
		for(ObjRelationship rel: this.getIsAandPartOfRelationships().getCollection()){
			if(!svs.containsObject(rel.getFromID())){
				svs.addObject(this.getObject(rel.getFromID()));
			}
			if(!svs.containsObject(rel.getToID())){
				svs.addObject(this.getObject(rel.getToID()));
			}
			svs.addObjectRelationship(rel);
		}
		return svs;
	}
	
	public String toString() {
		return this.g.toString();
	}

	public SourceVersionStructure extract(ObjCorrespondenceSet reduceByCorrSetObjects, boolean isDomain, Gomma gi) {
		
		Set<Integer> idSet;

		if (isDomain) {
			idSet = reduceByCorrSetObjects.getDomainObjIds();
		} else {
			idSet = reduceByCorrSetObjects.getRangeObjIds();
		}
		
		ObjSet ontologyPart = new ObjSet();
		
		SourceVersionStructure reducedSrc = null;
		boolean allObjAdded=true;
		int cnt = 0;
		try {
			for (int id : idSet) {
				try {
					ontologyPart.addObj(this.getObject(id));
				} catch (NoSuchElementException e) {
					// TODO: handle exception
					allObjAdded=false;		
					cnt++;
				}
			}
			if(!allObjAdded){
				System.out.println("Not all objects added ("+cnt+" affected)");
			}
			ObjSet reducedObjectSet = gi.getSourceManager().diff(this.getAllObjects(), ontologyPart);
			
			reducedSrc = new SourceVersionStructure(this.getSourceVersion());
			reducedSrc.addObjects(reducedObjectSet);
			/*
			for (ObjRelationship rel : this.getAllRelationships().getCollection()) {
				if (reducedObjectSet.contains(rel.getFromID())&&reducedObjectSet.contains(rel.getToID())) {
					reducedSrc.addObjectRelationship(rel);
				}
			}
			*/
		} catch (WrongSourceException e) {
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (GraphInitializationException e) {
			e.printStackTrace();
		} catch (GommaException e) {
			e.printStackTrace();
		}
		
		return reducedSrc;
	}
	public SourceVersionStructure contextExtract(ObjCorrespondenceSet mapDomainRange, SourceVersionStructure range, Gomma gi) {
		
		//um domain zu untersuchen, Uebergabe des inversen mappings (domain ist dann range)		
		
		Set<Integer> domainIdSet = mapDomainRange.getDomainObjIds();
		Set<Integer> rangeIdSet = mapDomainRange.getRangeObjIds();
		ObjSet ontologyPart = new ObjSet();
		
		SourceVersionStructure reducedSrc = null;
		try {
			for (int domId : domainIdSet) {
				boolean flag = true;
				//Domain-Konzepte deren Kinder keine Korrespondenz haben werden nicht "reduziert" (=bleiben in Ontologie
				//und koennen erneut gematcht werden)
				for(ObjRelationship rel: this.getOutRelationships(this.getObject(domId)).getCollection()){
					if(!domainIdSet.contains(rel.getToID())){
						flag = false;
					}
				}
				//Domain-Konzepte deren zugehoerige Range-Konzepte Kinder haben, die keine Korrespondenz haben 
				//werden nicht "reduziert"
				for(Integer ranID : mapDomainRange.getCorrespondingRangeObjIDSet(domId)){
					for(ObjRelationship rel: range.getOutRelationships(range.getObject(ranID)).getCollection()){
						if(!rangeIdSet.contains(rel.getToID())){
							flag = false;
						}
					}
				}
				
				if(flag){
					ontologyPart.addObj(this.getObject(domId));
				}else{
					//System.out.println(svs.getObject(id).getAccessionNumber());
				}
			}
		ObjSet reducedObjectSet = gi.getSourceManager().diff(this.getAllObjects(), ontologyPart);
		
		reducedSrc = new SourceVersionStructure(this.getSourceVersion());
		reducedSrc.addObjects(reducedObjectSet);
		
		for (ObjRelationship rel : this.getAllRelationships().getCollection()) {
			if (reducedObjectSet.contains(rel.getFromID())&&reducedObjectSet.contains(rel.getToID())) {
				reducedSrc.addObjectRelationship(rel);
			}
		}
		
		} catch (WrongSourceException e) {
		e.printStackTrace();
		} catch (NoSuchElementException e) {
		e.printStackTrace();
		} catch (RemoteException e) {
		e.printStackTrace();
		} catch (GraphInitializationException e) {
		e.printStackTrace();
		} catch (GommaException e) {
		e.printStackTrace();
		}
		
		return reducedSrc;
	}
}
