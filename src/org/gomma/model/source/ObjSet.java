/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 * 
 * creation date: 2007/11/10
 * changes:  -- 
 * 
 **/

package org.gomma.model.source;

import java.util.NoSuchElementException;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.AbstractGenericObjectSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.util.graph.NodeSet;

/**
 * The class collects all objects of a specified source version. The objects are indexed by the 
 * given object IDs. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class ObjSet extends AbstractGenericObjectSet<Obj> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -946828699623089629L;

	/**
	 * This constant represents the default identifier whenever no logical source is known. 
	 */
	public static final int DEFAULT_LDSID = -1;
	

	/**
	 * The ldsID uniquely identifies the logical source to which the different objects and 
	 * source versions belong. 
	 */
	private int ldsID;
	
	/**
	 * The constructor initializes the class. The ldsID is set with a default value and will be
	 * overwritten when the first object is added. 
	 */
	public ObjSet() {
		this(DEFAULT_LDSID);
	}
	
	/**
	 * The constructor initializes the object map using the specified ldsID. All set objects 
	 * are empty.
	 * @param ldsID unique identifier of a logical source 
	 */
	public ObjSet(int ldsID) {
		super(MapTypes.INT_OBJECT);
		this.ldsID=ldsID;
	}
	
	/**
	 * The methods adds the specified object to the object map. Only objects of the same logical
	 * source but may belong to different versions can be added.
	 * @param o object to be added
	 * @see org.gomma.model.source.Obj
	 * @exception NullPointerException
	 * @exception WrongSourceException
	 */
	public void addObj(Obj o) throws WrongSourceException {
		if (o==null) throw new NullPointerException("Could not resolve the object that should be added.");
		
		if (this.ldsID==DEFAULT_LDSID) this.ldsID = o.getLDSID();
		else if (this.ldsID!=o.getLDSID())
			throw new WrongSourceException("Could not add the object '"+o.getAccessionNumber()+"' ("+o.getID()+")"+
			"to the set of managed objects because of different logical sources.");
		
		super.addElement(o);
	}
	
	/**
	 * The methods adds the specified object to the object map. Only objects of the same logical
	 * source but may belong to different versions can be added.
	 * @param o object to be added
	 * @param mergeAttributeVersions true, if both objects, i.e., the available and the given object, utilize 
	 * the same object ID and their attributes should be merged; false if the given object will be rejected
	 * whenever there is an object available using the same object ID
	 * @see org.gomma.model.source.Obj
	 * @exception NullPointerException
	 * @exception WrongSourceException
	 */
	public void addObj(Obj o, boolean mergeAttributeVersions) throws WrongSourceException {
		
		if (o==null) throw new NullPointerException("Could not resolve the object that should be added.");
		
		if (!mergeAttributeVersions || !super.contains(o.getID())) {
			this.addObj(o);
			return;
		}
		
		if (this.ldsID == DEFAULT_LDSID) this.ldsID = o.getLDSID();
		else if (this.ldsID != o.getLDSID())
			throw new WrongSourceException("Could not add the object '"+o.getAccessionNumber()+"' ("+o.getID()+")"+
					"to the set of managed objects because of different logical sources.");
		
		Obj oldObj = super.getElement(o.getID());
		for (AttributeValueVersion avv : o.getAttributeValues())
			oldObj.addAttributeValue(avv);
		
		super.addElement(oldObj);
		
		
	}

	/**
	 * The method returns the object using the given objectID.
	 * @param objID ID of the object that should be retrieved
	 * @return object object identified by the specified object ID
	 * @exception NoSuchElementException
	 */
	public Obj getObj(int objID) throws NoSuchElementException {
		if (!super.contains(objID)) 
			throw new NoSuchElementException("Could not find object using the ID '"+objID+"'.");
		return super.getElement(objID);
	}
	
	/**
	 * The method returns the first object in the set using the specified accession number.
	 * @param accessionNumber accession number of the object one is looking for
	 * @return object using the specified accession number
	 * @throws NoSuchElementException
	 */
	public Obj getObj(String accessionNumber) throws NoSuchElementException {
		if (accessionNumber==null) throw new NullPointerException("Could not resolve the accession number of the object looking for.");
		for (Obj o : this)
			if (o.getAccessionNumber().equals(accessionNumber))
				return o;
		throw new NoSuchElementException("Could not find object with accession number '"+accessionNumber+"'.");
	}
	
	/**
	 * The method returns the set of source versions to that the objects in the structure belong.
	 * @return set of source versions
	 */
	public SourceVersionSet getSourceVersionSet() {
		SourceVersionSet set = new SourceVersionSet();
		for (Obj o : this)
			for (SourceVersion v : o.getSourceVersionSet())
				set.addSourceVersion(v);
		return set;
	}

	
	/**
	 * The method returns the LDS ID of all objects within the set.
	 * @return LDS ID sharing all objects within the set
	 */
	public int getLDSID() {
		return this.ldsID;
	}
	

	
	/**
	 * The method converts the currently managed set of objects to a 
	 * node set used by diverse graph algorithms.
	 * @return node set
	 */
	public NodeSet<Obj> toNodeSet() {
		NodeSet<Obj> s = new NodeSet<Obj>();
		for (Obj o : this) s.add(o);
		return s;
	}
	
	public String toAccessionListString(String separator) {
		String accessionListString="";
		for (Obj o : this)
			accessionListString += o.getAccessionNumber()+separator;
		return accessionListString;
	}
}
