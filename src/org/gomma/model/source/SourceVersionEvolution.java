package org.gomma.model.source;

import java.io.Serializable;

public class SourceVersionEvolution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -114255458319264179L;
	
	private SourceVersion fromSourceVersion;
	private SourceVersion toSourceVersion;
	
	private int numAddedObjects;
	private int numDeletedObjects;
	private int numFusedObjects;
	private int numToObsoleteObjects;
	
	
	public SourceVersionEvolution(Builder b) {
		this.fromSourceVersion = b.fromVersion;
		this.toSourceVersion = b.toVersion;
		
		this.numAddedObjects = b.numAddedObjects;
		this.numDeletedObjects = b.numDeletedObjects;
		this.numFusedObjects = b.numFusedObjects;
		this.numToObsoleteObjects = b.numToObsoleteObjects;
	}
	
	public SourceVersion getFromSourceVersion() {
		return this.fromSourceVersion;
	}
	
	public SourceVersion getToSourceVersion() {
		return this.toSourceVersion;
	}
	
	public int getNumberOfAddedObjects() {
		return this.numAddedObjects;
	}
	
	public int getNumberOfDeletedObjects() {
		return this.numDeletedObjects;
	}
	
	public int getNumberOfFusedObjects() {
		return this.numFusedObjects;
	}
	
	public int getNumberOfToObsoleteObjects() {
		return this.numToObsoleteObjects;
	}
	
	
	public static class Builder implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 6035248902488264586L;
		
		private final SourceVersion fromVersion;
		private final SourceVersion toVersion;
		
		private int numAddedObjects = 0;
		private int numDeletedObjects = 0;
		private int numFusedObjects = 0;
		private int numToObsoleteObjects = 0;
		
		public Builder(SourceVersion from, SourceVersion to) {
			this.fromVersion = from;
			this.toVersion = to;
		}
		
		public Builder numberOfAddedObjects(int n) {
			this.numAddedObjects = n;
			return this;
		}
		
		public Builder numberOfDeletedObjects(int n) {
			this.numDeletedObjects = n;
			return this;
		}
		
		public Builder numberOfFusedObjects(int n) {
			this.numFusedObjects= n;
			return this;
		}
		
		public Builder numberOfToObsoleteObjects(int n) {
			this.numToObsoleteObjects = n;
			return this;
		}
		
		public SourceVersionEvolution build() {
			return new SourceVersionEvolution(this);
		}
	}
}
