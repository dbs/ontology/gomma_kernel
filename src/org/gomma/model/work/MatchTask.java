/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/10
 * changes: --
 * 
 **/
package org.gomma.model.work;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.io.Serializable;
import java.util.Properties;
import java.util.Set;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DefaultVersionEndDate;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.util.date.SimplifiedGregorianCalendar;


public class MatchTask extends AbstractGenericObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5549707429923882385L;
	
	private WorkPackage workPackage;
	private String taskConfiguration;
	private int domainSourceVersionID;
	private int rangeSourceVersionID;
	private Set<Integer> domainObjIDs;
	private Set<Integer> rangeObjIDs;
	
	private TaskStatus taskStatus;
	private SimplifiedGregorianCalendar startTime;
	private SimplifiedGregorianCalendar endTime;
	private String resourceURL;
	
	private MatchTask(Builder b) {
		super(b.id);
		super.setName(b.name);
		
		this.workPackage = b.pack;
		this.taskConfiguration = b.taskConf;
		this.taskStatus = b.taskStatus;
		this.resourceURL = b.resourceURL;
		
		this.domainSourceVersionID = b.domainSourceVersionID;
		this.rangeSourceVersionID  = b.rangeSourceVersionID;
		
		this.domainObjIDs = b.domainObjIDs;
		this.rangeObjIDs  = b.rangeObjIDs;
		
		this.startTime  = b.startTime;
		this.endTime    = b.endTime;
	}
	
	public WorkPackage getWorkPackage() {
		return this.workPackage;
	}
	
	public String getTaskConfiguration() {
		return this.taskConfiguration;
	}
	
	public Properties getTaskConfigurationProperties(String separator) {
		Properties props = new Properties();
		
		for (String s : this.getTaskConfiguration().split(separator)) {
			String[] prop = s.split("=");
			if (prop.length!=2) continue;
			props.setProperty(prop[0],prop[1]);
		}
		
		return props;
	}
	
	public int getDomainSourceVersionID() {
		return this.domainSourceVersionID;
	}
	
	public int getRangeSourceVersionID() {
		return this.rangeSourceVersionID;
	}
	
	public Set<Integer> getDomainObjectIDs() {
		return this.domainObjIDs;
	}
	
	public Set<Integer> getRangeObjectIDs() {
		return this.rangeObjIDs;
	}
	
	public TaskStatus getTaskStatus() {
		return this.taskStatus;
	}
	
	public SimplifiedGregorianCalendar getStartTime() {
		return this.startTime;
	}
	
	public SimplifiedGregorianCalendar getEndTime() {
		return this.endTime;
	}
	
	public String getResourceURL() {
		return this.resourceURL;
	}
	
	public void setTaskStatus(TaskStatus s) {
		this.taskStatus=s;
	}
	
	public void setStartTime(SimplifiedGregorianCalendar cal) {
		this.startTime=cal;
	}
	
	public void setEndTime(SimplifiedGregorianCalendar cal) {
		this.endTime=cal;
	}
	
	public void addDomainObjectID(int id) {
		this.domainObjIDs.add(id);
	}
	
	public void addRangeObjectID(int id) {
		this.rangeObjIDs.add(id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof MatchTask)) return false;
		return ((MatchTask)o).getID()==super.getID();
	}
	
	
	
	
	public static class Builder {
		
		private final int id;
		private final WorkPackage pack;
		
		private String name = "N/A";
		private String taskConf = "";
		private TaskStatus taskStatus = TaskStatus.NO_STATUS_AVAILABLE;
		private int domainSourceVersionID, rangeSourceVersionID;
		private Set<Integer> domainObjIDs = new IntOpenHashSet(), rangeObjIDs = new IntOpenHashSet();
		private SimplifiedGregorianCalendar startTime = DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar endTime   = DefaultVersionEndDate.getInstance().getDate();
		private String resourceURL = "N/A";
		
		public Builder(int id, WorkPackage p) {
			this.id=id;
			this.pack=p;
		}
		
		public Builder(int id, int packageID, String packageName) {
			this.id=id;
			this.pack = new WorkPackage.Builder(packageID).name(packageName).build();
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder taskConfiguration(String conf) {
			this.taskConf = conf;
			return this;
		}
		
		public Builder taskStatus(TaskStatus status) {
			this.taskStatus = status;
			return this;
		}
		
		public Builder domainSourceVersionID(int id) {
			this.domainSourceVersionID=id;
			return this;
		}
		
		public Builder rangeSourceVersionID(int id) {
			this.rangeSourceVersionID=id;
			return this;
		}
	
		public Builder startTime(SimplifiedGregorianCalendar cal) {
			this.startTime=cal;
			return this;
		}
		
		public Builder endTime(SimplifiedGregorianCalendar cal) {
			this.endTime=cal;
			return this;
		}
		
		public Builder resourceURL(String url) {
			this.resourceURL=url;
			return this;
		}
		
		public MatchTask build() {
			return new MatchTask(this);
		}
		
	}
}
