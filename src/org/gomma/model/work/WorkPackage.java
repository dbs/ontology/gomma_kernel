/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/10
 * changes: --
 * 
 **/
package org.gomma.model.work;

import org.gomma.model.AbstractGenericObject;

public class WorkPackage extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 253204968390186376L;
	
	
	private String packageDescription;
	
	private WorkPackage(Builder b) {
		super(b.id);
		super.setName(b.name);
		this.packageDescription = b.descr;
	}
	
	public String getWorkPackageDescription() {
		return this.packageDescription;
	}
	
	@Override 
	public boolean equals(Object o) {
		if (!(o instanceof WorkPackage)) return false;
		return ((WorkPackage)o).getID()==this.getID();
	}
	
	
	public static class Builder {
		
		private final int id;
		
		private String name = "N/A";
		private String descr= "N/A";
		
		public Builder(int id) {
			this.id = id;
		}
		
		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder description(String descr) {
			this.descr = descr;
			return this;
		}
		
		public WorkPackage build() {
			return new WorkPackage(this);
		}
	}
}
