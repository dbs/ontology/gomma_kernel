/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/10
 * changes: --
 * 
 **/
package org.gomma.model.work;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class MatchTaskSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8859108163604174113L;
	
	private Map<Integer,MatchTask> taksMap;
	
	public MatchTaskSet() {
		this.taksMap = new Int2ObjectOpenHashMap<MatchTask>();
	}
	
	public void addMatchTask(MatchTask task) {
		this.taksMap.put(task.getID(),task);
	}
	
	public Collection<MatchTask> getCollection() {
		return this.taksMap.values();
	}
	
	public int size() {
		return this.taksMap.size();
	}
}
