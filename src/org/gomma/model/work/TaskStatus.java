/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/11/10
 * changes: --
 * 
 **/
package org.gomma.model.work;

import java.io.Serializable;

public enum TaskStatus implements Serializable {

	NO_STATUS_AVAILABLE {
		public int getID() { return 0; }
	},
	NOT_PROCESSED {
		public int getID() { return 1; }
	},
	IN_PROCESSING {
		public int getID() { return 2; }
	},
	PROCESSING_FINISHED {
		public int getID() { return 3; }
	};
	
	public abstract int getID();
	
	public static TaskStatus resolveTaskStatus(int id) {
		for (TaskStatus s : TaskStatus.values())
			if (s.getID()==id) return s;
		return NO_STATUS_AVAILABLE;
	}
}
