package org.gomma.model;

import java.util.Calendar;

import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the default start date of the class. The start date can 
 * be used for logical sources, objects, attribute values, and relationships 
 * as well. The class acts as singleton. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class DefaultVersionStartDate {

	/**
	 * The item represents the singleton instance.
	 */
	private static volatile DefaultVersionStartDate singleton = new DefaultVersionStartDate();
	
	/**
	 * The item represents the default start date.
	 */
	private final SimplifiedGregorianCalendar date;
	
	/**
	 * The constructor initializes the class.
	 */
	private DefaultVersionStartDate() {
		this.date = new SimplifiedGregorianCalendar(1900,Calendar.JANUARY,1);
	}
	
	/**
	 * The static method returns an instance of the class. The class acts as singleton.
	 * @return instance of the class
	 */
	public static DefaultVersionStartDate getInstance() {
		return singleton;
	}
	
	/**
	 * The method returns the default start date of a version.
	 * @return default start date
	 */
	public SimplifiedGregorianCalendar getDate() {
		return this.date;
	}
}
