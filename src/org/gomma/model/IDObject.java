package org.gomma.model;

/**
 * The class represents a generic object which only includes 
 * an internal unique object identifier.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class IDObject extends AbstractGenericObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2982549788988823561L;
	
	/**
	 * The constructor initializes the class.
	 * @param id internal unique identifier
	 */
	public IDObject(int id) {
		super(id);
	}

}
