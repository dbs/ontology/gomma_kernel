package org.gomma.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * The interface defines some general methods for managing generic object sets.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface GenericObjectSet<T extends GenericObject> extends Serializable, Iterable<T> {

	/**
	 * The method returns the set of internal unique identifiers for 
	 * the set of objects that are currently managed.
	 * @return set of unique object identifier
	 */
	public Set<Integer> getIDSet();
	
	/**
	 * The method returns the set of objects that are currently managed.
	 * @return set of currently managed objects
	 */
	public Collection<T> getCollection();
	
	/**
	 * The method returns true iff the currently set of managed objects contains
	 * an object using the specified internal unique object identifier; otherwise 
	 * it return false.
	 * @param id internal unique object identifier
	 * @return true/false
	 */
	public boolean contains(int id);
	
	/**
	 * The method returns true iff the currently set of managed objects contains
	 * the specified object; otherwise it returns false. 
	 * @param element element to look for in the object set
	 * @return true/false
	 */
	public boolean contains(T element);
	
	/**
	 * The method adds the given object to the currently managed set. 
	 * @param element element to be added
	 */
	public void addElement(T element);
	
	/**
	 * The method returns the object using the specified internal unique object identifier.
	 * @param id internal unique object identifier
	 * @return object using the specified internal unique identifier
	 * @throws NoSuchElementException
	 */
	public T getElement(int id) throws NoSuchElementException;
	
	/**
	 * The method returns the size of the currently managed set of objects.
	 * @return size of the set
	 */
	public int size();
	
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o);
}
