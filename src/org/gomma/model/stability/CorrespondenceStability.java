package org.gomma.model.stability;

import java.io.Serializable;





public final class CorrespondenceStability extends Object implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2744415520557745778L;
	private int domainObjID;
	private int rangeObjID;
	private int analysisID;
	private String analysisName;
	private String domainAcc;
	private String rangeAcc;
	private float lastConfidence;
	private float stabAvgValue;
	private float stabWMValue;
	
	/**
	 * The constructor initializes the class CorrespondenceStability.  
	 *
	 */
	private CorrespondenceStability(Builder b) {
		this.domainObjID=b.domainObjID;
		this.rangeObjID=b.rangeObjID;
        this.analysisID=b.analysisID;
		this.lastConfidence=b.lastConfidence;
		this.stabAvgValue=b.stabAvgValue;
		this.stabWMValue=b.stabWMValue;
		this.domainAcc=b.domainAcc;
		this.rangeAcc=b.rangeAcc;
		this.analysisName=b.analysisName;
	} 
	
	public int getDomainObjID(){
		return this.domainObjID;
	}
	
	public int getRangeObjID(){
		return this.rangeObjID;
	}
	
	public int getAnalysisID(){
		return this.analysisID;
	}
	
	public String getAnalysisName(){
		return this.analysisName;
	}
	
	public float getLastConfidence() {
		return this.lastConfidence;
	}
	
	public float getStabAvgValue(){
		return this.stabAvgValue;
	}
	
	public float getStabWMValue(){
		return this.stabWMValue;
	}
	
	public String getDomainAccessionNumber() {
		return this.domainAcc;
	}
	
	public String getRangeAccessionNumber() {
		return this.rangeAcc;
	}
	
	public boolean isReferencedTo(int analysisId){
		if(this.analysisID==analysisId)return true;
		else return false;
	}
	
	public static class Builder implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -7166266878349100635L;
		
		private final int domainObjID;
		private final int rangeObjID;
		private final int analysisID;
		private  String domainAcc="N/A" ;
		private  String rangeAcc="N/A" ;
		private  String analysisName="N/A";
		private float lastConfidence = 0F;
		private float stabAvgValue = 0F;
		private float stabWMValue= 0F;
		

		
		public Builder(int dId,int rId,int analysisId) {
			this.domainObjID=dId;
			this.rangeObjID=rId;
			this.analysisID=analysisId;
		}
		
	    public Builder analysisName(String name){
	    	this.analysisName=name;
	    	return this;
	    }
	    
		public Builder domainAccession(String dAcc) {
			this.domainAcc = dAcc;
			return this;
		}
		
		public Builder rangeAccession(String rAcc) {
			this.rangeAcc=rAcc;
			return this;
		}
		public Builder lastConfidence(float conf) {
			this.lastConfidence = conf;
			return this;
		}
		
		public Builder stabAvgValue(float stabAvg) {
			this.stabAvgValue = stabAvg;
			return this;
		}
		
		public Builder stabWMValue(float stabWM) {
			this.stabWMValue=stabWM;
			return this;
		}
		
		public CorrespondenceStability build() {
			return new CorrespondenceStability(this);
		}
	}

}
