package org.gomma.model.stability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.gomma.exceptions.WrongSourceException;




public class CorrespondenceStabilitySet extends Object implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4124611068568030144L;
	

	
    private ArrayList<CorrespondenceStability> correspondenceStabilityList;
    
   
	public CorrespondenceStabilitySet(){
		this.correspondenceStabilityList= new ArrayList<CorrespondenceStability>();
		
		
		
	}
	
/*	/**
	 * The method returns the unique correspondence identifier. 
	 * @param domainID unique domain object identifier
	 * @param rangeID unique range object identifier
	 * @return unique correspondence identifier
	 */
/*	private long getCorrespondenceID(int domainID, int rangeID) {
		return CantorDecoder.code(domainID,rangeID);
	}
*/
	/**
	 * The method adds a object correspondence to the map.
	 * @param o object correspondence to be added
	 * @see org.gomma.model.mapping.ObjCorrespondence
	 */
	public void addCorrespondenceStab(CorrespondenceStability corrStab) throws WrongSourceException {
		
		if (corrStab == null) throw new NullPointerException("Could not resolve the correspondence stability that should be added.");
		if(this.size()==0)
//		long corrStabID = this.getCorrespondenceID(corrStab.getDomainObjID(),corrStab.getRangeObjID());
		this.correspondenceStabilityList.add(corrStab);
		else if(corrStab.isReferencedTo(this.getCollection().iterator().next().getAnalysisID()))
		        this.correspondenceStabilityList.add(corrStab);
		     else throw new WrongSourceException("the stability that belong to different analysis could not be added together.");	
		
	}
    

	  public CorrespondenceStability getCorrespondenceStab(String domainObjAccession, String rangeObjAccession) throws NoSuchElementException{
		  CorrespondenceStability corrStab=new CorrespondenceStability.Builder(0, 0, this.getCollection().iterator().next().getAnalysisID()).build();
		  for(CorrespondenceStability tempCorrStab: this.getCollection()){
			  if(tempCorrStab.getDomainAccessionNumber().equalsIgnoreCase(domainObjAccession) && tempCorrStab.getRangeAccessionNumber().equalsIgnoreCase(rangeObjAccession)){
				  
				  corrStab=tempCorrStab;
			      break;
			  }
			  
		      }
		      if(corrStab.getDomainObjID()==0)throw new NoSuchElementException("Could not find object CorrespondenceStability using the '"+domainObjAccession+"' and '"+rangeObjAccession+"'.");
			     return corrStab;
	  }
	  
	  
	  
	  public CorrespondenceStabilitySet getCorrespondenceStabsWithConfidence(float minConfidence, float maxConfidence) throws WrongSourceException {
		  CorrespondenceStabilitySet set=new CorrespondenceStabilitySet();
		  for(CorrespondenceStability corrStab :this.correspondenceStabilityList){
			  if(corrStab.getLastConfidence()>=minConfidence && corrStab.getLastConfidence()<=maxConfidence)
				 set.addCorrespondenceStab(corrStab);
			 
	      }
		  return set;
	  }
	  
	  
	  public CorrespondenceStabilitySet getCorrespondenceStabsWithStabAvg(float minStabAvg, float maxStabAvg) throws WrongSourceException {
		  CorrespondenceStabilitySet set=new CorrespondenceStabilitySet();
		  for(CorrespondenceStability corrStab: this.correspondenceStabilityList){
			  if(corrStab.getStabAvgValue()>=minStabAvg && corrStab.getStabAvgValue()<=maxStabAvg)
				  set.addCorrespondenceStab(corrStab);
		  }
		  return set;
	  }
	  
	  public CorrespondenceStabilitySet getCorrespondenceStabsWithStabWM(float minStabWM, float maxStabWM) throws WrongSourceException {
		  CorrespondenceStabilitySet corrStabs=new CorrespondenceStabilitySet();
		  for(CorrespondenceStability corrStab: this.correspondenceStabilityList){
			  if(corrStab.getStabWMValue()>=minStabWM&& corrStab.getStabWMValue()<=maxStabWM)
				  corrStabs.addCorrespondenceStab(corrStab);
		  }
		  return corrStabs;
	  }
	  
	  public ArrayList<CorrespondenceStability> getCollection() {
			return this.correspondenceStabilityList;
		}
		
	 public int size(){
		 return this.correspondenceStabilityList.size();
	    }
	 
	 
	 public void clear(){
		 this.correspondenceStabilityList.clear();
	 }
}



