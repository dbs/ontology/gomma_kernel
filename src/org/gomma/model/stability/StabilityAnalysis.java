package org.gomma.model.stability;

import java.io.Serializable;

import org.gomma.model.AbstractGenericObject;
import org.gomma.model.DefaultVersionStartDate;
import org.gomma.util.date.SimplifiedGregorianCalendar;


public class StabilityAnalysis extends AbstractGenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1968810021131257662L;
    
	private StabilityAnalysis(Builder b) {
		super(b.analysisId);
		super.setName(b.name);
		
		this.mapId = b.mapId;
		this.mapName=b.mapName;
		this.creationDate = b.creationDate;
		this.firstMappingVersionDate = b.firstMappingVersionDate;
		this.lastMappingVersionDate = b.lastMappingVersionDate;
		this.numberOfMappingVersions = b.numberOfMappingVersions;
		
	}
	
	private int mapId;
	private String mapName;
	private SimplifiedGregorianCalendar creationDate;
	private SimplifiedGregorianCalendar firstMappingVersionDate;
	private SimplifiedGregorianCalendar lastMappingVersionDate;
	private int numberOfMappingVersions;
	
	public int getMapId(){
		return this.mapId;
	}
	
	public SimplifiedGregorianCalendar getCreationDate(){
		return this.creationDate;
	}
	
	public SimplifiedGregorianCalendar getFirstMappingVersionDate(){
		return this.firstMappingVersionDate;
	}
	
	public SimplifiedGregorianCalendar getLastMappingVersionDate(){
		return this.lastMappingVersionDate;
	}
	
	public String getMapName(){
		return this.mapName;
	}
	
	public int getNumberOfMappingVersions(){
		return this.numberOfMappingVersions;
	}
    public static class Builder implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 2998705073169827027L;

		private int analysisId=0;
		
		private final String name ;
		private int mapId=0;
		private String mapName="N/A";
		private SimplifiedGregorianCalendar creationDate = DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar firstMappingVersionDate=DefaultVersionStartDate.getInstance().getDate();
		private SimplifiedGregorianCalendar lastMappingVersionDate=DefaultVersionStartDate.getInstance().getDate();
		private int numberOfMappingVersions=0 ;
	    
	    public Builder(String name) {
			this.name=name;
			
		}
		
	    public Builder analysisId(int id){
	    	this.analysisId=id;
	    	return this;
	    }
	
		
		public Builder firstMappingVersionDate(SimplifiedGregorianCalendar cal) {
			this.firstMappingVersionDate = cal;
			return this;
		}
		
		public Builder lastMappingVersionDate(SimplifiedGregorianCalendar cal) {
			this.lastMappingVersionDate = cal;
			return this;
		}
		
		public Builder numberOfMappingVersions(int k) {
			this.numberOfMappingVersions = k;
			return this;
		}
		
		public Builder mapId(int mapId) {
			this.mapId=mapId;
			return this;
		}
		
		public Builder mapName(String mapName){
			this.mapName=mapName;
			return this;
		}
		
		public Builder creationDate(SimplifiedGregorianCalendar cal){
			this.creationDate=cal;
			return this;
		}
		public StabilityAnalysis build() {
			return new StabilityAnalysis(this);
		}
		
		
	}
}
