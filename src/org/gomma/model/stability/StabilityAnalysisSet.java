package org.gomma.model.stability;


import org.gomma.model.AbstractGenericObjectSet;


public class StabilityAnalysisSet extends AbstractGenericObjectSet<StabilityAnalysis> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4821762063592414752L;
	
	/**
	 * The constructor initializes the class Mappings.
	 */
	public StabilityAnalysisSet() {
		super();
	}
	
	public void addStabilityAnalysis(StabilityAnalysis sa){
		if (sa==null) 
			throw new NullPointerException("Could not resolve the Stability analysis that should be added.");
		super.addElement(sa);
	
	}


}
