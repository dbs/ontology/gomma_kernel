package org.gomma.model;

/**
 * The abstract class implements some general methods for managing generic objects.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public abstract class AbstractGenericObject extends Object implements GenericObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5481252125021367196L;

	/**
	 * The item represents the internal unique identifier of the object. 
	 */
	private final int objectID;
	
	/**
	 * The item represents the name of of the object.
	 */
	private String objectName;
	
	
	/**
	 * The constructor initializes the class.
	 * @param id unique identifier of the object
	 */
	public AbstractGenericObject(int id) {
		this.objectID = id;
		this.objectName = "";
	}
	
	/**
	 * @see GenericObject#getID()
	 */
	public int getID() {
		return this.objectID;
	}
	
	/**
	 * @see GenericObject#getName()
	 */
	public String getName() {
		return this.objectName;
	}
	
	/**
	 * @see GenericObject#setName(String)
	 */
	public void setName(String name) {
		this.objectName = name;
	}
	
	/**
	 * @see Object#hashCode()
	 */
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.objectID;
		result = 31 * result + this.objectName.hashCode();
		return result;
	}
	
	public String toString() {
		return this.objectName+" ("+this.objectID+")";
	}
}
