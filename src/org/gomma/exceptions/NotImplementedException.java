package org.gomma.exceptions;

/**
 * The class represents the exception of the GOMMA system
 * that is called whenever a functionality is not implemented.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class NotImplementedException extends GommaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6446266113100216620L;

	/**
	 * The constructor initializes the exception class using a message
	 * describing the error that have been occurred.
	 * @param message error message
	 */
	public NotImplementedException(String message) {
		super(message);
	}
}
