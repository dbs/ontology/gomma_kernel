/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.exceptions;

import org.gomma.exceptions.GommaException;

/**
 * The class ConfigurationException should be called whenever the configuration 
 * of the GOMMA system are ggoing wrong.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.4
 */
public class ConfigurationException extends GommaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1819180918798483362L;

	/**
	 * The constructor initializes the exception class using a message
	 * describing the error that have been occurred.
	 * @param message error message
	 */
	public ConfigurationException(String message) {
		super(message);
	}
}
