/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.exceptions;

import org.gomma.exceptions.GommaException;

/**
 * The class represents an exception of the GOMMA system. It is 
 * called whenever a graph operation cannot be correctly executed. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class GraphOperationExecutionException extends GommaException {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3424583632011864673L;

	/**
	 * The constructor initializes the exception class using a message
	 * describing the error that have been occurred.
	 * @param message error message
	 */
	public GraphOperationExecutionException(String message) {
		super(message);
	}
}
