package org.gomma.util;

import java.util.Comparator;

import org.gomma.model.mapping.MappingVersion;


public class MappingVersionDateComparator implements Comparator<MappingVersion> {
	
	public int compare(MappingVersion m1, MappingVersion m2) {
		if (m1.getCreationDate().before(m2.getCreationDate()))
			return 0;
		else
			return 1;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		
		return (o instanceof MappingVersionDateComparator);
	}
}
