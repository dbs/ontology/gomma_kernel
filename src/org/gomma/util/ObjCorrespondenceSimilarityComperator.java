package org.gomma.util;

import java.util.Comparator;

import org.gomma.model.mapping.ObjCorrespondence;

public class ObjCorrespondenceSimilarityComperator implements Comparator<ObjCorrespondence> {

	//@Override
	public int compare(ObjCorrespondence o1, ObjCorrespondence o2) {
		if (o1.getConfidence()<o2.getConfidence()) return -1;
		else if (o1.getConfidence()>o2.getConfidence()) return 1;
		return 0;
	}
	public boolean equals(Object o) {
		return (o instanceof ObjCorrespondenceSimilarityComperator);
	}
}
