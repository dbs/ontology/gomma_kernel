package org.gomma.util.math;

/**
 * The class implements mathematical function which are not available in java.lang.Math.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class MyMath {

	/**
	 * The method returns the logarithm for the basis 2.
	 * @param d decimal number for which the logarithm will be computed
	 * @return logarithm value of the specified decimal value
	 */
	public static double log2(double d) {
		return Math.log(d)/Math.log(2.0);
	}
	
	/**
	 * The method returns the logarithm for the basis 10.
	 * @param d decimal number for which the logarithm will be computed
	 * @return logarithm value of the specified decimal value
	 */
	public static double log10(double d) {
		return Math.log10(d);
	}
	
	/**
	 * The method returns the simple sigmoid value for a given value x. The 
	 * sigmoid value is computed by s = 1 / (1 + e^(-1*x))
	 * @param x decimal number for which the simple sigmoid will be computed
	 * @return logarithm value of the specified decimal value
	 */
	public static double sigmoid(float x) {
		return 1 / (1.0 + Math.pow(Math.E,-1*x));
	}
	
	/**
	 * The method returns the sigmoid value for a given value x using the 
	 * linear scaling modifiers a and b. The sigmoid value is computed by
	 * s = 1 / (1 + e^(-a*x+b)) 
	 * @param a linear coefficient of the exponent -a*x+b 
	 * @param x decimal number for which the sigmoid will be computed
	 * @param b decimal number of the exponent -a*x+b
	 * @return logarithm value of the specified decimal value
	 */
	public static double sigmoid(float a, float x, float b) {
		return 1 / (1.0 + Math.pow(Math.E,-a*x+b));
	}
	
	/**
	 * The method returns the absolute number of the given number. The absolute 
	 * number if always the positive number. If the given number is infinite
	 * then the method returns the infinite.
	 * @param a number
	 * @return absolute number 
	 */
	public static long abs(long a) {
		if (a<0) return a*-1;
		return a;
	}
	
	public static float round(float value, int decimalPlace) {
		if (value==Float.NaN || Float.isInfinite(value)) return value;
		
		double power_of_ten = 1D;
		double fudge_factor = 0.05D;
		
		while (decimalPlace-->0) {
			power_of_ten *= 10.0D;
			fudge_factor /= 10.0D;
		}
		return (float)(java.lang.Math.round((value + fudge_factor) * power_of_ten ) / power_of_ten);
	}
}
