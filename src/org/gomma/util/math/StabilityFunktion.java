package org.gomma.util.math;

import java.util.List;
import org.gomma.exceptions.OperatorException;

public interface StabilityFunktion {
	/**
	 * The method returns the correspondence stability which will be computed by given
	 * a list of confidence for the correspondence in the history 
	 * @param list of confidence
	 * @return stability for this correspondence
	 * @throws OperatorException
	 */
	public float computeCorrespondenceStabiltiy(List<Float> confidenceList) throws OperatorException;
	
	
}
