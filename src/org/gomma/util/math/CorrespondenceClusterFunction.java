package org.gomma.util.math;

import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.source.SourceVersionStructure;

public interface CorrespondenceClusterFunction {

	/**
	 * The method returns true if iff the cluster-function-specific objects are linked in the source graphs;
	 * otherwise it returns false.
	 * @param domain domain source graph
	 * @param range range source graph
	 * @param c1 correspondence 1
	 * @param c2 correspondence 2
	 * @return true/false
	 */
	public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2);
}
