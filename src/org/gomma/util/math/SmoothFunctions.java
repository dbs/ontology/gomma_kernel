package org.gomma.util.math;

/**
 * The class implements selected smoothing function.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum SmoothFunctions {

	SIGMOID_SMOOTHING {
		public float smooth(float x) {
			return 1F / (1F + (float)Math.pow(Math.E,-x - 0.5F));
		}
	},
	
	TRIGONOMETRIC_SMOOTHING {
		public float smooth(float x) {
			return 1 - (float)(Math.sin(Math.acos(x)));
		}
	};
	
	public abstract float smooth(float x);
}
