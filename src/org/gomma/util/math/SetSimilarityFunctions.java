package org.gomma.util.math;

import org.gomma.exceptions.OperatorException;

/**
 * The enumeration provides a set of set-based similarity functions and
 * acts as wrapper for specific similarity function implementations.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum SetSimilarityFunctions {

	/**
	 * The item represents the complete overlap similarity, i.e., 
	 * sim = 1 if all elements of set 1 are also available in set 2, 
	 * and vice versa; otherwise sim=0. 
	 */
	COMPLETE {
		public String toString() {
			return "complete overlap";
		}
		public float computeConfidence(int n12, int n_12, int n1_2, int n_1_2) {
			return (n12!=0&&n_12==0&&n1_2==0 && n_1_2==0)?1:0;
		}
	},
	
	/**
	 * The item represents the cosine similarity between two sets.
	 */
	COSINE {
		public String toString() {
			return "cosine";
		}
		public float computeConfidence(int n12, int n_12, int n1_2, int n_1_2) {
			return SimilarityFunctionImpl.cosine(n12,n12+n1_2,n12+n_12);
		}
	},
	
	/**
	 * The item represents the Dice similarity between two sets
	 */
	DICE {
		public String toString() {
			return "dice";
		}
		public float computeConfidence(int n12, int n_12, int n1_2, int n_1_2) {
			return SimilarityFunctionImpl.dice(n12,n12+n1_2,n12+n_12);
		}
	},
	
	/**
	 * The item represents the Jaccard similarity between two sets.
	 */
	JACCARD {
		public String toString() {
			return "jaccard";
		}
		public float computeConfidence(int n12, int n_12, int n1_2, int n_1_2) {
			return SimilarityFunctionImpl.jaccard(n12,n12+n1_2,n12+n_12);
		}
	};
	
	/**
	 * The method returns the similarity between two overlapping object sets
	 * taking the number of overlapping objects and set objects into account. 
	 * @param n12 number of objects that are part of both object sets O1 and O2, i.e., |intersection(O1,O2)|
	 * @param n_12 number of objects that are part of object set O2 but not of O1, i.e., |diff(O2,O1)|
	 * @param n1_2 number of objects that are part of object set O1 but not of O2, i.e., |diff(O1,O2)| 
	 * @param n_1_2 number of objects in the universe that neither part of O1 nor O2
	 * @return similarity between object sets
	 * @throws OperatorException
	 */
	public abstract float computeConfidence(int n12, int n_12, int n1_2, int n_1_2) throws OperatorException;

}
