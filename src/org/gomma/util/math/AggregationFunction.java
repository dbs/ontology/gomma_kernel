package org.gomma.util.math;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;

import org.gomma.util.date.DateHandler;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The enumeration provides a set of aggregation functions,
 * such as average, maximum, and minimum, including their 
 * implementation.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum AggregationFunction implements Serializable {

	/**
	 * The element represents the average aggregation function.
	 */
	AVERAGE {
		@Override
		public String toString() {
			return "average";
		}
		/**
		 * @see AggregationFunction#getLabel()
		 */
		public String getLabel() {
			return "Average-Aggregation";
		}
		/**
		 * @see AggregationFunction#aggregate(int, int)
		 */
		public float aggregate(int a, int b) {		
			return (float)(a+b)/(float)2;
		}
		/**
		 * @see AggregationFunction#aggregate(float, float)
		 */
		public float aggregate(float a, float b) {
			//float value =(float)(a+b)/(float)2;
			return (float)(a+b)/(float)2;
		}
		/**
		 * @see AggregationFunction#aggregate(double, double)
		 */
		public double aggregate(double a, double b) {
			return (double) (a+b)/(double)2;
		}
		/**
		 * @see AggregationFunction#aggregateIntegerList(List)
		 */
		public float aggregateIntegerList(List<Integer> list) {
			int result = 0;
			for (int value : list) result += value;
			return list.size()==0?0F:(float) result / (float)list.size();
		}
		/**
		 * @see AggregationFunction#aggregateFloatList(List)
		 */
		public float aggregateFloatList(List<Float> list) {
			float result = 0;
			for (float value : list) result += value;
			return list.size()==0?0F:(float) result / (float)list.size();
		}
		/**
		 * @see AggregationFunction#aggregate(double, double)
		 */
		public double aggregateDoubleList(List<Double> list) {
			double result = 0;
			for (double value : list) result += value;
			return list.size()==0?0D:result / (double)list.size();
		}
		/**
		 * @see AggregationFunction#aggregate(Calendar, Calendar)
		 */
		public Calendar aggregate(Calendar c1, Calendar c2) {
			Calendar result = c1.before(c2)?c1:c2;
			result.add(Calendar.DATE,(DateHandler.getDifferenceInDays(c1,c2) / 2));
			return result;
		}
		
		/**
		 * @see AggregationFunction#aggregate(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
		 */
		public SimplifiedGregorianCalendar aggregate(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2) {
			SimplifiedGregorianCalendar result = new SimplifiedGregorianCalendar(c1.before(c2)?c1:c2);
			result.add(Calendar.DATE, DateHandler.getDifferenceInDays(c1,c2) / 2);
			return result;
		}
	},
	
	
	/**
	 * The elements represents the minimum aggregation function.
	 */
	MINIMUM {
		@Override
		public String toString() {
			return "minimum";
		}
		/**
		 * @see AggregationFunction#getLabel()
		 */
		public String getLabel() {
			return "Minimum-Aggregation";
		}
		/**
		 * @see AggregationFunction#aggregate(int, int)
		 */
		public float aggregate(int a, int b) {
			return (float)a<b?a:b;
		}
		/**
		 * @see AggregationFunction#aggregate(float, float)
		 */
		public float aggregate(float a, float b) {
			return (float)a<b?a:b;
		}
		/**
		 * @see AggregationFunction#aggregate(double, double)
		 */
		public double aggregate(double a, double b) {
			return (double)a<b?a:b;
		}
		/**
		 * @see AggregationFunction#aggregateIntegerList(List)
		 */
		public float aggregateIntegerList(List<Integer> list) {
			int result=list.size()>0?list.get(0):Integer.MAX_VALUE;
			for (int value : list) 
				if (result>value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateFloatList(List)
		 */
		public float aggregateFloatList(List<Float> list) {
			float result=list.size()>0?list.get(0):Float.POSITIVE_INFINITY;
			for (float value : list) 
				if (result>value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateDoubleList(List)
		 */
		public double aggregateDoubleList(List<Double> list) {
			double result=list.size()>0?list.get(0):Double.POSITIVE_INFINITY;
			for (double value : list) 
				if (result>value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregate(Calendar, Calendar)
		 */
		public Calendar aggregate(Calendar c1, Calendar c2) {
			return c1.before(c2)?c1:c2;
		}
		
		/**
		 * @see AggregationFunction#aggregate(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
		 */
		public SimplifiedGregorianCalendar aggregate(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2) {
			return c1.before(c2)?c1:c2;
		}
	},
	
	/**
	 * The elements represents the maximum aggregation function.
	 */
	MAXIMUM {
		@Override
		public String toString() {
			return "maximum";
		}
		/**
		 * @see AggregationFunction#getLabel()
		 */
		public String getLabel() {
			return "Maximum-Aggregation";
		}
		/**
		 * @see AggregationFunction#aggregate(int, int)
		 */
		public float aggregate(int a, int b) {
			return (float)a<b?b:a;
		}
		/**
		 * @see AggregationFunction#aggregate(float, float)
		 */
		public float aggregate(float a, float b) {
			return (float)a<b?b:a;
		}
		/**
		 * @see AggregationFunction#aggregate(double, double)
		 */
		public double aggregate(double a, double b) {
			return (double)a<b?b:a;
		}
		/**
		 * @see AggregationFunction#aggregateIntegerList(List)
		 */
		public float aggregateIntegerList(List<Integer> list) {
			int result = list.size()>0?list.get(0):Integer.MIN_VALUE;
			for (int value : list) 
				if (result<value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateFloatList(List)
		 */
		public float aggregateFloatList(List<Float> list) {
			float result = list.size()>0?list.get(0):Float.NEGATIVE_INFINITY;
			for (float value : list) 
				if (result<value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateDoubleList(List)
		 */
		public double aggregateDoubleList(List<Double> list) {
			double result = list.size()>0?list.get(0):Double.NEGATIVE_INFINITY;
			for (double value : list) 
				if (result<value) result=value;
			return result;
		}
		/**
		 * @see AggregationFunction#aggregate(Calendar, Calendar)
		 */
		public Calendar aggregate(Calendar c1, Calendar c2) {
			return c1.before(c2)?c2:c1;
		}
		
		/**
		 * @see AggregationFunction#aggregate(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
		 */
		public SimplifiedGregorianCalendar aggregate(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2) {
			return c1.before(c2)?c2:c1;
		}
	},
	
	/**
	 * The element represents the summarization aggregation function.
	 */
	SUM {
		@Override
		public String toString() {
			return "sum";
		}
		/**
		 * @see AggregationFunction#getLabel()
		 */
		public String getLabel() {
			return "Sum-Aggregation";
		}
		/**
		 * @see AggregationFunction#aggregate(int, int)
		 */
		public float aggregate(int a, int b) {
			return (float)a+b;
		}
		/**
		 * @see AggregationFunction#aggregate(float, float)
		 */
		public float aggregate(float a, float b) {
			return (float)a+b;
		}
		/**
		 * @see AggregationFunction#aggregate(double, double)
		 */
		public double aggregate(double a, double b) {
			return (double)a+b;
		}
		/**
		 * @see AggregationFunction#aggregateIntegerList(List)
		 */
		public float aggregateIntegerList(List<Integer> list) {
			int result = 0;
			for (int value : list) result +=value; 
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateFloatList(List)
		 */
		public float aggregateFloatList(List<Float> list) {
			float result = 0F;
			for (float value : list) result += value; 
			return result;
		}
		/**
		 * @see AggregationFunction#aggregateDoubleList(List)
		 */
		public double aggregateDoubleList(List<Double> list) {
			double result = 0D;
			for (double value : list) result += value; 
			return result;
		}
		/**
		 * @see AggregationFunction#aggregate(Calendar, Calendar)
		 */
		public Calendar aggregate(Calendar c1, Calendar c2) {
			Calendar c3 = (Calendar)c1.clone();
			c3.add(Calendar.DAY_OF_MONTH,c2.get(Calendar.DAY_OF_MONTH));
			c3.add(Calendar.MONTH,c2.get(Calendar.MONTH));
			c3.add(Calendar.YEAR,Math.abs(c1.get(Calendar.YEAR)-c2.get(Calendar.YEAR)));
			return c3;
		}
		
		/**
		 * @see AggregationFunction#aggregate(SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
		 */
		public SimplifiedGregorianCalendar aggregate(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2) {
			return new SimplifiedGregorianCalendar(c1.getTimeInMillis()+c2.getTimeInMillis());
		}
	};
	
	/**
	 * The method executes the specified aggregation and returns its result for two specified integer values. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract float  aggregate(int a, int b);
	
	/**
	 * The method executes the specified aggregation and returns its result for two specified float values. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract float  aggregate(float a, float b);
	
	/**
	 * The method executes the specified aggregation and returns its result for two specified double values. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract double aggregate(double a, double b);
	
	/**
	 * The method executes the specified aggregation and returns its result for specified integer value list. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract float  aggregateIntegerList(List<Integer> list);
	
	/**
	 * The method executes the specified aggregation and returns its result for specified integer float list. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract float  aggregateFloatList(List<Float> list);
	
	/**
	 * The method executes the specified aggregation and returns its result for specified double value list. 
	 * @param a value 1
	 * @param b value 2
	 * @return aggregation result
	 */
	public abstract double aggregateDoubleList(List<Double> list);
	
	/**
	 * The method executes the specified aggregation and returns its result for two specified calendar values. 
	 * @param c1 value 1
	 * @param c2 value 2
	 * @return aggregation result
	 */
	public abstract Calendar aggregate(Calendar c1, Calendar c2);
	
	/**
	 * The method executes the specified aggregation and returns its result for two specified calendar values. 
	 * @param c1 value 1
	 * @param c2 value 2
	 * @return aggregation result
	 */
	public abstract SimplifiedGregorianCalendar aggregate(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2);
	/**
	 * The method returns the label of the aggregation method.
	 * @return aggregation method label
	 */
	public abstract String getLabel();
	
	public static AggregationFunction resolveAggregationFunction(String label) throws NoSuchElementException {
		for (AggregationFunction f : AggregationFunction.values())
			if (f.getLabel().equals(label))
				return f;
		throw new NoSuchElementException("Could not resolve the aggregation function '"+label+"'.");
	}
}
