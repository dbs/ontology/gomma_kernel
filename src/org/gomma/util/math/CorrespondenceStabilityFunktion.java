package org.gomma.util.math;

import java.util.List;
import org.gomma.exceptions.OperatorException;


public enum CorrespondenceStabilityFunktion implements StabilityFunktion {
	
	AVERAGE {
		public String toString() {
			return "average stability of a given confidence set during the history for the correspondence ";
		}
		public float computeCorrespondenceStabiltiy(List<Float> confidenceList) throws OperatorException{
			int kMax=confidenceList.size()-1;
			int cnt = 0;
		
			//System.out.println("kMax="+kMax);
			float stabAvg = 1.0F; //Defaultwert?
			float sum=0;
			if(kMax>0){
				if(confidenceList.size()!=0){
					for(int i=0;i<kMax;i++){
						sum+=Math.abs(confidenceList.get(i+1)-confidenceList.get(i));
						//System.out.println("sum="+sum);
						//System.out.println("i="+i);
					}
					stabAvg=1-(sum/((float)kMax));
				} else throw new OperatorException("could not catch confidence set which are used to calculate the Stability.");
			}
			return stabAvg;
		}
	},
	
	WEIGHTED_MAX {
		public String toString() {
			return "weighted maximum stability of a given confidence set during the history for the correspodnence";
		}
		public float computeCorrespondenceStabiltiy(List<Float> confidenceList) throws OperatorException{
			//kmax is the actual previous number of versions 
			int kMax=confidenceList.size()-1;
			float stabWM=1.0F;
			float lastConfidence;
			float max=0;
			if(kMax>0){
				if(confidenceList.size()!=0){
					lastConfidence=confidenceList.get(confidenceList.size()-1);
					for(int i=0; i<kMax;i++){
						float temp=Math.abs(lastConfidence-confidenceList.get(i))/((float)(kMax-i));
					    max=Math.max(max,temp);
					    stabWM=1-max;
					}
				} else throw new OperatorException("could not catch confidence set which are used to calculate the Stability.");
			}
			
			return stabWM;
	   }
	};
}
