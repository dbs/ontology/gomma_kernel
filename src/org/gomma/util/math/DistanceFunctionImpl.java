package org.gomma.util.math;


/**
 * The class implements some selected distance functions.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DistanceFunctionImpl {

	/**
	 * The items represents the match, mismatch, and gap costs.
	 */
	private static int MATCH = 0, MISMATCH = 1, GAP = 1;
	
	/**
	 * The method returns the Hamming distance for the tow given strings.
	 * The string need not necessarily of the same length. In case of 
	 * unequal length the shorter string will be extended at the end until
	 * the same length is exceeded. 
	 * @param str1 string 1
	 * @param str2 string 2
	 * @return Hamming distance
	 */
	public static float hammingDistance(String str1, String str2) {
		String s;
		if (str1.length()==str2.length()) {
			return hamming(str1,str2);
		} else
		if (str1.length()<str2.length()) {
			s = new String(str1);
			for (int i=0;i<str2.length()-str1.length();i++) s += " ";
			return hamming(s,str2);
		} else {
			s = new String(str2);
			for (int i=0;i<str1.length()-str2.length();i++) s += " ";
			return hamming(str1,s);
		}
	}
	
	/**
	 * The method returns the Hamming distance of the two given strings. The distance is then
	 * normalized by the maximal length of both strings. hence, the distance is between 0 and 1.
	 * @param str1 string 1
	 * @param str2 string 2
	 * @return normalized Hamming distance
	 */
	public static float normalizedHammingDistance(String str1, String str2) {
		int length = str1.length()<str2.length()?str2.length():str1.length();
		return hamming(str1,str2) / length;
	}
	
	/**
	 * The method returns the Levelstein distance of the two given strings.
	 * @param str1 string 1
	 * @param str2 string 2
	 * @return Levenstein distance
	 */
	public static int levenshteinDistance(String str1, String str2) {
	    //Add a dummy character to the beginning of both strings
	    str1 = " " + str1;
	    str2 = " " + str2;
	    int n = str1.length(), m = str2.length();
	    int D[][] = new int[n][m];
	    D[0][0] = 0;
	    int i, j;
	    for (i = 1; i < n; i++)
	      D[i][0] = D[i - 1][0] + distance(null, null);
	    for (j = 1; j < m; j++)
	      D[0][j] = D[0][j - 1] + distance(null, null);
	    for (i = 1; i < n; i++) {
	      for (j = 1; j < m; j++) {
	        int m1 = D[i - 1][j] + distance(new Character(str1.charAt(i)), null);
	        int m2 = D[i - 1][j - 1] +
	            distance(new Character(str1.charAt(i)), new Character(str2.charAt(j)));
	        int m3 = D[i][j - 1] + distance(null, new Character(str2.charAt(j)));
	        D[i][j] = (int)AggregationFunction.MINIMUM.aggregate(AggregationFunction.MINIMUM.aggregate(m1,m2),m3);
	        //System.out.print(D[i][j] + " ");
	      }
	      //System.out.println("");
	    }
	    return D[n - 1][m - 1];
	  }
	
	/**
	 * The method returns the Levenstein distance of the two given strings which 
	 * then normalized by the maximal length of the given string.
	 * @param str1 string 1
	 * @param str2 string 2
	 * @return normalized Levenstein distance
	 */
	public static float normalizedLevensteinDistance(String str1, String str2) {
		return (float) levenshteinDistance(str1, str2)/AggregationFunction.MAXIMUM.aggregate(str1.length(),str2.length());
	}
	
	/**
	 * The method returns the euclidean distance of the two given integer vectors. 
	 * @param a vector 1
	 * @param b vector 2
	 * @return euclidean distance
	 */
	public static float euclidicDistance(int[] a, int[] b) {
		if (a.length!=b.length) return 0;
		
		int sum=0;
		for (int i=0;i<a.length;i++)
			sum += Math.pow(a[i]-b[i],2);
		return (float)Math.sqrt(sum);
	}
	
	/**
	 * The method returns the euclidean distance of the two given float vectors. 
	 * @param a vector 1
	 * @param b vector 2
	 * @return euclidean distance
	 */
	public static float euclidicDistance(float[] a,float[] b){
		if (a.length!=b.length) return 0;
		
		float sum=0;
		for (int i=0;i<a.length;i++)
			sum += Math.pow(a[i]-b[i],2);
		return (float)Math.sqrt(sum);
	}
	
	/**
	 * The method returns the manhatten distance of the two given integer vectors. 
	 * @param a vector 1
	 * @param b vector 2
	 * @return Manhatten distance
	 */
	public static float manhattenDistance(int[] a, int[] b) {
		if (a.length!=b.length) return 0;
		
		int sum=0;
		for (int i=0;i<a.length;i++)
			sum += Math.abs(a[i]-b[i]);
		return (float)sum;
	}
	
	/**
	 * The method returns the manhatten distance of the two given float vectors. 
	 * @param a vector 1
	 * @param b vector 2
	 * @return manhatten distance
	 */
	public static float manhattenDistance(float[] a, float[] b) {
		if (a.length!=b.length) return 0;
		
		float sum=0;
		for (int i=0;i<a.length;i++)
			sum += Math.abs(a[i]-b[i]);
		return (float)sum;
	}
	
	/**
	 * The method returns the Hamming distance of two strings which are of equal length.
	 * @param str1 string 1
	 * @param str2 string 2
	 * @return Hamming distance
	 */
	private static float hamming(String str1, String str2) {
		int nEquals=0;
		for (int i=0;i<str1.length();i++)
			if (str1.charAt(i)==str2.charAt(i)) nEquals++;
		return str1.length()-nEquals;
	}
	
	/**
	 * The method returns the distance between two given characters using 
	 * constantly defined costs (see constants of this class).
	 * @param a character 1
	 * @param b character 2
	 * @return distance
	 */
	private static int distance(Character a, Character b) {
		if (a == null || b == null) return GAP;
	    if (!a.equals(b)) return MISMATCH;
	    return MATCH;
	}
	
}
