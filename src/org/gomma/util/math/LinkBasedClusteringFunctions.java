package org.gomma.util.math;

import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The enumeration provides a set of clustering functions including their implementation.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum LinkBasedClusteringFunctions implements CorrespondenceClusterFunction {

	/**
	 * The element represents the domain object link clustering method,
	 * i.e., two correspondences are grouped together when their domain
	 * objects are directly linked. 
	 */
	DOMAIN_OBJECT_LINK {
		
		/**
		 * @see LinkBasedClusteringFunctions#isLinked(SourceVersionStructure, SourceVersionStructure, ObjCorrespondence, ObjCorrespondence)
		 */
		public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2) {
			return 
				domain.isDirectlyConnected(domain.getObject(c1.getDomainObjID()), domain.getObject(c2.getDomainObjID())) ||
				domain.isDirectlyConnected(domain.getObject(c2.getDomainObjID()), domain.getObject(c1.getDomainObjID()));
		}
	},
	
	/**
	 * The element represents the range object link clustering method,
	 * i.e., two correspondences are grouped together when their range
	 * objects are directly linked. 
	 */
	RANGE_OBJECT_LINK {
		
		/**
		 * @see LinkBasedClusteringFunctions#isLinked(SourceVersionStructure, SourceVersionStructure, ObjCorrespondence, ObjCorrespondence)
		 */
		public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2) {
			return 
				range.isDirectlyConnected(range.getObject(c1.getRangeObjID()), range.getObject(c2.getRangeObjID())) ||
				range.isDirectlyConnected(range.getObject(c2.getRangeObjID()), range.getObject(c1.getRangeObjID()));
		}
	},
	
	/**
	 * The element represents the single object link clustering method,
	 * i.e., two correspondences are grouped together when at least one 
	 * of their domain and range objects are directly linked. 
	 */
	SINGLE_OBJECT_LINK {
		
		/**
		 * @see LinkBasedClusteringFunctions#isLinked(SourceVersionStructure, SourceVersionStructure, ObjCorrespondence, ObjCorrespondence)
		 */
		public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2) {
			return 
				DOMAIN_OBJECT_LINK.isLinked(domain, range, c1, c2) || RANGE_OBJECT_LINK.isLinked(domain, range, c1, c2);
		}
	},
	
	
	/**
	 * The element represents the both object link clustering method,
	 * i.e., two correspondences are grouped together when both of 
	 * their domain and range objects are directly linked. 
	 */
	BOTH_OBJECT_LINK_UNDIRECTED {
		
		/**
		 * @see LinkBasedClusteringFunctions#isLinked(SourceVersionStructure, SourceVersionStructure, ObjCorrespondence, ObjCorrespondence)
		 */
		public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2) {
			return 
				(domain.isDirectlyConnected(domain.getObject(c1.getDomainObjID()), domain.getObject(c2.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c1.getRangeObjID()), range.getObject(c2.getRangeObjID()))) ||
				
				(domain.isDirectlyConnected(domain.getObject(c1.getDomainObjID()), domain.getObject(c2.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c2.getRangeObjID()), range.getObject(c1.getRangeObjID()))) ||
				
				 (domain.isDirectlyConnected(domain.getObject(c2.getDomainObjID()), domain.getObject(c1.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c2.getRangeObjID()), range.getObject(c1.getRangeObjID()))) ||
				 
				 (domain.isDirectlyConnected(domain.getObject(c2.getDomainObjID()), domain.getObject(c1.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c1.getRangeObjID()), range.getObject(c2.getRangeObjID())));
		}
	},
	
	
	/**
	 * The element represents the both object link clustering method,
	 * i.e., two correspondences are grouped together when both of 
	 * their domain and range objects are directly linked. 
	 */
	BOTH_OBJECT_LINK_DIRECTED {
		
		/**
		 * @see LinkBasedClusteringFunctions#isLinked(SourceVersionStructure, SourceVersionStructure, ObjCorrespondence, ObjCorrespondence)
		 */
		public boolean isLinked(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondence c1, ObjCorrespondence c2) {
			return 
				(domain.isDirectlyConnected(domain.getObject(c1.getDomainObjID()), domain.getObject(c2.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c1.getRangeObjID()), range.getObject(c2.getRangeObjID()))) ||
				 
				(domain.isDirectlyConnected(domain.getObject(c2.getDomainObjID()), domain.getObject(c1.getDomainObjID())) &&
				 range.isDirectlyConnected(range.getObject(c2.getRangeObjID()), range.getObject(c1.getRangeObjID())));
		}
	};
	
}
