/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: 
 * 
 **/
package org.gomma.util.math;

/**
 * The class contains simple computational methods.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SimpleOperations {

	/**
	 * The method returns the average of two given integer values.
	 * @param i value i
	 * @param j value j
	 * @return average / mean of the given values
	 */
	public static float avg (int i, int j) {
		return (i+j)/2;
	}
	
	/**
	 * The method returns the average of two given float values.
	 * @param i value i
	 * @param j value j
	 * @return average / mean of the given values
	 */
	public static float avg (float i, float j) {
		return (i+j)/2;
	}
	
	/**
	 * The method returns the maximum of two given integer values.
	 * @param i value i
	 * @param j value j
	 * @return maximum of the given values
	 */
	public static int max (int i, int j) {
		return i<j?j:i;
	}
	
	/**
	 * The method returns the maximum of two given float values.
	 * @param i value i
	 * @param j value j
	 * @return maximum of the given values
	 */
	public static float max (float i, float j) {
		return i<j?j:i;
	}
	
	/**
	 * The method returns the minimum of two given integer values.
	 * @param i value i
	 * @param j value j
	 * @return minimum of the given values
	 */
	public static int min (int i, int j) {
		return i<j?i:j;
	}
	
	/**
	 * The method returns the minimum of two given float values.
	 * @param i value i
	 * @param j value j
	 * @return minimum of the given values
	 */
	public static float min (float i, float j) {
		return i<j?i:j;
	}
	
	public static float sum (int i, int j) {
		return i+j;
	}
	
	public static float sum (float i, float j) {
		return i+j;
	}
	
}
