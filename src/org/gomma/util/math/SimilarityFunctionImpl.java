/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: 
 * 
 **/
package org.gomma.util.math;

import java.util.List;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.operator.SetOperators;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.algorithm.MaximalCommonSubGraph;

/**
 * The class implements selected similarity functions. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SimilarityFunctionImpl {

	/**
	 * The method returns the dice similarity value.
	 * @param n12 number of shared objects that are in both, n1 and n2
	 * @param n1 number of relevant objects in n1
	 * @param n2 number of relevant objects in n2 
	 * @return dice similarity value
	 */
	public static float dice (int n12, int n1, int n2) {
		return (float) 2 * n12 / (float) (n1 + n2);
	}
	
	/**
	 * The method returns the cosine similarity value,
	 * @param n12 number of shared objects that are in both, n1 and n2
	 * @param n1 number of relevant objects in n1
	 * @param n2 number of relevant objects in n2 
	 * @return cosine similarity value
	 */
	public static float cosine (int n12, int n1, int n2) {
		return (float) n12 / (float) Math.sqrt((float) (n1 * n2));
	}
	
	/**
	 * The method returns the jaccard similarity value.
	 * @param n12 number of shared objects that are in both, n1 and n2
	 * @param n1 number of relevant objects in n1
	 * @param n2 number of relevant objects in n2 
	 * @return jaccard similarity value
	 */
	public static float jaccard (int n12, int n1, int n2) {
		return (float) n12 / (float) (n1 + n2 - n12);
	}
	
	/**
	 * The method returns the term frequency tf. In this more general case
	 * it can also be called as object frequency. The term frequency has been
	 * introduced in information retrieval; see for example 
	 * Baza-Yates/Ribeiro-Neto: Modern Information Retrieval, Adisson Wesley, 1999. 
	 * @param freq_ij frequency of object i in object corpus j
	 * @param max_freq_j maximal frequency of all objects in object corpus j 
	 * @return term / object frequency
	 */
	public static float tf (int freq_ij, int max_freq_j) {
		return (float) freq_ij / max_freq_j;
	}
	
	/**
	 * The method returns the inverse doument frequency idf. In this more general
	 * case it can also be called as inverse object corpus frequency. The inverse
	 * document frequency has been introduced in information retrieval; see for
	 * example Baza-Yates/Ribeiro-Neto: Modern Information Retrieval, Adisson Wesley, 1999. 
	 * @param n number of documents of the corpus
	 * @param n_i number of documents using the term i
	 * @return inverse doument frequency
	 */
	public static float idf (int n, int n_i) {
		return (float) Math.log( 1 + (n / n_i) );
	}
	
	/**
	 * The method returns the term frequency - inverse document frequency tf-idf.
	 * @param freq_ij frequency of object i in object corpus j
	 * @param max_freq_j maximal frequency of all objects in object corpus j
	 * @param n number of documents of the corpus
	 * @param n_i number of documents using the term i
	 * @return term frequency - inverse document frequency
	 */
	public static float tfidf (int freq_ij, int max_freq_j, int n, int n_i) {
		return tf(freq_ij,max_freq_j) / idf(n,n_i);
	}
	
	/**
	 * The method returns the normalized symmetric difference of both, object sets 
	 * and object relationship sets taking the object identifiers into account. In 
	 * particular, the normalized symmetric difference d is calculated as follows
	 * 
	 * d_objs = |intersect(objs(G1),objs(G2))| / |union(objs(G1),objs(G2))| 
	 * d_rels = |intersect(rels(G1),rels(G2))| / |union(rels(G1),rels(G2))|
	 * 
	 * d = 0.5 * (d_objs + d_rels)
	 * 
	 * @param objs1 first object set
	 * @param objRels1 first object relationship set
	 * @param objs2 second object set
	 * @param objRels2 second object relationship set
	 * @return normalized symmetric difference
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static float normalizedSymmetricDifference(ObjSet objs1, ObjRelationshipSet objRels1, ObjSet objs2, ObjRelationshipSet objRels2) throws OperatorException, WrongSourceException {
		return AggregationFunction.AVERAGE.aggregate(
				(float)SetOperators.intersect(objs1,objs2,true,false).size() / 
						(float)SetOperators.union(objs1,objs2,true).size(),
				(float)SetOperators.intersect(objRels1,objRels2,AggregationFunction.MAXIMUM).size() /
				(float)SetOperators.union(objRels1,objRels2,AggregationFunction.MAXIMUM).size());
	}

	
	public static <N extends Node,E extends Edge> float normalizedSymmetricDifference(Graph<N,E> g1, Graph<N,E> g2) throws GraphInitializationException, GraphOperationExecutionException {
		
		int nG1NodeSize = g1.getNodeSetSize(),
			nG2NodeSize = g2.getNodeSetSize(),
			nG1EdgeSize = g1.getEdgeSetSize(),
			nG2EdgeSize = g2.getEdgeSetSize(),
			nMcsNodeSize=0,
			nMcsEdgeSize=0;

		MaximalCommonSubGraph<N,E> mcs = new MaximalCommonSubGraph<N,E>(g1,g2);
		mcs.computeMaximalCommonSubgraph(true);
		
		List<Graph<N,E>> t = mcs.getG1SubGraphList();
 
		if (t.size()!=0) {
			Graph<N,E> g = t.get(0);
			nMcsNodeSize = g.getNodeSetSize();
			nMcsEdgeSize = g.getEdgeSetSize();
		}
		
		return AggregationFunction.AVERAGE.aggregate((float)(2 * nMcsNodeSize)/(nG1NodeSize + nG2NodeSize),
				(float)(2 * nMcsEdgeSize)/(nG1EdgeSize + nG2EdgeSize));
	}
	
//	public static float prefix(String str1, String str2) {
//		String str1Sub = str1.length()<str2.length()?str1:str1.substring(0,str2.length()-1);
//		String str2Sub = str1.length()<str2.length()?str2.substring(0,str1.length()-1):str2;
//		int nEquals=0;
//		for (int i=0;i<str1Sub.length();i++) {
//			if (str1Sub.charAt(i)==str2Sub.charAt(i)) nEquals++;
//		}
//		return (float)nEquals/str1Sub.length();
//	}
//	
//	public static float suffix(String str1, String str2) {
//		String str1Sub = str1.length()<str2.length()?str1:str1.substring(str1.length()-str2.length()-1);
//		String str2Sub = str2.length()<str2.length()?str2.substring(str2.length()-str1.length()-1):str2;
//		int nEquals=0;
//		for (int i=0;i<str1Sub.length();i++) {
//			if (str1Sub.charAt(i)==str2Sub.charAt(i)) nEquals++;
//		}
//		return (float)nEquals/str1Sub.length();
//	}
}
