package org.gomma.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * The class implements methods for comparing, computing differences etc. of dates.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DateHandler {

	private DateHandler() {}
	
	private static int[] DATE_PER_MONTH = new int[] {31,28,31,30,31,30,31,31,30,31,30,31};
	
	
	/**
	 * The method transforms and returns the string elements of the given list into calendar objects.
	 * @param list list of string elements that should be transformed to calendar objects 
	 * @return list of calendar objects
	 * @throws ParseException
	 */
	public static List<Calendar> toGregorianCalendarList(List<String> list) throws ParseException {
		List<Calendar> valueList = new ArrayList<Calendar>();
		for (String s : list)
			if (s!= null && !s.trim().equals("")) valueList.add(toGregorianCalendar(s));
		return valueList;
	}
	

	
	/**
	 * The method returns the difference in years between the given years, 
	 * i.e., the difference in months divided by 12
	 * @param c1 date 1
	 * @param c2 date 2
	 * @return difference in years between the two given dates  
	 */
	public static int getDifferenceInYears(Calendar c1, Calendar c2) {
		return getDifferenceInMonths(c1,c2)/12;
	}
	
	/**
	 * The method returns the difference in months between the two given calendar dates.
	 * It doesn't matter if the first date is before the second or vice versa. 
	 * @param c1 date 1
	 * @param c2 date 2
	 * @return difference in months between the two given dates
	 */
	public static int getDifferenceInMonths(Calendar c1, Calendar c2) {
		return Math.abs((c1.get(Calendar.YEAR)*12+c1.get(Calendar.MONTH))-
				(c2.get(Calendar.YEAR)*12+c2.get(Calendar.MONTH)));
	}
	
	/**
	 * The method returns the difference in days between the two given calendar dates.
	 * The method recognizes what date is before the other. 
	 * @param c1 date 1
	 * @param c2 date 2
	 * @return difference in days between the two given dates
	 */
	public static int getDifferenceInDays(Calendar c1, Calendar c2) {
		int daySum=0;
		Calendar minCal = c1.before(c2)?c1:c2;
		Calendar maxCal = c1.before(c2)?c2:c1;

		if (minCal.get(Calendar.YEAR)==maxCal.get(Calendar.YEAR)) {
			
			if (minCal.get(Calendar.MONTH)==maxCal.get(Calendar.MONTH)) {
				daySum = maxCal.get(Calendar.DAY_OF_MONTH) - minCal.get(Calendar.DAY_OF_MONTH); 
			} else {
				boolean isLeapYear = isLeapYear(minCal);
				
				if (isLeapYear && minCal.get(Calendar.MONTH)==Calendar.FEBRUARY) daySum++;
				daySum += DATE_PER_MONTH[minCal.get(Calendar.MONTH)]-minCal.get(Calendar.DAY_OF_MONTH);
				
				for (int month=minCal.get(Calendar.MONTH)+1;month<maxCal.get(Calendar.MONTH);month++) {
					if (month==Calendar.FEBRUARY && isLeapYear(minCal)) daySum++;
					daySum += DATE_PER_MONTH[month];
				}
				
				if (isLeapYear && maxCal.get(Calendar.MONTH)==Calendar.FEBRUARY) daySum++;
				daySum += DATE_PER_MONTH[maxCal.get(Calendar.MONTH)]-maxCal.get(Calendar.DAY_OF_MONTH);
			}
		} else {
			daySum += isLeapYear(minCal)?
					366-minCal.get(Calendar.DAY_OF_YEAR):365-minCal.get(Calendar.DAY_OF_YEAR);
			for (int year=minCal.get(Calendar.YEAR)+1;year<maxCal.get(Calendar.YEAR);year++)
				daySum += isLeapYear(minCal,year)?366:365;
			daySum += maxCal.get(Calendar.DAY_OF_YEAR);
		} //System.out.println(" : "+daySum);
		return daySum;
	}
	
	
	public static List<SimplifiedGregorianCalendar> toSimplifiedGregorianCalendarList(List<String> list) throws ParseException {
		List<SimplifiedGregorianCalendar> valueList = new ArrayList<SimplifiedGregorianCalendar>();
		for (String s : list)
			if (s!= null && !s.trim().equals("")) valueList.add(toSimplifiedGregorianCalendar(s));
		return valueList;
	}
	
	/**
	 * The method returns the difference in days between the two given calendar dates.
	 * The method recognizes what date is before the other. 
	 * @param c1 date 1
	 * @param c2 date 2
	 * @return difference in days between the two given dates
	 */
	public static int getDifferenceInDays(SimplifiedGregorianCalendar c1, SimplifiedGregorianCalendar c2) {
		return (int)(Math.abs(c1.getTimeInMillis()-c2.getTimeInMillis())/(3600 * 1000 * 24));
	}
	

	
	/**
	 * The method transforms and returns a single calendar date from the specified date string.
	 * @param dateString date string from which the calendar date should be extracted
	 * @return calendar date
	 * @throws ParseException
	 */
	private static Calendar toGregorianCalendar(String dateString) throws ParseException {
		if (dateString==null) throw new NullPointerException("Could not resolve the date to be converted.");
		
		String sep=".", format = "dd.mm.yyyy";
		SimpleDateFormat df;
		
		if (dateString.contains("-")) sep="-";
		else if (dateString.contains("/")) sep="/";
		
		if (dateString.matches("[0-9][0-9]"+sep+"[0-9][0-9]"+sep+"[0-9][0-9][0-9][0-9]")) 
			format = "dd"+sep+"MM"+sep+"yyyy";
		else if (dateString.matches("[0-9][0-9][0-9][0-9]"+sep+"[0-9][0-9]"+sep+"[0-9][0-9]")) 
			format="yyyy"+sep+"MM"+sep+"dd";
		
		df = new SimpleDateFormat(format);
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(df.parse(dateString));
	
		return cal;
	}
	
	/**
	 * The method transforms and returns a single calendar date from the specified date string.
	 * @param dateString date string from which the calendar date should be extracted
	 * @return calendar date
	 * @throws ParseException
	 */
	private static SimplifiedGregorianCalendar toSimplifiedGregorianCalendar(String dateString) throws ParseException {
		if (dateString==null) throw new NullPointerException("Could not resolve the date to be converted.");
		
		String sep=".", format = "dd.mm.yyyy";
		SimpleDateFormat df;
		
		if (dateString.contains("-")) sep="-";
		else if (dateString.contains("/")) sep="/";
		
		if (dateString.matches("[0-9][0-9]"+sep+"[0-9][0-9]"+sep+"[0-9][0-9][0-9][0-9]")) 
			format = "dd"+sep+"MM"+sep+"yyyy";
		else if (dateString.matches("[0-9][0-9][0-9][0-9]"+sep+"[0-9][0-9]"+sep+"[0-9][0-9]")) 
			format="yyyy"+sep+"MM"+sep+"dd";
		
		df = new SimpleDateFormat(format);
		SimplifiedGregorianCalendar cal = SimplifiedGregorianCalendar.getInstance();
		cal.setTime(df.parse(dateString));
	
		return cal;
	}
	
	/**
	 * The method returns true if the date is in a leap year; otherwise it returns false.
	 * @param c date
	 * @return true/false
	 */
	private static boolean isLeapYear(Calendar c) {
		return isLeapYear(c,c.get(Calendar.YEAR));
	}
	
	/**
	 * The method returns true if the date is in a leap year; otherwise it returns false.
	 * @param c date
	 * @param year year including the type of the calendar, e.g. Gregorian calendar etc.
	 * @return true/false
	 */
	private static boolean isLeapYear(Calendar c, int year) {
		if (c instanceof GregorianCalendar) {
			if (year % 400 == 0) return true;
			else if (year % 100 == 0) return false;
			else if (year % 4 == 0) return true;
			else return false;
		} else {
			return false;
		}
	}
	
}
