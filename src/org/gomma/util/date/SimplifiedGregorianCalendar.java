package org.gomma.util.date;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * The class is a simplified and synchronized version of the Gregorian 
 * calendar. It holds internally only the time-in-milli-seconds. Therefore, 
 * it does not manage date-times from different time zones. 
 * 
 * To set, get or calculate with calendar fields, such as YEAR, MONTH, 
 * and DAY_OF_MONTH, the class generates the Gregorian calendar at runtime. 
 * We assume that the additional access time is rather minimal. In contrast 
 * to the original Java-Calendar and GregorianCalendar instances of the 
 * class should be very space efficient in both, in their use in a application 
 * and by transferring the calendar data to another host (e.g., RMI service).
 * 
 * A further alternative for managing date-times is the JODA calendar API which is
 * as of Oct., 8th, 2009 available under http://joda-time.sourceforge.net.
 *    
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SimplifiedGregorianCalendar implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5279736641142693245L;
	
	private transient long timeInMillis;
	
	/**
	 * Default constructor of the class.
	 */
	public SimplifiedGregorianCalendar() {
		this.timeInMillis = System.currentTimeMillis();
	}
	
	/**
	 * The static method always creates and returns an instance 
	 * of the class. It returns not a reference to a singleton instance!  
	 * @return new instance of the class
	 */
	public static SimplifiedGregorianCalendar getInstance() {
		return new SimplifiedGregorianCalendar();
	}
	
	/**
	 * The constructor initializes the class by copying the date 
	 * and time zone from the given calendar object.
	 * @param cal calendar object from which the date and time zone should be copied
	 */
	public SimplifiedGregorianCalendar(Calendar cal) {
		this.timeInMillis = cal.getTimeInMillis();
	}
	
	/**
	 * The constructor initializes the class. The new serialized calendar object is 
	 * created as copy of the input calendar object.  
	 * @param cal serialized gregorian calendar
	 */
	public SimplifiedGregorianCalendar(SimplifiedGregorianCalendar cal) {
		this.timeInMillis = cal.getTimeInMillis();
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param year year
	 * @param month month field
	 * @param dayOfMonth day of the month
	 */
	public SimplifiedGregorianCalendar(int year, int month, int dayOfMonth) {
		Calendar cal = new GregorianCalendar(year,month,dayOfMonth);
		this.timeInMillis = cal.getTimeInMillis();
		
		cal=null;
	}
	
	public SimplifiedGregorianCalendar(long timeInMillis) {
		this.timeInMillis = timeInMillis;
	}
	
	/**
	 * @see GregorianCalendar#set(int, int)
	 */
	public synchronized void set(int field, int value) {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(this.timeInMillis);
		
		cal.set(field, value);
		this.timeInMillis = cal.getTimeInMillis();
		
		cal=null;
	}
	
	/**
	 * @see GregorianCalendar#set(int, int, int)
	 */
	public synchronized void set(int year, int month, int dayOfMonth) {
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month);
		this.set(Calendar.DAY_OF_MONTH, dayOfMonth);
	}
	
	public synchronized void setTime(Date date) {
		this.timeInMillis = date.getTime();
	}
	
	/**
	 * @see GregorianCalendar#after(Object)
	 */
	public synchronized boolean after(Object when) {
		if (!(when instanceof SimplifiedGregorianCalendar)) return false;
		return this.timeInMillis>((SimplifiedGregorianCalendar)when).getTimeInMillis();
	}
	
	/**
	 * @see GregorianCalendar#before(Object)
	 */
	public synchronized boolean before(Object when) {
		if (!(when instanceof SimplifiedGregorianCalendar)) return false;
		return this.timeInMillis<((SimplifiedGregorianCalendar)when).getTimeInMillis();
	}
	
	/**
	 * @see GregorianCalendar#add(int, int)
	 */
	public synchronized void add(int field, int amount) {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(this.timeInMillis);
		cal.add(field, amount);
		
		this.timeInMillis = cal.getTimeInMillis();
		cal=null;
	}

	/**
	 * @see GregorianCalendar#get(int)
	 */
	public synchronized int get(int field) {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(this.timeInMillis);
		
		int amount = cal.get(field);
		cal=null;
		
		return amount;
	}
	
	/**
	 * @see GregorianCalendar#getTime()
	 */
	public synchronized Date getTime() {
		return new Date(this.timeInMillis);
	}
	
	/**
	 * @see GregorianCalendar#getTimeInMillis()
	 * @return
	 */
	public synchronized long getTimeInMillis() {
		return this.timeInMillis;
	}
	
	public Calendar toCalendar() {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(this.timeInMillis);
		return cal;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (!(o instanceof SimplifiedGregorianCalendar)) return false;
		return this.timeInMillis==((SimplifiedGregorianCalendar)o).getTimeInMillis();
	}
	
	/**
	 * @see Object#hashCode()
	 */
	public int hashCode() {
		int result = 17;
		result = result ^ (result >>> this.timeInMillis);
		
		return result;
	}
	
	public String toString() {
		Calendar cal = this.toCalendar();
		String result = "timeInMilliSeconds: "+this.timeInMillis+
						"; Year: "+cal.get(Calendar.YEAR)+
						"; Month: "+cal.get(Calendar.MONTH)+
						"; DayOfMonth: "+cal.get(Calendar.DAY_OF_MONTH);
		cal = null;
		
		return result;
	}
	public String getFormattedDate() {
		Calendar cal = this.toCalendar();
		String result = cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH)+1) + "/" +cal.get(Calendar.YEAR);
		cal = null;
		
		return result;
	}
	public String getFormattedTimestamp() {
		Calendar cal = this.toCalendar();
		String result = cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH)+1) + "-" +cal.get(Calendar.YEAR);
		result += "_"+cal.get(Calendar.HOUR_OF_DAY)+"h "+cal.get(Calendar.MINUTE)+"min "+cal.get(Calendar.SECOND)+"s";
		cal = null;
		
		return result;
	}
	/**
     * We cannot change the signature of the method, so we use an
     * in-method-synchronization.
     *
     * @param out The output stream to serialize the class.
     * @throws java.io.IOException if there are problems to write into the stream.
     */
    private synchronized void writeObject(ObjectOutputStream out) throws IOException {
         out.defaultWriteObject();
         
         out.writeLong(this.timeInMillis);
         out.flush();
    }
    
    /**
     * We cannot change the signature of the method, so we use an
     * in-method-synchronization.
     * 
     * @param in The input stream to de-serialize the class.
     * @throws IOException if there are problems to read from the stream
     * @throws ClassNotFoundException if the class cannot be found
     */
    private synchronized void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
   		in.defaultReadObject();
   		
   		this.timeInMillis = in.readLong();
    }
}
