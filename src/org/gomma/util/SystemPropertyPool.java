package org.gomma.util;

import org.gomma.api.util.DataSourceSystems;

/**
 * The class implements methods for managing overall system properties.
 * The class is a singleton.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SystemPropertyPool {

	/**
	 * The item represents the singleton instance.
	 */
	private volatile static SystemPropertyPool singleton = null;
	
	/**
	 * The item represents the data source system that is used to store the repository.
	 */
	private DataSourceSystems dataSourceSystem; 
	
	/**
	 * Constructor of the class.
	 * @param b builder
	 */
	private SystemPropertyPool(Builder b) {
		this.dataSourceSystem = b.dataSourceSystem;
	}
	
	/**
	 * The method returns an instance of the class.
	 * @return instance of the class
	 */
	public static SystemPropertyPool getInstance() {
		return singleton;
	}
	
	/**
	 * The method returns the data source system in that the repository is stored.
	 * @return data source system
	 */
	public DataSourceSystems getDataSourceSystem() {
		return this.dataSourceSystem;
	}
	
	/**
	 * The method clears all system properties.
	 */
	public void shutdown() {
		this.dataSourceSystem = null;
	}
	
	/**
	 * The method stops the singleton instance.
	 */
	public static void stop() {
		singleton = null;
	}
	
	public static class Builder {
		private DataSourceSystems dataSourceSystem = DataSourceSystems.MYSQL;
		
		public Builder() {}
		
		public Builder dataSourceSystem(DataSourceSystems s) {
			this.dataSourceSystem = s;
			return this;
		}
		
		public SystemPropertyPool build() {
			singleton = new SystemPropertyPool(this); 
			return singleton;
		}
	}
}
