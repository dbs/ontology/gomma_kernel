package org.gomma.util;

import java.util.Random;

/**
 * The class represents the generator producing identifiers 
 * for job objects which can be submitted to and processed by
 * the job execution service.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since: JDK1.5
 */
public final class IDGenerator {

	/**
	 * The item represents the singleton of the generator.
	 */
	private static volatile IDGenerator singleton = null;
	
	private int highValue;
	private int objID;
	private int sourceID;
	
	/**
	 * The constructor initializes the class.
	 */
	private IDGenerator() {
		Random randomizer = new Random();
		highValue = randomizer.nextInt(10000);
		sourceID = randomizer.nextInt(10000);
	}
	
	/**
	 * The method returns an instance of the generator. If the generator 
	 * is not initialized, then the generator will be created. 
	 * @return instance of the job id generator
	 */
	public static IDGenerator getInstance() {
		if (singleton==null) {
			synchronized (IDGenerator.class) {
				if (singleton==null)
					singleton = new IDGenerator();
			}
		}
		return singleton;
	}
	
	/**
	 * The method stops the job id generator.
	 */
	public static void stop() {
		singleton = null;
	}
	
	/**
	 * The method returns the next job identifier that will be used internally.
	 * @return internal job identifier
	 */
	public long generateNextID() {
		long generatedID = (long)this.highValue;
		generatedID = generatedID << 24;
		generatedID += (this.objID++) << 32;
		generatedID += this.sourceID;
		return generatedID;
	}
}
