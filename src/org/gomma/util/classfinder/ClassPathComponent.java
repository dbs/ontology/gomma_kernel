package org.gomma.util.classfinder;


/**
 * 
 * @author Andre Rothe (arothe@kksl.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ClassPathComponent {

	private static final String FS = System.getProperty("file.separator");
	private String fileName;
	private String classPathComponent;

	/**
	 * We only know the class name (in the case of an archive). The file name
	 * must start with an "/"!
	 * 
	 * @param fileName
	 *            The name of a file
	 */
	public ClassPathComponent(String fileName) {
		this.fileName = fileName;
		this.classPathComponent = "";
	}

	/**
	 * We know a file name and a class path component. The latter we have to
	 * remove from the file name in order to get the class name! Both names must
	 * start with an "/"!
	 * 
	 * @param classPath
	 *            The class path component
	 * @param fileName
	 *            The name of a file within the class path component
	 */
	public ClassPathComponent(String classPath, String fileName) {
		this(fileName);
		this.classPathComponent = classPath;

		if (!getName().startsWith(classPath)) {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Get the full-qualified file name of the file.
	 * 
	 * @return The file name.
	 */
	public String getName() {
		return this.fileName;
	}

	/**
	 * Get the class name of the file, which the JVM can use to load the class.
	 * It contains dots instead of file separators and it hasn't a file
	 * extension like ".class"
	 * 
	 * @return The full-qualified class name.
	 */
	public String getClassName() {
		String res = "";
		try {

			res = getName().substring(this.classPathComponent.length() + 1);
			res = res.replace(FS, ".");
			res = res.replace("/", ".");
			res = res.substring(0, res.lastIndexOf("."));

		} catch (StringIndexOutOfBoundsException ex) {
			// class path and file name are equal
			// could be an jar archive
		}
		return res;
	}
}
