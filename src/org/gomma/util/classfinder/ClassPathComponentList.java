package org.gomma.util.classfinder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 
 * @author Andre Rothe (arothe@kksl.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ClassPathComponentList extends ArrayList<ClassPathComponent> {

	private static final long serialVersionUID = 4058256126747264152L;
	private static final String CP = System.getProperty("java.class.path");
	private static final String PS = System.getProperty("path.separator");
	private static final String FS = System.getProperty("file.separator");

	public ClassPathComponentList() {
		super();
		getClassPathComponents();
	}

	/**
	 * The top level class path component list can contain files, archives and
	 * directories. We have to check every component for class files. The
	 * components are separated by a path separator.
	 */
	private void getClassPathComponents() {

		StringTokenizer st = new StringTokenizer(CP, PS);
		while (st.hasMoreTokens()) {
			File f = new File(st.nextToken());
			checkComponent(f.getPath(), f);
		}
	}

	/**
	 * checks the given directory (check every component)
	 * 
	 * @param compName
	 *            the class path component where the directory exists
	 * @param dir
	 *            the directory reference
	 */
	private void checkDirectory(String compName, File dir) {

		String[] list = dir.list();
		for (int i = 0; i < list.length; i++) {

			File f = new File(list[i]);

			// Eclipse schneidet "build" ab wegen JAR-files!
			// getAbsolutePath() geht hier deshalb nicht
			f = new File(dir.getPath() + FS + f.getName());
			checkComponent(compName, f);
		}
	}

	/**
	 * checks the given file, if it is a directory, it will be checked
	 * recursively, if it is a file, it could be an archive, check that too
	 * 
	 * @param compName
	 *            The class path component in which the file exists
	 * @param f
	 *            the given file (or archive or directory)
	 */
	private void checkComponent(String compName, File f) {

		if (f.isDirectory()) {
			checkDirectory(compName, f);
		} else {
			if (isArchiveFile(f)) {
				checkArchiveFile(f);
			} else {
				add(new ClassPathComponent(compName, f.getAbsolutePath()));
			}
		}
	}

	/**
	 * Opens the given file as JAR archive.
	 * 
	 * @param f
	 *            the archive file
	 * @return true, if the file is an archive, false otherwise
	 */
	private boolean isArchiveFile(File f) {

		boolean res = false;
		try {
			new JarFile(f);
			res = true;
		} catch (IOException ex) {
			// no jar file given
		}
		return res;
	}

	/**
	 * get every file/dir within a JAR-archive, the iterator returns them all
	 * add the files to the class list
	 * 
	 * @param f
	 *            the archive
	 */
	private void checkArchiveFile(File f) {

		try {

			JarFile jar = new JarFile(f);
			Enumeration<JarEntry> e = jar.entries();
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (!je.isDirectory()) {

					// it is possible to have JARs within JARs, but
					// these must be specified within the Manifest
					// TODO: investigate Manifest to get more
					// class path-information
					
					add(new ClassPathComponent(FS + je.getName()));
				}
			}

		} catch (IOException ex) {
			// no archive
		}

	}
}
