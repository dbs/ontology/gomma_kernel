package org.gomma.util.classfinder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Andre Rothe (arothe@kksl.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ClassChecker {

	/**
	 * The list of class files to proof
	 */
	private ClassPathComponentList list;

	/**
	 * The constructor, it saves the class name list
	 * 
	 * @param list
	 *            A list of class names.
	 */
	public ClassChecker(ClassPathComponentList list) {
		this.list = list;
	}

	/**
	 * Get a list of all class names, which have a specified superclass or
	 * interface.
	 * 
	 * @param superClassName The full qualified name of the class or interface
	 * @param packageName
	 * @return a list of matching class names
	 */
	public List<String> getClassWithSuperClass(String packageName, String superClassName) {
		
		ArrayList<String> res = new ArrayList<String>();
		Iterator<ClassPathComponent> i = list.iterator();

		while (i.hasNext()) {
			ClassPathComponent c = i.next();
			if (c.getClassName().startsWith(packageName) && hasSuperClass(c.getClassName(), superClassName)) {
				res.add(c.getClassName());
			}
		}

		return res;
	}

	/**
	 * Helper method which checks superclass or interfaces of a given class.
	 * 
	 * @param className
	 *            The class which we have to check.
	 * @param superClassName
	 *            The interface or class name we are looking for.
	 * @return true, if the class has a matching interface or superclass, false
	 *         otherwise
	 */
	private boolean hasSuperClass(String className, String superClassName) {
		Class<?> c = null;
		Class<?> sc = null;
		
		try {
		
			if (className.equals(superClassName)) return false;
			
			sc = Class.forName(superClassName,false,this.getClass().getClassLoader());
			c = Class.forName(className,false,this.getClass().getClassLoader());

//		} catch (ClassNotFoundException ex) {
			// a class name is wrong
		} catch (Throwable t) {
			return false;
		}

		return ((c != null) ? sc.isAssignableFrom(c) : false);
	}

	// das kann hier erweitert werden um zusätzliche Prüfmethoden

}
