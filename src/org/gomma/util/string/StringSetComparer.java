package org.gomma.util.string;

import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.util.math.SetSimilarityFunctions;

public class StringSetComparer {

	public static float compare(Map<String,Integer> set1, Map<String,Integer> set2, SetSimilarityFunctions simFunc) throws OperatorException {
		int n12=0, n_12=0, n1_2=0;
		
		for (String set1Str : set1.keySet()) {
			if (set2.containsKey(set1Str)) n12++;
			else n1_2++;
		}
		for (String set2Str : set2.keySet()) {
			if (!set1.containsKey(set2Str)) n_12++;
		}
		
		return simFunc.computeConfidence(n12,n_12,n1_2,0);

	}
}
