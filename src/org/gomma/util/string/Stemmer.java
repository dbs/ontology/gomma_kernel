package org.gomma.util.string;

import org.gomma.util.string.PorterStemmer;

public enum Stemmer {

	PORTER {
		public String toString() {
			return "Porter stemmer";
		}
		public String stem(String s) {
			PorterStemmer stem = new PorterStemmer();
			for (char c : s.toCharArray()) stem.add(c);
			stem.stem();
			return stem.toString();
		}
	};
	
	public abstract String stem(String s);
}
