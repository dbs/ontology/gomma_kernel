package org.gomma.util.string;

public class SoundexTransformation {

	public static String soundex(String str, boolean fullFlag) {
		// compute the soundex equivalent to str
		if (str == null) return null;
		if (str.length() == 0) return "";

		int iIn, iOut;
		char c, prevDig;
		String out = "";
		str = str.toLowerCase();
		if (fullFlag) {
			iIn = 0;
		    iOut = 0;
		    prevDig = '*';
		} else { // skip the first char
			iIn = 1;
		    iOut = 1;
		    out += str.charAt(0);
		    prevDig = str.charAt(0);
		}
		
		while (iIn < str.length() && iOut < 4) {
			switch (str.charAt(iIn)) {
				case 'b':
		        case 'p':
		        case 'f':
		        case 'v': c = '1'; break;
		        case 'c':
		        case 's':
		        case 'k':
		        case 'g':
		        case 'j':
		        case 'q':
		        case 'x':
		        case 'z': c = '2'; break;
		        case 'd':
		        case 't': c = '3'; break;
		        case 'l': c = '4'; break;
		        case 'm':
		        case 'n': c = '5'; break;
		        case 'r': c = '6'; break;
		        default:  c = '*';
		      }
			
		      if (c != prevDig && c != '*') {
		    	  out += c;
		    	  prevDig = c;
		    	  iOut++;
		      }
		      iIn++;
		}
		
		if (iOut < 4) for (iIn = iOut; iIn < 4; iIn++) out += '0';
		return out;
	}
}
