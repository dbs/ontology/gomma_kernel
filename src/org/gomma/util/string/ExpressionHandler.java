package org.gomma.util.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum ExpressionHandler {

	NUMERIC_EXPRESSION {
		private static final String PATTERN = "\\s+\\d+\\s+";
		public String toString() {
			return "numeric expression deletion";
		}
		public String deleteExpression(String s) {
			(" "+s+" ").replaceAll(PATTERN,"");
			return s.trim();
		}
		public boolean matchesExpression(String s) {
			Pattern p = Pattern.compile(PATTERN);
			return p.matcher(" "+s+" ").matches();
		}
		public boolean containsExpression(String s) {
			Pattern p = Pattern.compile(PATTERN);
			return p.matcher(" "+s+" ").find();
		}
		public String getExpressionString(String s) {
			Pattern p = Pattern.compile(PATTERN);
			Matcher m;
			if ((m = p.matcher(" "+s+" ")).find()) {
				return (" "+s+" ").substring(m.start(),m.end());
			} else return "";
		}
	},
	ROMANIC_EXPRESSION {
		private static final String PATTERN = "\\s+(M|DM|LM|XM|VM|IM|D|LD|XD|VD|ID|C|LC|XC|VC|IC|L|XL|VL|IL|X|VX|IX|V|IV|I)+\\s+"; 
		public String toString() {
			return "romanic expression deletion";
		}
		public String deleteExpression(String s) {
			(" "+s+" ").replaceAll(PATTERN,"");
			return s.trim();
		}
		public boolean matchesExpression(String s) {
			Pattern p = Pattern.compile(PATTERN);
			return p.matcher(" "+s.toUpperCase()+" ").matches();
		}
		public boolean containsExpression(String s) {
			Pattern p = Pattern.compile(PATTERN);
			return p.matcher(" "+s.toUpperCase()+" ").find();
		}
		public String getExpressionString(String s) {
			Pattern p = Pattern.compile(PATTERN);
			Matcher m;
			if ((m = p.matcher(" "+s.toUpperCase()+" ")).find()) {
				return (" "+s+" ").substring(m.start(),m.end()).trim();
			} else return "";
		}
	},
	COPULA_ENGLISH {
		private final String[] COPULA_LIST = new String[] {
				"a", "and", "or", "for", "in", "out", "not", "the"
		};
		
		public String toString() {
			return "copula English";
		}
		public String deleteExpression(String s) {
			for (String pattern : COPULA_LIST) 
				s=s.replaceAll("\\s+"+pattern+"\\s+"," ");
			return s.trim();
		}
		public boolean matchesExpression(String s) {
			return false;
		}
		public boolean containsExpression(String s) {
			return false;
		}
		public String getExpressionString(String s) {
			return "";
		}
	};
	
	public abstract String deleteExpression(String s);
	public abstract boolean matchesExpression(String s);
	public abstract boolean containsExpression(String s);
	public abstract String getExpressionString(String s);
}
