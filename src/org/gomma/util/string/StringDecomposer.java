package org.gomma.util.string;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.Version;

public class StringDecomposer {

	public static Map<String,Integer> decomposeOverlappingLength(String str, int length) {
		return decomposeOverlappingLength(str,length,1);
	}
	
	public static Map<String,Integer> decomposeOverlappingLength(String str, int length, int stepwidth) {
		Map<String,Integer> result = new Object2IntOpenHashMap<String>();
		String t;
		
		if (str.length()==0) return result;
		
		if (str.length()<=length) {
			result.put(str, 1);
			return result;
		}
		
		for (int idx=0;idx+length<=str.length();idx+=stepwidth) {
			t=str.substring(idx,idx+length);
			if (result.containsKey(t)) result.put(t,result.get(t)+1);
			else result.put(t,1);
		}
		return result;
	}
	
	
	public static Map<String,Integer> decomposeWords(String str) {
		return decomposeWords(str,"\\d");
	}
	
	public static Map<String,Integer> decomposeWords(String str, String whiteSpacePattern) {
		Map<String,Integer> result = new Object2IntOpenHashMap<String>();
		
		if (str.length()==0) return result;
		
		for (String t : str.split(whiteSpacePattern)) {
			if (result.containsKey(t)) result.put(t,result.get(t)+1);
			else result.put(t,1);
		}
		return result;
	}

	public static Map<String, Integer> fillAndDecomposeOverlappingLength(String str, int length) {
		
		if (str.length()==0) return decomposeOverlappingLength(str, length);
		
		for(int i = 0;i<length-1;i++){
			str = "$"+str+"$";
		}
		return decomposeOverlappingLength(str, length);
	}
	
	public static Map<String,Integer>  decomposeWordFrequenciesLucene(String s1){
		
		StandardAnalyzer analyzer= new StandardAnalyzer(Version.LUCENE_36);
	    Map<String,Integer>  result= new HashMap<String,Integer>();

	    TokenStream tokenStream= analyzer.tokenStream(null, new StringReader(s1));
	    TermAttribute termAttribute = tokenStream.getAttribute(TermAttribute.class);
	    
	    try
	    {
	        while(tokenStream.incrementToken())
	        {
	            String word= termAttribute.term();
	       
	            if(result.containsKey(word))
	                result.put(word, result.get(word)+1);
	            else
	                result.put(word, 1);
	        }
	    }
	    catch(Exception e)
	    {
	        throw new RuntimeException("Tokenizing attribute value failed", e);
	    }
	    
	    return result;
	}
}
