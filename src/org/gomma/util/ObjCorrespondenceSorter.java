package org.gomma.util;

import java.util.Arrays;
import java.util.Collections;

import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

public class ObjCorrespondenceSorter {

	private ObjCorrespondence[] array;
	
	public ObjCorrespondenceSorter(ObjCorrespondenceSet objCorrs) {
		this.array = new ObjCorrespondence[objCorrs.size()];
		int i=0;
		
		for (ObjCorrespondence objCorr : objCorrs.getCollection())
			this.array[i++] = objCorr;
	}
	
	public ObjCorrespondence[] getAscendingSimilarityOrderedArray() {
		Arrays.sort(this.array,new ObjCorrespondenceSimilarityComperator());
//		this.sortCorrespondenceArray(0,this.array.length-1);
		return this.array;
	}

	public ObjCorrespondence[] getDescendingSimilarityOrderedArray() {
		Arrays.sort(this.array,Collections.reverseOrder(new ObjCorrespondenceSimilarityComperator()));
//		this.sortCorrespondenceArray(0,this.array.length-1);
		return this.array;
	}
	
/*	private void sortCorrespondenceArray(int from, int to) {
		int pivotPos;
		if (to>from) {
			pivotPos = partitionize(from,to);
			sortCorrespondenceArray(from,pivotPos-1);
			sortCorrespondenceArray(pivotPos+1,to);
		}
	}*/
	
/*	private int partitionize(int from, int to) {
		int l = from, r = to, pivotPos = (from + to) / 2;
		ObjCorrespondence t;
		
		while (l <= r) {
			while (this.array[l].confidence>=this.array[pivotPos].confidence && l<pivotPos) l++;
			while (this.array[r].confidence<this.array[pivotPos].confidence && r>pivotPos) r--;
			if (l<=r) {
				t=this.array[l];
				this.array[l]=this.array[r];
				this.array[r]=t;
				l++;r--;
			}
		}
		
		return r;
	}*/
}
