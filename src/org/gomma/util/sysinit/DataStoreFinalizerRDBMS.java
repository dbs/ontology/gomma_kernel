package org.gomma.util.sysinit;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;

/**
 * The class implements the DataStoreFinalizer interface and performs
 * a data store finalization for the repository in case it is a 
 * relational database. 
 * @author Toralf Kirsten (tkirsten@izbi.nui-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class DataStoreFinalizerRDBMS implements DataStoreFinalizer {
	
	/**
	 * The constructor initializes the class.
	 */
	public DataStoreFinalizerRDBMS() {}
	
	/**
	 * @see DataStoreFinalizer#stop()
	 */
	public void stop() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		dbh.closeDatabaseConnection();
		DatabaseHandler.stop();
	}

}
