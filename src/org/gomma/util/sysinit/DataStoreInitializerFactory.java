package org.gomma.util.sysinit;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.gomma.api.util.DataSourceSystems;
import org.gomma.util.SystemPropertyPool;

/**
 * The class represents a factory creating different types of data store initializer
 * depending on the property 'Type' that is included in the specific gomma-conf.ini file. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class DataStoreInitializerFactory {

	/**
	 * The constant represents the parameter name describing the source type of the 
	 * repository. The parameter is part of the gomma-con.ini file.  
	 */
	private static final String SOURCE_TYPE = "Source-Type";
	
	/**
	 * The constructor initializes the class.
	 */
	private DataStoreInitializerFactory() {}
	
	/**
	 * The method returns a data store initializer depending on the
	 * set 'Type' in the gomma-conf.ini file. If no 'Type' is specified
	 * the relational database type will be initialized. 
	 * @param props set of properties
	 * @return data store initializer
	 */
	public static DataStoreInitializer getInitializer(Properties props) {
		DataStoreInitializer initializer = null;
		DataSourceSystems sourceSystem = null;
		
		if (props.containsKey(SOURCE_TYPE))
			sourceSystem = DataSourceSystems.resolveDataSourceSystem(props.getProperty(SOURCE_TYPE));
		else {
			sourceSystem = DataSourceSystems.MYSQL;
			Logger.getLogger(DataStoreFinalizerFactory.class).info(
					"Could not find a specific source type, try to use standard MYSQL repository.");
		}
		
		
		new SystemPropertyPool.Builder().dataSourceSystem(sourceSystem).build();
		
		switch (SystemPropertyPool.getInstance().getDataSourceSystem().getType()) {
			case RDBMS: initializer = getInitRDBMS(props); break;
			default: break;
		}
		
		return initializer;
	}
	
	/**
	 * The method returns the data store initializer for the type 'relational database'.
	 * @param props set of data store properties
	 * @return data store initializer for a relational database
	 */
	private static DataStoreInitializerRDBMS getInitRDBMS(Properties props) {
		return new DataStoreInitializerRDBMS(props);
	}
	
	
	
}
