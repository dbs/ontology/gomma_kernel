package org.gomma.util.sysinit;

import org.gomma.exceptions.GommaException;

/**
 * The interface defines methods for initializing the data store 
 * with respect to the repository.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public interface DataStoreInitializer {

	/**
	 * The method initializes the data store of the repository.
	 * @throws GommaException
	 */
	public void initialize() throws GommaException;

}
