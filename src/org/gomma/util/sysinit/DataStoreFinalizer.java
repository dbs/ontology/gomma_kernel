package org.gomma.util.sysinit;

import org.gomma.exceptions.RepositoryException;

/**
 * The interface defines methods that are necessary to finish the 
 * work with the repository. It is called when the complete system
 * is in the shutdown process.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public interface DataStoreFinalizer {

	/**
	 * The method finalizes the repository work and executes a disconnect. 
	 * @throws RepositoryException
	 */
	public void stop() throws RepositoryException;
}
