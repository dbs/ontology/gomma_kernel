package org.gomma.util.sysinit;

import org.gomma.util.SystemPropertyPool;

/**
 * The class create data store finalizer with respect to the repository.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class DataStoreFinalizerFactory {

	/**
	 * The constructor initializes the class.
	 */
	private DataStoreFinalizerFactory() {}
	
	/**
	 * The method returns the finalizer that is specific for the data source
	 * system type of the repository. 
	 * @return data source finalizer
	 */
	public static DataStoreFinalizer getFinalizer() {
		
		DataStoreFinalizer finalizer = null;
		
		switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
			case MYSQL: finalizer = getRDBMSFinalizer(); break;
			case FEVER: finalizer = getRDBMSFinalizer(); break;
			default: break;
		}
		
		return finalizer;
	}
	
	/**
	 * The method returns the data store finalizer that is specific
	 * for a relational database system
	 * @return relational database data store finalizer
	 */
	public static DataStoreFinalizerRDBMS getRDBMSFinalizer() {
		return new DataStoreFinalizerRDBMS();
	}
}
