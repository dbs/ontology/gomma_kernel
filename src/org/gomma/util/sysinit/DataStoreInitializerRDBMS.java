package org.gomma.util.sysinit;

import java.util.Properties;

import org.gomma.api.util.DatabaseConnectionData;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.GommaException;

/**
 * The class implements methods for initializing the connection to 
 * the repository. The repository is available as relational database. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class DataStoreInitializerRDBMS implements DataStoreInitializer {

	
	
	/**
	 * The constant represents the URL database connection property name.
	 */
	public static final String DB_URL = "URL";
	
	/**
	 * The constant represents the JDBC driver property name.
	 */
	public static final String DB_DRIVER = "JDBC-Driver";
	
	/**
	 * The constant represents the database user property name.
	 */
	public static final String DB_USER = "User";
	
	/**
	 * The constant represents the database password property name.
	 */
	public static final String DB_PASSWORD = "Password";
	
	/**
	 * The item represents the set of properties holding the connection data.
	 */
	private Properties props;

	/**
	 * The constructor initializes the class.
	 * @param props connection properties
	 */
	public DataStoreInitializerRDBMS(Properties props) {
		this.props = props;
	}
	
	/**
	 * The method returns the data store type.
	 * @return data store type
	 */
	public static String getType() {
		return "rdbms";
	}
	
	/**
	 * @see DataStoreInitializer#initialize()
	 */
	public void initialize() throws GommaException {
		
		DatabaseConnectionData repositoryConData = new DatabaseConnectionData();
		repositoryConData.setDatabaseDriver(props.getProperty(DB_DRIVER));
		repositoryConData.setDatabaseUrl(props.getProperty(DB_URL));
		repositoryConData.setDatabaseUser(props.getProperty(DB_USER));
		repositoryConData.setDatabasePw(props.getProperty(DB_PASSWORD));
		
		try {
			DatabaseHandler dbh = DatabaseHandler.getInstance();
			dbh.setDatabaseConnectionData(repositoryConData);
			dbh.createDatabaseConnection();
		} catch (Exception e) {
			throw new GommaException(e.getMessage());
		}
	}
}
