package org.gomma.util;
import java.io.Serializable;
import java.util.Comparator;

import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;


public class ObjComparator implements Comparator<Obj>,Serializable {

	
	private static final long serialVersionUID = 7804480873705982770L;
	
	private String attribute;
	
	public ObjComparator(String attributeName) {
		this.attribute = attributeName;
	}
	
	public int compare(Obj arg0, Obj arg1) {
		String arg0Value = this.getAttValue(arg0, attribute);
		String arg1Value = this.getAttValue(arg1, attribute);
		return arg0Value.compareTo(arg1Value);
	}
	
	private String getAttValue(Obj thisObj,String attribute) {
		AttributeSet atts = thisObj.getAttributeValues().getAttributeSet();
		String value = null;

		for (Attribute att : atts.getAttributeSet(attribute).getCollection()) {
			for (AttributeValueVersion avv : thisObj.getAttributeValues(att)
					.getCollection()) {
				value = avv.toString();
			}
		}
		return value;
	}
}
