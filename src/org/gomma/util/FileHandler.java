package org.gomma.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileHandler {
	
	public static BufferedReader getReader(String fileLocation) {
		BufferedReader reader;
		try {
			FileInputStream file = new FileInputStream(fileLocation);
			InputStreamReader isr = new InputStreamReader (file);
			reader = new BufferedReader (isr);
		} catch (FileNotFoundException e) {
			InputStream is = FileHandler.class.getResourceAsStream(fileLocation);
			reader = new BufferedReader(new InputStreamReader(is));
		}
		return reader;
	}
	
}
