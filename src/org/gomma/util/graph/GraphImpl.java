/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.util.Collection;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The class implements some general methods 
 * handling nodes and edges within a graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class GraphImpl<N extends Node, E extends Edge> extends AbstractGraph<N,E> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8861017971466997294L;

	/**
	 * The constructor initializes the class.
	 */
	public GraphImpl() {
		super();
	}
	
//	/**
//	 * The constructor initializes the graph class.
//	 * @param maxNumberOfNodes maximal number of nodes the graph should contain
//	 * @param maxNumberOfEdges maximal number of edges the graph should contain
//	 */
//	public GraphImpl(int maxNumberOfNodes, int maxNumberOfEdges) {
//		super(maxNumberOfNodes,maxNumberOfEdges);
//	}
	
	/**
	 * @see Graph#addEdge(Edge)
	 */
	public synchronized void addEdge(E e) throws GraphInitializationException {
		super.addEdge(e,false);
	}
	
	/**
	 * @see Graph#getEdgeCollection(Node)
	 */
	public Collection<E> getEdgeCollection(N n) {
		Collection<E> edgeCollection = this.getIngoingEdgeCollection(n);
		for (E e : this.getOutgoingEdgeCollection(n))
			if (!edgeCollection.contains(e)) edgeCollection.add(e);
		return edgeCollection;
	}

	/**
	 * @see Graph#getEdgeCollection(Node, String)
	 */
	public Collection<E> getEdgeCollection(N n, String relType) {
		Collection<E> edgeCollection = this.getIngoingEdgeCollection(n,relType);
		for (E e : this.getOutgoingEdgeCollection(n,relType))
			if (!edgeCollection.contains(e)) edgeCollection.add(e);
		return edgeCollection;
	}
	
	/**
	 * @see Graph#isDirected()
	 */
	public boolean isDirected() {
		return false;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		
		if (!(o instanceof GraphImpl<?,?>)) return false;
		Graph<?,?> g = (Graph<?,?>)o;
		return super.equals(g);
	}
}
