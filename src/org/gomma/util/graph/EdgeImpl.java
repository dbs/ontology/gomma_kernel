package org.gomma.util.graph;

import java.io.Serializable;

import org.gomma.util.math.CantorDecoder;

/**
 * The class implements the edge interface. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class EdgeImpl extends Object implements Edge, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7314617137574945773L;

	
	/**
	 * The item represents the unique source node identifier.
	 */
	private int fromID;
	
	/**
	 * The item represents the unique target node identifier.
	 */
	private int toID;
	
	/**
	 * The item represents the edge type, e.g., is-a or part-of.
	 */
	private String type;
	
	
	/**
	 * The constructor initializes the class.
	 * @param fromID source node identifier
	 * @param toID target node identifier
	 */
	public EdgeImpl(int fromID, int toID) {
		this.fromID = fromID;
		this.toID   = toID;
//		this.edgeID = CantorDecoder.code(fromID,toID);
		this.type="";
	}
	
	/**
	 * @see Edge#getID()
	 */
	public long getID() {
		return CantorDecoder.code(this.fromID,this.toID);
	}
	
	/**
	 * @see Edge#getFromID()
	 */
	public int getFromID() {
		return this.fromID;
	}

	/**
	 * @see Edge#getToID()
	 */
	public int getToID() {
		return this.toID;
	}
	
	/**
	 * @see Edge#getRelationshipType()
	 */
	public String getRelationshipType() {
		return this.type;
	}
	
	/**
	 * @see Edge#setRelationshipType(String)
	 */
	public void setRelationshipType(String type) {
		this.type=type;

	}

	/**
	 * @see Edge#toString()
	 */
	@Override
	public String toString() {
		return "["+this.getFromID()+" - "+this.getToID()+"]";
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Edge)) return false;
		Edge e = (Edge)o;
		return this.getFromID()==e.getFromID() && this.getToID()==e.getToID();
	}
	
	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + fromID;
		result = 31 * result + toID;
		return result;
	}
}
