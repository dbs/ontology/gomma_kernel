package org.gomma.util.graph;

import java.io.Serializable;

/**
 * The class implements the node interface.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class NodeImpl extends Object implements Node, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5890655295136671039L;
	
	/**
	 * The item represents the unique node identifier.
	 */
	private int id;
	
	/**
	 * The constructor initializes the class by using the unique node identifier. 
	 * @param id unique node identifier
	 */
	public NodeImpl(int id) {
		this.id = id;
	}
	
	/**
	 * @see Node#getID()
	 */
	public int getID() {
		return this.id;
	}

	/**
	 * @see Node#toString()
	 */
	@Override
	public String toString() {
		return "ID: "+this.getID();
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Node)) return false;
		Node n = (Node)o;
		return this.getID()==n.getID();
	}
	
	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.id;
		return result;
	}
}
