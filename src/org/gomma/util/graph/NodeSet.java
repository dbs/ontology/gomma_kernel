package org.gomma.util.graph;

import it.unimi.dsi.fastutil.objects.ReferenceOpenHashSet;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * The class implements method for managing a set of nodes.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class NodeSet<N extends Node> extends Object implements Serializable, Iterable<N> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6535258313635635080L;
	
	/**
	 * The item represents the set of managed nodes.
	 */
	private Set<N> nodeSet;
	
	/**
	 * Constructor of the class.
	 */
	public NodeSet() {
		this.nodeSet = new ReferenceOpenHashSet<N>();
	}
	
	/**
	 * The method returns a set of of managed nodes.
	 * @return set of managed nodes
	 */
	public Set<N> getElementSet() {
		return this.nodeSet;
	}
	
	public Collection<N> getElementCollection() {
		return this.nodeSet;
	}
	
	/**
	 * @see Set#add(Object)
	 */
	public void add(N n) {
		this.nodeSet.add(n);
	}
	
	/**
	 * @see Set#contains(Object)
	 */
	public boolean contains(N n) {
		return this.nodeSet.contains(n);
	}
	
	/**
	 * @see Set#size()
	 */
	public int size() {
		return this.nodeSet.size();
	}
	
	/**
	 * The method returns true iff the current set is a subset of the 
	 * specified node set; otherwise it returns false.
	 * @param s node set
	 * @return true/false
	 */
	public boolean isSubSetOf(NodeSet<N> s) {
		if (this.size()>s.size()) return false;
		
		for (N n : this.getElementCollection())
			if (!s.contains(n)) return false;
		return true;
	}
	
	/**
	 * The method returns a node set which is a clone of the current set.
	 */
	public NodeSet<N> clone() {
		NodeSet<N> s = new NodeSet<N>();
		for (N n : this.getElementCollection()) s.add(n);
		return s;
	}
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (!(o instanceof NodeSet<?>)) return false;
		
		NodeSet<?> s = (NodeSet<?>)o;
		if (this.size()!=s.size()) return false;
		
		for (Object n : s.getElementSet())
			if (!this.nodeSet.contains(n)) return false;
		return true;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString() {
		StringBuilder buf = new StringBuilder();
		for (Node n : this.nodeSet) buf.append(n.toString()).append(",");
		return buf.substring(0,buf.length()-1);
	}

	/**
	 * @see Iterable#iterator()
	 */
	public Iterator<N> iterator() {
		return this.nodeSet.iterator();
	}
}
