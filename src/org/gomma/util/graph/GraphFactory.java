package org.gomma.util.graph;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The class represents a graph factory creating graphs of different types.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class GraphFactory {

	/**
	 * Constructor of the class.
	 */
	private GraphFactory() {}
	
	/**
	 * The method returns an undirected graph instance.
	 * The resulting graph is empty, i.e., it contains 
	 * neither nodes nor edges.
	 * @return undirected graph
	 */
	public static <N extends Node,E extends Edge> Graph<N,E> getUndirectedGraph() {
		return new GraphImpl<N,E>();
	}
	
	/**
	 * The method returns a directed graph instance.
	 * The resulting graph is empty, i.e., it contains
	 * neither nodes nor edges.
	 * @return directed graph
	 */
	public static <N extends Node,E extends Edge> DirectedGraph<N,E> getDirectedGraph() {
		return new DirectedGraphImpl<N,E>();
	}
	
	/**
	 * The method returns a directed acyclic graph instance.
	 * The resulting graph is empty, i.e., it contains
	 * neither nodes nor edges. 
	 * @return directed acyclic graph
	 */
	public static <N extends Node,E extends Edge> DirectedGraph<N,E> getDirectedAcyclicGraph() {
		return new DirectedAcyclicGraphImpl<N,E>();
	}
	
	/**
	 * The method returns a tree instance. The resulting tree
	 * is empty, i.e., it contains neither nodes nor edges.
	 * @return tree instance
	 */
	public static <N extends Node,E extends Edge> DirectedGraph<N,E> getTree() {
		return new TreeImpl<N,E>();
	}
	
	/**
	 * The method returns a directed graph instance for the specified graph type.
	 * The resulting graph is empty, i.e., it contains neither nodes nor edges.
	 * @param type graph type
	 * @return directed graph instance of the specified type
	 * @throws GraphInitializationException
	 */
	public static <N extends Node,E extends Edge> DirectedGraph<N,E> getDirectedGraph(GraphTypes type) throws GraphInitializationException {
		switch (type) {
			case DIRECTED: return new DirectedGraphImpl<N,E>();
			case DIRECTED_ACYCLIC: return new DirectedAcyclicGraphImpl<N,E>();
			case TREE : return new TreeImpl<N,E>();
			default: throw new GraphInitializationException("Could not resolve a valid graph type.");
		}
	}
}
