/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.util.ArrayList;
import java.util.Collection;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The class implements some general methods 
 * handling nodes and edges within a directed graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DirectedGraphImpl<N extends Node, E extends Edge> extends AbstractGraph<N,E> implements DirectedGraph<N,E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 400667110217295998L;

	/**
	 * The constructor initializes the class.
	 */
	public DirectedGraphImpl() {
		super();
	}
	
//	/**
//	 * The constructor initializes the graph class.
//	 * @param maxNumberOfNodes maximal number of nodes the graph should contain
//	 * @param maxNumberOfEdges maximal number of edges the graph should contain
//	 */
//	public DirectedGraphImpl(int maxNumberOfNodes, int maxNumberOfEdges) {
//		super(maxNumberOfNodes,maxNumberOfEdges);
//	}
	
	/**
	 * @see Graph#addEdge(Edge)
	 */
	public void addEdge(E e) throws GraphInitializationException {
		super.addEdge(e,true);
	}
	
	/**
	 * @see DirectedGraph#getSuccessorNodes(Node)
	 */
	public Collection<N> getSuccessorNodes(N n) {
		Collection<N> nodeCollection = new ArrayList<N>();
		for (E e : super.getOutgoingEdgeCollection(n)) {
			N a = super.nodeMap.get(e.getToID());
			if (!nodeCollection.contains(a)) nodeCollection.add(a);
		}
		return nodeCollection;
	}
	
	/**
	 * @see DirectedGraph#getPredecessorNodes(Node)
	 */
	public Collection<N> getPredecessorNodes(N n) {
		Collection<N> nodeCollection = new ArrayList<N>();
		for (E e : super.getIngoingEdgeCollection(n)) {
			N a = super.nodeMap.get(e.getFromID());
			if (!nodeCollection.contains(a)) nodeCollection.add(a);
		}
		return nodeCollection;
	}
	
	/**
	 * @see DirectedGraph#getIngoingEdges(Node)
	 */
	public Collection<E> getIngoingEdges(N n) {
		return super.getIngoingEdgeCollection(n);
	}
	
	/**
	 * @see DirectedGraph#getIngoingEdges(Node, String)
	 */
	public Collection<E> getIngoingEdges(N n, String relType) {
		return super.getIngoingEdgeCollection(n,relType);
	}
	
	/**
	 * @see DirectedGraph#getOutgoingEdges(Node)
	 */
	public Collection<E> getOutgoingEdges(N n) {
		return super.getOutgoingEdgeCollection(n);
	}
	
	/**
	 * @see DirectedGraph#getOutgoingEdges(Node, String)
	 */
	public Collection<E> getOutgoingEdges(N n, String relType) {
		return super.getOutgoingEdgeCollection(n,relType);
	}
	
	/**
	 * @see Graph#getEdgeCollection(Node)
	 */
	public Collection<E> getEdgeCollection(N n) {
		Collection<E> edges = this.getIngoingEdges(n);
		edges.addAll(this.getOutgoingEdges(n));
		return edges;
	}
	
	/**
	 * @see Graph#getEdgeCollection(Node, String)
	 */
	public Collection<E> getEdgeCollection(N n, String relType) {
		Collection<E> edges = this.getIngoingEdges(n,relType);
		edges.addAll(this.getOutgoingEdges(n,relType));
		return edges;
	}
	
	/**
	 * @see Graph#isDirected()
	 */
	public boolean isDirected() {
		return true;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		
		if (!(o instanceof Graph<?,?>)) return false;
		Graph<?,?> g = (Graph<?,?>)o;
		return super.equals(g);
	}
}
