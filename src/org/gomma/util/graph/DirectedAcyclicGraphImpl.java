/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.util.Collection;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The class implements some general methods 
 * handling nodes and edges within a directed acyclic graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DirectedAcyclicGraphImpl<N extends Node, E extends Edge> extends DirectedGraphImpl<N,E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1057734066598910113L;

	/**
	 * The constructor initializes the class.
	 */
	public DirectedAcyclicGraphImpl() {
		super();
	}
	
//	/**
//	 * The constructor initializes the graph class.
//	 * @param maxNumberOfNodes maximal number of nodes the graph should contain
//	 * @param maxNumberOfEdges maximal number of edges the graph should contain
//	 */
//	public DirectedAcyclicGraphImpl(int maxNumberOfNodes, int maxNumberOfEdges) {
//		super(maxNumberOfNodes,maxNumberOfEdges);
//	}
	
	/**
	 * @see Graph#addEdge(Edge)
	 */
	// FIXME: UEBERARBEITEN !!!
	@Override
	public void addEdge(E e) throws GraphInitializationException {
//		if (this.createsCycle(e)) 
//			throw new GraphInitializationException("The edge could not be added since it would result in a directed cyclic (!) graph.");
		super.addEdge(e);
	}
	
	/**
	 * The method returns true if adding the given edge object 
	 * to the graph would lead to graph inconsistency; otherwise
	 * the method returns false.
	 * @param e edge for which the graph consistency will be checked
	 * @return true/false
	 */
	protected boolean createsCycle(E e) {
		return isCyclic(super.getOutgoingEdges(super.getNode(e.getToID())),super.getNode(e.getFromID()));
	}
	
	/**
	 * The method returns true if there is a edge cycle within the graph;
	 * otherwise it returns false. 
	 * @param edges edge collection
	 * @param node node; if it is reached by any edge, than there is a cycle available 
	 * @return true/false
	 */
	private boolean isCyclic(Collection<E> edges, N node) {
		for (E e : edges) {
			if (e.getToID()==node.getID()) return true;
			if (isCyclic(super.getOutgoingEdges(super.getNode(e.getToID())),node))
				return true;
		}
		return false;
	}
}
