package org.gomma.util.graph;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * The enumeration provides a set of graph types including some basic functions.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum GraphTypes implements Serializable {

	/**
	 * The element represents the edgeless graph, i.e., the graph comprises only nodes. 
	 */
	EDGELESS {
		/**
		 * @see GraphTypes#getID()
		 */
		public int getID() { return 0; }
		/**
		 * @see GraphTypes#generateGraph()
		 */
		public <N extends Node,E extends Edge> Graph<N,E> generateGraph() { return GraphFactory.getUndirectedGraph(); }
		/**
		 * @see GraphTypes#isDirected()
		 */
		public boolean isDirected() {return false; }
		
		public String toString() { return "edgeless graph"; }
		/**
		 * @see GraphTypes#toXMLString()
		 */
		public String toXMLString() { return "edgeless"; } 
	},
	
	/**
	 * The element represents a undirected graph.
	 */
	UNDIRECTED {
		/**
		 * @see GraphTypes#getID()
		 */
		public int getID() { return 1; }
		/**
		 * @see GraphTypes#generateGraph()
		 */
		public <N extends Node,E extends Edge> Graph<N,E> generateGraph() { return GraphFactory.getUndirectedGraph(); }
		/**
		 * @see GraphTypes#isDirected()
		 */
		public boolean isDirected() {return false; }
		public String toString() { return "undirected graph"; }
		/**
		 * @see GraphTypes#toXMLString()
		 */
		public String toXMLString() { return "undirected"; }
	},
	
	/**
	 * The element represents a general directed graph.
	 */
	DIRECTED {
		/**
		 * @see GraphTypes#getID()
		 */
		public int getID() { return 2; }
		/**
		 * @see GraphTypes#generateGraph()
		 */
		public <N extends Node,E extends Edge> Graph<N,E> generateGraph() { return GraphFactory.getDirectedGraph(); }
		/**
		 * @see GraphTypes#isDirected()
		 */
		public boolean isDirected() {return true; }
		public String toString() { return "directed graph"; }
		/**
		 * @see GraphTypes#toXMLString()
		 */
		public String toXMLString() { return "directed"; }
	},
	
	/**
	 * The element represents a directed acyclic graph.
	 */
	DIRECTED_ACYCLIC {
		/**
		 * @see GraphTypes#getID()
		 */
		public int getID() { return 3; }
		/**
		 * @see GraphTypes#generateGraph()
		 */
		public <N extends Node,E extends Edge> Graph<N,E> generateGraph() { return GraphFactory.getDirectedAcyclicGraph(); }
		/**
		 * @see GraphTypes#isDirected()
		 */
		public boolean isDirected() {return true; }
		public String toString() { return "directed acyclic graph"; }
		/**
		 * @see GraphTypes#toXMLString()
		 */
		public String toXMLString() { return "directed_acyclic"; }
	},
	
	/**
	 * The element represents a tree. 
	 */
	TREE {
		/**
		 * @see GraphTypes#getID()
		 */
		public int getID() { return 4; }
		/**
		 * @see GraphTypes#generateGraph()
		 */
		public <N extends Node,E extends Edge> Graph<N,E> generateGraph() { return GraphFactory.getTree(); }
		/**
		 * @see GraphTypes#isDirected()
		 */
		public boolean isDirected() {return true; }
		public String toString() { return "tree"; }
		/**
		 * @see GraphTypes#toXMLString()
		 */
		public String toXMLString() { return "tree"; }
	};
	
	/**
	 * The method returns a unique identifier for the specified graph type.
	 * @return unique identifier
	 */
	public abstract int getID();
	
	/**
	 * The method returns true iff the graph type represents a directed graph; 
	 * otherwise it returns false.
	 * @return true/false
	 */
	public abstract boolean isDirected();
	
	/**
	 * The method generates and returns a graph instance of the specific type.
	 * The resulting graph is empty, i.e., it contains neither nodes nor edges.
	 * @return graph instance of the specific type
	 */
	public abstract <N extends Node,E extends Edge> Graph<N,E> generateGraph();
	
	/**
	 * The method returns a name of the graph type that can be used in XML documents.
	 * @return XML representation of the graph type name
	 */
	public abstract String toXMLString();
	
	
	/**
	 * The method returns a valid graph type for the specified unique graph type identifier. 
	 * @param id unique graph type identifier
	 * @return graph type
	 * @throws NoSuchElementException
	 */
	public static GraphTypes getGraphType(int id) throws NoSuchElementException {
		for (GraphTypes type : GraphTypes.values())
			if (type.getID()==id) return type;
		throw new NoSuchElementException("Could not resolve graph type using the identifier '"+id+"'.");
	}
}
