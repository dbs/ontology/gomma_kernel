package org.gomma.util.graph;

import java.io.Serializable;

import org.gomma.exceptions.WrongSourceException;

/**
 * The class manages statistics data for a specified node. To this data
 * belongs primarily the number of parent, children, siblings, and relations. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class NodeStatistics extends Object implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5194567270384932028L;
	
	/**
	 * The item represents the node for which the statistics data are managed.
	 */
	private Node node;
	
	/**
	 * The items represents the statistics data of the selected node.
	 */
	private int nChilds, nParents, nSiblings, nRelations;
	
	
	/**
	 * Constructor of the class.
	 * @param n node for which the statistics data should be managed
	 */
	public NodeStatistics(Node n) {
		this.node=n;
		this.nChilds=this.nParents=this.nSiblings=this.nRelations=0;
	}
	
	/**
	 * The method returns the node for that the statistics data are managed.
	 * @return node
	 */
	public Node getNode() {
		return this.node;
	}
	
	/**
	 * The method returns the number of children the selected node has. 
	 * @return number of children
	 */
	public int getNumberOfChilds() {
		return this.nChilds;
	}
	
	/**
	 * The method returns the number of parents the selected node has. 
	 * @return number of parents
	 */
	public int getNumberOfParents() {
		return this.nParents;
	}
	
	/**
	 * The method returns the number of siblings the selected node has.
	 * @return number of siblings
	 */
	public int getNumberOfSiblings() {
		return this.nSiblings;
	}
	
	/**
	 * The method returns the number of relations (cousin, uncles etc.)
	 * @return number of relations
	 */
	public int getNumberOfRelations() {
		return this.nRelations;
	}
	
	/**
	 * The method sets the number of children.
	 * @param n number of children
	 */
	public void setNumberOfChilds(int n) {
		this.nChilds=n;
	}
	
	/**
	 * The method sets the number of parents.
	 * @param n number of parents
	 */
	public void setNumberOfParents(int n) {
		this.nParents=n;
	}
	
	/**
	 * The method sets the number of siblings.
	 * @param n number of siblings
	 */
	public void setNumberOfSiblings(int n) {
		this.nSiblings=n;
	}
	
	/**
	 * The method sets the number of relations.
	 * @param n number of relations
	 */
	public void setNumberOfRelations(int n) {
		this.nRelations=n;
	}
	
	/**
	 * The method returns a vector containing the node statistics data. The resulting vector has length of 4
	 * and contains the number of parents [0], children [1], siblings [2], and relations [3].
	 * @return node statistics vector
	 */
	public int[] getValueVector() {
		int[] result = new int[4];
		
		result[0] = this.getNumberOfParents();
		result[1] = this.getNumberOfChilds();
		result[2] = this.getNumberOfSiblings();
		result[3] = this.getNumberOfRelations();
		
		return result;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof NodeStatistics)) return false;
		return ((NodeStatistics)o).getNode().equals(this.getNode());
	}
	
	/**
	 * The method merges the current data with the given node statistics 
	 * iff they manage data for the same node. In the merge process the 
	 * statistics data will be summarized. 
	 * @param stat
	 * @throws WrongSourceException
	 */
	public void merge(NodeStatistics stat) throws WrongSourceException {
		if (!this.getNode().equals(stat.getNode()))
			throw new WrongSourceException("The node statistics cannot be added because "+
					"the nodes for which the statistics is hold are different");
		
		this.setNumberOfChilds(this.getNumberOfChilds()+stat.getNumberOfChilds());
		this.setNumberOfParents(this.getNumberOfParents()+stat.getNumberOfParents());
		this.setNumberOfRelations(this.getNumberOfRelations()+stat.getNumberOfRelations());
		this.setNumberOfSiblings(this.getNumberOfSiblings()+stat.getNumberOfSiblings());
	}
}
