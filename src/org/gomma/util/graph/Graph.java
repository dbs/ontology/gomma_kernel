/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The interface defines some general methods 
 * handling nodes and edges within a graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface Graph<N extends Node, E extends Edge> {

	/**
	 * The method adds a node to the graph. The node will be identified by its identifier,
	 * i.e., an available node using the same identifier will be overwritten.
	 * @param n node to be added
	 */
	public void addNode(N n);
	
	/**
	 *The method returns true if the graph contains a node 
	 *using the specified node identifier; otherwise it returns false.
	 * @param nodeID node identifier
	 * @return true/false
	 */
	public boolean containsNode(int nodeID);
	/**
	 * The method returns the node object for the specified node identifier.
	 * @param nodeID node identifier
	 * @return node object for the specified node identifier
	 * @throws NoSuchElementException
	 */
	public N getNode(int nodeID) throws NoSuchElementException;
	
	/**
	 * The method adds a set of nodes to the graph.
	 * @param s set of nodes to be added
	 */
	public void addNodeSet(Set<N> s);
	
	/**
	 * The method returns set of all nodes available within the graph.
	 * @return set of nodes available within the graph
	 */
	public Collection<N> getNodeCollection();
	
	/**
	 * The method adds the given edge object to the graph and validates 
	 * the graph consistency.
	 * @param e edge to be added
	 * @throws GraphInitializationException
	 */
	public void addEdge(E e) throws GraphInitializationException;
	
	/**
	 * The method adds the set of edges to the graph.
	 * @param s set of edges that will be added to the graph
	 * @throws GraphInitializationException
	 */
	public void addEdgeSet(Set<E> s) throws GraphInitializationException;
	
	/**
	 * The method returns the collection of all edges available within the graph.
	 * @return collection of edges available within the graph  
	 */
	public Collection<E> getEdgeCollection();
	
	/**
	 * The method returns the number of nodes available within the graph.
	 * @return number of nodes of the graph
	 */
	public int getNodeSetSize();
	
	/**
	 * The method returns the number of edges available within the graph.
	 * @return number of edges of the graph
	 */
	public int getEdgeSetSize();
	
	/**
	 * The method returns the set of edges to which the specified 
	 * node object is associated (in- and out-going node).
	 * @param n node for which the associated edges will be retrieved
	 * @return set of edges
	 */
	public Collection<E> getEdgeCollection(N n);
	
	/**
	 * The method returns the set of edges of specified type
	 * to which the specified node object is associated (in- 
	 * or out-going node).
	 * @param n node for which the associated edges will be retrieved
	 * @param relType relationship type
	 * @return set of edges
	 */
	public Collection<E> getEdgeCollection(N n, String relType);
	
	/**
	 * The method returns true if node x and node y are directly connected by 
	 * an edge; otherwise it returns false
	 * @param x node x
	 * @param y node y
	 * @return true/false
	 */
	public boolean isDirectlyConnected(N x, N y);
	
	/**
	 * The method returns the edge connecting nodes x and y.
	 * @param x node x
	 * @param y node y
	 * @return edge connecting nodes x and y
	 * @throws NoSuchElementException
	 */
	public E getEdge(N x, N y) throws NoSuchElementException;
	/**
	 * The method returns true if the graph is directed, i.e., all edges
	 * are directed; otherwise it returns false.
	 * @return true/false
	 */
	public boolean isDirected();
	
	/**
	 * The method returns a string representing the edge list of the graph. 
	 * @return string representing the edge list of the graph
	 */
	public String toString();
	
//	/**
//	 * The method returns true if both graphs, the current and the given graph,
//	 * utilize the same set of nodes and edges; otherwise it returns false;
//	 * @param g
//	 * @return
//	 */
//	public boolean equals(Graph g);
}
