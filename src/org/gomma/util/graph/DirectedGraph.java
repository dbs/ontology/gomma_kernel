/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.util.Collection;

/**
 * The interface defines some general methods 
 * handling nodes and edges within a directed graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface DirectedGraph<N extends Node, E extends Edge> extends Graph<N,E> {
	
	/**
	 * The method returns the set of successor nodes of the specified node object.
	 * @param n node for which the successor nodes will be retrieved
	 * @return set of successor nodes
	 */
	public Collection<N> getSuccessorNodes(N n);
	
	/**
	 * The method returns the set of predecessor nodes of the specified node object.
	 * @param n node for which the predecessor nodes will be retrieved
	 * @return set of predecessor nodes
	 */
	public Collection<N> getPredecessorNodes(N n);
	
	/**
	 * The method returns the set of edges using the specified node as target.
	 * @param n target node of the returning edge set 
	 * @return set of in-going edges for the specified node
	 */
	public Collection<E> getIngoingEdges(N n);
	
	/**
	 * The method returns the set of edges using the specified node as target
	 * and are of the specified relationship type.
	 * @param n target node of the returning edge set
	 * @param relType relationship type  
	 * @return set of in-going edges for the specified node
	 */
	public Collection<E> getIngoingEdges(N n, String relType);
	
	/**
	 * The method returns the set of edges using the specified node as source.
	 * @param n source node of the returning edge set
	 * @return set of out-going edges for the specified node
	 */
	public Collection<E> getOutgoingEdges(N n);
	
	/**
	 * The method returns the set of edges using the specified node as source
	 * and are of the specified relationship type.
	 * @param n source node of the returning edge set
	 * @param relType relationship type
	 * @return set of out-going edges for the specified node
	 */
	public Collection<E> getOutgoingEdges(N n, String relType);
}
