/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

/**
 * The interface defines some general methods 
 * handling data of an edge.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface Edge {

	/**
	 * The method returns the edge identifier which should be unique for the graph.
	 * @return unique edge identifier
	 */
	public long getID();
	
	/**
	 * The method returns the unique object identifier of the 'from node'.
	 * @return unique identifier of the 'from node'
	 */
	public int getFromID();
	
	/**
	 * The method returns the unique object identifier of the 'to node'.
	 * @return unique identifier of the 'to node'
	 */
	public int getToID();
	
	/**
	 * The method returns the relationship type of the edge.
	 * @return relationship type of the edge
	 */
	public String getRelationshipType();
	
	/**
	 * The method sets the relationship type of the edge.
	 * @param type relationship type of the edge
	 */
	public void setRelationshipType(String type);
	
	/**
	 * The method returns a string representing the most important 
	 * edge data.
	 * @return string representing the most important edge data
	 */
	public String toString();
}
