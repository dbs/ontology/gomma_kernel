/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

/**
 * The class implements some general methods 
 * handling nodes and edges within a tree.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class TreeImpl<N extends Node, E extends Edge> extends DirectedAcyclicGraphImpl<N,E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6811275932270352914L;

	/**
	 * The constructor initializes the graph (=tree).
	 */
	public TreeImpl() {
		super();
	}
	
//	/**
//	 * The constructor initializes the graph class.
//	 * @param maxNumberOfNodes maximal number of nodes the graph should contain
//	 * @param maxNumberOfEdges maximal number of edges the graph should contain
//	 */
//	public TreeImpl(int maxNumberOfNodes, int maxNumberOfEdges) {
//		super(maxNumberOfNodes,maxNumberOfEdges);
//	}
	
	/**
	 * The method returns true if adding the given edge object 
	 * to the graph would lead to graph inconsistency; otherwise
	 * the method returns false.
	 * @param e edge for which the graph consistency will be checked
	 * @return true/false
	 */
	protected boolean createsCycle(Edge e) {
		return super.getIngoingEdges(super.getNode(e.getToID())).size()==1;
	}
}
