/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.exceptions.GraphInitializationException;

/**
 * The abstract class implements some general methods 
 * handling nodes and edges within a graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public abstract class AbstractGraph<N extends Node, E extends Edge> implements Graph<N,E>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4011600673851369599L;


	/**
	 * The item represents the set of nodes within the graph.
	 */
	protected List<N> nodeMap;
	
	/**
	 * The item represents the mapping between the node identifier and 
	 * the continuously increased identifiers used with in the adjacency 
	 * matrix. 
	 */
	protected Map<Integer,Integer> node2matrixID;
	
	/**
	 * The item represents the set of edges used to connect nodes.
	 */
	protected Map<Long,E> edgeMap;
	
	/**
	 * The item represents the adjacency matrix storing for each connected 
	 * node pair (i,j) i,j in set of nodes the corresponding edge identifier.
	 * The nodes are identified by the matrix-related node identifier which
	 * can be obtained by using the node-matrix-identifier mapping.
	 */
	protected AdjacencyMatrix matrix;
	
	/**
	 * The constructor initializes the graph class.
	 */
	protected AbstractGraph() {
		this.nodeMap = new ArrayList<N>(); 
		this.edgeMap = new HashMap<Long,E>(); 
		this.matrix  = new AdjacencyMatrix();
		
		this.node2matrixID = new HashMap<Integer,Integer>();  
	}
	
	/**
	 * The method adds the specified edge to the graph. The edge will be identified 
	 * by the given edge identifier, i.e., an available edge using the same edge identifier
	 * will overwritten. 
	 * @param e edge to be added
	 * @param isDirected true if the edge is directed; false if the edge is undirected
	 * @throws GraphInitializationException
	 */
	protected synchronized void addEdge(E e, boolean isDirected) throws GraphInitializationException {
		if (!this.containsNode(e.getFromID())) 
			throw new GraphInitializationException("The edge cannot be added since its 'from node = "+e.getFromID()+"' does not exists within the graph.");
		if (!this.containsNode(e.getToID())) 
			throw new GraphInitializationException("The edge cannot be added since its 'to node = "+e.getToID()+"' does not exists within the graph.");
		
		this.edgeMap.put(e.getID(),e);
		this.matrix.setValue(this.node2MatrixID(e.getFromID()),
				this.node2MatrixID(e.getToID()),e.getID());
		
		if (!isDirected)
			this.matrix.setValue(this.node2MatrixID(e.getToID()),
					this.node2MatrixID(e.getFromID()),e.getID());
	}

	/**
	 * @see Graph#addEdgeSet(Set)
	 */
	public void addEdgeSet(Set<E> s) throws GraphInitializationException {
		for (E e : s) this.addEdge(e);
	}

	/**
	 * @see Graph#addNode(Node)
	 */
	public synchronized void addNode(N n) {
		if (!this.containsNode(n.getID())) {
			this.matrix.setMatrixDimension(this.nodeMap.size()+1,this.nodeMap.size()+1);
			this.node2matrixID.put(n.getID(),this.nodeMap.size());
			this.nodeMap.add(n);
		}
	}
	
	/**
	 * @see Graph#containsNode(int)
	 */
	public boolean containsNode(int nodeID) {
		return this.node2matrixID.containsKey(nodeID);
	}

	/**
	 * @see Graph#getNode(int)
	 */
	public N getNode(int nodeID) throws NoSuchElementException {
		if (this.containsNode(nodeID)) return this.nodeMap.get(this.node2MatrixID(nodeID));
		throw new NoSuchElementException("Could not find any node using the specified identifier '"+nodeID+"'.");
	}
	
	/**
	 * @see Graph#addNodeSet(Set)
	 */
	public void addNodeSet(Set<N> s) {
		for (N n : s) this.addNode(n);		
	}

	/**
	 * @see Graph#getEdgeCollection()
	 */
	public Collection<E> getEdgeCollection() {
		Collection<E> edgeCollection = new HashSet<E>();
		for (E e : this.edgeMap.values())
			edgeCollection.add(e);
		return edgeCollection;
	}

	/**
	 * The method returns a collection of edges using the specified node 
	 * as target. 
	 * @param n node which is the target of all returned edges
	 * @return set of in-going edges
	 */
	protected Collection<E> getIngoingEdgeCollection(Node n) {
		Collection<E> edgeCollection = new ArrayList<E>();
		
		for (long edgeID : this.matrix.getColElementList(this.node2MatrixID(n.getID())))
			edgeCollection.add(this.edgeMap.get(edgeID));
		
		return edgeCollection;
	}
	
	/**
	 * The method returns a collection of edges using the specified node as target
	 * and are of the specified relationship type.
	 * @param n node which is the target of all returned edges
	 * @param relType relationship type
	 * @return set of in-going edges
	 */
	protected Collection<E> getIngoingEdgeCollection(N n, String relType) {
		Collection<E> edgeCollection = new ArrayList<E>();
		
		for (long edgeID : this.matrix.getColElementList(this.node2MatrixID(n.getID())))
			if (this.edgeMap.get(edgeID).getRelationshipType().equals(relType))
				edgeCollection.add(this.edgeMap.get(edgeID));
		
		return edgeCollection;
	}
	
	/**
	 * The method returns a collection of edges using the specified node 
	 * as source. 
	 * @param n node which is the source of all returned edges
	 * @return set of out-going edges
	 */
	protected Collection<E> getOutgoingEdgeCollection(N n) {
		Collection<E> edgeCollection = new ArrayList<E>();
		
		for (long edgeID : this.matrix.getRowElementList(this.node2MatrixID(n.getID())))
			edgeCollection.add(this.edgeMap.get(edgeID));
		
		return edgeCollection;
	}
	
	/**
	 * The method returns a collection of edges using the specified node 
	 * as source and are of the specified relationship type. 
	 * @param n node which is the source of all returned edges
	 * @param relType relationship type
	 * @return set of out-going edges
	 */
	protected Collection<E> getOutgoingEdgeCollection(N n,String relType) {
		Collection<E> edgeCollection = new ArrayList<E>();
		
		for (long edgeID : this.matrix.getRowElementList(this.node2MatrixID(n.getID())))
			if (this.edgeMap.get(edgeID).getRelationshipType().equals(relType))
				edgeCollection.add(this.edgeMap.get(edgeID));
		
		return edgeCollection;
	}

	/**
	 * @see Graph#getNodeCollection()
	 */
	public Collection<N> getNodeCollection() {
		return this.nodeMap;
	}

	/**
	 * @see Graph#getNodeSetSize()
	 */
	public int getNodeSetSize() {
		return this.nodeMap.size();
	}
	
	/**
	 * @see Graph#getEdgeSetSize()
	 */
	public int getEdgeSetSize() {
		return this.edgeMap.size();
	}
	
	/**
	 * @see Graph#isDirectlyConnected(Node, Node)
	 */
	public boolean isDirectlyConnected(N x, N y) {
		return this.matrix.getValue(
				this.node2MatrixID(x.getID()),
				this.node2MatrixID(y.getID()))!=AdjacencyMatrix.DEFAULT_VALUE;
	}
	
	/**
	 * @see Graph#getEdge(Node, Node)
	 */
	public E getEdge(N x, N y) throws NoSuchElementException {
		long edgeID = this.matrix.getValue(
				this.node2MatrixID(x.getID()),
				this.node2MatrixID(y.getID()));
		if (edgeID!=AdjacencyMatrix.DEFAULT_VALUE && this.edgeMap.containsKey(edgeID)) return this.edgeMap.get(edgeID);
		throw new NoSuchElementException("Could not find any edge connecting nodes '"+x.getID()+"' and '"+y.getID()+"'.");
	}
	
	public void compact() {
		
		this.matrix.compact();
	}
	
	/**
	 * @see Graph#toString()
	 */
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("nodes: {");
		for (N n : this.nodeMap) 
			buf.append(n.toString()).append(",");
		if (this.nodeMap.size()>0) 
			buf.deleteCharAt(buf.length()-1);
		buf.append("}");
		
		buf.append("\nedges: {");
		for (E e : this.getEdgeCollection())
			buf.append(e.toString()).append(",");
		if (this.edgeMap.size()>0)
			buf.deleteCharAt(buf.length()-1);
		buf.append("}");
		return buf.toString();
	}
	
	/**
	 * @see Graph#equals(Graph)
	 */
	public boolean equals(Graph<N,E> g) {
		
		if (this.getNodeSetSize()!=g.getNodeSetSize()) return false;
		if (this.getEdgeSetSize()!=g.getEdgeSetSize()) return false;
		
		for (N n : g.getNodeCollection())
			if (!this.containsNode(n.getID())) return false;
		
		for (E e : g.getEdgeCollection())
			if (!this.isDirectlyConnected(g.getNode(e.getFromID()),g.getNode(e.getToID()))) return false;
		
		return true;
	}
	
	private int node2MatrixID(int nodeID) {
		if (this.node2matrixID.containsKey(nodeID)) return this.node2matrixID.get(nodeID);
		throw new NoSuchElementException("Could not resolve the internal matrix identifier for the node identifier '"+nodeID+"'.");
	}
	
	
	
	
	private static class AdjacencyMatrix extends Object implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 8930245549267189144L;
		
		private static int DEFAULT_VALUE = 0;
		private static final int NO_MATRIX_ENTRY = -1;
		
		private int nRows;
		private int nCols;
		
		private List<MatrixElement> matrix;
		
		private Map<Integer,Set<Integer>> rowElementMap;
		private Map<Integer,Set<Integer>> colElementMap;
		
		public AdjacencyMatrix() {
			this.nRows = 0;
			this.nCols = 0;
			
			this.matrix = new ArrayList<MatrixElement>();
			this.rowElementMap = new HashMap<Integer,Set<Integer>>();
			this.colElementMap = new HashMap<Integer,Set<Integer>>();
		}
		
		public void setMatrixDimension(int nRows, int nCols) {
			this.nRows = nRows;
			this.nCols = nCols;
		}
		
		public int getNumberOfRows() {
			return this.nRows;
		}
		
		public int getNumberOfColumns() {
			return this.nCols;
		}
		
		public void setValue(int rowIdx, int colIdx, long value) throws IndexOutOfBoundsException {
			this.checkRowIdx(rowIdx);
			this.checkColIdx(colIdx);
			
			MatrixElement matrixElement = new MatrixElement(rowIdx,colIdx,value);
			
			// check if there already exists a value at position (rowIdx,colIdx)
			int elementIdx = this.indexOf(rowIdx, colIdx); 
			
			if (elementIdx!=NO_MATRIX_ENTRY) {
				// reset the available value
				this.matrix.set(elementIdx, matrixElement);
			} else {	
				elementIdx = this.matrix.size();
				this.matrix.add(matrixElement);
				
				this.addRowElementIdx(rowIdx, elementIdx);
				this.addColElementIdx(colIdx, elementIdx);
			}
		}
		
		public long getValue(int rowIdx, int colIdx) throws IndexOutOfBoundsException {
			this.checkRowIdx(rowIdx);
			this.checkColIdx(colIdx);
			
			int elementIdx = this.indexOf(rowIdx, colIdx);
			if (elementIdx != NO_MATRIX_ENTRY) return this.matrix.get(elementIdx).getValue();
						
			return DEFAULT_VALUE;
		}
		
//		returns the complete row of values including default values
//		optimization in method getRowElementList()
//		
//		public long[] getRow(int rowIdx) throws IndexOutOfBoundsException {
//			this.checkRowIdx(rowIdx);
//			long[] result = new long[this.nRows];
//			
//			if (DEFAULT_VALUE != 0) 
//				for (int i=0;i<this.nRows;i++) 
//					result[i] = DEFAULT_VALUE;
//			
//			if (this.rowElementMap.containsKey(rowIdx)) {
//				MatrixElement e;
//				for (int elementIdx : this.rowElementMap.get(rowIdx)) {
//					e = this.matrix.get(elementIdx);
//					result[e.getColIdx()] = e.getValue();
//				}
//			}
//		
//			return result;
//		}
		
		public List<Long> getRowElementList(int rowIdx) {
			this.checkRowIdx(rowIdx);
			List<Long> result = new ArrayList<Long>();
			
			if (this.rowElementMap.containsKey(rowIdx)) 
				for (int elementIdx : this.rowElementMap.get(rowIdx)) 
					result.add(this.matrix.get(elementIdx).getValue());
				
			return result;
		}
		
//		returns the complete column of values including default values
//		optimization in method getColElementList()
//		
//		public long[] getColumn(int colIdx) throws IndexOutOfBoundsException {
//			this.checkColIdx(colIdx);
//			long[] result = new long[this.nCols];
//			
//			if (DEFAULT_VALUE != 0) 
//				for (int i=0;i<this.nCols;i++) 
//					result[i] = DEFAULT_VALUE;
//			
//			if (this.colElementMap.containsKey(colIdx)) {
//				MatrixElement e;
//				for (int elementIdx : this.colElementMap.get(colIdx)) {
//					e = this.matrix.get(elementIdx);
//					result[e.getRowIdx()] = e.getValue();
//				}
//			}
//		
//			return result;
//		}

		public List<Long> getColElementList(int colIdx) {
			this.checkColIdx(colIdx);
			List<Long> result = new ArrayList<Long>();
			
			if (this.colElementMap.containsKey(colIdx)) 
				for (int elementIdx : this.colElementMap.get(colIdx)) 
					result.add(this.matrix.get(elementIdx).getValue());
				
			return result;
		}
		
		public int hashCode() {
			int result = 0;
			for (MatrixElement e : this.matrix)
				result += e.hashCode();
			return result;
		}
		
		public String toString() {
			String result = "";
			for (int i=0;i<this.getNumberOfRows();i++) {
				for (int j=0;j<this.getNumberOfColumns();j++)
					result += this.getValue(i,j)+"\t";
				result += "\n";
			}
			return result;
		}

		public void compact() {
			
		}
		
		private int indexOf(int rowIdx, int colIdx) {
			int idx = NO_MATRIX_ENTRY;
			
			if (this.rowElementMap.containsKey(rowIdx) && this.colElementMap.containsKey(colIdx)) 
				for (int rowElementIdx : this.rowElementMap.get(rowIdx)) 
					for (int colElementIdx : this.colElementMap.get(colIdx)) 
						if (rowElementIdx == colElementIdx)
							return rowElementIdx;	
			
			return idx;
		}
		
		private void addRowElementIdx(int rowIdx, int elementIdx) {
			Set<Integer> set;
			
			if (this.rowElementMap.containsKey(rowIdx)) set = this.rowElementMap.get(rowIdx);
			else set = new HashSet<Integer>();
			
			set.add(elementIdx);
			this.rowElementMap.put(rowIdx, set);
		}
		
		private void addColElementIdx(int colIdx, int elementIdx) {
			Set<Integer> set;
			
			if (this.colElementMap.containsKey(colIdx)) set = this.colElementMap.get(colIdx);
			else set = new HashSet<Integer>();
			
			set.add(elementIdx);
			this.colElementMap.put(colIdx, set);
		}
		
		private void checkRowIdx(int rowIdx) throws IndexOutOfBoundsException {
			if (rowIdx<0 && rowIdx>this.nRows)
				throw new IndexOutOfBoundsException(rowIdx+" ("+this.nRows+")");
		}
		
		private void checkColIdx(int colIdx) throws IndexOutOfBoundsException {
			if (colIdx<0 && colIdx>this.nCols)
				throw new IndexOutOfBoundsException(colIdx+" ("+this.nCols+")");
		}
		

	}
	
	
	private static class MatrixElement implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -4220524554733819716L;
		
		private final int rowIdx;
		private final int colIdx;
		private final long value;
		
		public MatrixElement(int rowIdx, int colIdx, long value) {
			this.rowIdx = rowIdx;
			this.colIdx = colIdx;
			this.value  = value;
		}
		
		public int getRowIdx() {
			return this.rowIdx;
		}
		
		public int getColIdx() {
			return this.colIdx;
		}
		
		public long getValue() {
			return this.value;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof MatrixElement)) return false;
			MatrixElement e = (MatrixElement)o;
			return this.rowIdx == e.getRowIdx() && this.colIdx == e.getColIdx() && this.value == e.getValue();
		}
		
		public int hashCode() {
			int result = 17;
			result = 31 * result + this.rowIdx;
			result = 31 * result + this.colIdx;
			result = 31 * result + (int)(this.value ^ (this.value >>> 32));
			return result;
		}
		
		public String toString() {
			return "ROW="+this.rowIdx+"; COL="+this.colIdx+"; VALUE="+this.value;
		}
	}
}
