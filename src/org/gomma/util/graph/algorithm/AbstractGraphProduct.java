/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;

/**
 * The abstract class implements some general method for generating a graph product. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public abstract class AbstractGraphProduct<N extends Node,E extends Edge> implements GraphProduct<N,E> {

	/**
	 * The items represent the undirected graphs for which the graph product should be computed.
	 */
	protected Graph<N,E> g1, g2;
	
	/**
	 * The item represents the table mapping the node 
	 * identifiers of input graphs to the identifiers 
	 * of generated graph product.
	 */
	protected Map<Integer,Map<Integer,Integer>> nodeIdMapTable;
	
	/**
	 * The item represents the maximal node identifier
	 * used to identify nodes within the generated 
	 * graph product; the step width is 1 starting with 0.
	 */
	protected int maxNodeID;
	
	/**
	 * The constructor initializes the class by using the 
	 * given graphs G1=(V1,E1) and G2=(V2,E2). 
	 * @param g1 graph G1
	 * @param g2 graph G2
	 */
	public AbstractGraphProduct(Graph<N,E> g1, Graph<N,E> g2) {
		this.g1 = g1;
		this.g2 = g2;
		this.nodeIdMapTable = new Int2ObjectOpenHashMap<Map<Integer,Integer>>();
		this.maxNodeID=-1;
	}
	
	/**
	 * @see GraphProduct#getG1Node(int)
	 */
	public N getG1Node(int id) {
		return this.g1.getNode(this.decodeX(id));
	}
	
	/**
	 * @see GraphProduct#getG2Node(int)
	 */
	public N getG2Node(int id) {
		return this.g2.getNode(this.decodeY(id));
	}
	
	public int getNodeID(int g1NodeID, int g2NodeID) {
		return this.encodeXY(g1NodeID,g2NodeID);
	}
	
	protected int encodeXY(int x,int y) {
		Map<Integer,Integer> t;
		
		if (this.nodeIdMapTable.containsKey(x)) t = nodeIdMapTable.get(x);
		else t = new Int2IntOpenHashMap();
		
		if (t.containsKey(y)) return t.get(y);
		
		t.put(y,++this.maxNodeID);
		this.nodeIdMapTable.put(x,t);
		
		return this.maxNodeID;
	}
	
	protected int decodeX(int z) throws NoSuchElementException {
		for (int x : this.nodeIdMapTable.keySet()) 
			for (int y : this.nodeIdMapTable.get(x).keySet())
				if (this.nodeIdMapTable.get(x).get(y)==z) return x;
		throw new NoSuchElementException("Could not find the node id.");
	}
	
	protected int decodeY(int z) throws NoSuchElementException {
		for (int x : this.nodeIdMapTable.keySet()) 
			for (int y : this.nodeIdMapTable.get(x).keySet())
				if (this.nodeIdMapTable.get(x).get(y)==z) return y;
		throw new NoSuchElementException("Could not find the node id.");
	}
}
