/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;

/**
 * The class implements the Similarity Flooding algorithm 
 * invented by Sergey Melnik. For more information see
 * 
 * S. Melnik, H. Garcia-Molina, E. Rahm: 
 * Similarity Flooding: A versatile graph matching algorithm 
 * and its application to schema matching. 
 * Proceedings 18th International Conference of Data Engineering, 2002. 
 * 
 * S. Melnik: Generic Model Management. Springer, LNCS 2967, 2004.
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SimilarityFlooding<N extends Node,E extends Edge> extends RandomSurferModel<N,E> {

	private static final int MAX_EDGE_SET_SIZE = 15000;
	
	
	/**
	 * The enumeration represents the variants of the fix point formula to compute the similarities.
	 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
	 * @version 1.0
	 * @since JDK1.5
	 *
	 */
	public enum FixPointFormulaVariants {
		/**
		 * Basic variant: d(i+1) := normalize( d(i) + phi(d(i)) )
		 */
		BASIC,
		/**
		 * Variant A: d(i+1) := normalize( d(0) + phi(d(i)) ) 
		 */
		ZERO_PLUS_PHI_OF_I,
		/**
		 * Variant B: d(i+1) := normalize( phi( d(0) + d(i) ) )
		 */
		PHI_OF_ZERO_AND_I,
		/**
		 * Variant C: d(i+1) := normalize( d(0) + d(i) + phi( d(0) + d(i) ) )
		 */
		ZERO_PLUS_I_PLUS_PHI_OF_ZERO_AND_I
	}
	
	/**
	 * The item represents the set of propagation factors for each edge.
	 */
	protected Map<Long,Float> propagationFactorMap;
	
	private Map<Integer,List<E>> nodeEdgeSet;
	private Optimizations optimization;
	
	private enum Optimizations {
		NO_OPTIMIZATION,
		EDGE_SET_EXTRACTION
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param maxIterations maximal number of iterations
	 */
	public SimilarityFlooding(Graph<N,E> g, int maxIterations) {
		this(g,0F,maxIterations);
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param threshold convergence threshold 
	 */
	public SimilarityFlooding(Graph<N,E> g, float threshold) {
		this(g,threshold,Integer.MAX_VALUE);
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param threshold convergence threshold
	 * @param maxIterations maximal number of iterations
	 */
	public SimilarityFlooding(Graph<N,E> g, float threshold, int maxIterations) {
		super(g,threshold,maxIterations);
		this.init();
	}
	
	public float getPropagationFactor(E e) {
		if (this.propagationFactorMap.containsKey(e.getID()))
			return this.propagationFactorMap.get(e.getID());
		else return 1F;
	}
	
	public void setEdgePropagationFactor(E e, float propFactor) {
//		if (!this.propagationFactorMap.containsKey(e))
			this.propagationFactorMap.put(e.getID(),propFactor);
	}
	
	public void printPropagationFatcors() {
		for (E e : super.g.getEdgeCollection())
			System.out.println(e.toString()+": "+this.getPropagationFactor(e));
	}
	
	public void setEdgePropagationFactors() {
		float propFactor;
		if (super.g.isDirected()) {
			DirectedGraph<N,E> dg = (DirectedGraph<N,E>)this.g;
			for (E e : dg.getEdgeCollection()) {
				propFactor = (float) 1 / ((float)
						dg.getOutgoingEdges(dg.getNode(e.getFromID()),e.getRelationshipType()).size() * 
						dg.getIngoingEdges(dg.getNode(e.getToID()),e.getRelationshipType()).size());
				this.setEdgePropagationFactor(e,propFactor);
			}
		} else {
			for (E e : super.g.getEdgeCollection()) {
				propFactor = (float) 1 / ((float)
						super.g.getEdgeCollection(super.g.getNode(e.getFromID()),e.getRelationshipType()).size() * 
						super.g.getEdgeCollection(super.g.getNode(e.getToID()),e.getRelationshipType()).size());
				this.setEdgePropagationFactor(e,propFactor);
			}
		}
	}
	
	public void compute() {
		this.compute(FixPointFormulaVariants.BASIC);
	}
	
	public void compute(FixPointFormulaVariants variant) {
		if (!super.iterationMap.containsKey(INITIAL_ITERATION)) 
			super.setInitialNodeSimilarities(1F);
		if (this.propagationFactorMap.size()!=super.g.getEdgeSetSize())
			this.setEdgePropagationFactors();
		
		do {
			super.lastIterationNumber++;
			for (N node : super.g.getNodeCollection()) {
				super.addNodeIteration(node,super.lastIterationNumber,
						this.computeNodeIteration(node,super.lastIterationNumber,variant));
			}
			this.normalizeNodeIteration(super.lastIterationNumber,variant);
		} while (super.lastIterationNumber<super.maxIterations && 
				this.getDeltaOfResidualVectors(super.lastIterationNumber)>=super.threshold);
	}
	
	private float computeNodeIteration(N node, int iteration, FixPointFormulaVariants variant) {
		switch (variant) {
			case BASIC: return this.computeNodeIterationVariant_Phi_I(node,iteration);
			case ZERO_PLUS_PHI_OF_I: return this.computeNodeIterationVariant_Phi_I(node,iteration);
			case PHI_OF_ZERO_AND_I: return this.computeNodeIterationVariant_Phi_0_I(node,iteration);
			case ZERO_PLUS_I_PLUS_PHI_OF_ZERO_AND_I: this.computeNodeIterationVariant_Phi_0_I(node,iteration);
			default: return this.computeNodeIterationVariant_Phi_I(node,iteration);
		}
	}
	
	private float computeNodeIterationVariant_Phi_I(N node, int iteration) {
		float nodeSim=0F;
		NodeSimilarityIteration iter;
		int nodeID;
		
		if (!super.iterationMap.containsKey(iteration-1)) return nodeSim;
		else iter = super.iterationMap.get(iteration-1);
		
		nodeSim = iter.getNodeSimilarity(node);
		
		for (E e : this.getEdgeSet(node)) {
			if (e.getFromID()==node.getID()) nodeID=e.getToID();
			else nodeID=e.getFromID();
			
			nodeSim += iter.getNodeSimilarity(super.g.getNode(nodeID)) * this.getPropagationFactor(e);
		}
		return nodeSim;
	}
	
	private float computeNodeIterationVariant_Phi_0_I(N node, int iteration) {
		float nodeSim=0F;
		NodeSimilarityIteration iterI, iter0;
		int nodeID;
		
		if (!super.iterationMap.containsKey(INITIAL_ITERATION)) return nodeSim;
		else iter0 = super.iterationMap.get(INITIAL_ITERATION);
		
		if (!super.iterationMap.containsKey(iteration-1)) return nodeSim;
		else iterI = super.iterationMap.get(iteration-1);
		
		for (E e : this.getEdgeSet(node)) {
			if (e.getFromID()==node.getID()) nodeID=e.getToID();
			else nodeID=e.getFromID();
			
			nodeSim += (iter0.getNodeSimilarity(super.g.getNode(nodeID)) + 
					iterI.getNodeSimilarity(super.g.getNode(nodeID))) * this.getPropagationFactor(e);
		}
		
		return nodeSim;
	}
	
	private void normalizeNodeIteration(int iteration, FixPointFormulaVariants variant) {
		switch (variant) {
			case BASIC: super.normalizeNodeIterationByGreatestValue(iteration);
			case ZERO_PLUS_PHI_OF_I: this.normalizeNodeIterationVariant_0_Phi_I(iteration);
			case PHI_OF_ZERO_AND_I: super.normalizeNodeIterationByGreatestValue(iteration);
			case ZERO_PLUS_I_PLUS_PHI_OF_ZERO_AND_I: this.normalizeNodeIterationVariant_0_I_Phi_I(iteration);
			default: super.normalizeNodeIterationByGreatestValue(iteration);
		}
	}
	
	private void normalizeNodeIterationVariant_0_Phi_I(int iteration) {
		float sumSim, maxSim=0F;		
		NodeSimilarityIteration iter_0, iter_Phi_I;
		iter_0 = super.iterationMap.get(INITIAL_ITERATION);
		iter_Phi_I = super.iterationMap.get(iteration);
		
		for (N node : this.g.getNodeCollection()) {
			sumSim = iter_0.getNodeSimilarity(node) + iter_Phi_I.getNodeSimilarity(node);
			iter_Phi_I.setNodeSimilarity(node,sumSim);
			if (maxSim<sumSim) maxSim=sumSim;
		}
		for (N node : this.g.getNodeCollection()) {
			if (maxSim>0F)
				iter_Phi_I.setNodeSimilarity(node,(float)iter_Phi_I.getNodeSimilarity(node) / maxSim);
			else iter_Phi_I.setNodeSimilarity(node, 0F);
		}
	}
	
	private void normalizeNodeIterationVariant_0_I_Phi_I(int iteration) {
		float sumSim, maxSim=0F;		
		NodeSimilarityIteration iter_0, iter_I, iter_Phi_I;
		iter_0 = super.iterationMap.get(INITIAL_ITERATION);
		iter_I = super.iterationMap.get(iteration-1);
		iter_Phi_I = super.iterationMap.get(iteration);
		
		for (N node : this.g.getNodeCollection()) {
			sumSim = iter_0.getNodeSimilarity(node) + iter_I.getNodeSimilarity(node) + iter_Phi_I.getNodeSimilarity(node);
			iter_Phi_I.setNodeSimilarity(node,sumSim);
			if (maxSim<sumSim) maxSim=sumSim;
		}
		for (N node : this.g.getNodeCollection()) {
			if (maxSim>0F)
				iter_Phi_I.setNodeSimilarity(node,(float)iter_Phi_I.getNodeSimilarity(node) / maxSim);
			else iter_Phi_I.setNodeSimilarity(node, 0F);
		}
	}
	
	private void init() {
		// set the initial propagation factor per edge 
		this.propagationFactorMap=new HashMap<Long,Float>();
		
		// set the optimization
		if (super.g.getEdgeSetSize()<MAX_EDGE_SET_SIZE) this.optimization=Optimizations.EDGE_SET_EXTRACTION;
		else this.optimization=Optimizations.NO_OPTIMIZATION;
		
		if (this.optimization==Optimizations.EDGE_SET_EXTRACTION) {
			this.nodeEdgeSet=new HashMap<Integer,List<E>>();
			List<E> edgeList;
			for (N node : super.g.getNodeCollection()) {
				if (this.nodeEdgeSet.containsKey(node.getID())) edgeList = this.nodeEdgeSet.get(node.getID());
				else edgeList = new ArrayList<E>();
				for (E e : super.g.getEdgeCollection(node)) edgeList.add(e);
				this.nodeEdgeSet.put(node.getID(),edgeList);
			}	
		} 
	}
	
	private Collection<E> getEdgeSet(N node) {
		switch (this.optimization) {
			case EDGE_SET_EXTRACTION: return this.nodeEdgeSet.get(node.getID());
			default: return super.g.getEdgeCollection(node);
		}
	}
}
