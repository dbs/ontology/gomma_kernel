/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.NodeSet;

/**
 * The class computes the maximal cliques of a given graph. The class 
 * has separate methods for handling undirected and directed graphs.
 * For more information about the maximal clique problem see, e.g.,
 *
 * D. Baum: Finding all maximal Cliques of a family of induced subgraphs.
 * ZIB-Report 03-53, Konrad-Zuse-Zentrum fuer Informationstechnik Berlin,
 * Dec. 2003.
 *
 * Gabriel Valiente: Algorithms on Trees and Graphs. Springer, 2002.
 * 
 * L. Babel, G.Tinhofer: A branch and bound algorithm for the maximum 
 * clique problem. Mathematical Methods of Operations Research, 
 * Physica Verlag (Springer), 34(3):207-217, 1990.
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class MaximalCliques<N extends Node,E extends Edge> {

	/**
	 * The item represents the given undirected graph 
	 * for which the maximal cliques should be computed.
	 */
	private Graph<N,E> g;
	
	/**
	 * The item holds all computed maximal cliques as 
	 * sets of nodes of the given graph.
	 */
	private List<NodeSet<N>> cliqueList;
	
	
	/**
	 * The constructor initializes the class by using 
	 * the given undirected graph G=(V,E).
	 * @param g undirected graph G
	 */
	public MaximalCliques(Graph<N,E> g) {
		this.g = g;
		this.cliqueList = new ArrayList<NodeSet<N>>();
	}
	
	
	/**
	 * The method returns the number of found maximal cliques.
	 * @return number of found maximal cliques.
	 */
	public int getCliqueNumber() {
		return this.cliqueList.size();
	}
	
	/**
	 * The method returns the number of cliques of specified size k.
	 * @param k size of clique
	 * @return number of cliques of specified size k
	 */
	public int getCliqueNumber(int k) {
		int i=0;
		for (NodeSet<N> c : this.cliqueList)
			if (c.size()==k) i++;
		return i;
	}
	
	/**
	 * The method returns the size of maximum cliques; if
	 * no maximal clique is found the method returns 0.
	 * @return size of maximum cliques
	 */
	public int getMaximumCliqueSize() {
		int i=0;
		for (NodeSet<N> c : this.cliqueList)
			if (i<c.size()) i=c.size();
		return i;
	}
	
	/**
	 * The method returns the maximum cliques as list of node sets.
	 * @return list of node sets forming maximum cliques
	 */
	public List<NodeSet<N>> getMaximumCliques() {
		return this.getMaximalCliques(this.getMaximumCliqueSize());
		
	}
	
	/**
	 * The method returns all maximal cliques of specified size k as 
	 * list of node sets.
	 * @param k size of maximal cliques to be retrieved
	 * @return list of nodes sets forming maximal cliques of specified size k.
	 */
	public List<NodeSet<N>> getMaximalCliques(int k) {
		List<NodeSet<N>> list = new ArrayList<NodeSet<N>>();
		for (NodeSet<N> c : this.cliqueList)
			if (c.size()==k) list.add(c);
		return list;
	}
	
	/**
	 * The method finds the maximal cliques of the given graph. The cliques
	 * are stored as node sets which can be obtained later on using separated
	 * methods. 
	 */
	public void computeMaximalCliques() {
		if (this.g.isDirected()) this.computeMaximalCliquesOfDirectedGraph(); 
		else this.computeMaximalCliquesOfUndirectedGraph();
	}
	
	/**
	 * The method finds the maximal cliques of the given undirected graph G=(V,E).
	 * The cliques are stored as node sets V'; each of them is a subset of V. To
	 * obtain the clique graph, the clique node sets V' can be used to induce the 
	 * clique graph G' as subgraph of G. 
	 */
	private void computeMaximalCliquesOfUndirectedGraph() {
		Collection<N> wColl, vColl;
		NodeSet<N> c1, c2;
		N v;
		boolean isInList;
		
		for (N n : this.g.getNodeCollection()) {
			wColl = this.getNodeCandidatesOfUndirectedGraph(n);
			
			c1 = new NodeSet<N>();
			c1.add(n);
			
			for (N w : wColl) {
				vColl = this.intersect(wColl,this.getNodeCandidatesOfUndirectedGraph(w));
				
				c2 = c1.clone();
				c2.add(w);
				
				while(!vColl.isEmpty()) {
					v = vColl.iterator().next();
					vColl.remove(v);
					c2.add(v);
					vColl = this.intersect(vColl,this.getNodeCandidatesOfUndirectedGraph(v));
				}
				
				isInList=false;
				for (NodeSet<N> t : this.cliqueList) 
					if (t.equals(c2) && c2.isSubSetOf(t)) isInList=true;
				if (!isInList && c2.size()>1) this.cliqueList.add(c2);
			}
		}
	}
	
	/**
	 * The method finds the maximal cliques of the given directed graph DG=(V,E).
	 * The cliques are stored as node sets V'; each of them is a subset of V. To
	 * obtain the clique graph, the clique node sets V' can be used to induce the 
	 * clique graph DG' as subgraph of DG. 
	 */	
	private void computeMaximalCliquesOfDirectedGraph() {
		Collection<N> wColl, vColl;
		NodeSet<N> c1, c2;
		N v;
		boolean isInList;
		
		DirectedGraph<N,E> dg = (DirectedGraph<N,E>)this.g;
		
		for (N n : dg.getNodeCollection()) {
			wColl = this.getNodeCandidatesOfDirectedGraph(dg,n);
			
			c1 = new NodeSet<N>();
			c1.add(n);
			
			for (N w : wColl) {
				vColl = this.intersect(wColl,this.getNodeCandidatesOfDirectedGraph(dg,w));
				
				c2 = c1.clone();
				c2.add(w);
				
				while(!vColl.isEmpty()) {
					v = vColl.iterator().next();
					vColl.remove(v);
					c2.add(v);
					vColl = this.intersect(vColl,this.getNodeCandidatesOfDirectedGraph(dg,v));
				}
				
				isInList=false;
				for (NodeSet<N> t : this.cliqueList) 
					if (t.equals(c2) && c2.isSubSetOf(t)) isInList=true;
				if (!isInList && c2.size()>1) this.cliqueList.add(c2);
			}
		}
	}
	
	
	/**
	 * The method returns a node collection that is the intersection of 
	 * two given node collections. The resulting node collection is empty
	 * if the two given node collections are disjunct.
	 * @param aColl node collection
	 * @param bColl node collection
	 * @return intersection of two given node collections
	 */
	private Collection<N> intersect(Collection<N> aColl, Collection<N> bColl) {
		Collection<N> cColl = new HashSet<N>();
		for (N n : aColl) 
			if (bColl.contains(n)) cColl.add(n);
		return cColl;
	}
	
	/**
	 * The method returns a node collection holding all nodes that are 
	 * adjacent with given node n in the undirected graph.
	 * @param n node for which the adjacent nodes should be collected
	 * @return node collection; each node is adjacent to the given node
	 */
	private Collection<N> getNodeCandidatesOfUndirectedGraph(N n) {
		Collection<N> candColl = new HashSet<N>();
		N t;
		
		
		for (E e : this.g.getEdgeCollection(n)) {
			t = this.g.getNode(e.getFromID());
			if (n.getID()!=t.getID() && !candColl.contains(t)) candColl.add(t);
			
			t = this.g.getNode(e.getToID());
			if (n.getID()!=t.getID() && !candColl.contains(t)) candColl.add(t);
		}
		return candColl;
	}
	
	/**
	 * The method returns a node collection holding all nodes that are 
	 * adjacent with the given node n in the directed graph DG=(V,E), i.e.,
	 * each node of the resulting collection is connected with the given 
	 * node n and n is connected with each node of the collection. More
	 * formally, for each v of V' as subset of V there is an edge e=(v,n)
	 * and an edge e'=(n,v) in the directed graph DG.  
	 * @param dg directed graph DG=(V,E)
	 * @param n node for which the adjacent node will be collected
	 * @return collection of nodes, all are adjacent with the given node n
	 */
	private Collection<N> getNodeCandidatesOfDirectedGraph(DirectedGraph<N,E> dg, N n) {
		Collection<N> candColl = new HashSet<N>();
		N t;
		
		for (E e : dg.getOutgoingEdges(n)) {
			t = dg.getNode(e.getToID());
			if (dg.isDirectlyConnected(t,n) && !candColl.contains(t)) candColl.add(t);
		}
		for (E e : dg.getIngoingEdges(n)) {
			t = dg.getNode(e.getFromID());
			if (dg.isDirectlyConnected(n,t) && !candColl.contains(t)) candColl.add(t);
		}
		
		return candColl;
	}

}
