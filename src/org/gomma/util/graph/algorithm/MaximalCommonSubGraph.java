/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import java.util.ArrayList;
import java.util.List;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.NodeSet;
import org.gomma.util.graph.algorithm.TensorComplementGraphProduct;

/**
 * The class computes the maximal common sub-graphs of two given graphs. 
 * The class has separate methods for handling undirected and directed 
 * graphs. For information about maximal common sub-graph problem
 * see e.g.
 * 
 * Gabriel Valiente: Algorithms on Trees and Graphs. Springer, 2002.
 * 
 * J.W. Raymond, P. Willet: Maximal common subgraph isomorphism
 * algorithms for the matching of chemical structures. Journal of
 * Computer-Aided Molecular Design, 16: 521-533, 2002
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class MaximalCommonSubGraph<N extends Node,E extends Edge> {

	/**
	 * The items represents the given undirected graphs G1=(V1,E1) 
	 * and G2=(V2,E2) and also the generated undirected graph product
	 * G3=(V3,E3).  
	 */
	private Graph<N,E> g1, g2;
	private Graph<Node,Edge> g3;
	
	/**
	 * The item represents the list of node sets representing the 
	 * maximum cliques of G3.
	 */
	private List<NodeSet<Node>> nodeSetList;
	
	/**
	 * The item represents the class to generate the graph 
	 * product of G1 and G2.
	 */
	private GraphProduct<N,E> graphProduct;
	
	/**
	 * The constructor initializes the class by using the 
	 * undirected graph G1 and G2 for which the maximal
	 * common sub-graphs should be computed.
	 * @param g1 undirected graph G1
	 * @param g2 undirected graph G2
	 */
	public MaximalCommonSubGraph(Graph<N,E> g1, Graph<N,E> g2) {
		this.g1=g1;
		this.g2=g2;
		this.g3=null;
		
		this.nodeSetList=null;
		this.graphProduct=null;
	}
	
	
	/**
	 * The method finds the maximal common sub-graph that exists 
	 * between G1 and G2.
	 * @param considerEdgeLabels true if edge labels should be considered;
	 * otherwise false
	 * @throws GraphInitializationException
	 * @throws GraphOperationExecutionException
	 */
	public void computeMaximalCommonSubgraph(boolean considerEdgeLabels) throws GraphInitializationException, GraphOperationExecutionException {
		this.graphProduct = new TensorComplementGraphProduct<N,E>(this.g1,this.g2);
		this.g3 = this.graphProduct.getGraphProduct(considerEdgeLabels);
		
		MaximalCliques<Node,Edge> mc = new MaximalCliques<Node,Edge>(this.g3);
		mc.computeMaximalCliques();
		this.nodeSetList = mc.getMaximumCliques();
	}
	
	/**
	 * The method returns a list of undirected graphs of G1 representing the 
	 * maximal common sub-graphs which exist between G1 and G2.
	 * @return list of undirected sub-graphs of G1
	 * @throws GraphInitializationException
	 * @throws GraphOperationExecutionException
	 */
	public List<Graph<N,E>> getG1SubGraphList() throws GraphInitializationException, GraphOperationExecutionException {
		List<Graph<N,E>> graphList = new ArrayList<Graph<N,E>>(this.nodeSetList.size());
		Graph<N,E> g;
		NodeSet<N> g1NodeSet;
		boolean isInList;
		
		for (NodeSet<Node> s : this.nodeSetList) {
			g1NodeSet = new NodeSet<N>();
			
			for (Node n : s.getElementSet()) 
				g1NodeSet.add(this.graphProduct.getG1Node(n.getID()));
			g = InducedSubGraph.getInducedSubGraph(this.g1,g1NodeSet);
			
			isInList = false;
			for (Graph<N,E> t : graphList) if (t.equals(g)) isInList=true;
			if (!isInList) graphList.add(g);
		}
		
		return graphList; 
	}
	
	/**
	 * The method returns a list of undirected graphs of G2 representing the 
	 * maximal common sub-graphs which exist between G1 and G2.
	 * @return list of undirected sub-graphs of G2
	 * @throws GraphInitializationException
	 * @throws GraphOperationExecutionException
	 */
	public List<Graph<N,E>> getG2SubGraphList() throws GraphInitializationException, GraphOperationExecutionException {
		List<Graph<N,E>> graphList = new ArrayList<Graph<N,E>>(this.nodeSetList.size());
		Graph<N,E> g;
		NodeSet<N> g2NodeSet;
		boolean isInList;
		
		for (NodeSet<Node> s : this.nodeSetList) {
			g2NodeSet = new NodeSet<N>();
			
			for (Node n : s.getElementSet()) 
				g2NodeSet.add(this.graphProduct.getG2Node(n.getID()));
			g = InducedSubGraph.getInducedSubGraph(this.g2,g2NodeSet);
			
			isInList = false;
			for (Graph<N,E> t : graphList) if (t.equals(g)) isInList=true;
			if (!isInList) graphList.add(g);
		}
		
		return graphList;
	}
}
