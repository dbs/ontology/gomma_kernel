package org.gomma.util.graph.algorithm;

import java.util.HashSet;
import java.util.Set;

import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.NodeStatistics;

/**
 * The class implements a method for determining statistics 
 * of specified node of given graph.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class NodeStatistician<N extends Node,E extends Edge> {

	/**
	 * The item represents the graph for which the node statistics 
	 * can be computed.
	 */
	private final Graph<N,E> g;
	
	/**
	 * The constructor initializes the class.
	 * @param g graph for which the node statistics can be computed
	 */
	public NodeStatistician(Graph<N,E> g) {
		this.g=g;
	}
	
	/**
	 * The method returns the statistics for the specified node.
	 * @param node node for which the statistics will be computed
	 * @return node statistics
	 */
	public NodeStatistics getNodeStatistics(N node) {
		return this.g.isDirected()?this.getNodeStatisticsOfDirectedGraph(node):this.getNodeStatisticsOfUndirectedGraph(node);
	}
	
	/**
	 * The method returns the statistics for the specified node in a undirected graph.
	 * @param node node for which the statistics will be computed
	 * @return node statistics
	 */
	private NodeStatistics getNodeStatisticsOfUndirectedGraph(N node) {
		NodeStatistics stats = new NodeStatistics(node);
		// FIXME: Implementation needed
		return stats;
	}
	
	/**
	 * The method returns the statistics for the specified node in a directed graph.
	 * @param node node for which the statistics will be computed
	 * @return node statistics
	 */
	private NodeStatistics getNodeStatisticsOfDirectedGraph(N node) {
		NodeStatistics stats = new NodeStatistics(node);
		
		DirectedGraph<N,E> dg = (DirectedGraph<N,E>)this.g;
		Set<N> siblingSet = new HashSet<N>(), 
				  parentSiblingSet = new HashSet<N>(),
				  parentSiblingChildSet = new HashSet<N>(); 
		
		for (N parentNode : dg.getPredecessorNodes(node)) {
			for (N sibling : dg.getSuccessorNodes(parentNode))
				siblingSet.add(sibling);
			for (N grandParentNode : dg.getPredecessorNodes(parentNode))
				for (N parentSiblingNode : dg.getSuccessorNodes(grandParentNode))
				parentSiblingSet.add(parentSiblingNode);
			if (parentSiblingSet.size()>0) parentSiblingSet.remove(parentNode);
		}
		if (siblingSet.size()>0) siblingSet.remove(node);
		
		for (N parentSiblingNode : parentSiblingSet)
			for (N parentSiblingChildNode : dg.getSuccessorNodes(parentSiblingNode))
				parentSiblingChildSet.add(parentSiblingChildNode);
		
		stats.setNumberOfChilds(dg.getSuccessorNodes(node).size());
		stats.setNumberOfParents(dg.getPredecessorNodes(node).size());
		stats.setNumberOfSiblings(siblingSet.size());
		stats.setNumberOfRelations(parentSiblingSet.size()+parentSiblingChildSet.size());
		
		return stats;
	}
}
