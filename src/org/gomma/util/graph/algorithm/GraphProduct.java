/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;

/**
 * The interface defines some general methods 
 * generating and managing a graph product.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface GraphProduct<N extends Node, E extends Edge> {

	/**
	 * The method returns a graph representing the graph product of 
	 * already specified graphs G1 and G2.
	 * @param considerEdgeLabels true if the graph product should consider
	 * edge labels; otherwise false
	 * @return graph representing the graph product of G1 and G2
	 * @throws GraphInitializationException
	 * @throws GraphOperationExecutionException 
	 */
	public Graph<Node,Edge> getGraphProduct(boolean considerEdgeLabels) throws GraphInitializationException, GraphOperationExecutionException;
	
	/**
	 * The method returns the node of graph G1 which is associated 
	 * with a node (for which the unique identifier is specified) 
	 * of the graph product.
	 * @param id unique identifier of a node of the graph product
	 * @return node of graph G1 which associated with the node of the graph product
	 */
	public N getG1Node(int id);
	
	/**
	 * The method returns the node of graph G2 which is associated 
	 * with a node (for which the unique identifier is specified) 
	 * of the graph product.
	 * @param id unique identifier of a node of the graph product
	 * @return node of graph G2 which associated with the node of the graph product
	 */
	public N getG2Node(int id);
	
	/**
	 * The method returns the node identifier of graph product 
	 * representing the specified pair of G1 and G2 nodes. 
	 * @param g1NodeID G1 node identifier
	 * @param g2NodeID G2 node identifier
	 * @return node identifier of the graph product representing the pair of G1 and G2 nodes
	 */
	public int getNodeID(int g1NodeID, int g2NodeID);
}
