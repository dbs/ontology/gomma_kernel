package org.gomma.util.graph.algorithm;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.graph.NodeStatistics;

/**
 * The class implements methods for determining statistics of 
 * source version objects taking specified attributes into account.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class ObjStatistician {

	/**
	 * The item represents the element set and their relationships of a specific source version.
	 */
	private SourceVersionStructure structure;
	
	/**
	 * The constructor initializes the class.
	 * @param structure source version structure (graph G=(V,E))
	 */
	public ObjStatistician(SourceVersionStructure structure) {
		this.structure=structure;
	}
	
	/**
	 * The method returns the node statistics of the object o. The statistics are divided by 
	 * the available attribute values that exists for the specified attribute name.
	 * @param o object for which the node statistics will be computed
	 * @param attName attribute name for which the node statistics will be separated
	 * @return set of node (object) statistics separated by attribute values
	 * @throws WrongSourceException
	 */
	public Map<String,NodeStatistics> getObjStatistics(Obj o, String attName) throws WrongSourceException {
		Map<String,NodeStatistics> stats = new Object2ObjectOpenHashMap<String,NodeStatistics>();
		
		Set<Obj> siblingSet = new HashSet<Obj>(), 
					 parentSiblingSet = new HashSet<Obj>(),
					 parentSiblingChildSet = new HashSet<Obj>(),
					 parentSet = new HashSet<Obj>(),
					 childSet = new HashSet<Obj>(); 
		
		// get all children of object o
		for (Obj childObj : this.structure.getSuccessorObjects(o))
			childSet.add(childObj);
		
		// get all parents of object o
		for (Obj parentObj : this.structure.getPredecessorObjects(o)) {
			parentSet.add(parentObj);
			// get all siblings of object o
			for (Obj siblingObj : this.structure.getSuccessorObjects(parentObj))
				siblingSet.add(siblingObj);
			// get all grand parents of of object o
			for (Obj grandParentObj : this.structure.getPredecessorObjects(parentObj))
				// get all siblings of parents of object o, i.e., ants and uncles of object o
				for (Obj parentSiblingObj : this.structure.getSuccessorObjects(grandParentObj))
					parentSiblingSet.add(parentSiblingObj);
			// remove parents from ants and uncle set
			if (parentSiblingSet.size()>0) parentSiblingSet.remove(parentObj);
		}
		if (siblingSet.size()>0) siblingSet.remove(o);
		
		for (Obj parentSiblingObj : parentSiblingSet)
			for (Obj parentSiblingChildObj : this.structure.getSuccessorObjects(parentSiblingObj))
				parentSiblingChildSet.add(parentSiblingChildObj);
		
		stats = this.transform(o,stats,Metrics.PARENTS,this.getAttributeBasedStatistics(parentSet,attName));
		stats = this.transform(o,stats,Metrics.CHILDREN,this.getAttributeBasedStatistics(childSet,attName));
		stats = this.transform(o,stats,Metrics.RELATIONS,this.getAttributeBasedStatistics(parentSiblingSet,attName));
		stats = this.transform(o,stats,Metrics.RELATIONS,this.getAttributeBasedStatistics(parentSiblingChildSet,attName));
		stats = this.transform(o,stats,Metrics.SIBLINGS,this.getAttributeBasedStatistics(siblingSet,attName));
		
		return stats;
	}

	/**
	 * The method determines the occurrence of attribute values in the specified 
	 * set of objects. It only considers values which are associated to the attribute
	 * using the given attribute name. The method returns a map of attribute values
	 * and their associated occurrence number. If there is no attribute available 
	 * using the specified attribute name, then the resulting map is empty. Objects
	 * without the attribute using the given name are ignored. 
	 * @param set set of objects
	 * @param attName attribute name
	 * @return mapping between found attribute values and their occurrence in the object set
	 */
	private Map<String,Integer> getAttributeBasedStatistics(Set<Obj> set, String attName) {
		Map<String,Integer> stat = new Object2IntOpenHashMap<String>();
		AttributeSet attSet;
		String attValue;
		
		for (Obj o : set) {
			attValue="";
			SourceVersion v = o.getSourceVersionSet().getLatestSourceVersion();
			attSet = o.getAttributeValues(v.getID()).getAttributeSet();
			
			for (Attribute att : attSet.getAttributeSet(attName)) 
				for (AttributeValueVersion avv : o.getAttributeValues(v.getID(),att))
					attValue += avv.toString();
			
			if (attValue.length()==0) attValue="N/A";
			if (stat.containsKey(attValue)) stat.put(attValue,stat.get(attValue)+1);
			else stat.put(attValue,1);
		}
		
		return stat;
	}
	
	/**
	 * The method transforms the specified attribute value occurrence and adds them 
	 * to the given node statistics which returns at the end. 
	 * @param o object for which the statistics will be transformed
	 * @param statMap map of currently managed node statistics to that the transformed statistics data will be added 
	 * @param metric metric (e.g., parents ~, children, and siblings statistics)
	 * @param numMap attribute value mapping
	 * @return mapping between attribute values and node statistics
	 */
	private Map<String,NodeStatistics> transform(Obj o, Map<String,NodeStatistics> statMap, Metrics metric, Map<String,Integer> numMap) {
		NodeStatistics stat;
		for (String key : numMap.keySet()) {
			if (statMap.containsKey(key)) stat = statMap.get(key);
			else stat = new NodeStatistics(o);
			switch (metric) {
				case PARENTS: stat.setNumberOfParents(stat.getNumberOfParents()+numMap.get(key)); break;
				case CHILDREN: stat.setNumberOfChilds(stat.getNumberOfChilds()+numMap.get(key)); break;
				case RELATIONS: stat.setNumberOfRelations(stat.getNumberOfRelations()+numMap.get(key)); break;
				case SIBLINGS: stat.setNumberOfSiblings(stat.getNumberOfSiblings()+numMap.get(key)); break;
			}
			statMap.put(key,stat);
		}
		return statMap;
	}
	
	/**
	 * The enumeration defines the metrics for which statistics will be computed.
	 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
	 * @version 1.0
	 * @since JDK1.5
	 */
	private enum Metrics {
		PARENTS,
		CHILDREN,
		SIBLINGS,
		RELATIONS
	}
}
