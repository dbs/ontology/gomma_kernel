/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import it.unimi.dsi.fastutil.ints.Int2FloatOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import java.util.Map;

import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;

/**
 * The abstract class implements some general methods
 * for handling a random surfer model used by different 
 * algorithms, such as Similarity Flooding or Google's PageRank. 
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public abstract class RandomSurferModel<N extends Node,E extends Edge> {

	/**
	 * The constant defines the initial iteration number.
	 */
	protected static final int INITIAL_ITERATION = 0;
	
	/**
	 * The item represents the similarity propagation graph G=(V,E)
	 * used to compute the similarity flooding algorithm. 
	 */
	protected Graph<N,E> g;
	
	/**
	 * The item represents the set of node similarities for each iteration.
	 */
	protected Map<Integer,NodeSimilarityIteration> iterationMap;
	
	/**
	 * The item represents the number of the current iteration.
	 */
	protected int lastIterationNumber;
	
	/**
	 * The item represents the maximal number of iterations.
	 */
	protected int maxIterations;
	
	/**
	 * The item represents the convergence threshold.
	 */
	protected float threshold;
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param threshold convergence threshold
	 * @param maxIterations maximal number of iterations
	 */
	public RandomSurferModel(Graph<N,E> g, float threshold, int maxIterations) {
		this.g=g;
		this.iterationMap=new Int2ObjectOpenHashMap<NodeSimilarityIteration>();
		this.lastIterationNumber=0;
		this.maxIterations=maxIterations;
		this.threshold=threshold;
	}
	
	/**
	 * The method returns the number of the last iteration that have 
	 * been performed.
	 * @return number of last performed iteration
	 */
	public int getLastIterationNumber() {
		return this.lastIterationNumber;
	}

	/**
	 * The method returns the computed rank/similarity of the specified 
	 * node within the specified iteration number.
	 * @param nIteration number of iteration
	 * @param node node for which the similarity should be retrieved
	 * @return node rank/similarity
	 */
	public float getNodeSimilarity(int nIteration, N node) {
		if (!this.iterationMap.containsKey(nIteration)) return 0;
		return this.iterationMap.get(nIteration).getNodeSimilarity(node);
	}
	
	/**
	 * The method sets the initial node rank/similarity.
	 * @param node node for which the initial value should be set
	 * @param simValue rank/similarity
	 */
	public void setInitialNodeSimilarity(N node, float simValue) {
		this.addNodeIteration(node,INITIAL_ITERATION,simValue);
	}
	
	/**
	 * The method set the initial rank/similarity for all nodes
	 * @param simValue initial simValue that should be used for all nodes
	 */
	public void setInitialNodeSimilarities(float simValue) {
		for (N node : this.g.getNodeCollection()) 
			this.setInitialNodeSimilarity(node,simValue);
	}
	
	/**
	 * The method stores the rank/similarity for the specified node and the number of iteration.
	 * @param node node
	 * @param iteration number of iteration
	 * @param simValue rank/similarity
	 */
	protected void addNodeIteration(N node, int iteration, float simValue) {
		NodeSimilarityIteration iter;
		if (this.iterationMap.containsKey(iteration)) iter=this.iterationMap.get(iteration);
		else iter=new NodeSimilarityIteration(iteration);
		iter.setNodeSimilarity(node,simValue);
		this.iterationMap.put(iteration,iter);
	}
	
	/**
	 * The method returns the delta of two residual vectors, i.e., 
	 * the computed node rank/similarity of the specified iteration number i and and i-1.  
	 * @param iteration iteration number i
	 * @return delta of rank/similarity node vectors
	 */
	protected float getDeltaOfResidualVectors(int iteration) {
		float delta=0F;
		
		if (this.iterationMap.containsKey(iteration) && this.iterationMap.containsKey(iteration-1)) {
			for (N node : this.g.getNodeCollection())
				delta += Math.abs(this.iterationMap.get(iteration).getNodeSimilarity(node) -
						this.iterationMap.get(iteration-1).getNodeSimilarity(node));
		}
		return delta;
	}
	
	/**
	 * The method normalizes the values of a node iteration relative to the largest value.
	 * @param iteration cycle number
	 */
	protected void normalizeNodeIterationByGreatestValue(int iteration) {
		if (!this.iterationMap.containsKey(iteration)) return;
		
		float maxSim=0F;
		NodeSimilarityIteration iter = this.iterationMap.get(iteration);
		
		for (N node : this.g.getNodeCollection())
			if (maxSim<iter.getNodeSimilarity(node)) maxSim=iter.getNodeSimilarity(node);
		
		for (N node : this.g.getNodeCollection()) {
			if (maxSim>0F) 
				iter.setNodeSimilarity(node,(float)iter.getNodeSimilarity(node) / maxSim);
			else iter.setNodeSimilarity(node, 0F);
		}
	}
	
	/**
	 * The class implements methods for managing resulting data of a fix point iteration.
	 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
	 * @version 1.0
	 * @since JDK1.5
	 */
	protected class NodeSimilarityIteration {
		private int iteration;
		private Map<Integer,Float> nodeSimilarity;
		
		public NodeSimilarityIteration(int iteration) {
			this.iteration=iteration;
			this.nodeSimilarity=new Int2FloatOpenHashMap();
		}
		
		public int getItertation() {
			return this.iteration;
		}
		
		public float getNodeSimilarity(N node) {
			if (this.nodeSimilarity.containsKey(node.getID())) 
				return this.nodeSimilarity.get(node.getID());
			return 0F;
		}

		public void setNodeSimilarity(N node, float sim) {
			this.nodeSimilarity.put(node.getID(),sim);
		}
	}
	
}
