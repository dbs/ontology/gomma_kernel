/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.Node;

/**
 * The class implements Google's PageRank algorithm
 * for computing page rank values. For more information see
 * 
 * L. Page, S. Brin: 
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class PageRank<N extends Node,E extends Edge> extends RandomSurferModel<N,E> {

	/**
	 * The constant represents the dumping factor d, that should be set 0 < d < 1
	 */
	private static final float DUMPING_FACTOR = 0.85F;
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param maxIterations maximal number of iterations
	 */
	public PageRank(Graph<N,E> g, int maxIterations) {
		super(g,0F,maxIterations);
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param threshold convergence threshold 
	 */
	public PageRank(Graph<N,E> g, float threshold) {
		super(g,threshold,Integer.MAX_VALUE);
	}
	
	/**
	 * The constructor initializes the class using the specified parameters.
	 * @param g graph
	 * @param threshold convergence threshold
	 * @param maxIterations maximal number of iterations
	 */
	public PageRank(Graph<N,E> g, float threshold, int maxIterations) {
		super(g,threshold,maxIterations);
	}
	
	/**
	 * The method computes the page rank.
	 * @param normalize true iff the resulting rank values after each iteration should be normalized; false otherwise 
	 */
	public void compute(boolean normalize) {
		if (!super.iterationMap.containsKey(INITIAL_ITERATION)) 
			super.setInitialNodeSimilarities(1F-DUMPING_FACTOR);
		
		do {
			super.lastIterationNumber++;
			for (N node : super.g.getNodeCollection()) {
				super.addNodeIteration(node,super.lastIterationNumber,
						this.computeNodeIteration(node,super.lastIterationNumber));
			}
			if (normalize) super.normalizeNodeIterationByGreatestValue(super.lastIterationNumber);
		} while (super.lastIterationNumber<super.maxIterations && 
				this.getDeltaOfResidualVectors(super.lastIterationNumber)>=super.threshold);
	}
	
	/**
	 * The method computes and returns the page rank of the specified node in the given iteration.   
	 * @param node node for which the page rank should be computed
	 * @param iteration iteration (cycle number)
	 * @return computed page rank
	 */
	private float computeNodeIteration(N node, int iteration) {
		float sum=0F;
		NodeSimilarityIteration iter;
		
		if (!super.iterationMap.containsKey(iteration-1)) return 1F-DUMPING_FACTOR;
		iter = super.iterationMap.get(iteration-1);
		
		if (super.g.isDirected()) {
			DirectedGraph<N,E> dg = (DirectedGraph<N,E>)super.g;
			for (N predNode : dg.getPredecessorNodes(node))
				sum += (float) iter.getNodeSimilarity(predNode) / (float) dg.getOutgoingEdges(predNode).size();
		}
		return (1F - DUMPING_FACTOR) + DUMPING_FACTOR * sum;
	}
}
