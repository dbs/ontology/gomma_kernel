/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import java.util.HashMap;
import java.util.Map;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.util.graph.DirectedAcyclicGraphImpl;
import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.DirectedGraphImpl;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.GraphImpl;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.TreeImpl;

/**
 * The class computes the greatest connected sub-graph of a given 
 * graph G=V,E) and a specified node n which is an element of V.
 * The class has separate methods for handling undirected and 
 * directed graphs. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class GreatestConnectedSubGraph {
	
	/**
	 * The method returns the greatest connected sub graph of the current graph
	 * containing the specified node.
	 * @param g graph
	 * @param n node that is an element of the sub graph
	 * @return greatest connected sub graph
	 * @throws GraphInitializationException 
	 */
	public static <N extends Node,E extends Edge> Graph<N,E> getGreatestConnectedSubGraph(Graph<N,E> g, N n) throws GraphInitializationException {
		Graph<N,E> subGraph = new GraphImpl<N,E>();
		
		Map<Long,E> edgeMap = getConnectedEdges(n,g,new HashMap<Long,E>());
		
		for (E e : edgeMap.values()) {
			subGraph.addNode(g.getNode(e.getFromID()));
			subGraph.addNode(g.getNode(e.getToID()));
			subGraph.addEdge(e);
		}
		
		return subGraph;
	}
	
	/**
	 * The method returns the greatest connected sub graph of the current graph
	 * containing the specified node.
	 * @param g directed graph
	 * @param n node that is an element of the sub graph
	 * @return greatest connected sub graph
	 * @throws GraphInitializationException 
	 */
	public static <N extends Node,E extends Edge> DirectedGraph<N,E> getGreatestConnectedSubGraph(DirectedGraph<N,E> g, N n) throws GraphInitializationException {
		DirectedGraph<N,E> subGraph;
		
		if (g instanceof TreeImpl<?,?>) subGraph = new TreeImpl<N,E>(); 
		else if (g instanceof DirectedAcyclicGraphImpl<?,?>) subGraph = new DirectedAcyclicGraphImpl<N,E>(); 
		else subGraph = new DirectedGraphImpl<N,E>();
		
		Map<Long,E> edgeMap = getPredecessorEdges(n,g,new HashMap<Long,E>());
		edgeMap = getSuccessorEdges(n,g,edgeMap);
		
		for (E e : edgeMap.values()) {
			subGraph.addNode(g.getNode(e.getFromID()));
			subGraph.addNode(g.getNode(e.getToID()));
			subGraph.addEdge(e);
		}
		
		return subGraph;
	}
	
	
	private static <N extends Node,E extends Edge> Map<Long,E> getConnectedEdges(N n, Graph<N,E> g, Map<Long,E> edgeMap) {
		N nextNode;
		for (E e : g.getEdgeCollection(n)) {
			if (edgeMap.containsKey(e.getID())) continue;
			edgeMap.put(e.getID(),e);
			
			nextNode = g.getNode(e.getFromID());
			edgeMap = getConnectedEdges(nextNode,g,edgeMap);
			
			nextNode = g.getNode(e.getToID());
			edgeMap  = getConnectedEdges(nextNode,g,edgeMap);
		}
		return edgeMap;
	}
	
	private static <N extends Node,E extends Edge> Map<Long,E> getPredecessorEdges(N n, DirectedGraph<N,E> g, Map<Long,E> edgeMap) {
		N predNode;
		for (E e : g.getIngoingEdges(n)) {
			if (edgeMap.containsKey(e.getID())) continue;
			edgeMap.put(e.getID(),e);
			
			predNode = g.getNode(e.getFromID());
			
			edgeMap = getPredecessorEdges(predNode,g,edgeMap);
			edgeMap = getSuccessorEdges(predNode,g,edgeMap);
		}
		return edgeMap;
	}
	
	private static <N extends Node,E extends Edge> Map<Long,E> getSuccessorEdges(N n, DirectedGraph<N,E> g, Map<Long,E> edgeMap) {
		N succNode;
		for (E e : g.getOutgoingEdges(n)) {
			if (edgeMap.containsKey(e.getID())) continue;
			edgeMap.put(e.getID(),e);
			
			succNode = g.getNode(e.getToID());
			
			edgeMap = getSuccessorEdges(succNode,g,edgeMap);
			edgeMap = getPredecessorEdges(succNode,g,edgeMap);
		}
		return edgeMap;
	}
}
