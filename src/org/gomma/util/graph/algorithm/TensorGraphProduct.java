/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/08
 * 
 * changes: --
 * 
 **/
package org.gomma.util.graph.algorithm;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.GraphOperationExecutionException;
import org.gomma.util.graph.DirectedAcyclicGraphImpl;
import org.gomma.util.graph.DirectedGraph;
import org.gomma.util.graph.DirectedGraphImpl;
import org.gomma.util.graph.Edge;
import org.gomma.util.graph.EdgeImpl;
import org.gomma.util.graph.Graph;
import org.gomma.util.graph.GraphImpl;
import org.gomma.util.graph.Node;
import org.gomma.util.graph.NodeImpl;
import org.gomma.util.graph.TreeImpl;

/**
 * The class computes the tensor graph product of two given graphs. The class
 * has separate methods for generating the tensor graph product of undirected 
 * and directed graphs. The tensor graph product G_p of two graph G1=(V1,E1)
 * and G2=(V2,E2) is defined as follows
 * 
 * G_p(G1,G2) = (V_p,E_p)
 * 
 * whereby 
 * V_p = union(V1,V2)
 * E_p = {((x,y),(x',y')) |
 *   x,x' in V1 and y,y' in V2 and
 *   (x,x') in E1 and (y,y') in E2
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class TensorGraphProduct<N extends Node,E extends Edge> extends AbstractGraphProduct<N,E> {

	/**
	 * The constructor initializes the class by using the 
	 * given graphs G1=(V1,E1) and G2=(V2,E2). 
	 * @param g1 graph G1
	 * @param g2 graph G2
	 */
	public TensorGraphProduct(Graph<N,E> g1, Graph<N,E> g2) {
		super(g1,g2);
	}
	
	/**
	 * @see GraphProduct#getGraphProduct(boolean)
	 */
	public Graph<Node,Edge> getGraphProduct(boolean considerEdgeLabels) throws GraphInitializationException, GraphOperationExecutionException {
		return this.g1.isDirected()&&this.g2.isDirected()?getDirectedGraphProduct(considerEdgeLabels):getUndirectedGraphProduct(considerEdgeLabels);
	}
	
	/**
	 * The method returns an undirected graph representing the graph product 
	 * of specified undirected graphs G1 and G2. 
	 * @return undirected graph which is the graph product of G1 and G2
	 * @throws GraphOperationExecutionException
	 * @throws GraphInitializationException
	 */
	private Graph<Node,Edge> getUndirectedGraphProduct(boolean considerEdgeLabels) throws GraphOperationExecutionException, GraphInitializationException {
		Graph<Node,Edge> graphProduct = new GraphImpl<Node,Edge>();
		Edge gEdge;
		
//		for (Node g1Node : this.g1.getNodeCollection())
//			for (Node g2Node : this.g2.getNodeCollection()) 
//				graphProduct.addNode(new NodeImpl(this.encodeXY(g1Node.getID(),g2Node.getID())));
		
		for (E eG1 : g1.getEdgeCollection()) {
			if (eG1.getFromID()==eG1.getToID()) continue;
			for (Edge eG2 : g2.getEdgeCollection()) {
				if (eG2.getFromID()==eG2.getToID()) continue;
				
				gEdge = new EdgeImpl(super.encodeXY(eG1.getFromID(),eG2.getFromID()),
						super.encodeXY(eG1.getToID(),eG2.getToID()));
				
				if (considerEdgeLabels) {
					if (eG1.getRelationshipType().equals(eG2.getRelationshipType()))
						gEdge.setRelationshipType(eG1.getRelationshipType());
					else continue;
				}
				
				if (!graphProduct.containsNode(gEdge.getFromID()))
					graphProduct.addNode(new NodeImpl(gEdge.getFromID()));
				
				if (!graphProduct.containsNode(gEdge.getToID()))
					graphProduct.addNode(new NodeImpl(gEdge.getToID()));
				
				graphProduct.addEdge(gEdge);
			}
		}
		
		return graphProduct;
	}
	
	/**
	 * The method returns a directed graph representing the graph product 
	 * of specified directed graphs G1 and G2. 
	 * @return directed graph which is the graph product of G1 and G2
	 * @throws GraphOperationExecutionException
	 * @throws GraphInitializationException
	 */
	private DirectedGraph<Node,Edge> getDirectedGraphProduct(boolean considerEdgeLabels) throws GraphInitializationException, GraphOperationExecutionException {
		DirectedGraph<Node,Edge> graphProduct;
		Edge gEdge;
		
		DirectedGraph<N,E> dg1 = (DirectedGraph<N,E>)this.g1;
		DirectedGraph<N,E> dg2 = (DirectedGraph<N,E>)this.g2;
		
//		int maxNumberOfNodes = dg1.getNodeSetSize()*dg2.getNodeSetSize();
//		int maxNumberOfEdges = (maxNumberOfNodes * maxNumberOfNodes - maxNumberOfNodes) / 2;
		
		if (dg1 instanceof TreeImpl<?,?> && dg2 instanceof TreeImpl<?,?>) 
			graphProduct = new TreeImpl<Node,Edge>();
		else if ((dg1 instanceof DirectedAcyclicGraphImpl<?,?> || dg2 instanceof DirectedGraphImpl<?,?>) &&
				!(dg1 instanceof DirectedGraphImpl<?,?> || dg2 instanceof DirectedGraphImpl<?,?>))
			graphProduct = new DirectedAcyclicGraphImpl<Node,Edge>();
		else graphProduct = new DirectedGraphImpl<Node,Edge>();
		
//		for (Node g1Node : dg1.getNodeCollection())
//			for (Node g2Node : dg2.getNodeCollection())
//				graphProduct.addNode(new NodeImpl(this.encodeXY(g1Node.getID(),g2Node.getID())));
		
		for (E eG1 : dg1.getEdgeCollection()) {
			if (eG1.getFromID()==eG1.getToID()) continue;
			
			for (E eG2 : dg2.getEdgeCollection()) {
				if (eG2.getFromID()==eG2.getToID()) continue;
				
				gEdge = new EdgeImpl(super.encodeXY(eG1.getFromID(),eG2.getFromID()),
						super.encodeXY(eG1.getToID(),eG2.getToID()));
				
				if (considerEdgeLabels) {
					if (eG1.getRelationshipType().equals(eG2.getRelationshipType()))
						gEdge.setRelationshipType(eG1.getRelationshipType());
					else continue;
				}
				
				if (!graphProduct.containsNode(gEdge.getFromID()))
					graphProduct.addNode(new NodeImpl(gEdge.getFromID()));
				
				if (!graphProduct.containsNode(gEdge.getToID()))
					graphProduct.addNode(new NodeImpl(gEdge.getToID()));
				
				graphProduct.addEdge(gEdge);
			}
		}
		
		return graphProduct;
	}
}
