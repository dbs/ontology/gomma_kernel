/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.clustering.CorrespondenceClusterTypes;
import org.gomma.exceptions.GommaException;
import org.gomma.filter.FilterTypes;
import org.gomma.io.FileTypes;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.IDObjectSet;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingStatisticsSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;
import org.gomma.rmi.Server;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.CorrespondenceClusterFunction;

/**
 * The class implements the GOMMA service interface including 
 * administration methods from the service interface. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class GommaServiceImpl implements GommaService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5810335802593301875L;
	
	/**
	 * The item represents the GOMMA application interface
	 */
	private Gomma gommaService;

	/**
	 * The constructor initializes the class.
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public GommaServiceImpl() throws GommaException, RemoteException {
		this.gommaService = new GommaImpl();
	}


	/**
	 * @see Server#initialize()
	 */
	public void initialize() throws GommaException, RemoteException {
		this.gommaService.initialize();
		
	}
	
	/**
	 * @see Server#initializeStandalone()
	 */
	public void initializeStandalone() throws GommaException, RemoteException {
		this.gommaService.initializeStandalone();
		
	}


	/**
	 * @see Server#initialize(String)
	 */
	public void initialize(String confFileNameLoc) throws GommaException, RemoteException {
		this.gommaService.initialize(confFileNameLoc);
	}


	/**
	 * @see Server#shutdown()
	 */
	public void shutdown() throws GommaException, RemoteException {
		this.gommaService.shutdown();
	}

	
	
	
	
	
	
	/**
	 * Source Management
	 */
	
	/**
	 * @see SourceManager#getSourceSet()
	 */
	public SourceSet getSourceSet() throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceSet();
	}
	
	/**
	 * @see SourceManager#getSourceSet(String)
	 */
	public SourceSet getSourceSet(String objType)  throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceSet(objType);
	}
	
	/**
	 * @see SourceManager#getSourceSet(String, String)
	 */
	public SourceSet getSourceSet(String objType, String physicalSource) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceSet(objType, physicalSource);
	}
	
	/**
	 * @see SourceManager#insertSource(String, String, String, boolean)
	 */
	public boolean insertSource(String objType, String physicalSource, String sourceURL, boolean isOntology) throws GommaException, RemoteException  {
		return this.gommaService.getSourceManager().insertSource(objType, physicalSource, sourceURL, isOntology);
	}
	
	/**
	 * @see SourceManager#updateSource(Source)
	 */
	public boolean updateSource(Source s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().updateSource(s);
	}
	
	/**
	 * @see SourceManager#deleteSource(Source)
	 */
	public boolean deleteSource(Source s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().deleteSource(s);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet()
	 */
	public SourceVersionSet getSourceVersionSet() throws GommaException,	RemoteException {
		return this.gommaService.getSourceManager().getSourceVersionSet();
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source)
	 */
	public SourceVersionSet getSourceVersionSet(Source s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceVersionSet(s);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, String)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceVersionSet(s, version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(String, String, String)
	 */
	public SourceVersionSet getSourceVersionSet(String objType, String physicalSource, String version) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceVersionSet(objType, physicalSource, version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceVersionSet(s, minDate, maxDate);
	}
	
	/**
	 * @see SourceManager#insertSourceVersion(String, String, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	public boolean insertSourceVersion(String objType, String physicalSource, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().insertSourceVersion(objType, physicalSource, version, creationDate, type);
	}
	
	/**
	 * @see SourceManager#updateSourceVersion(SourceVersion)
	 */
	public boolean updateSourceVersion(SourceVersion v) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().updateSourceVersion(v);
	}
	
	/**
	 * @see SourceManager#deleteSourceVersion(SourceVersion)
	 */
	public boolean deleteSourceVersion(SourceVersion sv) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().deleteSourceVersion(sv);
	}

	
	/**
	 * @see SourceManager#getSourceStructure(SourceVersion)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceStructure(s);
	}

	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion s, ObjSet objs) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceStructurePortion(s, objs);
	}

	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet, int, boolean)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion s, ObjSet objs, int nLevels, boolean getSuccessors) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSourceStructurePortion(s, objs, nLevels, getSuccessors);
	}
	
	

	/**
	 * @see SourceManager#importSource(String)
	 */
	public void importSource(String sourceDescriptionFile) throws GommaException, RemoteException {
		this.gommaService.getSourceManager().importSource(sourceDescriptionFile);
	}

	/**
	 * @see SourceManager#importSource(HashMap<String, String)
	 */
	public void importSource(HashMap<String, String> importDescriptionObj)
			throws GommaException, RemoteException {
		this.gommaService.getSourceManager().importSource(importDescriptionObj);		
	}
	/**
	 * @see SourceManager#exportSource(String, SourceVersionStructure, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersionStructure svs, FileTypes type) throws GommaException, RemoteException {
		this.gommaService.getSourceManager().exportSource(sourceFileName, svs, type);
	}

	/**
	 * @see SourceManager#exportSource(String, SourceVersion, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersion s, FileTypes type) throws GommaException, RemoteException {
		this.gommaService.getSourceManager().exportSource(sourceFileName, s, type);
	}
	
	
	
	/**
	 * @see SourceManager#getAttributeSet()
	 */
	public AttributeSet getAttributeSet() throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getAttributeSet();
	}
	


	/**
	 * @see SourceManager#getObjectSet(SourceVersion)
	 */
	public ObjSet getObjectSet(SourceVersion v) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v);
	}

	/**
	 * @see SourceManager#getObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v,attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion,String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v,attributeNames);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v,set);
	}

	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet,AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v,set,attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v,set, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumber, op);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumber, op, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumber, op, attributeNames);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List<String>)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumbers);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List<String>, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumbers, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List<String>, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, accNumbers, attributeNames);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, queryCondition);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, queryCondition, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(v, queryCondition, attributeNames);
	}
	
	/**
	 * @see SourceManager#getObjectSet(String, String, String, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(String objType, String physicalSource, String version, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getObjectSet(objType, physicalSource, version, accNumber, op);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion)
	 */
	public ObjSet getRootObjectSet(SourceVersion s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getRootObjectSet(s);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getRootObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getRootObjectSet(s);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, String[])
	 */
	public ObjSet getRootObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getRootObjectSet(s, attributeNames);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion)
	 */
	public ObjSet getLeafObjectSet(SourceVersion s) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getLeafObjectSet(s);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getLeafObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getLeafObjectSet(s, attSet);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion, String[])
	 */
	public ObjSet getLeafObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getLeafObjectSet(s, attributeNames);
	}
	
	/**
	 * @see SourceManager#union(ObjSet, ObjSet)
	 */
	public ObjSet union(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().union(o1, o2);
	}
	
	/**
	 * @see SourceManager#intersect(ObjSet, ObjSet)
	 */
	public ObjSet intersect(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().intersect(o1, o2);
	}

	/**
	 * @see SourceManager#diff(ObjSet, ObjSet)
	 */
	public ObjSet diff(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().diff(o1, o2);
	}
	
	/**
	 * @see SourceManager#majority(ObjSet[], int)
	 */
	public ObjSet majority(ObjSet[] o, int min) throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().majority(o, min);
	}
	
	
	
	
	/**
	 * Mapping Management
	 */
	

	/**
	 * @see MappingManager#getMappingSet()
	 */
	public MappingSet getMappingSet() throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingSet();
	}
	
	/**
	 * @see MappingManager#getMappingSet(String, QueryMatchOperations)
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingSet(name,op);
	}
	
	/**
	 * @see MappingManager#getMappingSet(Source, Source)
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingSet(domainSource, rangeSource);
	}
	
	/**
	 * @see MappingManager#insertMapping(String, Source, Source, boolean, boolean, String, String, String, String, AttributeValueSet)
	 */
	public boolean insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType, AttributeValueSet set) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().insertMapping(name, domainSource, rangeSource, isInstanceMapping, isDerived, 
				mapClass, mapMethod, mapTool, mapType, set);
	}
	
	/**
	 * @see MappingManager#updateMapping(Mapping)
	 */
	public boolean updateMapping(Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().updateMapping(m);
	}
	
	/**
	 * @see MappingManager#deleteMapping(Mapping)
	 */
	public boolean deleteMapping (Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().deleteMapping(m);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet()
	 */
	public MappingVersionSet getMappingVersionSet() throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingVersionSet();
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingVersionSet(m);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, String, QueryMatchOperations)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingVersionSet(m, name, op);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getMappingVersionSet(m, minDate, maxDate);
	}
	
	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, int, int, int, SourceVersionSet, SourceVersionSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate,
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs,
			SourceVersionSet domainSourceVersionSet, SourceVersionSet rangeSourceVersionSet)  throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().insertMappingVersion(m, name, creationDate, 
				lowestConfidence, lowestSupport, nCorrs, nDomainObjs, nRangeObjs, 
				domainSourceVersionSet, rangeSourceVersionSet);
	}

	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, ObjCorrespondenceSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate, float minConf, int minSupport, ObjCorrespondenceSet corrSet) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().insertMappingVersion(m, name, creationDate, minConf, minSupport, corrSet);
	}
	
	/**
	 * @see MappingManager#updateMappingVersion(MappingVersion)
	 */
	public boolean updateMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().updateMappingVersion(v);
	}
	
	/**
	 * @see MappingManager#deleteMappingVersion(MappingVersion)
	 */
	public boolean deleteMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().deleteMappingVersion(v);
	}

	
	
	/**
	 * @see MappingManager#importMapping(String)
	 */
	public void	importMapping(String mappingFile) throws GommaException, RemoteException {
		this.gommaService.getMappingManager().importMapping(mappingFile);
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, FileTypes type) throws GommaException, RemoteException {
		this.gommaService.getMappingManager().exportMappingVersion(mappingFileName, v, type);
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, ObjCorrespondenceSet, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, ObjCorrespondenceSet corrs, FileTypes type) throws GommaException, RemoteException {
		this.gommaService.getMappingManager().exportMappingVersion(mappingFileName, v, corrs, type);
	}
	
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getObjectCorrespondenceSet(v);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSetSimple(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getObjectCorrespondenceSetSimple(v);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion,int,boolean)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,
			int objId, boolean isDomain) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getObjectCorrespondenceSet(v, objId, isDomain);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion,String[])
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,String[] corrTypes) throws RemoteException, GommaException {
		return this.gommaService.getMappingManager().getObjectCorrespondenceSet(v, corrTypes);
	}
	/**
	 * @throws GommaException 
	 * @throws RemoteException 
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion,float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,float confThreshold) throws RemoteException, GommaException {
		return this.gommaService.getMappingManager().getObjectCorrespondenceSet(v,confThreshold);
	}
	/**
	 * @see MappingManager#getObjectCorrespondence(MappingVersion,String,String)
	 */
	public ObjCorrespondence getObjectCorrespondence(MappingVersion v,
			String dAcc, String rAcc) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().getObjectCorrespondence(v, dAcc, rAcc);
	}

	
	/**
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, ObjCorrespondenceSet)
	 */
	public boolean insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return this.gommaService.getMappingManager().insertObjectCorrespondenceSet(v, set);
	}
	
	
	
	
	/**
	 * M a p p i n g   O p e r a t i o n s
	 */

	
	/**
	 * @see MappingOperationManager#union(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion union(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().union(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#intersect(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion intersect(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().intersect(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#diff(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion diff(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().diff(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#majority(String, MappingVersionSet, int, ObjCorrespondenceSet)
	 */
	public MappingVersion majority(String newMappingVersionName, MappingVersionSet set, int nMin, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().majority(newMappingVersionName, set, nMin, resultObjCorrespondences);
	}

	

	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().union(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().union(o1, o2, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().union(sets, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().union(sets, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	/**
	 * @see MappingOperationManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().intersect(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().intersect(o1, o2, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingOperationManager#diff(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet diff(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().diff(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().majority(o, nMin);
	}
	
	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().majority(o, nMin, supportAggFunc, confidenceAggFunc);
	}

	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().majority(sets, nMin, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	
	/**
	 * @see MappingOperationManager#compose(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet compose(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().compose(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#compose(ObjCorrespondenceSet, ObjCorrespondenceSet, SourceVersionStructure, SourceVersionStructure)
	 */
	public ObjCorrespondenceSet hierarchicalCompose(ObjCorrespondenceSet domainMap, ObjCorrespondenceSet rangeMap,
													SourceVersionStructure rangeMapDomain,SourceVersionStructure rangeMapRange) 
													throws RemoteException,	GommaException {
		return this.gommaService.getMappingOperationManager().hierarchicalCompose(domainMap, rangeMap, rangeMapDomain, rangeMapRange);
	}

	
	/**
	 * @see MappingOperationManager#inverse(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet inverse (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().inverse(o);
	}
	
	/**
	 * @see MappingOperationManager#domain(ObjCorrespondenceSet)
	 */
	public ObjSet domain (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().domain(o);
	}
	
	/**
	 * @see MappingOperationManager#range(ObjCorrespondenceSet)
	 */
	public ObjSet range (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().range(o);
	}
		
	

	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(set, type, threshold);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(set, minOccurrence, minConf);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(MappingVersion, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, FilterTypes type, float threshold) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(v, type, threshold);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(MappingVersion, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(v, minOccurrence, minConf);
	}
	

	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold,boolean isDomain) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(set, type, threshold, isDomain);
	}

	@Override
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n)throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(set, type, n);
	}


	@Override
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n, boolean isDomain)throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().filterObjectCorrespondenceSet(set, type, n,isDomain);
	}
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().clusterObjectCorrespondenceSet(domain, range, set);
	}
	
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet, CorrespondenceClusterTypes, CorrespondenceClusterFunction)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set, CorrespondenceClusterTypes type, CorrespondenceClusterFunction func) throws GommaException, RemoteException {
		return this.gommaService.getMappingOperationManager().clusterObjectCorrespondenceSet(domain, range, set, type, func);
	}
	
	
	/**
	 * Source Evolution Management
	 */
	
	/**
	 * @see SourceEvolutionManager#addedObjects(ObjSet, ObjSet)
	 */
	public ObjSet addedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().addedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#deletedObjects(ObjSet, ObjSet)
	 */
	public ObjSet deletedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().deletedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#sameObjects(ObjSet, ObjSet)
	 */
	public ObjSet sameObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().sameObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#toObsoleteObjects(ObjSet, ObjSet)
	 */
	public ObjSet toObsoleteObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().toObsoleteObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#fusedObjects(ObjSet, ObjSet)
	 */
	public ObjSet fusedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().fusedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getObjectGrowth(ObjSet, ObjSet)
	 */
	public float getObjectGrowth(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().getObjectGrowth(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getAddedObjectFraction(ObjSet, ObjSet)
	 */
	public float getAddedObjectFraction(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().getAddedObjectFraction(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getDeletedObjectFraction(ObjSet, ObjSet)
	 */
	public float getDeletedObjectFraction(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().getDeletedObjectFraction(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getAddDeleteRatio(ObjSet, ObjSet)
	 */
	public float getAddDeleteRatio(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().getAddDeleteRatio(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getCoverage(ObjSet)
	 */
	public float getCoverage(ObjSet objs) throws GommaException,RemoteException {
		return this.gommaService.getSourceEvolutionManager().getCoverage(objs);
	}

	
	
	/**
	 * Evolution Statistics Management
	 */

	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStats(String, String, int, Calendar, Calendar)
	 */
	public Map<Integer, Integer> getAllEvolutionStats(String pdsName, String objectType, int action, Calendar start, Calendar end) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getAllEvolutionStats(pdsName, objectType, action, start, end);
	}

	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStats(SourceVersionSet, int)
	 */
    public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet set, int action) throws GommaException,RemoteException{
    	return this.gommaService.getEvolutionStatisticsManager().getAllEvolutionStats(set, action);
    }
	
	/**
	 * @see EvolutionStatisticsManager#getAverageEvolutionStats(String, String, int, int, Calendar, Calendar)
	 */
	public float getAverageEvolutionStats(String pdsName, String objectType, int action, int period, Calendar start, Calendar end) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getAverageEvolutionStats(pdsName, objectType, action, period, start, end);
	}


	/**
	 * @see EvolutionStatisticsManager#getLDSVersionStats(String, String, int)
	 */
	public Map<Integer, Integer> getLDSVersionStats(String pdsName,	String objectType, int statType) throws GommaException,	RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getLDSVersionStats(pdsName, objectType, statType);
	}


	/**
	 * @see EvolutionStatisticsManager#storeEvolutionStats(String, String)
	 */
	public void storeEvolutionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		this.gommaService.getEvolutionStatisticsManager().storeEvolutionStats(pdsName, objectType);
	}


	/**
	 * @see EvolutionStatisticsManager#storeLDSVersionStats(String, String)
	 */
	public void storeLDSVersionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		this.gommaService.getEvolutionStatisticsManager().storeLDSVersionStats(pdsName, objectType);
	}

	/**
	 * @see EvolutionStatisticsManager#getObsoleteMapping(List<String>)
	 */
	public Map<String, List<String>> getObsoleteMapping(List<String> accessions) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getObsoleteMapping(accessions);
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeSourceVersionEvolution()
	 */
	public boolean storeSourceVersionEvolution() throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().storeSourceVersionEvolution();
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeSourceVersionEvolution()
	 */
	public boolean storeMappingVersionEvolution() throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().storeMappingVersionEvolution();
	}
	
	/**
	 * @see EvolutionStatisticsManager#getSourceVersionEvolutionList(Source)
	 */
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getSourceVersionEvolutionList(s);
	}

	
	
	/**
	 *  S t a t i s t i c s   M a n a g e m e n t
	 */
	
	/**
	 * @see StatisticsManager#storeSourceStatistics()
	 */
	public boolean storeSourceStatistics() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().storeSourceStatistics();
	}
	
	/**
	 * @see StatisticsManager#getSourceStatisticsSet()
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getSourceStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#storeSourceVersionStatistics()
	 */
	public boolean storeSourceVersionStatistics() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().storeSourceVersionStatistics();
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet()
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getSourceVersionStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet(Source)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getSourceVersionStatisticsSet(s);
	}
	
	/**
	 * @see StatisticsManager#storeMappingStatistics()
	 */
	public boolean storeMappingStatistics() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().storeMappingStatistics();
	}
	
	/**
	 * @see StatisticsManager#getMappingStatisticsSet()
	 */
	public MappingStatisticsSet getMappingStatisticsSet() throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getMappingStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#getConfidenceFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getConfidenceFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getSupportFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getSupportFrequencies(v);
	}
	
	/**
	 * @see StatisticsManager#getDomainObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getDomainObjectFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getRangeObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getRangeObjectFrequencies(v);
	}
	
	
	
	
	/**
	 * Mapping Correspondence Evolution Management
	 */
	

	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet addedCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().addedCorrespondences(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet addedCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().addedCorrespondences(v1, v2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet deletedCorrespondences(ObjCorrespondenceSet corrs1,	ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().deletedCorrespondences(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet deletedCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().deletedCorrespondences(v1, v2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getAddedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getAddedCorrespondenceFraction(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getAddedCorrespondenceFraction(MappingVersion v1, MappingVersion v2)	throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getAddedCorrespondenceFraction(v1, v2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getCorrespondenceGrowth(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getCorrespondenceGrowth(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(MappingVersion, MappingVersion)
	 */
	public float getCorrespondenceGrowth(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getCorrespondenceGrowth(v1, v2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getDeletedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getDeletedCorrespondenceFraction(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getDeletedCorrespondenceFraction(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getDeletedCorrespondenceFraction(v1, v2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet sameCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().sameCorrespondences(corrs1, corrs2);
	}


	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet sameCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.gommaService.getMappingCorrespondenceEvolutionManager().sameCorrespondences(v1, v2);
	}
	
	
	
	
	/**
	 * Mapping Object Evolution Management
	 */
	
	/**
	 * @see MappingObjectEvolutionManager#addedObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet addedObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().addedObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#addedObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet addedObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().addedObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#deletedObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet deletedObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().deletedObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#deletedObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet deletedObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().deletedObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#sameObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet sameObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().sameObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#sameObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet sameObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().sameObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getObjectGrowth(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getObjectGrowth(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getObjectGrowth(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getObjectGrowth(MappingVersion, MappingVersion, boolean)
	 */
	public float getObjectGrowth(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getObjectGrowth(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddedObjectFraction(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getAddedObjectFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getAddedObjectFraction(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddedObjectFraction(MappingVersion, MappingVersion, boolean)
	 */
	public float getAddedObjectFraction(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getAddedObjectFraction(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getDeletedObjectFraction(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getDeletedObjectFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getDeletedObjectFraction(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getDeletedObjectFraction(MappingVersion, MappingVersion, boolean)
	 */
	public float getDeletedObjectFraction(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getDeletedObjectFraction(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddDeleteRatio(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getAddDeleteRatio(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getAddDeleteRatio(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddDeleteRatio(MappingVersion, MappingVersion, boolean)
	 */
	public float getAddDeleteRatio(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return this.gommaService.getMappingObjectEvolutionManager().getAddDeleteRatio(v1, v2, domain);
	}
	
	
	
	
	
	
	/**
	 * Match Manager
	 */
	
	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return this.gommaService.getMatchManager().match(s1, s2, simFunc, aggFunc, simThreshold, props);
	}
	 
	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return this.gommaService.getMatchManager().match(s1, s2, simFunc, aggFunc, simThreshold, props, initialMap);
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return this.gommaService.getMatchManager().match(domain, range, simFunc, aggFunc, simThreshold, props);
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return this.gommaService.getMatchManager().match(domain, range, simFunc, aggFunc, simThreshold, props, initialMap);
	}
	
	/**
	 * @see DistributedMatchManager#match(String,String,String,String,String,String,String,String,float)
	 */
	public ObjCorrespondenceSet match(String domainObjectType,
			String domainName, String rangeObjectType, String rangeName,
			String domainVersionNumber, String rangeVersionNumber,
			String domainConstraint, String rangeConstraint, 
			SimilarityFunction simFunction, AggregationFunction aggFunction, 
			float simThreshold, MatchProperties props)
			throws GommaException, RemoteException {
		return this.gommaService.getDistributedMatchManager().match(domainObjectType, domainName, rangeObjectType, rangeName, domainVersionNumber, rangeVersionNumber, domainConstraint, rangeConstraint, simFunction, aggFunction, simThreshold, props);
	}
	
	/**
	 * Work Manager
	 */
	
	/**
	 * @see WorkManager#getWorkPackageSet()
	 */
	public WorkPackageSet getWorkPackageSet() throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().getWorkPackageSet();
	}
	
	/**
	 * @see WorkManager#getWorkPackages(String)
	 */
	public WorkPackageSet getWorkPackageSet(String name) throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().getWorkPackageSet(name);
	}

	/**
	 * @see WorkManager#insertWorkPackage(String, String)
	 */
	public boolean insertWorkPackage(String name, String description) throws GommaException,RemoteException {
		return this.gommaService.getWorkManager().insertWorkPackage(name, description);
	}
	
	/**
	 * @see WorkManager#updateWorkPackage(WorkPackage)
	 */
	public boolean updateWorkPackage(WorkPackage pack) throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().updateWorkPackage(pack);
	}
	
	/**
	 * @see WorkManager#deleteWorkPackage(WorkPackage)
	 */
	public boolean deleteWorkPackage(WorkPackage pack) throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().deleteWorkPackage(pack);
	}
	
	/**
	 * @see WorkManager#insertMatchTask(String, WorkPackage, SourceVersion, SourceVersion, ObjSet, ObjSet, String)
	 */
	public boolean insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().insertMatchTask(name, pack, domainSV, rangeSV, domainObjs, rangeObjs, taskConf);
	}
	
	/**
	 * @see WorkManager#getNextMatchTask(WorkPackage, String)
	 */
	public MatchTask getNextMatchTask(WorkPackage pack, String uri) throws GommaException, RemoteException, NoSuchElementException {
		return this.gommaService.getWorkManager().getNextMatchTask(pack, uri);
	}
	
	/**
	 * @see WorkManager#updateMatchTaskStatus(MatchTask)
	 */
	public boolean updateMatchTaskStatus(MatchTask task) throws GommaException, RemoteException {
		return this.gommaService.getWorkManager().updateMatchTaskStatus(task);
	}
	
	/**
	 * @see WorkManager#deleteMatchTask(MatchTask)
	 */
	public boolean deleteMatchTask(MatchTask task) throws GommaException,RemoteException {
		return this.gommaService.getWorkManager().deleteMatchTask(task);
	}


	/**
	 * @see StabilityManager#deleteCorrespondenceStabs(String)
	 */
	public boolean deleteCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().deleteCorrespondenceStabs(analysisName);
	}


	/**
	 * @see StabilityManager#getAnalysisName(String)
	 */
	public List<String> getAnalysisName(String mapName) throws GommaException,
			RemoteException {
		return this.gommaService.getStabilityManager().getAnalysisName(mapName);
	}


	/**
	 * @see StabilityManager#getCorrespondenceStabs(String,SimplifiedGregorianCalendar,SimplifiedGregorianCalendar,int)
	 */
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName,SimplifiedGregorianCalendar versionBegin,SimplifiedGregorianCalendar versionEnd, int kMapVersions)
			throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().getCorrespondenceStabs(mapName, versionBegin, versionEnd, kMapVersions);
	}

	/**
	 * @see StabilityManager#getCorrespondenceStabs(String)
	 */
	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().getCorrespondenceStabs(analysisName);
	}
	
	/**
	 * @see StabilityManager#getStabilityAnalysisStatistics()
	 */
	public StabilityAnalysisSet getStabilityAnalysisStatistics()
			throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().getStabilityAnalysisStatistics();
	}

	/**
	 * @see StabilityManager#hasStabilityAnalysis(String)
	 */
	public boolean hasStabilityAnalysis(String analysisName) throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().hasStabilityAnalysis(analysisName);
	}


	/**
	 * @see StabilityManager#hasStabilityAnalysis(String,int,calendar,calendar)
	 */
	public boolean hasStabilityAnalysis(String mapName, int kMapVersions,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd) throws GommaException,
			RemoteException {
		return this.gommaService.getStabilityManager().hasStabilityAnalysis(mapName, kMapVersions, versionBegin, versionEnd);
	}


	/**
	 * @see StabilityManager#setCorrespondenceStabs(MappingVersionSet,String,Calendar,Calendar,int,String)
	 */
	public boolean setCorrespondenceStabs(MappingVersionSet set, String mapName,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions,
			String analysisName) throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().setCorrespondenceStabs(set, mapName, versionBegin, versionEnd, kMapVersions, analysisName);
		
	}


	/**
	 * @see StabilityManager#updateStabilityAnalysis(StabilityAnlysis,String)
	 */
	public boolean updateStabilityAnalysis(StabilityAnalysis sa) throws GommaException, RemoteException {
		return this.gommaService.getStabilityManager().updateStabilityAnalysis(sa);
	}

	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceEvolutionConfidence(CorrespondenceStability)
	 */
	public Map<SimplifiedGregorianCalendar, Float> getCorrespondenceEvolutionConfidence(CorrespondenceStability corrStab) throws GommaException, RemoteException {
		
		return this.gommaService.getMappingCorrespondenceEvolutionManager().getCorrespondenceEvolutionConfidence( corrStab);
	}


	/**
	 * @see SourceManager#searchObject(SourceVersion ,String)
	 */
	public ObjSet searchObject(SourceVersion sv, String keyword)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().searchObject(sv, keyword);
	}

	/**
	 * @see SourceManager#searchObject(SourceVersionSet ,String)
	 */
	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet, String keyword)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceManager().getSearchSourceVersionSet(svSet, keyword);
	}


	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStatsWithAvgData(SourceVersionSet, int, int)
	 */
	public Map<Map<Integer, Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet set,
			int action,int period) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getAllEvolutionStatsWithAvgData(set, action,period);
	}


	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet(SourceVersionSet)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(
			SourceVersionSet set) throws GommaException, RemoteException {
		return this.gommaService.getStatisticsManager().getSourceVersionStatisticsSet(set);
	}


	/**
	 * @see SourceEvolutionManager#addedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet addedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().addedObjects(v1, v2);
	}


	/**
	 * @see SourceEvolutionManager#deletedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet deletedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().deletedObjects(v1, v2);
	}


	/**
	 * @see SourceEvolutionManager#fusedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet fusedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().fusedObjects(v1, v2);
	}


	/**
	 * @see SourceEvolutionManager#toObsoleteObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet toObsoleteObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return this.gommaService.getSourceEvolutionManager().toObsoleteObjects(v1, v2);
	}


	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionList(Mapping)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getMappingVersionEvolutionList(m);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionListWithAvgData(Mapping,int)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionListWithAvgData(Mapping m,int period) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getMappingVersionEvolutionListWithAvgData(m,period);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet,int)
	 */
	public List<MappingVersionEvolution> getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet mvSet,int period) throws GommaException, RemoteException {
		return this.gommaService.getEvolutionStatisticsManager().getPartOfMappingVersionEvolutionListWithAvgData(mvSet,period);
	}

}
