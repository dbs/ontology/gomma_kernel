package org.gomma.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GommaLogFactory {

	private GommaLogFactory() {}
	
	public static void createRMIServerLoggers(RMIServerLogCongig config, String logDir) {
		if (config.logServerCalls) enableLogging("sun.rmi.server.call", logDir+"/server.calls.log");
		if (config.logServerRefs) enableLogging("sun.rmi.server.ref", logDir+"/server.refs.log");
		if (config.logGarbageCollection) enableLogging("sun.rmi.dgc", logDir+"/server.dgc.log");
		if (config.logServerLoad) enableLogging("sun.rmi.loader", logDir+"/server.loader.log");
		if (config.logTransportMisc) enableLogging("sun.rmi.transport.misc", logDir+"/server.misc.log");
		if (config.logTransportTCP) enableLogging("sun.rmi.transport.tcp", logDir+"/server.tcp.log");
		if (config.logTransportProxy) enableLogging("sun.rmi.transport.proxy", logDir+"/server.proxy.log");
	}
	
	public static void createRMIClientLoggers(RMIClientLogCongig config, String logDir) {
		if (config.logClientCalls) enableLogging("sun.rmi.client.call", logDir+"/client.calls.log");
        if (config.logClientRefs) enableLogging("sun.rmi.client.ref", logDir+"/client.refs.log");
        if (config.logGarbageCollection) enableLogging("sun.rmi.dgc", logDir+"/client.dgc.log");
        if (config.logClientLoad) enableLogging("sun.rmi.loader", logDir+"/client.loader.log");
        if (config.logTransportMisc) enableLogging("sun.rmi.transport.misc", logDir+"/client.misc.log");
        if (config.logTransportTCP) enableLogging("sun.rmi.transport.tcp", logDir+"/client.tcp.log");
        if (config.logTransportProxy) enableLogging("sun.rmi.transport.proxy", logDir+"/client.proxy.log");
	}
	
	public static void createGommaServerLogger(String logDir) {
		enableLogging("org.gomma",logDir+"/rmi.log");
	}
	
	private static void enableLogging(String loggername, String filename) {

		try {
			// default: ALL to SimpleFormatter
			String udir = System.getProperty("user.dir");
			FileHandler fh = new FileHandler(udir + "/" + filename,10000000,5,false);
			fh.setFormatter(new SimpleFormatter());

			Logger logger = Logger.getLogger(loggername);
			logger.setUseParentHandlers(false);
			logger.setLevel(Level.ALL);
			logger.addHandler(fh);

		} catch (IOException e) {
			System.out.println("logger exception");
		}
	}
	
	public static class RMIServerLogCongig {
		private boolean logServerCalls = false;
		private boolean logServerRefs = false;
		private boolean logGarbageCollection = false;
		private boolean logServerLoad = false;
		private boolean logTransportTCP = false;
		private boolean logTransportProxy = false;
		private boolean logTransportMisc = false;
		
		public RMIServerLogCongig() {}
		
		public RMIServerLogCongig logServerCalls() { this.logServerCalls = true; return this; }
		public RMIServerLogCongig logServerRefs() { this.logServerRefs = true; return this; }
		public RMIServerLogCongig logGarbageCollection() { this.logGarbageCollection = true; return this; }
		public RMIServerLogCongig logServerLoad() { this.logServerLoad = true; return this; }
		public RMIServerLogCongig logTransportTCP() { this.logTransportTCP = true; return this; }
		public RMIServerLogCongig logTransportProxy() { this.logTransportProxy = true; return this; }
		public RMIServerLogCongig logTransportMisc() { this.logTransportMisc = true; return this; }
	}
	
	public static class RMIClientLogCongig {
		private boolean logClientCalls = false;
		private boolean logClientRefs = false;
		private boolean logGarbageCollection = false;
		private boolean logClientLoad = false;
		private boolean logTransportTCP = false;
		private boolean logTransportProxy = false;
		private boolean logTransportMisc = false;
		
		public RMIClientLogCongig() {}
		
		public RMIClientLogCongig logClientCalls() { this.logClientCalls = true; return this; }
		public RMIClientLogCongig logClientRefs() { this.logClientRefs = true; return this; }
		public RMIClientLogCongig logGarbageCollection() { this.logGarbageCollection = true; return this; }
		public RMIClientLogCongig logClientLoad() { this.logClientLoad = true; return this; }
		public RMIClientLogCongig logTransportTCP() { this.logTransportTCP = true; return this; }
		public RMIClientLogCongig logTransportProxy() { this.logTransportProxy = true; return this; }
		public RMIClientLogCongig logTransportMisc() { this.logTransportMisc = true; return this; }
	}
}
