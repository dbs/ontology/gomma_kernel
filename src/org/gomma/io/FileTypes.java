package org.gomma.io;

import java.io.Serializable;

/**
 * The enumeration contains relevant in- and output data formats.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public enum FileTypes implements Serializable {

	/**
	 * The format defines the standard XML format; In future it should be clearly defined which format it is, 
	 * i.e, source vs. mapping and the GommaXML vs. alignment format.
	 */
	XML {
		public String toString() {
			return "XML-based data exchange";
		}
	}
//	CSV {
//		public String toString() {
//			return "CSV-based data exchange";
//		}
//	}
}
