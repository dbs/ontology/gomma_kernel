/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/

package org.gomma.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

/**
 * The class PropertyIniReader reads *.ini files and returns the content.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.4
 */
public class PropertyIniReader {

	private static final String NL = "\n";
	
	/**
	 * The filename represents the name of the *.ini file.
	 */
	private PropertyMap propertyMap;
	
	
	
	/**
	 * The constructor initializes the class PropertyIniReader.
	 * @param fileName name of the *.ini file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public PropertyIniReader (String fileName) throws FileNotFoundException, IOException {
		this.propertyMap = this.readIniFile(fileName);
	}
		
	/**
	 * The method returns a set of parameter name and value associations.
	 * @param section name of the section for which the parameter definitions should be retrieved
	 * @return properties containing a set of parameter name and value associations
	 */
	public Properties getSection (String sectionName) throws NoSuchElementException {
		return this.propertyMap.getPropertySection(sectionName);
	}
	
	public Set<String> getSectionSet() {
		return this.propertyMap.getSectionSet();
	}
	
	
	private PropertyMap readIniFile(String fileNameLoc) throws FileNotFoundException, IOException {
		StringBuilder buf = new StringBuilder();
		String line;
		BufferedReader reader;
		
		try {
			FileInputStream fis = new FileInputStream(fileNameLoc);
			reader = new BufferedReader(new InputStreamReader(fis));
			
			while ((line=reader.readLine())!=null) buf.append(line).append(NL);
			
			reader.close();
			fis.close();
			
		} catch (FileNotFoundException e) {
			InputStream is = getClass().getResourceAsStream(fileNameLoc);
			reader = new BufferedReader(new InputStreamReader(is));
			
			while ((line=reader.readLine())!=null) buf.append(line).append(NL);
			
			reader.close();
			is.close();
		}
		
		return this.parseIniFile(buf.toString());
	}
	
	private PropertyMap parseIniFile(String buf) {
		
		PropertyMap map = new PropertyMap();
		String sectionName = null;
		String[] prop;

		for (String line : buf.split(NL)) {
			if (line.trim().length() == 0) continue;
			if (line.startsWith("#")) {
				sectionName = line.substring(1).trim();
				continue;
			}
			if (sectionName != null && !sectionName.equals("")) {
				prop = line.trim().split("=");
				if (prop.length == 1) map.addProperty(sectionName,prop[0],"");
				else if (prop.length == 2) map.addProperty(sectionName,prop[0],prop[1]);
			}
		}
		
		return map;
	}
}
