package org.gomma.io.importer;

import org.gomma.io.importer.models.ImportMapping;

public interface MappingWrapper {
	public void getCorrespondences(ImportMapping map);
}
