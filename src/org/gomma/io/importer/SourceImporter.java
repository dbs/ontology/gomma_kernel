package org.gomma.io.importer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

public class SourceImporter {

	/**
	 * @param args
	 */
	private PreSourceImporter preImport;
	private PostSourceImporter postImport;
	private String physicalSourceName;
	private String objectType;
	private boolean isOntology;
	private String location;
	private String datastring;
	private String version;
	private SimplifiedGregorianCalendar timestamp;
	private GraphTypes structureType = GraphTypes.EDGELESS;
	private String importerClass;
	private boolean isNewLDS;
	private SourceVersion latestVersionInRep;
	private String accessionprefix;
	
	public static final String SOURCE  			= "source";
	public static final String OBJECTTYPE  		= "objecttype";
	public static final String ISONTOLOGY  		= "isontology";
	public static final String LOCATION  		= "location";
	public static final String DATASTRING  		= "datastring";
	public static final String VERSION  		= "version";
	public static final String TIMESTAMP  		= "timestamp";
	public static final String STRUCTURETYPE  	= "structureType";
	public static final String IMPORTER  		= "importer";
	public static final String ACCESSIONSPREFIX	= "accessionprefix";
	
	public SourceImporter() {}

	public void importSource(String importDescriptionFile) throws ImportException {
		this.readParametersAndImport(importDescriptionFile);
	}
	
	public void importSource(HashMap<String,String> importDescriptionObj) throws ImportException {
		this.readParametersAndImport(importDescriptionObj);
	}
	
	private void readParametersAndImport(String importDescriptionFile) throws ImportException {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(importDescriptionFile));
			String line;
			
			while ((line=reader.readLine())!=null) {
			
				line = line.trim();
				String[] lineParts = line.split("\t");
				
				if (lineParts.length==2) {
					String property = lineParts[0].trim();
					String value = lineParts[1].trim();
					
					if (property.equals(SOURCE)) {
						physicalSourceName = value;
					} else if (property.equals(OBJECTTYPE)) {
						objectType = value;
					} else if (property.equals(ISONTOLOGY)) {
						isOntology = value.equals("1") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true");
					} else if (property.equals(LOCATION)) {
						location = value;						
					} else if (property.equals(VERSION)) {
						version = value;
					} else if (property.equals(TIMESTAMP)) {
						String[] valueParts = value.split("-");
						int year = Integer.parseInt(valueParts[0]);
						int month = Integer.parseInt(valueParts[1]);
						int day = Integer.parseInt(valueParts[2]);
						timestamp = new SimplifiedGregorianCalendar(year,month-1,day);
					} else if (property.equals(STRUCTURETYPE)) {
						structureType=GraphTypes.getGraphType(Integer.parseInt(value));
					} else if (property.equals(IMPORTER)) {
						importerClass = value;
					} else if (property.equals(ACCESSIONSPREFIX)) {
						accessionprefix = value;
					}
					
				} else if (lineParts.length==1) {
					
					String property = lineParts[0].trim();
					if (property.equals("#")) {
						this.importSingleSource();
						physicalSourceName = null;
						objectType = null;
						location = null;
						version = null;
						timestamp = null;
						structureType=GraphTypes.EDGELESS;
						importerClass = null;
						isNewLDS = false;
						latestVersionInRep = null;
						accessionprefix = "";
					}
				}
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	private void readParametersAndImport(HashMap<String,String> importDescriptionObj) throws ImportException {
		
		for(String key : importDescriptionObj.keySet()){
		
			String property = key.trim();
			String value	= importDescriptionObj.get(key).trim();
			
			if (property.equals(SOURCE)) {
				physicalSourceName = value;
			} else if (property.equals(OBJECTTYPE)) {
				objectType = value;
			} else if (property.equals(ISONTOLOGY)) {
				isOntology = value.equals("1") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true");
			} else if (property.equals(LOCATION)) {
				location = value;						
			} else if (property.equals(DATASTRING)) {
				datastring = value;						
			} else if (property.equals(VERSION)) {
				version = value;
			} else if (property.equals(TIMESTAMP)) {
				String[] valueParts = value.split("-");
				int year = Integer.parseInt(valueParts[0]);
				int month = Integer.parseInt(valueParts[1]);
				int day = Integer.parseInt(valueParts[2]);
				timestamp = new SimplifiedGregorianCalendar(year,month-1,day);
			} else if (property.equals(STRUCTURETYPE)) {
				structureType=GraphTypes.getGraphType(Integer.parseInt(value));
			} else if (property.equals(IMPORTER)) {
				importerClass = value;
			} else if (property.equals(ACCESSIONSPREFIX)) {
				accessionprefix = value;
			}
		}
		this.importSingleSource();
		
		physicalSourceName = null;
		objectType = null;
		location = null;
		datastring = null;	
		version = null;
		timestamp = null;
		structureType=GraphTypes.EDGELESS;
		importerClass = null;
		isNewLDS = false;
		latestVersionInRep = null;
		accessionprefix = "";
	}
	private void importSingleSource() throws ImportException {
		try {
			System.out.println("\nStarting import of "+objectType+"@"+physicalSourceName+" of version "+version+" from "+timestamp.get(Calendar.YEAR)+"-"+(timestamp.get(Calendar.MONTH)+1)+"-"+timestamp.get(Calendar.DAY_OF_MONTH));
			if (isImportable()) {
				this.doPreImport();
				this.doPostImport();
			} else {
				System.out.println("Import aborted: "+objectType+"@"+physicalSourceName+" has a newer version in the repository !");
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
			throw new ImportException(e.getMessage());
		}
	}
	private void doPreImport() throws ImportException {
		this.preImport = this.getResponsibleImporter(physicalSourceName);
		this.preImport.importSource();
	}
	private void doPostImport() throws ImportException {
		this.postImport = new PostSourceImporter(this);
		this.postImport.importSource();
	}
	private PreSourceImporter getResponsibleImporter(String source) throws ImportException {
		PreSourceImporter result;
		try {
			Class<?> resultClass = Class.forName(importerClass);
			result = (PreSourceImporter)resultClass.newInstance();
			result.setMainImporter(this);
			return result;
		} catch (ClassNotFoundException e) {
			throw new ImportException(e.getMessage());
		} catch (InstantiationException e) {
			throw new ImportException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new ImportException(e.getMessage());
		}
	}
	private boolean isImportable() throws RepositoryException {
		//Checken ob zu importierende Quelle neuer als alle Versionen im Rep. ist
		//Drei Varianten: 1. LDS noch nicht vorhanden -> evtl. anlegen und Import fortsetzen
		//				  2. LDS vorhanden, jedoch in bereits neuerer Version ->Abbruch des Import
		//                3. LDS vorhanden, zu importierende Version ist die Neueste -> Import fortsetzen
		SourceSet s = APIFactory.getInstance().getSourceAPI().getSourceSet(objectType, physicalSourceName);
		isNewLDS = s.size()==0;
		
		if (!isNewLDS) {
			SourceVersionSet set = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s.iterator().next());
			latestVersionInRep = set.getLatestSourceVersion();
			if (this.timestamp.equals(latestVersionInRep.getCreationVersionDate()) || 
					this.timestamp.before(latestVersionInRep.getCreationVersionDate()))
				return false;
		}
		return true;
	}

	public String getLocation() {
		return location;
	}
	public String getDataString() {
		return datastring;
	}
	public String getObjectType() {
		return objectType;
	}
	public PostSourceImporter getPostImport() {
		return postImport;
	}
	public PreSourceImporter getPreImport() {
		return preImport;
	}
	public String getSourceName() {
		return physicalSourceName;
	}
	public SimplifiedGregorianCalendar getTimestamp() {
		return timestamp;
	}
	public String getVersion() {
		return version;
	}
	public String getImporterClass() {
		return importerClass;
	}
	public boolean isNewLDS() {
		return isNewLDS;
	}
	public SourceVersion getLatestVersionInRep() {
		return latestVersionInRep;
	}
	public boolean isOntology() {
		return isOntology;
	}
	public GraphTypes getStructureType() {
		return this.structureType;
	}
	public String getAccessionPrefix() {
		return this.accessionprefix;
	}
}
