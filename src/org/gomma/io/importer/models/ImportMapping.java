package org.gomma.io.importer.models;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class ImportMapping extends Object implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1741054773672180418L;
	
	public String baseName;
	public String versionName;
	public String mappingClass;
	public String mappingType;
	public String mappingMethod;
	public String mappingTool;
	public Calendar mappingTimestamp;
	public int minSupport;
	public float minConfidence;
	public boolean isInstanceMap;
	public List<ImportSource> domainSources;
	public List<ImportSource> rangeSources;
	public List <ImportSource> instanceDomainSources;
	public List <ImportSource> instanceRangeSources;
	public Map<String,String> parameters;
	public List<ImportCorrespondence> correspondences;
	//for external Mappings, e.g., GOA, Ensembl, ...
	public boolean isExternalImport;
	public String accessClass;
	public String accessLocation;
	public Map<String, String> accessProps;
	
	public ImportMapping() {
		this.mappingClass = "ontology_mapping";
		this.mappingType = "is_equivalent_to";
		this.isInstanceMap = false;
		this.mappingMethod = "N/A";
		this.mappingTool = "N/A";
		this.minSupport = 0;
		this.minConfidence = 0;
		this.parameters = new Object2ObjectOpenHashMap<String,String>();
		this.domainSources = new Vector<ImportSource>();
		this.rangeSources = new Vector<ImportSource>();
		this.instanceDomainSources = new Vector<ImportSource>();
		this.instanceRangeSources = new Vector<ImportSource>();
		this.correspondences = new Vector<ImportCorrespondence>();
		this.isExternalImport = false;
		this.accessProps = new Object2ObjectOpenHashMap<String, String>();
	}
	public void addCorrespondence(ImportCorrespondence newCorrespondence) {
		this.correspondences.add(newCorrespondence);
	}
	public void addParameter(String attribute, String value) {
		this.parameters.put(attribute,value);
	}
	public void addDomainSource(ImportSource newDomainSource) {
		this.domainSources.add(newDomainSource);
	}
	public void addRangeSource(ImportSource newRangeSource) {
		this.rangeSources.add(newRangeSource);
	}
	public void addInstanceDomainSource(ImportSource newInstanceDomainSource) {
		this.instanceDomainSources.add(newInstanceDomainSource);
	}
	public void addInstanceRangeSource(ImportSource newInstanceRangeSource) {
		this.instanceRangeSources.add(newInstanceRangeSource);
	}
}
