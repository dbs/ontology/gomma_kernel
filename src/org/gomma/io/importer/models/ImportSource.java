package org.gomma.io.importer.models;

import java.io.Serializable;
import java.util.Calendar;

public class ImportSource extends Object implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3008194184692783709L;
	
	public String objectType;
	public String sourceName;
	public Calendar timestamp;
	public String version;
	public boolean isOntology;
	
	public ImportSource() {
		this.isOntology = false;
	}
	public String getLDSIdentifier() {
		return this.objectType + "@" + this.sourceName;
	}
}
