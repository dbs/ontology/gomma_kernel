package org.gomma.io.importer.models;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.io.Serializable;
import java.util.Map;

public class ImportCorrespondence extends Object implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6997735777429649258L;
	
	public float confidence;
	public int support;
	public int nChecked;
	public String corr_type;
	public String domainObjectType;
	public String domainSourceName;
	public String domainObjAcc;
	public String rangeObjectType;
	public String rangeSourceName;
	public String rangeObjAcc;
	public Map<String, Object> corrAtts; 
	
	public ImportCorrespondence() {
		this.confidence = 0;
		this.support = 0;
		this.nChecked = 0;
		this.corr_type = "N/A";
		corrAtts = new Object2ObjectOpenHashMap<String, Object>();
	}
}
