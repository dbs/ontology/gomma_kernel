package org.gomma.io.importer.models;

import java.util.ArrayList;
import java.util.List;

import org.gomma.model.DataTypes;

public class ImportObj {

	private String accession;
	private List<ImportObjAttribute> objAttrList;
	
	
	public ImportObj(String acc) {
		this.accession = acc;
		this.objAttrList = new ArrayList<ImportObjAttribute>();
	}
	
	public void addAttribute(String attName, String attScope, DataTypes type, String value) {
		ImportObjAttribute att = new ImportObjAttribute(attName,attScope,type,value);
		this.objAttrList.add(att);
	}
	
	public String getAccessionNumber() {
		return this.accession;
	}
	
	public List<ImportObjAttribute> getAttributeList() {
		return this.objAttrList;
	}
	
	
	public class ImportObjAttribute {
		
		private String attName;
		private String scope;
		private DataTypes dataType;
		private String value;
		
		
		public ImportObjAttribute(String name, String scope, DataTypes type, String value) {
			this.attName = name;
			this.scope = scope;
			this.dataType = type;
			this.value = value;
		}		
		
		public String getAttName() {
			return attName;
		}

		public String getScope() {
			return scope;
		}

		public DataTypes getDataType() {
			return dataType;
		}

		public String getValue() {
			return value;
		}
		
		public void setAttName(String attName) {
			this.attName = attName;
		}

		public void setScope(String scope) {
			this.scope = scope;
		}

		public void setDataType(DataTypes dataType) {
			this.dataType = dataType;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
