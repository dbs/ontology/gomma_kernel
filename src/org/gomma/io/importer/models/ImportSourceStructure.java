package org.gomma.io.importer.models;

import java.util.ArrayList;
import java.util.List;

public class ImportSourceStructure {

	private List<ImportObjRelationship> relationList;
	
	public ImportSourceStructure() {
		this.relationList = new ArrayList<ImportObjRelationship>();
	}
	
	public void addRelationship(String fromAccessioNumber, String toAccessionNumber, String type) {
		ImportObjRelationship rel = new ImportObjRelationship(fromAccessioNumber, toAccessionNumber, type);
		//System.out.println(rel.toAccessionNumber+" "+rel.type+" "+rel.fromAccessionNumber);
		/*System.out.println(rel.fromAccessionNumber);
		System.out.println(rel.type);
		System.out.println(rel.toAccessionNumber);
		*/
		if (!this.relationList.contains(rel)) this.relationList.add(rel);
	}
	
	public List<ImportObjRelationship> getRelationshipSet() {
		return this.relationList;
	}
	
	public class ImportObjRelationship {
		
		private String fromAccessionNumber;
		private String toAccessionNumber;
		private String type;
		
		public ImportObjRelationship(String fromAcc, String toAcc, String type) {
			this.fromAccessionNumber = fromAcc;
			this.toAccessionNumber   = toAcc;
			this.type = type;
		}

		public String getFromAccessionNumber() {
			return fromAccessionNumber;
		}

		public String getToAccessionNumber() {
			return toAccessionNumber;
		}

		public String getType() {
			return type;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof ImportObjRelationship)) return false;
			
			ImportObjRelationship rel = (ImportObjRelationship)o;
			return 
				this.fromAccessionNumber.equals(rel.getFromAccessionNumber()) &&
				this.toAccessionNumber.equals(rel.getToAccessionNumber()) &&
				this.type.equals(rel.getType());
		}
		
		public int hashCode() {
			int result = 17;
			result = 31 * result + this.fromAccessionNumber.hashCode();
			result = 31 * result + this.toAccessionNumber.hashCode();
			result = 31 * result + this.type.hashCode();
			return result;
		}
		
		public String toString() {
			return this.fromAccessionNumber + " - "+this.toAccessionNumber;
		}
	}
}
