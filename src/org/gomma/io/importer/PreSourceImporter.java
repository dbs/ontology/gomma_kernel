package org.gomma.io.importer;

import java.util.List;

import org.gomma.api.APIFactory;
import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;


public abstract class PreSourceImporter {
	private SourceImporter sourceImporter;
	
	public void setMainImporter(SourceImporter sourceImporter) {
		this.sourceImporter = sourceImporter;
	}
	public SourceImporter getMainImporter() {
		return this.sourceImporter;
	}

	public void importSource() throws ImportException {
		try {
			// parse data
			loadSourceData();
			// clean the temporary tables
			APIFactory.getInstance().getSourceVersionImportAPI().cleanTmpTables();
			// import the parsed data into temporary tables
			importIntoTmpTables();
			// remove parsed data
			removeDownloadedSourceData();
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	protected SourceVersionImportAPI getImportAPI() throws RepositoryException {
		return APIFactory.getInstance().getSourceVersionImportAPI();
	}
	
	protected void importIntoTmpTables(List<ImportObj> objList, ImportSourceStructure objRelSet) throws ImportException {
		this.uploadObjectData(objList);
		this.uploadSourceData(objRelSet);
	}
	
	protected abstract void loadSourceData() throws ImportException;
	protected abstract void importIntoTmpTables() throws ImportException;
	protected abstract void removeDownloadedSourceData() throws ImportException;
	
	
	private void uploadObjectData(List<ImportObj> objList) throws ImportException {
		try {
			APIFactory.getInstance().getSourceVersionImportAPI().insertTemporaryObjectSet(objList);
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	private void uploadSourceData(ImportSourceStructure structure) throws ImportException {
		try {
			APIFactory.getInstance().getSourceVersionImportAPI().insertTemporarySourceStructure(structure);
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
}
