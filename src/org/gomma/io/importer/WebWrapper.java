package org.gomma.io.importer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import org.gomma.exceptions.ImportException;

public abstract class WebWrapper {

	public void saveWebSite(String fileNameLoc, String url) throws ImportException {
		try {
			PrintWriter writer = new PrintWriter(fileNameLoc);
			writer.write(this.getWebSite(url).toString());
			writer.flush();
			writer.close();
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	protected StringBuffer getWebSite(String url) throws ImportException {
		try {
			StringBuffer buf = new StringBuffer();
			BufferedReader reader = new BufferedReader(this.getURLStream(url));
			String line;
			while ((line=reader.readLine())!=null) buf.append(line);
			reader.close();
			return buf;
		} catch (IOException e) {
			throw new ImportException(e.getMessage());
		}
	}
	
	protected InputStreamReader getURLStream(String urlQuery) throws ImportException {
		try {
			URL url = new URL(urlQuery);
			System.out.println("retrieving data from "+urlQuery);
			return(new InputStreamReader(url.openStream()));
		} catch(Exception e) {
			throw new ImportException(e.getMessage());
		}
	}
}
