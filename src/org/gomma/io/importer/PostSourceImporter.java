package org.gomma.io.importer;

import java.util.GregorianCalendar;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.ImportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.SourceSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public class PostSourceImporter {
	private SourceImporter sourceImporter;
	public PostSourceImporter(SourceImporter mainImporter) {
		this.sourceImporter = mainImporter;
	}
	public void importSource() throws ImportException {
		try {
			long start, duration;
			System.out.println("Starting to integrate version into repository");
			if (sourceImporter.isNewLDS()) {
				start = System.currentTimeMillis();
				System.out.print("Importing first version of LDS ...");
				
				APIFactory.getInstance().getSourceAPI().insertSource(
						sourceImporter.getObjectType(), 
						sourceImporter.getSourceName(),
						"", sourceImporter.isOntology());
				
				APIFactory.getInstance().getSourceVersionImportAPI().integrateNewAttributes();
				APIFactory.getInstance().getSourceVersionImportAPI().integrateNewRelationshipTypes();
				
				int sourceID = this.getSourceIDForNewLDS(sourceImporter.getObjectType(),sourceImporter.getSourceName());
				APIFactory.getInstance().getSourceVersionImportAPI().importFirstVersion(sourceID,
						new SimplifiedGregorianCalendar(sourceImporter.getTimestamp()));
				
				APIFactory.getInstance().getSourceVersionAPI().insertSourceVersion(
						sourceImporter.getObjectType(), 
						sourceImporter.getSourceName(), 
						sourceImporter.getVersion(),
						new SimplifiedGregorianCalendar(sourceImporter.getTimestamp()),
						sourceImporter.getStructureType());
				
				duration = (System.currentTimeMillis()-start);
				System.out.println(" Done ! ("+duration+" ms)");
				
			} else {
				
				start = System.currentTimeMillis();
				System.out.print("Calculating differences between last and import version ...");
				SimplifiedGregorianCalendar lastTimestamp = this.sourceImporter.getLatestVersionInRep().getCreationVersionDate();
				int sourceID = this.sourceImporter.getLatestVersionInRep().getSource().getID();
				
				APIFactory.getInstance().getSourceVersionImportAPI().integrateNewAttributes();
				APIFactory.getInstance().getSourceVersionImportAPI().integrateNewRelationshipTypes();
				
				APIFactory.getInstance().getSourceVersionImportAPI().computeVersionDiffsForImport(sourceID,lastTimestamp);
				
				duration = (System.currentTimeMillis()-start);
				System.out.println(" Done ! ("+duration+" ms)");
				
				start = System.currentTimeMillis();
				System.out.print("Integrating differences into repository ...");
				SimplifiedGregorianCalendar startTimestamp = new SimplifiedGregorianCalendar(sourceImporter.getTimestamp());
				SimplifiedGregorianCalendar endTimestamp = new SimplifiedGregorianCalendar(startTimestamp);
				endTimestamp.add(GregorianCalendar.DAY_OF_MONTH,-1);
				
				APIFactory.getInstance().getSourceVersionImportAPI().integrateVersionDiffs(sourceID,startTimestamp,endTimestamp);
				
				APIFactory.getInstance().getSourceVersionAPI().insertSourceVersion(
						sourceImporter.getObjectType(), 
						sourceImporter.getSourceName(), 
						sourceImporter.getVersion(),
						new SimplifiedGregorianCalendar(sourceImporter.getTimestamp()),
						sourceImporter.getStructureType());
				
				duration = (System.currentTimeMillis()-start);
				System.out.println(" Done ! ("+duration+" ms)");
			}
			System.out.print("Cleaning up ...");
			APIFactory.getInstance().getSourceVersionImportAPI().cleanTmpTables();
			System.out.println("done\nIntegration of new LDS version completed !");
		} catch (RepositoryException e) {
			throw new ImportException(e.getMessage());
		}
	}
	private int getSourceIDForNewLDS(String objectType, String sourceName) throws RepositoryException {
		try {
			SourceSet sourceSet = APIFactory.getInstance().getSourceAPI().getSourceSet(objectType, sourceName);
			return sourceSet.iterator().next().getID();
			
		} catch (NumberFormatException e) {
			throw new RepositoryException(e.getMessage());
		} catch (RepositoryException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}
