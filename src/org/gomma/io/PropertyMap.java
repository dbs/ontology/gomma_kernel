/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: --
 * 
 **/
package org.gomma.io;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;

/**
 * The class stores sets of properties. Each property set 
 * is associated and identified by a section title.  
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class PropertyMap {

	/**
	 * The attribute stores the property sets
	 */
	private Map<String,Properties> propertyMap;
	
	/**
	 * The constructor initializes the class and the containing property map.
	 */
	public PropertyMap() {
		this.propertyMap =  new HashMap<String,Properties>();
	}
	
	/**
	 * The method adds a property consisting of a property name and its corresponding value
	 * to the specified section.
	 * @param sectionName name of the section to which the property is added
	 * @param propertyName name of the property for which a value is stored
	 * @param propertyValue value of the property
	 */
	public void addProperty(String sectionName, String propertyName, String propertyValue) {
		Properties props;
		if (this.propertyMap.containsKey(sectionName)) props = this.propertyMap.get(sectionName);
		else props = new Properties();
		props.setProperty(propertyName,propertyValue);
		this.propertyMap.put(sectionName,props);
	}
	
	/**
	 * The method returns the set of properties of the specified section.
	 * @param sectionName name of the section for which the property set is retrieved
	 * @return set of properties
	 * @see java.util.Properties
	 * @throws NoSuchElementException
	 */
	public Properties getPropertySection(String sectionName) throws NoSuchElementException {
		if (!this.propertyMap.containsKey(sectionName)) 
			throw new NoSuchElementException("Could not resolve a section using the name '"+sectionName+"'.");
		return this.propertyMap.get(sectionName);
	}
	
	public Set<String> getSectionSet() {
		return this.propertyMap.keySet();
	}
	
	/**
	 * The method returns an iterator over the set of section names available in the property map.
	 * @return iterator over section names
	 */
	public Iterator<String> sectionIterator() {
		return this.propertyMap.keySet().iterator();
	}
}
