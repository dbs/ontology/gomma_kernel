package org.gomma.io.exporter;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.ExportException;
import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.attribute.AttributeValueVersionSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;

public class XMLSourceVersionExporter extends AbstractGommaExporter implements SourceVersionExporter {

	public XMLSourceVersionExporter() {
		super();
	}
	
	public void exportSource(PrintWriter writer, SourceVersion v) throws ExportException {
		try {
			this.exportSource(writer,APIFactory.getInstance().getSourceVersionStructureAPI().getSourceStructure(v));
		} catch (RepositoryException e) {
			throw new ExportException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new ExportException(e.getMessage());
		} catch (GraphInitializationException e) {
			throw new ExportException(e.getMessage());
		}
	}
	
	public void exportSource(PrintWriter writer, SourceVersionStructure svs) throws ExportException, WrongSourceException {
		StringBuffer buf = new StringBuffer();
		
		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(NL)
		   .append("<source ")
		   .append(this.getSourceVersionAttributeBuffer(svs.getSourceVersion())).append(NL)
		   .append(this.getObjectBuffer(svs.getAllObjects(),svs.getSourceVersion().getID()));
		writer.print(buf);
		buf = new StringBuffer();
		buf.append(this.getRelationshipBuffer(svs))
		   .append("</source>");
		writer.print(buf);
	}
	
	
	
	private StringBuffer getSourceVersionAttributeBuffer(SourceVersion sv) {
		StringBuffer buf = new StringBuffer();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		buf//.append("identifier=\"").append(sv.ldsID).append("\" ")
		   .append("objecttype=\"").append(sv.getSource().getObjectType()).append("\" ")
		   .append("name=\"").append(sv.getSource().getPhysicalSourceName()).append("\" ")
		   .append("timestamp=\"").append(df.format(sv.getCreationVersionDate().getTime())).append("\" ")
		   .append("version=\"").append(sv.getVersion()).append("\" ")
		   .append("is_ontology=\"").append(sv.getSource().isOntology()?"yes":"no").append("\" ")
		   .append("structural_type=\"").append(sv.getStructuralType().toXMLString()).append("\" ")
		   .append("url=\"").append(sv.getSource().getSourceURL()).append("\">");
		
		return buf;
	}
	
	private StringBuffer getObjectBuffer(ObjSet objs, int sourceVersionId) {
		StringBuffer buf = new StringBuffer();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		buf.append("<objects>").append(NL);
		
		for (Obj obj : objs) {
			buf.append("<object ")
			   .append("identifier=\"").append(obj.getID()).append("\" ")
			   .append("accession=\"").append(obj.getAccessionNumber()).append("\" ")
			   .append("date_from=\"").append(df.format(obj.getFromDate().getTime())).append("\" ")
			   .append("date_to=\"").append(df.format(obj.getToDate().getTime())).append("\">").append(NL)
			   .append(this.getObjectAttributeBuffer(obj.getAttributeValues(sourceVersionId)))
			   .append("</object>").append(NL);
		}
		
		buf.append("</objects>").append(NL);
		
		return buf;
	}
	
	private StringBuffer getObjectAttributeBuffer(AttributeValueVersionSet set) {
		StringBuffer buf = new StringBuffer();
		Attribute att;
		
		buf.append("<params>").append(NL);
		
		for (AttributeValueVersion avv : set) {
			att = avv.getAttribute();
			buf.append("<param ")
			   .append("name=\"").append(att.getName()).append("\" ")
			   .append("scope=\"").append(att.getScope()).append("\" ")
			   .append("type=\"").append(att.getDataType().toString()).append("\">")
			   .append(avv.toString()).append("</param>").append(NL);
		}
		
		buf.append("</params>");
		
		return buf;
	}
	
	private StringBuffer getRelationshipBuffer(SourceVersionStructure svs) {
		StringBuffer buf = new StringBuffer();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		buf.append("<relationships>");
		
		for (ObjRelationship rela : svs.getAllRelationships().getCollection())
			buf.append("<relationship ")
			   .append("parent_object_ref=\"").append(rela.getFromID()).append("\" ")
			   .append("child_object_ref=\"").append(rela.getToID()).append("\" ")
			   .append("type=\"").append(rela.getRelationshipType()).append("\" ")
			   .append("date_from=\"").append(df.format(rela.getFromDate().getTime())).append("\" ")
			   .append("date_to=\"").append(df.format(rela.getToDate().getTime())).append("\" />").append(NL);
		
		buf.append("</relationships>");
		
		return buf;
	}
}
