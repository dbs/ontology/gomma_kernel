package org.gomma.io.exporter;

/**
 * The abstract class defines some general constants and methods for exporting data. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public abstract class AbstractGommaExporter {
	
	/**
	 * The constant represents the new line character.
	 */
	protected static final String NL = "\n";
	
	/**
	 * The constant represents the tabulator character.
	 */
	protected static final String TAB= "\t";
	
		
	
}
