package org.gomma.io.exporter;

import org.gomma.io.FileTypes;

/**
 * The class represents the factory for returning the initialized class 
 * with that mapping data can be exported.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingExportFactory {

	/**
	 * Constructor of the class.
	 */
	private MappingExportFactory() {}
	
	/**
	 * The method returns the mapping exporter for the specified file type.
	 * @param types file type
	 * @return mapping exporter
	 */
	public static MappingExporter getExporter(FileTypes types) {
		switch (types) {
			case XML: return getXMLExporter();
			default: return null;
		}
	}
	
	/**
	 * The method returns the mapping exporter for the XML file type.
	 * @return XML-based mapping exporter
	 */
	private static XMLMappingExporter getXMLExporter() {
		return new XMLMappingExporter();
	}
}
