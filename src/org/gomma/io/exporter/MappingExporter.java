package org.gomma.io.exporter;

import java.io.PrintWriter;

import org.gomma.exceptions.ExportException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The interface defines methods for exporting a mapping version.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingExporter {

	/**
	 * The method exports the data of the given mapping version object 
	 * including their object correspondences. The latter will be automatically loaded
	 * from the repository.
	 * @param writer print writer that is used to export the mapping version data.
	 * @param v mapping version for that the data is exported
	 * @throws ExportException
	 */
	public void exportMappingVersion(PrintWriter writer, MappingVersion v) throws ExportException;
	
	/**
	 * The method exports the data of the given mapping version object
	 * and set of object correspondences.  
	 * @param writer print writer that is used to export the mapping version data
	 * @param v mapping version for that the data is exported
	 * @param corrs set of object correspondences that is exported
	 * @throws ExportException
	 */
	public void exportMappingVersion(PrintWriter writer, MappingVersion v, ObjCorrespondenceSet corrs) throws ExportException;
}
