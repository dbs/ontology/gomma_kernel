package org.gomma.io.exporter;

import java.io.PrintWriter;

import org.gomma.exceptions.ExportException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The interface defines methods to export source data.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceVersionExporter {

	/**
	 * The method exports 
	 * @param writer
	 * @param sv
	 * @throws ExportException
	 */
	public void exportSource(PrintWriter writer, SourceVersion sv) throws ExportException;
	
	public void exportSource(PrintWriter writer, SourceVersionStructure svs) throws ExportException, WrongSourceException;
}
