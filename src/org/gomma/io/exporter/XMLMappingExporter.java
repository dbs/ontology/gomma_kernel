package org.gomma.io.exporter;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.ExportException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public class XMLMappingExporter extends AbstractGommaExporter implements MappingExporter {

	public XMLMappingExporter() {
		super();
	}
	
	public void exportMappingVersion(PrintWriter writer, MappingVersion v) throws ExportException {
		try {
			this.exportMappingVersion(writer,v,
					APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v));
		} catch (RepositoryException e) {
			throw new ExportException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new ExportException(e.getMessage());
		}
	}
	public void exportMappingVersionWithStatus(PrintWriter writer, MappingVersion v) throws ExportException {
		try {
			this.exportMappingVersionWithStatus(writer,v,
					APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v));
		} catch (RepositoryException e) {
			throw new ExportException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new ExportException(e.getMessage());
		}
	}
	public void exportMappingVersion(PrintWriter writer, MappingVersion v, ObjCorrespondenceSet corrs) throws ExportException {
		StringBuffer buf = new StringBuffer();

		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(NL)
		   .append(this.getMappingHeader(v.getMapping(),v))
		   .append(this.getMappingVersionHeader(v));
		writer.print(buf);
		this.writeMappingData(writer,v,corrs);
		//writer.println("</mapping-version>");
		writer.println("</mapping>");
	}
	
	public void exportMappingVersionWithStatus(PrintWriter writer, MappingVersion v, ObjCorrespondenceSet corrs) throws ExportException {
		StringBuffer buf = new StringBuffer();

		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(NL)
		   .append(this.getMappingHeader(v.getMapping(),v))
		   .append(this.getMappingVersionHeader(v));
		writer.print(buf);
		this.writeMappingDataWithStatus(writer,v,corrs);
		//writer.println("</mapping-version>");
		writer.println("</mapping>");
	}	
	
	private StringBuffer getMappingHeader(Mapping m,MappingVersion v) {
			StringBuffer buf = new StringBuffer();
			
			SimplifiedGregorianCalendar domainCal = v.getDomainSourceVersionSet().getLatestSourceVersion().getCreationVersionDate();
			SimplifiedGregorianCalendar rangeCal = v.getRangeSourceVersionSet().getLatestSourceVersion().getCreationVersionDate();
			String date;
			if(domainCal.before(rangeCal)){
				date = rangeCal.get(Calendar.YEAR)+"-"+ (rangeCal.get(Calendar.MONTH)+1)+"-"+ rangeCal.get(Calendar.DAY_OF_MONTH);
			}else{
				date = domainCal.get(Calendar.YEAR)+"-"+ (domainCal.get(Calendar.MONTH)+1)+"-"+ domainCal.get(Calendar.DAY_OF_MONTH);
			}

			buf.append("<mapping ")
			   .append("baseName=\"").append(m.getName()).append("\" ")
			   .append("versionName=\"").append(v.getName()).append("\" ")
			   .append("timestamp=\"").append(date).append("\" ")
			   .append("is_instance_map=\"").append(m.isInstanceMapping()?"true":"false").append("\" ")
			   .append("mapping_class=\"").append(m.getMappingClassName()).append("\" ")
			   .append("mapping_type=\"").append(m.getMappingTypeName()).append("\" ")
			   .append("mapping_tool=\"").append(m.getMappingToolName()).append("\" ")
			   .append("mapping_method=\"").append(m.getMappingMethodName())
			   .append("\">").append(NL);
			   
			return buf;
	}
	
	private StringBuffer getMappingVersionHeader(MappingVersion v) {
		StringBuffer buf = new StringBuffer();
		
		buf.append("<metadata ")
		   .append("minConfidence=\"").append(v.getLowestConfidenceValue()).append("\" ")
		   .append("minSupport=\"").append(v.getLowestSupportValue()).append("\">").append(NL);
		
		buf.append("<domain_sources>").append(NL);
		buf.append(this.getSourceVersionBuffer(v.getDomainSourceVersionSet().getLatestSourceVersion())).append(NL);
		buf.append("</domain_sources>").append(NL);
		
		buf.append("<range_sources>").append(NL);
		buf.append(this.getSourceVersionBuffer(v.getRangeSourceVersionSet().getLatestSourceVersion())).append(NL);
		buf.append("</range_sources>").append(NL);
		
		/*
		//Instanzquellen
		if(v.getDomainInstanceSourceVersionSet().size()>0 || v.getRangeInstanceSourceVersionSet().size()>0){
			buf.append("<instance_sources>").append(NL);
			
			if(v.getDomainInstanceSourceVersionSet().size()>0){
				buf.append("<domain_sources>").append(NL);
				buf.append(this.getSourceVersionBuffer(v.getDomainInstanceSourceVersionSet().getLatestSourceVersion())).append(NL);
				buf.append("</domain_sources>").append(NL);
			}
			if(v.getRangeInstanceSourceVersionSet().size()>0){
				buf.append("<range_sources>").append(NL);
				buf.append(this.getSourceVersionBuffer(v.getRangeInstanceSourceVersionSet().getLatestSourceVersion())).append(NL);
				buf.append("</range_sources>").append(NL);
			}		   
			buf.append("</instance_sources>").append(NL);			
		}*/

		/*
		buf.append("<params>").append(NL);
		for (A att : map.attributes.getCollection())
			buf.append("<param ")
			   .append("name=\"").append(att.attributeName).append("\" ")
			   .append("scop...???*/
		

		buf.append("</metadata>").append(NL);
		
		return buf;
	}
	
	private StringBuffer getSourceVersionBuffer(SourceVersion sv) {
		StringBuffer buf = new StringBuffer();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		buf.append("<source ")
//		   .append("identifier=\"").append(sv.ldsID).append("\" ")
		   .append("objecttype=\"").append(sv.getSource().getObjectType()).append("\" ")
		   .append("name=\"").append(sv.getSource().getPhysicalSourceName()).append("\" ")
		   .append("timestamp=\"").append(df.format(sv.getCreationVersionDate().getTime())).append("\" ")
		   .append("version=\"").append(sv.getVersion()).append("\" ")
		   .append("is_ontology=\"").append(sv.getSource().isOntology()?"yes":"no").append("\" ")
		   .append("structural_type=\"").append(sv.getStructuralType().toXMLString()).append("\" ")
		   .append("url=\"").append(sv.getSource().getSourceURL()).append("\" />");
		
		return buf;
	}

	
	private void writeMappingData(PrintWriter writer, MappingVersion v, ObjCorrespondenceSet corrs) {
		StringBuffer buf = new StringBuffer();

		buf.append("<correspondences>").append(NL);
		
		for (ObjCorrespondence corr : corrs.getCollection()) {
			
			buf.append("<correspondence ")
			   .append("support=\"").append(corr.getSupport()).append("\" ")
			   .append("confidence=\"").append(corr.getConfidence()).append("\" ")
			   .append("user_checked=\"").append(corr.getNumberOfChecks()).append("\" ")
			   .append("corr_type=\"").append(corr.getCorrespondenceTypeName()).append("\">").append(NL);
			
			buf.append("<domain_objects>").append(NL)
			   .append("<object ")
			   .append("accession=\"").append(corr.getDomainAccessionNumber()).append("\" ")
			   .append("objecttype=\"").append(v.getDomainSourceVersionSet().getSource(corr.getDomainLDSID()).getObjectType()).append("\" ")
			   .append("source_name=\"").append(v.getDomainSourceVersionSet().getSource(corr.getDomainLDSID()).getPhysicalSourceName()).append("\" />").append(NL);
			buf.append("</domain_objects>").append(NL);
			
			buf.append("<range_objects>").append(NL)
			   .append("<object ")
			   .append("accession=\"").append(corr.getRangeAccessionNumber()).append("\" ")
			   .append("objecttype=\"").append(v.getRangeSourceVersionSet().getSource(corr.getRangeLDSID()).getObjectType()).append("\" ")
			   .append("source_name=\"").append(v.getRangeSourceVersionSet().getSource(corr.getRangeLDSID()).getPhysicalSourceName()).append("\" />").append(NL);
			buf.append("</range_objects>").append(NL);
			
			buf.append("</correspondence>").append(NL);
			
			if (buf.length()>5000) {
				writer.print(buf);
				buf=new StringBuffer();
			}
		}
		
		buf.append("</correspondences>").append(NL);
		writer.print(buf);
	}
	
	private void writeMappingDataWithStatus(PrintWriter writer, MappingVersion v, ObjCorrespondenceSet corrs) {
		StringBuffer buf = new StringBuffer();

		buf.append("<correspondences>").append(NL);
		
		for (ObjCorrespondence corr : corrs.getCollection()) {
						
			buf.append("<correspondence ")
			   .append("support=\"").append(corr.getSupport()).append("\" ")
			   .append("confidence=\"").append(corr.getConfidence()).append("\" ")
			   .append("user_checked=\"").append(corr.getNumberOfChecks()).append("\" ")
			   .append("status=\"").append(corr.getCorrespondenceStatusName()).append("\" ")
			   .append("corr_type=\"").append(corr.getCorrespondenceTypeName()).append("\">").append(NL);
			
			buf.append("<domain_objects>").append(NL)
			   .append("<object ")
			   .append("accession=\"").append(corr.getDomainAccessionNumber()).append("\" ")
			   .append("objecttype=\"").append(v.getDomainSourceVersionSet().getSource(corr.getDomainLDSID()).getObjectType()).append("\" ")
			   .append("source_name=\"").append(v.getDomainSourceVersionSet().getSource(corr.getDomainLDSID()).getPhysicalSourceName()).append("\" />").append(NL);
			buf.append("</domain_objects>").append(NL);
			
			buf.append("<range_objects>").append(NL)
			   .append("<object ")
			   .append("accession=\"").append(corr.getRangeAccessionNumber()).append("\" ")
			   .append("objecttype=\"").append(v.getRangeSourceVersionSet().getSource(corr.getRangeLDSID()).getObjectType()).append("\" ")
			   .append("source_name=\"").append(v.getRangeSourceVersionSet().getSource(corr.getRangeLDSID()).getPhysicalSourceName()).append("\" />").append(NL);
			buf.append("</range_objects>").append(NL);
			
			buf.append("</correspondence>").append(NL);
			
			if (buf.length()>5000) {
				writer.print(buf);
				buf=new StringBuffer();
			}
		}
		
		buf.append("</correspondences>").append(NL);
		writer.print(buf);
	}
}


