package org.gomma.io.exporter;

import org.gomma.io.FileTypes;

public class SourceVersionExportFactory {

	public static SourceVersionExporter getExporter(FileTypes types) {
	
		switch (types) {
			case XML: return getXMLExporter();
			default: return null;
		}
	}
	
	private static XMLSourceVersionExporter getXMLExporter() {
		return new XMLSourceVersionExporter();
	}
}
