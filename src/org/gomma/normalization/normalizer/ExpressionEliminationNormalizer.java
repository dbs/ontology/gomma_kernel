package org.gomma.normalization.normalizer;

import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueFactory;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.util.string.ExpressionHandler;

public class ExpressionEliminationNormalizer extends AbstractNormalizer {

	private ExpressionHandler deleter;
	
	public ExpressionEliminationNormalizer(ExpressionHandler d) {
		this.deleter=d;
	}
	
	public ObjSet normalize(ObjSet inputObjs) throws OperatorException {
		try {
			ObjSet outputObjs = new ObjSet();
			AttributeSet attSet;
			int sourceVersionID = super.getSourceVersionID();
			String attName = super.getAttributeName();
		
			for (Obj obj : inputObjs) {
				
				if (sourceVersionID!=-1) {
					attSet = obj.getAttributeValues(sourceVersionID).getAttributeSet();
					
					if (!attSet.contains(attName))
						throw new NoSuchElementException("Could not find attributes with name '"+attName+"'.");
					
					for (Attribute att : attSet.getAttributeSet(attName)) {
						for (AttributeValueVersion avv : obj.getAttributeValues(sourceVersionID,att)) {
							obj.addAttributeValue(AttributeValueFactory.getValue(
									avv.getID(),att,avv.getFromDate(),avv.getToDate(),
									this.deleter.deleteExpression(avv.toString())));
						}
					}
				} else {
//					if (!obj.getAttributes(sourceVersionID).getAttributeNames().contains(attName))
//						throw new NoSuchElementException();
//					for (SourceVersion s : obj.getSourceVersionSet().getCollection()) {
//						for (ObjAttribute att : obj.getAttribute(s.getID(),attName)) 
//							att.valueString = this.deleter.deleteExpression(att.valueString);
//					}
				}
				outputObjs.addObj(obj);
			}
		
			return outputObjs;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
}
