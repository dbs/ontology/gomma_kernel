package org.gomma.normalization.normalizer;

import java.util.Properties;

import org.gomma.exceptions.OperatorException;
import org.gomma.model.source.ObjSet;

public interface Normalizer {

	public ObjSet normalize(ObjSet inputObjs) throws OperatorException;
	
	public Properties getNormalizationPropeties();
	public void setNormalizationProperties(Properties props);
}
