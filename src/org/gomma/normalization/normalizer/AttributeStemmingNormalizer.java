package org.gomma.normalization.normalizer;

import java.util.NoSuchElementException;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueFactory;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.util.string.Stemmer;

public class AttributeStemmingNormalizer extends AbstractNormalizer {

	private Stemmer stemmer;
	
	public AttributeStemmingNormalizer(Stemmer s) {
		this.stemmer=s;
	}
	
	public ObjSet normalize(ObjSet inputObjs) throws OperatorException {
		try {
			ObjSet outputObjs = new ObjSet();
			AttributeSet attSet;
			int sourceVersionID = super.getSourceVersionID();
			String attName = super.getAttributeName();
			StringBuffer attValueBuf;
			
			for (Obj obj : inputObjs) {
				
				if (sourceVersionID!=-1) {
					attSet = obj.getAttributeValues(sourceVersionID).getAttributeSet();
					
					if (!attSet.contains(attName))
						throw new NoSuchElementException("Could not find attributes with name '"+attName+"'.");
					
					for (Attribute att : attSet.getAttributeSet(attName)) {
						for (AttributeValueVersion avv : obj.getAttributeValues(sourceVersionID,att)) {
							attValueBuf = new StringBuffer();
							for (String s :avv.toString().split("\\s"))
								attValueBuf.append(this.stemmer.stem(s)).append(" ");
							obj.addAttributeValue(AttributeValueFactory.getValue(
									avv.getID(),att,avv.getFromDate(),avv.getToDate(),attValueBuf.toString().trim()));
						}
					}
				} else {
//					if (!obj.getAttributes(sourceVersionID).getAttributeNames().contains(attName))
//						throw new NoSuchElementException();
//					for (SourceVersion sv : obj.getSourceVersionSet().getCollection()) {
//						for (ObjAttribute att : obj.getAttribute(sv.getID(),attName)) {
//							attValueBuf = new StringBuffer();
//							for (String s :att.valueString.split("\\s"))
//								attValueBuf.append(this.stemmer.stem(s)).append(" ");
//							att.valueString = attValueBuf.substring(0,attValueBuf.length()-1);
//						}
//					}
				}
				outputObjs.addObj(obj);
			}
		
			return outputObjs;
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
	}
}
