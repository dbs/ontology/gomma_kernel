package org.gomma.normalization.normalizer;

import java.util.NoSuchElementException;
import java.util.Properties;

import org.gomma.normalization.normalizer.Normalizer;

public abstract class AbstractNormalizer implements Normalizer {

	protected Properties normalizationProps;
	
	public Properties getNormalizationPropeties() {
		return this.normalizationProps;
	}
	
	
	public void setNormalizationProperties(Properties props) {
		this.normalizationProps=props;
	}
	
	public int getSourceVersionID() {
		if (this.normalizationProps.containsKey("[sourceVersionID]")) 
			return Integer.parseInt(this.normalizationProps.getProperty("[sourceVersionID]"));
		else return -1;
	}
	
	protected String getAttributeName() {
		if (this.normalizationProps.containsKey("[attribute]")) 
			return this.normalizationProps.getProperty("[attribute]");
		else throw new NoSuchElementException("Could not find any attribute names");
	}
}
