package org.gomma.normalization;

import org.gomma.exceptions.OperatorException;
import org.gomma.normalization.NormalizationType;
import org.gomma.normalization.normalizer.AttributeStemmingNormalizer;
import org.gomma.normalization.normalizer.Normalizer;
import org.gomma.normalization.normalizer.ExpressionEliminationNormalizer;
import org.gomma.util.string.ExpressionHandler;
import org.gomma.util.string.Stemmer;

public class NormalizationFactory {

	public static Normalizer getNormalizer(NormalizationType type) throws OperatorException {
		
		switch (type) {
			case ROMANIC_NUMBER_DELETIONS: return getExpressionDeleter(ExpressionHandler.ROMANIC_EXPRESSION);
			case NUMERIC_NUMBER_DELETIONS: return getExpressionDeleter(ExpressionHandler.NUMERIC_EXPRESSION);
			case PORTER_STEMMER: return getAttributeStemmer(Stemmer.PORTER);
			default: throw new OperatorException("Could not resolve a valid normalization type.");
		}
	}
	
	private static AttributeStemmingNormalizer getAttributeStemmer(Stemmer s) {
		return new AttributeStemmingNormalizer(s);
	}
	
	private static ExpressionEliminationNormalizer getExpressionDeleter(ExpressionHandler d) {
		return new ExpressionEliminationNormalizer(d);
	}
}
