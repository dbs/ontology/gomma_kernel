package org.gomma.normalization;

public enum NormalizationType {

//	TRIGRAM_UPPER_THRESHOLD {
//		public String toString() {
//			return "upper threshold";
//		}
//	},
//	LOWER_THESHOLD {
//		public String toString() {
//			return "lower threshold";
//		}
//	},
//	BOTH_THRESHOLD {
//		public String toString() {
//			return "upper and lower threshold";
//		}
//	},
	PORTER_STEMMER {
		public String toString() {
			return "Porter stemming normalization";
		}
	},
	ROMANIC_NUMBER_DELETIONS {
		public String toString() {
			return "romanic number deletion";
		}
	},
	NUMERIC_NUMBER_DELETIONS {
		public String toString() {
			return "numeric number deletions";
		}
	}
}
