/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: 
 * 2008-03-25 - Source version adaption (T.Kirsten)
 * 
 **/

package org.gomma.operator;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.api.cache.CacheManager;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.math.AggregationFunction;

/**
 * The class SetOperators implements set-based operators for the defined models, 
 * such as objects and mappings.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SetOperators {

	public enum CorrespondenceAttributes { CONFIDENCE, SUPPORT, TYPE };
	
	public static ObjCorrespondenceSet filter(ObjCorrespondenceSet set, int minOccurrence, float minConfidence) throws OperatorException, WrongSourceException {
		ObjCorrespondenceSet resultSet = new ObjCorrespondenceSet();
		
		for (ObjCorrespondence c : set.getCollection()) {
			if (c.getConfidence()>=minConfidence && c.getSupport()>=minOccurrence)
				resultSet.addCorrespondence(c);
		}
		
		return resultSet;
	}
	
	
	/**
	 * The method computes the union of two sets of objects objs1 and objs2. Both 
	 * object sets need to be of the same logical source but can be of different 
	 * versions. Each object of the unified result set contains the attribute sets 
	 * of the input objects of objs1 and objs2.  
	 * @param set1 set of objects to be unified
	 * @param set2 set of objects to be unified
	 * @return unified object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet union (ObjSet set1, ObjSet set2) throws OperatorException, WrongSourceException {
		return union(set1,set2,true);
	}
	
	/**
	 * The method computes the union of two sets of objects objs1 and objs2. Both 
	 * object sets need to be of the same logical source but can be of different 
	 * versions. Each object of the unified result set contains the attribute sets 
	 * of the input objects of objs1 and objs2.  
	 * @param set1 set of objects to be unified
	 * @param set2 set of objects to be unified
	 * @param mergeAttributeVersions true if object attributes will be merged whenever two objects 
	 * are added using the same object ID; false if the object of the objs2 is rejected whenever
	 * there is an object in objs1 using the same object ID 
	 * @return unified object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet union (ObjSet set1, ObjSet set2, boolean mergeAttributeVersions) throws OperatorException, WrongSourceException {

		if (set1.size()==0 && set2.size()!=0) return set2;
		if (set1.size()!=0 && set2.size()==0) return set1;
		
		if (set1.size()==0 && set2.size()==0) 
			return new ObjSet(
					set1.getLDSID()!=ObjSet.DEFAULT_LDSID?set1.getLDSID():set2.getLDSID());
		
		if (set1.getLDSID()!=set2.getLDSID()) 
			throw new OperatorException("Could not merge object sets of different logical sources.");
		
		ObjSet set = new ObjSet();
		for (Obj o : set1) set.addObj(o,false);
		for (Obj o : set2) set.addObj(o,mergeAttributeVersions);
		return set;
	}
	
	/**
	 * The method returns mapping metadata that is produced by taking the union 
	 * between two given mappings m1 and m2. The mapping is described by the given 
	 * attributes. The mapping statistics will be exactly computed by taking the 
	 * object correspondences into account, i.e., the intersection of object correspondences 
	 * of mappings m1 and m2 needs to be computed before.
	 * @param newMappingVersionName unique name of the resulting mapping version
	 * @param v1 mapping for which the union should be computed
	 * @param v1 mapping for which the union should be computed
	 * @param corrs object correspondences for which the resulting mapping metadata should be produced
	 * @return resulting mapping metadata
	 * @throws OperatorException
	 */
	public static MappingVersion union (String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet corrs) throws OperatorException {
		
		if (!v1.getMapping().equals(v2.getMapping()))
			throw new OperatorException("Could not unify mapping versions because they belong to different mappings");
		
		int minSupport = Integer.MAX_VALUE;
		float minConfidence = Float.MAX_VALUE;
		
		for (ObjCorrespondence cor : corrs.getCollection()) {
			minSupport = Math.round(AggregationFunction.MINIMUM.aggregate(minSupport, cor.getSupport()));
			minConfidence = AggregationFunction.MINIMUM.aggregate(minConfidence, cor.getConfidence());
		}
		
		MappingVersion result = new MappingVersion.Builder(MappingVersion.DEFAULT_MAPPING_VERSION_ID,v1.getMapping())
				.name(newMappingVersionName)
				.creationDate(SimplifiedGregorianCalendar.getInstance())
				.lowestConfidence(minConfidence)
				.lowestSupport(minSupport)
				.numberOfCorrespondences(corrs.size())
				.numberOfDomainObjects(corrs.getDomainObjIds().size())
				.numberOfRangeObjects(corrs.getRangeObjIds().size())
				.build();
		
		for (SourceVersion v : v1.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v2.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v1.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		for (SourceVersion v : v2.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		
//		for (SourceVersion v : v1.getDomainInstanceSourceVersionSet().getCollection()) 
		
		return result;
	}
	
	/**
	 * The method unifies the two given sets of object correspondences objs1 and objs2. 
	 * @param set1 set of object correspondences to be unified
	 * @param set2 set of object correspondences to be unified
	 * @return unified set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet union (ObjCorrespondenceSet set1, ObjCorrespondenceSet set2)  throws OperatorException, WrongSourceException {
		return union(set1,set2,AggregationFunction.SUM,AggregationFunction.AVERAGE);
	}
	
	/**
	 * The method unifies the two given sets of object correspondences objs1 and objs2. 
	 * @param set1 set of object correspondences to be unified
	 * @param set2 set of object correspondences to be unified
	 * @param supportAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. support
	 * @param confidenceAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. confidence
	 * @return unified set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet union (ObjCorrespondenceSet set1, ObjCorrespondenceSet set2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc)  throws OperatorException, WrongSourceException {	
		ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		
		for (ObjCorrespondence obj: set1.getCollection())
			if (!set2.containsCorrespondence(obj.getDomainObjID(),obj.getRangeObjID())) set.addCorrespondence(obj);
		
		for (ObjCorrespondence obj: set2.getCollection()) {
			if (!set1.containsCorrespondence(obj.getDomainObjID(),obj.getRangeObjID())) set.addCorrespondence(obj);
			else set.addCorrespondence(mergeObjCorrespondences(set1.getCorrespondence(obj.getDomainObjID(),obj.getRangeObjID()),obj,supportAggFunc,confidenceAggFunc));
		}
		return set;
	}
	
	/**
	 * The method unifies the multiple given sets of object correspondences objs. 
	 * @param sets array of sets of object correspondences to be unified
	 * @param supportAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. support
	 * @param confidenceAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. confidence
	 * @return unified set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet union (ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc)  throws OperatorException, WrongSourceException {	
		return SetOperators.union(sets, supportAggFunc, confidenceAggFunc, false);
	}
	
	/**
	 * The method unifies the multiple given sets of object correspondences objs. 
	 * @param sets array of sets of object correspondences to be unified
	 * @param supportAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. support
	 * @param confidenceAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. confidence
	 * @param strictMode specifies how the similarites are aggregated (only the given ones or w.r.t. number of sets)
	 * @return unified set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet union (ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode)  throws OperatorException, WrongSourceException {	
		ObjCorrespondenceSet resultSet = new ObjCorrespondenceSet();
		HashMap<String, List<Float>> unifiedCorrsSim = new HashMap<String, List<Float>>();
		HashMap<String, List<Integer>> unifiedCorrsSupport = new HashMap<String, List<Integer>>();
		HashMap<String, ObjCorrespondence> unifiedCorrs = new HashMap<String, ObjCorrespondence>();
		
		for (ObjCorrespondenceSet set : sets) {
			for (ObjCorrespondence corr : set.getCollection()) {
				String corrAsString = corr.getDomainObjID()+"-"+corr.getRangeObjID();
				
				//Correspondences
				if (!unifiedCorrs.containsKey(corrAsString)) {
					unifiedCorrs.put(corrAsString, corr);
				}
	
				//Similarities
				if (!unifiedCorrsSim.containsKey(corrAsString)) {
					unifiedCorrsSim.put(corrAsString, new Vector<Float>());
				}
				List<Float> currentSimilarites = unifiedCorrsSim.get(corrAsString);
				currentSimilarites.add(corr.getConfidence());
				unifiedCorrsSim.put(corrAsString, currentSimilarites);
				//Supports
				if (!unifiedCorrsSupport.containsKey(corrAsString)) {
					unifiedCorrsSupport.put(corrAsString, new Vector<Integer>());
				}
				List<Integer> currentSupports = unifiedCorrsSupport.get(corrAsString);
				currentSupports.add(corr.getSupport());
				unifiedCorrsSupport.put(corrAsString, currentSupports);
			}
		}
		
		for (String corrAsString : unifiedCorrs.keySet()) {
			ObjCorrespondence corr = unifiedCorrs.get(corrAsString);
			List<Float> similarities = unifiedCorrsSim.get(corrAsString);
			List<Integer> supports = unifiedCorrsSupport.get(corrAsString);
			if (strictMode) {
				while (similarities.size()<sets.length) {
					similarities.add(0.0F);
				}
				while (supports.size()<sets.length) {
					supports.add(0);
				}
			}
			
			ObjCorrespondence c = new ObjCorrespondence.Builder(
					corr.getDomainObjID(),corr.getDomainLDSID(),
					corr.getRangeObjID(),corr.getRangeLDSID())
						.domainAccessionNumber(corr.getDomainAccessionNumber())
						.rangeAccessionNumber(corr.getRangeAccessionNumber())
						.confidence(confidenceAggFunc.aggregateFloatList(similarities))
						.support(Math.round(supportAggFunc.aggregateIntegerList(supports)))
						.numberOfUserChecks(corr.getNumberOfChecks())
						.typeName(corr.getCorrespondenceTypeName())
						.statusName(corr.getCorrespondenceStatusName())
						.typeDescription(corr.getCorrespondenceTypeDescription())
						.build();
			
			resultSet.addCorrespondence(c);
		}
		
		return resultSet;
	}
	
	/**
	 * The method unifies two given sets of object relationships
	 * @param set1 set of object relationships to be unified
	 * @param set2 set of object relationships to be unified
	 * @param aggFunc aggregation function (avg, max, min) for merging intersect object relationships w.r.t. date from and to
	 * @return unified set of object relationships
	 */
	public static ObjRelationshipSet union (ObjRelationshipSet set1, ObjRelationshipSet set2, AggregationFunction aggFunc) {
		ObjRelationshipSet set = new ObjRelationshipSet();
		
		for (ObjRelationship objRel : set1.getCollection()) 
			if (!set2.containsRelationship(objRel.getID())) set.addObjectRelationship(objRel);
		
		for (ObjRelationship objRel : set2.getCollection()) {
			if (!set1.containsRelationship(objRel.getID())) set.addObjectRelationship(objRel);
			else set.addObjectRelationship(mergeObjRelationship(objRel,set1.getObjectRelationship(objRel.getID()),aggFunc));
		}
		return set;
	}
	
	/**
	 * The method computes the intersection of two given object sets. Both 
	 * object sets need to be of the same logical source but can be of different 
	 * versions. Each object of the unified result set contains the attribute sets 
	 * of the input objects of objs1 and objs2.
	 * @param set1 object set to compute the intersection with
	 * @param set2 object set to compute the intersection with
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet intersect (ObjSet set1, ObjSet set2) throws OperatorException, WrongSourceException {
		return intersect(set1,set2,true,true);
	}
	
	/**
	 * The method computes the intersection of two given object sets. Both 
	 * object sets need to be of the same logical source but can be of different 
	 * versions. Each object of the unified result set contains the attribute sets 
	 * of the input objects of objs1 and objs2.
	 * @param set1 object set to compute the intersection with
	 * @param set2 object set to compute the intersection with
	 * @param mergeAttributeVersions true if the intersect object should be merged; false if the
	 * intersection is reduced to id comparison 
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet intersect (ObjSet set1, ObjSet set2, boolean isIdBasedCompared, boolean mergeAttributeVersions) throws OperatorException, WrongSourceException {
		
		if (set1.size()==0||set2.size()==0)
			return new ObjSet(
					set1.getLDSID()!=ObjSet.DEFAULT_LDSID?set1.getLDSID():set2.getLDSID());
		
		if (set1.getLDSID()!=set2.getLDSID()) 
			throw new OperatorException("Could not merge object sets of different logical sources.");
		
		ObjSet set = new ObjSet();
		for (Obj o : set1) {
			
			if (!set2.contains(o.getID())) continue;
			
			if (isIdBasedCompared) {
				set.addObj(o,false);
				set.addObj(set2.getObj(o.getID()),mergeAttributeVersions);
			} else if (o.hasSameAttributes(set.getObj(o.getID()))) {
				set.addObj(o,false);
				set.addObj(set2.getObj(o.getID()),mergeAttributeVersions);
			}
		}
		return set;
	}
	
	/**
	 * The method returns mapping metadata that is produced by taking the intersection 
	 * between two given mappings m1 and m2. The mapping is described by the given 
	 * attributes. The mapping statistics will be exactly computed by taking the 
	 * object correspondences into account, i.e., the intersection of object correspondences 
	 * of mappings m1 and m2 needs to be computed before.
	 * @param newMappingVersionName unique name of the resulting mapping version
	 * @param v1 mapping for which the intersection should be computed
	 * @param v1 mapping for which the intersection should be computed
	 * @param corrs object correspondences for which the resulting mapping metadata should be produced
	 * @return resulting mapping metadata
	 * @throws OperatorException
	 */
	public static MappingVersion intersect (String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet corrs) throws OperatorException {
		
		if (!v1.getMapping().equals(v2.getMapping()))
			throw new OperatorException("Could not intersect mapping versions because they belong to different mappings");
		
		int minSupport = Integer.MAX_VALUE;
		float minConfidence = Float.MAX_VALUE;
		
		for (ObjCorrespondence cor : corrs.getCollection()) {
			minSupport = Math.round(AggregationFunction.MINIMUM.aggregate(minSupport, cor.getSupport()));
			minConfidence = AggregationFunction.MINIMUM.aggregate(minConfidence, cor.getConfidence());
		}
		
		MappingVersion result = new MappingVersion.Builder(MappingVersion.DEFAULT_MAPPING_VERSION_ID,v1.getMapping())
					.name(newMappingVersionName)
					.creationDate(SimplifiedGregorianCalendar.getInstance())
					.lowestConfidence(minConfidence)
					.lowestSupport(minSupport)
					.numberOfCorrespondences(corrs.size())
					.numberOfDomainObjects(corrs.getDomainObjIds().size())
					.numberOfRangeObjects(corrs.getRangeObjIds().size())
					.build();
		
		for (SourceVersion v : v1.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v2.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v1.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		for (SourceVersion v : v2.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		
//		for (SourceVersion v : v1.getDomainInstanceSourceVersionSet().getCollection()) 

		return result;
	}
	
	/**
	 * The method computes the intersection of two given object correspondence sets.
	 * @param set1 set of object correspondences to compute the intersection with
	 * @param set2 set of object correspondences to compute the intersection with
	 * @return merged set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet intersect (ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws OperatorException, WrongSourceException {
		return intersect(set1,set2,AggregationFunction.SUM,AggregationFunction.AVERAGE);
	}
	
	/**
	 * The method computes the intersection of two given object correspondence sets.
	 * @param set1 set of object correspondences to compute the intersection with
	 * @param set2 set of object correspondences to compute the intersection with
	 * @param supportAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. support
	 * @param confidenceAggFunc aggregation function (avg, max, min) for merging intersect correspondences w.r.t. confidence
	 * @return merged set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet intersect (ObjCorrespondenceSet set1, ObjCorrespondenceSet set2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws OperatorException, WrongSourceException {
		ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		
		for (ObjCorrespondence obj : set1.getCollection()) {
			if (set2.containsCorrespondence(obj.getDomainObjID(),obj.getRangeObjID())) 
				set.addCorrespondence(mergeObjCorrespondences(
						obj,set2.getCorrespondence(obj.getDomainObjID(),obj.getRangeObjID()),supportAggFunc,confidenceAggFunc));
		}
		return set;
	}
	
	/**
	 * The method computes the intersection of two given object relationship sets.
	 * @param set1 set of object relationships to compute the intersection with
	 * @param set2 set of object relationships to compute the intersection with
	 * @param aggFunc aggregation function (avg, max, min) for merging intersect object relationships w.r.t. date from and to
	 * @return merged set of object relationships
	 */
	public static ObjRelationshipSet intersect (ObjRelationshipSet set1, ObjRelationshipSet set2, AggregationFunction aggFunc) {
		ObjRelationshipSet set = new ObjRelationshipSet();
		
		for (ObjRelationship objRel : set1.getCollection()) {
			if (set2.containsRelationship(objRel.getID())) 
			set.addObjectRelationship(mergeObjRelationship(objRel,set2.getObjectRelationship(objRel.getID()),aggFunc));
		}
		return set;
	}
	
	/**
	 * The method computes the difference between two given object sets.
	 * @param set1 object set from which the set difference is computed.
	 * @param set2 object set used to compute the set difference.
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet diff (ObjSet set1, ObjSet set2) throws OperatorException, WrongSourceException {
		return diff(set1,set2,true);
	}
	
	/**
	 * The method computes the difference between two given object sets.
	 * @param set1 object set from which the set difference is computed.
	 * @param set2 object set used to compute the set difference.
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet diff (ObjSet set1, ObjSet set2, boolean isIdBasedCompared) throws OperatorException, WrongSourceException {
		if (set2.size()==0)	return set1;
		if (set1.size()==0) return set1;
				
		if (set1.getLDSID()!=set2.getLDSID()) 
			throw new OperatorException("Could not merge object sets of different logical sources ("+set1.getLDSID()+" vs. "+set2.getLDSID()+").");
		
		ObjSet set = new ObjSet(set1.getLDSID());
		
		for (Obj o : set1) {
			if (set2.contains(o)) continue;
			
			if (isIdBasedCompared) set.addObj(o,false);
			else if (o.hasSameAttributes(set2.getObj(o.getID())))
				set.addObj(o,false);
		}
		return set;
	}
	
	/**
	 * The method returns mapping metadata that is produced by taking the difference 
	 * between two given mappings m1 and m2. The mapping is described by the given 
	 * attributes. The mapping statistics will be exactly computed by taking the 
	 * object correspondences into account, i.e., the difference of object correspondences 
	 * of mappings m1 and m2 needs to be computed before.
	 * @param newMappingVersionName unique name of the resulting mapping
	 * @param v1 mapping for which the difference should be computed
	 * @param v2 mapping which is used to compute the difference
	 * @param corrs object correspondences for which the resulting mapping metadata should be produced
	 * @return resulting mapping metadata
	 * @throws OperatorException
	 */
	public static MappingVersion diff (String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet corrs) throws OperatorException {
		
		int minSupport = Integer.MAX_VALUE;
		float minConfidence = Float.MAX_VALUE;
		
		for (ObjCorrespondence cor : corrs.getCollection()) {
			minSupport = Math.round(AggregationFunction.MINIMUM.aggregate(minSupport, cor.getSupport()));
			minConfidence = AggregationFunction.MINIMUM.aggregate(minConfidence, cor.getConfidence());
		}
		
		MappingVersion result = new MappingVersion.Builder(MappingVersion.DEFAULT_MAPPING_VERSION_ID,v1.getMapping())
					.name(newMappingVersionName)
					.creationDate(SimplifiedGregorianCalendar.getInstance())
					.lowestConfidence(minConfidence)
					.lowestSupport(minSupport)
					.numberOfCorrespondences(corrs.size())
					.numberOfDomainObjects(corrs.getDomainObjIds().size())
					.numberOfRangeObjects(corrs.getRangeObjIds().size())
					.build();
		
		for (SourceVersion v : v1.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v2.getDomainSourceVersionSet()) result.addDomainSourceVersion(v);
		for (SourceVersion v : v1.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		for (SourceVersion v : v2.getRangeSourceVersionSet()) result.addRangeSourceVersion(v);
		
//		for (SourceVersion v : v1.getDomainInstanceSourceVersionSet().getCollection()) 

		return result;
	}
	
	
	/**
	 * The method computes the difference between two given sets of object correspondences.
	 * @param set1 object correspondence set from which the set difference is computed
	 * @param set2 object correspondence set used to compute the set difference
	 * @return merged object set correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet diff (ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws OperatorException, WrongSourceException {
		ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		for (ObjCorrespondence cor : set1.getCollection()) {
			if (!set2.containsCorrespondence(cor.getDomainObjID(), cor.getRangeObjID())) set.addCorrespondence(cor);
		}
		return set;
	}
	
	/**
	 * The method merges the given sets of objects. The generated object set only consists
	 * of objects that are available in at least n given object sets. 
	 * @param o array of object sets to be merged
	 * @param nMin minimal number of object sets in which the objects should be element of 
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet majority (ObjSet[] o, int nMin) throws OperatorException, WrongSourceException {
		return majority(o,nMin,true);
	}
	
	/**
	 * The method merges the given sets of objects. The generated object set only consists
	 * of objects that are available in at least n given object sets. 
	 * @param o array of object sets to be merged
	 * @param nMin minimal number of object sets in which the objects should be element of 
	 * @return merged object set
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjSet majority (ObjSet[] o, int nMin, boolean isIdBasedCompared) throws OperatorException, WrongSourceException {
		if (o==null) throw new NullPointerException();
		
		ObjSet objs = new ObjSet();
		
		if (o.length==0) return objs;
		
		if (nMin<0 || nMin>o.length) 
			throw new OperatorException("The specified minimal number is out of range.");
		
		if (nMin<2) {
			for (int i=0;i<o.length;i++) objs = SetOperators.union(objs,o[i],true);
			return objs;
		}
		if (nMin==o.length) {
			objs = o[0];
			for (int i=1;i<o.length;i++) objs = SetOperators.intersect(objs,o[i],isIdBasedCompared,true);
			return objs;
		}
		
		ObjSet testObjs = o[0];
		int cnt;
		for (int i=1;i<o.length;i++) testObjs = SetOperators.union(testObjs,o[i],true);
		for (int objId : testObjs.getIDSet()) {
			cnt=0;
			for (int i=0;i<o.length;i++) {
				if (o[i].contains(objId)) cnt++;
				else continue;
				if (cnt!=nMin) continue;
				objs.addObj(testObjs.getObj(objId),true);
				break;
			}
		}
		
		return objs;
	}
	
	/**
	 * The method returns mapping metadata that is produced by taking the majority 
	 * of a given mapping set. The resulting mapping is described by the given 
	 * attributes. The mapping statistics will be exactly computed by taking the 
	 * object correspondences into account, i.e., the majority of object correspondences 
	 * of the given mapping set needs to be computed before.
	 * @param newMappingName unique name of the resulting mapping
	 * @param maps mapping set for which the majority should be computed
	 * @param nMin minimal number of an object it should be present in the given mapping set
	 * @param mapAtts attributes of the resulting mapping
	 * @param corrs object correspondences for which the resulting mapping metadata should be produced
	 * @return resulting mapping metadata
	 * @throws OperatorException
	 */
	public static MappingVersion majority (String newMappingVersionName, MappingVersionSet set, int nMin, ObjCorrespondenceSet corrs) throws OperatorException {
		
		Mapping m = null;
		
		for (MappingVersion v : set)
			if (m==null) m = v.getMapping();
			else if (!m.equals(v.getMapping()))
				throw new OperatorException("Could not compute the majority of mapping versions because they belong to different mappings.");
		
		int minSupport = Integer.MAX_VALUE;
		float minConfidence = Float.MAX_VALUE;
		
		for (ObjCorrespondence cor : corrs.getCollection()) {
			minSupport = Math.round(AggregationFunction.MINIMUM.aggregate(minSupport, cor.getSupport()));
			minConfidence = AggregationFunction.MINIMUM.aggregate(minConfidence, cor.getConfidence());
		}
		
		MappingVersion result = new MappingVersion.Builder(MappingVersion.DEFAULT_MAPPING_VERSION_ID,m)
					.name(newMappingVersionName)
					.creationDate(SimplifiedGregorianCalendar.getInstance())
					.lowestConfidence(minConfidence)
					.lowestSupport(minSupport)
					.numberOfCorrespondences(corrs.size())
					.numberOfDomainObjects(corrs.getDomainObjIds().size())
					.numberOfRangeObjects(corrs.getRangeObjIds().size())
					.build();
		
		for (MappingVersion mapV : set) {
			for (SourceVersion sourceV : mapV.getDomainSourceVersionSet()) result.addDomainSourceVersion(sourceV);
			for (SourceVersion sourceV : mapV.getRangeSourceVersionSet()) result.addRangeSourceVersion(sourceV);
		}
//		for (SourceVersion v : v1.getDomainInstanceSourceVersionSet().getCollection()) 

		return result;
	}
	
	public static ObjCorrespondenceSet majority (ObjCorrespondenceSet[] o, int nMin) throws OperatorException, WrongSourceException {
		return majority(o,nMin,AggregationFunction.SUM,AggregationFunction.AVERAGE);
	}
	
	/**
	 * The method merges the given sets of object correspondences. The generated set of object 
	 * correspondences only contains such object correspondences which are available in at 
	 * least n given sets.
	 * @param o array of sets of object correspondences to be merged
	 * @param nMin minimal number of sets to which the object correspondences should belong 
	 * @return merged set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet majority (ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws OperatorException, WrongSourceException {
		/*ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		
		if (nMin<0 || nMin>o.length) 
			throw new OperatorException("The specified minimal number is out of range.");
		
		if (o.length==0) return set;
		if (nMin<2) {
			for (int i=0;i<o.length;i++) set = SetOperators.union(set,o[i],supportAggFunc,confidenceAggFunc);
			return set;
		}
		if (nMin==o.length) {
			set = o[0];
			for (int i=1;i<o.length;i++) set = SetOperators.intersect(set,o[i],supportAggFunc,confidenceAggFunc);
			return set;
		}
		
		ObjCorrespondenceSet testObjs = o[0];
		int cnt;
		for (int i=1;i<o.length;i++) testObjs = SetOperators.union(testObjs,o[i],supportAggFunc,confidenceAggFunc);
		for (ObjCorrespondence obj : testObjs.getCollection()) {
			cnt=0;
			for (int i=0;i<o.length;i++) {
				if (o[i].containsCorrespondence(obj.getDomainObjID(),obj.getRangeObjID())) cnt++;
				else continue;
				if (cnt!=nMin) continue;
				set.addCorrespondence(obj);
				break;
			}
		}
		*/
		return SetOperators.majority(sets, nMin, supportAggFunc, confidenceAggFunc, false);
	}
	
	/**
	 * The method merges the given sets of object correspondences. The generated set of object 
	 * correspondences only contains such object correspondences which are available in at 
	 * least n given sets.
	 * @param o array of sets of object correspondences to be merged
	 * @param nMin minimal number of sets to which the object correspondences should belong 
	 * @return merged set of object correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet majority (ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws OperatorException, WrongSourceException {
		ObjCorrespondenceSet resultSet = new ObjCorrespondenceSet();
		HashMap<String, List<Float>> unifiedCorrsSim = new HashMap<String, List<Float>>();
		HashMap<String, List<Integer>> unifiedCorrsSupport = new HashMap<String, List<Integer>>();
		HashMap<String, ObjCorrespondence> unifiedCorrs = new HashMap<String, ObjCorrespondence>();
		
		for (ObjCorrespondenceSet set : sets) {
			for (ObjCorrespondence corr : set.getCollection()) {
				String corrAsString = corr.getDomainObjID()+"-"+corr.getRangeObjID();
				
				//Correspondences
				if (!unifiedCorrs.containsKey(corrAsString)) {
					unifiedCorrs.put(corrAsString, corr);
				}
	
				//Similarities
				if (!unifiedCorrsSim.containsKey(corrAsString)) {
					unifiedCorrsSim.put(corrAsString, new Vector<Float>());
				}
				List<Float> currentSimilarites = unifiedCorrsSim.get(corrAsString);
				currentSimilarites.add(corr.getConfidence());
				unifiedCorrsSim.put(corrAsString, currentSimilarites);
				//Supports
				if (!unifiedCorrsSupport.containsKey(corrAsString)) {
					unifiedCorrsSupport.put(corrAsString, new Vector<Integer>());
				}
				List<Integer> currentSupports = unifiedCorrsSupport.get(corrAsString);
				currentSupports.add(corr.getSupport());
				unifiedCorrsSupport.put(corrAsString, currentSupports);
			}
		}
		
		for (String corrAsString : unifiedCorrs.keySet()) {
			ObjCorrespondence corr = unifiedCorrs.get(corrAsString);
			List<Float> similarities = unifiedCorrsSim.get(corrAsString);
			List<Integer> supports = unifiedCorrsSupport.get(corrAsString);
			if (similarities.size()>=nMin) {
				if (strictMode) {
				while (similarities.size()<sets.length) {
					similarities.add(0.0F);
				}
				while (supports.size()<sets.length) {
					supports.add(0);
				}
			}
			
			ObjCorrespondence c = new ObjCorrespondence.Builder(
					corr.getDomainObjID(),corr.getDomainLDSID(),
					corr.getRangeObjID(),corr.getRangeLDSID())
						.domainAccessionNumber(corr.getDomainAccessionNumber())
						.rangeAccessionNumber(corr.getRangeAccessionNumber())
						.confidence(confidenceAggFunc.aggregateFloatList(similarities))
						.support(Math.round(supportAggFunc.aggregateIntegerList(supports)))
						.numberOfUserChecks(corr.getNumberOfChecks())
						.typeName(corr.getCorrespondenceTypeName())
						.statusName(corr.getCorrespondenceStatusName())
						.typeDescription(corr.getCorrespondenceTypeDescription())
						.build();
			
			resultSet.addCorrespondence(c);
			}
			
		}
		return resultSet;
	}
	
	public static ObjCorrespondenceSet setAttribute(ObjCorrespondenceSet set, CorrespondenceAttributes corrAtt, String value) throws OperatorException {
		
		switch (corrAtt) {
			case CONFIDENCE:
				float confidence = Float.parseFloat(value);
				for (ObjCorrespondence cor : set.getCollection())
					cor.resetConfidence(confidence);
				break;
			case SUPPORT:
				int support = Integer.parseInt(value);
				for (ObjCorrespondence cor : set.getCollection())
					cor.resetSupport(support);
				break;
			case TYPE:
				for (ObjCorrespondence cor : set.getCollection())
					cor.resetType(value);
				break;
			default: throw new OperatorException("Could not resolve a valid coresspondence attribute.");
		}
		
		return set;
	}
	
	private static ObjCorrespondence mergeObjCorrespondences(ObjCorrespondence obj1, ObjCorrespondence obj2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws OperatorException {
		if (!(obj1.getDomainLDSID()==obj2.getDomainLDSID() && obj1.getRangeLDSID()==obj2.getRangeLDSID() &&
			obj1.getDomainObjID()==obj2.getDomainObjID() && obj1.getRangeObjID()==obj2.getRangeObjID()))
			throw new OperatorException("Could not merge inequal object correspondences.");
		
		ObjCorrespondence c = new ObjCorrespondence.Builder(
				obj1.getDomainObjID(),obj1.getDomainLDSID(),
				obj1.getRangeObjID(),obj1.getRangeLDSID())
					.domainAccessionNumber(obj1.getDomainAccessionNumber())
					.rangeAccessionNumber(obj1.getRangeAccessionNumber())
					.confidence(confidenceAggFunc.aggregate(obj1.getConfidence(), obj2.getConfidence()))
					.support(Math.round(supportAggFunc.aggregate(obj1.getSupport(), obj2.getSupport())))
					.numberOfUserChecks(Math.round(AggregationFunction.MAXIMUM.aggregate(obj1.getNumberOfChecks(),obj2.getNumberOfChecks())))
					.typeName(obj1.getCorrespondenceTypeName())
					.statusName(obj1.getCorrespondenceStatusName())
					.typeDescription(obj1.getCorrespondenceTypeDescription())
					.build();
		
		for (int id : obj1.getDomainSourceVersionIDSet()) c.addDomainSourceVersionID(id);
		for (int id : obj2.getDomainSourceVersionIDSet()) c.addDomainSourceVersionID(id);
		for (int id : obj1.getRangeSourceVersionIDSet())  c.addRangeSourceVersionID(id);
		for (int id : obj2.getRangeSourceVersionIDSet())  c.addRangeSourceVersionID(id);
		
		return c;
	}
	
	private static ObjRelationship mergeObjRelationship(ObjRelationship objRel1, ObjRelationship objRel2, AggregationFunction aggFunc) {
		SimplifiedGregorianCalendar fromDate = objRel1.getFromDate().equals(objRel2.getFromDate())?
				objRel1.getFromDate():CacheManager.getInstance().getDateCache().getCalendar(
					aggFunc.aggregate(objRel1.getFromDate(),objRel2.getFromDate()).getTimeInMillis());
				
		SimplifiedGregorianCalendar toDate   = objRel1.getToDate().equals(objRel2.getToDate())?
				objRel1.getToDate():CacheManager.getInstance().getDateCache().getCalendar(
					aggFunc.aggregate(objRel1.getToDate(),objRel2.getToDate()).getTimeInMillis());
				
		ObjRelationship result = new ObjRelationship.Builder(objRel1.getFromID(),objRel1.getToID())
									.fromDate(fromDate)
									.toDate(toDate)
									.isDirected(objRel1.isDirected() && objRel2.isDirected())
									.build();
		
		result.setRelationshipType(objRel1.getRelationshipType().equals(objRel2.getRelationshipType())?
				objRel1.getRelationshipType():"N/A (no merge possible)");
	
		return result;
	}
}
