package org.gomma.operator.fusion;

import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.model.source.Source;
import org.gomma.util.classfinder.ClassChecker;
import org.gomma.util.classfinder.ClassPathComponentList;

public class ObjFusionFactory {

	private ObjFusionFactory() {}
	
	public static ObjFusion getObjFusioner(Source s) throws OperatorException {
				
		try {

			ClassChecker classChecker = new ClassChecker(new ClassPathComponentList());
			List<String> classNameList = classChecker.getClassWithSuperClass(ObjFusion.class.getPackage().getName(),ObjFusion.class.getName());
		
			for (String className : classNameList) {
				Class<?> c = Class.forName(className);
				ObjFusion fuser = (ObjFusion)c.newInstance();
				
				if (fuser.isUsable(s)) return fuser;
			}
			throw new OperatorException("Could not resolve an existing fuse implementation for the data source '"+s.getName()+"'.");
		} catch (Exception e) {
			throw new OperatorException(e.getMessage());
		}
	}
}
