package org.gomma.operator.fusion;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.operator.SetOperators;

public class GeneOntologyObjFusion implements ObjFusion {

	private static final String[] PDS_NAMES = {"GO", "GeneOntology", "Gene Ontology"};
	
	public boolean isUsable(Source s) {
		boolean isUsable = false;
		
		for (String pdsName : PDS_NAMES) {
			if (pdsName.equalsIgnoreCase(s.getPhysicalSourceName())) {
				isUsable=true;
				break;
			}
		}
		
		return isUsable;
	}
	
	public ObjSet fuseObjectSets(ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		ObjSet fusedObjSet = new ObjSet(objs1.getLDSID());
		ObjSet deletedObjSet = SetOperators.diff(objs1,objs2); 
		
		AttributeSet attSet;
		
		for (Obj deletedObj : deletedObjSet) {
			for (Obj obj2 : objs2) {

				if (!obj2.getAttributeValues().getAttributeSet().contains("synonym")) continue;

				attSet = obj2.getAttributeValues().getAttributeSet().getAttributeSet("synonym");
				
				for (Attribute att : attSet) {
					if (!att.getScope().equalsIgnoreCase("alt_id")) continue;
					
					for (AttributeValueVersion avv : obj2.getAttributeValues(att)) {
						if (avv.toString().contains(deletedObj.getAccessionNumber()))
							fusedObjSet.addElement(obj2);
					}
				}
			}
		}
		
		return fusedObjSet;
	}
	
	
	
	
//	ObjSet tmpObjs = this.deletedObjectsInternal(objs1,objs2);
//	ObjSet result = new ObjSet();
//	for (int key : tmpObjs.getIDSet()) {
//		Obj tmpObj = tmpObjs.getObj(key);
//		for (int key2 : objs2.getIDSet()) {
//			Obj tmpObj2 = objs2.getObj(key2);
//			Set<Integer> sourceVersionKeys = tmpObj2.getSourceVersionSet().getIDSet();
//			int keyNewVersion = sourceVersionKeys.iterator().next();
//			if (tmpObj2.getAttributeValues(keyNewVersion).getAttributeSet().contains("synonym")) {
//				for (Attribute att : tmpObj2.getAttributeValues(keyNewVersion).getAttributeSet().getAttributeSet("synonym")) {
//					AttributeValueVersionSet tmpObj2AttNewVersion = tmpObj2.getAttributeValues(keyNewVersion,att);
//					for (AttributeValueVersion avv : tmpObj2AttNewVersion) {
//						if (tmpObj2Att.valueString.equals(tmpObj.getAccessionNumber())&&tmpObj2Att.attributeScope.equals("alt_id")) {
//							result.addObj(tmpObj);
//							break;
//						}
//					}
//				}
//				
//				
//			}
//		}
//	}
//	return result;

}
