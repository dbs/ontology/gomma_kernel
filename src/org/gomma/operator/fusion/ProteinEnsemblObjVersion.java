package org.gomma.operator.fusion;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;

public class ProteinEnsemblObjVersion implements ObjFusion {

	private static final String LDS_NAME = "Protein@Ensembl";

	public boolean isUsable(Source s) {
		if (s.getName().substring(0,LDS_NAME.length()).equalsIgnoreCase(LDS_NAME)) return true;
		return false;
	}
	
	public ObjSet fuseObjectSets(ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		
		return new ObjSet(objs1.getLDSID());
	}

}
