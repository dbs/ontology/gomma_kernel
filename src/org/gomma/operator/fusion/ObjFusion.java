package org.gomma.operator.fusion;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;

public interface ObjFusion {
	
	public boolean isUsable(Source s);
	
	public ObjSet fuseObjectSets(ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException;
}
