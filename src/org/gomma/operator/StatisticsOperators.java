package org.gomma.operator;

import java.util.Calendar;
import java.util.Map;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public final class StatisticsOperators {
	
	public static final int MONTH_PERIOD = 1;
	public static final int QUARTAL_PERIOD = 2;
	public static final int HALFYEAR_PERIOD = 3;
	public static final int YEAR_PERIOD = 4;
	
	public static final int ADD_ACTION = 10;
	public static final int DEL_ACTION = 11;
	public static final int TOOBS_ACTION = 12;
	public static final int FUSE_ACTION = 13;
	
	public static final int OBJECTS = 20;
	public static final int RELATIONSHIPS = 21;
	
	
	
	private static volatile StatisticsOperators singleton = null;
	
	private StatisticsOperators() {}
	
	public static StatisticsOperators getInstance() {
		if (singleton == null) {
			synchronized (StatisticsOperators.class) {
				if (singleton == null)
					singleton = new StatisticsOperators();
			}
		}
		return singleton;
	}
	
	public float getAverageEvolutionStats(SourceVersionSet versions, int action, int period, Calendar start, Calendar end) throws RepositoryException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI()
					.getAverageEvolutionStats(versions,action,period,
							new SimplifiedGregorianCalendar(start),new SimplifiedGregorianCalendar(end));
	}
	
	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet versions, int action, Calendar start, Calendar end) throws RepositoryException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI()
					.getAllEvolutionStats(versions,action,
							new SimplifiedGregorianCalendar(start),new SimplifiedGregorianCalendar(end));
	}
	
	public Map<Integer,Integer> getLDSVersionStats(SourceVersionSet versions, int statType) throws GommaException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI()
					.getLDSVersionStats(versions,statType);
	}
	
	public float getCoverage(ObjSet objs) throws RepositoryException {
		return (float) (objs.size() / (float)APIFactory.getInstance().getEvolutionStatisticsAPI()
					.getNumberOfObjects(objs));
	}
}
