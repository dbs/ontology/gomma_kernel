/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes: 
 * 2008-03-25 - Source version adaption (T.Kirsten)
 * 
 **/

package org.gomma.operator;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.IDObject;
import org.gomma.model.IDObjectSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.SimilarityFunctionImpl;
import org.gomma.util.math.SimpleOperations;

/**
 * 
 * This class MappingOperators implements different operators for manipulating given mappings. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingOperators {
	
	
	public enum SupportFunctions {
		
		AVERAGE {
			public int compute(int domainSupportValue, int rangeSupportValue) {
				return Math.round(AggregationFunction.AVERAGE.aggregate(domainSupportValue,rangeSupportValue));
			}
			public int compute(List<Integer> supportValueList) {
				return Math.round(AggregationFunction.AVERAGE.aggregateIntegerList(supportValueList));
			}
		},
		MAXIMUM {
			public int compute(int domainSupportValue, int rangeSupportValue) {
				return Math.round(AggregationFunction.MAXIMUM.aggregate(domainSupportValue,rangeSupportValue));
			}
			public int compute(List<Integer> supportValueList) {
				return Math.round(AggregationFunction.MAXIMUM.aggregateIntegerList(supportValueList));
			}
		},
		MINIMUM {
			public int compute(int domainSupportValue, int rangeSupportValue) {
				return Math.round(AggregationFunction.MINIMUM.aggregate(domainSupportValue,rangeSupportValue));
			}
			public int compute(List<Integer> supportValueList) {
				return Math.round(AggregationFunction.MINIMUM.aggregateIntegerList(supportValueList));
			}
		};
		
		public abstract int compute(int domainSupportValue, int rangeSupportValue);
		public abstract int compute(List<Integer> supportValueList);
	}
	
	public enum ConfidenceFunctions {
		
		AVERAGE {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return AggregationFunction.AVERAGE.aggregate(domainConfidenceValue,rangeConfidenceValue);
			}
			public float compute(List<Float> confidenceValueList) {
				return AggregationFunction.AVERAGE.aggregateFloatList(confidenceValueList);
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return 0F;
			}
		},
		MAXIMUM {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return AggregationFunction.MAXIMUM.aggregate(domainConfidenceValue,rangeConfidenceValue);
			}
			public float compute(List<Float> confidenceValueList) {
				return AggregationFunction.MAXIMUM.aggregateFloatList(confidenceValueList);
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return 0F;
			}
		},
		MINIMUM {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return AggregationFunction.MINIMUM.aggregate(domainConfidenceValue,rangeConfidenceValue);
			}
			public float compute(List<Float> confidenceValueList) {
				return AggregationFunction.MINIMUM.aggregateFloatList(confidenceValueList);
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return 0F;
			}
		},
		DICE {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return 0;
			}
			public float compute(List<Float> confidenceValueList) {
				return 0;
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return SimilarityFunctionImpl.dice(nSharedObjs, nDomainCorrs, nRangeCorrs);
			}
		},
		JACCARD {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return 0;
			}
			public float compute(List<Float> confidenceValueList) {
				return 0;
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return SimilarityFunctionImpl.jaccard(nSharedObjs, nDomainCorrs, nRangeCorrs);
			}
		},
		COSINE {
			public float compute(float domainConfidenceValue, float rangeConfidenceValue) {
				return 0;
			}
			public float compute(List<Float> confidenceValueList) {
				return 0;
			}
			public float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs) {
				return SimilarityFunctionImpl.cosine(nSharedObjs, nDomainCorrs, nRangeCorrs);
			}
		};
		
		public abstract float compute(float domainConfidenceValue, float rangeConfidenceValue);
		public abstract float compute(List<Float> confidenceValueList);
		public abstract float compute(int nSharedObjs, int nDomainCorrs, int nRangeCorrs);
	}

	/**
	 * The item represents the singleton instance of the operator class.
	 */
	private static volatile MappingOperators singleton = null;
	
	/**
	 * The constructor initializes the class MappingOperators.
	 */
	private MappingOperators() {}
	
	/**
	 * The static method returns an instance of the operator class.
	 * @return operator class instance
	 */
	public static MappingOperators getInstance() {
		if (singleton == null) {
			synchronized (MappingOperators.class) {
				if (singleton == null)
					singleton = new MappingOperators();
			}
		}
		return singleton;
	}
	
	/**
	 * The method compose composes two given mappings, called domain and range.
	 * @param domain domain mapping (set of object correspondences)
	 * @param range range mapping (set of object correspondences)
	 * @return composed mapping (set of object correspondences)
	 * @see org.gomma.model.mapping.ObjCorrespondenceSet
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet compose (ObjCorrespondenceSet domain, ObjCorrespondenceSet range) throws OperatorException, WrongSourceException {
		return compose(domain,range,
				SupportFunctions.AVERAGE,SupportFunctions.AVERAGE,
				ConfidenceFunctions.AVERAGE,ConfidenceFunctions.AVERAGE,
				true,0);
	}
	
	/**
	 * The method compose composes two given mappings, called domain and range. The 
	 * correspondence attributes, such as support and confidence are set by the 
	 * specified compose function. 
	 * @param domain domain mapping (set of object correspondences)
	 * @param range range mapping (set of object correspondences)
	 * @param verticalSupportFunction function for aggregating multiple support values
	 * @param horizontalSupportFunction function for aggregating multiple support values
	 * @param verticalConfidenceFunction function for aggregating multiple confidence values
	 * @param horizontalConfidenceFunction function for aggregating multiple confidence values
	 * @param horizontalBeforeVertical determines the aggregation strategy; true if the values
	 * of the two connecting correspondences should be aggregated before the all values
	 * which are computed for every join path are aggregated; false if first all values of 
	 * the join paths are aggregated before the domain and range values are merged
	 * @param simThreshold threshold of the corresponding similarity function 
	 * @return composed mapping (set of object correspondences)
	 * @see org.gomma.model.mapping.ObjCorrespondenceSet
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet compose (ObjCorrespondenceSet domainSet, ObjCorrespondenceSet rangeSet, 
			SupportFunctions verticalSupportFunction, SupportFunctions horizontalSupportFunction, 
			ConfidenceFunctions verticalConfidenceFunction, ConfidenceFunctions horizontalConfidenceFunction, 
			boolean horizontalBeforeVertical, float simThreshold) throws OperatorException, WrongSourceException {
		
		if (domainSet.getRangeLDSID() != rangeSet.getDomainLDSID())
			throw new OperatorException("Could not resolve the same logical sources to execute the compose operation.");

		/* pre-processing: assume there are two mappings a-b and b-c, 
		 * then find all match partners a-c and store shared objects b */
		Map<Integer,Map<Integer,Set<Integer>>> matchPartnersAC = new Int2ObjectOpenHashMap<Map<Integer,Set<Integer>>>();
		Map<Integer,Set<Integer>> sharedObjIdsC;
		Set<Integer> sharedObjIds;
		
		for (int domainSetRangeObjID : domainSet.getRangeObjIds()) {
			if (!rangeSet.getDomainObjIds().contains(domainSetRangeObjID)) continue;
			
			for (int domainSetDomainObjID : domainSet.getCorrespondingDomainObjIDSet(domainSetRangeObjID)) {
			
				if (matchPartnersAC.containsKey(domainSetDomainObjID)) {
					sharedObjIdsC=matchPartnersAC.get(domainSetDomainObjID);
					//System.out.println(sharedObjIdsC.size());
				}else{
					sharedObjIdsC = new Int2ObjectOpenHashMap<Set<Integer>>();
				}
				
				for (int rangeSetRangeObjID : rangeSet.getCorrespondingRangeObjIDSet(domainSetRangeObjID)) {
				
					if (sharedObjIdsC.containsKey(rangeSetRangeObjID)) sharedObjIds = sharedObjIdsC.get(rangeSetRangeObjID);
					else sharedObjIds = new IntOpenHashSet();
					
					sharedObjIds.add(domainSetRangeObjID);
					sharedObjIdsC.put(rangeSetRangeObjID,sharedObjIds);
				}
				matchPartnersAC.put(domainSetDomainObjID,sharedObjIdsC);
			}
		}
		
		
		ObjCorrespondenceSet resultSet = new ObjCorrespondenceSet();
		ObjCorrespondenceSet subDomainSet, subRangeSet;
		ObjCorrespondence corr;
		int support;
		float confidence;
		
		for (int a : matchPartnersAC.keySet()) {
			sharedObjIdsC = matchPartnersAC.get(a);
			subDomainSet = domainSet.getCorrespondenceSet(a,true);
			//System.out.println(subDomainSet.size());
			
			for (int c : sharedObjIdsC.keySet()) {
				subRangeSet = rangeSet.getCorrespondenceSet(c,false);
				//System.out.println(subRangeSet.size());
				support = computeSupportValue(subDomainSet,subRangeSet,verticalSupportFunction,horizontalSupportFunction,horizontalBeforeVertical);
				confidence=0;
				
				if (verticalConfidenceFunction==horizontalConfidenceFunction && (
						verticalConfidenceFunction==ConfidenceFunctions.COSINE || verticalConfidenceFunction==ConfidenceFunctions.DICE ||
						verticalConfidenceFunction==ConfidenceFunctions.JACCARD)) {
				
					confidence = verticalConfidenceFunction.compute(sharedObjIdsC.get(c).size(),subDomainSet.size(),subRangeSet.size());
					
				} else confidence=computeConfidenceValue(subDomainSet,subRangeSet,verticalConfidenceFunction,horizontalConfidenceFunction,horizontalBeforeVertical);
				int sharedConceptID = subDomainSet.getRangeObjIds().iterator().next();
				if (confidence>=simThreshold) {
					corr = new ObjCorrespondence.Builder(a, domainSet.getDomainLDSID(), c, rangeSet.getRangeLDSID())
							//.domainAccessionNumber(domainSet.getCorrespondence(a, sharedConceptID).getDomainAccessionNumber())
							//.rangeAccessionNumber(rangeSet.getCorrespondence(sharedConceptID, c).getRangeAccessionNumber())
							.confidence(confidence)
							.support(support)
							.numberOfUserChecks(0).build();
					resultSet.addCorrespondence(corr);
				}
			}	
		}
		return resultSet;
	}
	
	public static ObjCorrespondenceSet multiCompose (List<ObjCorrespondenceSet> corrSetList,  
			ConfidenceFunctions verticalConfidenceFunction, ConfidenceFunctions horizontalConfidenceFunction, 
		    float simThreshold) throws OperatorException, WrongSourceException {
		
		//if (corrSetList.size()==2) {
		//	return compose(corrSetList.get(0), corrSetList.get(1), null, null, verticalConfidenceFunction, horizontalConfidenceFunction, true, simThreshold);
		//}
		
		//if (domainSet.getRangeLDSID() != rangeSet.getDomainLDSID())
		//	throw new OperatorException("Could not resolve the same logical sources to execute the compose operation.");

		/* pre-processing: assume there are two mappings a-b and b-c, 
		 * then find all match partners a-c and store shared objects b */
		Map<Integer,Map<Integer,Set<List<Integer>>>> matchPartnersAC = new Int2ObjectOpenHashMap<Map<Integer,Set<List<Integer>>>>();
		Map<Integer,Set<List<Integer>>> sharedObjIdsC;
		//Set<List<Integer>> sharedObjIds;
		
		//Initiale Befüllung
		for (ObjCorrespondence objCorr : corrSetList.get(0).getCollection()) {
			if (matchPartnersAC.containsKey(objCorr.getDomainObjID())) {
				sharedObjIdsC = matchPartnersAC.get(objCorr.getDomainObjID());
			} else {
				sharedObjIdsC = new Int2ObjectOpenHashMap<Set<List<Integer>>>();
			}
			sharedObjIdsC.put(objCorr.getRangeObjID(), new HashSet<List<Integer>>());
			matchPartnersAC.put(objCorr.getDomainObjID(), sharedObjIdsC);
		}
		Map<Integer,Map<Integer,Set<List<Integer>>>> result = matchPartnersAC;
		for (int i=1;i<corrSetList.size();i++) {
			result = computeSimpleComposeStep(result, corrSetList.get(i));
		}
		return buildObjCorrespondenceSetForMultiCompose(result, corrSetList, verticalConfidenceFunction, horizontalConfidenceFunction, simThreshold);
	}
	
	private static ObjCorrespondenceSet buildObjCorrespondenceSetForMultiCompose(Map<Integer,Map<Integer,Set<List<Integer>>>> traverses, 
			List<ObjCorrespondenceSet> corrSetList, ConfidenceFunctions verticalConfidenceFunction, ConfidenceFunctions horizontalConfidenceFunction, 
			float simThreshold) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		for (int domainSetObjID : traverses.keySet()) {
			Map<Integer, Set<List<Integer>>> rangeSet = traverses.get(domainSetObjID);
			for (int rangeSetObjID : rangeSet.keySet()) {
				Set<List<Integer>> paths = rangeSet.get(rangeSetObjID);
				List<Float> aggPaths = new Vector<Float>();
				for (List<Integer> path : paths) {
					List<Float> horizontalAggFloats = new Vector<Float>();
					float firstConf = corrSetList.get(0).getCorrespondence(domainSetObjID, path.get(0)).getConfidence();
					horizontalAggFloats.add(firstConf);
					for (int i=1;i<=path.size()-1;i++) {
						int fromID = path.get(i-1);
						int toID = path.get(i);
						float conf = corrSetList.get(i).getCorrespondence(fromID, toID).getConfidence();
						horizontalAggFloats.add(conf);
					}
					float lastConf = corrSetList.get(corrSetList.size()-1).getCorrespondence(path.get(path.size()-1),rangeSetObjID).getConfidence();
					horizontalAggFloats.add(lastConf);
					aggPaths.add(horizontalConfidenceFunction.compute(horizontalAggFloats));
				}
				float finalConf = verticalConfidenceFunction.compute(aggPaths);
				if (finalConf>=simThreshold) {
					ObjCorrespondence corr = new  ObjCorrespondence.Builder(domainSetObjID,corrSetList.get(0).getDomainLDSID(),rangeSetObjID,corrSetList.get(corrSetList.size()-1).getRangeLDSID())
						.confidence(finalConf)
						.support(1)
						.numberOfUserChecks(0)
						.build();
					result.addCorrespondence(corr);
				}
			}
		}
		return result;
	}
	
	private static Map<Integer,Map<Integer,Set<List<Integer>>>> computeSimpleComposeStep(Map<Integer,Map<Integer,Set<List<Integer>>>> domainSet, ObjCorrespondenceSet rangeSet) {
		Map<Integer,Map<Integer,Set<List<Integer>>>> result = new Int2ObjectOpenHashMap<Map<Integer,Set<List<Integer>>>>();
		Map<Integer,Set<List<Integer>>> targetsAndWays;
		Set<List<Integer>> ways;
		
		for (int domainSetDomainObjID : domainSet.keySet()) {
			Map<Integer,Set<List<Integer>>> domainSetRangeObjIDs = domainSet.get(domainSetDomainObjID);
			for (int domainSetRangeObjID : domainSetRangeObjIDs.keySet()) {
				if (!rangeSet.getDomainObjIds().contains(domainSetRangeObjID)) continue;
				if (result.containsKey(domainSetDomainObjID)) {
					targetsAndWays = result.get(domainSetDomainObjID);
				} else {
					targetsAndWays = new Int2ObjectOpenHashMap<Set<List<Integer>>>();
				}
				Set<List<Integer>> oldWays = domainSetRangeObjIDs.get(domainSetRangeObjID);
				for (int rangeSetRangeObjID : rangeSet.getCorrespondingRangeObjIDSet(domainSetRangeObjID)) {
					if (targetsAndWays.containsKey(rangeSetRangeObjID)) {
						ways = targetsAndWays.get(rangeSetRangeObjID);
					} else {
						ways = new HashSet<List<Integer>>();
					}
					//Hole bisherige Wege aus domainSet und erweitere diese
					for (List<Integer> oldWay : oldWays) {
						List<Integer> newWay = new Vector<Integer>(oldWay);
						newWay.add(domainSetRangeObjID);
						ways.add(newWay);
					}
					if (oldWays.size()==0) {
						List<Integer> newWay = new Vector<Integer>();
						newWay.add(domainSetRangeObjID);
						ways.add(newWay);
					}
					targetsAndWays.put(rangeSetRangeObjID, ways);
				}
				result.put(domainSetDomainObjID, targetsAndWays);
			}
		}
		return result;
	}
	
	public static ObjCorrespondenceSet hierarchicalCompose(ObjCorrespondenceSet domainMap, ObjCorrespondenceSet rangeMap, 
														 SourceVersionStructure rangeMapDomain,SourceVersionStructure rangeMapRange) 
														 throws OperatorException, WrongSourceException{
		//domainMap = linkes mapping des hierarchieCompose = bleibt unveraendert
		//rangeMap = rechtes mapping des hierarchieCompose --> wird um Korrespondenzen zu eltern und kindern der gematchten konzepte im hub erweitert
		//rangeMapDomain = hubOntology = domain des range (=rechten) mappings
		//rangeMapRange = range des range (=rechten) mappings
		
		ObjCorrespondenceSet parentCorrSet = new ObjCorrespondenceSet();
		ObjCorrespondenceSet childCorrSet = new ObjCorrespondenceSet();
		/*
		System.out.println(domainMap.getDomainLDSID());
		System.out.println(rangeMap.getDomainLDSID());
		System.out.println(rangeMapDomain.getSourceVersion());
		System.out.println(rangeMapRange.getSourceVersion());*/
		
		for(IDObject domID:rangeMap.getDomainIDObjectSet()){ //DomainIds von rangeMapping (Achtung domain und range fuer mapping UND auch fuer compose(domainMap,rangeMap) genutzt!
			
			Obj o = rangeMapDomain.getObject(domID.getID());
			ObjSet predecessors = rangeMapDomain.getPredecessorObjects(o);	//Eltern?
			ObjSet successors = rangeMapDomain.getSuccessorObjects(o);		//Kinder?
			
			for(Integer rangeMapRangeObjectID:rangeMap.getCorrespondingRangeObjIDSet(o.getID())){
				//alle Konzepte in RangeMap die zu aktuellem Hub-Onjekt o matchen
				
				for(Obj p:predecessors){ //Eltern von o				
					 ObjCorrespondence corr = new  ObjCorrespondence.Builder(p.getID(),p.getLDSID(),rangeMapRangeObjectID,rangeMapRange.getObject(rangeMapRangeObjectID).getLDSID())
											 						.confidence(rangeMap.getCorrespondence( o.getID(),rangeMapRangeObjectID).getConfidence())
																	.support(rangeMap.getCorrespondence( o.getID(),rangeMapRangeObjectID).getSupport())
																	.numberOfUserChecks(rangeMap.getCorrespondence( o.getID(),rangeMapRangeObjectID).getNumberOfChecks())
											 						.build();
					 parentCorrSet.addCorrespondence(corr);
				}
			
				for(Obj c:successors){ //Kinder von o					
					 ObjCorrespondence corr = new  ObjCorrespondence.Builder(c.getID(),c.getLDSID(),rangeMapRangeObjectID,rangeMapRange.getObject(rangeMapRangeObjectID).getLDSID())
											 						.confidence(rangeMap.getCorrespondence( o.getID(),rangeMapRangeObjectID).getConfidence())
																	.support(rangeMap.getCorrespondence(o.getID(),rangeMapRangeObjectID).getSupport())
																	.numberOfUserChecks(rangeMap.getCorrespondence(o.getID(),rangeMapRangeObjectID).getNumberOfChecks())
											 						.build();
					 childCorrSet.addCorrespondence(corr);
				}
				
			}
		}
			
		ObjCorrespondenceSet extendedRangeSet = SetOperators.union(rangeMap,SetOperators.union(childCorrSet,parentCorrSet));
		ObjCorrespondenceSet resultSet		  = compose(domainMap,extendedRangeSet,SupportFunctions.AVERAGE,SupportFunctions.AVERAGE,ConfidenceFunctions.AVERAGE,ConfidenceFunctions.AVERAGE,true,0);
		
		return resultSet;
	}
	
	
	/**
	 * The method domain returns the set of objects in the domain of the mapping.
	 * @param objs set of object correspondences
	 * @return set of objects for the domain of the set of correspondences
	 * @throws WrongSourceException
	 */
	public ObjSet domain (ObjCorrespondenceSet set) throws WrongSourceException, RepositoryException {
		if (set==null) throw new NullPointerException();

		IDObjectSet objSet = set.getDomainIDObjectSet();
		SourceVersionSet versionSet = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet();
		ObjSet resultSet = new ObjSet();
		
		for (int versionID : set.getDomainSourceVersionIDSet()) {
			for (Obj o : APIFactory.getInstance().getObjectAPI().getObjectSet(
					versionSet.getSourceVersion(versionID), objSet))
				resultSet.addObj(o);
		}
		
		return resultSet;
	}
	
	/**
	 * The method range returns the set of objects in the range of the mapping.
	 * @param objs set of object correspondences
	 * @return set of objects for the range of the set of correspondences
	 * @throws WrongSourceException
	 */
	public ObjSet range (ObjCorrespondenceSet set) throws WrongSourceException, RepositoryException {
		if (set==null) throw new NullPointerException();
		
		IDObjectSet objSet = set.getRangeIDObjectSet();
		SourceVersionSet versionSet = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet();
		ObjSet resultSet = new ObjSet();
		
		for (int versionID : set.getRangeSourceVersionIDSet()) {
			for (Obj o : APIFactory.getInstance().getObjectAPI().getObjectSet(
					versionSet.getSourceVersion(versionID), objSet))
				resultSet.addObj(o);
		}
		
		return resultSet;
	}
	
	/**
	 * The method inverse returns the inverse of the specified object correspondences, 
	 * i.e., input objects are then output objects and output objects are then input 
	 * objects afterwards.
	 * @param objs set of object correspondences
	 * @return set of object correspondences
	 * @throws WrongSourceException
	 */
	public static ObjCorrespondenceSet inverse (ObjCorrespondenceSet set) throws WrongSourceException {
		ObjCorrespondenceSet inverseCorrs = new ObjCorrespondenceSet();
		for (ObjCorrespondence corr : set.getCollection()) 
			inverseCorrs.addCorrespondence(corr.invert());
		return inverseCorrs;
	}
	
	

	
	private static int computeSupportValue(ObjCorrespondenceSet subDomainSet, ObjCorrespondenceSet subRangeSet,
			SupportFunctions verticalSupportFunction, SupportFunctions horizontalSupportFunction, boolean horizontalBeforeVertical) throws OperatorException {
		
		int support=0;
		
		if (horizontalBeforeVertical) {
			List<Integer> supportValueList = new ArrayList<Integer>(subDomainSet.size()*subRangeSet.size());
			
			for (ObjCorrespondence domainCorr : subDomainSet) 
				for (ObjCorrespondence rangeCorr : subRangeSet.getCorrespondenceSet(domainCorr.getRangeObjID(), true)) //korrektur vorher false
					supportValueList.add(horizontalSupportFunction.compute(domainCorr.getSupport(), rangeCorr.getSupport()));
					
			support = verticalSupportFunction.compute(supportValueList);
			
		} else {
			int domainSupportValue=0, rangeSupportValue=0;
			
			for (ObjCorrespondence domainCorr : subDomainSet) {
				for (ObjCorrespondence rangeCorr : subRangeSet) {
					
					switch (verticalSupportFunction) {
						case AVERAGE: 
							domainSupportValue += domainCorr.getSupport(); 
							rangeSupportValue  += rangeCorr.getSupport(); 
							break;
						case MAXIMUM: 
							domainSupportValue = SupportFunctions.MAXIMUM.compute(domainSupportValue,domainCorr.getSupport());
							rangeSupportValue  = SupportFunctions.MAXIMUM.compute(rangeSupportValue,rangeCorr.getSupport());
							break;
						case MINIMUM:
							domainSupportValue = SupportFunctions.MINIMUM.compute(domainSupportValue,domainCorr.getSupport());
							rangeSupportValue  = SupportFunctions.MINIMUM.compute(rangeSupportValue,rangeCorr.getSupport());
							break;
						default: throw new OperatorException("Could not resolve a valid function ("+verticalSupportFunction+")to compute the support.");
					}
				}
			}
			support = horizontalSupportFunction.compute(domainSupportValue, rangeSupportValue);
		}
		return support;
	}
	
	private static float computeConfidenceValue(ObjCorrespondenceSet subDomainSet, ObjCorrespondenceSet subRangeSet,
			ConfidenceFunctions verticalConfidenceFunction, ConfidenceFunctions horizontalConfidenceFunction, boolean horizontalBeforeVertical) throws OperatorException {
		
		float confidence=0;
		
		if (horizontalBeforeVertical) {
			List<Float> confidenceValueList = new ArrayList<Float>(subDomainSet.size()*subRangeSet.size());
			
			for (ObjCorrespondence domainCorr : subDomainSet) 
				for (ObjCorrespondence rangeCorr : subRangeSet.getCorrespondenceSet(domainCorr.getRangeObjID(), true))//korrektur vorher false
					confidenceValueList.add(horizontalConfidenceFunction.compute(domainCorr.getConfidence(), rangeCorr.getConfidence()));
					
			confidence = verticalConfidenceFunction.compute(confidenceValueList);
			
		} else {
			float domainConfidenceValue=0F, rangeConfidenceValue=0F;
			
			for (ObjCorrespondence domainCorr : subDomainSet) {
				for (ObjCorrespondence rangeCorr : subRangeSet.getCorrespondenceSet(domainCorr.getRangeObjID(), true)) { //korrektur vorher false
					
					switch (verticalConfidenceFunction) {
						case AVERAGE: 
							domainConfidenceValue += domainCorr.getConfidence(); 
							rangeConfidenceValue  += rangeCorr.getConfidence(); 
							break;
						case MAXIMUM: 
							domainConfidenceValue = SimpleOperations.max(domainConfidenceValue,domainCorr.getConfidence());
							rangeConfidenceValue  = SimpleOperations.max(rangeConfidenceValue,rangeCorr.getConfidence());
							break;
						case MINIMUM:
							domainConfidenceValue = SimpleOperations.min(domainConfidenceValue,domainCorr.getConfidence());
							rangeConfidenceValue  = SimpleOperations.min(rangeConfidenceValue,rangeCorr.getConfidence());
							break;
						default: throw new OperatorException("Could not resolve a valid function ("+verticalConfidenceFunction+")to compute the confidence.");
					}
				}
			}
			
			confidence = horizontalConfidenceFunction.compute(domainConfidenceValue, rangeConfidenceValue);
		}
		return confidence;
	}
}
