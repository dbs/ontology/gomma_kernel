/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.operator;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersion;
import org.gomma.operator.fusion.ObjFusionFactory;

/**
 * The class contains several operators and measurements for source and mapping evolution.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class EvolutionOperators {


	
	private static volatile EvolutionOperators singleton = null;
	
	/**
	 * The constructor initializes the class by using the given set of already initialized mapping operators
	 */
	private EvolutionOperators() {}
	
	public static EvolutionOperators getInstance() {
		if (singleton == null) {
			synchronized (EvolutionOperators.class) {
				if (singleton == null)
					singleton = new EvolutionOperators();
			}
		}
		return singleton;
	}
	
	/**
	 * The method compares two object sets and return a set of objects that has been added to the 
	 * second object set, i.e., it is not part of the first object set. Typically, the first object 
	 * set should be of an older source version than the second given object set.   
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return object set that has been added to the younger of both sources
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjSet addedObjects (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return SetOperators.diff(objs2, objs1);
	}
	
	/**
	 * The method compares the objects sets of two given sets of correspondences and returns a set of objects 
	 * that has been added to the second object set; i.e., it is not part of the second object set. Typically, 
	 * the first object set should be of an older source version than the second given object set. The domain
	 * parameter whether the object set is taken from the domain or from the range of the mapping.
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources 
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources 
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings 
	 * @return object set that has been added to the younger of both sources
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet addedObjects (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?addedObjects(MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:addedObjects(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method compares two object sets and return a set of objects that has been deleted from the 
	 * first object set, i.e., it is not part of the second object set. Typically, the first object 
	 * set should be of an older source version than the second given object set.   
	 * 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return object set that has been deleted to the older of both sources
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjSet deletedObjects (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return SetOperators.diff(this.deletedObjectsInternal(objs1,objs2),this.fusedObjects(objs1,objs2));
	}
	/**
	 * The method compares two version and internally computes all deleted objects between these versions. 
	 * Note that the resulting set also contains objects that were fused to an object of the newer version.
	 * 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	private ObjSet deletedObjectsInternal (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return SetOperators.diff(objs1,objs2);
	}
	/**
	 * The method compares two object sets and returns a set of objects that were marked as obsolete, i.e.,
	 * in the older version 'isObsolete' was 'false' whereas in the new version the value is 'true'.
	 * 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return object set with objects that were marked as obsolete between the old and the new source
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjSet toObsoleteObjects (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		ObjSet sharedObjSet = SetOperators.intersect(objs1,objs2);
		ObjSet obsoleteObjSet = new ObjSet(objs1.getLDSID());
		
		for (Obj obj : sharedObjSet) {
			
			SourceVersion s1 = obj.getSourceVersionSet().getEarliestSourceVersion();
			SourceVersion s2 = obj.getSourceVersionSet().getLatestSourceVersion();
			
			AttributeSet attSet1 = obj.getAttributeValues(s1.getID()).getAttributeSet();
			AttributeSet attSet2 = obj.getAttributeValues(s2.getID()).getAttributeSet();
			
			boolean obsoleteOldVersion = false;
			boolean obsoleteNewVersion = false;
			for (Attribute att : attSet1.getAttributeSet("toObsolete")) {
				for (AttributeValueVersion avv : obj.getAttributeValues(s1.getID(),att))
					if (avv.toString().equalsIgnoreCase("true")) {
						obsoleteOldVersion = true;
					} else {
						obsoleteOldVersion = false;
					}
			}
			for (Attribute att : attSet2.getAttributeSet("toObsolete")) {
				for (AttributeValueVersion avv : obj.getAttributeValues(s2.getID(),att))
					if (avv.toString().equalsIgnoreCase("true")) {
						obsoleteNewVersion = true;
					} else {
						obsoleteNewVersion = false;
					}
			}
			
			if (obsoleteOldVersion==false&&obsoleteNewVersion==true) {
				obsoleteObjSet.addObj(obj);
			}
		}
		return obsoleteObjSet;
	}
	/**
	 * The method compares two object sets and returns a set objects that were fused to an existing object
	 * in the newer version.
	 * 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @param pds physical data source of both objects sets
	 * @return object set with objects that were fused to another object in the newer version
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjSet fusedObjects(ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		
		int ldsID=-1;
		
		if (objs1.getLDSID()==objs2.getLDSID()) {
			if (objs1.getLDSID()!=ObjSet.DEFAULT_LDSID) {
				if (objs1.size()==0) return objs1;
				if (objs2.size()==0) return objs2;
				
				ldsID = objs1.getLDSID();
			} else {
				if (objs1.size()==0) return objs1;
				if (objs2.size()==0) return objs2;
				// FIXME: What should we do when the object sets are not empty?
			}
		} else {
			if (objs1.getLDSID()!=ObjSet.DEFAULT_LDSID && objs2.getLDSID()==ObjSet.DEFAULT_LDSID) {
				ldsID = objs1.getLDSID();
			} else 
			if (objs1.getLDSID()==ObjSet.DEFAULT_LDSID && objs2.getLDSID()!=ObjSet.DEFAULT_LDSID) {
				ldsID = objs2.getLDSID();
			} else {
				if (objs1.size()==0) return objs1;
				if (objs2.size()==0) return objs2;
				
				throw new OperatorException(
						"Could not execute the fuse operator since the object set are of two "+
						"unequal sources or sources are not available within the repository");
			}
		}
		
		try {
			Source s = APIFactory.getInstance().getSourceAPI().getSourceSet().getSource(ldsID);
			return ObjFusionFactory.getObjFusioner(s).fuseObjectSets(objs1, objs2);	
		} catch (RepositoryException e) {
			throw new OperatorException(e.getMessage());
		}
	}
	
	/**
	 * The method compares the objects sets of two given sets of correspondences and returns a set of objects 
	 * that has been deleted from the first object set; i.e., it is not part of the first object set. Typically, 
	 * the first object set should be of an older source version than the second given object set. The domain
	 * parameter whether the object set is taken from the domain or from the range of the mapping.
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources 
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources 
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings 
	 * @return object set that has been deleted to the younger of both sources
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet deletedObjects (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?deletedObjectsInternal(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:deletedObjectsInternal(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method compares two object sets and return a set of objects that has not been changed from the 
	 * first  to the second object set. Typically, the first object set should be of an older source version 
	 * than the second given object set.   
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return object set that has not been changed between the both sources
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjSet sameObjects (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return SetOperators.intersect(objs1,objs2);
	}
	
	/**
	 * The method compares the objects sets of two given sets of correspondences and returns a set of objects 
	 * that has not been changed between the two extracted object sets. Typically, the first object set 
	 * should be of an older source version than the second given object set. The domain parameter whether
	 * the object set is taken from the domain or from the range of the mapping.
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources 
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources 
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings 
	 * @return object set that has not been changed between both sources
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet sameObjects (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?sameObjects(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:sameObjects(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method calculates and returns the growth rate between the given object sets. Typically, the first 
	 * object set should be of an older source version than the second given object set.   
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return growth rate between both object sets
	 */
	public float getObjectGrowth (ObjSet objs1, ObjSet objs2) {
		return (float) objs1.size() / objs2.size();
	}
	
	/**
	 * The method calculates and returns the growth rate between the two object sets which are extracted from
	 * the given correspondence sets. Typically, the first object set should be of an older source version 
	 * than the second given object set. The domain parameter whether the object set is taken from the domain 
	 * or from the range of the mapping.
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings
	 * @return growth rate between both object sets
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public float getObjectGrowth (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?getObjectGrowth(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:getObjectGrowth(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method calculates and returns the fraction of added objects between two given object sets to a base
	 * object set. Typically, the first object set should be of an older source version than the second given 
	 * object set. Then, the method determines the fraction of added objects of the younger source. 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return fraction of added objects
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getAddedObjectFraction (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return (float) addedObjects(objs1,objs2).size() / objs2.size();
	}
	
	/**
	 * The method calculates and returns the fraction of added objects between two object sets which have been
	 * extracted from the given correspondence sets to a base object set. Typically, the first object set 
	 * should be of an older source version than the second given object set. Then, the method determines the 
	 * fraction of added objects of the younger source. 
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings
	 * @return fraction of added objects based on the extracted object sets from the given correspondence sets
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public float getAddedObjectFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?getAddedObjectFraction(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:getAddedObjectFraction(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method calculates and returns the fraction of deleted objects between two given object sets to a base
	 * object set. Typically, the first object set should be of an older source version than the second given 
	 * object set. Then, the method determines the fraction of deleted objects of the older source. 
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return fraction of deleted objects
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getDeletedObjectFraction (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return (float) deletedObjects(objs1, objs2).size() / objs1.size();
	}
	
	/**
	 * The method calculates and returns the fraction of deleted objects between two object sets which have been
	 * extracted from the given correspondence sets to a base object set. Typically, the first object set 
	 * should be of an older source version than the second given object set. Then, the method determines the 
	 * fraction of deleted objects of the older source. 
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings
	 * @return fraction of deleted objects based on the extracted object sets from the given correspondence sets
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public float getDeletedObjectFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?getDeletedObjectFraction(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:getDeletedObjectFraction(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
	
	/**
	 * The method calculates and returns the ratio between added and deleted objects of two given object sets. 
	 * Typically, the first object set should be of an older source version than the second given 
	 * object set.
	 * @param objs1 object set, typically from the oldest of both sources
	 * @param objs2 object set, typically from the youngest of both sources
	 * @return ratio of added and deleted objects
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getAddDeleteRatio (ObjSet objs1, ObjSet objs2) throws OperatorException, WrongSourceException {
		return (float) addedObjects(objs1,objs2).size() / deletedObjects(objs1,objs2).size();
	}
	
	/**
	 * The method calculates and returns the ratio between added and deleted objects of two given object sets 
	 * which have been extracted from two given correspondence sets. Typically, the first object set should be 
	 * of an older source version than the second given object set.
	 * @param corrs1 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the older of both sources
	 * @param corrs2 correspondence set from which the object set will be extracted; typically, it contains
	 * the objects from the younger of both sources
	 * @param domain true if the objects sets should be extracted from the domain of both mappings; false if 
	 * the object sets should be extracted from the range of both mappings
	 * @return ratio of added and deleted objects
	 * @throws OperatorException
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public float getAddDeleteRatio (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws OperatorException, RepositoryException, WrongSourceException {
		return domain?getAddDeleteRatio(
				MappingOperators.getInstance().domain(corrs1),
				MappingOperators.getInstance().domain(corrs2))
				:getAddDeleteRatio(
						MappingOperators.getInstance().range(corrs1),
						MappingOperators.getInstance().range(corrs2));
	}
		
	/**
	 * The method returns a set of correspondences which have been added to one of two mapping versions. Thereby,
	 * the first mapping should connect objects of older source version than the second mapping.
	 * @param corrs1 set of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @return set of correspondences which have been added to the mapping connecting younger source versions 
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet addedCorrespondences (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return SetOperators.diff(corrs2,corrs1);
	}
	
	/**
	 * The method returns a set of correspondences which have been deleted from one of two mapping versions. Thereby,
	 * the first mapping should connect objects of older source version than the second mapping.
	 * @param corrs1 set of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @return set of correspondences which have been deleted from the mapping connecting older source versions 
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet deletedCorrespondences (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return SetOperators.diff(corrs1,corrs2);
	}
	
	/**
	 * The method returns a set of correspondences which not have been changed between two given mapping versions. Thereby,
	 * the first mapping should connect objects of older source version than the second mapping.
	 * @param corrs1 set of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @return set of correspondences which not have been changed between both mapping versions 
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet sameCorrespondences (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return SetOperators.intersect(corrs1,corrs2);
	}
	
	/**
	 * The method calculates and returns the growth rate between two given correspondence sets by simply taking their 
	 * size into account.
	 * @param corrs1 set of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @param corrs2
	 * @return growth rate of two given correspondence sets
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getCorrespondenceGrowth (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return (float) corrs1.size() / corrs2.size(); 
	}
	
	/**
	 * The method calculates and returns the fraction of added correspondences between two given correspondence sets.
	 * @param corrs1 et of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @return fraction of added correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getAddedCorrespondenceFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return (float) addedCorrespondences(corrs1,corrs2).size() / corrs2.size();
	}
	
	/**
	 * The method calculates and returns the fraction of deleted correspondences between two given correspondence sets.
	 * @param corrs1 et of correspondences connecting objects of older source version than the second mapping
	 * @param corrs2 set of correspondences connecting objects of younger source version than the first mapping
	 * @return fraction of deleted correspondences
	 * @throws OperatorException
	 * @throws WrongSourceException
	 */
	public float getDeletedCorrespondenceFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws OperatorException, WrongSourceException {
		return (float) deletedCorrespondences(corrs1,corrs2).size() / corrs1.size();
	}
}
