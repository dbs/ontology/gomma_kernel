/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/25/03
 * 
 * changes: --
 * 
 **/
package org.gomma.operator;

import java.util.HashMap;
import java.util.Properties;

import org.gomma.exceptions.OperatorException;
import org.gomma.match.MatchProperties;
import org.gomma.match.MatcherFactory;
import org.gomma.match.matcher.Matcher;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.match.metrics.SimilarityFunctionConfiguration;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.normalization.NormalizationFactory;
import org.gomma.normalization.NormalizationType;
import org.gomma.normalization.normalizer.Normalizer;
import org.gomma.util.math.AggregationFunction;

/**
 * The class MatchOperators implements a generic match operator using different parameter settings
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MatchOperators {

	/**
	 * Constructor of the class.
	 */
	private MatchOperators() {}
	
	public static ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2) throws OperatorException {
		return match(s1,s2,StringSimilarityFunctions.TRIGRAM_DICE,1F);
	}
	
	public static ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, float simThreshold) throws OperatorException {
		MatchProperties props = new MatchProperties();
		props.addDomainAttributeName("accession");
		props.addRangeAttributeName("accession");
		return match(s1,s2,simFunc,AggregationFunction.AVERAGE,simThreshold,props);
	}
	
	public static ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties matchProperties) throws OperatorException {
		return match(s1,s2,simFunc,aggFunc,simThreshold,matchProperties, new ObjCorrespondenceSet());
	}
	
	public static ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties matchProperties, ObjCorrespondenceSet initialMap) throws OperatorException {
		if (s1==null) throw new NullPointerException("Could not resolve the specified first source to be matched.");
		if (s2==null) throw new NullPointerException("Could not resolve the specified second source to be matched.");
		
		if (simFunc==null) throw new NullPointerException("Could not resolve the specified similarity function to be used.");
		if (simThreshold<0 || simThreshold>1.0) throw new OperatorException("The specified throwshold is out of range (0..1): "+simThreshold);
		if (aggFunc==null) throw new NullPointerException("Could not resolve the specified aggregation function.");
		if (matchProperties==null) throw new NullPointerException("Could not resolve match properties.");
		if (initialMap==null) throw new NullPointerException("Could not resolve the initial object correspondences.");
		
		SimilarityFunctionConfiguration conf = null;
		if (simFunc instanceof SimilarityFunctionConfiguration) conf = (SimilarityFunctionConfiguration)simFunc;
		else throw new OperatorException("Could not resolve the similarity function configuration.");
		
		Matcher matcher = MatcherFactory.getMatcher(conf);
		matcher.setSimilarityFunction(simFunc);
		matcher.setSimilarityThreshold(simThreshold);
		matcher.setAggregationFunction(aggFunc);
		matcher.setMatchProperties(matchProperties);
		matcher.setInitialMapping(initialMap);

		return matcher.match(s1,s2);
	}
	
	public static ObjCorrespondenceSet match(ObjSet domain, ObjSet range) throws OperatorException {
		return match(domain,range,StringSimilarityFunctions.TRIGRAM_DICE,1F);
	}
	
	public static ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, float simThreshold) throws OperatorException {
		MatchProperties props = new MatchProperties();
		props.addDomainAttributeName("accession");
		props.addRangeAttributeName("accession");
		return match(domain,range,simFunc,AggregationFunction.AVERAGE,simThreshold,props);
	}
	
	public static ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties matchProperties) throws OperatorException {
		return match(domain,range,simFunc,aggFunc,simThreshold,matchProperties, new ObjCorrespondenceSet());
	}
	
	public static ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties matchProperties, ObjCorrespondenceSet initialMap) throws OperatorException {
		if (domain==null) throw new NullPointerException("Could not resolve the specified domain source to be matched.");
		if (range==null) throw new NullPointerException("Could not resolve the specified range source to be matched.");
		
		if (simFunc==null) throw new NullPointerException("Could not resolve the specified similarity function to be used.");
		if (simThreshold<0 || simThreshold>1.0) throw new OperatorException("The specified throwshold is out of range (0..1): "+simThreshold);
		if (aggFunc==null) throw new NullPointerException("Could not resolve the specified aggregation function.");
		if (matchProperties==null) throw new NullPointerException("Could not resolve match properties.");
		if (initialMap==null) throw new NullPointerException("Could not resolve the initial object correspondences.");
		
		SimilarityFunctionConfiguration conf = null;
		if (simFunc instanceof SimilarityFunctionConfiguration) conf = (SimilarityFunctionConfiguration)simFunc;
		else throw new OperatorException("Could not resolve the similarity function configuration.");
		
		Matcher matcher = MatcherFactory.getMatcher(conf);
		matcher.setSimilarityFunction(simFunc);
		matcher.setSimilarityThreshold(simThreshold);
		matcher.setAggregationFunction(aggFunc);
		matcher.setMatchProperties(matchProperties);
		matcher.setInitialMapping(initialMap);
		
		return matcher.match(domain, range);
	}
		
	
	public static ObjSet normalize(ObjSet objs, NormalizationType type, Properties normalizationProps) throws OperatorException {
		if (objs==null) throw new NullPointerException("Could not resolve objects to be normalized.");
		if (type==null) throw new NullPointerException("Could not resolve normalization type.");
		if (normalizationProps==null) throw new NullPointerException("Could not resolve normalization properties.");
		
		Normalizer normalizer = NormalizationFactory.getNormalizer(type);
		normalizer.setNormalizationProperties(normalizationProps);
		return normalizer.normalize(objs);
	}
}
