package org.gomma.operator;

import java.util.List;

import org.gomma.clustering.CorrespondenceClusterFactory;
import org.gomma.clustering.CorrespondenceClusterTypes;
import org.gomma.clustering.clusterer.CorrespondenceClusterer;
import org.gomma.exceptions.OperatorException;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.CorrespondenceClusterFunction;
import org.gomma.util.math.LinkBasedClusteringFunctions;

/**
 * The class ClusteringOperators implements a generic clustering operator using different parameter settings
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class ClusteringOperators {

	/**
	 * Constructor of the class.
	 */
	private ClusteringOperators() {}
	
	
	public static List<ObjCorrespondenceSet> cluster(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set) throws OperatorException {
		return cluster(domain,range,set,CorrespondenceClusterTypes.SOURCE_GRAPH,LinkBasedClusteringFunctions.BOTH_OBJECT_LINK_DIRECTED);
	}
	
	public static List<ObjCorrespondenceSet> cluster(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set, CorrespondenceClusterTypes type, CorrespondenceClusterFunction func) throws OperatorException {
		if (domain==null) throw new NullPointerException("Could not resolve the structure of the domain source version.");
		if (range==null) throw new NullPointerException("Could not resolve the structure of the range source version.");
		if (set==null) throw new NullPointerException("Could not resolve the set of object correspondences.");
		if (type==null) throw new NullPointerException("Could not resolve the clusterer type.");
		if (func==null) throw new NullPointerException("Could not resolve the correspondence cluster function.");
		
		CorrespondenceClusterer clusterer = CorrespondenceClusterFactory.getCorrespondenceClusterer(type);
		
		clusterer.setDomainSourceVersionStructure(domain);
		clusterer.setRangeSourceVersionStructure(range);
		clusterer.setClusterFunction(func);
						
		return clusterer.cluster(set);
	}
}
