package org.gomma.operator;


import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.util.math.CorrespondenceStabilityFunktion;
import org.gomma.util.math.StabilityFunktion;

public class StabilityOperators {
private static volatile StabilityOperators singleton = null;
	
	/**
	 * The constructor initializes the class by using the given set of already initialized mapping operators
	 */
	private StabilityOperators() {}
	
	public static StabilityOperators getInstance() {
		if (singleton == null) {
			synchronized (StabilityOperators.class) {
				if (singleton == null)
					singleton = new StabilityOperators();
			}
		}
		return singleton;
	}
	
	
	public CorrespondenceStability calculateCorrespondenceStab(MappingVersionSet mvs, ObjCorrespondence corr,String analysisName,int analysisId) throws OperatorException, WrongSourceException, RepositoryException {
		
		CorrespondenceStability corrStab;
		float stabAvg;
		float stabWM;
		
		List<MappingVersion> list=mvs.sort();
		List<Float> confidenceList=new ArrayList<Float>();
		confidenceList.add(corr.getConfidence());
		System.out.println("  1.Schritt:map version durchlaufen...");
		for(int i=0;i<list.size()-1;i++){
			ObjCorrespondenceSet tempCorrs=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(list.get(i));
			ObjCorrespondence tempCorr;
			if(tempCorrs.containsCorrespondence(corr.getDomainObjID(),corr.getRangeObjID())){
				
				 tempCorr=tempCorrs.getCorrespondence(corr.getDomainObjID(),corr.getRangeObjID());
				 confidenceList.add(tempCorr.getConfidence());					
			}else {
				float confidence=0;						
				confidenceList.add(confidence);
			}
		}
		//make sure the first similarity in the set >0
		List<Float> newConfidenceList=confidenceList;	
		System.out.println("  2.Schritt:make sure of the kmax from the confidence list... ");
		for(int i=0;i<confidenceList.size();i++){
			if(confidenceList.get(i)==0){				
			    newConfidenceList=confidenceList.subList(i+1, confidenceList.size());
				continue;		
			}else break;
		}
		System.out.println("  3.Schritt:calculate the stabAvg and stabWM through the confidence list...");
		stabAvg=((StabilityFunktion)CorrespondenceStabilityFunktion.AVERAGE).computeCorrespondenceStabiltiy(newConfidenceList);
		stabWM=((StabilityFunktion)CorrespondenceStabilityFunktion.WEIGHTED_MAX).computeCorrespondenceStabiltiy(newConfidenceList);	
		corrStab=new CorrespondenceStability.Builder(corr.getDomainObjID(),corr.getRangeObjID(), analysisId)
		         .analysisName(analysisName)
		         .domainAccession(corr.getDomainAccessionNumber())
		         .rangeAccession(corr.getRangeAccessionNumber())
		         .stabAvgValue(stabAvg)
		         .stabWMValue(stabWM)
		         .lastConfidence(corr.getConfidence())
		         .build();
		System.out.println("the stabAvg is "+corrStab.getStabAvgValue()+". The stabWM is "+corrStab.getStabWMValue()+". The lastConfidence is "+corrStab.getLastConfidence());
		
		
		return corrStab;
	
	}
	/**
	 * The method calculates and returns the Stabilities of correspondences by  given MapVersion sets.
	 * @param mvs considered map versions
	 * @param corrs correspondences in last map version
	 * @param analysisID the analysisID of the Analysis which save the Metadata of the Stability
	 * @return Object CorrespondenceStabilitySet
	 */
	public CorrespondenceStabilitySet calculateCorrespondenceStabs(MappingVersionSet mvs, ObjCorrespondenceSet corrs, String analysisName,int analysisId) throws OperatorException, WrongSourceException, RepositoryException {
		CorrespondenceStabilitySet corrStabSet=new CorrespondenceStabilitySet();
	    //int analysisId=this.apiFactory.getStabilityAPI().loadAnalysisID(analysisName);
		System.out.println("starting operating methode calculateCorrespondenceStabs...");
		System.out.println("correspondence's size of last map version ist: "+corrs.size());
			List<MappingVersion> list=mvs.sort();
			Map<ObjCorrespondence, List<Float>> historyCorrSet=new Object2ObjectOpenHashMap<ObjCorrespondence, List<Float>>();
			//historyCorrSet initial value
		    for(ObjCorrespondence corr:corrs.getCollection()){
		    	ArrayList<Float> confList=new ArrayList<Float>();
		    	confList.add(corr.getConfidence());
		    	historyCorrSet.put(corr, confList); 	
		    }
            System.out.println("mvSet durchlaufen...");
            //for(int i=0;i<list.size()-1;i++){
            for(int i=list.size()-2;i>=0;i--){
			    System.out.println("Getting all correspondences of the "+(i)+". map version");
				ObjCorrespondenceSet tempCorrs=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(list.get(i));
				ObjCorrespondence tempCorr;
				System.out.println("Compare these correspondence with those correpondence which appear in last map version...");
				for(ObjCorrespondence corr:corrs.getCollection()){
					//System.out.println("    save the confidence of these correspondence in each confidenceList...");
				   if(tempCorrs.containsCorrespondence(corr.getDomainObjID(),corr.getRangeObjID())){
					 tempCorr=tempCorrs.getCorrespondence(corr.getDomainObjID(),corr.getRangeObjID());
					 historyCorrSet.get(corr).add(0,tempCorr.getConfidence());
				   }else {
					   historyCorrSet.get(corr).add(0,0.0F);
				   }	
		    	}
			}	
			System.out.println("mvSet schon durchgelaufen, all confidenceList are saved. The size of confidenceList is: "+historyCorrSet.size());
			System.out.println("calculate the stabAvg and stabWM through each confidence list...");
			
			for(ObjCorrespondence corr:corrs.getCollection()){
				
		     List<Float> confList=historyCorrSet.get(corr);
		     
		     //System.out.println(confList);
		     
		     //make sure the first similarity in the set >0
		     List<Float> newConfidenceList=confList;	
		     for(int i=0;i<confList.size();i++){
			    if(confList.get(i)==0){				
			       newConfidenceList= confList.subList(i+1, confList.size());
				   continue;		
			    }else break;
		     } 		   
			 float stabAvg=((StabilityFunktion)CorrespondenceStabilityFunktion.AVERAGE).computeCorrespondenceStabiltiy(newConfidenceList);
			 float stabWM=((StabilityFunktion)CorrespondenceStabilityFunktion.WEIGHTED_MAX).computeCorrespondenceStabiltiy(newConfidenceList);
			 
			 if(corr.getDomainAccessionNumber().equalsIgnoreCase("GO:0033155") && corr.getRangeAccessionNumber().equalsIgnoreCase("GO:0033156")){
				 //System.out.println("  stabiltiy of one of corespondences is calculated and will be saved in a CorrStab instance...");
				 System.out.println("Stabiltiy for "+corr.getDomainAccessionNumber() +"-"+corr.getRangeAccessionNumber() +"is calculated and will be saved in a CorrStab instance...");	
			 }
			 
			CorrespondenceStability corrStab=new CorrespondenceStability.Builder(corr.getDomainObjID(),corr.getRangeObjID(), analysisId)
			         .domainAccession(corr.getDomainAccessionNumber())
			         .rangeAccession(corr.getRangeAccessionNumber())
			         .analysisName(analysisName)
			         .stabAvgValue(stabAvg)
			         .stabWMValue(stabWM)
			         .lastConfidence(corr.getConfidence())
			         .build();
			if(corr.getDomainAccessionNumber().equalsIgnoreCase("GO:0033155") && corr.getRangeAccessionNumber().equalsIgnoreCase("GO:0033156")){
				System.out.println(corr.getDomainAccessionNumber() +"-"+corr.getRangeAccessionNumber() +": the stabAvg is "+corrStab.getStabAvgValue()+". The stabWM is "+corrStab.getStabWMValue()+". The lastConfidence is "+corrStab.getLastConfidence());
			}
			
			corrStabSet.addCorrespondenceStab(corrStab);
			 if(corr.getDomainAccessionNumber().equalsIgnoreCase("GO:0033155") && corr.getRangeAccessionNumber().equalsIgnoreCase("GO:0033156")){
				 System.out.println(corr.getDomainAccessionNumber() +"-"+corr.getRangeAccessionNumber() +": corrStab is saved in a CorrStabSet and this CorrStabSet is current with "+corrStabSet.size()+"element(s)");
			}
		}
		System.out.println("ended methode CalculateCorrespondenceStabs...");
		return corrStabSet;
	}
	
	
	 
/*	public float getStabAvg(List<Float> confList)throws OperatorException{
		//kmax is the real previous number of versions 
		int kMax=confList.size()-1;
		float stabAvg;
		float sum=0;
		if(confList.size()!=0){
		for(int i=0;i<kMax;i++){
			sum+=Math.abs(confList.get(i+1)-confList.get(i));
		}
		stabAvg=1-sum/(kMax);
		}
		else throw new OperatorException("could not catch confidence set which are used to calculate the Stability.");
		return stabAvg;
	}
	
	public float getStabWM(List<Float> confList)throws OperatorException{
		//kmax is the real previous number of versions 
		int kMax=confList.size()-1;
		float stabWM=0;
		float lastConfidence;
		float max=0;
		if(confList.size()!=0){
			lastConfidence=confList.get(confList.size()-1);
			for(int i=0; i<kMax;i++){
				float temp=Math.abs(lastConfidence-confList.get(i))/(kMax-i);
			    max=Math.max(max,temp);
			    stabWM=1-max;
			}
		}
		else throw new OperatorException("could not catch confidence set which are used to calculate the Stability.");
		return stabWM;
	}
*/
}
