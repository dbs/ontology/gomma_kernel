/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes:
 * 
 * 2008/24/03 -- add import method
 * 
 **/

package org.gomma;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;

/**
 * The interface represents the GOMMA manager interface that 
 * can be used to retrieve specific manager of the GOMMA kernel. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface GommaManager extends Remote {
	
	/**
	 * The method returns the source manager comprising relevant
	 * source and source-version methods.
	 * @return source manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public SourceManager getSourceManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the mapping manager comprising relevant
	 * mapping methods, e.g., the retrieval of mapping metadata but 
	 * also correspondence data.
	 * @return mapping manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public MappingManager getMappingManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the mapping operation manager to manipulate mappings.
	 * @return mapping operation manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public MappingOperationManager getMappingOperationManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the match manager comprising relevant methods
	 * allowing to match sources and source versions.
	 * @return match manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public MatchManager getMatchManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the work manager comprising relevant methods
	 * to execute different tasks, e.g., match tasks, in a distributed fashion.
	 * @return work manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public WorkManager getWorkManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the source evolution manager comprising relevant methods
	 * that can be used to determine the evolution (history) of sources taking the
	 * available source versions into account.
	 * @return source evolution manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public SourceEvolutionManager getSourceEvolutionManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the mapping object evolution manager comprising relevant
	 * methods that can be used to determine the evolution of mapping objects, i.e., 
	 * source objects that are included in a mapping. 
	 * @return mapping object evolution manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public MappingObjectEvolutionManager getMappingObjectEvolutionManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the mapping correspondence evolution manager comprising
	 * relevant methods that can be used to determine the evolution of mapping 
	 * correspondences.
	 * @return mapping correspondence evolution manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public MappingCorrespondenceEvolutionManager getMappingCorrespondenceEvolutionManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the evolution statistics manager comprising relevant methods
	 * that can be used to retrieve pre-computed evolution statistics.
	 * @return evolution statistics manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public EvolutionStatisticsManager getEvolutionStatisticsManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the statistics manager comprising relevant methods
	 * that can be used to generate and retrieve source and mapping statistics.
	 * @return statistics manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public StatisticsManager getStatisticsManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the stability manager comprising relevant methods
	 * that can be used to calculate the stability of the  correspondence confidence 
	 * during the evolution.
	 * @author Yiming Huang (Diploma Student)
	 * @return statistics manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public StabilityManager getStabilityManager() throws RemoteException, GommaException;
	
	/**
	 * The method returns the distributed match manager for solving distributed match
	 * tasks.
	 * 
	 * @author Michael Hartung
	 * @return distributed match manager
	 * @throws RemoteException
	 * @throws GommaException
	 */
	public DistributedMatchManager getDistributedMatchManager() throws RemoteException, GommaException;
	
}
