package org.gomma.manager.analysis;

import java.rmi.RemoteException;
import java.util.List;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingStatisticsSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class StatisticsManagerModuleImpl implements StatisticsManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2390952537592531052L;

	/**
	 * The constructor initializes the class.
	 */
	public StatisticsManagerModuleImpl() {}
	
	/**
	 * @see StatisticsManager#storeSourceStatictics()
	 */
	public boolean storeSourceStatistics() throws GommaException, RemoteException {
		APIFactory.getInstance().getSourceStatisticsAPI().deleteSourceStatistics();
		return APIFactory.getInstance().getSourceStatisticsAPI().insertSourceStatictics()!=0;
	}
	
	/**
	 * @see StatisticsManager#getSourceStatisticsSet()
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceStatisticsAPI().getSourceStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#storeSourceVersionStatistics()
	 */
	public boolean storeSourceVersionStatistics() throws GommaException, RemoteException {
		APIFactory.getInstance().getSourceStatisticsAPI().deleteSourceVersionStatistics();
		return APIFactory.getInstance().getSourceStatisticsAPI().insertSourceVersionStatistics()!=0;
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet()
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceStatisticsAPI().getSourceVersionStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet(Source)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceStatisticsAPI().getSourceVersionStatisticsSet(s);
	}
	
	/**
	 * @see StatisticsManager#storeMappingStatistics()
	 */
	public boolean storeMappingStatistics() throws GommaException, RemoteException {
		APIFactory.getInstance().getMappingStatisticsAPI().deleteMappingStatistics();
		return APIFactory.getInstance().getMappingStatisticsAPI().insertMappingStatistics()!=0;
	}
	
	/**
	 * @see StatisticsManager#getMappingStatisticsSet()
	 */
	public MappingStatisticsSet getMappingStatisticsSet() throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingStatisticsAPI().getMappingStatistics();
	}
	
	
	
	
	
	
	/**
	 * @see StatisticsManager#getConfidenceFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws RepositoryException, RemoteException {
		return APIFactory.getInstance().getCorrespondenceAnalysisAPI().getConfidenceFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getSupportFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws RepositoryException, RemoteException {
		return APIFactory.getInstance().getCorrespondenceAnalysisAPI().getSupportFrequencies(v);
	}

	
	/**
	 * @see StatisticsManager#getDomainObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws RepositoryException, RemoteException {
		return APIFactory.getInstance().getCorrespondenceAnalysisAPI().getDomainObjectFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getRangeObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws RepositoryException, RemoteException {
		return APIFactory.getInstance().getCorrespondenceAnalysisAPI().getRangeObjectFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getSourceVersionSatisticsSet(SourceVersionSet)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(
			SourceVersionSet set) throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceStatisticsAPI().getSourceVersionStatisticsSet(set);
	}
}
