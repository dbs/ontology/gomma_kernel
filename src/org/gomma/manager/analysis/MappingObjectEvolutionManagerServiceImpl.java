/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingObjectEvolutionManagerServiceImpl extends AbstractManagerService implements MappingObjectEvolutionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1308354552649835167L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public MappingObjectEvolutionManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#addedObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet addedObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.addedObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#addedObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet addedObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.addedObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#deletedObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet deletedObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.deletedObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#deletedObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet deletedObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.deletedObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#sameObjects(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public ObjSet sameObjects(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.sameObjects(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#sameObjects(MappingVersion, MappingVersion, boolean)
	 */
	public ObjSet sameObjects(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.sameObjects(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getObjectGrowth(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getObjectGrowth(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getObjectGrowth(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getObjectGrowth(MappingVersion, MappingVersion, boolean)
	 */
	public float getObjectGrowth(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getObjectGrowth(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddedObjectFraction(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getAddedObjectFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getAddedObjectFraction(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddedObjectFraction(MappingVersion, MappingVersion, boolean)
	 */
	public float getAddedObjectFraction(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getAddedObjectFraction(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getDeletedObjectFraction(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getDeletedObjectFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getDeletedObjectFraction(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getDeletedObjectFraction(MappingVersion, MappingVersion, boolean)
	 */
	public float getDeletedObjectFraction(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getDeletedObjectFraction(v1, v2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddDeleteRatio(ObjCorrespondenceSet, ObjCorrespondenceSet, boolean)
	 */
	public float getAddDeleteRatio(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getAddDeleteRatio(corrs1, corrs2, domain);
	}
	
	/**
	 * @see MappingObjectEvolutionManager#getAddDeleteRatio(MappingVersion, MappingVersion, boolean)
	 */
	public float getAddDeleteRatio(MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException {
		return super.gommaService.getAddDeleteRatio(v1, v2, domain);
	}

}
