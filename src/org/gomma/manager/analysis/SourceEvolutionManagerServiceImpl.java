/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceEvolutionManagerServiceImpl extends AbstractManagerService implements SourceEvolutionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7840730504353979521L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public SourceEvolutionManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see SourceEvolutionManager#addedObjects(ObjSet, ObjSet)
	 */
	public ObjSet addedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.addedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#deletedObjects(ObjSet, ObjSet)
	 */
	public ObjSet deletedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.deletedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#sameObjects(ObjSet, ObjSet)
	 */
	public ObjSet sameObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.sameObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#toObsoleteObjects(ObjSet, ObjSet)
	 */
	public ObjSet toObsoleteObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.toObsoleteObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#fusedObjects(ObjSet, ObjSet)
	 */
	public ObjSet fusedObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.fusedObjects(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getObjectGrowth(ObjSet, ObjSet)
	 */
	public float getObjectGrowth(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.getObjectGrowth(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getAddedObjectFraction(ObjSet, ObjSet)
	 */
	public float getAddedObjectFraction(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.getAddedObjectFraction(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getDeletedObjectFraction(ObjSet, ObjSet)
	 */
	public float getDeletedObjectFraction(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.getDeletedObjectFraction(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getAddDeleteRatio(ObjSet, ObjSet)
	 */
	public float getAddDeleteRatio(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException {
		return super.gommaService.getAddDeleteRatio(objs1, objs2);
	}
	
	/**
	 * @see SourceEvolutionManager#getCoverage(ObjSet)
	 */
	public float getCoverage(ObjSet objs) throws GommaException,RemoteException {
		return super.gommaService.getCoverage(objs);
	}

	/**
	 * @see SourceEvolutionManager#addedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet addedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return super.gommaService.addedObjects(v1, v2);
	}

	/**
	 * @see SourceEvolutionManager#deletedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet deletedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return super.gommaService.deletedObjects(v1, v2);
	}

	/**
	 * @see SourceEvolutionManager#fusedObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet fusedObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return super.gommaService.fusedObjects(v1, v2);
	}

	/**
	 * @see SourceEvolutionManager#toObsoleteObjects(SourceVersion, SourceVersion)
	 */
	public ObjSet toObsoleteObjects(SourceVersion v1, SourceVersion v2)
			throws GommaException, RemoteException {
		return super.gommaService.toObsoleteObjects(v1, v2);
	}
	
}
