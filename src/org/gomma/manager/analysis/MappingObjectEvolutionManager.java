/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;

/**
 * The interface defines methods to retrieve evolutionary differences
 * between objects sets of used in different mapping versions. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingObjectEvolutionManager extends Serializable, Remote {

	public ObjSet addedObjects   (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public ObjSet addedObjects   (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public ObjSet deletedObjects (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public ObjSet deletedObjects (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public ObjSet sameObjects    (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public ObjSet sameObjects    (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public float getObjectGrowth          (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public float getObjectGrowth          (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public float getAddedObjectFraction   (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public float getAddedObjectFraction   (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public float getDeletedObjectFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public float getDeletedObjectFraction (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
	public float getAddDeleteRatio        (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2, boolean domain) throws GommaException, RemoteException;
	
	public float getAddDeleteRatio        (MappingVersion v1, MappingVersion v2, boolean domain) throws GommaException, RemoteException;
	
}
