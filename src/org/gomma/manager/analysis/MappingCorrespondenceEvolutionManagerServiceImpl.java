/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.rmi.RemoteException;
import java.util.Map;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.util.date.SimplifiedGregorianCalendar;


/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingCorrespondenceEvolutionManagerServiceImpl extends AbstractManagerService implements MappingCorrespondenceEvolutionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7537431795854380270L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public MappingCorrespondenceEvolutionManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet addedCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.addedCorrespondences(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet addedCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.addedCorrespondences(v1, v2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet deletedCorrespondences (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.deletedCorrespondences(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet deletedCorrespondences (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.deletedCorrespondences(v1, v2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet sameCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.sameCorrespondences(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet sameCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.sameCorrespondences(v1, v2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getCorrespondenceGrowth(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.getCorrespondenceGrowth(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(MappingVersion, MappingVersion)
	 */
	public float getCorrespondenceGrowth(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.getCorrespondenceGrowth(v1, v2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getAddedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.getAddedCorrespondenceFraction(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getAddedCorrespondenceFraction(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.getAddedCorrespondenceFraction(v1, v2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getDeletedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return super.gommaService.getDeletedCorrespondenceFraction(corrs1, corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getDeletedCorrespondenceFraction(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return super.gommaService.getDeletedCorrespondenceFraction(v1, v2);
	}

	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceEvolutionConfidence(CorrespondenceStability)
	 */
	public Map<SimplifiedGregorianCalendar, Float> getCorrespondenceEvolutionConfidence(CorrespondenceStability corrStab) throws GommaException, RemoteException {
		return super.gommaService.getCorrespondenceEvolutionConfidence(corrStab);
	}


}
