/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;

/**
 * The interface defines methods to create and retrieve pre-calculated
 * evolution statistics of sources and mappings. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface EvolutionStatisticsManager extends Serializable, Remote {

	public Map<Integer,Integer> getLDSVersionStats(String pdsName, String objectType, int statType) throws GommaException, RemoteException;
	
	public void storeLDSVersionStats(String pdsName, String objectType) throws GommaException, RemoteException;
	
	public void storeEvolutionStats(String pdsName, String objectType) throws GommaException, RemoteException;
	
	public boolean storeMappingVersionEvolution() throws GommaException, RemoteException;
	
	public float getAverageEvolutionStats(String pdsName, String objectType, int action, int period, Calendar start, Calendar end) throws GommaException, RemoteException;
	
	public Map<Integer,Integer> getAllEvolutionStats(String pdsName, String objectType, int action, Calendar start, Calendar end) throws GommaException, RemoteException;
	
	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet set,int action) throws GommaException,RemoteException;
	
	public Map<String, List<String>> getObsoleteMapping(List<String> accessions) throws GommaException, RemoteException;
	
	public Map<Map<Integer,Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet set,int action,int period) throws GommaException, RemoteException;    
	
	public boolean storeSourceVersionEvolution() throws GommaException, RemoteException;
	
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws GommaException, RemoteException;
	
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws GommaException, RemoteException;
	
	public List<MappingVersionEvolution> getMappingVersionEvolutionListWithAvgData(Mapping m,int period) throws GommaException, RemoteException;
	
	public List<MappingVersionEvolution> getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet mvSet,int period) throws GommaException, RemoteException;
	
}
