/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.operator.StatisticsOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class EvolutionStatisticsManagerModuleImpl implements EvolutionStatisticsManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5528995543817022290L;

	/**
	 * The constructor initializes the class.
	 */
	public EvolutionStatisticsManagerModuleImpl() {}
	
	/**
	 * @see EvolutionStatisticsManager#getLDSVersionStats(String, String, int)
	 */
	public Map<Integer,Integer> getLDSVersionStats(String pdsName, String objectType, int statType) throws GommaException, RemoteException {
		return StatisticsOperators.getInstance().getLDSVersionStats(
				this.getSourceVersionSet(objectType, pdsName),statType);
	}

	/**
	 * @see EvolutionStatisticsManager#storeLDSVersionStats(String, String)
	 */
	public void storeLDSVersionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		APIFactory.getInstance().getEvolutionStatisticsAPI().storeLDSVersionStats(
				this.getSourceVersionSet(objectType, pdsName));
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeEvolutionStats(String, String)
	 */
	public void storeEvolutionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		APIFactory.getInstance().getEvolutionStatisticsAPI().storeEvolutionStats(
				this.getSourceVersionSet(objectType, pdsName));
	}
	
	/**
	 * @see EvolutionStatisticsManager#getAverageEvolutionStats(String, String, int, int, Calendar, Calendar)
	 */
	public float getAverageEvolutionStats(String pdsName, String objectType, int action, int period, Calendar start, Calendar end) throws GommaException, RemoteException {	
		return StatisticsOperators.getInstance().getAverageEvolutionStats(
				this.getSourceVersionSet(objectType, pdsName),action,period,start,end);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStats(String, String, int, Calendar, Calendar)
	 */
	public Map<Integer,Integer> getAllEvolutionStats(String pdsName, String objectType, int action, Calendar start, Calendar end) throws GommaException, RemoteException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getAllEvolutionStats(
				this.getSourceVersionSet(objectType, pdsName),action,
				new SimplifiedGregorianCalendar(start),new SimplifiedGregorianCalendar(end));
	}

	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet set,int action) throws GommaException, RemoteException{
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getAllEvolutionStats(set, action, 
				set.getEarliestSourceVersion().getCreationVersionDate(), 
				set.getLatestSourceVersion().getCreationVersionDate());
	}
	
	public Map<Map<Integer,Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet set,int action,int period) throws GommaException, RemoteException{
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getAllEvolutionStatsWithAvgData(set, action, period,
				      set.getEarliestSourceVersion().getCreationVersionDate(), 
				      set.getLatestSourceVersion().getCreationVersionDate());
	}
	
	/**
	 * @see EvolutionStatisticsManager#getObsoleteMapping(List<String>)
	 */
	public Map<String, List<String>> getObsoleteMapping(
			List<String> accessions) throws GommaException, RemoteException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getObsoleteMappings(accessions);
	}
	
	private SourceVersionSet getSourceVersionSet(String objectType, String pdsName) throws GommaException {
		SourceSet sourceSet = APIFactory.getInstance().getSourceAPI().getSourceSet(objectType, pdsName);
		
		if (sourceSet.size()==0)
			throw new NoSuchElementException("The source '"+objectType+"@"+pdsName+"' could not be found.");
		
		if (sourceSet.size()>1)
			throw new GommaException("Too many sources available sharing object type '"+
					objectType+"' and physical source name '"+pdsName+"'.");
		
		Source s = sourceSet.iterator().next();
		SourceVersionSet versionSet = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s);
		
		if (versionSet.size()==0) 
			throw new NoSuchElementException("Could not find any source version for source '"+s.getName()+"'.");
		
		return versionSet;
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeMappingVersionEvolution()
	 */
	public boolean storeMappingVersionEvolution() throws GommaException, RemoteException {
		MappingSet mappingSet = APIFactory.getInstance().getMappingAPI().getMappingSet();
	
		MappingVersionSet versionSet;
		
		for (Mapping m : mappingSet) {
			versionSet = APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m);
			APIFactory.getInstance().getEvolutionStatisticsAPI().storeMapVersionEvolutionStats(versionSet);
		}
		
		return true;
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeSourceVersionEvolution()
	 */
	public boolean storeSourceVersionEvolution() throws GommaException, RemoteException {
		SourceSet sourceSet = APIFactory.getInstance().getSourceAPI().getSourceSet();
	
		SourceVersionSet versionSet;
		
		for (Source s : sourceSet) {
			versionSet = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s);
			APIFactory.getInstance().getEvolutionStatisticsAPI().storeEvolutionStats(versionSet);
		}
		
		return true;
	}
	
	/**
	 * @see EvolutionStatisticsManager#getSourceVersionEvolutionList(Source)
	 */
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws GommaException, RemoteException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getSourceVersionEvolutionList(s);
	}

	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionList(Mapping)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws GommaException, RemoteException {
		return APIFactory.getInstance().getEvolutionStatisticsAPI().getMappingVersionEvolutionList(m);
		
		
	}
	
	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionListWithAvgData(Mapping,int)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionListWithAvgData(Mapping m, int period)throws GommaException,RemoteException{
		MappingVersionSet mvSet=APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m);
		return this.getPartOfMappingVersionEvolutionListWithAvgData(mvSet, period);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet,int)
	 */
	public List<MappingVersionEvolution> getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet mvSet,int period)throws GommaException,RemoteException{
		Mapping m=mvSet.getCollection().iterator().next().getMapping();
		MappingVersion firstVersion=mvSet.sort().get(0);
		MappingVersion lastVersion=mvSet.getLastMappingVersion();
		List<MappingVersionEvolution> result=new ArrayList<MappingVersionEvolution>();
		int numAddCorr=0,numDelCorr=0,numSamCorr=0; float numAFracCorr=0f,numDFracCorr=0f;
		int numAddDomainObj=0,numDelDomainObj=0,numSamDomainObj=0; float numAFracDomainObj=0f,numDFracDomainObj=0f;
		int numAddRangeObj=0,numDelRangeObj=0,numSamRangeObj=0; float numAFracRangeObj=0f,numDFracRangeObj=0f;
		
		List<MappingVersionEvolution> all=APIFactory.getInstance().getEvolutionStatisticsAPI().getMappingVersionEvolutionList(m);
		for(MappingVersionEvolution e:all){
			if(e.getFromMappingVersion().getCreationDate().before(firstVersion.getCreationDate())
					||e.getToMappingVersion().getCreationDate().after(lastVersion.getCreationDate()))continue;
			else{
				result.add(e);
				numAddCorr+=e.getNumberOfAddedCorrs();
				numDelCorr+=e.getNumberOfDeletedCorrs();
				numSamCorr+=e.getNumberOfSameCorrs();
				numAFracCorr+=e.getAddedCorrFraction();
				numDFracCorr+=e.getDeletedCorrFraction();
				numAddDomainObj+=e.getNumberOfAddedDomainObjs();
				numDelDomainObj+=e.getNumberOfDeletedDomainObjs();
				numSamDomainObj+=e.getNumberOfSameDomainObjs();
				numAFracDomainObj+=e.getAddedDomainObjsFraction();
				numDFracDomainObj+=e.getDeletedDomainObjsFraction();
				numAddRangeObj+=e.getNumberOfAddedRangeObjs();
				numDelRangeObj+=e.getNumberOfDeletedRangeObjs();
				numSamRangeObj+=e.getNumberOfSameRangeObjs();
				numAFracRangeObj+=e.getAddedRangeObjsFraction();
				numDFracRangeObj+=e.getDeletedRangeObjsFraction();
				
			}
		}
		float observationPeriod = lastVersion.getCreationDate().getTimeInMillis()-firstVersion.getCreationDate().getTimeInMillis();				
		float monthPeriod = 30f*24f*3600000f;
		float quartalPeriod = 90f*24f*3600000f;
		float halfyearPeriod = 180f*24f*3600000f;
		float yearPeriod = 365f*24f*3600000f;
		MappingVersionEvolution mve=null;
		if(period==StatisticsOperators.MONTH_PERIOD){
			mve=new MappingVersionEvolution.Builder(firstVersion, lastVersion)
			                 .numberOfAddedCorrs((int)(numAddCorr/observationPeriod*monthPeriod))
			                 .numberOfDeletedCorrs((int)(numDelCorr/observationPeriod*monthPeriod))
			                 .numberOfSameCorrs((int)(numSamCorr/observationPeriod*monthPeriod))
			                 .addedCorrFraction(numAFracCorr/observationPeriod*monthPeriod)
			                 .deletedCorrFraction(numDFracCorr/observationPeriod*monthPeriod)
			                 .numberOfAddedDomainObjs((int)(numAddDomainObj/observationPeriod*monthPeriod))
			                 .numberOfDeletedDomainObjs((int)(numDelDomainObj/observationPeriod*monthPeriod))
			                 .numberOfSameDomainObjs((int)(numSamDomainObj/observationPeriod*monthPeriod))
			                 .addedDomainObjsFraction(numAFracDomainObj/observationPeriod*monthPeriod)
			                 .deletedDomainObjsFraction(numDFracDomainObj/observationPeriod*monthPeriod)
			                 .numberOfAddedRangeObjs((int)(numAddRangeObj/observationPeriod*monthPeriod))
			                 .numberOfDeletedRangeObjs((int)(numDelRangeObj/observationPeriod*monthPeriod))
			                 .numberOfSameRangeObjs((int)(numSamRangeObj/observationPeriod*monthPeriod))
			                 .addedRangeObjsFraction((float)Math.round(numAFracRangeObj/observationPeriod*monthPeriod*100)/100)
			                 .deletedRangeObjsFraction((float)Math.round(numDFracRangeObj/observationPeriod*monthPeriod*100)/100)
			                 .build();
		}
		else if(period==StatisticsOperators.QUARTAL_PERIOD) {
			mve=new MappingVersionEvolution.Builder(firstVersion, lastVersion)
            .numberOfAddedCorrs((int)(numAddCorr/observationPeriod*quartalPeriod))
            .numberOfDeletedCorrs((int)(numDelCorr/observationPeriod*quartalPeriod))
            .numberOfSameCorrs((int)(numSamCorr/observationPeriod*quartalPeriod))
            .addedCorrFraction(numAFracCorr/observationPeriod*quartalPeriod)
            .deletedCorrFraction(numDFracCorr/observationPeriod*quartalPeriod)
            .numberOfAddedDomainObjs((int)(numAddDomainObj/observationPeriod*quartalPeriod))
            .numberOfDeletedDomainObjs((int)(numDelDomainObj/observationPeriod*quartalPeriod))
            .numberOfSameDomainObjs((int)(numSamDomainObj/observationPeriod*quartalPeriod))
            .addedDomainObjsFraction(numAFracDomainObj/observationPeriod*quartalPeriod)
            .deletedDomainObjsFraction(numDFracDomainObj/observationPeriod*quartalPeriod)
            .numberOfAddedRangeObjs((int)(numAddRangeObj/observationPeriod*quartalPeriod))
            .numberOfDeletedRangeObjs((int)(numDelRangeObj/observationPeriod*quartalPeriod))
            .numberOfSameRangeObjs((int)(numSamRangeObj/observationPeriod*quartalPeriod))
            .addedRangeObjsFraction((float)Math.round(numAFracRangeObj/observationPeriod*quartalPeriod*100)/100)
            .deletedRangeObjsFraction((float)Math.round(numDFracRangeObj/observationPeriod*monthPeriod*100)/100)
            .build();
		}
		else if(period==StatisticsOperators.HALFYEAR_PERIOD){
			float addFrac=(float)Math.round(numAFracRangeObj/observationPeriod*halfyearPeriod*100)/100;
			float delFrac=(float)Math.round(numDFracRangeObj/observationPeriod*halfyearPeriod*100)/100;
			mve=new MappingVersionEvolution.Builder(firstVersion, lastVersion)
            .numberOfAddedCorrs((int)(numAddCorr/observationPeriod*halfyearPeriod))
            .numberOfDeletedCorrs((int)(numDelCorr/observationPeriod*halfyearPeriod))
            .numberOfSameCorrs((int)(numSamCorr/observationPeriod*halfyearPeriod))
            .addedCorrFraction(numAFracCorr/observationPeriod*halfyearPeriod)
            .deletedCorrFraction(numDFracCorr/observationPeriod*halfyearPeriod)
            .numberOfAddedDomainObjs((int)(numAddDomainObj/observationPeriod*halfyearPeriod))
            .numberOfDeletedDomainObjs((int)(numDelDomainObj/observationPeriod*halfyearPeriod))
            .numberOfSameDomainObjs((int)(numSamDomainObj/observationPeriod*halfyearPeriod))
            .addedDomainObjsFraction(numAFracDomainObj/observationPeriod*halfyearPeriod)
            .deletedDomainObjsFraction(numDFracDomainObj/observationPeriod*halfyearPeriod)
            .numberOfAddedRangeObjs((int)(numAddRangeObj/observationPeriod*halfyearPeriod))
            .numberOfDeletedRangeObjs((int)(numDelRangeObj/observationPeriod*halfyearPeriod))
            .numberOfSameRangeObjs((int)(numSamRangeObj/observationPeriod*halfyearPeriod))
            .addedRangeObjsFraction(addFrac)
            .deletedRangeObjsFraction(delFrac)
            .build();
		}
		else if(period==StatisticsOperators.YEAR_PERIOD){
			mve=new MappingVersionEvolution.Builder(firstVersion, lastVersion)
            .numberOfAddedCorrs((int)(numAddCorr/observationPeriod*yearPeriod))
            .numberOfDeletedCorrs((int)(numDelCorr/observationPeriod*yearPeriod))
            .numberOfSameCorrs((int)(numSamCorr/observationPeriod*yearPeriod))
            .addedCorrFraction(numAFracCorr/observationPeriod*yearPeriod)
            .deletedCorrFraction(numDFracCorr/observationPeriod*yearPeriod)
            .numberOfAddedDomainObjs((int)(numAddDomainObj/observationPeriod*yearPeriod))
            .numberOfDeletedDomainObjs((int)(numDelDomainObj/observationPeriod*yearPeriod))
            .numberOfSameDomainObjs((int)(numSamDomainObj/observationPeriod*yearPeriod))
            .addedDomainObjsFraction(numAFracDomainObj/observationPeriod*yearPeriod)
            .deletedDomainObjsFraction(numDFracDomainObj/observationPeriod*yearPeriod)
            .numberOfAddedRangeObjs((int)(numAddRangeObj/observationPeriod*yearPeriod))
            .numberOfDeletedRangeObjs((int)(numDelRangeObj/observationPeriod*yearPeriod))
            .numberOfSameRangeObjs((int)(numSamRangeObj/observationPeriod*yearPeriod))
            .addedRangeObjsFraction((float)Math.round(numAFracRangeObj/observationPeriod*yearPeriod*100)/100)
            .deletedRangeObjsFraction((float)Math.round(numDFracRangeObj/observationPeriod*yearPeriod*100)/100)
            .build();
		}
		result.add(mve);
        return result;
	}
}
