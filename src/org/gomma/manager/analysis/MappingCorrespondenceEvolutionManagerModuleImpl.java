/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import org.gomma.api.APIFactory;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.operator.EvolutionOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingCorrespondenceEvolutionManagerModuleImpl implements MappingCorrespondenceEvolutionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6418160937717791536L;

	/**
	 * The constructor initializes the class.
	 * @param apiFactory repository API factory 
	 */
	public MappingCorrespondenceEvolutionManagerModuleImpl() {}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet addedCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.addedCorrespondences(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#addedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet addedCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().addedCorrespondences(corrs1,corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet deletedCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.deletedCorrespondences(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#deletedCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet deletedCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().deletedCorrespondences(corrs1,corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(MappingVersion, MappingVersion)
	 */
	public ObjCorrespondenceSet sameCorrespondences(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.sameCorrespondences(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#sameCorrespondences(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet sameCorrespondences(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().sameCorrespondences(corrs1,corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(MappingVersion, MappingVersion)
	 */
	public float getCorrespondenceGrowth(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.getCorrespondenceGrowth(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceGrowth(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getCorrespondenceGrowth(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().getCorrespondenceGrowth(corrs1,corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getAddedCorrespondenceFraction(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.getAddedCorrespondenceFraction(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getAddedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getAddedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().getAddedCorrespondenceFraction(corrs1,corrs2);
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(MappingVersion, MappingVersion)
	 */
	public float getDeletedCorrespondenceFraction(MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException {
		return this.getDeletedCorrespondenceFraction(
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v1),
				APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v2));
	}
	
	/**
	 * @see MappingCorrespondenceEvolutionManager#getDeletedCorrespondenceFraction(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public float getDeletedCorrespondenceFraction(ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException {
		return EvolutionOperators.getInstance().getDeletedCorrespondenceFraction(corrs1,corrs2);
	}

	/**
	 * @see MappingCorrespondenceEvolutionManager#getCorrespondenceEvolutionConfidence(MappingVersionSet,CorrespondenceStabilitySet, String,String)
	 */
	public Map<SimplifiedGregorianCalendar, Float> getCorrespondenceEvolutionConfidence(CorrespondenceStability corrStab) throws GommaException, RemoteException {
		
			Map<SimplifiedGregorianCalendar,Float> confidenceMap =new Object2FloatOpenHashMap<SimplifiedGregorianCalendar>();
	//		DateFormat df=DateFormat.getDateInstance();
			int id=corrStab.getAnalysisID();
			
			StabilityAnalysis sa=APIFactory.getInstance().getStabilityAPI()
			        .getStabilityAnalysisSetWithAnalysisID(id)
			        .iterator().next();
			
			Mapping map=APIFactory.getInstance().getMappingAPI()
			        .getMappingSet(sa.getMapName(), QueryMatchOperations.EXACT_MATCH)
			        .iterator().next();
			
			SimplifiedGregorianCalendar dateFrom = sa.getFirstMappingVersionDate();
			SimplifiedGregorianCalendar dateTo=sa.getLastMappingVersionDate();
			MappingVersionSet mapVersionSet=APIFactory.getInstance().getMappingVersionAPI()
			             .getMappingVersionSet(map).getMappingVersionSet(dateFrom, dateTo, map);

			MappingVersionSet mvSet=mapVersionSet.getPreviouslyCreatedMappingVersionSet(sa.getNumberOfMappingVersions()-1, map);

			mvSet.addMappingVersion(mapVersionSet.getLastMappingVersion());            
			
			List<MappingVersion> mapList=mvSet.sort();
			
			for(MappingVersion mv : mapList) {
				ObjCorrespondenceSet corrs=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(
						mv,corrStab.getDomainAccessionNumber(),corrStab.getRangeAccessionNumber());
			if (corrs.getCollection().iterator().hasNext()==false) {
				confidenceMap.put(mv.getCreationDate(), (float)0.0);}
			else{
			 confidenceMap.put(mv.getCreationDate(), corrs.getCollection().iterator().next().getConfidence());
			}
		}
		return confidenceMap;
		
	}
	

	
}
