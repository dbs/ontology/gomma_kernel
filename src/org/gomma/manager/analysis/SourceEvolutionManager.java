/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

/**
 * The interface defines methods to retrieve evolutionary differences
 * and measures between sources and source versions. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceEvolutionManager extends Serializable, Remote {

	public ObjSet addedObjects   (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public ObjSet addedObjects(SourceVersion v1,SourceVersion v2) throws GommaException,RemoteException;
	
	public ObjSet deletedObjects(SourceVersion v1,SourceVersion v2) throws GommaException,RemoteException;
	
	public ObjSet deletedObjects (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public ObjSet sameObjects    (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public ObjSet toObsoleteObjects(ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public ObjSet toObsoleteObjects(SourceVersion v1,SourceVersion v2)throws GommaException, RemoteException;
	
	public ObjSet fusedObjects   (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public ObjSet fusedObjects(SourceVersion v1,SourceVersion v2) throws GommaException, RemoteException;
	
	public float getObjectGrowth          (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public float getAddedObjectFraction   (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public float getDeletedObjectFraction (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public float getAddDeleteRatio        (ObjSet objs1, ObjSet objs2) throws GommaException, RemoteException;
	
	public float getCoverage(ObjSet objs) throws GommaException,RemoteException;
}
