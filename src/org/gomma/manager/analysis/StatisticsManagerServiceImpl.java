package org.gomma.manager.analysis;

import java.rmi.RemoteException;
import java.util.List;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingStatisticsSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;

public class StatisticsManagerServiceImpl extends AbstractManagerService implements StatisticsManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4393058372922941242L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public StatisticsManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see StatisticsManager#storeSourceStatistics() 
	 */
	public boolean storeSourceStatistics() throws GommaException, RemoteException {
		return super.gommaService.storeSourceStatistics();
	}
	
	/**
	 * @see StatisticsManager#getSourceStatisticsSet()
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws GommaException, RemoteException {
		return super.gommaService.getSourceStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#storeSourceVersionStatistics()
	 */
	public boolean storeSourceVersionStatistics() throws GommaException, RemoteException {
		return super.gommaService.storeSourceVersionStatistics();
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet()
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws GommaException, RemoteException {
		return super.gommaService.getSourceVersionStatisticsSet();
	}
	
	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet(Source)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws GommaException, RemoteException {
		return super.gommaService.getSourceVersionStatisticsSet(s);
	}
	
	/**
	 * @see StatisticsManager#storeMappingStatistics()
	 */
	public boolean storeMappingStatistics() throws GommaException, RemoteException {
		return super.gommaService.storeMappingStatistics();
	}
	
	/**
	 * @see StatisticsManager#getMappingStatisticsSet()
	 */
	public MappingStatisticsSet getMappingStatisticsSet() throws GommaException, RemoteException {
		return super.gommaService.getMappingStatisticsSet();
	}
	
	
	
	
	
	
	/**
	 * @see StatisticsManager#getConfidenceFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return super.gommaService.getConfidenceFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getSupportFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return super.gommaService.getSupportFrequencies(v);
	}
	
	/**
	 * @see StatisticsManager#getDomainObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return super.gommaService.getDomainObjectFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getRangeObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws GommaException, RemoteException {
		return super.gommaService.getRangeObjectFrequencies(v);
	}

	/**
	 * @see StatisticsManager#getSourceVersionStatisticsSet(SourceVersionSet)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(
			SourceVersionSet set) throws GommaException, RemoteException {
		return super.gommaService.getSourceVersionStatisticsSet(set);
	}
}
