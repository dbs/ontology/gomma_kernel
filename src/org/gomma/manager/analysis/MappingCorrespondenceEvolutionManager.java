/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.util.date.SimplifiedGregorianCalendar;


/**
 * The interface defines methods to retrieve evolution-based
 * differences and measures between sets of mapping correspondences. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingCorrespondenceEvolutionManager extends Serializable, Remote {

	public ObjCorrespondenceSet addedCorrespondences   (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet addedCorrespondences   (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet deletedCorrespondences (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet deletedCorrespondences (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet sameCorrespondences    (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet sameCorrespondences    (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
	
	public float getCorrespondenceGrowth          (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public float getCorrespondenceGrowth          (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
	
	public float getAddedCorrespondenceFraction   (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public float getAddedCorrespondenceFraction   (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
	
	public float getDeletedCorrespondenceFraction (ObjCorrespondenceSet corrs1, ObjCorrespondenceSet corrs2) throws GommaException, RemoteException;
	
	public float getDeletedCorrespondenceFraction (MappingVersion v1, MappingVersion v2) throws GommaException, RemoteException;
    
	public Map<SimplifiedGregorianCalendar, Float> getCorrespondenceEvolutionConfidence(CorrespondenceStability corrStab) throws GommaException, RemoteException ;
	

}
