/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.analysis;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class EvolutionStatisticsManagerServiceImpl extends AbstractManagerService implements EvolutionStatisticsManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2567608246625088839L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public EvolutionStatisticsManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}

	/**
	 * @see EvolutionStatisticsManager#getLDSVersionStats(String, String, int)
	 */
	public Map<Integer,Integer> getLDSVersionStats(String pdsName, String objectType, int statType) throws GommaException, RemoteException {
		return super.gommaService.getLDSVersionStats(pdsName, objectType, statType);
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeLDSVersionStats(String, String)
	 */
	public void storeLDSVersionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		super.gommaService.storeLDSVersionStats(pdsName, objectType);
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeEvolutionStats(String, String)
	 */
	public void storeEvolutionStats(String pdsName, String objectType) throws GommaException, RemoteException {
		super.gommaService.storeEvolutionStats(pdsName, objectType);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getAverageEvolutionStats(String, String, int, int, Calendar, Calendar)
	 */
	public float getAverageEvolutionStats(String pdsName, String objectType, int action, int period, Calendar start, Calendar end) throws GommaException, RemoteException {
		return super.gommaService.getAverageEvolutionStats(pdsName, objectType, action, period, start, end);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStats(SourceVersionSet, int)
	 */
	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet set,int action) throws GommaException,RemoteException{
		return super.gommaService.getAllEvolutionStats(set, action);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStats(String, String, int, Calendar, Calendar)
	 */
	public Map<Integer,Integer> getAllEvolutionStats(String pdsName, String objectType, int action, Calendar start, Calendar end) throws GommaException, RemoteException {
		return super.gommaService.getAllEvolutionStats(pdsName, objectType, action, start, end);
	}
	
	/**
	 * @see EvolutionStatisticsManager#getObsoleteMapping(List<String>)
	 */
	public Map<String, List<String>> getObsoleteMapping(
			List<String> accessions) throws GommaException, RemoteException {
		return super.gommaService.getObsoleteMapping(accessions);
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeSourceVersionEvolution()
	 */
	public boolean storeSourceVersionEvolution() throws GommaException, RemoteException {
		return super.gommaService.storeSourceVersionEvolution();
	}
	
	/**
	 * @see EvolutionStatisticsManager#storeMappingVersionEvolution()
	 */
	public boolean storeMappingVersionEvolution() throws GommaException, RemoteException {
		return super.gommaService.storeMappingVersionEvolution();
	}
	
	/**
	 * @see EvolutionStatisticsManager#getSourceVersionEvolutionList(Source)
	 */
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws GommaException, RemoteException {
		return this.gommaService.getSourceVersionEvolutionList(s);
	}

	/**
	 * @see EvolutionStatisticsManager#getAllEvolutionStatsWithAvgData(SourceVersionSet, int, int)
	 */
	public Map<Map<Integer, Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet set,
			int action,int period) throws GommaException, RemoteException {
		return this.gommaService.getAllEvolutionStatsWithAvgData(set, action,period);
	}

	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionList(Mapping)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionEvolutionList(m);
	}

	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionListWithAvgData(Mapping,int)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionListWithAvgData(
			Mapping m, int period) throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionEvolutionListWithAvgData(m, period);
	}

	/**
	 * @see EvolutionStatisticsManager#getPartOfMappingVersionEvolutionListWithAvgData(MappingVersionSet,int)
	 */
	public List<MappingVersionEvolution> getPartOfMappingVersionEvolutionListWithAvgData(
			MappingVersionSet mvSet, int period) throws GommaException,
			RemoteException {
		return this.gommaService.getPartOfMappingVersionEvolutionListWithAvgData(mvSet, period);
	}
}
