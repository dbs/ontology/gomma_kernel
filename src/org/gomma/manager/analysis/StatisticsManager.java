package org.gomma.manager.analysis;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.gomma.exceptions.GommaException;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingStatisticsSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;

/**
 * The interface defines methods to create and retrieve pre-calculated
 * statistics of sources and mappings. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface StatisticsManager extends Serializable, Remote {

	public boolean storeSourceStatistics() throws GommaException, RemoteException;
	
	public SourceStatisticsSet getSourceStatisticsSet() throws GommaException, RemoteException;
	
	public boolean storeSourceVersionStatistics() throws GommaException, RemoteException;
	
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws GommaException, RemoteException;
	
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws GommaException, RemoteException;
	
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(SourceVersionSet set) throws GommaException, RemoteException;
	
	public boolean storeMappingStatistics() throws GommaException, RemoteException;
	
	public MappingStatisticsSet getMappingStatisticsSet() throws GommaException, RemoteException;
	
	
	
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws GommaException, RemoteException;
	
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws GommaException, RemoteException;
	
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws GommaException, RemoteException;
	
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws GommaException, RemoteException;

}
