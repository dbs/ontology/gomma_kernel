/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager;

import java.rmi.RemoteException;

import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;

/**
 * The class represents the abstract manager factory returning
 * available manager which semantically group functions of the 
 * system.  
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */ 
public abstract class AbstractManagerFactory {

	/**
	 * The item represents the source manager.
	 */
	private SourceManager sourceManager = null;
	
	/**
	 * The item represents the mapping manager.
	 */
	private MappingManager mappingManager = null;
	
	/**
	 * The item represents the mapping operation manager.
	 */
	private MappingOperationManager mappingOperationManager = null;
		
	/**
	 * The item represents the source evolution manager.
	 */
	private SourceEvolutionManager sourceEvolutionManager = null;
	
	/**
	 * The item represents the mapping object evolution manager.
	 */
	private MappingObjectEvolutionManager mappingObjectEvolutionManager = null;
	
	/**
	 * The item represents the mapping correspondence evolution manager.
	 */
	private MappingCorrespondenceEvolutionManager mappingCorrespondenceEvolutionManager = null;
	
	/**
	 * The item represents the evolution statistics manager.
	 */
	private EvolutionStatisticsManager evolutionStatisticsManager = null;
	
	/**
	 * The item represents the statistics manager. 
	 */
	private StatisticsManager statisticsManager = null;
	
	/**
	 * The item represents the match manager.
	 */
	private MatchManager matchManager = null;
	
	/**
	 * The item represents the work manager.
	 */
	private WorkManager workManager = null;
	
	/**
	 * The item represents the work manager.
	 */
	private StabilityManager stabilityManager = null;
	
	/**
	 * The item represents the distributed match manager.
	 */
	private DistributedMatchManager distributedMatchManager = null;
	
	/**
	 * The constructor initializes the class.
	 */
	protected AbstractManagerFactory() {}
	
	/**
	 * The method resets the manager initialization, i.e., all managers are 
	 * re-initialized when they are called next time.
	 * @throws RemoteException
	 */
	public void shutdown() throws RemoteException {
		this.sourceManager = null;
		this.mappingManager = null;
		this.mappingOperationManager = null;
		
		this.sourceEvolutionManager = null;
		this.mappingObjectEvolutionManager = null;
		this.mappingCorrespondenceEvolutionManager = null;
		this.evolutionStatisticsManager = null;
		
		this.matchManager = null;
		this.workManager = null;
		this.stabilityManager=null;
	}
	
	/**
	 * The method initializes and returns the source manager.
	 * @return source manager
	 * @throws RemoteException
	 */
	public SourceManager getSourceManager() throws RemoteException {
		if (this.sourceManager == null)
			this.sourceManager = initSourceManager();
		return this.sourceManager;
	}
	
	/**
	 * The method initializes and returns the mapping manager
	 * @return mapping manager
	 * @throws RemoteException
	 */
	public MappingManager getMappingManager() throws RemoteException {
		if (this.mappingManager == null) 
			this.mappingManager = initMappingManager();
		return this.mappingManager;
	}
	
	/**
	 * The method initializes and returns the mapping operation manager. 
	 * @return mapping operation manager
	 * @throws RemoteException
	 */
	public MappingOperationManager getMappingOperationManager() throws RemoteException {
		if (this.mappingOperationManager == null)
			this.mappingOperationManager = initMappingOperationManager();
		return this.mappingOperationManager;
	}
	
	
	/**
	 * The method initializes and returns the source evolution manager.
	 * @return source evolution manager
	 * @throws RemoteException
	 */
	public SourceEvolutionManager getSourceEvolutionManager() throws RemoteException {
		if (this.sourceEvolutionManager == null) 
			this.sourceEvolutionManager = initSourceEvolutionManager();
		return this.sourceEvolutionManager;
	}
	
	/**
	 * The method initializes and returns the mapping object evolution manager.
	 * @return mapping object evolution manager
	 * @throws RemoteException
	 */
	public MappingObjectEvolutionManager getMappingObjectEvolutionManager() throws RemoteException {
		if (this.mappingObjectEvolutionManager == null) 
			this.mappingObjectEvolutionManager = initMappingObjectEvolutionManager();
		return this.mappingObjectEvolutionManager;
	}
	
	/**
	 * The method initializes and returns the mapping correspondence evolution manager.
	 * @return mapping correspondence evolution manager
	 * @throws RemoteException
	 */
	public MappingCorrespondenceEvolutionManager getMappingCorrespondenceEvolutionManager() throws RemoteException {
		if (this.mappingCorrespondenceEvolutionManager == null)
			this.mappingCorrespondenceEvolutionManager = initMappingCorrespondenceEvolutionManager();
		return this.mappingCorrespondenceEvolutionManager;
	}
	
	/**
	 * The method initializes and returns the evolution statistics manager
	 * @return evolution statistics manager
	 * @throws RemoteException
	 */
	public EvolutionStatisticsManager getEvolutionStatisticsManager() throws RemoteException {
		if (this.evolutionStatisticsManager == null)
			this.evolutionStatisticsManager = initEvolutionStatisticsManager();
		return this.evolutionStatisticsManager;
	}
	
	/**
	 * The method initializes and returns the statistics manager. 
	 * @return statistics manager
	 * @throws RemoteException
	 */
	public StatisticsManager getStatisticsManager() throws RemoteException {
		if (this.statisticsManager == null) 
			this.statisticsManager = initStatisticsManager();
		return this.statisticsManager;
	}
	
	/**
	 * The method initializes and returns the match manager.
	 * @return match manager
	 * @throws RemoteException
	 */
	public MatchManager getMatchManager() throws RemoteException {
		if (this.matchManager == null)
			this.matchManager = initMatchManager();
		return matchManager;
	}
	
	/**
	 * The method returns a new distributed match manager instance.
	 * @return distributed match manager
	 * @throws RemoteException
	 */
	public DistributedMatchManager getDistributedMatchManager() throws RemoteException {
		if (this.distributedMatchManager == null) {
			this.distributedMatchManager = initDistributedMatchManager();
		}
		return distributedMatchManager;
	}
	
	
	/**
	 * The method initializes and returns the work manager.
	 * @return work manager
	 * @throws RemoteException
	 */
	public WorkManager getWorkManager() throws RemoteException {
		if (this.workManager == null) 
			this.workManager = initWorkManager();
		return this.workManager;
	}
	
	/**
	 * The method initializes and returns the stability manager.
	 * @return stability manager
	 * @throws RemoteException
	 */
	public StabilityManager getStabilityManager() throws RemoteException {
		if (this.stabilityManager == null) 
			this.stabilityManager = initStabilityManager();
		return this.stabilityManager;
	}
	
	
	/**
	 * The method returns new source manager instance.
	 * @return source manager
	 * @throws RemoteException
	 */
	protected abstract SourceManager initSourceManager() throws RemoteException;
	
	/**
	 * The method returns a new mapping manager instance.
	 * @return mapping manager
	 * @throws RemoteException
	 */
	protected abstract MappingManager initMappingManager() throws RemoteException;
	
	/**
	 * This method returns a new mapping operation manager instance.
	 * @return mapping operation manager
	 * @throws RemoteException
	 */
	protected abstract MappingOperationManager initMappingOperationManager() throws RemoteException;
	
	/**
	 * The method returns a new source evolution manager instance.
	 * @return source evolution manager
	 * @throws RemoteException
	 */
	protected abstract SourceEvolutionManager initSourceEvolutionManager() throws RemoteException;
	
	/**
	 * The method returns a new mapping object evolution manager instance.
	 * @return mapping object evolution manager
	 * @throws RemoteException
	 */
	protected abstract MappingObjectEvolutionManager initMappingObjectEvolutionManager() throws RemoteException;
	
	/**
	 * The method returns a new mapping correspondence evolution manager instance.
	 * @return mapping correspondence evolution manager
	 * @throws RemoteException
	 */
	protected abstract MappingCorrespondenceEvolutionManager initMappingCorrespondenceEvolutionManager() throws RemoteException;
	
	/**
	 * The method returns a new evolution statistics manager instance.
	 * @return evolution statistics manager instance
	 * @throws RemoteException
	 */
	protected abstract EvolutionStatisticsManager initEvolutionStatisticsManager() throws RemoteException;
	
	/**
	 * The method returns a new statistics manager instance.
	 * @return statistics manager instance
	 * @throws RemoteException
	 */
	protected abstract StatisticsManager initStatisticsManager() throws RemoteException;
	
	/**
	 * The method returns a new match manager instance.
	 * @return match manager
	 * @throws RemoteException
	 */
	protected abstract MatchManager initMatchManager() throws RemoteException;

	/**
	 * The method returns a new distributed match manager instance.
	 * @return distributed match manager
	 * @throws RemoteException
	 */
	protected abstract DistributedMatchManager initDistributedMatchManager() throws RemoteException;
	
	/**
	 * The method returns a new work manager instance.
	 * @return work manager
	 * @throws RemoteException
	 */
	protected abstract WorkManager initWorkManager() throws RemoteException;
	
	/**
	 * The method returns a new stability manager instance.
	 * @return stability manager
	 * @throws RemoteException
	 */
	protected abstract StabilityManager initStabilityManager() throws RemoteException;
	
	
}
