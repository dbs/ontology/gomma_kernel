/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager;

import java.rmi.RemoteException;

import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.EvolutionStatisticsManagerModuleImpl;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManagerModuleImpl;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManagerModuleImpl;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManagerModuleImpl;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.analysis.StatisticsManagerModuleImpl;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.distributedmatching.DistributedMatchManagerModuleImpl;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingManagerModuleImpl;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MappingOperationManagerModuleImpl;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.mapping.MatchManagerModuleImpl;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.source.SourceManagerModuleImpl;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.stability.StabilityManagerModuleImpl;
import org.gomma.manager.work.WorkManager;
import org.gomma.manager.work.WorkManagerModuleImpl;

/**
 * The class represents the kernel-specific manager factory.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class ManagerModuleFactory extends AbstractManagerFactory {

	private volatile static ManagerModuleFactory singleton = null;
	
	
	public static ManagerModuleFactory getInstance() {
		if (singleton == null) {
			synchronized (ManagerModuleFactory.class) {
				if (singleton == null)
					singleton = new ManagerModuleFactory();
			}
		}
		return singleton;
	}
	
	public static void stop() {
		singleton = null;
	}
	
	/**
	 * The constructor initializes the class.
	 */
	private ManagerModuleFactory() {
		super();
	}
	
	/**
	 * @see AbstractManagerFactory#initSourceManager()
	 */
	protected SourceManager initSourceManager() throws RemoteException {
		return new SourceManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingManager()
	 */
	protected MappingManager initMappingManager() throws RemoteException {
		return new MappingManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingOperationManager()
	 */
	protected MappingOperationManager initMappingOperationManager() throws RemoteException {
		return new MappingOperationManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initSourceEvolutionManager()
	 */
	protected SourceEvolutionManager initSourceEvolutionManager() throws RemoteException {
		return new SourceEvolutionManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingObjectEvolutionManager()
	 */
	protected MappingObjectEvolutionManager initMappingObjectEvolutionManager() throws RemoteException {
		return new MappingObjectEvolutionManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingCorrespondenceEvolutionManager()
	 */
	protected MappingCorrespondenceEvolutionManager initMappingCorrespondenceEvolutionManager() throws RemoteException {
		return new MappingCorrespondenceEvolutionManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initEvolutionStatisticsManager()
	 */
	protected EvolutionStatisticsManager initEvolutionStatisticsManager() throws RemoteException {
		return new EvolutionStatisticsManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initStatisticsManager()
	 */
	protected StatisticsManager initStatisticsManager() throws RemoteException {
		return new StatisticsManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initMatchManager()
	 */
	protected MatchManager initMatchManager() throws RemoteException {
		return new MatchManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initWorkManager()
	 */
	protected WorkManager initWorkManager() throws RemoteException {
		return new WorkManagerModuleImpl();
	}
	
	/**
	 * @see AbstractManagerFactory#initStabilityManager()
	 */
	protected StabilityManager initStabilityManager()throws RemoteException{
		return new StabilityManagerModuleImpl();
	}

	/**
	 * @see AbstractManagerFactory#initDistributedMatchManager()
	 */
	protected DistributedMatchManager initDistributedMatchManager() throws RemoteException {
		return new DistributedMatchManagerModuleImpl();
	}
}
