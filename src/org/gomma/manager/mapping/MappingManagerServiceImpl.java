/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.io.FileTypes;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingManagerServiceImpl extends AbstractManagerService implements MappingManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7051621866507987921L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public MappingManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see MappingManager#getMappingMetadata()
	 */
	public MappingSet getMappingSet() throws GommaException, RemoteException {
		return super.gommaService.getMappingSet();
	}
	
	/**
	 * @see MappingManager#getMappingSet(String, org.gomma.api.util.QueryMatchOperations)
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getMappingSet(name, op);
	}
	
	/**
	 * @see MappingManager#getMappingSet(Source, Source)
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws GommaException, RemoteException {
		return super.gommaService.getMappingSet(domainSource,rangeSource);
	}
	
	/**
	 * @see MappingManager#insertMapping(String, Source, Source, boolean, boolean, String, String, String, String, AttributeValueSet)
	 */
	public boolean insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType, AttributeValueSet set) throws GommaException, RemoteException {
		return this.gommaService.insertMapping(name, domainSource, rangeSource, isInstanceMapping, isDerived, mapClass, mapMethod, mapTool, mapType, set);
	}
	
	/**
	 * @see MappingManager#updateMapping(Mapping)
	 */
	public boolean updateMapping(Mapping m) throws GommaException, RemoteException {
		return super.gommaService.updateMapping(m);
	}
	
	/**
	 * @see MappingManager#deleteMapping(Mapping)
	 */
	public boolean deleteMapping (Mapping m) throws GommaException, RemoteException {
		return super.gommaService.deleteMapping(m);
	}
	
	
	
	
	/**
	 * @see MappingManager#getMappingVersionSet()
	 */
	public MappingVersionSet getMappingVersionSet() throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionSet();
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionSet(m);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, String, QueryMatchOperations)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionSet(m,name,op);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException {
		return this.gommaService.getMappingVersionSet(m, minDate, maxDate);
	}
	
	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, int, int, int, SourceVersionSet, SourceVersionSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate,
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs,
			SourceVersionSet domainSourceVersionSet, SourceVersionSet rangeSourceVersionSet)  throws GommaException, RemoteException {
		return this.gommaService.insertMappingVersion(m, name, creationDate, lowestConfidence, lowestSupport, nCorrs, 
				nDomainObjs, nRangeObjs, domainSourceVersionSet, rangeSourceVersionSet);
	}

	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, ObjCorrespondenceSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate, float minConf, int minSupport, ObjCorrespondenceSet corrSet) throws GommaException, RemoteException {
		return super.gommaService.insertMappingVersion(m, name, creationDate, minConf, minSupport, corrSet);
	}
	
	/**
	 * @see MappingManager#updateMappingVersion(MappingVersion)
	 */
	public boolean updateMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.updateMappingVersion(v);
	}
	
	/**
	 * @see MappingManager#deleteMappingVersion(MappingVersion)
	 */
	public boolean deleteMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		return this.gommaService.deleteMappingVersion(v);
	}
	
	
	
	
	
	/**
	 * @see MappingManager#importMapping(String)
	 */
	public void	importMapping(String mappingFile) throws GommaException, RemoteException {
		super.gommaService.importMapping(mappingFile);
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, FileTypes type) throws GommaException, RemoteException {
		super.gommaService.exportMappingVersion(mappingFileName, v, type);
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, ObjCorrespondenceSet, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, ObjCorrespondenceSet corrs, FileTypes type) throws GommaException, RemoteException {
		super.gommaService.exportMappingVersion(mappingFileName, v, corrs, type);
	}
	
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws GommaException, RemoteException {
		return super.gommaService.getObjectCorrespondenceSet(v);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSetSimple(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws GommaException, RemoteException{
		return super.gommaService.getObjectCorrespondenceSetSimple(v);
	}
	
	/**
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, ObjCorrespondenceSet)
	 */
	public boolean insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return super.gommaService.insertObjectCorrespondenceSet(v,set);
	}

	/**
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, int,boolean)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,
			int objId, boolean isDomain) throws GommaException, RemoteException {
		return super.gommaService.getObjectCorrespondenceSet(v, objId, isDomain);
	}
	/**
	 * @throws GommaException 
	 * @throws RemoteException 
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,float confThreshold) throws RemoteException, GommaException {
		return super.gommaService.getObjectCorrespondenceSet(v, confThreshold);
	}
	/**
	 * @throws GommaException 
	 * @throws RemoteException 
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, String[])
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,String[] corrTypes) throws RemoteException, GommaException {
		
		return super.gommaService.getObjectCorrespondenceSet(v, corrTypes);
	}
	/**
	 * @see MappingManager#insertObjectCorrespondence(MappingVersion, String, String)
	 */
	public ObjCorrespondence getObjectCorrespondence(MappingVersion v,
			String dAcc, String rAcc) throws GommaException, RemoteException {
		return super.gommaService.getObjectCorrespondence(v, dAcc, rAcc);
	}
	
}
