/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.io.FileTypes;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The interface defines methods to manage mappings including their correspondences
 * between sources and source versions.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingManager extends Serializable, Remote {

	/**
	 * M A P P I N G   M A N A G E M E N T
	 */
	
	
	public MappingSet getMappingSet() throws GommaException, RemoteException;
	
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException;
	
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws GommaException, RemoteException;
	
	public boolean insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType, AttributeValueSet set) throws GommaException, RemoteException;
	
	public boolean updateMapping(Mapping m) throws GommaException, RemoteException;
	
	public boolean deleteMapping(Mapping m) throws GommaException, RemoteException;
	
	
	
	public void	importMapping(String mappingFile) throws GommaException, RemoteException;
	
	public void exportMappingVersion(String mappingFileName, MappingVersion v, FileTypes type) throws GommaException, RemoteException;
	
	public void exportMappingVersion(String mappingFileName, MappingVersion v, ObjCorrespondenceSet corrs, FileTypes type) throws GommaException, RemoteException;
	
	
	
	/**
	 * M A P P I N G   V E R S I O N   M A N A G E M E N T
	 */
	
	
	public MappingVersionSet getMappingVersionSet() throws GommaException, RemoteException;
	
	public MappingVersionSet getMappingVersionSet(Mapping m) throws GommaException, RemoteException;
	
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException;
	
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException;
	
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate,
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs,
			SourceVersionSet domainSourceVersionSet, SourceVersionSet rangeSourceVersionSet)  throws GommaException, RemoteException;
	
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate, float minConf, int minSupport, ObjCorrespondenceSet corrSet) throws GommaException, RemoteException;
	
	public boolean updateMappingVersion(MappingVersion v) throws GommaException, RemoteException;
	
	public boolean deleteMappingVersion(MappingVersion v) throws GommaException, RemoteException;
	
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   M A N A G E M E N T
	 */
	
	
	public boolean insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,int objId,boolean isDomain) throws GommaException, RemoteException;
	
	public ObjCorrespondence getObjectCorrespondence(MappingVersion v,String domainAcc,String rangeAcc) throws GommaException, RemoteException;

	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, float confThreshold) throws RemoteException, GommaException;
	
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, String[] corrTypes) throws RemoteException, GommaException;
}
