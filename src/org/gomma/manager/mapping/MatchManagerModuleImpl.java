/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.MatchOperators;
import org.gomma.util.math.AggregationFunction;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MatchManagerModuleImpl implements MatchManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 503861593591913436L;

	/**
	 * The constructor initializes the class.
	 */
	public MatchManagerModuleImpl() {}

	
	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return MatchOperators.match(s1,s2,simFunc,aggFunc,simThreshold,props);
		
	}
	
	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return MatchOperators.match(s1,s2,simFunc,aggFunc,simThreshold,props,initialMap);
		
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return MatchOperators.match(domain, range, simFunc, aggFunc, simThreshold, props);
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return MatchOperators.match(domain, range, simFunc, aggFunc, simThreshold, props, initialMap);
	}
}
