/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MatchManagerServiceImpl extends AbstractManagerService implements MatchManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2474129209536446012L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public MatchManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}

	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return super.gommaService.match(s1, s2, simFunc, aggFunc, simThreshold, props);
	}
	 
	/**
	 * @see MatchManager#match(SourceVersionStructure, SourceVersionStructure, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return super.gommaService.match(s1, s2, simFunc, aggFunc, simThreshold, props, initialMap);
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return super.gommaService.match(domain, range, simFunc, aggFunc, simThreshold, props);
	}
	
	/**
	 * @see MatchManager#match(ObjSet, ObjSet, SimilarityFunction, AggregationFunction, float, MatchProperties, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException {
		return super.gommaService.match(domain, range, simFunc, aggFunc, simThreshold, props, initialMap);
	}
}
