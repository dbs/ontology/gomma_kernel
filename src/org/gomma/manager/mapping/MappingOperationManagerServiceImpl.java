package org.gomma.manager.mapping;

import java.rmi.RemoteException;
import java.util.List;

import org.gomma.GommaService;
import org.gomma.clustering.CorrespondenceClusterTypes;
import org.gomma.exceptions.GommaException;
import org.gomma.filter.FilterTypes;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.CorrespondenceClusterFunction;

/**
 * The class represents the service-based implementation of the 
 * mapping operation manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingOperationManagerServiceImpl extends AbstractManagerService implements MappingOperationManager {

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public MappingOperationManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}

	
	/**
	 * M A P P I N G   V E R S I O N   O P E R A T I O N S
	 */
	
	/**
	 * @see MappingOperationManager#union(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion union(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return super.gommaService.union(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#intersect(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion intersect(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return super.gommaService.intersect(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#diff(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion diff(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return super.gommaService.diff(newMappingVersionName, v1, v2, resultObjCorrespondences);
	}
	
	/**
	 * @see MappingOperationManager#majority(String, MappingVersionSet, int, ObjCorrespondenceSet)
	 */
	public MappingVersion majority(String newMappingVersionName, MappingVersionSet set, int nMin, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		return super.gommaService.majority(newMappingVersionName, set, nMin, resultObjCorrespondences);
	}
	
	
	

	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return super.gommaService.union(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return super.gommaService.union(o1, o2, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return super.gommaService.union(sets, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingOperationManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return super.gommaService.union(sets, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	/**
	 * @see MappingOperationManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return super.gommaService.intersect(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return super.gommaService.intersect(o1, o2, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingOperationManager#diff(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet diff(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return super.gommaService.diff(o1, o2);
	}
	
	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin) throws GommaException, RemoteException {
		return super.gommaService.majority(o, nMin);
	}
	
	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return super.gommaService.majority(o, nMin, supportAggFunc, confidenceAggFunc);
	}
	
	/**
	 * @see MappingOperationManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return super.gommaService.majority(sets, nMin, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	/**
	 * C O R R E S P O N D E N C E   O P E R A T I O N S
	 */
	
	/**
	 * @see MappingOperationManager#compose(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet compose(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return super.gommaService.compose(o1, o2);
	}
	/** 
	 * @see MappingOperationManager#hierarchicalCompose(ObjCorrespondenceSet, ObjCorrespondenceSet,SourceVersionStructure,SourceVersionStructure)
	 */
	public ObjCorrespondenceSet hierarchicalCompose(ObjCorrespondenceSet domainMap,	ObjCorrespondenceSet rangeMap,
													SourceVersionStructure rangeMapDomain, SourceVersionStructure rangeMapRange) throws RemoteException, GommaException {
		
		return super.gommaService.hierarchicalCompose(domainMap, rangeMap,rangeMapDomain,rangeMapRange);
	}
	/**
	 * @see MappingOperationManager#inverse(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet inverse (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return super.gommaService.inverse(o);
	}
	
	/**
	 * @see MappingOperationManager#domain(ObjCorrespondenceSet)
	 */
	public ObjSet domain (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return super.gommaService.domain(o);
	}
	
	/**
	 * @see MappingOperationManager#range(ObjCorrespondenceSet)
	 */
	public ObjSet range (ObjCorrespondenceSet o) throws GommaException, RemoteException {
		return super.gommaService.range(o);
	}
	
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   F I L T E R I N G
	 */
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(set, minOccurrence, minConf);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(set, type, threshold);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(MappingVersion, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(v, minOccurrence, minConf);
	}
	
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(MappingVersion, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, FilterTypes type, float threshold) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(v, type, threshold);
	}
	/**
	 * @see MappingOperationManager#filterObjectCorrespondenceSet(MappingVersion, FilterTypes, float, boolean)
	 */	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold,boolean isDomain) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(set, type, threshold, isDomain);
	}
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(set, type, n);
	}
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n, boolean isDomain) throws GommaException, RemoteException {
		return super.gommaService.filterObjectCorrespondenceSet(set, type, n, isDomain);
	}
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   C L U S T E R I N G
	 */
	
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return super.gommaService.clusterObjectCorrespondenceSet(domain, range, set);
	}
	
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet, CorrespondenceClusterTypes, CorrespondenceClusterFunction)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set, CorrespondenceClusterTypes type, CorrespondenceClusterFunction func) throws GommaException, RemoteException {
		return super.gommaService.clusterObjectCorrespondenceSet(domain, range, set, type, func);
	}
}
