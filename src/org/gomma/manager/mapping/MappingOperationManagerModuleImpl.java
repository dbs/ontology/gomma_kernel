package org.gomma.manager.mapping;

import java.rmi.RemoteException;
import java.util.List;

import org.gomma.api.APIFactory;
import org.gomma.clustering.CorrespondenceClusterTypes;
import org.gomma.exceptions.GommaException;
import org.gomma.filter.FilterFactory;
import org.gomma.filter.FilterTypes;
import org.gomma.filter.filter.Filter;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.ClusteringOperators;
import org.gomma.operator.MappingOperators;
import org.gomma.operator.SetOperators;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.CorrespondenceClusterFunction;

/**
 * The class represents the kernel-specific implementation of the
 * mapping operation manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingOperationManagerModuleImpl implements MappingOperationManager {

	/**
	 * The constructor initializes the class.
	 */
	public MappingOperationManagerModuleImpl() {}
	
	
	/**
	 * M A P P I N G   V E R S I O N   O P E R A T I O N S
	 */
	
	/**
	 * @see MappingManager#union(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion union(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		if (newMappingVersionName==null) 
			throw new NullPointerException("Could not resolve the new mapping version name.");
		
		return SetOperators.union(newMappingVersionName,v1,v2,resultObjCorrespondences);
	}

	/**
	 * @see MappingManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return SetOperators.union(o1,o2);
	}
	
	/**
	 * @see MappingManager#union(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return SetOperators.union(o1,o2,supportAggFunc,confidenceAggFunc);
	}
	
	/**
	 * @see MappingManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc,AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return SetOperators.union(sets, supportAggFunc, confidenceAggFunc);
	}

	/**
	 * @see MappingManager#union(ObjCorrespondenceSet[], AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet union(ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return SetOperators.union(sets, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	
	
	
	
	/**
	 * @see MappingManager#intersect(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)t(
	 */
	public MappingVersion intersect(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		if (newMappingVersionName==null) 
			throw new NullPointerException("Could not resolve the new mapping version name.");
		
		return SetOperators.intersect(newMappingVersionName,v1,v2,resultObjCorrespondences);
	}

	/**
	 * @see MappingManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return SetOperators.intersect(o1,o2);
	}
	
	/**
	 * @see MappingManager#intersect(ObjCorrespondenceSet, ObjCorrespondenceSet, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return SetOperators.intersect(o1,o2,supportAggFunc,confidenceAggFunc);
	}
	
	
	
	
	
	/**
	 * @see MappingManager#diff(String, MappingVersion, MappingVersion, ObjCorrespondenceSet)
	 */
	public MappingVersion diff(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException {
		if (newMappingVersionName==null) 
			throw new NullPointerException("Could not resolve the new mapping version name.");
		
		return SetOperators.diff(newMappingVersionName,v1,v2,resultObjCorrespondences);
	}
	
	/**
	 * @see MappingManager#diff(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet diff(ObjCorrespondenceSet o1, ObjCorrespondenceSet o2) throws GommaException, RemoteException {
		return SetOperators.diff(o1,o2);
	}
	
	
	
	/**
	 * @see MappingManager#majority(String, MappingVersionSet, int, ObjCorrespondenceSet)
	 */
	public MappingVersion majority(String newMappingVersionName, MappingVersionSet mArray, int nMin, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException {
		return SetOperators.majority(newMappingVersionName,mArray,nMin,resultObjCorrespondences);
	}

	
	/**
	 * @see MappingManager#majority(ObjCorrespondenceSet[], int)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin) throws GommaException, RemoteException {
		return SetOperators.majority(o,nMin);
	}
	
	/**
	 * @see MappingManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] o, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException {
		return SetOperators.majority(o,nMin,supportAggFunc,confidenceAggFunc);
	}

	/**
	 * @see MappingManager#majority(ObjCorrespondenceSet[], int, AggregationFunction, AggregationFunction, boolean)
	 */
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode) throws GommaException, RemoteException {
		return SetOperators.majority(sets, nMin, supportAggFunc, confidenceAggFunc, strictMode);
	}
	
	
	/**
	 * C O R R E S P O N D E N C E   O P E R A T I O N S
	 */
	
	/**
	 * @see MappingManager#compose(ObjCorrespondenceSet, ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet compose(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2)  throws GommaException, RemoteException {
		return MappingOperators.compose(set1,set2);
	}
	/**
	 * @see MappingManager#compose(ObjCorrespondenceSet, ObjCorrespondenceSet,SourceVersionStructure,SourceVersionStructure)
	 */
	public ObjCorrespondenceSet hierarchicalCompose(ObjCorrespondenceSet domainMap, ObjCorrespondenceSet rangeMap,
													SourceVersionStructure rangeMapDomain,SourceVersionStructure rangeMapRange) 
													throws RemoteException,	GommaException {
		return MappingOperators.hierarchicalCompose(domainMap, rangeMap, rangeMapDomain, rangeMapRange);
	}
	
	/**
	 * @see MappingManager#inverse(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet inverse(ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return MappingOperators.inverse(set);
	}
	
	/**
	 * @see MappingManager#domain(ObjCorrespondenceSet)
	 */
	public ObjSet domain(ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return MappingOperators.getInstance().domain(set);
	}
	
	/**
	 * @see MappingManager#range(ObjCorrespondenceSet)
	 */
	public ObjSet range(ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return MappingOperators.getInstance().range(set);
	}
	
	
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   F I L T E R I N G
	 */

	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return SetOperators.filter(set, minOccurrence, minConf);
	}
	
	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold) throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, threshold, false, 0);
		return filter.filter(set);
	}
	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(ObjCorrespondenceSet, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold,boolean isDomain) throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, threshold, isDomain, 0);
		return filter.filter(set);
	}	
	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(MappingVersion, int, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConf) throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v, minOccurrence, minConf);
	}
	
	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(MappingVersion, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, FilterTypes type, float threshold) throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, threshold, false, 0);
		return filter.filter(APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v));
	}
	
	/**
	 * @see MappingManager#filterObjectCorrespondenceSet(MappingVersion, FilterTypes, float)
	 */
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, FilterTypes type, float threshold, boolean isDomain) throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, threshold, isDomain, 0);
		return filter.filter(APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v));
	}
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n)throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, 0.0F, false, n);				
		return filter.filter(set);
	}
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n, boolean isDomain)throws GommaException, RemoteException {
		Filter filter = FilterFactory.getFilter(type, 0.0F, isDomain, n);				
		return filter.filter(set);
	}
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   C L U S T E R I N G
	 */
	
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		return ClusteringOperators.cluster(domain, range, set);
	}
	
	/**
	 * @see MappingOperationManager#clusterObjectCorrespondenceSet(SourceVersionStructure, SourceVersionStructure, ObjCorrespondenceSet, CorrespondenceClusterTypes, CorrespondenceClusterFunction)
	 */
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set, CorrespondenceClusterTypes type, CorrespondenceClusterFunction func) throws GommaException, RemoteException {
		return ClusteringOperators.cluster(domain, range, set, type, func);
	}
}
