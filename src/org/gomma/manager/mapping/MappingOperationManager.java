package org.gomma.manager.mapping;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.gomma.clustering.CorrespondenceClusterTypes;
import org.gomma.exceptions.GommaException;
import org.gomma.filter.FilterTypes;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;
import org.gomma.util.math.CorrespondenceClusterFunction;

/**
 * The interface defines methods to operate on and manipulate mappings including their correspondences
 * between sources and source versions.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingOperationManager extends Remote {

	/**
	 * M A P P I N G   V E R S I O N   O P E R A T I O N S
	 */
	
	public MappingVersion union(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException;
	
	public MappingVersion intersect(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException;
	
	public MappingVersion diff(String newMappingVersionName, MappingVersion v1, MappingVersion v2, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException;
	
	public MappingVersion majority(String newMappingVersionName, MappingVersionSet set, int nMin, ObjCorrespondenceSet resultObjCorrespondences) throws GommaException, RemoteException;
	
	
	
	public ObjCorrespondenceSet union(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet union(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet union (ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc)  throws GommaException, RemoteException;	
		
	public ObjCorrespondenceSet union (ObjCorrespondenceSet[] sets, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode)  throws GommaException, RemoteException;	
	
	
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet intersect(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet diff(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] set, int nMin) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet majority(ObjCorrespondenceSet[] set, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet majority (ObjCorrespondenceSet[] sets, int nMin, AggregationFunction supportAggFunc, AggregationFunction confidenceAggFunc, boolean strictMode)  throws GommaException, RemoteException;	
	
	
	/**
	 * C O R R E S P O N D E N C E   O P E R A T I O N S
	 */
	
	public ObjCorrespondenceSet compose(ObjCorrespondenceSet set1, ObjCorrespondenceSet set2) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet hierarchicalCompose(ObjCorrespondenceSet domainMap, ObjCorrespondenceSet rangeMap, SourceVersionStructure rangeMapDomain, SourceVersionStructure rangeMapRange) throws RemoteException, GommaException;
	
	public ObjCorrespondenceSet inverse(ObjCorrespondenceSet set) throws GommaException, RemoteException;
	
	public ObjSet domain(ObjCorrespondenceSet set) throws GommaException, RemoteException;
	
	public ObjSet range(ObjCorrespondenceSet set) throws GommaException, RemoteException;
	
	
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   F I L T E R I N G
	 */
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, int minOccurrence, float minConf) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, float threshold, boolean isDomain) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(ObjCorrespondenceSet set, FilterTypes type, int n, boolean isDomain) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConf) throws GommaException, RemoteException;
	
	public ObjCorrespondenceSet filterObjectCorrespondenceSet(MappingVersion v, FilterTypes type, float threshold) throws GommaException, RemoteException;
	
	
	
	/**
	 * O B J E C T   C O R R E S P O N D E N C E   C L U S T E R I N G
	 */
	
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set) throws GommaException, RemoteException;
	
	public List<ObjCorrespondenceSet> clusterObjectCorrespondenceSet(SourceVersionStructure domain, SourceVersionStructure range, ObjCorrespondenceSet set, CorrespondenceClusterTypes type, CorrespondenceClusterFunction func) throws GommaException, RemoteException;

}
