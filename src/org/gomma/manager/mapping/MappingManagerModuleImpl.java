/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.gomma.api.APIFactory;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.FileTypes;
import org.gomma.io.exporter.MappingExportFactory;
import org.gomma.io.exporter.MappingExporter;
import org.gomma.io.importer.MappingParser;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the kernel-specific implementation of the
 * mapping manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingManagerModuleImpl implements MappingManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5960684515852655407L;

	/**
	 * The constructor initializes the class.
	 */
	public MappingManagerModuleImpl() {}
	
	
	/**
	 * @see MappingManager#getMappingSet()
	 */
	public MappingSet getMappingSet() throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingAPI().getMappingSet();
	}
	
	/**
	 * @see MappingManager#getMappingSet(String, QueryMatchOperations)
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingAPI().getMappingSet(name, op);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, String, QueryMatchOperations)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m, name, op);
	}
	
	/**
	 * @see MappingManager#getMappingSet(Source, Source)
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingAPI().getMappingSet(domainSource, rangeSource);
	}
	
	/**
	 * @see MappingManager#insertMapping(String, Source, Source, boolean, boolean, String, String, String, String, AttributeValueSet)
	 */
	public boolean insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType, AttributeValueSet set) throws GommaException, RemoteException {
		if (APIFactory.getInstance().getMappingAPI().insertMapping(
				name, domainSource, rangeSource, isInstanceMapping, 
				isDerived, mapClass, mapMethod, mapTool, mapType)!=0) {
			
			if (set.size()==0) return true;
			
			MappingSet mSet = APIFactory.getInstance().getMappingAPI().getMappingSet(domainSource, rangeSource);
			Mapping m = mSet.getMapping(name);
			
			return APIFactory.getInstance().getMappingAPI().insertMappingAttributeValues(m, set)!=0;
		}
		return false;
	}
	
	/**
	 * @see MappingManager#updateMapping(Mapping)
	 */
	public boolean updateMapping(Mapping m) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingAPI().updateMapping(m)!=0;
	}
	
	/**
	 * @see MappingManager#deleteMapping(Mapping)
	 */
	public boolean deleteMapping(Mapping m) throws GommaException, RemoteException {
		
		for (MappingVersion v : this.getMappingVersionSet(m)) {
			APIFactory.getInstance().getObjCorrespondenceAPI().deleteObjectCorrespondenceSet(v);
			APIFactory.getInstance().getMappingVersionAPI().deleteMappingVersionSourceSet(v);
			APIFactory.getInstance().getMappingVersionAPI().deleteMappingVersion(v);
		}
		
		APIFactory.getInstance().getMappingAPI().deleteMappingAttributeValues(m);
		return APIFactory.getInstance().getMappingAPI().deleteMapping(m)!=0;
	}
	
	/**
	 * @see MappingManager#updateMappingMetadata(Mapping)
	 */
	public boolean updateMappingMetadata(Mapping map) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingAPI().updateMapping(map)!=0;
	}
	
	
	
	
	
	/**
	 * @see MappingManager#importMapping(String)
	 */
	public void importMapping(String mappingFile) throws GommaException, RemoteException {
		MappingParser p = new MappingParser(mappingFile);
		p.importMapping();
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, FileTypes type) throws GommaException, RemoteException {
		if (v.getID()==MappingVersion.DEFAULT_MAPPING_VERSION_ID) 
			throw new GommaException("The mapping could not be exported because it exists temporarily and, "+
					"thus, is not stored within the repository.");
		try {
			PrintWriter w = new PrintWriter(mappingFileName);
			MappingExporter exporter = MappingExportFactory.getExporter(type);
			exporter.exportMappingVersion(w,v);
			w.flush();
			w.close();
		} catch (IOException e) {
			throw new GommaException(e.getMessage());
		}
	}
	
	/**
	 * @see MappingManager#exportMappingVersion(String, MappingVersion, ObjCorrespondenceSet, FileTypes)
	 */
	public void exportMappingVersion(String mappingFileName, MappingVersion v, ObjCorrespondenceSet corrs, FileTypes type) throws GommaException, RemoteException {
		try {
			PrintWriter w = new PrintWriter(mappingFileName);
			MappingExporter exporter = MappingExportFactory.getExporter(type);
			exporter.exportMappingVersion(w,v,corrs);
			w.flush();
			w.close();
		} catch (IOException e) {
			throw new GommaException(e.getMessage());
		}
	}
	
	
	
	/**
	 * @see MappingManager#getMappingVersionSet()
	 */
	public MappingVersionSet getMappingVersionSet() throws GommaException, RemoteException {
		
		MappingVersionSet set = new MappingVersionSet();
		
		for (Mapping m : APIFactory.getInstance().getMappingAPI().getMappingSet()) 
			for (MappingVersion v : this.getMappingVersionSet(m))
				set.addMappingVersion(v);
		
		return set;
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m);
	}
	
	/**
	 * @see MappingManager#getMappingVersionSet(Mapping, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m,minDate,maxDate);
	}
	
	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, int, int, int, SourceVersionSet, SourceVersionSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate,
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs,
			SourceVersionSet domainSourceVersionSet, SourceVersionSet rangeSourceVersionSet) throws GommaException, RemoteException {
		
		if (APIFactory.getInstance().getMappingVersionAPI().insertMappingVersion(
				m, name, creationDate, lowestConfidence, lowestSupport, nCorrs, nDomainObjs, nRangeObjs)!=0) {
			
			MappingVersionSet set = this.getMappingVersionSet(m);
			MappingVersion v = set.getMappingVersion(name);
			
			return 
				APIFactory.getInstance().getMappingVersionAPI().insertMappingVersionSourceSet(
						v, domainSourceVersionSet, true)!=0 &&
				APIFactory.getInstance().getMappingVersionAPI().insertMappingVersionSourceSet(
						v, rangeSourceVersionSet, false)!=0;
				
		}
		return false;
	}

	
	/**
	 * @see MappingManager#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, ObjCorrespondenceSet)
	 */
	public boolean insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate, float minConf, int minSupport, ObjCorrespondenceSet corrSet) throws GommaException, RemoteException {
		
		SourceVersionSet sourceVersionSet = APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet();
		MappingVersionSet mapVersionSet = APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m);
		SourceVersionSet domainSourceVersionSet = new SourceVersionSet(), rangeSourceVersionSet = new SourceVersionSet();
		
		// check if the name is already available
		for (MappingVersion v : mapVersionSet)
			if (name.equals(v.getName())) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				name += "_"+df.format(creationDate.getTime());
				break;
			}
		
		for (int versionID : corrSet.getDomainSourceVersionIDSet())
			domainSourceVersionSet.addSourceVersion(sourceVersionSet.getSourceVersion(versionID));
		for (int versionID : corrSet.getRangeSourceVersionIDSet())
			rangeSourceVersionSet.addSourceVersion(sourceVersionSet.getSourceVersion(versionID));
		
		boolean noError = this.insertMappingVersion(m, name, creationDate, 
									minConf, minSupport, corrSet.size(), corrSet.getDomainObjIds().size(), corrSet.getRangeObjIds().size(), 
									domainSourceVersionSet, rangeSourceVersionSet);
		
		if (noError) {
			mapVersionSet = this.getMappingVersionSet(m,name,QueryMatchOperations.EXACT_MATCH);
			
			if (mapVersionSet.size()==0) 
				throw new RepositoryException("Could not insert mapping version.");
			else if (mapVersionSet.size()>1) 
				throw new RepositoryException("Could not identify the correct mapping version to associate object correspondences.");
			
			MappingVersion v = mapVersionSet.getMappingVersion(name);
			noError = this.insertObjectCorrespondenceSet(v, corrSet);
		}
		
		return noError;
	}
	
	/**
	 * @see MappingManager#updateMappingVersion(MappingVersion)
	 */
	public boolean updateMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMappingVersionAPI().updateMappingVersion(v)!=0;
	}
	
	/**
	 * @see MappingManager#deleteMappingVersion(MappingVersion)
	 */
	public boolean deleteMappingVersion(MappingVersion v) throws GommaException, RemoteException {
		APIFactory.getInstance().getObjCorrespondenceAPI().deleteObjectCorrespondenceSet(v);
		APIFactory.getInstance().getMappingVersionAPI().deleteMappingVersionSourceSet(v);
		
		return APIFactory.getInstance().getMappingVersionAPI().deleteMappingVersion(v)!=0;
	}
	
	
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,String[] corrTypes) throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v, corrTypes);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSetSimple(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws GommaException, RemoteException{
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(v);
	}
	
	/**
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion, int, boolean)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,int objId,boolean isDomain) throws GommaException, RemoteException{
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v, objId, isDomain);
	}
	/**
	 * @throws GommaException 
	 * @see MappingManager#getObjectCorrespondenceSet(MappingVersion, float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,float confThreshold) throws GommaException {
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(v, confThreshold);
	}
	/**
	 * @see MappingManager#getObjectCorrespondence(MappingVersion, String,String)
	 */
	public ObjCorrespondence getObjectCorrespondence(MappingVersion v,String dAcc, String rAcc) throws GommaException, RemoteException{
		return APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(v, dAcc, rAcc)
		         .getCollection().iterator().next();
	}
	
	/**
	 * @see MappingManager#insertObjectCorrespondenceSet(MappingVersion, ObjCorrespondenceSet)
	 */
	public boolean insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws GommaException, RemoteException {
		APIFactory.getInstance().getObjCorrespondenceAPI().deleteObjectCorrespondenceSet(v);
		return APIFactory.getInstance().getObjCorrespondenceAPI().insertObjectCorrespondenceSet(v,set)!=0;
	}

}
