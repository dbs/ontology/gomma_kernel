/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.mapping;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

import org.gomma.exceptions.GommaException;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

/**
 * The interface defines methods to execute metadata-based matcher in order align
 * different source and source versions.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MatchManager extends Serializable, Remote {

	/**
	 * The method matches objects of two given source versions and returns the correspondences between the objects as result of the match process.
	 * @param s1 source version structure that should be determined in the match process
	 * @param s2 source version structure that should be determined in the match process
	 * @param simFunc similarity function
	 * @param aggFunc aggregation function
	 * @param simThreshold similarity threshold
	 * @param props match properties
	 * @return object correspondences found within the match process
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException;
	
	/**
	 * The method matches objects of two given source versions and returns the correspondences between the objects as result of the match process.
	 * @param s1 source version structure that should be determined in the match process
	 * @param s2 source version structure that should be determined in the match process
	 * @param simFunc similarity function
	 * @param aggFunc aggregation function
	 * @param simThreshold similarity threshold
	 * @param props match properties
	 * @param initialMap initial mapping that will be used within the match process to refine the mapping 
	 * @return object correspondences found within the match process
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public ObjCorrespondenceSet match(SourceVersionStructure s1, SourceVersionStructure s2, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException;
	
	/**
	 * The method matches objects of two given source versions and returns the correspondences between the objects as result of the match process.
	 * @param domain set of objects that should be determined in the match process
	 * @param range set of objects that should be determined in the match process
	 * @param simFunc similarity function
	 * @param aggFunc aggregation function
	 * @param simThreshold similarity threshold
	 * @param props match properties
	 * @return object correspondences found within the match process
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props) throws GommaException, RemoteException;
	
	/**
	 * The method matches objects of two given source versions and returns the correspondences between the objects as result of the match process.
	 * @param domain set of objects that should be determined in the match process
	 * @param range set of objects that should be determined in the match process
	 * @param simFunc similarity function
	 * @param aggFunc aggregation function
	 * @param simThreshold similarity threshold
	 * @param props match properties
	 * @param initialMap initial mapping that will be used within the match process to refine the mapping 
	 * @return object correspondences found within the match process
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public ObjCorrespondenceSet match(ObjSet domain, ObjSet range, SimilarityFunction simFunc, AggregationFunction aggFunc, float simThreshold, MatchProperties props, ObjCorrespondenceSet initialMap) throws GommaException, RemoteException;

}
