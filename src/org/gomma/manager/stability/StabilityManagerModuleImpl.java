package org.gomma.manager.stability;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.operator.StabilityOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public final class StabilityManagerModuleImpl implements StabilityManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7888551882947537082L;

	public boolean deleteCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		
		APIFactory.getInstance().getStabilityAPI().deleteCorrespondenceStabs(analysisName);
		
		return APIFactory.getInstance().getStabilityAPI().deleteStabilityAnalysis(analysisName)!=0;
	}

	public List<String> getAnalysisName(String mapName) throws GommaException,
			RemoteException {
		List<String> nameList=new ArrayList<String>();
		StabilityAnalysisSet sas= APIFactory.getInstance().getStabilityAPI().getStabilityAnalysisSetWithMapName(mapName);
		for(StabilityAnalysis sa : sas){
			nameList.add(sa.getName());
		}
		return nameList;
	}

	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		return APIFactory.getInstance().getStabilityAPI().getCorrespondenceStabs(analysisName);
	}
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions)
            throws GommaException, RemoteException{
		return APIFactory.getInstance().getStabilityAPI().getCorrespondenceStabs(mapName, versionBegin, versionEnd, kMapVersions);
	}
	
	public StabilityAnalysisSet getStabilityAnalysisStatistics()
			throws GommaException, RemoteException {
		return APIFactory.getInstance().getStabilityAPI().getStabilityAnalysisStatistics();
	}

	public boolean hasStabilityAnalysis(String analysisName)
			throws GommaException, RemoteException {
		 return APIFactory.getInstance().getStabilityAPI().getStabilityAnalysisSetWithAnalysisName(analysisName).size()!=0;
	}

	public boolean hasStabilityAnalysis(String mapName, int kMapVersions,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd) throws GommaException,
			RemoteException {
		return APIFactory.getInstance().getStabilityAPI().getStabilityAnalysisSet(mapName, kMapVersions, versionBegin, versionEnd).size()!=0;
	}

	public boolean setCorrespondenceStabs(MappingVersionSet set, String mapName,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions,
			String analysisName) throws GommaException, RemoteException {
		
//		APIFactory.getInstance().getStabilityAPI().insertStabilityAnalysis(mapName, versionBegin, versionEnd, kMapVersions, analysisName);
		ObjCorrespondenceSet corrSet=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSetSimple(
				set.getLastMappingVersion());
		if(corrSet.size()==0 || corrSet==null) return false;
		APIFactory.getInstance().getStabilityAPI().insertStabilityAnalysis(mapName, versionBegin, versionEnd, kMapVersions, analysisName);
		int analysisID=APIFactory.getInstance().getStabilityAPI().getStabilityAnalysisSetWithAnalysisName(analysisName)
		               .iterator().next().getID();
		boolean affectedRows=false;
		
		CorrespondenceStabilitySet corrStabSet = new CorrespondenceStabilitySet();
		CorrespondenceStabilitySet corrStabs=StabilityOperators.getInstance().calculateCorrespondenceStabs(
				set, corrSet, analysisName, analysisID);
		
		for(CorrespondenceStability corrStab:corrStabs.getCollection()){
		   corrStabSet.addCorrespondenceStab(corrStab);
		   
		   if(corrStabSet.size()==1000) {
			   affectedRows=APIFactory.getInstance().getStabilityAPI().insertCorrespondenceStabs(corrStabSet)!=0;
			   if(!affectedRows)throw new RepositoryException("insert correspondence stability failed");
			   corrStabSet.clear();
		   }	    
		}
		
		affectedRows=APIFactory.getInstance().getStabilityAPI().insertCorrespondenceStabs(corrStabSet)!=0;
		if(!affectedRows)throw new RepositoryException("insert correspondence stability failed");
		return affectedRows;
	}

	public boolean updateStabilityAnalysis(StabilityAnalysis sa) throws GommaException, RemoteException {
		return APIFactory.getInstance().getStabilityAPI().updateStabilityAnalysis(sa)!=0;
	}

	

}
