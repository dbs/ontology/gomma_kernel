package org.gomma.manager.stability;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.gomma.exceptions.GommaException;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;


public interface StabilityManager extends Serializable, Remote {
     
	public boolean deleteCorrespondenceStabs(String analysisName)throws GommaException,RemoteException;
	
	public boolean setCorrespondenceStabs(MappingVersionSet set,String mapName,SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd,int kMapVersions,String analysisName)throws GommaException, RemoteException;
	
	//public void setCorrespondenceStabs(MapVersions mvs,String mapName,String firstVersionName,String lastVersionName,int kMapVersions,String analysisName)throws GommaException, RemoteException;
	
	public boolean hasStabilityAnalysis(String analysisName)throws GommaException, RemoteException;
	
	public List<String> getAnalysisName(String mapName)throws GommaException, RemoteException;
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName)throws GommaException, RemoteException;
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions)throws GommaException, RemoteException;
	
	public boolean hasStabilityAnalysis(String mapName, int kMapVersions, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd )throws GommaException, RemoteException;
	
	//public boolean hasStabilitiesAnalysis(String mapName, int kMapVersions, String firstVersionName,String lastVersionName)throws GommaException, RemoteException;
	
	public  StabilityAnalysisSet getStabilityAnalysisStatistics()throws GommaException, RemoteException;
	
	public boolean updateStabilityAnalysis(StabilityAnalysis sa)throws GommaException, RemoteException;
}
