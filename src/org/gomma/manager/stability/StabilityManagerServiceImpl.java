package org.gomma.manager.stability;

import java.rmi.RemoteException;
import java.util.List;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public final class StabilityManagerServiceImpl extends AbstractManagerService
		implements StabilityManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5480285400849315943L;
	
	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public StabilityManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}

	public boolean deleteCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		return this.gommaService.deleteCorrespondenceStabs(analysisName);
	}

	public List<String> getAnalysisName(String mapName) throws GommaException,
			RemoteException {
		return this.gommaService.getAnalysisName(mapName);
	}

	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName)
			throws GommaException, RemoteException {
		return this.gommaService.getCorrespondenceStabs(analysisName);
	}
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions)
	        throws GommaException, RemoteException{
		return this.gommaService.getCorrespondenceStabs(mapName, versionBegin, versionEnd, kMapVersions);
	}

	public StabilityAnalysisSet getStabilityAnalysisStatistics()
			throws GommaException, RemoteException {
		return this.gommaService.getStabilityAnalysisStatistics();
	}

	public boolean hasStabilityAnalysis(String analysisName) throws GommaException, RemoteException {
		return this.gommaService.hasStabilityAnalysis(analysisName);
	}

	public boolean hasStabilityAnalysis(String mapName, int kMapVersions,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd) throws GommaException,
			RemoteException {
		return this.gommaService.hasStabilityAnalysis(mapName, kMapVersions, versionBegin, versionEnd);
	}

	public boolean setCorrespondenceStabs(MappingVersionSet set, String mapName,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions,
			String analysisName) throws GommaException, RemoteException {
		return this.gommaService.setCorrespondenceStabs(set, mapName, versionBegin, versionEnd, kMapVersions, analysisName);

	}

	public boolean updateStabilityAnalysis(StabilityAnalysis sa)throws GommaException, RemoteException{
		return  this.gommaService.updateStabilityAnalysis(sa);
		
	}

}
