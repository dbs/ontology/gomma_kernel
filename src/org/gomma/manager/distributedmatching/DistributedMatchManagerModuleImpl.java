/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.distributedmatching;

import java.rmi.RemoteException;

import org.gomma.exceptions.GommaException;
import org.gomma.manager.ManagerModuleFactory;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class DistributedMatchManagerModuleImpl implements DistributedMatchManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 503861593591913436L;

	/**
	 * The constructor initializes the class.
	 */
	public DistributedMatchManagerModuleImpl() {}
	
	/**
	 * @see DistributedMatchManager#match(String, String, String, String, String, String, Strimg, String, float)
	 */
	public ObjCorrespondenceSet match(String domainObjectType, String domainName, String rangeObjectType, String rangeName, String domainVersionNumber, String rangeVersionNumber, String domainConstraint, String rangeConstraint, SimilarityFunction simFunction, AggregationFunction aggFunction, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		SourceVersion domainVersion = ManagerModuleFactory.getInstance().getSourceManager().getSourceVersionSet(domainObjectType, domainName, domainVersionNumber).getLatestSourceVersion();
		SourceVersion rangeVersion = ManagerModuleFactory.getInstance().getSourceManager().getSourceVersionSet(rangeObjectType, rangeName, rangeVersionNumber).getLatestSourceVersion();
		
		ObjSet domainObjects = ManagerModuleFactory.getInstance().getSourceManager().getObjectSet(domainVersion, domainConstraint);
		ObjSet rangeObjects = ManagerModuleFactory.getInstance().getSourceManager().getObjectSet(rangeVersion, rangeConstraint);
		
		SourceVersionStructure domainVersionStructure = ManagerModuleFactory.getInstance().getSourceManager().getSourceStructurePortion(domainVersion, domainObjects);
		SourceVersionStructure rangeVersionStructure = ManagerModuleFactory.getInstance().getSourceManager().getSourceStructurePortion(rangeVersion, rangeObjects);
		
		return ManagerModuleFactory.getInstance().getMatchManager().match(domainVersionStructure, rangeVersionStructure, simFunction, aggFunction, simThreshold, props);
	}
}
