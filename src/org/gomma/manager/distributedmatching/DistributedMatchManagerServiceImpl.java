/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.distributedmatching;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.SimilarityFunction;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.util.math.AggregationFunction;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class DistributedMatchManagerServiceImpl extends AbstractManagerService implements DistributedMatchManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2474129209536446012L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public DistributedMatchManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see DistributedMatchManager#match(String, String, String, String, String, String, String, String, float)
	 */
	public ObjCorrespondenceSet match(String domainObjectType, String domainName, String rangeObjectType, String rangeName, String domainVersionNumber, String rangeVersionNumber, String domainConstraint, String rangeConstraint, SimilarityFunction simFunction, AggregationFunction aggFunction, float simThreshold, MatchProperties props) throws GommaException, RemoteException {
		return this.gommaService.match(domainObjectType, domainName, rangeObjectType, rangeName, domainVersionNumber, rangeVersionNumber, domainConstraint, rangeConstraint, simFunction, aggFunction, simThreshold, props);
	}
}
