/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager;

import java.rmi.RemoteException;

import org.gomma.GommaService;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.EvolutionStatisticsManagerServiceImpl;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManagerServiceImpl;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManagerServiceImpl;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManagerServiceImpl;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.analysis.StatisticsManagerServiceImpl;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.distributedmatching.DistributedMatchManagerServiceImpl;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingManagerServiceImpl;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MappingOperationManagerServiceImpl;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.mapping.MatchManagerServiceImpl;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.source.SourceManagerServiceImpl;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.stability.StabilityManagerServiceImpl;
import org.gomma.manager.work.WorkManager;
import org.gomma.manager.work.WorkManagerServiceImpl;

/**
 * The class represents the service-based manager factory. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class ManagerServiceFactory extends AbstractManagerFactory {

	/**
	 * The item represents the GOMMA service.
	 */
	private GommaService gommaService;
	
	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public ManagerServiceFactory(GommaService gommaService) {
		this.gommaService = gommaService;
	}
	
	/**
	 * @see AbstractManagerFactory#initEvolutionStatisticsManager()
	 */
	protected EvolutionStatisticsManager initEvolutionStatisticsManager() throws RemoteException {
		return new EvolutionStatisticsManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initStatisticsManager()
	 */
	public StatisticsManager initStatisticsManager() throws RemoteException {
		return new StatisticsManagerServiceImpl(this.gommaService);
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingCorrespondenceEvolutionManager()
	 */
	protected MappingCorrespondenceEvolutionManager initMappingCorrespondenceEvolutionManager() throws RemoteException {
		return new MappingCorrespondenceEvolutionManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initMappingManager()
	 */
	protected MappingManager initMappingManager() throws RemoteException {
		return new MappingManagerServiceImpl(this.gommaService);
	}
	
	/**
	 * @see AbstractManagerFactory#initMappingOperationManager()
	 */
	protected MappingOperationManager initMappingOperationManager() {
		return new MappingOperationManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initMappingObjectEvolutionManager()
	 */
	protected MappingObjectEvolutionManager initMappingObjectEvolutionManager() throws RemoteException {
		return new MappingObjectEvolutionManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initMatchManager()
	 */
	protected MatchManager initMatchManager() throws RemoteException {
		return new MatchManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initSourceEvolutionManager()
	 */
	protected SourceEvolutionManager initSourceEvolutionManager() throws RemoteException {
		return new SourceEvolutionManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initSourceManager()
	 */
	protected SourceManager initSourceManager() throws RemoteException {
		return new SourceManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initWorkManager()
	 */
	protected WorkManager initWorkManager() throws RemoteException {
		return new WorkManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initStabilityManager()
	 */
	protected StabilityManager initStabilityManager() throws RemoteException {
		return new StabilityManagerServiceImpl(this.gommaService);
	}

	/**
	 * @see AbstractManagerFactory#initDistributedMatchManager()
	 */
	protected DistributedMatchManager initDistributedMatchManager() throws RemoteException {
		return new DistributedMatchManagerServiceImpl(this.gommaService);
	}
}
