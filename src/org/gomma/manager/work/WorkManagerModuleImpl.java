/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.work;

import java.rmi.RemoteException;
import java.util.NoSuchElementException;

import org.gomma.api.APIFactory;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.MatchTaskSet;
import org.gomma.model.work.TaskStatus;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class WorkManagerModuleImpl implements WorkManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1105379506261096729L;

	/**
	 * The constructor initializes the class.
	 * @param apiFactory repository API factory
	 */
	public WorkManagerModuleImpl() {
		
	}
	
	/**
	 * @see WorkManager#getWorkPackageSet()
	 */
	public WorkPackageSet getWorkPackageSet() throws GommaException, RemoteException {
		return APIFactory.getInstance().getWorkPackageAPI().getWorkPackageSet();
	}
	
	/**
	 * @see WorkManager#getWorkPackageSet(String)
	 */
	public WorkPackageSet getWorkPackageSet(String name) throws GommaException, RemoteException {
		return APIFactory.getInstance().getWorkPackageAPI().getWorkPackageSet(name, QueryMatchOperations.EXACT_MATCH);
	}
	
	/**
	 * @see WorkManager#insertWorkPackage(String, String)
	 */
	public boolean insertWorkPackage(String name, String description) throws GommaException,RemoteException {
		return APIFactory.getInstance().getWorkPackageAPI().insertWorkPackage(name,description)!=0;
	}
	
	/**
	 * @see WorkManager#updateWorkPackage(WorkPackage)
	 */
	public boolean updateWorkPackage(WorkPackage p) throws GommaException, RemoteException {
		return APIFactory.getInstance().getWorkPackageAPI().updateWorkPackage(p)!=0;
	}
	
	/**
	 * @see WorkManager#deleteWorkPackage(WorkPackage)
	 */
	public boolean deleteWorkPackage(WorkPackage p) throws GommaException, RemoteException {
		APIFactory.getInstance().getMatchTaskAPI().deleteMatchTaskObjects(p);
		APIFactory.getInstance().getMatchTaskAPI().deleteMatchTasks(p);
		return APIFactory.getInstance().getWorkPackageAPI().deleteWorkPackage(p)!=0;
	}
	
	/**
	 * @see WorkManager#insertMatchTask(String, WorkPackage, SourceVersion, SourceVersion, ObjSet, ObjSet, String)
	 */
	public boolean insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMatchTaskAPI().insertMatchTask(name,pack,domainSV,rangeSV,domainObjs,rangeObjs,taskConf)!=0;
	}
	
	/**
	 * @see WorkManager#getNextMatchTask(WorkPackage, String)
	 */
	public MatchTask getNextMatchTask(WorkPackage pack, String uri) throws GommaException, RemoteException, NoSuchElementException {
		MatchTaskSet tasks = APIFactory.getInstance().getMatchTaskAPI().getMatchTasks(pack, TaskStatus.NOT_PROCESSED);
		MatchTask t1, t2;
		
		if (tasks.size()==0) throw new NoSuchElementException("No more match task available to compute!");
		t1=tasks.getCollection().iterator().next();
		
		t2 = new MatchTask.Builder(t1.getID(),t1.getWorkPackage())
				.name(t1.getName())
				.resourceURL(uri)
				.domainSourceVersionID(t1.getDomainSourceVersionID())
				.rangeSourceVersionID(t1.getRangeSourceVersionID())
				.startTime(SimplifiedGregorianCalendar.getInstance())
				.endTime(SimplifiedGregorianCalendar.getInstance())
				.taskConfiguration(t1.getTaskConfiguration())
				.taskStatus(TaskStatus.IN_PROCESSING).build();
		for (int id : t1.getDomainObjectIDs()) t2.addDomainObjectID(id);
		for (int id : t1.getRangeObjectIDs()) t2.addRangeObjectID(id);

		this.updateMatchTaskStatus(t2);
		
		return t2;
	}
	
	/**
	 * @see WorkManager#updateMatchTaskStatus(MatchTask)
	 */
	public boolean updateMatchTaskStatus(MatchTask task) throws GommaException, RemoteException {
		return APIFactory.getInstance().getMatchTaskAPI().updateMatchTask(task)!=0;
	}
	
	/**
	 * @see WorkManager#deleteMatchTask(MatchTask)
	 */
	public boolean deleteMatchTask(MatchTask task) throws GommaException,RemoteException {		
		APIFactory.getInstance().getMatchTaskAPI().deleteMatchTaskObjects(task);
		return APIFactory.getInstance().getMatchTaskAPI().deleteMatchTask(task)!=0;
	}
}
