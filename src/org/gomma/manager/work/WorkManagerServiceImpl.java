/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.work;

import java.rmi.RemoteException;
import java.util.NoSuchElementException;

import org.gomma.GommaService;
import org.gomma.exceptions.GommaException;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class WorkManagerServiceImpl extends AbstractManagerService implements WorkManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7854044420549404847L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public WorkManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}
	
	/**
	 * @see WorkManager#getWorkPackageSet()
	 */
	public WorkPackageSet getWorkPackageSet() throws GommaException, RemoteException {
		return super.gommaService.getWorkPackageSet();
	}
	
	/**
	 * @see WorkManager#getWorkPackageSet(String)
	 */
	public WorkPackageSet getWorkPackageSet(String name) throws GommaException, RemoteException {
		return super.gommaService.getWorkPackageSet(name);
	}

	/**
	 * @see WorkManager#insertWorkPackage(String, String)
	 */
	public boolean insertWorkPackage(String name, String description) throws GommaException,RemoteException {
		return super.gommaService.insertWorkPackage(name, description);
	}
	
	/**
	 * @see WorkManager#updateWorkPackage(WorkPackage)
	 */
	public boolean updateWorkPackage(WorkPackage pack) throws GommaException, RemoteException {
		return super.gommaService.updateWorkPackage(pack);
	}
	
	/**
	 * @see WorkManager#deleteWorkPackage(WorkPackage)
	 */
	public boolean deleteWorkPackage(WorkPackage pack) throws GommaException, RemoteException {
		return super.gommaService.deleteWorkPackage(pack);
	}
	
	/**
	 * @see WorkManager#insertMatchTask(String, WorkPackage, SourceVersion, SourceVersion, ObjSet, ObjSet, String)
	 */
	public boolean insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws GommaException, RemoteException {
		return super.gommaService.insertMatchTask(name, pack, domainSV, rangeSV, domainObjs, rangeObjs, taskConf);
	}
	
	/**
	 * @see WorkManager#getNextMatchTask(WorkPackage, String)
	 */
	public MatchTask getNextMatchTask(WorkPackage pack, String uri) throws GommaException, RemoteException, NoSuchElementException {
		return super.gommaService.getNextMatchTask(pack, uri);
	}
	
	/**
	 * @see WorkManager#updateMatchTaskStatus(MatchTask)
	 */
	public boolean updateMatchTaskStatus(MatchTask task) throws GommaException, RemoteException {
		return super.gommaService.updateMatchTaskStatus(task);
	}
	
	/**
	 * @see WorkManager#deleteMatchTask(MatchTask)
	 */
	public boolean deleteMatchTask(MatchTask task) throws GommaException,RemoteException {
		return super.gommaService.deleteMatchTask(task);
	}
}
