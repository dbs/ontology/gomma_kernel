/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.work;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;

import org.gomma.exceptions.GommaException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;

/**
 * The interface defines methods to manage work packages and tasks 
 * that can be executed in a distributed fashion. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface WorkManager extends Serializable, Remote {

	/**
	 * The method returns a set of work packages.
	 * @return set of work packages
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public WorkPackageSet getWorkPackageSet() throws GommaException, RemoteException;
	
	/**
	 * The method returns the work package according to the given name.
	 * @param name
	 * @return
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public WorkPackageSet getWorkPackageSet(String name) throws GommaException, RemoteException;
	
	/**
	 * The method creates a new work package object and inserts it into the 
	 * repository. The method returns true if the insert operation has been successful;
	 * otherwise it returns false. 
	 * @param name name of the work package
	 * @param description description of the work package
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean insertWorkPackage(String name, String description) throws GommaException,RemoteException;
	
	/**
	 * The method updates the work package with the database and returns true 
	 * if the update process has been successful; otherwise it returns false.
	 * @param pack work package that should be updated
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean updateWorkPackage(WorkPackage pack) throws GommaException, RemoteException;
	
	/**
	 * The method deletes the specified work package and all associated match 
	 * tasks from the repository. It returns true if the delete process has been 
	 * successful; otherwise it returns false.
	 * @param pack work package that should be deleted
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean deleteWorkPackage(WorkPackage pack) throws GommaException, RemoteException;
	
	/**
	 * The method creates a new match task object and inserts it into the 
	 * repository. The method returns true if the insert process has been
	 * successful; otherwise it returns false. 
	 * @param name task name
	 * @param pack work package to which the match task is associated
	 * @param domainSV domain source version
	 * @param rangeSV range source version
	 * @param domainObjs set of domain objects that should be determined in the match process 
	 * @param rangeObjs set of range objects that should be determined in the match process
	 * @param taskConf task configuration 
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws GommaException, RemoteException;
	
	/**
	 * The method returns a match task that needs to be processed within the specified work 
	 * package. If no more match tasks available an NoSuchElementException will be thrown. 
	 * @param pack work package to that the returning match match task is associated 
	 * @param uri URI of the client application to which the next match task is delivered
	 * @return single match task that should be processed
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public MatchTask getNextMatchTask(WorkPackage pack, String uri) throws GommaException, RemoteException, NoSuchElementException;
	
	/**
	 * The method updates the status of the specified match task within the repository and returns
	 * true if the update process has been successful; otherwise it returns false.
	 * @param task match task those status should be updated
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean updateMatchTaskStatus(MatchTask task) throws GommaException, RemoteException;
	
	/**
	 * The method deletes the specified match task from the repository and returns true
	 * if the delete process has been successful; otherwise it returns false. 
	 * @param task match task which should be deleted within the repository
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public boolean deleteMatchTask(MatchTask task) throws GommaException,RemoteException;
	
}
