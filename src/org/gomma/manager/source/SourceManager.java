/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.source;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.io.FileTypes;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The interface defines methods to manage and retrieve metadata
 * and objects of sources and source versions.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceManager extends Serializable, Remote {

	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet,String keyword) throws GommaException, RemoteException;
	
	public ObjSet searchObject(SourceVersion sv, String keyword) throws GommaException, RemoteException;
	
	public SourceSet getSourceSet() throws GommaException, RemoteException;
	
	public SourceSet getSourceSet(String objType)  throws GommaException, RemoteException;
	
	public SourceSet getSourceSet(String objType, String physicalSource) throws GommaException, RemoteException;
	
	public boolean insertSource(String objType, String physicalSource, String sourceURL, boolean isOntology) throws GommaException, RemoteException;
	
	public boolean updateSource(Source s) throws GommaException, RemoteException;
	
	public boolean deleteSource(Source s) throws GommaException, RemoteException;
	
	
	
	
	
	public SourceVersionSet getSourceVersionSet() throws GommaException, RemoteException;
	
	public SourceVersionSet getSourceVersionSet(Source s) throws GommaException, RemoteException;
	
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws GommaException, RemoteException;
	
	public SourceVersionSet getSourceVersionSet(String objType, String physicalSource, String version) throws GommaException, RemoteException;
	
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException;
	
	public boolean insertSourceVersion(String objType, String physicalSource, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws GommaException, RemoteException;
	
	public boolean updateSourceVersion(SourceVersion v) throws GommaException, RemoteException;
	
	public boolean deleteSourceVersion(SourceVersion v) throws GommaException, RemoteException;
	
	
	public void	importSource(String sourceDescriptionFile) throws GommaException, RemoteException;
	
	public void	importSource(HashMap<String, String> importDescriptionObj) throws GommaException, RemoteException;
	
	public void exportSource(String sourceFileName, SourceVersion v, FileTypes type) throws GommaException, RemoteException;
	
	public void exportSource(String sourceFileName, SourceVersionStructure svs, FileTypes type) throws GommaException, RemoteException;
	
	
	public AttributeSet getAttributeSet() throws GommaException, RemoteException;
	
	
	
	
	
	public ObjSet getObjectSet(SourceVersion v) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getObjectSet(String objType, String physicalSource, String version, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException;
	
	
	public ObjSet getRootObjectSet(SourceVersion s) throws GommaException, RemoteException;
	
	public ObjSet getRootObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getRootObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public ObjSet getLeafObjectSet(SourceVersion s) throws GommaException, RemoteException;
	
	public ObjSet getLeafObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException;
	
	public ObjSet getLeafObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException;
	
	
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws GommaException, RemoteException;
	
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs) throws GommaException, RemoteException;
	
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs, int nLevels, boolean getSuccessors) throws GommaException, RemoteException;
	
	
	public ObjSet union(ObjSet o1, ObjSet o2) throws GommaException, RemoteException;
	
	public ObjSet intersect(ObjSet o1, ObjSet o2) throws GommaException, RemoteException;
	
	public ObjSet diff(ObjSet o1, ObjSet o2) throws GommaException, RemoteException;
	
	public ObjSet majority(ObjSet[] o, int nMin) throws GommaException, RemoteException;
}
