/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.source;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

import org.gomma.api.APIFactory;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.io.FileTypes;
import org.gomma.io.exporter.SourceVersionExportFactory;
import org.gomma.io.exporter.SourceVersionExporter;
import org.gomma.io.importer.SourceImporter;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The class represents the kernel-specific implementation of the
 * manager interface
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceManagerModuleImpl implements SourceManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9177838244873365549L;
	
	/**
	 * The constructor initializes the class.
	 */
	public SourceManagerModuleImpl() {}
	
	/**
	 * @see SourceManager#getSourceSet()
	 */
	public SourceSet getSourceSet() 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().getSourceSet();
	}
	
	/**
	 * @see SourceManager#getSourceSet(String)
	 */
	public SourceSet getSourceSet(String objType) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().getSourceSet(objType);
	}
	
	/**
	 * @see SourceManager#getSourceSet(String, String)
	 */
	public SourceSet getSourceSet(String objType, String physicalSource) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().getSourceSet(objType, physicalSource);
	}
	
	/**
	 * @see SourceManager#insertSource(String, String, String, boolean)
	 */
	public boolean insertSource(String objType, String physicalSource, String sourceURL, boolean isOntology) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().insertSource(objType, physicalSource, sourceURL, isOntology)!=0;
	}
	
	/**
	 * @see SourceManager#updateSource(Source)
	 */
	public boolean updateSource(Source s)  
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().updateSource(s)!=0;
	}
	
	/**
	 * @see SourceManager#deleteSource(Source)
	 */
	public boolean deleteSource(Source s) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceAPI().deleteSource(s)!=0;
	}
	
	
	
	
	
	
	/**
	 * @see SourceManager#getSourceVersionSet()
	 */
	public SourceVersionSet getSourceVersionSet() 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet();
	}

	/**
	 * @see SourceManager#getSourceVersionSet(Source)
	 */
	public SourceVersionSet getSourceVersionSet(Source s) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, String)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, String version) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s, version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(String, String, String)
	 */
	public SourceVersionSet getSourceVersionSet(String objType, String physicalSource, String version) 
		throws GommaException, RemoteException {
		SourceSet sourceSet = APIFactory.getInstance().getSourceAPI().getSourceSet(objType, physicalSource);
		
		if (sourceSet.size()==0) return new SourceVersionSet();
		else if (sourceSet.size()>1)
			throw new GommaException("There are multiple single sources (LDS) available "+
					"meeting the specified object type '"+objType+"' and physical source '"+physicalSource+"'.");
		
		return APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(sourceSet.iterator().next(),version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet(s,minDate,maxDate);
	}
	
	/**
	 * @see SourceManager#insertSourceVersion(String, String, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	public boolean insertSourceVersion(String objType, String physicalSource, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) 
		throws GommaException, RemoteException {
		
		SourceSet set = this.getSourceSet(objType,physicalSource);
		
		if (set.size()==0)
			throw new GommaException("Could not find any source for object type '"+objType+"' and physical source '"+physicalSource+"'.");
		
		if (set.size()>1)
			throw new GommaException("Too many sources available for object type '"+objType+"' and physical source '"+physicalSource+"'.");
		
		Source s = set.iterator().next();
		return APIFactory.getInstance().getSourceVersionAPI().insertSourceVersion(s, version, creationDate, type)!=0;
	}
	
	/**
	 * @see SourceManager#updateSourceVersion(SourceVersion)
	 */
	public boolean updateSourceVersion(SourceVersion v) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().updateSourceVersion(v)!=0;
	}
	
	/**
	 * @see SourceManager#deleteSourceVersion(SourceVersion)
	 */
	public boolean deleteSourceVersion(SourceVersion v) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().deleteSourceVersion(v)!=0;
	}
	
	
	
	
	/**
	 * @see SourceManager#getAttributeSet()
	 */
	public AttributeSet getAttributeSet() 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getAttributeAPI().getAttributeSet();
	}
	
	
	
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion)
	 */
	public ObjSet getObjectSet(SourceVersion v) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet)
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getObjectSet(v);
		
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String[] attributeNames) 
		throws GommaException, RemoteException {
		return this.getObjectSet(v, this.resolveAttributeSet(attributeNames));
	}
		
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, set);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet)
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getObjectSet(v,set);
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, set, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, String[] attributeNames)
		throws GommaException, RemoteException {
		return this.getObjectSet(v, set, this.resolveAttributeSet(attributeNames));
	}
	
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, accNumber, op);
	}
	

	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) 
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getObjectSet(v, accNumber, op);
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, accNumber, op, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, String[] attributeNames) 
		throws GommaException, RemoteException {
		return this.getObjectSet(v, accNumber, op, this.resolveAttributeSet(attributeNames));
	}
	
	/**
	 * @see SourceManager#getObjectSet(String, String, String, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(String objType, String physicalSource, String version, String accNumber, QueryMatchOperations op) 
		throws GommaException, RemoteException {
		
		SourceVersion v = this.getSourceVersionSet(objType, physicalSource, version).getLatestSourceVersion();
		return this.getObjectSet(v, accNumber, op);
	}
	
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List<String>)
	 */	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) 
		throws GommaException, RemoteException{
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, accNumbers);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet)
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getObjectSet(v, accNumbers);
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, accNumbers, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, String[] attributeNames) 
		throws GommaException, RemoteException {
		return this.getObjectSet(v, accNumbers, this.resolveAttributeSet(attributeNames));
	}
	
	
	
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) 
		throws RepositoryException, WrongSourceException {
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, queryCondition);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) 
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getObjectSet(v, queryCondition);
		return APIFactory.getInstance().getObjectAPI().getObjectSet(v, queryCondition, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, String[] attributeNames) 
		throws GommaException, RemoteException {
		return this.getObjectSet(v, queryCondition, this.resolveAttributeSet(attributeNames));
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion)
	 */
	public ObjSet getRootObjectSet(SourceVersion v) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getRootObjectSet(v);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getRootObjectSet(SourceVersion v, AttributeSet attSet)
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getRootObjectSet(v);
		return APIFactory.getInstance().getObjectAPI().getRootObjectSet(v,attSet);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, String[])
	 */
	public ObjSet getRootObjectSet(SourceVersion v, String[] attributeNames)
		throws GommaException, RemoteException {
		return this.getRootObjectSet(v, this.resolveAttributeSet(attributeNames));
	}
	
	
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion)
	 */
	public ObjSet getLeafObjectSet(SourceVersion v) 
		throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getLeafObjectSet(v);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getLeafObjectSet(SourceVersion v, AttributeSet attSet)
		throws GommaException, RemoteException {
		if (attSet.size()==0) return this.getLeafObjectSet(v);
		return APIFactory.getInstance().getObjectAPI().getLeafObjectSet(v, attSet);
	}
	
	
	public ObjSet getLeafObjectSet(SourceVersion v, String[] attributeNames)
		throws GommaException, RemoteException {
		return this.getLeafObjectSet(v, this.resolveAttributeSet(attributeNames));
	}
	
	
	
	/**
	 * @see SourceManager#getSourceStructure(SourceVersion)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws GommaException, RemoteException {	
		return APIFactory.getInstance().getSourceVersionStructureAPI().getSourceStructure(v);
	}
	
	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs) throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionStructureAPI().getSourceStructure(v,objs);
	}
	
	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet, int, boolean)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs, int nLevels, boolean getSuccessors) throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionStructureAPI().getSourceStructure(v,objs,nLevels,getSuccessors);
	}
	
	
	
	/**
	 * @see SourceManager#importSource(String)
	 */
	public void importSource(String sourceDescriptionFile) throws GommaException, RemoteException {
		SourceImporter s = new SourceImporter();
		s.importSource(sourceDescriptionFile);
	}
	/**
	 * @see SourceManager#importSource(HashMap<String, String>)
	 */
	public void importSource(HashMap<String, String> importDescriptionObj) throws GommaException, RemoteException {
		SourceImporter s = new SourceImporter();
		s.importSource(importDescriptionObj);
	}
	/**
	 * @see SourceManager#exportSource(String, SourceVersion, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersion s, FileTypes type) throws GommaException, RemoteException {
		try {
			PrintWriter w = new PrintWriter(sourceFileName);
			SourceVersionExporter exporter = SourceVersionExportFactory.getExporter(type);
			exporter.exportSource(w,s);
			w.flush();
			w.close();
		} catch (IOException e) {
			throw new GommaException(e.getMessage());
		}
	}
	
	/**
	 * @see SourceManager#exportSource(String, SourceVersionStructure, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersionStructure svs, FileTypes type) throws GommaException, RemoteException {
		try {
			PrintWriter w = new PrintWriter(sourceFileName);
			SourceVersionExporter exporter = SourceVersionExportFactory.getExporter(type);
			exporter.exportSource(w,svs);
			w.flush();
			w.close();
		} catch (IOException e) {
			throw new GommaException(e.getMessage());
		}
	}
	
	
	
	/**
	 * @see SourceManager#union(ObjSet, ObjSet)
	 */
	public ObjSet union(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return SetOperators.union(o1,o2);
	}
	
	/**
	 * @see SourceManager#intersect(ObjSet, ObjSet)
	 */
	public ObjSet intersect(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return SetOperators.intersect(o1,o2);
	}
	
	/**
	 * @see SourceManager#diff(ObjSet, ObjSet)
	 */
	public ObjSet diff(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return SetOperators.diff(o1,o2);
	}
	
	/**
	 * @see SourceManager#majority(ObjSet[], int)
	 */
	public ObjSet majority(ObjSet[] o, int nMin) throws GommaException, RemoteException {
		return SetOperators.majority(o,nMin);
	}

	/**
	 * @see SourceManager#searchObject(SourceVersion,String)
	 */
	public ObjSet searchObject(SourceVersion sv, String keyword)
			throws GommaException, RemoteException {
		return APIFactory.getInstance().getObjectAPI().getObjectSetWithKeyword(sv, keyword);
	}

	/**
	 * @see SourceManager#searchObject(SourceVersionSet,String)
	 */
	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet, String keyword)
			throws GommaException, RemoteException {
		return APIFactory.getInstance().getSourceVersionAPI().getSearchSourceVersionSet(svSet, keyword);
		
	}
	
	
	
	
	
	private AttributeSet resolveAttributeSet(String[] attributeNames) throws GommaException, RemoteException {
		AttributeSet completeAttributeSet = this.getAttributeSet();
		AttributeSet resultSet = new AttributeSet();
		
		for (String attName : attributeNames) {
			for (Attribute att : completeAttributeSet.getAttributeSet(attName)) 
				resultSet.addAttribute(att);
		}
		
		return resultSet;
	}
}
