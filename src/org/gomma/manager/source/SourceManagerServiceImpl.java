/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.manager.source;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

import org.gomma.GommaService;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.io.FileTypes;
import org.gomma.manager.AbstractManagerService;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The class represents the service-based implementation of the 
 * manager interface.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceManagerServiceImpl extends AbstractManagerService implements SourceManager {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4317149206168915431L;

	/**
	 * The constructor initializes the class.
	 * @param gommaService GOMMA service
	 */
	public SourceManagerServiceImpl(GommaService gommaService) {
		super(gommaService);
	}

	/**
	 * @see SourceManager#getSourceSet()
	 */
	public SourceSet getSourceSet() throws GommaException, RemoteException {
		return this.gommaService.getSourceSet();
	}
	
	/**
	 * @see SourceManager#getSourceSet(String)
	 */
	public SourceSet getSourceSet(String objType) throws GommaException, RemoteException {
		return this.gommaService.getSourceSet(objType);
	}
	
	/**
	 * @see SourceManager#getSourceSet(String, String)
	 */
	public SourceSet getSourceSet(String objType, String physicalSource) throws GommaException, RemoteException {
		return this.gommaService.getSourceSet(objType, physicalSource);
	}
	
	/**
	 * @see SourceManager#insertSource(String, String, String, boolean)
	 */
	public boolean insertSource(String objType, String physicalSource, String sourceURL, boolean isOntology) throws GommaException, RemoteException {
		return this.gommaService.insertSource(objType, physicalSource, sourceURL, isOntology);
	}
	
	/**
	 * @see SourceManager#updateSource(Source)
	 */
	public boolean updateSource(Source s) throws GommaException, RemoteException {
		return this.gommaService.updateSource(s);
	}
	
	/**
	 * @see SourceManager#deleteSource(Source)
	 */
	public boolean deleteSource(Source s) throws GommaException, RemoteException {
		return this.gommaService.deleteSource(s);
	}
	
	
	
	
	
	/**
	 * @see SourceManager#getSourceVersionSet()
	 */
	public SourceVersionSet getSourceVersionSet() throws GommaException,	RemoteException {
		return this.gommaService.getSourceVersionSet();
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source)
	 */
	public SourceVersionSet getSourceVersionSet(Source s) throws GommaException, RemoteException {
		return this.gommaService.getSourceVersionSet(s);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, String)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws GommaException, RemoteException {
		return this.gommaService.getSourceVersionSet(s, version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(String, String, String)
	 */
	public SourceVersionSet getSourceVersionSet(String objType, String physicalSource, String version) throws GommaException, RemoteException {
		return this.gommaService.getSourceVersionSet(objType, physicalSource, version);
	}
	
	/**
	 * @see SourceManager#getSourceVersionSet(Source, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws GommaException, RemoteException {
		return this.gommaService.getSourceVersionSet(s, minDate, maxDate);
	}
	
	/**
	 * @see SourceManager#insertSourceVersion(String, String, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	public boolean insertSourceVersion(String objType,String physicalSource, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws GommaException, RemoteException {
		return this.gommaService.insertSourceVersion(objType, physicalSource, version, creationDate, type);
	}
	
	/**
	 * @see SourceManager#updateSourceVersion(SourceVersion)
	 */
	public boolean updateSourceVersion(SourceVersion v) throws GommaException, RemoteException {
		return this.gommaService.updateSourceVersion(v);
	}
	
	/**
	 * @see SourceManager#deleteSourceVersion(SourceVersion)
	 */
	public boolean deleteSourceVersion(SourceVersion sv) throws GommaException, RemoteException {
		return super.gommaService.deleteSourceVersion(sv);
	}


	/**
	 * @see SourceManager#importSource(String)
	 */
	public void importSource(String sourceDescriptionFile) throws GommaException, RemoteException {
		super.gommaService.importSource(sourceDescriptionFile);
	}	

	/**
	 * @see SourceManager#importSource(String)
	 */
	public void importSource(HashMap<String,String> importDescriptionObj) throws GommaException, RemoteException {
		super.gommaService.importSource(importDescriptionObj);
	}	
		
	/**
	 * @see SourceManager#exportSource(String, SourceVersion, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersion s, FileTypes type) throws GommaException, RemoteException {
		super.gommaService.exportSource(sourceFileName, s, type);
	}

	/**
	 * @see SourceManager#exportSource(String, SourceVersionStructure, FileTypes)
	 */
	public void exportSource(String sourceFileName, SourceVersionStructure svs, FileTypes type) throws GommaException, RemoteException {
		super.gommaService.exportSource(sourceFileName, svs, type);
	}

	
	
	/**
	 * @see SourceManager#getAttributeSet()
	 */
	public AttributeSet getAttributeSet() throws GommaException, RemoteException {
		return super.gommaService.getAttributeSet();
	}
	
	
	
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion)
	 */
	public ObjSet getObjectSet(SourceVersion v) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, set);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, set, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, IDObjectSet, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, set, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumber, op);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumber, op, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, QueryMatchOperations, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumber, op, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(String, String, String, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(String objType, String physicalSource, String version, String accNumber, QueryMatchOperations op) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(objType, physicalSource, version, accNumber, op);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List<String>)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumbers);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumbers, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, List, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, accNumbers, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, queryCondition);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, queryCondition, attSet);
	}
	
	/**
	 * @see SourceManager#getObjectSet(SourceVersion, String, String[])
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, String[] attributeNames) throws GommaException, RemoteException {
		return this.gommaService.getObjectSet(v, queryCondition, attributeNames);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion)
	 */
	public ObjSet getRootObjectSet(SourceVersion s) throws GommaException, RemoteException {
		return super.gommaService.getRootObjectSet(s);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getRootObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException {
		return super.gommaService.getRootObjectSet(s, attSet);
	}
	
	/**
	 * @see SourceManager#getRootObjectSet(SourceVersion, String[])
	 */
	public ObjSet getRootObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException {
		return super.gommaService.getRootObjectSet(s, attributeNames);
	}
	
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion)
	 */
	public ObjSet getLeafObjectSet(SourceVersion s) throws GommaException, RemoteException {
		return super.gommaService.getLeafObjectSet(s);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getLeafObjectSet(SourceVersion s, AttributeSet attSet) throws GommaException, RemoteException {
		return super.gommaService.getLeafObjectSet(s, attSet);
	}
	
	/**
	 * @see SourceManager#getLeafObjectSet(SourceVersion, String[])
	 */
	public ObjSet getLeafObjectSet(SourceVersion s, String[] attributeNames) throws GommaException, RemoteException {
		return super.gommaService.getLeafObjectSet(s, attributeNames);
	}
	
	

	
	/**
	 * @see SourceManager#getSourceStructure(SourceVersion)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws GommaException, RemoteException {
		return super.gommaService.getSourceStructure(v);
	}

	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs) throws GommaException, RemoteException {
		return super.gommaService.getSourceStructurePortion(v, objs);
	}

	/**
	 * @see SourceManager#getSourceStructurePortion(SourceVersion, ObjSet, int, boolean)
	 */
	public SourceVersionStructure getSourceStructurePortion(SourceVersion v, ObjSet objs, int nLevels, boolean getSuccessors) throws GommaException, RemoteException {
		return super.gommaService.getSourceStructurePortion(v, objs, nLevels, getSuccessors);
	}

	
	
	
	
	/**
	 * @see SourceManager#union(ObjSet, ObjSet)
	 */
	public ObjSet union(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return super.gommaService.union(o1, o2);
	}
	
	/**
	 * @see SourceManager#intersect(ObjSet, ObjSet)
	 */
	public ObjSet intersect(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return super.gommaService.intersect(o1, o2);
	}

	/**
	 * @see SourceManager#majority(ObjSet[], int)
	 */
	public ObjSet majority(ObjSet[] o, int min) throws GommaException, RemoteException {
		return super.gommaService.majority(o, min);
	}
	
	/**
	 * @see SourceManager#diff(ObjSet, ObjSet)
	 */
	public ObjSet diff(ObjSet o1, ObjSet o2) throws GommaException, RemoteException {
		return super.gommaService.diff(o1, o2);
	}

	/**
	 * @see SourceManager#searchObject(SourceVersion,String)
	 */
	public ObjSet searchObject(SourceVersion sv, String keyword)
			throws GommaException, RemoteException {
		return super.gommaService.searchObject(sv, keyword);
	}

	/**
	 * @see SourceManager#searchObject(SourceVersionSet,String)
	 */
	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet, String keyword)
			throws GommaException, RemoteException {
		return super.gommaService.getSearchSourceVersionSet(svSet, keyword);
	}	
	
	


}
