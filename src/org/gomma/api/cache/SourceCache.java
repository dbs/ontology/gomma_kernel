package org.gomma.api.cache;

import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;

/**
 * The class implements a cache for source objects.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceCache implements Cache {

	/**
	 * The item represents the internal cache of source objects.
	 */
	private SourceSet cache;
	
	/**
	 * Constructor of the class.
	 */
	public SourceCache() {
		this.cache = new SourceSet();
	}
	
	/**
	 * The method creates and returns a source objects using the specified parameters, e.g., 
	 * the unique identifier, semantic objects type and physical source name etc. The method
	 * stores the object within the cache if it is not available there.
	 * @param id unique identifier of the source
	 * @param objType semantic object type of the source
	 * @param pdsName physical source name
	 * @param url URL of the source
	 * @param isOntology true, if the source is an ontology; otherwise false
	 * @return source object
	 */
	public Source getSource(int id, String objType, String pdsName, String url, boolean isOntology) {
		if (this.cache.contains(id))
			return this.cache.getSource(id);
		
		Source s = new Source.Builder(id)
					.objectType(objType)
					.physicalSourceName(pdsName)
					.sourceURL(url)
					.isOntology(isOntology).build();
		this.cache.addSource(s);
		
		return s;
	}
	
	/**
	 * @see Cache#clear()
	 */
	public void clear() {
		this.cache = new SourceSet();
	}
}
