package org.gomma.api.cache;

/**
 * The interface defines the set of fundamental methods of the internal cache
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface Cache {

	/**
	 * The method clears the cache.
	 */
	public void clear();
}
