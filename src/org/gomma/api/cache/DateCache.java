package org.gomma.api.cache;

import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;

import java.util.Map;

import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements a cache for calendar objects.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class DateCache implements Cache {

	/**
	 * The item represents the internal calendar cache.
	 */
	private Map<Long,SimplifiedGregorianCalendar> cache;
	
	/**
	 * Constructor of the class.
	 */
	public DateCache() {
		this.cache = new Long2ObjectOpenHashMap<SimplifiedGregorianCalendar>();
	}
	
	/**
	 * The method creates and returns a serialized gregorian calendar object using the given time in milliseconds 
	 * @param timeInMilli time in milliseconds
	 * @return created serialized gregorian calendar
	 */
	public synchronized SimplifiedGregorianCalendar getCalendar(long timeInMilli) {
		if (this.cache.containsKey(timeInMilli)) 
			return this.cache.get(timeInMilli);
		
		SimplifiedGregorianCalendar cal = new SimplifiedGregorianCalendar(timeInMilli);
		this.cache.put(timeInMilli,cal);
		
		return cal;
	}
	
	/**
	 * @see Cache#clear()
	 */
	public synchronized void clear() {
		this.cache.clear();
		System.gc();
	}
}
