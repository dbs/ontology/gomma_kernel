package org.gomma.api.cache;

/**
 * The class implements a cache manager managing a set of caches.
 * It can be used to retrieve a cache but also to clear all caches 
 * at once. The cache manager acts as singleton and, thus, can be 
 * called independently from different classes and objects within 
 * the kernel.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class CacheManager {

	/**
	 * The item represents the calendar cache. 
	 */
	private DateCache dateCache = null;
	
	/**
	 * The item represents the source cache.
	 */
	private SourceCache sourceCache = null;
	
	/**
	 * The item represents the attribute cache.
	 */
	private AttributeCache attributeCache = null;
	
	/**
	 * The item represents an instance of the cache manager.
	 */
	private volatile static CacheManager singleton = null;
	
	/**
	 * The method returns an instance of the cache manager.
	 * @return instance of the cache manager.
	 */
	public static CacheManager getInstance() {
		if (singleton == null) {
			synchronized (CacheManager.class) {
				if (singleton == null)
					singleton = new CacheManager();
			}
		}
		return singleton;
	}
	
	/**
	 * Constructor of the class.
	 */
	private CacheManager() {}

	/**
	 * The method closes all included caches but not the cache manager.
	 * Thus, a cache will be reinitialized the next time it will e accessed. 
	 */
	public void shutdown() {
		this.dateCache = null;
		this.sourceCache = null;
		this.attributeCache = null;
	}
	
	/**
	 * The method stops the cache manager (destructor).
	 */
	public static void stop() {
		singleton = null;
	}
	
	/**
	 * The method clears all caches.
	 */
	public void clearCaches() {
		this.dateCache.clear();
		this.sourceCache.clear();
	}
	
	/**
	 * The method returns the calendar cache.
	 * @return calendar cache.
	 */
	public DateCache getDateCache() {
		if (this.dateCache == null) 
			this.dateCache = new DateCache();
		return this.dateCache;
	}
	
	/**
	 * The method returns the source cache.
	 * @return source cache.
	 */
	public SourceCache getSourceCache() {
		if (this.sourceCache == null)
			this.sourceCache = new SourceCache();
		return this.sourceCache;
	}
	
	/**
	 * The method returns the attribute cache.
	 * @return attribute cache.
	 */
	public AttributeCache getAttributeCache() {
		if (this.attributeCache == null)
			this.attributeCache = new AttributeCache();
		return this.attributeCache;
	}
}
