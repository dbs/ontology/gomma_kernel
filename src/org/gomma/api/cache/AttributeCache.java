package org.gomma.api.cache;

import org.gomma.model.DataTypes;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;

/**
 * The class implements a cache for attribute objects. Such attribute objects 
 * are typically used to specify property values of sources, source objects, 
 * mappings, and correspondences attribute values. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class AttributeCache implements Cache {
	
	/**
	 * The item represents the internal set of cached attributes.
	 */
	private AttributeSet cache;
	
	/**
	 * Constructor of the class.
	 */
	public AttributeCache() {
		this.cache = new AttributeSet();
	}
	
	/**
	 * The method returns an attribute using the specified identifier id, name, 
	 * scope, and data type. The attribute object is stored within the cache
	 * if it is not available there.
	 * @param id unique attribute identifier
	 * @param name attribute name
	 * @param scope attribute scope
	 * @param type data type of associated attribute values
	 * @return attribute object
	 */
	public synchronized Attribute getAttribute(int id, String name, String scope, DataTypes type) {
		if (this.cache.contains(id)) return this.cache.getAttribute(id);
		
		Attribute a = new Attribute.Builder(id)
						.name(name)
						.scope(scope)
						.dataType(type).build();
		this.cache.addAttribute(a);
		
		return a;
	}

	/**
	 * @see Cache#clear()
	 */
	public synchronized void clear() {
		this.cache = new AttributeSet();
		System.gc();
	}
}
