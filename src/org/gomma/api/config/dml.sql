-- retrieval statements
1;select lds_version_id, lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date, structure_type from gomma_obj_types a, gomma_pds b, gomma_lds c, gomma_lds_versions d where a.obj_type_id=c.obj_type_id_fk and b.pds_id=c.pds_id_fk and c.lds_id = d.lds_id_fk
2;
3;select map_id, a.name, b.name, b.descr, c.name, c.descr, d.name, d.descr, e.name, e.descr, minSupport, minConfidence, is_inst_map, is_derived, number_of_corrs, number_of_domain_objs, number_of_range_objs, lineage_pattern from gomma_maps a, gomma_map_types b, gomma_map_classes c, gomma_map_methods d, gomma_map_tools e where b.map_type_id=a.map_type_id_fk and c.map_class_id=a.map_class_id_fk and d.map_method_id = a.map_method_id_fk and e.map_tool_id = a.map_tool_id_fk
4;select map_id_fk, att_name, att_scope, att_value_s, isnull(att_value_i), att_value_i, isnull(att_value_f),att_value_f from gomma_map_atts where map_id_fk in ([mapIdList])
5;select map_id_fk, lds_version_id, lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date, structure_type from gomma_obj_types a, gomma_pds b, gomma_lds c, gomma_lds_versions d, gomma_map_src_versions e where a.obj_type_id=c.obj_type_id_fk and b.pds_id=c.pds_id_fk and c.lds_id = d.lds_id_fk and d.lds_version_id = e.lds_version_id_fk and map_id_fk in ([mapIdList]) and is_domain_lds = [isDomainLds]
6;select map_id_fk, lds_version_id, lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date, structure_type from gomma_obj_types a, gomma_pds b, gomma_lds c, gomma_lds_versions d, gomma_map_inst_versions e where a.obj_type_id=c.obj_type_id_fk and b.pds_id=c.pds_id_fk and c.lds_id = d.lds_id_fk and d.lds_version_id = e.lds_version_id_fk and map_id_fk in ([mapIdList]) and is_domain_lds = [isDomainLds]
7;select pub_ref_id, accession, url from gomma_pub_refs a, gomma_pub_atts b where a.pub_ref_id=b.pub_ref_id_fk
8;select pub_ref_id_fk, att_name, att_scope, att_value_s, isnull(att_value_i), att_value_i, isnull(att_value_f), att_value_f from gomma_pub_atts where pub_ref_id_fk in ([pubRefIdList])
9;select obj_id, accession, a.date_from, a.date_to, c.lds_version_id, b.lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date from gomma_obj_versions a, gomma_lds b, gomma_lds_versions c, gomma_pds d, gomma_obj_types e where a.lds_id_fk=b.lds_id and b.lds_id=c.lds_id_fk and b.pds_id_fk=d.pds_id and b.obj_type_id_fk=e.obj_type_id and c.lds_date between a.date_from and a.date_to and lds_version_id in ([ldsVersionIdList])
10;select obj_id, lds_version_id, att_name, att_scope, att_value_s, isnull(att_value_i), att_value_i, isnull(att_value_f), att_value_f, a.date_from, a.date_to from gomma_obj_atts a, gomma_obj_versions b, gomma_lds_versions c where a.obj_id_fk=b.obj_id and b.lds_id_fk=c.lds_id_fk and c.lds_version_id in ([ldsVersionIdList]) and b.obj_id in ([objIdList]) and c.lds_date between a.date_from and a.date_to
11;select a.d_obj_id_fk, b.accession, b.lds_id_fk, e.lds_version_id, a.r_obj_id_fk, c.accession, c.lds_id_fk, f.lds_version_id, a.support, a.confidence, a.user_checked, d.name, d.descr from gomma_obj_corrs a, gomma_obj_versions b, gomma_obj_versions c, gomma_corr_types d, gomma_lds_versions e, gomma_lds_versions f, gomma_map_src_versions g, gomma_map_src_versions h where b.obj_id=a.d_obj_id_fk and c.obj_id=a.r_obj_id_fk and d.corr_type_id=a.corr_type_id_fk and e.lds_version_id=g.lds_version_id_fk and f.lds_version_id=h.lds_version_id_fk and e.lds_id_fk=b.lds_id_fk and f.lds_id_fk=c.lds_id_fk and e.lds_date between b.date_from and b.date_to and f.lds_date between c.date_from and c.date_to and a.map_id_fk=[mapId] and g.map_id_fk=[mapId] and h.map_id_fk=[mapId]
12;select count(*) from (select distinct c.accession from gomma_lds_versions a, gomma_lds b, gomma_obj_versions c where a.lds_id_fk=b.lds_id and b.lds_id=c.lds_id_fk and a.lds_date between c.date_from and date_to and a.lds_version_id in ([ldsVersionIdList])) t
13;
14;
15;
16;select obj_id, accession, a.date_from, a.date_to, c.lds_version_id, b.lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date from gomma_obj_versions a, gomma_lds b, gomma_lds_versions c, gomma_pds d, gomma_obj_types e where a.lds_id_fk=b.lds_id and b.lds_id=c.lds_id_fk and b.pds_id_fk=d.pds_id and b.obj_type_id_fk=e.obj_type_id and c.lds_date between a.date_from and a.date_to and lds_version_id=[ldsVersionId] and not exists (select * from gomma_lds_structure f where a.obj_id=f.c_obj_id_fk and b.lds_id=f.lds_id_fk and c.lds_date between f.date_from and f.date_to)
17;select obj_id, accession, a.date_from, a.date_to, c.lds_version_id, b.lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date from gomma_obj_versions a, gomma_lds b, gomma_lds_versions c, gomma_pds d, gomma_obj_types e where a.lds_id_fk=b.lds_id and b.lds_id=c.lds_id_fk and b.pds_id_fk=d.pds_id and b.obj_type_id_fk=e.obj_type_id and c.lds_date between a.date_from and a.date_to and lds_version_id=[ldsVersionId] and not exists (select * from gomma_lds_structure f where a.obj_id=f.p_obj_id_fk and b.lds_id=f.lds_id_fk and c.lds_date between f.date_from and f.date_to)
--
-- insert statements
21;insert into gomma_pds (pds_name) values (?)
22;insert into gomma_obj_types (obj_type_name) values (?)
23;insert into gomma_lds (obj_type_id_fk,pds_id_fk,url,is_ontology) select obj_type_id, pds_id,?,? from gomma_obj_types, gomma_pds where obj_type_name=? and pds_name=?
24;insert into gomma_maps (`map_type_id_fk`,`map_class_id_fk`,`map_method_id_fk`,`map_tool_id_fk`,`name`,`is_inst_map`,`is_derived`,`minSupport`,`minConfidence`,`number_of_corrs`,`number_of_domain_objs`,`number_of_range_objs`,`lineage_pattern`) values (?,?,?,?,?,?,?,?,?,?,?,?,?)
25;insert into gomma_obj_corrs (map_id_fk,d_obj_id_fk,r_obj_id_fk,support,confidence,user_checked) values (?,?,?,?,?,?)
26;insert into gomma_map_atts (map_id_fk,att_name,att_scope,att_value_s,att_value_i,att_value_f) values (?,?,?,?,?,?)
29;insert into gomma_lds_versions (lds_id_fk,version,lds_date,structure_type) select lds_id,?,?,? from gomma_lds, gomma_pds, gomma_obj_types where pds_name = ? and obj_type_name = ? and pds_id_fk = pds_id and obj_type_id_fk = obj_type_id
--
-- update statements
30;update gomma_maps set map_type_id_fk=?,map_class_id_fk=?,map_method_id_fk=?,map_tool_id_fk=?,name=?,is_inst_map=?,is_derived=?,minSupport=?,minConfidence=?,number_of_corrs=?,number_of_domain_objs=?,number_of_range_objs=?,lineage_pattern=? where map_id=?
--
-- import statements
41;insert into gomma_obj_versions (lds_id_fk,accession,date_from) select ?,accession,? from tmp_objects;
42;insert into gomma_lds_structure (lds_id_fk,p_obj_id_fk,c_obj_id_fk,rela_type_id_fk,date_from) select ?,parent.obj_id,child.obj_id,rela_type_id,? from tmp_structure, gomma_obj_versions parent, gomma_obj_versions child, gomma_rela_types rela_types where parent.accession = tmp_structure.parent_acc and child.accession = tmp_structure.child_acc and tmp_structure.rel_type = rela_types.name
43;insert into gomma_obj_atts (obj_id_fk,att_name,att_scope,att_value_s,date_from) select o.obj_id, a.attribute, a.scope, a.value, ? from gomma_obj_versions o, tmp_obj_attribute a where o.accession = a.accession
44;insert into tmp_add_objects (accession) select accession from tmp_objects where accession NOT IN (select accession from gomma_obj_versions where lds_id_fk = ? and (? between date_from AND date_to))
45;insert into tmp_del_objects (obj_id) select obj_id from gomma_obj_versions where lds_id_fk = ? and (? between date_from AND date_to) and accession NOT IN (select accession from tmp_objects)
46;insert into tmp_add_structure (parent_acc,child_acc,rel_type) select parent_acc, child_acc, rel_type from tmp_structure where (parent_acc,child_acc,rel_type) NOT IN (select parent.accession,child.accession,rela_types.name from gomma_obj_versions parent, gomma_obj_versions child, gomma_lds_structure structure, gomma_rela_types rela_types where parent.lds_id_fk = ? and child.lds_id_fk = ? and parent.obj_id = structure.p_obj_id_fk and child.obj_id = structure.c_obj_id_fk and structure.rela_type_id_fk = rela_types.rela_type_id and (? between structure.date_from AND structure.date_to))
47;insert into tmp_del_structure (parent_obj_id,child_obj_id,rel_type_id) select structure.p_obj_id_fk,structure.c_obj_id_fk,rela_type_id_fk from gomma_obj_versions parent, gomma_obj_versions child, gomma_lds_structure structure, gomma_rela_types rela_types where parent.lds_id_fk = ? and child.lds_id_fk = ? and parent.obj_id = structure.p_obj_id_fk and child.obj_id = structure.c_obj_id_fk and structure.rela_type_id_fk = rela_types.rela_type_id and (? between structure.date_from AND structure.date_to) and (parent.accession,child.accession,rela_types.name) NOT IN (select parent_acc, child_acc, rel_type from tmp_structure)
48;insert into tmp_add_obj_attribute (accession,attribute,scope,value) select accession,attribute,scope,value from tmp_obj_attribute where (accession,attribute,scope,value) NOT IN (select o.accession, a.att_name, a.att_scope, a.att_value_s from gomma_obj_versions o, gomma_obj_atts a where o.lds_id_fk = ? and o.obj_id = a.obj_id_fk and (? between a.date_from AND a.date_to))
49;insert into tmp_del_obj_attribute (obj_id,attribute,scope,value) select o.obj_id, a.att_name, a.att_scope, a.att_value_s from gomma_obj_versions o, gomma_obj_atts a where o.lds_id_fk = ? and o.obj_id = a.obj_id_fk and (? between a.date_from AND a.date_to) and (o.accession,a.att_name,a.att_scope,a.att_value_s) NOT IN (select accession,attribute,scope,value from tmp_obj_attribute)
50;insert into gomma_obj_versions (lds_id_fk,accession,date_from) select ?,accession,? from tmp_add_objects
51;update gomma_obj_versions SET date_to = ? where lds_id_fk = ? and obj_id IN (select obj_id from tmp_del_objects) and date_to > ?
52;insert into gomma_lds_structure (lds_id_fk,p_obj_id_fk,c_obj_id_fk,rela_type_id_fk,date_from) select ?,parent.obj_id,child.obj_id,rela_types.rela_type_id,? from tmp_add_structure, gomma_obj_versions parent, gomma_obj_versions child, gomma_rela_types rela_types where parent.lds_id_fk = ? and child.lds_id_fk = ? and parent.accession = tmp_add_structure.parent_acc and child.accession = tmp_add_structure.child_acc and (? between child.date_from and child.date_to) and (? between parent.date_from and parent.date_to) and rela_types.name = tmp_add_structure.rel_type
53;update gomma_lds_structure SET date_to = ? where (p_obj_id_fk,c_obj_id_fk,rela_type_id_fk) IN (select parent_obj_id, child_obj_id, rel_type_id from tmp_del_structure) and date_to > ?
54;insert into gomma_obj_atts (obj_id_fk,att_name,att_scope,att_value_s,date_from) select o.obj_id, a.attribute, a.scope, a.value, ? from tmp_add_obj_attribute a, gomma_obj_versions o where o.lds_id_fk = ? and a.accession = o.accession and (? between o.date_from and o.date_to) 
55;update gomma_obj_atts SET date_to = ? where (obj_id_fk,att_name,att_scope,att_value_s) IN (select obj_id, attribute, scope, value from tmp_del_obj_attribute) and date_to > ?
56;insert into gomma_maps (map_type_id_fk,map_class_id_fk,map_method_id_fk,map_tool_id_fk,name,is_inst_map,minSupport,minConfidence,number_of_corrs) select a.map_type_id, b.map_class_id, c.map_method_id, d.map_tool_id, ?,?,?,?,? from gomma_map_types a, gomma_map_classes b, gomma_map_methods c, gomma_map_tools d where a.name = ? and b.name = ? and c.name = ? and d.name = ?
57;insert into gomma_map_src_versions (map_id_fk,lds_version_id_fk,is_domain_lds) select ?,d.lds_version_id,? from gomma_pds a, gomma_obj_types b, gomma_lds c, gomma_lds_versions d where d.lds_id_fk = c.lds_id and c.obj_type_id_fk = b.obj_type_id and c.pds_id_fk = a.pds_id and a.pds_name = ? and b.obj_type_name = ? and d.lds_date = ?
58;insert into gomma_map_inst_versions (map_id_fk,lds_version_id_fk,is_domain_lds) select ?,d.lds_version_id,? from gomma_pds a, gomma_obj_types b, gomma_lds c, gomma_lds_versions d where d.lds_id_fk = c.lds_id and c.obj_type_id_fk = b.obj_type_id and c.pds_id_fk = a.pds_id and a.pds_name = ? and b.obj_type_name = ? and d.lds_date = ?
59;insert into gomma_obj_corrs (map_id_fk,d_obj_id_fk,r_obj_id_fk,corr_type_id_fk,support,confidence,user_checked) select ?, a.obj_id, b.obj_id, c.corr_type_id, ?, ? ,? from gomma_obj_versions a, gomma_obj_versions b, gomma_corr_types c where a.accession = ? and (? between a.date_from and a.date_to) and b.accession = ? and (? between b.date_from and b.date_to) and c.name = ?
60;insert into gomma_map_atts (map_id_fk,att_name,att_value_s,att_value_i,att_value_f) values (?,?,?,?,?)


63;

65; 
--
-- delete statements
71;delete from gomma_pds where pds_name=[pdsName]
72;delete from gomma_obj_types where obj_type_name=[objTypeName]
73;delete from gomma_lds where lds_id=[ldsId]
74;delete from gomma_maps where map_id=[mapId]
75;delete from gomma_obj_corrs where map_id_fk=[mapId]
76;delete from gomma_map_atts where map_id_fk=[mapId]
77;delete from gomma_pub_refs where pub_ref_id=[pubRefId]
78;delete from gomma_pub_atts where pub_ref_id_fk=[pubRefId]

80;
81;
82;
83;