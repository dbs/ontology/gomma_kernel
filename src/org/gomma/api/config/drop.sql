
drop table if exists gomma_pub_atts;
drop table if exists gomma_pub_map_rel;
drop table if exists gomma_pub_refs;
drop table if exists gomma_map_inst_versions;
drop table if exists gomma_map_src_versions;
drop table if exists gomma_obj_corrs;
drop table if exists gomma_corr_types;
drop table if exists gomma_map_att_values;
drop table if exists gomma_map_versions;
drop table if exists gomma_maps;
drop table if exists gomma_map_types;
drop table if exists gomma_map_classes;
drop table if exists gomma_map_tools;
drop table if exists gomma_map_methods;
drop table if exists gomma_map_stats;
drop table if exists gomma_lds_structure;
drop table if exists gomma_rela_types;
drop table if exists gomma_obj_att_values;
drop table if exists gomma_obj_versions;
drop table if exists gomma_lds_versions;
drop table if exists gomma_lds;
drop table if exists gomma_pds;
drop table if exists gomma_obj_types;
drop table if exists gomma_attributes;


drop table if exists gomma_lds_stats;
drop table if exists gomma_lds_version_stats;
drop table if exists gomma_evol_stats;
drop table if exists gomma_map_stats;

drop table if exists gomma_task_objs;
drop table if exists gomma_match_tasks;
drop table if exists gomma_work_packs;
drop table if exists gomma_work_stati;
drop table if exists gomma_stability_analysis;
drop table if exists gomma_correspondence_stabilities;

/**
 * Tables to generate unique identificators
 */

drop table if exists gomma_keys;
drop table if exists gomma_data_source;

/*
Temp-Tables for Import
*/
drop table if exists tmp_objects;

drop table if exists tmp_add_objects;

drop table if exists tmp_del_objects;

drop table if exists tmp_structure;

drop table if exists tmp_add_structure;

drop table if exists tmp_del_structure;

drop table if exists tmp_obj_attribute;

drop table if exists tmp_add_obj_attribute;

drop table if exists tmp_del_obj_attribute;
