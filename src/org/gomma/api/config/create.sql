
create table if not exists gomma_obj_types (
  obj_type_id     integer      not null auto_increment,
  obj_type_name   varchar(200) not null,
  primary key (obj_type_id),
  index using btree (obj_type_name)
);

create table if not exists gomma_pds (
  pds_id          integer      not null auto_increment,
  pds_name        varchar(200) not null,
  primary key (pds_id),
  index using btree (pds_name)
);

create table if not exists gomma_lds (
  lds_id          integer      not null auto_increment,
  pds_id_fk       integer      not null references gomma_pds (pds_id),
  obj_type_id_fk  integer      not null references gomma_obj_types (obj_type_id),
  is_ontology     integer      not null default 0,
  url             varchar(200),
  primary key (lds_id),
  index using btree (pds_id_fk,obj_type_id_fk)
);

create table if not exists gomma_lds_versions (
  lds_version_id  integer      not null auto_increment,
  lds_id_fk       integer      not null references gomma_lds (lds_id),
  version         varchar(50),
  lds_date        date         not null default '1900-01-01',
  structure_type  integer      not null default 0,
  primary key (lds_version_id),
  index using btree (lds_id_fk,version)
);

create table if not exists gomma_attributes (
  att_id          integer      not null auto_increment,
  att_name        varchar(200) not null,
  att_scope       varchar(200) not null,
  data_type_id_fk integer      not null,
  primary key (att_id),
  index using btree (att_id,att_name,att_scope),
  key att_name (att_name)
);

create table if not exists gomma_obj_versions (
  obj_id          integer      not null auto_increment,
  lds_id_fk       integer      not null references gomma_lds (lds_id),
  accession       varchar(200) not null,
  date_from       date         not null default '1900-01-01',
  date_to         date         not null default '2099-12-31',
  primary key (obj_id),
  index using btree (lds_id_fk,accession),
  key accession (accession),
  key date_from (date_from),
  key date_to (date_to)
);

create table if not exists gomma_obj_att_values (
  att_value_id    integer      not null auto_increment,
  obj_id_fk       integer      not null references gomma_obj_versions (obj_id),
  att_id_fk       integer      not null references gomma_attributes (att_id),
  att_value_s     varchar(5000),
  att_value_i     integer,
  att_value_f     float,
  date_from       date         not null default '1900-01-01',
  date_to         date         not null default '2099-12-31',
  PRIMARY KEY  (att_value_id),
  KEY att_value_s (att_value_s(5000)),
  KEY date_from (date_from),
  KEY date_to (date_to),
  KEY att_id_fk (att_id_fk),
  KEY obj_id_fk (obj_id_fk),
  KEY obj_id_fk_2 (obj_id_fk,att_id_fk)
);

create table if not exists gomma_rela_types (
  rela_type_id    integer      not null auto_increment,
  name            varchar(200)  not null,
  descr           varchar(200),
  primary key (rela_type_id),
  index using btree (name)
);

create table if not exists gomma_lds_structure (
  lds_id_fk       integer      not null references gomma_lds (lds_id),
  p_obj_id_fk     integer      not null references gomma_obj_versions (obj_id),
  c_obj_id_fk     integer      not null references gomma_obj_versions (obj_id),
  rela_type_id_fk integer      not null references gomma_rela_types (rela_type_id),
  date_from       date         not null default '1900-01-01',
  date_to         date         not null default '2099-12-31',
  is_directed     integer      not null default 1,
  PRIMARY KEY (`p_obj_id_fk`,`c_obj_id_fk`,`rela_type_id_fk`,`date_from`,`lds_id_fk`),
  key p_obj_id_fk (p_obj_id_fk),
  key c_obj_id_fk (c_obj_id_fk),
  key rela_type_id_fk (rela_type_id_fk),
  key date_from (date_from),
  key date_to (date_to)
);

create table if not exists gomma_map_classes (
  map_class_id    integer      not null auto_increment,
  name            varchar(50)  not null,
  descr           varchar(200),
  primary key (map_class_id),
  index using btree (name)
);

create table if not exists gomma_map_types (
  map_type_id     integer      not null auto_increment,
  name            varchar(50)  not null,
  descr           varchar(200),
  primary key (map_type_id),
  index using btree (name)
);

create table if not exists gomma_map_tools (
  map_tool_id     integer       not null auto_increment,
  name            varchar(50)   not null,
  descr           varchar(200),
  primary key (map_tool_id),
  index using btree (name)
);

create table if not exists gomma_map_methods (
  map_method_id    integer      not null auto_increment,
  name             varchar(50)  not null,
  descr            varchar(200),
  parent_method_id integer,
  primary key (map_method_id),
  index using btree (name)
);

create table if not exists gomma_maps (
  map_id           integer      not null auto_increment,
  map_type_id_fk   integer      not null references gomma_map_types (map_type_id),
  map_class_id_fk  integer      not null references gomma_map_classes (map_class_id),
  map_method_id_fk integer      not null references gomma_map_methods (map_method_id),
  map_tool_id_fk   integer      not null references gomma_map_tools (map_tool_id),
  d_lds_id_fk      integer      not null references gomma_lds (lds_id),
  r_lds_id_fk      integer      not null references gomma_lds (lds_id),
  name             varchar(200) not null,
  is_inst_map      integer      not null default 1,
  is_derived       integer      not null default 0,
  primary key (map_id),
  index using btree (name),
  index using btree (map_type_id_fk,map_class_id_fk,map_method_id_fk,map_tool_id_fk)
);

create table if not exists gomma_map_att_values (
  att_value_id    integer      not null auto_increment,
  map_id_fk       integer      not null references gomma_maps (map_id),
  att_id_fk       integer      not null references gomma_attributes (att_id),
  att_value_s     varchar(2000),
  att_value_i     integer,
  att_value_f     float,
  primary key (att_value_id),
  index using btree (map_id_fk,att_id_fk)
);

create table if not exists gomma_map_versions (
  map_version_id   integer      not null auto_increment,
  map_id_fk        integer      not null references gomma_maps (map_id),
  name             varchar(200),
  creation_date    timestamp,
  min_support      integer      not null default 0,
  min_confidence   float        not null default 0,
  num_corrs        integer      default 0,
  num_d_objs       integer      default 0,
  num_r_objs       integer      default 0,
  primary key (map_version_id),
  index using btree (map_version_id,map_id_fk,name)
);

create table if not exists gomma_corr_types (
  corr_type_id    integer      not null auto_increment,
  name            varchar(50)  not null,
  descr           varchar(200),
  primary key (corr_type_id),
  index using btree (name)
);

create table if not exists gomma_obj_corrs (
  map_version_id_fk  integer      not null references gomma_map_versions (map_version_id),
  d_obj_id_fk        integer      not null references gomma_obj_versions (obj_id),
  r_obj_id_fk        integer      not null references gomma_obj_versions (obj_id),
  corr_type_id_fk    integer      not null default -1 references gomma_corr_types (corr_type_id),
  support            integer      not null default 0,
  confidence         float        not null default 0,
  user_checked       integer      not null default 0,
  primary key (map_version_id_fk,d_obj_id_fk,r_obj_id_fk),
  index using btree (corr_type_id_fk)
);

create table if not exists gomma_map_src_versions (
  map_version_id_fk integer    not null references gomma_map_versions (map_version_id),
  lds_version_id_fk integer    not null references gomma_lds_versions (lds_version_id),
  is_domain_lds     integer    not null default 1,
  primary key (map_version_id_fk,lds_version_id_fk,is_domain_lds)
);

create table if not exists gomma_map_inst_versions (
  map_version_id_fk integer    not null references gomma_map_versions (map_version_id),
  lds_version_id_fk integer    not null references gomma_lds_versions (lds_version_id),
  is_domain_lds     integer    not null default 1,
  primary key (map_version_id_fk,lds_version_id_fk,is_domain_lds)
);

create table if not exists gomma_lds_stats (
  lds_id_fk             integer      not null references gomma_lds (lds_id),
  start_lds_version_id_fk integer    not null references gomma_lds_version (lds_version_id),
  stop_lds_version_id_fk  integer    not null references gomma_lds_version (lds_version_id),
  num_versions          integer      default 0,
  avg_num_concepts      float        default 0,
  avg_num_rels          float        default 0,
  primary key (lds_id_fk),
  index using btree (start_lds_version_id_fk),
  index using btree (stop_lds_version_id_fk)
);

create table if not exists gomma_lds_version_stats (
  lds_id_fk             integer      not null,
  lds_version_id_fk     integer      not null,
  num_objs              integer      default 0,
  num_rels              integer      default 0,
  primary key (lds_version_id_fk)
);

create table if not exists gomma_evol_stats (
  lds_id_fk              integer      not null,
  from_lds_version_id_fk integer      not null,
  to_lds_version_id_fk   integer      not null,
  numberOfAddedObjects   integer,
  numberOfDeletedObjects integer,
  numberOfToObsoleteObjects integer,
  numberOfFusedObjects   integer,
  primary key (from_lds_version_id_fk,to_lds_version_id_fk)
);

create table if not exists gomma_map_stats (
  map_id_fk              integer     not null references gomma_maps (map_id),
  num_versions           integer     default 0,
  avg_num_corrs          float       default 0,
  avg_d_objs             float       default 0,
  avg_r_objs             float       default 0,
  primary key (map_id_fk)
);

create table if not exists gomma_work_stati (
  status_id       integer      not null,
  name            varchar(100),
  primary key (status_id)
);

create table if not exists gomma_work_packs (
  pack_id         integer      not null auto_increment,
  name            varchar(100) not null,
  descr           varchar(255),
  primary key (pack_id)
);

create table if not exists gomma_match_tasks (
  task_id         integer      not null auto_increment,
  pack_id_fk      integer      not null references gomma_work_packs (pack_id),
  name            varchar(100) not null,
  d_lds_version_id_fk integer  not null references gomma_lds_versions (lds_version_id),
  r_lds_version_id_fk integer  not null references gomma_lds_versions (lds_version_id),
  status_id_fk    integer      not null references gomma_work_status (status_id),
  task_conf       varchar(255) not null,
  start_time      timestamp,
  end_time        timestamp,
  resource_url    varchar(100),
  primary key (task_id),
  index using btree (pack_id_fk)
);

create table if not exists gomma_task_objs (
  task_id_fk      integer      not null references gomma_work_tasks (task_id),
  obj_id_fk       integer      not null references gomma_obj_versions (obj_id),
  is_domain_obj   integer      not null default 0,
  primary key (task_id_fk,obj_id_fk,is_domain_obj),
  index using btree (task_id_fk,is_domain_obj)
);

create table if not exists gomma_stability_analysis(
  analysis_id     integer    not null auto_increment,
  name            varchar(200)  not null,
  map_id_fk       integer       not null references gomma_maps(map_id),
  num_versions    integer,
  creation_date   date        not null, 
  first_version_date date  not null,
  last_version_date  date  not null,
  primary key (analysis_id),
  index using btree(map_id_fk,creation_date)
);

create table if not exists gomma_correspondence_stabilities(
  domain_obj_id_fk   integer      not null references gomma_obj_versions (obj_id),
  range_obj_id_fk    integer      not null references gomma_obj_versions (obj_id),
  analysis_id_fk     integer      not null references gomma_stability_analysises(analysis_id),
  last_confidence    float        not null default -1,
  stabAvg            float        not null default -1,
  stabWM             float        not null default -1,
  primary key(domain_obj_id_fk,range_obj_id_fk,analysis_id_fk)
);

/*
 * Table for generating unique object keys within the GOMMA application
 */
create table if not exists gomma_keys (
  gomma_obj_id    integer      not null auto_increment,
  primary key (gomma_obj_id)
);

create table if not exists gomma_data_source (
  source_id       integer      not null,
  primary key (source_id)
);



/*
Temp-Tables for Import
*/
create table if not exists tmp_objects (
  accession       varchar(200) NOT NULL,
  primary key  (accession)
);

create table if not exists tmp_add_objects (
  accession       varchar(200) NOT NULL,
  primary key  (accession)
);

create table if not exists tmp_del_objects (
  obj_id          integer NOT NULL,
  primary key  (obj_id)
);

create table if not exists tmp_structure (
  parent_acc      varchar(200) NOT NULL,
  child_acc       varchar(200) NOT NULL,
  rel_type        varchar(200) NOT NULL,
  key parent_acc (parent_acc),
  key child_acc (child_acc),
  key rel_type (rel_type)
);

create table if not exists tmp_add_structure (
  parent_acc      varchar(200) NOT NULL,
  child_acc       varchar(200) NOT NULL,
  rel_type        varchar(200) NOT NULL,
  key parent_acc (parent_acc),
  key child_acc (child_acc),
  key rel_type (rel_type)
);

create table if not exists tmp_del_structure (
  parent_obj_id   integer NOT NULL,
  child_obj_id    integer NOT NULL,
  rel_type_id     integer NOT NULL,
  key parent_acc (parent_obj_id),
  key child_acc (child_obj_id),
  key rel_type_id (rel_type_id)
);

create table if not exists tmp_obj_attribute (
  accession       varchar(200) NOT NULL,
  attribute       varchar(200) NOT NULL,
  scope           varchar(200) NOT NULL default "N/A",
  data_type       integer      NOT NULL default 1,
  value           varchar(5000) NOT NULL,
  key accession (accession),
  key attribute (attribute),
  key scope (scope),
  key value (value(1000)),
  index using btree (accession,attribute,scope)
);

create table if not exists tmp_add_obj_attribute (
  accession       varchar(200) NOT NULL,
  attribute       varchar(200) NOT NULL,
  scope           varchar(200) NOT NULL,
  data_type       integer      NOT NULL default 1,
  value           varchar(5000) NOT NULL,
  key accession (accession),
  key attribute (attribute),
  key scope (scope),
  index using btree (accession,attribute,scope),
  key value (value(1000))
);

create table if not exists tmp_del_obj_attribute (
  obj_id          integer      NOT NULL,
  attribute       varchar(200) NOT NULL,
  scope           varchar(200) NOT NULL,
  data_type       integer      NOT NULL default 1,
  value           varchar(5000) NOT NULL,
  key obj_id (obj_id),
  key attribute (attribute),
  key scope (scope),
  key value (value(1000)),
  index using btree (obj_id,attribute,scope)
);


