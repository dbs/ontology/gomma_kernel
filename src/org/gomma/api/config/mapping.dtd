<?xml version="1.0" encoding="UTF-8"?>

<!-- DTD for exchanging mapping data -->

<!ELEMENT mapping (metadata, (correspondences|external_access))>
<!ATTLIST mapping 
			name                    CDATA #REQUIRED
			is_instance_map         (yes|no|true|false) "false"
			mapping_class			(instance_mapping|annotation_mapping|ontology_mapping) "ontology_mapping"
			mapping_type			(is_equivalent_to) "is_equivalent_to">


<!ELEMENT metadata (domain_sources, range_sources, instance_sources?, params?, references?)>
<!ATTLIST metadata 
			mapping_method CDATA #IMPLIED
			mapping_tool CDATA #IMPLIED
			minConfidence CDATA #IMPLIED
			minSupport CDATA #IMPLIED>

<!ELEMENT domain_sources   (source+)>
<!ELEMENT range_sources    (source+)>
<!ELEMENT instance_sources (domain_sources, range_sources)>
<!ELEMENT params           (param+)>
<!ELEMENT references       (reference+)>

<!ELEMENT source EMPTY>
<!ATTLIST source 
			identifier					ID    #REQUIRED
			objecttype					CDATA #REQUIRED
			name						CDATA #REQUIRED
			timestamp					CDATA #REQUIRED
			version						CDATA #IMPLIED
			is_ontology					(yes|no|true|false) "false"
			structural_type				(edgeless|undirected|directed|directed_acyclic|tree) "edgeless"
			url							CDATA #IMPLIED>

<!ELEMENT param (#PCDATA)>
<!ATTLIST param 
			name                        CDATA #REQUIRED
			scope                       CDATA #REQUIRED
			type                        (integer|float|string) "string">

<!ELEMENT reference EMPTY>
<!ATTLIST reference 
			type						CDATA #IMPLIED
			ref							CDATA #IMPLIED
			url							CDATA #IMPLIED>

<!ELEMENT correspondences (correspondence+)>

<!ELEMENT corr_att (#PCDATA)>
<!ATTLIST corr_att 
			name                        CDATA #REQUIRED
			type                        (integer|float|string) "string">

<!ELEMENT correspondence  (domain_objects, range_objects, corr_att*)>
<!ATTLIST correspondence 
			support						NMTOKEN #IMPLIED
			confidence					NMTOKEN #IMPLIED
			user_checked				NMTOKEN #IMPLIED
			corr_type					CDATA   #IMPLIED>

<!ELEMENT domain_objects (object+)>
<!ELEMENT range_objects  (object+)>

<!ELEMENT object (params*)>
<!ATTLIST object 
			accession					CDATA #REQUIRED
			objecttype					CDATA #REQUIRED
			source_name					IDREF #REQUIRED>

<!ELEMENT ea_param (#PCDATA)>
<!ATTLIST ea_param 
			name                        CDATA #REQUIRED>

<!ELEMENT external_access (ea_param*)>
<!ATTLIST external_access 
			access_class				CDATA #REQUIRED
			location					CDATA #REQUIRED>