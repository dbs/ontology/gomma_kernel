package org.gomma.api;

/**
 * The class implements the administrative repository interface 
 * for the FEVER system using a relational database system at 
 * the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class RepositoryAPI_FEVER extends RepositoryAPI_RDBMS {

	/**
	 * @see RepositoryAPI_RDBMS#getCreationSchemaScriptFileName()
	 */
	protected String getCreationSchemaScriptFileName() {
		throw new RuntimeException("No implementation available");
	}

	/**
	 * @see RepositoryAPI_RDBMS#getDropSchemaScriptFileName()
	 */
	protected String getDropSchemaScriptFileName() {
		throw new RuntimeException("No implementation available");
	}

}
