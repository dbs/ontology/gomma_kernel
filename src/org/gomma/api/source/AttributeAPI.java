package org.gomma.api.source;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.attribute.AttributeSet;

public interface AttributeAPI {

	public AttributeSet getAttributeSet() throws RepositoryException;
}
