package org.gomma.api.source;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;

/**
 * The class implements the source API. It is the 
 * implementation for the FEVER system using a relational 
 * database at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceAPI_FEVER extends SourceAPI_RDBMS {

	private static final String OBJECT_TYPE = "N/A";
	
	/**
	 * @see SourceAPI#getSourceSet(String)
	 */
	@Override
	public SourceSet getSourceSet(String objType) throws RepositoryException {
		return super.loadSourceSet("where '"+OBJECT_TYPE+"'='"+objType+"'");
	}

	/**
	 * @see SourceAPI#getSourceSet(String, String)
	 */
	@Override
	public SourceSet getSourceSet(String objType, String physicalSource) throws RepositoryException {
		return super.loadSourceSet("where '"+OBJECT_TYPE+"'='"+objType+"' AND name='"+physicalSource+"'");
	}


	
	/**
	 * @see SourceAPI#insertSource(String, String, String, boolean)
	 */
	@Override
	public int insertSource(String objectType, String physicalSource, String sourceURL, boolean isOntology) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceAPI#updateSource(Source)
	 */
	@Override
	public int updateSource(Source s) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceAPI#deleteSource(Source)
	 */
	@Override
	public int deleteSource(Source s) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	
	/**
	 * The method returns the FEVER specific statement to load a set of sources.
	 * @return FEVER specific statement for loading a set of sources
	 */
	@Override
	protected String getLoadSourceStatement() { 
		return
		"select id,'N/A',name,'N/A',0 "+
		"from dataset ";
	}
}
