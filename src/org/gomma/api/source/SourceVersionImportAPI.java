package org.gomma.api.source;

import java.util.List;

import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public interface SourceVersionImportAPI {

	/**
	 * @deprecated
	 */
	public void insertIntoTmpTables(String[] dmlQueryPattern) throws RepositoryException;
	
	/**
	 * @deprecated
	 */
	public void insertIntoTmpTables(List<String[]> objects, String statement) throws RepositoryException;
	
	public void cleanTmpTables() throws RepositoryException;
	
	public void computeVersionDiffsForImport(int sourceID, SimplifiedGregorianCalendar lastTimestamp) throws RepositoryException;
	
	public void integrateVersionDiffs(int sourceID, SimplifiedGregorianCalendar startTimestamp, SimplifiedGregorianCalendar endTimestamp) throws RepositoryException;
	
	public void importFirstVersion(int sourceID,SimplifiedGregorianCalendar from) throws RepositoryException;
	
	
	
	
	public boolean insertTemporaryObjectSet(List<ImportObj> objList) throws RepositoryException;
	
	public boolean insertTemporarySourceStructure(ImportSourceStructure structure) throws RepositoryException;
	
	public int integrateNewAttributes() throws RepositoryException;
	
	public int integrateNewRelationshipTypes() throws RepositoryException; 
	
//	public boolean appendNewObjectSet(String objectType, String sourceName, Calendar versionDate) throws RepositoryException;
//	public boolean updatePastObjectSet(String objectType, String sourceName, Calendar versionDate) throws RepositoryException;
//	public boolean updateFutureObjectSet();
	
	
}
