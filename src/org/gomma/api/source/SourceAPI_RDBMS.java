package org.gomma.api.source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;

/**
 * The class implements the source API. It is the default 
 * implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceAPI_RDBMS implements SourceAPI {
	
	/**
	 * Constructor of the class.
	 */
	public SourceAPI_RDBMS() {}
	
	/**
	 * @see SourceAPI#getSourceSet()
	 */
	public SourceSet getSourceSet() throws RepositoryException {
		return this.loadSourceSet("");
	}
	
	/**
	 * @see SourceAPI#getSourceSet(String)
	 */
	public SourceSet getSourceSet(String objType) throws RepositoryException {
		return this.loadSourceSet(" and obj_type_name='"+objType+"'");
	}
	
	/**
	 * @see SourceAPI#getSourceSet(String, String)
	 */
	public SourceSet getSourceSet(String objType, String physicalSource) throws RepositoryException {
		return this.loadSourceSet(" and obj_type_name='"+objType+"' and pds_name='"+physicalSource+"'");
	}
	
	/**
	 * @see SourceAPI#insertSource(String, String, String, boolean)
	 */
	public int insertSource(String objectType, String physicalSource,
			String sourceURL, boolean isOntology) throws RepositoryException {
		
		int affectedRows = this.insertObjectType(objectType);
		affectedRows += this.insertPDS(physicalSource);
		affectedRows += this.insertLDS(objectType, physicalSource, sourceURL, isOntology);
		
		return affectedRows;
	}
	
	/**
	 * @see SourceAPI#updateSource(Source)
	 */
	public int updateSource(Source s) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getUpdateLogicalSourceStatement();
		PreparedStatement pStmt=null;
		int affectedRows=0;
		
		this.insertObjectType(s.getObjectType());
		this.insertPDS(s.getPhysicalSourceName());
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,s.getObjectType());
			pStmt.setString(2,s.getPhysicalSourceName());
			pStmt.setString(3,s.getSourceURL());
			pStmt.setInt(4,s.isOntology()?1:0);
			pStmt.setInt(5,s.getID());
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	/**
	 * @see SourceAPI#deleteSource(Source)
	 */
	public int deleteSource(Source s) throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getDeleteLogicalSourceStatement().replaceAll(
				"\\[ldsID\\]",Integer.toString(s.getID()));
		
		return dbh.executeDml(query);
	}


	
	
	
	
	/**
	 * The method return a set of source objects meeting the specified query conditions. 
	 * @param queryCond query conditions
	 * @return set of sources
	 * @throws RepositoryException
	 */
	protected SourceSet loadSourceSet(String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getLoadSourceStatement()+queryCond;
		ResultSet rs = dbh.executeSelect(query);
		SourceSet set = this.extractSources(rs, new SourceSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	/**
	 * The method returns the default SQL statement to load a set of sources.
	 * @return SQL statement for loading a set of sources
	 */
	protected String getLoadSourceStatement() { 
		return 
		"select lds_id,obj_type_name,pds_name,url,is_ontology "+
		"from gomma_lds a, gomma_pds b,gomma_obj_types c "+
		"where a.pds_id_fk=b.pds_id and a.obj_type_id_fk=c.obj_type_id";
	}
	
	/**
	 * The method returns the default SQL statement to store an semantic object type into the repository.
	 * @return SQL statement for inserting an object type
	 */
	protected String getInsertObjectTypeStatement() { 
		return 
		"insert into gomma_obj_types (obj_type_name) "+
		"select ? from dual where not exists (select * from gomma_obj_types where obj_type_name=?)";
	}
	
	/**
	 * The method returns the default SQL statement to store a physical source name into the repository.
	 * @return SQL statement for inserting a physical source 
	 */
	protected String getInsertPhysicalSourceStatement() { 
		return
		"insert into gomma_pds (pds_name) "+
		"select ? from dual where not exists (select * from gomma_pds where pds_name=?)";
	}
	
	/**
	 * The method returns the default SQL statement to store a logical source into the repository.
	 * @return SQL statement for inserting a logical source
	 */
	protected String getInsertLogicalSourceStatement() { 
		return
		"insert into gomma_lds (obj_type_id_fk,pds_id_fk,url,is_ontology) "+
		"select obj_type_id, pds_id,?,? "+
		"from gomma_obj_types a, gomma_pds b "+
		"where a.obj_type_name=? "+
		  "and b.pds_name=? "+
		  "and not exists ("+
		  	"select * from gomma_lds c "+
		  	"where a.obj_type_id=c.obj_type_id_fk and b.pds_id=c.pds_id_fk)";
	}
	
	/**
	 * The method returns the default SQL statement to update a logical source within the repository.
	 * @return SQL statement for updating a logical source
	 */
	protected String getUpdateLogicalSourceStatement() { 
		return
		"update gomma_lds set "+
		"obj_type_id_fk=(select obj_type_id from gomma_obj_types where obj_type_name=?),"+
		"pds_id_fk=(select pds_id from gomma_pds where pds_name=?),"+
		"url=?,is_ontology=? "+
		"where lds_id=?";
	}
	
	/**
	 * The method returns the default SQL statement to delete logical sources from the repository.
	 * @return SQL statement for deleting a logical source
	 */
	protected String getDeleteLogicalSourceStatement() { 
		return
		"delete from gomma_lds where lds_id = [ldsID]";
	}
	
	
	
	/**
	 * The method creates and stores a semantic object type within the repository
	 * and returns the number of affected rows. 
	 * @param objectType semantic object type
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	private int insertObjectType(String objectType) throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getInsertObjectTypeStatement();
		PreparedStatement pStmt=null;
		int affectedRows=0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,objectType);
			pStmt.setString(2,objectType);
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * The method creates and stores a physical source within the repository 
	 * and returns the number of affected rows.
	 * @param sourceName physical source name
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	private int insertPDS(String sourceName) throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getInsertPhysicalSourceStatement();
		PreparedStatement pStmt=null;
		int affectedRows=0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,sourceName);
			pStmt.setString(2,sourceName);
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * The method creates and stores a logical source within the repository
	 * and returns the number of affected rows.
	 * @param objType semantic object type
	 * @param sourceName physical source name 
	 * @param sourceURL source URL
	 * @param isOntology true if the source is an ontology, false otherwise
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	private int insertLDS(String objType, String sourceName, String sourceURL, boolean isOntology) throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getInsertLogicalSourceStatement();
		PreparedStatement pStmt=null;
		int affectedRows=0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,sourceURL);
			pStmt.setInt(2,isOntology?1:0);
			pStmt.setString(3,objType);
			pStmt.setString(4,sourceName);
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	
	/**
	 * The method extracts and returns a set of sources from the given SQL query result set
	 * @param rs query result set
	 * @param set set of sources
	 * @return set of extracted sources
	 * @throws RepositoryException
	 */
	private SourceSet extractSources(ResultSet rs, SourceSet set) throws RepositoryException {
		Source s;
		
		try {
			while (rs.next()) {
				s = new Source.Builder(rs.getInt(1))
						.objectType(rs.getString(2))
						.physicalSourceName(rs.getString(3))
						.sourceURL(rs.getString(4))
						.isOntology(rs.getInt(5)==1)
						.build();
				set.addSource(s);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}