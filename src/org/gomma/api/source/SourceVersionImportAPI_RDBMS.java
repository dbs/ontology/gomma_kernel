package org.gomma.api.source;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.io.importer.models.ImportObj.ImportObjAttribute;
import org.gomma.io.importer.models.ImportSourceStructure.ImportObjRelationship;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the source version import API. It is the default
 * implementation for a relational database.
 * @version 1.0
 * @since JDK 1.5
 */
public class SourceVersionImportAPI_RDBMS implements SourceVersionImportAPI {

	private static final String UPLOAD_TMP_OBJECT = 
		"insert into tmp_objects (accession) value (?)";
	
	private static final String UPLOAD_TMP_OBJECT_ATTRIBUTE = 
		"insert into tmp_obj_attribute (accession,attribute,scope,data_type,value) values (?,?,?,?,?)";
	
	private static final String UPLOAD_SOURCE_STRUCTURE = 
		"insert into tmp_structure (parent_acc,child_acc,rel_type) values (?,?,?)";
	
	
	
	private static final String COMPUTE_ADDED_OBJECTS =
		"insert into tmp_add_objects (accession) "+
		"select accession from tmp_objects "+
		"where accession NOT IN ("+
			"select accession from gomma_obj_versions "+
			"where lds_id_fk = ? "+
			  "and (? between date_from AND date_to))";
	
	private static final String COMPUTE_DELETED_OBJECTS =
		"insert into tmp_del_objects (obj_id) "+
		"select obj_id from gomma_obj_versions "+
		"where lds_id_fk = ? "+
		  "and (? between date_from AND date_to) "+
		  "and accession NOT IN (select accession from tmp_objects)";
	
	private static final String COMPUTE_ADDED_RELATIONSHIPS =
		"insert into tmp_add_structure (parent_acc,child_acc,rel_type) "+
		"select parent_acc, child_acc, rel_type from tmp_structure "+
		"where (parent_acc,child_acc,rel_type) NOT IN ("+
			"select parent.accession,child.accession,rela_types.name "+
			"from gomma_obj_versions parent, gomma_obj_versions child, gomma_lds_structure structure, gomma_rela_types rela_types "+
			"where parent.lds_id_fk = ? "+
			  "and child.lds_id_fk = ? "+
			  "and parent.obj_id = structure.p_obj_id_fk "+
			  "and child.obj_id = structure.c_obj_id_fk "+
			  "and structure.rela_type_id_fk = rela_types.rela_type_id "+
			  "and (? between structure.date_from AND structure.date_to))";
	
	private static final String COMPUTE_DELETED_RELATIONSHIPS = 
		"insert into tmp_del_structure (parent_obj_id,child_obj_id,rel_type_id) "+
		"select structure.p_obj_id_fk,structure.c_obj_id_fk,rela_type_id_fk "+
		"from gomma_obj_versions parent, gomma_obj_versions child, gomma_lds_structure structure, gomma_rela_types rela_types "+
		"where parent.lds_id_fk = ? "+
		  "and child.lds_id_fk = ? "+
		  "and parent.obj_id = structure.p_obj_id_fk "+
		  "and child.obj_id = structure.c_obj_id_fk "+
		  "and structure.rela_type_id_fk = rela_types.rela_type_id and " +
		  "(? between structure.date_from AND structure.date_to) "+
		  "and (parent.accession,child.accession,rela_types.name) NOT IN (select parent_acc, child_acc, rel_type from tmp_structure)";
	
	private static final String COMPUTE_ADDED_ATTRIBUTES =
		"insert into tmp_add_obj_attribute (accession,attribute,scope,value) "+
		"select accession,attribute,scope,value from tmp_obj_attribute "+
		"where (accession,attribute,scope,value) NOT IN ("+
			"select o.accession, a.att_name, a.att_scope, v.att_value_s "+
			"from gomma_obj_versions o, gomma_obj_att_values v, gomma_attributes a "+
			"where o.lds_id_fk = ? "+
			  "and o.obj_id = v.obj_id_fk "+
			  "and v.att_id_fk = a.att_id "+
			  "and (? between v.date_from AND v.date_to))";
	
	private static final String COMPUTE_DELETED_ATTRIBUTES = 
		"insert into tmp_del_obj_attribute (obj_id,attribute,scope,value) "+
		"select o.obj_id, a.att_name, a.att_scope, v.att_value_s "+
		"from gomma_obj_versions o, gomma_obj_att_values v, gomma_attributes a "+
		"where o.lds_id_fk = ? "+
		  "and o.obj_id = v.obj_id_fk "+
		  "and (? between v.date_from AND v.date_to) "+
	      "and v.att_id_fk = a.att_id "+
		  "and NOT EXISTS (select tmp.accession from tmp_obj_attribute tmp " +
		  "where o.accession = tmp.accession and a.att_name = tmp.attribute and a.att_scope = tmp.scope and v.att_value_s = tmp.value)";
	
	private static final String INTEGRATE_ADDED_OBJECTS =
		"insert into gomma_obj_versions (lds_id_fk,accession,date_from) select ?,accession,? from tmp_add_objects";
	
	private static final String UPDATE_DELETED_OBJECTS = 
		"update gomma_obj_versions SET date_to = ? where lds_id_fk = ? and obj_id IN (select obj_id from tmp_del_objects) and date_to > ?";
	
	private static final String INTEGRATE_ADDED_RELATIONSHIPS =
		"insert into gomma_lds_structure (lds_id_fk,p_obj_id_fk,c_obj_id_fk,rela_type_id_fk,date_from) "+
		"select ?,parent.obj_id,child.obj_id,rela_types.rela_type_id,? "+
		"from tmp_add_structure, gomma_obj_versions parent, gomma_obj_versions child, gomma_rela_types rela_types "+
		"where parent.lds_id_fk = ? "+
		  "and child.lds_id_fk = ? "+
		  "and parent.accession = tmp_add_structure.parent_acc "+
		  "and child.accession = tmp_add_structure.child_acc "+
		  "and (? between child.date_from and child.date_to) "+
		  "and (? between parent.date_from and parent.date_to) "+
		  "and rela_types.name = tmp_add_structure.rel_type";
	
	private static final String UPDATE_DELETED_RELATIONSHIPS =
		"update gomma_lds_structure SET date_to = ? "+
		"where (p_obj_id_fk,c_obj_id_fk,rela_type_id_fk) IN ("+
			"select parent_obj_id, child_obj_id, rel_type_id from tmp_del_structure) and date_to > ?";
	
	private static final String INTEGRATE_ADDED_ATTRIBUTES = 
		"insert into gomma_obj_att_values (obj_id_fk,att_id_fk,att_value_s,date_from) "+
		"select o.obj_id, a.att_id, tmp.value, ? "+
		"from tmp_add_obj_attribute tmp, gomma_obj_versions o, gomma_attributes a "+
		"where o.lds_id_fk = ? "+
		  "and tmp.accession = o.accession "+
	      "and tmp.attribute = a.att_name "+
	      "and tmp.scope = a.att_scope "+
		  "and (? between o.date_from and o.date_to)";
	
	private static final String UPDATE_DELETED_ATTRIBUTES =
		"update gomma_obj_att_values SET date_to = ? "+
			"where (obj_id_fk,att_id_fk,att_value_s) IN ( "+
				"select tmp.obj_id, a.att_id, value from tmp_del_obj_attribute tmp, gomma_attributes a "+
	            "where tmp.attribute = a.att_name and tmp.scope = a.att_scope) and date_to > ?";
	
	private static final String SAVE_FIRST_VERSION_OBJECTS =
		"insert into gomma_obj_versions (lds_id_fk,accession,date_from) "+
		"select ?,accession,? from tmp_objects";
	
	private static final String SAVE_FIRST_VERSION_STRUCTURE =
		"insert into gomma_lds_structure (lds_id_fk,p_obj_id_fk,c_obj_id_fk,rela_type_id_fk,date_from) "+
		"select ?,parent.obj_id,child.obj_id,rela_type_id,? "+
		"from tmp_structure, gomma_obj_versions parent, gomma_obj_versions child, gomma_rela_types rela_types "+
		"where parent.accession = tmp_structure.parent_acc "+
		  "and child.accession = tmp_structure.child_acc "+
		  "and tmp_structure.rel_type = rela_types.name";
	
	private static final String SAVE_FIRST_VERSION_ATTRIBUTES = 
		"insert into gomma_obj_att_values (obj_id_fk,att_id_fk,att_value_s,date_from) "+
		"select o.obj_id, a.att_id, tmp.value, ? "+
		"from gomma_obj_versions o, tmp_obj_attribute tmp, gomma_attributes a "+
		"where o.accession = tmp.accession "+
	    "and tmp.attribute = a.att_name "+
	    "and tmp.scope = a.att_scope";
	
	
	
	private static final String INTEGRATE_NEW_ATTRIBUTES = 
		"insert into gomma_attributes (att_name, att_scope, data_type_id_fk) "+
		"select distinct attribute, scope, '1' from tmp_obj_attribute a "+
		"where not exists ("+
			"select * from gomma_attributes b "+
			"where a.attribute=b.att_name "+
			  "and a.scope=b.att_scope )";
	
	private static final String INTEGRATE_NEW_RELATIONSHIP_TYPES = 
		"insert into gomma_rela_types (name) "+
		"select distinct rel_type from tmp_structure a "+
		"where not exists ("+
			"select * from gomma_rela_types b "+
			"where a.rel_type=b.name)";
	
	public SourceVersionImportAPI_RDBMS() {}
	
	public void cleanTmpTables() throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		dbh.executeDml("truncate tmp_objects");
		dbh.executeDml("truncate tmp_add_objects");
		dbh.executeDml("truncate tmp_del_objects");
		dbh.executeDml("truncate tmp_structure");
		dbh.executeDml("truncate tmp_add_structure");
		dbh.executeDml("truncate tmp_del_structure");
		dbh.executeDml("truncate tmp_obj_attribute");
		dbh.executeDml("truncate tmp_add_obj_attribute");
		dbh.executeDml("truncate tmp_del_obj_attribute");
		
	}
	
	/**
	 * @deprecated
	 */
	public void insertIntoTmpTables(String[] statements) throws RepositoryException {
		DatabaseHandler.getInstance().executeDml(statements);
	}
	
	/**
	 * @deprecated
	 */
	public void insertIntoTmpTables(List<String[]> objects, String statement) throws RepositoryException {
		if (objects.size()==0) return;
	
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		
		try {
			pStmt = dbh.prepareStatement(statement);
			for (String[] values : objects) {
				for (int j=0;j<values.length;j++) {
					pStmt.setString(j+1,values[j]);
				}
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException("insertIntoTmpTables(): "+e.getMessage());
		}
	}
	public void computeVersionDiffsForImport(int sourceID, SimplifiedGregorianCalendar lastTimestamp) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		
		try {
			//Added Objects
			String sqlQuery = COMPUTE_ADDED_OBJECTS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(lastTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			
			//Deleted Objects
			sqlQuery = COMPUTE_DELETED_OBJECTS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(lastTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Added Relationships
			sqlQuery = COMPUTE_ADDED_RELATIONSHIPS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setInt(2,sourceID);
			pStmt.setDate(3,new Date(lastTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			
			//Deleted Relationships
			sqlQuery = COMPUTE_DELETED_RELATIONSHIPS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setInt(2,sourceID);
			pStmt.setDate(3,new Date(lastTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			
			//Added Attributes
			sqlQuery = COMPUTE_ADDED_ATTRIBUTES;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(lastTimestamp.getTime().getTime()));			
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//System.out.println(pStmt.toString());
			//Deleted Attributes
			sqlQuery = COMPUTE_DELETED_ATTRIBUTES;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(lastTimestamp.getTime().getTime()));
			pStmt.execute();
			
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException("computeVersionDiffsForImport(): "+e.getMessage());
		}
	}
	public void integrateVersionDiffs(int sourceID, SimplifiedGregorianCalendar startTimestamp, SimplifiedGregorianCalendar endTimestamp) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		
		try {
			//Integrate added Objects
			String sqlQuery = INTEGRATE_ADDED_OBJECTS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(startTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Update deleted Objects
			sqlQuery = UPDATE_DELETED_OBJECTS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setDate(1,new Date(endTimestamp.getTime().getTime()));
			pStmt.setInt(2,sourceID);
			pStmt.setDate(3,new Date(endTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Integrate added Relationships
			sqlQuery = INTEGRATE_ADDED_RELATIONSHIPS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(startTimestamp.getTime().getTime()));
			pStmt.setInt(3,sourceID);
			pStmt.setInt(4,sourceID);
			pStmt.setDate(5,new Date(startTimestamp.getTime().getTime()));
			pStmt.setDate(6,new Date(startTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Update deleted Relationships
			sqlQuery = UPDATE_DELETED_RELATIONSHIPS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setDate(1,new Date(endTimestamp.getTime().getTime()));
			pStmt.setDate(2,new Date(endTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Integrate added Attributes
			sqlQuery = INTEGRATE_ADDED_ATTRIBUTES;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setDate(1,new Date(startTimestamp.getTime().getTime()));
			pStmt.setInt(2,sourceID);
			pStmt.setDate(3,new Date(startTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			//Update deleted Attributes
			sqlQuery = UPDATE_DELETED_ATTRIBUTES;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setDate(1,new Date(endTimestamp.getTime().getTime()));
			pStmt.setDate(2,new Date(endTimestamp.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException("integrateVersionDiffs(): "+e.getMessage());
		} 
	}
	public void importFirstVersion(int sourceID, SimplifiedGregorianCalendar from) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		
		try {
			String sqlQuery = SAVE_FIRST_VERSION_OBJECTS;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(from.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			sqlQuery = SAVE_FIRST_VERSION_STRUCTURE;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setInt(1,sourceID);
			pStmt.setDate(2,new Date(from.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
			sqlQuery = SAVE_FIRST_VERSION_ATTRIBUTES;
			pStmt = dbh.prepareStatement(sqlQuery);
			pStmt.setDate(1,new Date(from.getTime().getTime()));
			pStmt.execute();
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException("importFirstVersion(): "+e.getMessage());
		}
	}
	
	
	
	
	
	
	
	public boolean insertTemporaryObjectSet(List<ImportObj> objList) throws RepositoryException {
		
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmtObj = null, pStmtAtt = null;
		int importInterval = 1000, cnt=0;
		
		try {
			pStmtObj = dbh.prepareStatement(UPLOAD_TMP_OBJECT);
			pStmtAtt = dbh.prepareStatement(UPLOAD_TMP_OBJECT_ATTRIBUTE);
			
			
			for (ImportObj importObj : objList) {
				
				if (cnt++==importInterval) {
					pStmtObj.executeBatch();
					pStmtAtt.executeBatch();
					
					pStmtObj.clearBatch();
					pStmtAtt.clearBatch();
					
					cnt=0;
				}
				
				String acc = importObj.getAccessionNumber();
				
				if(acc.length()>600){ //max Laenge in db--> varchar(996)
					acc = acc.substring(0, 550);
				}
				
				pStmtObj.setString(1,acc);
				pStmtObj.addBatch();
				
				for (ImportObjAttribute att : importObj.getAttributeList()) {
					pStmtAtt.setString(1,acc);
					pStmtAtt.setString(2,att.getAttName());
					pStmtAtt.setString(3,att.getScope());
					pStmtAtt.setInt(4,att.getDataType().getID());
					pStmtAtt.setString(5,att.getValue());
					pStmtAtt.addBatch();
				}
			}
			
			if (cnt!=0) {
				pStmtObj.executeBatch();
				pStmtAtt.executeBatch();
			}
			
		} catch (SQLException e) {
			throw new RepositoryException("insertTemporaryObjectSet(): "+e.getMessage());
		} finally {
			if (pStmtObj!=null) dbh.closeStatement(pStmtObj);
			if (pStmtAtt!=null) dbh.closeStatement(pStmtAtt);
		}
		
		return false;
	}
	
	public boolean insertTemporarySourceStructure(ImportSourceStructure structure) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		
		try {
			pStmt = dbh.prepareStatement(UPLOAD_SOURCE_STRUCTURE);
			
			for (ImportObjRelationship rel : structure.getRelationshipSet()) {
				pStmt.setString(1, rel.getFromAccessionNumber());
				pStmt.setString(2, rel.getToAccessionNumber());
				pStmt.setString(3, rel.getType());
				pStmt.addBatch();
			}
			pStmt.executeBatch();
		} catch (SQLException e) {
			throw new RepositoryException("insertTemporarySourceStructure(): "+e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return false;
	}
	
	public int integrateNewAttributes() throws RepositoryException {
		return DatabaseHandler.getInstance().executeDml(INTEGRATE_NEW_ATTRIBUTES);
	}
	
	public int integrateNewRelationshipTypes() throws RepositoryException {
		return DatabaseHandler.getInstance().executeDml(INTEGRATE_NEW_RELATIONSHIP_TYPES);
	}
	
}
