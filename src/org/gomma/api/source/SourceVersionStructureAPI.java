package org.gomma.api.source;

import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The interface defines method to manage the structure of a source version, 
 * i.e., a set of relationship among object of the source version within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceVersionStructureAPI {

	/**
	 * The method returns the source structure for the given source version.
	 * @param v source version for which the source structure should be loaded
	 * @return source structure
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 * @throws GraphInitializationException 
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws RepositoryException, WrongSourceException, GraphInitializationException;
	
	/**
	 * The method returns the source structure for the given source version 
	 * limited to the specified set of objects. 
	 * @param v source version
	 * @param inputObjs set of object for that the structure should be loaded
	 * @return source structure
	 * @throws RepositoryException
	 * @throws GraphInitializationException
	 * @throws WrongSourceException
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v, ObjSet inputObjs) throws RepositoryException, GraphInitializationException, WrongSourceException;
	
	/**
	 * The method returns the source structure for the given source version
	 * limited to the specified set of objects and additional predecessor/successor 
	 * objects. 
	 * @param s source version
	 * @param inputObjs set of predefined objects
	 * @param nLevels number of predecessor/successor object levels
	 * @param getSuccessors true if the successor levels should be loaded; false otherwise
	 * @return source structure
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 * @throws GraphInitializationException
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion s, ObjSet inputObjs, int nLevels, boolean getSuccessors) throws RepositoryException, WrongSourceException, GraphInitializationException;
	
	
}
