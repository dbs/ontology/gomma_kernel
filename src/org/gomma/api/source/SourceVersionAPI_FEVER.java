package org.gomma.api.source;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The class implements the source version API. It is the implementation
 * for the FEVER system using a relational database at the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceVersionAPI_FEVER extends SourceVersionAPI_RDBMS {

	private static final SimplifiedGregorianCalendar CURRENT_DATE = 
		SimplifiedGregorianCalendar.getInstance();
	private static final String VERSION = "v1"; 


	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source)
	 */
	@Override
	public SourceVersionSet getSourceVersionSet(Source s) throws RepositoryException {
		SourceVersionSet set = new SourceVersionSet();
		SourceVersion v = new SourceVersion.Builder(s, s.getID())
			.version(VERSION)
			.creationDate(SimplifiedGregorianCalendar.getInstance())
			.graphType(GraphTypes.EDGELESS)
			.build();
		set.addSourceVersion(v);
		
		return set;
	}

	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source, String)
	 */
	@Override
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws RepositoryException {
		
		if (version.equals(VERSION))
			return this.getSourceVersionSet(s);
		else return new SourceVersionSet();
	}

	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	@Override
	public SourceVersionSet getSourceVersionSet(Source s,
			SimplifiedGregorianCalendar minDate,
			SimplifiedGregorianCalendar maxDate) throws RepositoryException {
		
		SimplifiedGregorianCalendar cal = SimplifiedGregorianCalendar.getInstance();
		
		if (minDate.before(cal) && maxDate.after(cal))
			return this.getSourceVersionSet(s);
		else return new SourceVersionSet();
	}

	/**
	 * @see SourceVersionAPI#insertSourceVersion(Source, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	@Override
	public int insertSourceVersion(Source s, String version,
			SimplifiedGregorianCalendar creationDate, GraphTypes type)
			throws RepositoryException {
		throw new RuntimeException("No implementation available");
	}

	/**
	 * @see SourceVersionAPI#insertSourceVersion(String, String, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	@Override
	public int insertSourceVersion(String objectType,
			String physicalSourceName, String version,
			SimplifiedGregorianCalendar creationDate, GraphTypes type)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionAPI#updateSourceVersion(SourceVersion)
	 */
	@Override
	public int updateSourceVersion(SourceVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionAPI#deleteSourceVersion(SourceVersion)
	 */
	@Override
	public int deleteSourceVersion(SourceVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	
	/**
	 * The method returns the FEVER specific statement to load a set of source versions.
	 * @return FEVER specific statement for loading source versions
	 */
	@Override
	protected String getLoadSourceVersionStatement() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return 
			// lds_version_id, lds_id, obj_type_name, pds_name, is_ontology, url, version
		"select id,id,'N/A',name,0,'N/A','"+VERSION+"',"+
			// lds_date
			"date('"+df.format(CURRENT_DATE.getTime())+"'),"+
			// structure_type
			GraphTypes.EDGELESS.getID()+" "+
		"from dataset ";
	}
}
