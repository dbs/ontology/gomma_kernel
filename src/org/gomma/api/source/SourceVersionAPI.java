package org.gomma.api.source;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The interface defines methods to manage source versions within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceVersionAPI {

	/**
	 * The method returns specified versions which reference to the keyword within the given versions 
	 * @return specified logical source versions in the GOMMA repository
	 * @see org.gomma.model.source.SourceVersion
	 * @see org.gomma.model.source.SourceVersionSet
	 * @throws RepositoryException
	 */
	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet,String keyword) throws RepositoryException;
	
	/**
	 * The method returns all versions of logical sources available in the GOMMA repository
	 * @return all logical source versions available in the GOMMA repository
	 * @see org.gomma.model.source.SourceVersion
	 * @see org.gomma.model.source.SourceVersionSet
	 * @throws RepositoryException
	 */
	public SourceVersionSet getSourceVersionSet() throws RepositoryException;
	
	/**
	 * The method returns relevant versions of logical sources available in the GOMMA repository 
	 * meeting the specified conditions. Conditions can be specified in a very structured form or 
	 * unstructured by simply using keywords. In the first case, the keywordQuery contains a set 
	 * of conditions which are connected by logical operators (AND, OR, NOT). Each condition is an
	 * expression consisting of an attribute name, a comparison operator (e.g., =,>) and a value to
	 * compare with. In case of specified keywords, preselected attributes will be queried using 
	 * the specified keywords. 
	 * @param s source for which the versions should be retrieved
	 * @return set of relevant source versions
	 * @throws RepositoryException
	 */
	public SourceVersionSet getSourceVersionSet(Source s) throws RepositoryException;

	/**
	 * The method returns the set of source versions for the given source object 
	 * and the specified version number.
	 * @param s source
	 * @param version version number
	 * @return set of source versions
	 * @throws RepositoryException
	 */
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws RepositoryException;
	
	/**
	 * The method returns the set of source versions for the given source object and
	 * those which creation date is between the specified minimum and maximum date.
	 * @param s source
	 * @param minDate lowest date
	 * @param maxDate latest date
	 * @return set of source versions
	 * @throws RepositoryException
	 */
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws RepositoryException;
	
	/**
	 * The method creates and stores a source version within the repository and
	 * returns the number of affected rows.
	 * @param s source for which the version should be created
	 * @param version version number
	 * @param creationDate creation date
	 * @param type structural graph type
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int insertSourceVersion(Source s, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws RepositoryException;
	
	/**
	 * The method creates and stores a source version within the repository and
	 * returns the number of affected rows.
	 * @param objectType semantic object type
	 * @param physicalSourceName physical source name
	 * @param version version number of the source
	 * @param creationDate creation date
	 * @param type structural graph type
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int insertSourceVersion(String objectType, String physicalSourceName, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws RepositoryException;
	
	/**
	 * The method updates the specified source version within the repository 
	 * and returns the number of affected rows. 
	 * @param v source version
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int updateSourceVersion(SourceVersion v) throws RepositoryException;
	
	/**
	 * The method deletes the specified source version from the repository
	 * and returns the number of affected rows.
	 * @param v source version
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteSourceVersion(SourceVersion v) throws RepositoryException;
}
