package org.gomma.api.source;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.DataTypes;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueFactory;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

/**
 * The class implements the object API. It is the default 
 * implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class ObjectAPI_RDBMS implements ObjectAPI {
	
	/**
	 * Constructor of the class.
	 */
	public ObjectAPI_RDBMS() {}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion)
	 */
	public ObjSet getObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v,""));
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v,""), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, IDObjectSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v," and obj_id in ("+set.getSeparatedIDString(",")+")"));
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, IDObjectSet, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v," and obj_id in ("+set.getSeparatedIDString(",")+")"), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, QueryMatchOperations)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v, this.rewriteAccessionPatternQuery(" and accession ",accNumber, op)));
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, QueryMatchOperations, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v, this.rewriteAccessionPatternQuery(" and accession ",accNumber, op)), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, List<String>)
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v, this.rewriteAccessionListQuery(accNumbers)));
	}
	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadObjectSet(v, this.rewriteAccessionListQuery(accNumbers)), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSetWithKeyword(SourceVersion, String)
	 */
	public  ObjSet getObjectSetWithKeyword(SourceVersion sv, String keyword) throws RepositoryException, WrongSourceException {
		if(keyword==null||keyword.length()==0) throw new NullPointerException("keyword could not be null.");
		else {
			return this.loadObjectAttributeSet(sv, this.loadObjectSet(sv, " and (accession like '%"+keyword+"%' " +
					"or exists (select * from gomma_obj_att_values c " +
					"where a.obj_id=c.obj_id_fk and b.lds_date between c.date_from " +
					"and c.date_to and c.att_value_s like '%"+keyword+"%')) "));
		}
	}
	
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) throws RepositoryException, WrongSourceException {
		if (queryCondition==null||queryCondition.length()==0) 
			return this.loadObjectAttributeSet(v, this.loadObjectSet(v, ""));
		else return this.loadObjectAttributeSet(v, this.loadObjectSet(v, " and "+queryCondition));
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, AttributeSet)
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		if (queryCondition==null||queryCondition.length()==0) 
			return this.loadObjectAttributeSet(v, this.loadObjectSet(v, ""), attSet);
		else return this.loadObjectAttributeSet(v, this.loadObjectSet(v, " and "+queryCondition),attSet);
	}
	
	
	/**
	 * @see ObjectAPI#getRootObjectSet(SourceVersion)
	 */
	public ObjSet getRootObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadRootObjectSet(v));
	}
	
	/**
	 * @see ObjectAPI#getRootObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getRootObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadRootObjectSet(v), attSet);
	}
	
	/**
	 * @see ObjectAPI#getLeafObjectSet(SourceVersion)
	 */
	public ObjSet getLeafObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadLeafObjectSet(v));
	}
	
	/**
	 * @see ObjectAPI#getLeafObjectSet(SourceVersion, AttributeSet)
	 */
	public ObjSet getLeafObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(v, this.loadLeafObjectSet(v), attSet); 
	}
	
	/**
	 * The method returns the default SQL statement to load an object set.
	 * @return SQL statement for retrieving a set of object versions 
	 */
	protected String getLoadObjectVersionStatement() { 
		return 
		"select obj_id, accession, date_from, date_to "+
		"from gomma_obj_versions a, gomma_lds_versions b "+
		"where a.lds_id_fk=b.lds_id_fk and lds_version_id=[ldsVersionID] "+
		"and lds_date between date_from and date_to";
	}
	

	
	protected String getLoadObjectAttributeStatement(AttributeSet set) {
		return this.getLoadObjectAttributeStatement()+
				" and b.att_id in ("+set.getSeparatedIDString(",")+")";
	}
	
	/**
	 * The method returns the default SQL statement to load object attribute set.
	 * @return SQL statement for loading object attribute and attribute values 
	 */
	protected String getLoadObjectAttributeStatement() { 
		return
		"select obj_id, att_id, att_name, att_scope, data_type_id_fk, "+
		"att_value_id, a.date_from, a.date_to, "+
		"att_value_f, att_value_i, att_value_s "+
		"from gomma_obj_versions a, gomma_attributes b, gomma_obj_att_values c, gomma_lds_versions d "+
		"where a.obj_id=c.obj_id_fk and a.lds_id_fk=d.lds_id_fk and b.att_id=c.att_id_fk "+
		"and d.lds_version_id=[ldsVersionId] "+
		"and a.obj_id in ([objIdList]) "+
		"and d.lds_date between c.date_from and c.date_to";
	}
	
	
	
	
	
	private ObjSet loadRootObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectSet(v,
				" and not exists (select * from gomma_lds_structure f "+
				"where a.obj_id=f.c_obj_id_fk and b.lds_id_fk=f.lds_id_fk "+
				"and b.lds_date between f.date_from and f.date_to)");
	}
	
	private ObjSet loadLeafObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectSet(v,
				" and not exists (select * from gomma_lds_structure f "+
				"where a.obj_id=f.p_obj_id_fk and b.lds_id_fk=f.lds_id_fk "+
				"and b.lds_date between f.date_from and f.date_to)");
	}
	
	protected ObjSet loadObjectSet(SourceVersion v, String queryCond) throws RepositoryException, WrongSourceException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = getLoadObjectVersionStatement().replaceAll("\\[ldsVersionID\\]",Integer.toString(v.getID()))+queryCond;
		ResultSet rs = dbh.executeSelect(query);
		ObjSet set = this.extractObjData(rs,v,new ObjSet());
		dbh.closeStatement(rs);
		
		CacheManager.getInstance().getDateCache().clear();
		return set;
	}
	
	protected ObjSet loadObjectAttributeSet(SourceVersion v, ObjSet objSet, AttributeSet attSet) throws RepositoryException, WrongSourceException {
		 return this.loadObjectAttributeSet(objSet, this.getLoadObjectAttributeStatement(attSet)
					.replaceAll("\\[ldsVersionId\\]",Integer.toString(v.getID()))
					.replaceAll("\\[objIdList\\]",objSet.getSeparatedIDString(",")));
	}
	
	protected ObjSet loadObjectAttributeSet(SourceVersion v, ObjSet objSet) throws RepositoryException, WrongSourceException {
		return this.loadObjectAttributeSet(objSet, this.getLoadObjectAttributeStatement()
				.replaceAll("\\[ldsVersionId\\]",Integer.toString(v.getID()))
				.replaceAll("\\[objIdList\\]",objSet.getSeparatedIDString(",")));
	}
	
	private ObjSet loadObjectAttributeSet(ObjSet objSet, String sqlQuery) throws RepositoryException, WrongSourceException {
		//System.out.println(sqlQuery);
		if(objSet.size()==0) return objSet;
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(sqlQuery);
		ObjSet set = this.extractObjAttributes(rs,objSet);
		dbh.closeStatement(rs);
		
		CacheManager.getInstance().getAttributeCache().clear();
		
		return set;
	}
	
	
/*	protected Map<Source,ObjSet> loadObjectSet(SourceVersionSet svSet, String keyword) throws RepositoryException, WrongSourceException {
		String idString="";
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = getLoadObjectVersionStatementWithoutLdsID().replaceAll("\\[cond\\]",keyword);
		ResultSet rs = dbh.executeSelect(query);
		Map<Source,ObjSet> m = this.extractObjData(rs,svSet);
		dbh.closeStatement(rs);
		for(ObjSet os:m.values()){
			idString+=os.getSeparatedIDString(",")+",";
		}
		idString=idString.substring(0, idString.length()-1);
		
		
		query = getLoadObjectAttributeStatementWithoutLdsID()
					.replaceAll("\\[objIdList\\]",idString);
		rs = dbh.executeSelect(query);
		m = this.extractObjAttributes(rs,m);
		dbh.closeStatement(rs);
		
		CacheManager.getInstance().getDateCache().clear();
		
		return m;
	}
*/
/*	protected Map<Source,ObjSet> loadObjectSet(SourceVersionSet svSet, String keyword) throws RepositoryException, WrongSourceException {
		String idString="";
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = getLoadObjectVersionStatementWithoutLdsID().replaceAll("\\[cond\\]",keyword);
		ResultSet rs = dbh.executeSelect(query);
		Map<Source,ObjSet> m = this.extractObjData(rs,svSet);
		dbh.closeStatement(rs);
		for(ObjSet os:m.values()){
			idString+=os.getSeparatedIDString(",")+",";
		}
		idString=idString.substring(0, idString.length()-1);
		
		
		query = getLoadObjectAttributeStatementWithoutLdsID()
					.replaceAll("\\[objIdList\\]",idString);
		rs = dbh.executeSelect(query);
		m = this.extractObjAttributes(rs,m);
		dbh.closeStatement(rs);
		
		CacheManager.getInstance().getDateCache().clear();
		
		return m;
	}
*/	
	/**
	 * The method extracts objects from the result set.
	 * @param rs result set containing objects
	 * @param set set objects to be extended by the objects in the result set
	 * @return set of objects
	 * @throws SQLException
	 * @throws WrongSourceException
	 */
	private ObjSet extractObjData(ResultSet rs, SourceVersion v, ObjSet set) throws RepositoryException, WrongSourceException {
		Obj o;
	
		try {
			while (rs.next()) {
				if (set.contains(rs.getInt(1))) continue;
				
				o = new Obj.Builder(rs.getInt(1),v.getSource().getID(),rs.getString(2))
							.sourceVersion(v)
							.fromDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(3).getTime()))
							.toDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(4).getTime()))
							.build();
				
				set.addObj(o);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}

	
	protected String rewriteAccessionPatternQuery(String prevQueryCond, String accPattern, QueryMatchOperations op) {
		accPattern = accPattern.replaceAll("'","");
		switch (op) {
			case EXACT_MATCH: prevQueryCond += "= '"+accPattern+"'"; break;
			case STARTS_WITH: prevQueryCond += " like '"+accPattern+"%'"; break;
			case ENDS_WITH:   prevQueryCond += " like '%"+accPattern+"'"; break;
			case CONTAINS:    prevQueryCond += " like  '%"+accPattern+"%'"; break;
		}
		
		return prevQueryCond;
	}
	
/*	private Map<Source ,ObjSet> extractObjData(ResultSet rs, SourceVersionSet svSet) throws RepositoryException, WrongSourceException {
		Map<Source,ObjSet> m=new HashMap<Source,ObjSet>();
		List<Integer> id=new ArrayList<Integer>();
		Obj o;
	    SourceVersion v=null;
	    boolean svChanged=false;
		try {
			while (rs.next()) {
				
				for(SourceVersion sv:svSet.getCollection()){
					if(sv.getID()==rs.getInt(1)){
						if(!id.contains(sv.getID())){
							id.add(sv.getID());
							svChanged=true;
							v=sv;
						}else{
							svChanged=false;
						}
						break;
					}
				}
				
				if(svChanged){
					if(!m.containsKey(v.getSource())){
						ObjSet os=new ObjSet();
						o = new Obj.Builder(rs.getInt(2),v.getSource().getID(),rs.getString(3))
						.sourceVersion(v)
						.fromDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(4).getTime()))
						.toDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(5).getTime()))
						.build();
						
						m.put(v.getSource(), os);
					}else{
						if(m.get(v.getSource()).contains(rs.getInt(2)))
							m.get(v.getSource()).getElement(rs.getInt(2)).addSourceVersion(v);
						else{
							o = new Obj.Builder(rs.getInt(2),v.getSource().getID(),rs.getString(3))
							.sourceVersion(v)
							.fromDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(4).getTime()))
							.toDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(5).getTime()))
							.build();
							
							m.get(v.getSource()).addObj(o);
						}
					}
				}else{
					if(!m.get(v.getSource()).contains(rs.getInt(2))){
						o = new Obj.Builder(rs.getInt(2),v.getSource().getID(),rs.getString(3))
						.sourceVersion(v)
						.fromDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(4).getTime()))
						.toDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(5).getTime()))
						.build();
						
						m.get(v.getSource()).addObj(o);
					}						
				}		
			}
			return m;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
*/		
	/**
	 * The method extends a set of objects with their corresponding attributes.
	 * @param rs result set containing the attribute data 
	 * @param objs set of objects for which attribute data is loaded
	 * @return set of objects
	 * @throws SQLException
	 */
	private ObjSet extractObjAttributes(ResultSet rs, ObjSet set) throws RepositoryException {
		Obj obj;
		Attribute att = null;
		AttributeValueVersion avv = null;
		
		try {
			while (rs.next()) {
				if (!set.contains(rs.getInt(1))) continue;
			
				obj = set.getObj(rs.getInt(1));
				att = CacheManager.getInstance().getAttributeCache().getAttribute(
						rs.getInt(2),rs.getString(3),rs.getString(4), DataTypes.resolveDataType(rs.getInt(5)));
				
				switch (att.getDataType()) {
					case FLOAT: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getFloat(9));
						break;
					case INTEGER: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getInt(10));
						break;
					case STRING: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getString(11));
						break;
					default: throw new RepositoryException("Could not create an attribute value version for data type '"+
							att.getDataType().toString()+"'.");
				}
			
				obj.addAttributeValue(avv);
				set.addObj(obj);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	/**
	 * The method extends a set of objects with their corresponding attributes.
	 * @param rs result set containing the attribute data 
	 * @param objs set of objects for which attribute data is loaded
	 * @return set of objects
	 * @throws SQLException
	 */
/*	private Map<Source,ObjSet> extractObjAttributes(ResultSet rs, Map<Source,ObjSet> m) throws RepositoryException {
		Obj obj=null;
		ObjSet currentOS=null;
		Attribute att = null;
		AttributeValueVersion avv = null;
		
		try {
			while (rs.next()) {
				for(ObjSet os:m.values()){
					if(os.contains(rs.getInt(1))){
						obj=os.getElement(rs.getInt(1));
						currentOS=os;
						break;
					}
				}
			
				att = CacheManager.getInstance().getAttributeCache().getAttribute(
						rs.getInt(2),rs.getString(3),rs.getString(4), DataTypes.resolveDataType(rs.getInt(5)));
				
				switch (att.getDataType()) {
					case FLOAT: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getFloat(9));
						break;
					case INTEGER: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getInt(10));
						break;
					case STRING: 
						avv = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()), 
								CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()),
								rs.getString(11));
						break;
					default: throw new RepositoryException("Could not create an attribute value version for data type '"+
							att.getDataType().toString()+"'.");
				}
			
				obj.addAttributeValue(avv);
				for(ObjSet os :m.values()){
					if(os.equals(currentOS))
						os.addObj(obj);
				}
			}
			return m;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
*/	

	
	private String rewriteAccessionListQuery(List<String> accNumbers) {
		StringBuffer queryCond = new StringBuffer(" and accession in (");
		
		for(String acc: accNumbers){
			queryCond.append("'"+acc+"',");
		}
		queryCond.delete(queryCond.length()-1, queryCond.length());
		queryCond.append(" ) ");
		
		return queryCond.toString();
	}
}
