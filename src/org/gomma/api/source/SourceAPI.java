package org.gomma.api.source;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;

/**
 * The interface defines methods to manage sources within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceAPI {

	/**
	 * The method returns the complete set of sources available in the repository.
	 * @return set of sources
	 * @throws RepositoryException
	 */
	public SourceSet getSourceSet() throws RepositoryException;
	
	/**
	 * The method returns a set of sources for the specified semantic object type.
	 * @param objType semantic object type of the source objects
	 * @return set of sources
	 * @throws RepositoryException
	 */
	public SourceSet getSourceSet(String objType) throws RepositoryException;
	
	/**
	 * The method returns a set of source for the specified semantic source type 
	 * and the physical source name.
	 * @param objType semantic source type
	 * @param physicalSource physical source name
	 * @return set of sources
	 * @throws RepositoryException
	 */
	public SourceSet getSourceSet(String objType, String physicalSource) throws RepositoryException;
	
	/**
	 * The method creates and stores a source into the repository and returns the 
	 * number of affected rows.
	 * @param objectType semantic source type
	 * @param physicalSource physical source name
	 * @param sourceURL source URL
	 * @param isOntology true if the source is an ontology; false otherwise
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int insertSource(String objectType, String physicalSource, String sourceURL, boolean isOntology) throws RepositoryException;
	
	/**
	 * The method updates the given source object within the repository and returns the 
	 * number of affected rows.
	 * @param s source object to be updated
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int updateSource(Source s) throws RepositoryException;
	
	/**
	 * The method deletes the source object from the repository and returns the number 
	 * of affected rows. 
	 * @param s source object that should be deleted
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteSource(Source s) throws RepositoryException;
}
