package org.gomma.api.source;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.gomma.api.APIFactory;
import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.IDObject;
import org.gomma.model.IDObjectSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements the source structure API interface. It is the
 * default implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceVersionStructureAPI_RDBMS implements SourceVersionStructureAPI {

	/**
	 * The constant represents the MySQL statement for loading the source structure. 
	 */
	private static final String LOAD_SOURCE_VERSION_STRUCTURE = 
		"select a.p_obj_id_fk, a.c_obj_id_fk, name, a.is_directed, a.date_from, a.date_to "+
		"from gomma_lds_structure a, gomma_lds_versions b, gomma_rela_types c "+
		"where a.lds_id_fk=b.lds_id_fk and c.rela_type_id=a.rela_type_id_fk "+
		"and lds_date between date_from and date_to and lds_version_id=[ldsVersionId]";
	
	
	/**
	 * Constructor of the class.
	 */
	public SourceVersionStructureAPI_RDBMS() {}
		
	/**
	 * @see SourceVersionStructureAPI#loadSourceStructure(SourceVersion) 
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws RepositoryException, WrongSourceException, GraphInitializationException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_SOURCE_VERSION_STRUCTURE.replaceAll("\\[ldsVersionId\\]",Integer.toString(v.getID()));
		ResultSet rs = null;
		
		// initialize and load objects
		SourceVersionStructure structure = new SourceVersionStructure(v);
		structure.addObjects(APIFactory.getInstance().getObjectAPI().getObjectSet(v));

		// load object relationships
		rs = dbh.executeSelect(query);
		structure = this.extractSourceVersionStructure(rs,structure);
		dbh.closeStatement(rs);
			
		return structure;
	}
	
	/**
	 * @see SourceVersionStructureAPI#getSourceStructure(SourceVersion, ObjSet)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v, ObjSet inputObjs) throws RepositoryException, GraphInitializationException, WrongSourceException {
	
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_SOURCE_VERSION_STRUCTURE.replaceAll("\\[ldsVersionId\\]",Integer.toString(v.getID()));;
		ResultSet rs = null;
		
		
		SourceVersionStructure structure = new SourceVersionStructure(v);	
		if (inputObjs.size()==0) return structure;
			
		String sepObjIDString = inputObjs.getSeparatedIDString(",");

		for (Obj o : inputObjs) structure.addObject(o);
			
		// load child relationships
		rs = dbh.executeSelect(query+" and p_obj_id_fk in ("+sepObjIDString+")");
		structure = this.extractSourceVersionStructure(rs,structure);
		dbh.closeStatement(rs);
			
		// load parent relationships
		rs = dbh.executeSelect(query+" and c_obj_id_fk in ("+sepObjIDString+")");
		structure = this.extractSourceVersionStructure(rs,structure);
		dbh.closeStatement(rs);
			
		return structure;
		 
	}
	
	/**
	 * @see SourceVersionStructureAPI#getSourceStructure(SourceVersion, ObjSet, int, boolean)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v, ObjSet inputObjs, int nLevels, boolean getSuccessors) throws RepositoryException, WrongSourceException, GraphInitializationException {
	
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_SOURCE_VERSION_STRUCTURE.replaceAll("\\[ldsVersionId\\]",Integer.toString(v.getID()));
		ResultSet rs = null;
		
		SourceVersionStructure structure = new SourceVersionStructure(v);
		if (inputObjs.size()==0) return structure;
		
		structure.addObjects(inputObjs);
		ObjSet queryObjs = inputObjs;
		
		Set<ObjRelationship> set;		
		for (int i=0; i<nLevels; i++) {
			rs = dbh.executeSelect(query
						+ (getSuccessors?" and p_obj_id_fk in (":" and c_obj_id_fk in (")
						+ queryObjs.getSeparatedIDString(",") + ")");
			set = this.extractSourceVersionStructure(rs);
			dbh.closeStatement(rs);
					
			if (set.size()==0) break;
					
			IDObjectSet objSet = new IDObjectSet();
			IDObject o;
			for (ObjRelationship rel : set) {
				if (getSuccessors) o = new IDObject(rel.getToID());
				else o = new IDObject(rel.getFromID());
				objSet.addElement(o);
			}
			
			queryObjs = APIFactory.getInstance().getObjectAPI().getObjectSet(v,objSet);
			structure.addObjects(queryObjs);
			structure.addObjectRelationships(set);
		}
		return structure;
	}
	
	
	/**
	 * The method returns the source version structure, i.e., relationships 
	 * between objects of a specific source version
	 * @param rs result set containing the source structure
	 * @param structure source version structure
	 * @return completed source version structure
	 * @see org.gomma.model.source.SourceVersionStructure
	 * @throws SQLException
	 * @throws GraphInitializationException 
	 */
	private SourceVersionStructure extractSourceVersionStructure(ResultSet rs, SourceVersionStructure structure) throws RepositoryException, GraphInitializationException {
		
		try {
			while (rs.next()) {
				if (!structure.containsObject(rs.getInt(1))) continue;
				if (!structure.containsObject(rs.getInt(2))) continue;
			
				structure.addObjectRelationship(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getInt(4)==1,
						CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(5).getTime()),
						CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(6).getTime()));
			}
			return structure;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	/**
	 * The method extracts and returns the source structure from the given SQL query result set. 
	 * @param rs query result set
	 * @return source version structure
	 * @throws RepositoryException
	 */
	private Set<ObjRelationship> extractSourceVersionStructure(ResultSet rs) throws RepositoryException {
		Set<ObjRelationship> set = new HashSet<ObjRelationship>();
		ObjRelationship edge;
		
		try {
			while (rs.next()) {
				edge = new ObjRelationship.Builder(rs.getInt(1),rs.getInt(2))
							.isDirected(rs.getInt(4)==1)
							.fromDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(5).getTime()))
							.toDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(6).getTime()))
							.build();
				edge.setRelationshipType(rs.getString(3));
				set.add(edge);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}
