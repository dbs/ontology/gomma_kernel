package org.gomma.api.source;

import java.util.List;

import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportObj;
import org.gomma.io.importer.models.ImportSourceStructure;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the source version import API. It is the implementation 
 * for the FEVER system using a relational database at the back-end.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public final class SourceVersionImportAPI_FEVER implements SourceVersionImportAPI {

	
	

	/**
	 * @see SourceVersionImportAPI#insertIntoTmpTables(String[])
	 */
	public void insertIntoTmpTables(String[] dmlQueryPattern) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#insertIntoTmpTables(List, String)
	 */
	public void insertIntoTmpTables(List<String[]> objects, String statement) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#insertTemporaryObjectSet(List)
	 */
	public boolean insertTemporaryObjectSet(List<ImportObj> objList) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#insertTemporarySourceStructure(ImportSourceStructure)
	 */
	public boolean insertTemporarySourceStructure(ImportSourceStructure structure) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#integrateNewAttributes()
	 */
	public int integrateNewAttributes() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#integrateNewRelationshipTypes()
	 */
	public int integrateNewRelationshipTypes() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#importFirstVersion(int, SimplifiedGregorianCalendar)
	 */
	public void importFirstVersion(int sourceID, SimplifiedGregorianCalendar from) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see SourceVersionImportAPI#computeVersionDiffsForImport(int, SimplifiedGregorianCalendar)
	 */
	public void computeVersionDiffsForImport(int sourceID, SimplifiedGregorianCalendar lastTimestamp) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see SourceVersionImportAPI#integrateVersionDiffs(int, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public void integrateVersionDiffs(int sourceID,
			SimplifiedGregorianCalendar startTimestamp,
			SimplifiedGregorianCalendar endTimestamp)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceVersionImportAPI#cleanTmpTables()
	 */
	public void cleanTmpTables() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

}
