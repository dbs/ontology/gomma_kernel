package org.gomma.api.source;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.DataTypes;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the object API. It is the  
 * implementation for the FEVER system using a relational database
 * at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class ObjectAPI_FEVER extends ObjectAPI_RDBMS {

	private static final SimplifiedGregorianCalendar START_DATE =
		new SimplifiedGregorianCalendar(2000, Calendar.JANUARY, 1);
	
	private static final SimplifiedGregorianCalendar END_DATE = 
		new SimplifiedGregorianCalendar(2099,Calendar.DECEMBER,31);
	
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, IDObjectSet)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) 
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, super.loadObjectSet(
				v, " and obj_id in ("+set.getSeparatedIDString(",")+")"));
	}

	@Override
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet)
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, super.loadObjectSet(
				v, " and obj_id in ("+set.getSeparatedIDString(",")+")"), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, QueryMatchOperations)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) 
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, this.loadObjectSet(v, 
				super.rewriteAccessionPatternQuery(" and obj_attributeidvalue ",accNumber, op)));
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, QueryMatchOperations, AttributeSet)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet attSet) 
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, this.loadObjectSet(v, 
				super.rewriteAccessionPatternQuery(" and obj_attributeidvalue ",accNumber, op)), attSet);
	}
	

	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, List)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) 
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, super.loadObjectSet(v, 
				this.rewriteAccessionListQuery(accNumbers)));
	}

	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, List, AttributeSet)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) 
		throws RepositoryException, WrongSourceException {
		return super.loadObjectAttributeSet(v, super.loadObjectSet(v, 
				this.rewriteAccessionListQuery(accNumbers)), attSet);
	}
	
	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) 
		throws RepositoryException, WrongSourceException {
		if (queryCondition==null||queryCondition.length()==0) 
			return this.getObjectSet(v);
		 else return super.loadObjectAttributeSet(v, super.loadObjectSet(v, " and "+queryCondition));
	}

	/**
	 * @see ObjectAPI#getObjectSet(SourceVersion, String, AttributeSet)
	 */
	@Override
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) 
		throws RepositoryException, WrongSourceException {
		if (queryCondition==null||queryCondition.length()==0) 
			return this.getObjectSet(v, attSet);
		 else return super.loadObjectAttributeSet(v, super.loadObjectSet(v, " and "+queryCondition), attSet);
	}
	
	
	/**
	 * @see ObjectAPI#getRootObjectSet(SourceVersion)
	 */
	@Override
	public ObjSet getRootObjectSet(SourceVersion v) throws RepositoryException,	WrongSourceException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjectAPI#getRootObjectSet(SourceVersion, AttributeSet)
	 */
	@Override
	public ObjSet getRootObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException,	WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see ObjectAPI#getLeafObjectSet(SourceVersion)
	 */
	@Override
	public ObjSet getLeafObjectSet(SourceVersion v) throws RepositoryException,	WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see ObjectAPI#getLeafObjectSet(SourceVersion, AttributeSet)
	 */
	@Override
	public ObjSet getLeafObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException,	WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
	
	
	
	/**
	 * The method returns the FEVER specific SQL statement to load an object set.
	 * @return FEVER specific SQL statement for retrieving a set of object versions 
	 */
	@Override
	protected String getLoadObjectVersionStatement() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return 
		"select distinct obj_id, obj_attributeidvalue, "+
			"date('"+df.format(START_DATE.getTime())+"'),"+
			"date('"+df.format(END_DATE.getTime())+"') "+
		"from object, attribute "+
		"where obj_id=attr_objectid_fk and obj_ldsid_fk=[ldsVersionID] ";
	}
	
	/**
	 * The method returns the FEVER-specific SQL statement to load attribute values 
	 * describing the objects. The value set is limited by the given attribute set.
	 * @return FEVER-specific SQL statement for loading object attribute and attribute values
	 */
	@Override
	protected String getLoadObjectAttributeStatement(AttributeSet set) {
		return this.getLoadObjectAttributeStatement()+
				"and b.att_name_id in ("+set.getSeparatedIDString(",")+")";
	}
	
	/**
	 * The method returns the FEVER-specific SQL statement to load attribute value set describing the objects.
	 * @return FEVER-specific SQL statement for loading object attribute and attribute values 
	 */
	@Override
	protected String getLoadObjectAttributeStatement() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return
		"select attr_objectid_fk,att_name_id,attr_name,'N/A',"+DataTypes.STRING.getID()+",attr_id,"+
			"date('"+df.format(START_DATE.getTime())+"'),"+
			"date('"+df.format(END_DATE.getTime())+"'),"+
			"0,0,attr_value "+
		"from object a, attribute b "+
		"where a.obj_id=b.attr_objectid_fk "+
		  "and a.obj_ldsid_fk=[ldsVersionId] "+
		  "and a.obj_id in ([objIdList])";
	}
	
	private String rewriteAccessionListQuery(List<String> accNumbers) {
		StringBuffer queryCond = new StringBuffer(" and obj_attributeidvalue in (");
		
		for(String acc: accNumbers){
			queryCond.append("'"+acc+"',");
		}
		queryCond.delete(queryCond.length()-1, queryCond.length());
		queryCond.append(" ) ");
		
		return queryCond.toString();
	}
}
