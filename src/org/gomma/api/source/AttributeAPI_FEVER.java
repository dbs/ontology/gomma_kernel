package org.gomma.api.source;

import org.gomma.model.DataTypes;

/**
 * This class represents the FEVER-specific implementation of the 
 * attribute API interface for querying attribute sets.  
 * @author Toralf Kirsten (toralf@kirsten-home.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class AttributeAPI_FEVER extends AttributeAPI_RDBMS {

	
	/**
	 * @see AttributeAPI_RDBMS#getLoadAttributeStatement()
	 */
	@Override
	protected String getLoadAttributeStatement() {
		return "select att_name_id,name, 'N/A',"+DataTypes.STRING.getID()+" from attribute_name ";
	}
}
