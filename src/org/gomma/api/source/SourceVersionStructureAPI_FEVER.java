package org.gomma.api.source;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.GraphInitializationException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;

/**
 * The class implements the source structure API interface. It is the
 * implementation for the FEVER system using a relational database at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceVersionStructureAPI_FEVER implements SourceVersionStructureAPI {

	/**
	 * @see SourceVersionStructureAPI#getSourceStructure(SourceVersion)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v) throws RepositoryException, WrongSourceException, GraphInitializationException {
		return this.getSourceStructure(v, APIFactory.getInstance().getObjectAPI().getObjectSet(v));
	}

	/**
	 * @see SourceVersionStructureAPI#getSourceStructure(SourceVersion, ObjSet)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v, ObjSet inputObjs) throws RepositoryException, GraphInitializationException, WrongSourceException {
		SourceVersionStructure result = new SourceVersionStructure(v);
		
		for (Obj o : inputObjs)
			result.addObject(o);
		
		return result;
	}

	/**
	 * @see SourceVersionStructureAPI#getSourceStructure(SourceVersion, ObjSet, int, boolean)
	 */
	public SourceVersionStructure getSourceStructure(SourceVersion v, ObjSet inputObjs, int nLevels, boolean getSuccessors) throws RepositoryException, WrongSourceException, GraphInitializationException {
		return this.getSourceStructure(v, inputObjs);
	}

}
