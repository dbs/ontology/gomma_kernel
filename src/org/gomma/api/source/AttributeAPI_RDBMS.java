package org.gomma.api.source;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.DataTypes;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;

/**
 * This class is the default implementation of the attribute API interface 
 * for relational database systems using the default schema.  
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class AttributeAPI_RDBMS implements AttributeAPI {

	
	/**
	 * @see AttributeAPI#getAttributeSet()
	 */
	public AttributeSet getAttributeSet() throws RepositoryException {
		return this.getAttributeSet("");
	}
	
	/**
	 * The method returns the SQL statement to load the complete set of attributes.
	 * @return complete set of attributes
	 */
	protected String getLoadAttributeStatement() {
		return "select att_id, att_name, att_scope, data_type_id_fk from gomma_attributes ";
	}
	
	/**
	 * The method loads and returns the attribute set for the specified query condition.
	 * @param queryCond query condition
	 * @return set of attributes
	 * @throws RepositoryException
	 */
	private AttributeSet getAttributeSet(String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = getLoadAttributeStatement() + (queryCond.length()!=0?queryCond:"");
		ResultSet rs = dbh.executeSelect(query);
		AttributeSet set = this.extractAttributeSet(rs);
		
		return set;
	}
	
	
	/**
	 * The method extracts and returns the set of attributes from the given result set. 
	 * @param rs SQL result set
	 * @return set of attributes
	 * @throws RepositoryException
	 */
	private AttributeSet extractAttributeSet(ResultSet rs) throws RepositoryException {
		AttributeSet set = new AttributeSet();
		Attribute att;
		
		try {
			while (rs.next()) {
				att = new Attribute.Builder(rs.getInt(1))
							.name(rs.getString(2))
							.scope(rs.getString(3))
							.dataType(DataTypes.resolveDataType(rs.getInt(4)))
							.build();
				
				set.addAttribute(att);
			}	
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		return set;
	}
}
