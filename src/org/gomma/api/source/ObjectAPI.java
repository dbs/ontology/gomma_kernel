package org.gomma.api.source;

import java.util.List;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.IDObjectSet;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;

/**
 * The interface defines methods to manage objects within the repository. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface ObjectAPI {

	/**
	 * The method returns the complete set of objects for the given source version object.
	 * Each object is described by the complete set of attribute values.
	 * @param v source version for which the objects should be loaded
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns the complete set of objects for the given source version object.
	 * Each object is at most described by the specified set of attribute values.
	 * @param v source version for which the objects should be loaded
	 * @param attSet set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting 
	 * the unique identifiers of the specified set. Each object is described by the 
	 * complete set of attribute values.
	 * @param v source version
	 * @param set set of unique object identifiers
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting 
	 * the unique identifiers of the specified set. Each object is at most described 
	 * by the specified set of attribute values.
	 * @param v source version
	 * @param set set of unique object identifiers
	 * @param attSet set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, IDObjectSet set, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting
	 * the accession number pattern and the specified query match method
	 * @param v source version
	 * @param accNumber accession number pattern
	 * @param op query operation
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting the
	 * accession number pattern and the specified query match method. Each object is at 
	 * most described by the set of specified attributes. 
	 * @param v source version
	 * @param accNumber accession number pattern
	 * @param op query match operation
	 * @param set set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, String accNumber, QueryMatchOperations op, AttributeSet set) throws RepositoryException, WrongSourceException;
	
	
	/**
	 * The method returns a set of objects for the given source version and meeting
	 * a list of accession numbers. Each object is described by the complete set of 
	 * attribute values.
	 * @param v source version
	 * @param accNumbers List of accessions
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */	
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting
	 * a list of accession numbers. Each object is at most described by the set of 
	 * specified attributes. 
	 * @param v source version
	 * @param accNumbers List of accessions
	 * @param attSet set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, List<String> accNumbers, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting
	 * the query condition.
	 * @param v source version
	 * @param queryCondition Query condition
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */	
	public ObjSet getObjectSet(SourceVersion v, String queryCondition) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects for the given source version and meeting
	 * the query condition. Each object is at most described by the set of 
	 * specified attributes. 
	 * @param v source version
	 * @param queryCondition
	 * @param attSet
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getObjectSet(SourceVersion v, String queryCondition, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	
	/**
	 * The method returns a set of root objects for the specified source version. 
	 * Root objects in the sense of this method are those which have no predecessor.
	 * Each object is described by the complete set of attribute values.
	 * @param v source version
	 * @return set of root objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getRootObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of root objects for the specified source version. 
	 * Root objects in the sense of this method are those which have no predecessor.
	 * Each object is at most described by the set of specified attributes. 
	 * @param v source version
	 * @param attSet set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getRootObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of leaf objects for the specified source version.
	 * Leaf objects in the sense of this method are those which have no successor. 
	 * Each object is described by the complete set of attribute values.
	 * @param v source version
	 * @return set of leaf objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getLeafObjectSet(SourceVersion v) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of leaf objects for the specified source version.
	 * Leaf objects in the sense of this method are those which have no successor.
	 * Each object is at most described by the set of specified attributes. 
	 * @param v source version
	 * @param attSet set of attributes
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjSet getLeafObjectSet(SourceVersion v, AttributeSet attSet) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of objects with the keyword for the given source version 
	 * @param v source version
	 * @param keyword  keyword to search in Object
	 * @return set of objects
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */	
	public ObjSet getObjectSetWithKeyword(SourceVersion v, String keyword) throws RepositoryException, WrongSourceException;
	

}
