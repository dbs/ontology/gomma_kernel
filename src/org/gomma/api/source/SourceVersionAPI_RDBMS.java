package org.gomma.api.source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;
import org.gomma.util.graph.GraphTypes;

/**
 * The class implements the source version API. It is the default implementation
 * for a relational database. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceVersionAPI_RDBMS implements SourceVersionAPI {

	
	
	/**
	 * Constructor of the class.
	 */
	public SourceVersionAPI_RDBMS() {}
	
	/**
	 * @see SourceVersionAPI#getSearchSourceVersionSet(SourceVersionSet, String)
	 */
	public SourceVersionSet getSearchSourceVersionSet(SourceVersionSet svSet,String key) throws RepositoryException{
		if(key==null ||key.length()==0)throw new NullPointerException("keyword should not be null");
		return this.loadSearchSourceVersionSet(svSet, key);
	}
	
	/**
	 * @see SourceVersionAPI#getSourceVersionSet()
	 */
	public SourceVersionSet getSourceVersionSet() throws RepositoryException {
		return this.loadSourceVersionSet("");
	}
	
	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source)
	 */
	public SourceVersionSet getSourceVersionSet(Source s) throws RepositoryException {
		return this.loadSourceVersionSet(" and lds_id_fk="+Integer.toString(s.getID()));
	}

	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source, String)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, String version) throws RepositoryException {
		return this.loadSourceVersionSet(" and lds_id_fk="+Integer.toString(s.getID())+
				" and version='"+version+"'");
	}
	
	/**
	 * @see SourceVersionAPI#getSourceVersionSet(Source, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public SourceVersionSet getSourceVersionSet(Source s, SimplifiedGregorianCalendar minDate, SimplifiedGregorianCalendar maxDate) throws RepositoryException {
		DateFormat df = new SimpleDateFormat("YYYY-MM-DD");
		return this.loadSourceVersionSet("and lds_id_fk="+Integer.toString(s.getID())+
				"and lds_date between '"+df.format(minDate.getTime())+"' and '"+df.format(maxDate.getTime())+"'");
	}
	
	private SourceVersionSet loadSourceVersionSet(String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getLoadSourceVersionStatement()+queryCond;
		ResultSet rs = dbh.executeSelect(query);
		
		SourceVersionSet set = this.extractSourceVersionSet(rs, new SourceVersionSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	protected SourceVersionSet loadSearchSourceVersionSet(SourceVersionSet svSet, String keyword) throws RepositoryException{
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = getSearchSourceVersionStatement().replaceAll("\\[cond\\]",keyword);
		ResultSet rs = dbh.executeSelect(query);
		SourceVersionSet sv = this.extractSearchSourceVersionSet(rs,svSet);
		
		return sv;
	}
	
	/**
	 * @see SourceVersionAPI#insertSourceVersion(Source, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	public int insertSourceVersion(Source s, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getInsertSourceVersionBySourceStatement();
		PreparedStatement pStmt = null;
		int affectedRows=0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setInt(1,s.getID());
			pStmt.setString(2,version);
			pStmt.setDate(3,new java.sql.Date(creationDate.getTimeInMillis()),creationDate.toCalendar());
			pStmt.setInt(4,type.getID());
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	/**
	 * @see SourceVersionAPI#insertSourceVersion(String, String, String, SimplifiedGregorianCalendar, GraphTypes)
	 */
	public int insertSourceVersion(String objectType, String physicalSourceName, String version, SimplifiedGregorianCalendar creationDate, GraphTypes type) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getInsertSourceVersionStatement();
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,version);
			pStmt.setDate(2,new java.sql.Date(creationDate.getTimeInMillis()),creationDate.toCalendar());
			pStmt.setInt(3,type.getID());
			pStmt.setString(4,physicalSourceName);
			pStmt.setString(5,objectType);
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	
	/**
	 * @see SourceVersionAPI#updateSourceVersion(SourceVersion)
	 */
	public int updateSourceVersion(SourceVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getUpdateSourceVersionStatement();
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,v.getVersion());
			pStmt.setDate(2, new java.sql.Date(v.getCreationVersionDate().getTimeInMillis()),v.getCreationVersionDate().toCalendar());
			pStmt.setInt(3, v.getStructuralType().getID());
			pStmt.setInt(4, v.getID());
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	
	/**
	 * @see SourceVersionAPI#deleteSourceVersion(SourceVersion)
	 */
	public int deleteSourceVersion(SourceVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = getDeleteSourceVersionStatement().replaceAll("\\[ID\\]",Integer.toString(v.getID()));
		
		return dbh.executeDml(query); 
	}
	
	/**
	 * The method returns the default SQL statement to load a set of source versions.
	 * @return SQL statement for loading source versions
	 */
	protected String getLoadSourceVersionStatement() {
		return 
		"select lds_version_id, lds_id, obj_type_name, pds_name, is_ontology, url, version, lds_date, structure_type "+
		"from gomma_obj_types a, gomma_pds b, gomma_lds c, gomma_lds_versions d "+
		"where a.obj_type_id=c.obj_type_id_fk "+
		  "and b.pds_id=c.pds_id_fk "+
		  "and c.lds_id = d.lds_id_fk";
	}
	
	protected String getSearchSourceVersionStatement(){
		return 
		"select distinct lds_version_id "+
		"from gomma_obj_versions a, gomma_lds_versions b ,gomma_lds d " +
		"where d.lds_id=b.lds_id_fk and a.lds_id_fk= b.lds_id_fk  and lds_date between date_from and date_to " +
		"and (accession like '%[cond]%' " +
		"or exists (select *from gomma_obj_att_values c " +
		"where a.obj_id=c.obj_id_fk and b.lds_date between c.date_from " +
		"and c.date_to and c.att_value_s like '%[cond]%'))";
	}
	/**
	 * The method returns the default SQL statement to store a source version within the repository.
	 * @return SQL statement for inserting a source version 
	 */
	protected String getInsertSourceVersionBySourceStatement() {
		return
		"insert into gomma_lds_versions (lds_id_fk,version,lds_date,structure_type) "+
		"value (?,?,?,?)";
	}
	
	/**
	 * The method returns the default SQL statement to store a source version within the repository.
	 * @return SQL statement for inserting a source version
	 */
	protected String getInsertSourceVersionStatement() { 
		return
		"insert into gomma_lds_versions (lds_id_fk,version,lds_date,structure_type) "+
		"select lds_id,?,?,? "+
		"from gomma_lds, gomma_pds, gomma_obj_types "+
		"where pds_name = ? "+
		  "and obj_type_name = ? "+
		  "and pds_id_fk = pds_id "+
		  "and obj_type_id_fk = obj_type_id";
	}
	
	/**
	 * The method returns the default SQL statement to update a source version within the repository.
	 * @return SQL statement for updating the source version
	 */
	protected String getUpdateSourceVersionStatement() { 
		return 
		"update gomma_lds_versions "+
		"set version=?, lds_date=?, structure_type=? where lds_version_id=?";
	}
	
	/**
	 * The method returns the default SQL statement to delete a specific source version.
	 * @return SQL statement for deleting a source version
	 */
	protected String getDeleteSourceVersionStatement() { 
		return 
		"delete from gomma_lds_versions where lds_version_id=[ID]";
	}
	
	private SourceVersionSet extractSearchSourceVersionSet(ResultSet rs,SourceVersionSet srcs) throws RepositoryException{
		SourceVersionSet v=new SourceVersionSet();
		
		try {
			while (rs.next()) {
				
				for(SourceVersion sv:srcs.getCollection()){
					if(sv.getID()==rs.getInt(1)){
						if(!v.contains(sv.getID())){
							v.addSourceVersion(sv);
						}
						break;
					}
				}
			}
			return v;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
	}
	
	/**
	 * The method returns source version metadata from the given result set.
	 * @param rs result set containing the source version metadata
	 * @param srcs set of source versions to be extended by the current source versions
	 * @return set of source versions
	 * @see org.gomma.model.source.SourceVersion
	 * @see org.gomma.model.source.SourceVersionSet
	 * @throws SQLException
	 */
	private SourceVersionSet extractSourceVersionSet(ResultSet rs, SourceVersionSet srcs) throws RepositoryException {
		SourceVersion sv;
		
		try {
			while (rs.next()) {
				sv = new SourceVersion.Builder(
					CacheManager.getInstance().getSourceCache().getSource(
							rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(6),rs.getInt(5)==1),
					rs.getInt(1))
						.version(rs.getString(7))
						.creationDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()))
						.graphType(GraphTypes.getGraphType(rs.getInt(9))).build();
			
				srcs.addSourceVersion(sv);
			}
			return srcs;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	
}
