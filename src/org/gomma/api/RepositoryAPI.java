package org.gomma.api;

import org.gomma.exceptions.ConfigurationException;
import org.gomma.exceptions.RepositoryException;

/**
 * The interface defines administrative methods too manage the 
 * repository, e.g., to create and drop the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface RepositoryAPI {

	/**
	 * The method creates the schema for the specified repository. The  
	 * statements are specified within a file located in the directory org.gomma.api.config.
	 * @throws RepositoryException
	 * @throws ConfigurationException
	 */
	public void createSchema() throws RepositoryException, ConfigurationException;
	
	/**
	 * The method deletes the schema in the specified repository. The 
	 * statements are specified within the file located in the directory org.gomma.api.
	 * @throws RepositoryException
	 * @throws ConfigurationException
	 */
	public void dropSchema() throws RepositoryException, ConfigurationException;
	
	/**
	 * The method returns a new high value that is used to 
	 * create a new overall unique object identifier. All
	 * generated high values are stored within the repository.
	 * @return high value that is used to create an overall unique object identifier
	 * @throws RepositoryException
	 */
	public int getHighValue() throws RepositoryException;
	
	/**
	 * The method returns the identifier for the repository data source
	 * that is unique over all available repositories.
	 * @return unique repository identifier
	 * @throws RepositoryException
	 */
	public int getSourceID() throws RepositoryException;
}
