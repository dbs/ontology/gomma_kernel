package org.gomma.api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.ConfigurationException;
import org.gomma.exceptions.RepositoryException;

/**
 * The class implements the administrative repository interface. It is the 
 * default implementation for a relational database system. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public abstract class RepositoryAPI_RDBMS implements RepositoryAPI {
	
	
	
	private static final String INSERT_HIGH_VALUE = 
		"insert into gomma_keys () values()";
	
	private static final String LOAD_SOURCE_ID =
		"select source_id from gomma_data_source";
	
	public RepositoryAPI_RDBMS() {}

	/**
	 * @see RepositoryAPI#createSchema()
	 */
	public void createSchema() throws RepositoryException, ConfigurationException {
		DatabaseHandler.getInstance()
			.executeDml(this.readFile(getCreationSchemaScriptFileName()).split(";"));
	}

	/**
	 * @see RepositoryAPI#dropSchema()
	 */
	public void dropSchema() throws RepositoryException, ConfigurationException {
		DatabaseHandler.getInstance()
			.executeDml(this.readFile(getDropSchemaScriptFileName()).split(";"));
	}
	
	
	/**
	 * @see RepositoryAPI#getHighValue()
	 */
	public int getHighValue() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_HIGH_VALUE;
		PreparedStatement pStmt = null;
		int highValue = -1;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			
			ResultSet rs = pStmt.getGeneratedKeys();
			while (rs.next()) highValue = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return highValue;
	}
	
	/**
	 * @see RepositoryAPI#getSourceID()
	 */
	public int getSourceID() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_SOURCE_ID;
		int sourceID = -1;
		
		try {
			ResultSet rs = dbh.executeSelect(query);
			while (rs.next()) sourceID = rs.getInt(1);
			dbh.closeStatement(rs);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		return sourceID;
	}
	
	/**
	 * The method returns the name and location (path included) of the file
	 * containing the DDL to create the schema
	 * @return name and location of the schema creation DDL file
	 */
	protected abstract String getCreationSchemaScriptFileName();
	
	/**
	 * The method returns the name and location (path included) of the file
	 * containing the DDL to drop the schema
	 * @return name and location of the schema drop DDL file
	 */
	protected abstract String getDropSchemaScriptFileName();
	
	/**
	 * The method reads and returns the content of the DDL file.
	 * @param fileNameLoc name and location of the DDL file.
	 * @return content of the DDL file
	 * @throws ConfigurationException
	 */
	private String readFile(String fileNameLoc) throws ConfigurationException {
		
		String line;
		StringBuilder buf = new StringBuilder();
		
		try {
			URL url = ClassLoader.getSystemResource(fileNameLoc);
			BufferedReader reader = new BufferedReader(new FileReader(url.getPath()));
			
			while ((line=reader.readLine())!=null) buf.append(line);
			reader.close();
			return buf.toString();
			
		} catch (FileNotFoundException e) {
			throw new ConfigurationException(e.getMessage());
		} catch (IOException e) {
			throw new ConfigurationException(e.getMessage());
		}
	}

}
