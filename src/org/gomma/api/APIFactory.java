package org.gomma.api;

import org.gomma.api.analysis.CorrespondenceAnalysisAPI;
import org.gomma.api.analysis.CorrespondenceAnalysisAPI_FEVER;
import org.gomma.api.analysis.CorrespondenceAnalysisAPI_RDBMS;
import org.gomma.api.analysis.EvolutionStatisticsAPI;
import org.gomma.api.analysis.EvolutionStatisticsAPI_FEVER;
import org.gomma.api.analysis.EvolutionStatisticsAPI_RDBMS;
import org.gomma.api.analysis.MappingStatisticsAPI;
import org.gomma.api.analysis.MappingStatisticsAPI_FEVER;
import org.gomma.api.analysis.MappingStatisticsAPI_RDBMS;
import org.gomma.api.analysis.SourceStatisticsAPI;
import org.gomma.api.analysis.SourceStatisticsAPI_FEVER;
import org.gomma.api.analysis.SourceStatisticsAPI_RDBMS;
import org.gomma.api.analysis.StabilityAPI;
import org.gomma.api.analysis.StabilityAPI_FEVER;
import org.gomma.api.analysis.StabilityAPI_RDBMS;
import org.gomma.api.mapping.MappingAPI;
import org.gomma.api.mapping.MappingAPI_FEVER;
import org.gomma.api.mapping.MappingAPI_RDBMS;
import org.gomma.api.mapping.MappingVersionAPI;
import org.gomma.api.mapping.MappingVersionAPI_FEVER;
import org.gomma.api.mapping.MappingVersionAPI_RDBMS;
import org.gomma.api.mapping.MappingVersionImportAPI;
import org.gomma.api.mapping.MappingVersionImportAPI_FEVER;
import org.gomma.api.mapping.MappingVersionImportAPI_RDBMS;
import org.gomma.api.mapping.ObjCorrespondenceAPI;
import org.gomma.api.mapping.ObjCorrespondenceAPI_FEVER;
import org.gomma.api.mapping.ObjCorrespondenceAPI_RDBMS;
import org.gomma.api.source.AttributeAPI;
import org.gomma.api.source.AttributeAPI_FEVER;
import org.gomma.api.source.AttributeAPI_RDBMS;
import org.gomma.api.source.ObjectAPI;
import org.gomma.api.source.ObjectAPI_FEVER;
import org.gomma.api.source.ObjectAPI_RDBMS;
import org.gomma.api.source.SourceAPI;
import org.gomma.api.source.SourceAPI_FEVER;
import org.gomma.api.source.SourceAPI_RDBMS;
import org.gomma.api.source.SourceVersionAPI;
import org.gomma.api.source.SourceVersionAPI_FEVER;
import org.gomma.api.source.SourceVersionAPI_RDBMS;
import org.gomma.api.source.SourceVersionImportAPI;
import org.gomma.api.source.SourceVersionImportAPI_FEVER;
import org.gomma.api.source.SourceVersionImportAPI_RDBMS;
import org.gomma.api.source.SourceVersionStructureAPI;
import org.gomma.api.source.SourceVersionStructureAPI_FEVER;
import org.gomma.api.source.SourceVersionStructureAPI_RDBMS;
import org.gomma.api.util.DataSourceSystems;
import org.gomma.api.work.MatchTaskAPI;
import org.gomma.api.work.MatchTaskAPI_FEVER;
import org.gomma.api.work.MatchTaskAPI_RDBMS;
import org.gomma.api.work.WorkPackageAPI;
import org.gomma.api.work.WorkPackageAPI_FEVER;
import org.gomma.api.work.WorkPackageAPI_RDBMS;
import org.gomma.exceptions.RepositoryException;
import org.gomma.util.SystemPropertyPool;

/**
 * The class represents a factory providing functional-specific 
 * access to the repository. The factory is a singleton (see Gamma et. al) 
 * and, thus, can be called from somewhere in the kernel.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class APIFactory {

	/**
	 * The item represents the administrative repository API.  
	 */
	private RepositoryAPI repositoryAPI = null;
	
	/**
	 * The item represents the source API.
	 */
	private SourceAPI sourceAPI = null;
	
	/**
	 * The item represents the source version API. 
	 */
	private SourceVersionAPI sourceVersionAPI = null;
	
	/**
	 * The item represents the source version structure API. 
	 */
	private SourceVersionStructureAPI sourceVersionStructureAPI = null;
	
	/**
	 * The item represents the object API.
	 */
	private ObjectAPI objectAPI = null;
	
	/**
	 * The item represents the attribute API.
	 */
	private AttributeAPI attributeAPI = null;
	
	/**
	 * The item represents the mapping API. 
	 */
	private MappingAPI mappingAPI = null;
	
	/**
	 * The item represents the mapping version API.
	 */
	private MappingVersionAPI mappingVersionAPI = null;
	
	/**
	 * The item represents the mapping version import API.
	 */
	private MappingVersionImportAPI mappingVersionImportAPI = null;
	
	/**
	 * The item represents the object correspondence API.
	 */
	private ObjCorrespondenceAPI objCorrespondenceAPI = null;
	
	/**
	 * The item represents the stability API.
	 */
	private StabilityAPI stabilityAPI=null;
	
	/**
	 * The item represents the source version import API.
	 */
	private SourceVersionImportAPI sourceVersionImportAPI= null;
	
	/**
	 * The item represents the work package API.
	 */
	private WorkPackageAPI workPackageAPI = null;
	
	/**
	 * The item represents the match task API. 
	 */
	private MatchTaskAPI matchTaskAPI = null;
	
	/**
	 * The item represents the source statistics API.
	 */
	private SourceStatisticsAPI sourceStatisticsAPI = null;
	
	/**
	 * The item represents the mapping statistics API.
	 */
	private MappingStatisticsAPI mappingStatisticsAPI = null;
	
	/**
	 * The item represents the evolution statistics API.
	 */
	private EvolutionStatisticsAPI evolutionStatisticsAPI = null;
	
	/**
	 * The item represents the correspondence analysis API.
	 */
	private CorrespondenceAnalysisAPI correspondenceAnalysisAPI = null;
	
	/**
	 * The item represents the current repository system, e.g., MySQL etc.
	 */
	private DataSourceSystems currentRepositorySystem = null;
	
	/**
	 * The item represents the singleton instance of the API factory.
	 */
	private volatile static APIFactory singleton = null;
	
	
	/**
	 * The method returns an instance of the API factory. The factory acts as singleton
	 * @return an instance of the API factory
	 */
	public static APIFactory getInstance() {
		if (singleton == null) {
			synchronized (APIFactory.class) {
				if (singleton==null)
					singleton = new APIFactory();
			}
		}
		return singleton;
	}
	
	/**
	 * Constructor of the class.
	 */
	private APIFactory() {}
	
	/**
	 * The method performs a shutdown of all included APIs 
	 * but not the API factory itself.
	 */
	public void shutdown() {
		this.repositoryAPI = null;
		
		this.sourceAPI = null;
		this.sourceVersionAPI = null;
		this.sourceVersionStructureAPI = null;
		this.objectAPI = null;
		this.attributeAPI = null;
		
		this.mappingAPI = null;
		this.mappingVersionAPI = null;
		this.objCorrespondenceAPI = null;
		this.stabilityAPI=null;
		
		this.sourceVersionImportAPI = null;
		this.mappingVersionImportAPI= null;
		
		this.workPackageAPI = null;
		this.matchTaskAPI = null;
		
		this.sourceStatisticsAPI = null;
		this.mappingStatisticsAPI = null;
		this.evolutionStatisticsAPI = null;
		
		this.correspondenceAnalysisAPI = null;
	}
	
	/**
	 * The method stops the API factory (destructor).
	 */
	public static void stop() {
		singleton = null;
	}
	
	/**
	 * The method returns the general repository API allowing to create
	 * and drop the repository schema etc.
	 * @return general repository API
	 * @throws RepositoryException
	 */
	public RepositoryAPI getRepositoryAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.repositoryAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.repositoryAPI = new RepositoryAPI_MySQL(); break;
				case FEVER: this.repositoryAPI = new RepositoryAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The general repository API is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.repositoryAPI;
	}
	
	/**
	 * The method returns the source API.
	 * {@link org.gomma.api.source.SourceAPI}
	 * @return source API
	 * @throws RepositoryException
	 */
	public SourceAPI getSourceAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.sourceAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.sourceAPI = new SourceAPI_RDBMS(); break;
				case FEVER: this.sourceAPI = new SourceAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The source API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.sourceAPI;
	}
	
	/**
	 * The method returns the source version API.
	 * {@link org.gomma.api.source.SourceVersionAPI}
	 * @return source version API
	 * @throws RepositoryException
	 */
	public SourceVersionAPI getSourceVersionAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.sourceVersionAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.sourceVersionAPI = new SourceVersionAPI_RDBMS(); break;
				case FEVER: this.sourceVersionAPI = new SourceVersionAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The source version API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.sourceVersionAPI;
	}
	
	/**
	 * The method returns the source version structure API.
	 * {@link org.gomma.api.source.SourceVersionStructureAPI}
	 * @return source version structure API
	 * @throws RepositoryException
	 */
	public SourceVersionStructureAPI getSourceVersionStructureAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.sourceVersionStructureAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.sourceVersionStructureAPI = new SourceVersionStructureAPI_RDBMS(); break;
				case FEVER: this.sourceVersionStructureAPI = new SourceVersionStructureAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The source version structure API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.sourceVersionStructureAPI;
	}
	
	/**
	 * The method returns the object API.
	 * {@link org.gomma.api.source.ObjectAPI}
	 * @return object API
	 * @throws RepositoryException
	 */
	public ObjectAPI getObjectAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.objectAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.objectAPI = new ObjectAPI_RDBMS(); break;
				case FEVER: this.objectAPI = new ObjectAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The object API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.objectAPI;
	}
	
	/**
	 * The method return the attribute API.
	 * {@link org.gomma.api.source.AttributeAPI}
	 * @return attribute API
	 * @throws RepositoryException
	 */
	public AttributeAPI getAttributeAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.attributeAPI==null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.attributeAPI = new AttributeAPI_RDBMS(); break;
				case FEVER: this.attributeAPI = new AttributeAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The attribute API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.attributeAPI;
	}
	
	/**
	 * The method returns the mapping API.
	 * {@link org.gomma.api.mapping.MappingAPI}
	 * @return mapping API
	 * @throws RepositoryException 
	 */
	public MappingAPI getMappingAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.mappingAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.mappingAPI = new MappingAPI_RDBMS(); break;
				case FEVER: this.mappingAPI = new MappingAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The mapping API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.mappingAPI;
	}
	
	/**
	 * The method returns the mapping version API.
	 * {@link org.gomma.api.mapping.MappingVersionAPI}
	 * @return mapping version API
	 * @throws RepositoryException
	 */
	public MappingVersionAPI getMappingVersionAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.mappingVersionAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.mappingVersionAPI = new MappingVersionAPI_RDBMS(); break;
				case FEVER: this.mappingVersionAPI = new MappingVersionAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The mapping version API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.mappingVersionAPI;
	}
	
	/**
	 * The method returns the mapping version import API.
	 * {@link org.gomma.api.mapping.MappingVersionImportAPI}
	 * @return mapping version import API
	 * @throws RepositoryException
	 */
	public MappingVersionImportAPI getMappingVersionImportAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.mappingVersionImportAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.mappingVersionImportAPI = new MappingVersionImportAPI_RDBMS(); break;
				case FEVER: this.mappingVersionImportAPI = new MappingVersionImportAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The mapping version import API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.mappingVersionImportAPI;
	}
	
	/**
	 * The method returns the object correspondence API.
	 * {@link org.gomma.api.mapping.ObjCorrespondenceAPI}
	 * @return object correspondence API
	 * @throws RepositoryException
	 */
	public ObjCorrespondenceAPI getObjCorrespondenceAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.objCorrespondenceAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.objCorrespondenceAPI = new ObjCorrespondenceAPI_RDBMS(); break;
				case FEVER: this.objCorrespondenceAPI = new ObjCorrespondenceAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The object correspondence API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.objCorrespondenceAPI;
	}
	
	/**
	 * The method returns the stability API.
	 * {@link org.gomma.api.analysis.StabilityAPI}
	 * @return stability API
	 * @throws RepositoryException
	 */
	public StabilityAPI getStabilityAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.stabilityAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.stabilityAPI = new StabilityAPI_RDBMS(); break;
				case FEVER: this.stabilityAPI = new StabilityAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The stability API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.stabilityAPI;
	}
	
	/**
	 * The method returns the source version import API.
	 * {@link org.gomma.api.source.SourceVersionImportAPI}
	 * @return source version import API
	 * @throws RepositoryException
	 */
	public SourceVersionImportAPI getSourceVersionImportAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.sourceVersionImportAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.sourceVersionImportAPI = new SourceVersionImportAPI_RDBMS(); break;
				case FEVER: this.sourceVersionImportAPI = new SourceVersionImportAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The source version import API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.sourceVersionImportAPI;
	}
	
	
	/**
	 * The method returns the work package API.
	 * {@link org.gomma.api.work.WorkPackageAPI}
	 * @return work package API
	 * @throws RepositoryException
	 */
	public WorkPackageAPI getWorkPackageAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.workPackageAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.workPackageAPI = new WorkPackageAPI_RDBMS(); break;
				case FEVER: this.workPackageAPI = new WorkPackageAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The work package API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.workPackageAPI;
	}
	
	/**
	 * The method returns the match task API.
	 * {@link org.gomma.api.work.MatchTaskAPI}
	 * @return match task API
	 * @throws RepositoryException
	 */
	public MatchTaskAPI getMatchTaskAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.matchTaskAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.matchTaskAPI = new MatchTaskAPI_RDBMS(); break;
				case FEVER: this.matchTaskAPI = new MatchTaskAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The match task API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.matchTaskAPI;
	}
	
	/**
	 * The method returns the source statistics API.
	 * {@link org.gomma.api.analysis.SourceStatisticsAPI}
	 * @return source statistics API
	 * @throws RepositoryException
	 */
	public SourceStatisticsAPI getSourceStatisticsAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.sourceStatisticsAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.sourceStatisticsAPI = new SourceStatisticsAPI_RDBMS(); break;
				case FEVER: this.sourceStatisticsAPI = new SourceStatisticsAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The source statistics API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.sourceStatisticsAPI;
	}
	
	/**
	 * The method returns the mapping statistics API.
	 * {@link org.gomma.api.analysis.MappingStatisticsAPI}
	 * @return mapping statistics API
	 * @throws RepositoryException
	 */
	public MappingStatisticsAPI getMappingStatisticsAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.mappingStatisticsAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.mappingStatisticsAPI = new MappingStatisticsAPI_RDBMS(); break;
				case FEVER: this.mappingStatisticsAPI = new MappingStatisticsAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The mapping statistics API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.mappingStatisticsAPI;
	}
	
	/**
	 * The method returns the source evolution API.
	 * {@link org.gomma.api.analysis.EvolutionStatisticsAPI}
	 * @return source evolution API
	 * @throws RepositoryException
	 */
	public EvolutionStatisticsAPI getEvolutionStatisticsAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.evolutionStatisticsAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.evolutionStatisticsAPI = new EvolutionStatisticsAPI_RDBMS(); break;
				case FEVER: this.evolutionStatisticsAPI = new EvolutionStatisticsAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The evolution statistics API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.evolutionStatisticsAPI;
	}
	
	/**
	 * The method returns the object correspondence analysis API.
	 * {@link org.gomma.api.analysis.CorrespondenceAnalysisAPI}
	 * @return correspondence analysis API
	 * @throws RepositoryException
	 */
	public CorrespondenceAnalysisAPI getCorrespondenceAnalysisAPI() throws RepositoryException {
		this.checkDataSourceSystem();
		if (this.correspondenceAnalysisAPI == null) {
			switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
				case MYSQL: this.correspondenceAnalysisAPI = new CorrespondenceAnalysisAPI_RDBMS(); break;
				case FEVER: this.correspondenceAnalysisAPI = new CorrespondenceAnalysisAPI_FEVER(); break;
				default: throw new RepositoryException(
						"The correspondence analysis API of the repository is currently not implemented "+
						"for data source system '"+SystemPropertyPool.getInstance().getDataSourceSystem().getName()+"'.");
			}
		}
		return this.correspondenceAnalysisAPI;
	}
	
	/**
	 * The method checks whether the currently used repository system is set; 
	 * otherwise it resets the repository system, i.e., it sets which database management system is used 
	 * taking the system properties into account. 
	 */
	private void checkDataSourceSystem() {
		if (this.currentRepositorySystem == null) {
			this.currentRepositorySystem = SystemPropertyPool.getInstance().getDataSourceSystem();
			return;
		} else if (this.currentRepositorySystem != SystemPropertyPool.getInstance().getDataSourceSystem()) {
			this.shutdown();
			this.currentRepositorySystem = SystemPropertyPool.getInstance().getDataSourceSystem();
		}
			
	}
}
