package org.gomma.api.mapping;

import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The interface defines methods for managing sets of object correspondences within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface ObjCorrespondenceAPI {
    
	
	/**
	 * The method returns a set of object correspondences for the given mapping version 
	 * which domain accession number value and range accession number value is equal  the specified values  
	 * @param v mapping version
	 * @param dAcc domain accession number value
	 * @param rAcc domain accession number value
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v, String dAcc, String rAcc) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns the simple set of object correspondences for the given mapping version object.
	 * @param v mapping version 
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws RepositoryException, WrongSourceException;
	
	
	/**
	 * The method returns the complete set of object correspondences for the given mapping version object.
	 * @param v mapping version 
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of object correspondences for the given mapping version 
	 * which having the given object as domain or range object.
	 * @param v mapping version 
	 * @param objId object Id
	 * @param isDomain domain object or range object
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, int objId, boolean isDomain) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of object correspondences for the given mapping version 
	 * which support and (!) confidence value is equal or greater than the specified values  
	 * @param v mapping version
	 * @param minOccurrence lowest support value
	 * @param minConfidence lowest confidence value
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConfidence) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method stores the given set of object correspondences for the given mapping version object
	 * and returns the number of affected rows. 
	 * @param v mapping version
	 * @param set set of object correspondences that should be stored
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
		
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, String[] corrTypes) throws RepositoryException, WrongSourceException;
	
	/**
	 * The method returns a set of object correspondences for the given mapping version 
	 * containing correspondences with the given correspondence types.
	 * @param v mapping version 
	 * @param corrTypes correspondence types
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	
	public int insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws RepositoryException;
	
	/**
	 * The method deletes the complete set of object correspondences for the given mapping 
	 * version object and returns the number of affected rows. 
	 * @param v mapping version
	 * @return number of deleted object correspondences
	 * @throws RepositoryException
	 */
	public int deleteObjectCorrespondenceSet(MappingVersion v) throws RepositoryException;

	/**
	 * The method returns a set of object correspondences for the given mapping version 
	 * which confidence value is equal or greater than the specified values  
	 * @param v mapping version
	 * @param minConfidence lowest confidence value
	 * @return set of object correspondences
	 * @throws GommaException 
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,float confThreshold) throws GommaException;
}
