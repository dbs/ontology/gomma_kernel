package org.gomma.api.mapping;

import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class implements the object correspondence API. It is the implementation 
 * for the FEVER system using a relational database system at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class ObjCorrespondenceAPI_FEVER implements ObjCorrespondenceAPI {

	

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws RepositoryException, WrongSourceException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion, int, float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,
			int minOccurrence, float minConfidence) throws RepositoryException,
			WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
	/**
	 * @throws GommaException 
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion, float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,
			float confThreshold) throws GommaException {
		throw new RepositoryException("No implementation available");
	}
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSetSimple(MappingVersion, String, String)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v, String dAcc, String rAcc)
			throws RepositoryException, WrongSourceException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSetSimple(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v) throws RepositoryException, WrongSourceException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion, String[])
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, String[] corrTypes) throws RepositoryException, WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see ObjCorrespondenceAPI#insertObjectCorrespondenceSet(MappingVersion, ObjCorrespondenceSet)
	 */
	public int insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjCorrespondenceAPI#deleteObjectCorrespondenceSet(MappingVersion)
	 */
	public int deleteObjectCorrespondenceSet(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion,int,boolean)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v,
			int objId, boolean isDomain) throws RepositoryException,
			WrongSourceException {
		throw new RepositoryException("No implementation available");
	}
}
