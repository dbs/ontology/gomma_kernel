package org.gomma.api.mapping;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportCorrespondence;
import org.gomma.io.importer.models.ImportMapping;
import org.gomma.io.importer.models.ImportSource;

/**
 * The class implements the mapping version import API. It is the default
 * implementation for a relational database.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class MappingVersionImportAPI_RDBMS implements MappingVersionImportAPI {
	
	private static final String IMPORT_MAPPING_CORE_DATA =
		"insert into gomma_maps (map_type_id_fk,map_class_id_fk,map_method_id_fk,map_tool_id_fk,d_lds_id_fk,r_lds_id_fk,name,is_inst_map) "+
			"select a.map_type_id, b.map_class_id, c.map_method_id, d.map_tool_id, d_lds.lds_id, r_lds.lds_id, ?, ? "+
			"from gomma_map_types a, gomma_map_classes b, gomma_map_methods c, gomma_map_tools d, gomma_obj_types d_type, gomma_pds d_pds, gomma_lds d_lds, gomma_obj_types r_type, gomma_pds r_pds, gomma_lds r_lds "+
			"where a.name = ? and b.name = ? and c.name = ? and d.name = ? "+
            "and d_pds.pds_name = ? and d_type.obj_type_name = ? and d_pds.pds_id = d_lds.pds_id_fk and d_type.obj_type_id = d_lds.obj_type_id_fk "+
            "and r_pds.pds_name = ? and r_type.obj_type_name = ? and r_pds.pds_id = r_lds.pds_id_fk and r_type.obj_type_id = r_lds.obj_type_id_fk";
	
	private static final String IMPORT_MAPPING_ATTRIBUTES =
		"insert into gomma_map_atts (map_id_fk,att_name,att_value_s,att_value_i,att_value_f) " +
			"values (?,?,?,?,?)";
	
	private static final String IMPORT_MAPPING_VERSION_CORE_DATA = 
		"insert into gomma_map_versions (map_id_fk,name,creation_date,min_support,min_confidence,num_corrs,num_d_objs,num_r_objs) "+ 
			"values (?,?,?,?,?,?,?,?)";
	
	private static final String IMPORT_SRC_VERSION_METADATA =
		"insert into gomma_map_src_versions (map_version_id_fk,lds_version_id_fk,is_domain_lds) " +
			"select ?,d.lds_version_id,? " +
			"from gomma_pds a, gomma_obj_types b, gomma_lds c, gomma_lds_versions d " +
			"where d.lds_id_fk = c.lds_id and c.obj_type_id_fk = b.obj_type_id " +
			"and c.pds_id_fk = a.pds_id and a.pds_name = ? and b.obj_type_name = ? and d.lds_date = ?";
	
	private static final String IMPORT_INST_VERSION_METADATA =
		"insert into gomma_map_inst_versions (map_version_id_fk,lds_version_id_fk,is_domain_lds) " +
			"select ?,d.lds_version_id,? from gomma_pds a, gomma_obj_types b, gomma_lds c, gomma_lds_versions d " +
			"where d.lds_id_fk = c.lds_id and c.obj_type_id_fk = b.obj_type_id and c.pds_id_fk = a.pds_id " +
			"and a.pds_name = ? and b.obj_type_name = ? and d.lds_date = ?";
	
	private static final String IMPORT_MAPPING_VERSION_CORRS = 
		"insert into gomma_obj_corrs (map_version_id_fk,d_obj_id_fk,r_obj_id_fk,corr_type_id_fk,support,confidence,user_checked) " +
			"select ?, a.obj_id, b.obj_id, c.corr_type_id, ?, ? ,? " +
			"from gomma_obj_versions a, gomma_obj_versions b, gomma_corr_types c " +
			"where a.accession = ? and (? between a.date_from and a.date_to) " +
			"and b.accession = ? and (? between b.date_from and b.date_to) and c.name = ?";
	
	public MappingVersionImportAPI_RDBMS() {}

	public int importMapping(ImportMapping mapping) throws RepositoryException {
		try {
			DatabaseHandler dbh = DatabaseHandler.getInstance();
			
			//Mapping Core Daten registrieren
			int mapID;
			PreparedStatement pStmt = dbh.prepareStatement(IMPORT_MAPPING_CORE_DATA);
			if (mapping.baseName!=null) {
				pStmt.setString(1,mapping.baseName);
			} else {
				throw new RepositoryException("Mapping name not specified");
			}
			//Check if Mapping exists if yes, take the MapID
			ResultSet rs = dbh.executeSelect("select map_id from gomma_maps where name = '"+mapping.baseName+"' order by map_id DESC");
			if (rs.next()) {
				mapID = rs.getInt(1);
				dbh.closeStatement(rs);
			} else {
				dbh.closeStatement(rs);
				pStmt.setInt(2,mapping.isInstanceMap?1:0);
				pStmt.setString(3,mapping.mappingType);
				pStmt.setString(4,mapping.mappingClass);
				pStmt.setString(5,mapping.mappingMethod);
				pStmt.setString(6,mapping.mappingTool);
				pStmt.setString(7, mapping.domainSources.get(0).sourceName);
				pStmt.setString(8, mapping.domainSources.get(0).objectType);
				pStmt.setString(9, mapping.rangeSources.get(0).sourceName);
				pStmt.setString(10, mapping.rangeSources.get(0).objectType);
				pStmt.execute();
				dbh.closeStatement(pStmt);
				rs = dbh.executeSelect("select map_id from gomma_maps where name = '"+mapping.baseName+"' order by map_id DESC");
				if (rs.next()) {
					mapID = rs.getInt(1);
					dbh.closeStatement(rs);
				} else {
					dbh.closeStatement(rs);
					throw new RepositoryException("Could not register mapping core data in repository.");
				}
				
				//Mapping-Attribute integrieren
				pStmt = dbh.prepareStatement(IMPORT_MAPPING_ATTRIBUTES);
				Set<String> attributes = mapping.parameters.keySet();
				if (attributes.size()>0) {
					for (String attribute : attributes) {
						String value = mapping.parameters.get(attribute);
						pStmt.setInt(1,mapID);
						pStmt.setString(2,attribute);
						pStmt.setString(3,value);
						try {
							int intValue = Integer.parseInt(value);
							pStmt.setInt(4,intValue);
						} catch (NumberFormatException e) {
							pStmt.setNull(4,Types.INTEGER);
						}
						try {
							float floatValue = Float.parseFloat(value);
							pStmt.setFloat(5,floatValue);
						} catch (NumberFormatException e) {
							pStmt.setNull(5,Types.FLOAT);
						}
						pStmt.addBatch();
					}
					pStmt.executeBatch();
				}
				dbh.closeStatement(pStmt);
			}
			
			//Mapping-Version integrieren
			//Check if Mapping-Version exists, if yes throw Exception
			rs = dbh.executeSelect("select map_version_id from gomma_map_versions where name = '"+mapping.versionName+"' order by map_version_id DESC");
			if (rs.next()) {
				dbh.closeStatement(rs);
				throw new RepositoryException("Mapping version with same name already in in repository.");
			}
			dbh.closeStatement(rs);
			pStmt = dbh.prepareStatement(IMPORT_MAPPING_VERSION_CORE_DATA);
			pStmt.setInt(1, mapID);
			if (mapping.versionName!=null) {
				pStmt.setString(2,mapping.versionName);
			} else {
				throw new RepositoryException("Mapping-Version name not specified");
			}
			pStmt.setDate(3,new Date(mapping.mappingTimestamp.getTime().getTime()));
			pStmt.setInt(4,mapping.minSupport);
			pStmt.setFloat(5,mapping.minConfidence);
			pStmt.setInt(6,mapping.correspondences.size());
			List<String> objAccs = new Vector<String>();
			for (int i=0;i<mapping.correspondences.size();i++) {
				String tmpObj = mapping.correspondences.get(i).domainObjAcc;
				if (!objAccs.contains(tmpObj)) {
					objAccs.add(tmpObj);
				}
			}
			pStmt.setInt(7,objAccs.size());
			objAccs = new Vector<String>();
			for (int i=0;i<mapping.correspondences.size();i++) {
				String tmpObj = mapping.correspondences.get(i).rangeObjAcc;
				if (!objAccs.contains(tmpObj)) {
					objAccs.add(tmpObj);
				}
			}
			pStmt.setInt(8,objAccs.size());
			pStmt.execute();
			dbh.closeStatement(pStmt);
			
			int mapVersionID;
			rs = dbh.executeSelect("select map_version_id from gomma_map_versions where name = '"+mapping.versionName+"' order by map_version_id DESC");
			
			if (rs.next()) {
				mapVersionID = rs.getInt(1);
				dbh.closeStatement(rs);
			} else {
				dbh.closeStatement(rs);
				throw new RepositoryException("Could not register mapping version core data in repository.");
			}
			
			//SourceVersions des Mappings registrieren
			pStmt = dbh.prepareStatement(IMPORT_SRC_VERSION_METADATA);
			
			Map<String,Calendar> domainSourcesTimestamps = new Object2ObjectOpenHashMap<String,Calendar>(); 
			for (int i=0;i<mapping.domainSources.size();i++) {
				ImportSource s = mapping.domainSources.get(i);
				pStmt.setInt(1,mapVersionID);
				pStmt.setInt(2,1);
				pStmt.setString(3,s.sourceName);
				pStmt.setString(4,s.objectType);
				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
				domainSourcesTimestamps.put(s.getLDSIdentifier(),s.timestamp);
				pStmt.addBatch();
			}
			Map<String,Calendar> rangeSourcesTimestamps = new Object2ObjectOpenHashMap<String,Calendar>();
			for (int i=0;i<mapping.rangeSources.size();i++) {
				ImportSource s = mapping.rangeSources.get(i);
				pStmt.setInt(1,mapVersionID);
				pStmt.setInt(2,0);
				pStmt.setString(3,s.sourceName);
				pStmt.setString(4,s.objectType);
				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
				rangeSourcesTimestamps.put(s.getLDSIdentifier(),s.timestamp);
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
			
			//InstVersions des Mappings registrieren
			pStmt = dbh.prepareStatement(IMPORT_INST_VERSION_METADATA);
			
			for (int i=0;i<mapping.instanceDomainSources.size();i++) {
				ImportSource s = mapping.instanceDomainSources.get(i);
				pStmt.setInt(1,mapVersionID);
				pStmt.setInt(2,1);
				pStmt.setString(3,s.sourceName);
				pStmt.setString(4,s.objectType);
				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
				pStmt.addBatch();
			}
			for (int i=0;i<mapping.instanceRangeSources.size();i++) {
				ImportSource s = mapping.instanceRangeSources.get(i);
				pStmt.setInt(1,mapVersionID);
				pStmt.setInt(2,0);
				pStmt.setString(3,s.sourceName);
				pStmt.setString(4,s.objectType);
				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
			
			//Korrespondenzen importieren
			pStmt = dbh.prepareStatement(IMPORT_MAPPING_VERSION_CORRS);
			for (int i=0;i<mapping.correspondences.size();i++) {
				ImportCorrespondence c = mapping.correspondences.get(i);
				pStmt.setInt(1,mapVersionID);
				pStmt.setInt(2,c.support);
				pStmt.setFloat(3,c.confidence);
				pStmt.setInt(4,c.nChecked);
				pStmt.setString(5,c.domainObjAcc);
				Calendar domainObjDate = domainSourcesTimestamps.get(c.domainObjectType+"@"+c.domainSourceName);
				if (domainObjDate!=null) {
					pStmt.setDate(6,new Date(domainObjDate.getTime().getTime()));
					pStmt.setString(7,c.rangeObjAcc);
					Calendar rangeObjDate = rangeSourcesTimestamps.get(c.rangeObjectType+"@"+c.rangeSourceName);
					if (rangeObjDate!=null) {
						pStmt.setDate(8,new Date(rangeObjDate.getTime().getTime()));
						pStmt.setString(9,c.corr_type);
						pStmt.addBatch();
					}
				}
				if (i>0 && i%1000==0) {
					pStmt.executeBatch();
				}
			}
			try{
				pStmt.executeBatch();
			}catch (SQLException e){
				// nur wenn letzter batch genau durch 1000 teilbar war
				System.out.println(e);
			}
			
			dbh.closeStatement(pStmt);
			
			//Anzahl der Domain und Range-Objekte registrieren
			/*sqlQuery = "update gomma_maps set number_of_domain_objs = (select count(distinct d_obj_id_fk) from gomma_obj_corrs where map_id_fk = "+mapID+") where map_id = "+mapID;
			dbh.executeDml(sqlQuery);
			sqlQuery = "update gomma_maps set number_of_range_objs = (select count(distinct r_obj_id_fk) from gomma_obj_corrs where map_id_fk = "+mapID+") where map_id = "+mapID;
			dbh.executeDml(sqlQuery);*/
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		return 0;
	}
	
	
}
