package org.gomma.api.mapping;


import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportMapping;


public interface MappingVersionImportAPI {
	
	public int importMapping(ImportMapping mappingVersion) throws RepositoryException;
}
