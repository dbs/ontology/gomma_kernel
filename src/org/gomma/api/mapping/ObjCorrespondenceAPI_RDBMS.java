package org.gomma.api.mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class implements the object correspondence API. It is the default 
 * implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class ObjCorrespondenceAPI_RDBMS implements ObjCorrespondenceAPI {
	
	/**
	 * The constant represents the MySQL statement for loading a set of object correspondences.
	 */
	private static final String LOAD_CORRESPONDENCE_SET = 
		"select "+
			"a.d_obj_id_fk, b.accession, b.lds_id_fk, e.lds_version_id, "+
			"a.r_obj_id_fk, c.accession, c.lds_id_fk, f.lds_version_id, "+
			"a.confidence, a.support, a.user_checked, "+
			"d.name, d.descr "+
		"from gomma_obj_corrs a, gomma_obj_versions b, gomma_obj_versions c, gomma_corr_types d, "+
		"gomma_lds_versions e, gomma_lds_versions f, gomma_map_src_versions g, gomma_map_src_versions h "+
		"where b.obj_id=a.d_obj_id_fk "+
			"and c.obj_id=a.r_obj_id_fk "+
			"and d.corr_type_id=a.corr_type_id_fk "+
			"and e.lds_version_id=g.lds_version_id_fk "+
			"and f.lds_version_id=h.lds_version_id_fk "+
			"and e.lds_id_fk=b.lds_id_fk "+
			"and f.lds_id_fk=c.lds_id_fk "+
			"and e.lds_date between b.date_from and b.date_to "+
			"and f.lds_date between c.date_from and c.date_to "+
			"and a.map_version_id_fk=[mapVersionID] and g.map_version_id_fk=[mapVersionID] and h.map_version_id_fk=[mapVersionID]";
	
	private static final String LOAD_CORRESPONDENCE_SET_SIMPLE=
		 "select "+
		    "a.d_obj_id_fk, b.accession, " +
		    "a.r_obj_id_fk, c.accession, " +
		    "a.confidence, " +
		    "a.support " +
		    "from gomma_obj_corrs a, gomma_obj_versions b, gomma_obj_versions c " +
		    "where b.obj_id=a.d_obj_id_fk " +
		       "and c.obj_id=a.r_obj_id_fk " +
		       "and a.map_version_id_fk=[mapVersionID]";
		 
	
	/**
	 * The constant represents the MySQL statement for inserting a set of object correspondences.
	 */
	private static final String INSERT_CORRESPONDENCE_SET = 
		"insert into gomma_obj_corrs "+
			"(map_version_id_fk,d_obj_id_fk,r_obj_id_fk,corr_type_id_fk,confidence,support,user_checked) "+
		"select ?, ?, ?, corr_type_id, ?, ? ,? "+
		"from gomma_corr_types c "+
		"where name = ?";
	
	/**
	 * The constant represents the MySQL statement for delete complete set of object correspondences of specified mapping version.
	 */
	private static final String DELETE_CORRESPONDENCE_SET = 
		"delete from gomma_obj_corrs where map_version_id_fk=[mapVersionID]";
	
	/**
	 * Constructor of the class.
	 */
	public ObjCorrespondenceAPI_RDBMS() {}
	
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v) throws RepositoryException, WrongSourceException {
		return this.loadObjectCorrespondenceSet(v,"");
	}
	
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSetSimple(MappingVersion)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v)throws RepositoryException, WrongSourceException{
		return this.loadObjectCorrespondenceSetSimple(v,"");
	}
	
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSetSimple(MappingVersion,string,string)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSetSimple(MappingVersion v, String dAcc,String rAcc) throws RepositoryException, WrongSourceException{
		return this.loadObjectCorrespondenceSetSimple(v," and b.accession='"+dAcc+"' and c.accession='"+rAcc+"'");
	}
	/**
	 * @throws WrongSourceException 
	 * @throws RepositoryException 
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSetSimple(MappingVersion,string,string)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, float confThreshold) throws RepositoryException, WrongSourceException{
		return this.loadObjectCorrespondenceSet(v," and a.confidence >= "+confThreshold+" ");
	}
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion,int,boolean)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, int objId, boolean isDomain) throws RepositoryException, WrongSourceException{
		if(isDomain)
		return this.loadObjectCorrespondenceSetSimple(v, " and a.d_obj_id_fk= "+objId);
		else
		return this.loadObjectCorrespondenceSetSimple(v, " and a.r_obj_id_fk= "+objId);
	}
	
	private ObjCorrespondenceSet loadObjectCorrespondenceSetSimple(MappingVersion v,String queryCond) throws RepositoryException, WrongSourceException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_CORRESPONDENCE_SET_SIMPLE.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()))+queryCond;
		
		ResultSet rs = dbh.executeSelect(query);
		ObjCorrespondenceSet set = this.extractObjCorrespondencesSimple(rs);
		dbh.closeStatement(rs);
		
		return set;
	}
	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion, int, float)
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, int minOccurrence, float minConfidence) throws RepositoryException, WrongSourceException {
		return this.loadObjectCorrespondenceSet(v," and a.confidence>="+Float.toString(minConfidence)+" and a.support>="+Integer.toString(minOccurrence));
	}

	/**
	 * @see ObjCorrespondenceAPI#getObjectCorrespondenceSet(MappingVersion, String[])
	 */
	public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, String[] corrTypes) throws RepositoryException, WrongSourceException {
		String queryCond = " and (";
		for(int i=0;i< corrTypes.length ;i++){
			String corrType = corrTypes[i];
			if (i!=corrTypes.length-1)
				queryCond+= " d.name = \""+corrType+"\" or ";
			else
				queryCond+= " d.name = \""+corrType+"\" )"; 
		}
		
		return this.loadObjectCorrespondenceSet(v,queryCond);
	}
	
	/* old method getObjectCorrespondenceSet(..)
	 public ObjCorrespondenceSet getObjectCorrespondenceSet(MappingVersion v, String[] corrTypes) throws RepositoryException, WrongSourceException {
		String queryCond = "";
		for(String corrType:corrTypes){
			queryCond+= " and d.name = \""+corrType+"\"";
		}
		
		return this.loadObjectCorrespondenceSet(v,queryCond);
	}
	 */
	
	/**
	 * The method returns a set of object correspondences for the given 
	 * mapping version object and the specified query condition.
	 * @param v mapping version
	 * @param queryCond query condition
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	private ObjCorrespondenceSet loadObjectCorrespondenceSet(MappingVersion v, String queryCond) throws RepositoryException, WrongSourceException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_CORRESPONDENCE_SET.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()))+queryCond;

		ResultSet rs = dbh.executeSelect(query);
		ObjCorrespondenceSet set = this.extractObjCorrespondences(rs);
		dbh.closeStatement(rs);
		
		return set;
	}
	
	
	/**
	 * @see ObjCorrespondenceAPI#insertObjectCorrespondenceSet(MappingVersion, ObjCorrespondenceSet)
	 */
	public int insertObjectCorrespondenceSet(MappingVersion v, ObjCorrespondenceSet set) throws RepositoryException {
		// Assuming that the mapping, all relevant sources and objects are already exist within the repository
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_CORRESPONDENCE_SET;
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			
			for (ObjCorrespondence cor : set.getCollection()) {
				pStmt.setInt(1,v.getID());
				pStmt.setInt(2,cor.getDomainObjID());
				pStmt.setInt(3,cor.getRangeObjID());
				pStmt.setFloat(4,cor.getConfidence());
				pStmt.setInt(5,cor.getSupport());
				pStmt.setInt(6,cor.getNumberOfChecks());
				pStmt.setString(7, cor.getCorrespondenceTypeName());
				pStmt.addBatch();
			}
			for (int i : pStmt.executeBatch()) affectedRows+=i;
		} catch (SQLException e) {
			throw new RepositoryException("insertObjCorrespondenceSet(): "+e.getMessage()); 
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * @see ObjCorrespondenceAPI#deleteObjectCorrespondenceSet(MappingVersion)
	 */
	public int deleteObjectCorrespondenceSet(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_CORRESPONDENCE_SET.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()));
		
		return dbh.executeDml(query);
	}



	
	/**
	 * The method extracts and returns a set of object correspondences from a given SQL query result set.
	 * @param rs query result set
	 * @return set of object correspondences
	 * @throws RepositoryException
	 * @throws WrongSourceException
	 */
	private ObjCorrespondenceSet extractObjCorrespondences(ResultSet rs) throws RepositoryException, WrongSourceException {
		
		ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		ObjCorrespondence  cor = null;
		int domainObjID, rangeObjID;
		
		try {
			while (rs.next()) {
				domainObjID = rs.getInt(1);
				rangeObjID =  rs.getInt(5);
				
				if (!set.containsCorrespondence(domainObjID,rangeObjID))
					cor = new ObjCorrespondence.Builder(domainObjID,rs.getInt(3),rangeObjID,rs.getInt(7))
							.domainAccessionNumber(rs.getString(2))
							.rangeAccessionNumber(rs.getString(6))
							.confidence(rs.getFloat(9))
							.support(rs.getInt(10))
							.numberOfUserChecks(rs.getInt(11))
							.typeName(rs.getString(12))
							.typeDescription(rs.getString(13)).build();
				else cor = set.getCorrespondence(domainObjID,rangeObjID);
				
				cor.addDomainSourceVersionID(rs.getInt(4));
				cor.addRangeSourceVersionID(rs.getInt(8));
				
				set.addCorrespondence(cor);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException("loadObjCorrespondenceSet(): "+e.getMessage());
		}
	}	
	
private ObjCorrespondenceSet extractObjCorrespondencesSimple(ResultSet rs) throws RepositoryException, WrongSourceException {
		
		ObjCorrespondenceSet set = new ObjCorrespondenceSet();
		ObjCorrespondence  cor = null;
		int domainObjID, rangeObjID;
		
		try {
			while (rs.next()) {
				domainObjID = rs.getInt(1);
				rangeObjID =  rs.getInt(3);
				
				if (!set.containsCorrespondence(domainObjID,rangeObjID))
					cor = new ObjCorrespondence.Builder(domainObjID,0,rangeObjID,0)
							.domainAccessionNumber(rs.getString(2))
							.rangeAccessionNumber(rs.getString(4))
							.confidence(rs.getFloat(5))
							.support(rs.getInt(6))
							.build();
				else cor = set.getCorrespondence(domainObjID,rangeObjID);
				
				set.addCorrespondence(cor);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException("loadObjCorrespondenceSet(): "+e.getMessage());
		}
	}	
	
	
		
//	
//	private static String rewriteCorrespondenceQuery(String sqlQuery, String query) {
//		if (query.trim().length()==0) return sqlQuery;
//		
//		query = query.replaceAll("\\[support\\]","support");
//		query = query.replaceAll("\\[(conf|confidence)\\]","confidence");
//		
//		String matchPattern, matchStr, matchResult;
//		Pattern p;
//		Matcher m;
//		// replace domain and range objects
//		for (String obj : new String[]{"\\[domainObj\\]","\\[rangeObj\\]"}) {
//			matchPattern = obj+"\\s*(=\\s*\\d+|\\s+in\\s+\\(((\\d),)*\\d+\\))";
//			p = Pattern.compile(matchPattern);
//			while ((m=p.matcher(query)).find()) {
//				matchStr = query.substring(m.start(),m.end());
//				matchResult = (matchStr.startsWith("[domain")?"d":"r")+"_obj_id_fk"+
//								(matchStr.contains("=")?"="+matchStr.split("=")[1]:matchStr.replaceAll(obj,""))+" ";
//				query = query.replaceFirst(matchPattern,matchResult);
//			}
//		}
//		return sqlQuery += " and ("+query+")";
//	}

}
