package org.gomma.api.mapping;

import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportMapping;

/**
 * Frage: Brauchen wir ein explizites Mapping-Import-API?
 * Es kann doch auf die Methoden des MappingAPI, MappingVersionAPI und 
 * ObjCorespondenceAPI zurueckgegriffen werden.
 * 
 *@deprecated
 */
public class MappingImportAPI_MySQL {

	public MappingImportAPI_MySQL() {}
	
	public void importMapping(ImportMapping mapping) throws RepositoryException {
//		try {
//			//Mapping Core Daten registrieren
//			String sqlQuery = this.sql.getStatement(GommaRepositorySQL.IMPORT_MAPPING_CORE_DATA);
//			PreparedStatement pStmt = this.dbHandler.prepareStatement(sqlQuery);
//			if (mapping.versionName!=null) {
//				pStmt.setString(1,mapping.versionName);
//			} else {
//				throw new RepositoryException("Mapping name not specified");
//			}
//			
//			pStmt.setInt(2,mapping.isInstanceMap?1:0);
//			pStmt.setInt(3,mapping.minSupport);
//			pStmt.setFloat(4,mapping.minConfidence);
//			pStmt.setInt(5,mapping.correspondences.size());
//			pStmt.setString(6,mapping.mappingType);
//			pStmt.setString(7,mapping.mappingClass);
//			pStmt.setString(8,mapping.mappingMethod);
//			pStmt.setString(9,mapping.mappingTool);
//			pStmt.execute();
//			
//			int mapID;
//			ResultSet rs = this.dbHandler.executeSelect("select map_id from gomma_maps where name = '"+mapping.versionName+"' order by map_id DESC");
//			
//			if (rs.next()) {
//				mapID = rs.getInt(1);
//				this.dbHandler.closeStatement(rs);
//			} else {
//				this.dbHandler.closeStatement(rs);
//				throw new RepositoryException("Could not register mapping core data in repository.");
//			}
//			
//			//Mapping-Attribute integrieren
//			sqlQuery = this.sql.getStatement(GommaRepositorySQL.IMPORT_MAPPING_ATTRIBUTES);
//			pStmt = this.dbHandler.prepareStatement(sqlQuery);
//			Set<String> attributes = mapping.parameters.keySet();
//			if (attributes.size()>0) {
//				for (String attribute : attributes) {
//					String value = mapping.parameters.get(attribute);
//					pStmt.setInt(1,mapID);
//					pStmt.setString(2,attribute);
//					pStmt.setString(3,value);
//					try {
//						int intValue = Integer.parseInt(value);
//						pStmt.setInt(4,intValue);
//					} catch (NumberFormatException e) {
//						pStmt.setNull(4,Types.INTEGER);
//					}
//					try {
//						float floatValue = Float.parseFloat(value);
//						pStmt.setFloat(5,floatValue);
//					} catch (NumberFormatException e) {
//						pStmt.setNull(5,Types.FLOAT);
//					}
//					pStmt.addBatch();
//				}
//				pStmt.executeBatch();
//				this.dbHandler.closeStatement(pStmt);
//			}
//			
//			//SourceVersions des Mappings registrieren
//			sqlQuery = this.sql.getStatement(GommaRepositorySQL.IMPORT_MAPPING_SRC_VERSION);
//			pStmt = this.dbHandler.prepareStatement(sqlQuery);
//			
//			HashMap<String,Calendar> domainSourcesTimestamps = new HashMap<String,Calendar>(); 
//			for (int i=0;i<mapping.domainSources.size();i++) {
//				ImportSource s = mapping.domainSources.get(i);
//				pStmt.setInt(1,mapID);
//				pStmt.setInt(2,1);
//				pStmt.setString(3,s.sourceName);
//				pStmt.setString(4,s.objectType);
//				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
//				domainSourcesTimestamps.put(s.getLDSIdentifier(),s.timestamp);
//				pStmt.addBatch();
//			}
//			HashMap<String,Calendar> rangeSourcesTimestamps = new HashMap<String,Calendar>();
//			for (int i=0;i<mapping.rangeSources.size();i++) {
//				ImportSource s = mapping.rangeSources.get(i);
//				pStmt.setInt(1,mapID);
//				pStmt.setInt(2,0);
//				pStmt.setString(3,s.sourceName);
//				pStmt.setString(4,s.objectType);
//				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
//				rangeSourcesTimestamps.put(s.getLDSIdentifier(),s.timestamp);
//				pStmt.addBatch();
//			}
//			pStmt.executeBatch();
//			this.dbHandler.closeStatement(pStmt);
//			
//			//InstVersions des Mappings registrieren
//			sqlQuery = this.sql.getStatement(GommaRepositorySQL.IMPORT_MAPPING_INST_VERSION);
//			pStmt = this.dbHandler.prepareStatement(sqlQuery);
//			
//			for (int i=0;i<mapping.instanceDomainSources.size();i++) {
//				ImportSource s = mapping.instanceDomainSources.get(i);
//				pStmt.setInt(1,mapID);
//				pStmt.setInt(2,1);
//				pStmt.setString(3,s.sourceName);
//				pStmt.setString(4,s.objectType);
//				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
//				pStmt.addBatch();
//			}
//			for (int i=0;i<mapping.instanceRangeSources.size();i++) {
//				ImportSource s = mapping.instanceRangeSources.get(i);
//				pStmt.setInt(1,mapID);
//				pStmt.setInt(2,0);
//				pStmt.setString(3,s.sourceName);
//				pStmt.setString(4,s.objectType);
//				pStmt.setDate(5,new Date(s.timestamp.getTime().getTime()));
//				pStmt.addBatch();
//			}
//			pStmt.executeBatch();
//			this.dbHandler.closeStatement(pStmt);
//			
//			//Korrespondenzen importieren
//			sqlQuery = this.sql.getStatement(GommaRepositorySQL.IMPORT_MAPPING_CORRESPONDENCES);
//			pStmt = this.dbHandler.prepareStatement(sqlQuery);
//			
//			for (int i=0;i<mapping.correspondences.size();i++) {
//				ImportCorrespondence c = mapping.correspondences.get(i);
//				pStmt.setInt(1,mapID);
//				pStmt.setInt(2,c.support);
//				pStmt.setFloat(3,c.confidence);
//				pStmt.setInt(4,c.nChecked);
//				pStmt.setString(5,c.domainObjAcc);
//				Calendar domainObjDate = domainSourcesTimestamps.get(c.domainObjectType+"@"+c.domainSourceName);
//				if (domainObjDate!=null) {
//					pStmt.setDate(6,new Date(domainObjDate.getTime().getTime()));
//					pStmt.setString(7,c.rangeObjAcc);
//					Calendar rangeObjDate = rangeSourcesTimestamps.get(c.rangeObjectType+"@"+c.rangeSourceName);
//					if (rangeObjDate!=null) {
//						pStmt.setDate(8,new Date(rangeObjDate.getTime().getTime()));
//						pStmt.setString(9,c.corr_type);
//						pStmt.addBatch();
//					}
//				}
//			}
//			pStmt.executeBatch();
//			this.dbHandler.closeStatement(pStmt);
//			
//			//Anzahl der Domain und Range-Objekte registrieren
//			sqlQuery = "update gomma_maps set number_of_domain_objs = (select count(distinct d_obj_id_fk) from gomma_obj_corrs where map_id_fk = "+mapID+") where map_id = "+mapID;
//			this.dbHandler.executeDml(sqlQuery);
//			sqlQuery = "update gomma_maps set number_of_range_objs = (select count(distinct r_obj_id_fk) from gomma_obj_corrs where map_id_fk = "+mapID+") where map_id = "+mapID;
//			this.dbHandler.executeDml(sqlQuery);
//			
//		} catch (SQLException e) {
//			throw new RepositoryException(e.getMessage());
//		}
	}
}
