package org.gomma.api.mapping;

import java.rmi.RemoteException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.source.Source;

/**
 * The class implements the mapping API. It is the implementation for the 
 * FEVER system using a relational database at the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingAPI_FEVER implements MappingAPI {

	/**
	 * @see MappingAPI#getMappingSet()
	 */
	public MappingSet getMappingSet() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#getMappingSet(Source, Source)
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#getMappingSet(String, QueryMatchOperations)
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#insertMapping(String, Source, Source, boolean, boolean, String, String, String, String)
	 */
	public int insertMapping(String name, Source domainSource,
			Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#insertMappingAttributeValues(Mapping, AttributeValueSet)
	 */
	public int insertMappingAttributeValues(Mapping m, AttributeValueSet set) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#updateMapping(Mapping)
	 */
	public int updateMapping(Mapping m) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see MappingAPI#deleteMapping(Mapping)
	 */
	public int deleteMapping(Mapping m) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingAPI#deleteMappingAttributeValues(Mapping)
	 */
	public int deleteMappingAttributeValues(Mapping m) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
