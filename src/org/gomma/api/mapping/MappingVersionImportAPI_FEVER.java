package org.gomma.api.mapping;

import org.gomma.exceptions.RepositoryException;
import org.gomma.io.importer.models.ImportMapping;

/**
 * The class implements the mapping version import API. It is the 
 * implementation for the FEVER system using a relational database
 * at the back-end.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class MappingVersionImportAPI_FEVER implements MappingVersionImportAPI {

	/**
	 * @see MappingVersionImportAPI#importMapping(ImportMapping)
	 */
	public int importMapping(ImportMapping mappingVersion) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

}
