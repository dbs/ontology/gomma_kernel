package org.gomma.api.mapping;

import java.rmi.RemoteException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The interface defines methods for managing mapping version objects within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingVersionAPI {

	/**
	 * The method returns the complete set of mapping versions for the specified mapping. 
	 * @param mmapping for that the mapping version should be loaded
	 * @return set of mapping versions
	 * @throws RepositoryException
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws RepositoryException;
	
	/**
	 * The method returns a set of mapping versions that are associated with the given 
	 * mapping object are matches the given version name using the query match operator.
	 * @param m mapping for that the mapping versions should be loaded
	 * @param name name pattern of the mapping versions 
	 * @param op name string match operator
	 * @return set of mapping versions
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException;
	
	/**
	 * The method returns a set of mapping versions that are associated with the given mapping object 
	 * and are created between the specified from and to date (including the dates).
	 * @param m mapping for that the mapping versions should be loaded
	 * @param fromDate lowest calendar date
	 * @param toDate highest calendar date
	 * @return set of mapping versions
	 * @throws RepositoryException
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) throws RepositoryException;
	
	/**
	 * The method creates and stores a mapping version object within the repository using the specified 
	 * parameters. It returns the number of created mapping version objects within the repository.  
	 * @param m mapping to that the mapping version belongs 
	 * @param name name of the mapping version
	 * @param creationDate creation date
	 * @param lowestConfidence lowest confidence value in the correspondence set
	 * @param lowestSupport lowest support value in the correspondence set
	 * @param nCorrs number of correspondences
	 * @param nDomainObjs number of domain objects
	 * @param nRangeObjs number of range objects
	 * @return numberof created mapping version objects within the repository
	 * @throws RepositoryException
	 */
	public int insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate,
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs) throws RepositoryException;
	
	/**
	 * The method associates the given mapping version object with the set of source versions and returns
	 * the number of created associations.
	 * @param v mapping version
	 * @param set set of source versions to which the mapping version is associated
	 * @param isDomain true if the association considers the mapping domain; false otherwise
	 * @return number of created associations
	 * @throws RepositoryException
	 */
	public int insertMappingVersionSourceSet(MappingVersion v, SourceVersionSet set, boolean isDomain) throws RepositoryException;
	
	/**
	 * The method updates the specified mapping version within the repository 
	 * and returns the number of updated mapping version in the repository.
	 * @param v mapping version that should be updated
	 * @return number of updated mapping versions in the repository
	 * @throws RepositoryException
	 */
	public int updateMappingVersion(MappingVersion v) throws RepositoryException;
	
	/**
	 * The method deletes the given mapping version object within the repository
	 * and returns the number of deleted objects.
	 * @param v mapping version that should be deleted
	 * @return number of deleted mapping versions in the repository
	 * @throws RepositoryException
	 */
	public int deleteMappingVersion(MappingVersion v) throws RepositoryException;
	
	/**
	 * The method deletes the source version associations for the specified mapping version
	 * and returns the number of affected rows in the repository.
	 * @param v mapping version
	 * @return number of affected rows in the repository
	 * @throws RepositoryException
	 */
	public int deleteMappingVersionSourceSet(MappingVersion v) throws RepositoryException;
}
