package org.gomma.api.mapping;

import java.rmi.RemoteException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the mapping version API for managing mapping version. It is
 * the implementation for the FEVER system using a relational database at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingVersionAPI_FEVER implements MappingVersionAPI {

	

	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping, String, QueryMatchOperations)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m,
			SimplifiedGregorianCalendar fromDate,
			SimplifiedGregorianCalendar toDate) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, int, int, int)
	 */
	public int insertMappingVersion(Mapping m, String name,
			SimplifiedGregorianCalendar creationDate, float lowestConfidence,
			int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#insertMappingVersionSourceSet(MappingVersion, SourceVersionSet, boolean)
	 */
	public int insertMappingVersionSourceSet(MappingVersion v, SourceVersionSet set, boolean isDomain) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#updateMappingVersion(MappingVersion)
	 */
	public int updateMappingVersion(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#deleteMappingVersion(MappingVersion)
	 */
	public int deleteMappingVersion(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingVersionAPI#deleteMappingVersionSourceSet(MappingVersion)
	 */
	public int deleteMappingVersionSourceSet(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
