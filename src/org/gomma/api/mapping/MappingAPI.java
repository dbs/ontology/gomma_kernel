package org.gomma.api.mapping;

import java.rmi.RemoteException;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.source.Source;

/**
 * The interface defines methods for managing mapping objects and 
 * their associated describing attributes. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingAPI {

	/**
	 * The method returns the complete set of mapping objects 
	 * available in the GOMMA repository.
	 * @return set of mapping objects
	 * @throws RepositoryException
	 */
	public MappingSet getMappingSet() throws RepositoryException;
	
	/**
	 * The method returns the set of mapping objects from the repository
	 * that associates objects from the specified domain to the range 
	 * source.
	 * @param domainSource domain source
	 * @param rangeSource range source
	 * @return set of mapping objects associating objects between the 
	 * specified domain and range sources
	 * @throws RepositoryException
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws RepositoryException;
	
	/**
	 * The method returns the set of mapping objects meeting the specified name (considering
	 * the given query match operation). 
	 * @param name mapping name or portion of that
	 * @param op query match operation
	 * @return set of mapping objects
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException;
	
	/**
	 * The method creates a mapping object within the repository using the specified parameters.
	 * It returns the number of created mapping objects within the repository.
	 * @param name mapping name
	 * @param domainSource domain source
	 * @param rangeSource range source
	 * @param isInstanceMapping true if the mapping is an instance mapping; otherwise false
	 * @param isDerived true if the mapping is derived from other data; otherwise false
	 * @param mapClass name of the mapping class
	 * @param mapMethod name of the mapping method
	 * @param mapTool name of the mapping tool
	 * @param mapType name of the mapping type
	 * @return number of created mapping objects within the repository
	 * @throws RepositoryException
	 */
	public int insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType) throws RepositoryException;
	
	/**
	 * The method inserts a set of mapping attribute values for the specified mapping objects.
	 * It returns the number of stored attribute values within the repository.
	 * @param m mapping for which the attribute values are stored
	 * @param set set of attribute values that should be stored
	 * @return number of affected rows within the repository
	 * @throws RepositoryException
	 */
	public int insertMappingAttributeValues(Mapping m, AttributeValueSet set) throws RepositoryException;
	
	/**
	 * The method updates the given mapping object within the repository and returns the
	 * number of affected rows.
	 * @param m mapping that should be updated
	 * @return number of affected rows within the repository
	 * @throws RepositoryException
	 */
	public int updateMapping(Mapping m) throws RepositoryException;
	
	/**
	 * The method deletes the specified mapping object from the repository and returns
	 * the number of affected rows in the repository
	 * @param m mapping that should be deleted
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteMapping(Mapping m) throws RepositoryException;
	
	/**
	 * The method deletes all attribute values for the specified mapping object and returns
	 * the number of affected rows (could be 0 if the mapping is not further described
	 * by attribute values). 
	 * @param m mapping for those the attribute values should be deleted
	 * @return number of affected rows in the repository
	 * @throws RepositoryException
	 */
	public int deleteMappingAttributeValues(Mapping m) throws RepositoryException;
}
