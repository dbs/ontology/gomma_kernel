package org.gomma.api.mapping;

import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.gomma.api.APIFactory;
import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the mapping version API for managing mapping version. It is
 * the default implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingVersionAPI_RDBMS implements MappingVersionAPI {

	/**
	 * The constant represents the MySQL statement for loading a set of mapping versions.
	 */
	private static final String LOAD_MAPPING_VERSION_SET = 
		"select map_version_id,name,creation_date,"+
			"min_confidence,min_support,num_corrs,num_d_objs,num_r_objs "+
		"from gomma_map_versions where map_id_fk=[mapID]";
	
	/**
	 * The constant represents the MySQL statement for loading source version - mapping version associations.
	 */
	private static final String LOAD_MAPPING_VERSION_SOURCES = 
		"select map_version_id_fk,lds_version_id_fk,is_domain_lds from gomma_map_src_versions where map_version_id_fk in ([mapVersionIDList])";
	
	/**
	 * The constant represents the MySQL statement for inserting a mapping version.
	 */
	private static final String INSERT_MAPPING_VERSION =
		"insert into gomma_map_versions ("+
			"map_id_fk,name,creation_date,min_confidence,min_support,num_corrs,num_d_objs,num_r_objs) "+
		"values ([mapID],?,?,?,?,?,?,?)";
	
	/**
	 * The constant represents the MySQL statement for inserting associations among source versions and mapping versions.
	 */
	private static final String INSERT_MAPPING_VERSION_SOURCES =
		"insert into gomma_map_src_versions (map_version_id_fk,lds_version_id_fk,is_domain_lds) "+
		"values ([mapVersionID],?,?)";
	
	/**
	 * The constant represents the MySQL statement for updating a mapping version.
	 */
	private static final String UPDATE_MAPPING_VERSION =
		"update gomma_map_versions "+
		"set map_id_fk=?, name=?, creation_date=?,min_confidence=?,min_support=?,num_corrs=?,num_d_objs=?,num_r_objs=? "+
		"where map_version_id=[mapVersionID]";
	
	/**
	 * The constant represents the MySQL statement for deleting a mapping version.
	 */
	private static final String DELETE_MAPPING_VERSION = 
		"delete from gomma_map_versions where map_version_id=[mapVersionID]";
	
	/**
	 * The constant represents the MySQL statement for deleting source version associations for mapping version. 
	 */
	private static final String DELETE_MAPPING_VERSION_SOURCES = 
		"delete from gomma_map_src_versions where map_version_id=[mapVersionID]";
	
	/**
	 * Constructor of the class.
	 */
	public MappingVersionAPI_RDBMS() {}
	
	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m) throws RepositoryException {
		return this.loadMappingVersionSet(m,"");
	}
	
	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping, String, QueryMatchOperations)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, String name, QueryMatchOperations op) throws GommaException, RemoteException {
		String queryCond = " and name ";
		name = name.replaceAll("'","");
		
		switch (op) {
			case EXACT_MATCH: queryCond +="= '"+name+"'"; break;
			case STARTS_WITH: queryCond +="like '"+name+"%'"; break;
			case ENDS_WITH:   queryCond +="like '%"+name+"'"; break;
			case CONTAINS:    queryCond +="like '%"+name+"%"; break;
		}
		
		return this.loadMappingVersionSet(m, queryCond);
	}
	
	/**
	 * @see MappingVersionAPI#getMappingVersionSet(Mapping, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public MappingVersionSet getMappingVersionSet(Mapping m, SimplifiedGregorianCalendar fromDate, SimplifiedGregorianCalendar toDate) throws RepositoryException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return this.loadMappingVersionSet(m," and creation_date between '"+df.format(fromDate.getTime())+"' and '"+df.format(toDate.getTime())+"'");
	}
	
	/**
	 * The method loads a set of mapping versions for the given mapping object and the specified query conditions. 
	 * @param m mapping
	 * @param queryCond query condition
	 * @return set of mapping versions
	 * @throws RepositoryException
	 */
	private MappingVersionSet loadMappingVersionSet(Mapping m, String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		String query = LOAD_MAPPING_VERSION_SET.replaceAll("\\[mapID\\]",Integer.toString(m.getID()))+queryCond;
		
		ResultSet rs = dbh.executeSelect(query);
		MappingVersionSet set = this.extractMappingVersionSet(rs,m);
		dbh.closeStatement(rs);
		
		if (set.size()==0) return set;
		
		query = LOAD_MAPPING_VERSION_SOURCES.replaceAll("\\[mapVersionIDList\\]",set.getSeparatedIDString(","));
		rs = dbh.executeSelect(query);
		set = this.extractMappingVersionSources(rs, set,
			APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	/**
	 * @see MappingVersionAPI#insertMappingVersion(Mapping, String, SimplifiedGregorianCalendar, float, int, int, int, int)
	 */
	public int insertMappingVersion(Mapping m, String name, SimplifiedGregorianCalendar creationDate, 
			float lowestConfidence, int lowestSupport, int nCorrs, int nDomainObjs, int nRangeObjs) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_MAPPING_VERSION.replaceAll("\\[mapID\\]",Integer.toString(m.getID()));
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,name.replaceAll("'",""));
			pStmt.setTimestamp(2,new java.sql.Timestamp(creationDate.getTimeInMillis()));
			pStmt.setFloat(3,lowestConfidence);
			pStmt.setInt(4,lowestSupport);
			pStmt.setInt(5,nCorrs);
			pStmt.setInt(6,nDomainObjs);
			pStmt.setInt(7,nRangeObjs);
			
			pStmt.execute();
			affectedRows = pStmt.getUpdateCount();
		} catch (SQLException e) {
		 	throw new RepositoryException("insertMappingVersion(): "+e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	/**
	 * @see MappingVersionAPI#insertMappingVersionSourceSet(MappingVersion, SourceVersionSet, boolean)
	 */
	public int insertMappingVersionSourceSet(MappingVersion v, SourceVersionSet set, boolean isDomain) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_MAPPING_VERSION_SOURCES.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()));
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			for (SourceVersion sv : set) {
				pStmt.setInt(1,sv.getID());
				pStmt.setInt(2,isDomain?1:0);
				pStmt.addBatch();
			}
			
			pStmt.executeBatch();
			affectedRows = pStmt.getUpdateCount();
		} catch (SQLException e) {
		 	throw new RepositoryException("insertMappingVersionSourceSet(): "+e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	/**
	 * @see MappingVersionAPI#updateMappingVersion(MappingVersion)
	 */
	public int updateMappingVersion(MappingVersion v) throws RepositoryException {
	
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = UPDATE_MAPPING_VERSION.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()));
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setInt(1,v.getMapping().getID());
			pStmt.setString(2,v.getName().replaceAll("'",""));
			pStmt.setTimestamp(3,new java.sql.Timestamp(v.getCreationDate().getTimeInMillis()));
			pStmt.setFloat(4,v.getLowestConfidenceValue());
			pStmt.setInt(5,v.getLowestSupportValue());
			pStmt.setInt(6,v.getNumberOfCorrespondences());
			pStmt.setInt(7,v.getNumberOfDomainObjects());
			pStmt.setInt(8,v.getNumberOfRangeObjects());
			
			pStmt.execute();
			affectedRows = pStmt.getUpdateCount();
		} catch (SQLException e) {
		 	throw new RepositoryException("updateMappingVersion(): "+e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	/**
	 * @see MappingVersionAPI#deleteMappingVersion(MappingVersion)
	 */
	public int deleteMappingVersion(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MAPPING_VERSION.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * @see MappingVersionAPI#deleteMappingVersionSourceSet(MappingVersion)
	 */
	public int deleteMappingVersionSourceSet(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MAPPING_VERSION_SOURCES.replaceAll("\\[mapVersionID\\]",Integer.toString(v.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * The method extracts and returns a set of mapping version from the given SQL query result set. 
	 * @param rs query result set
	 * @param m mapping
	 * @return set of mapping versions
	 * @throws RepositoryException
	 */
	private MappingVersionSet extractMappingVersionSet(ResultSet rs, Mapping m) throws RepositoryException {
		MappingVersionSet set = new MappingVersionSet();
		MappingVersion v;
		
		try {
			while (rs.next()) {
				v = new MappingVersion.Builder(rs.getInt(1), m)
						.name(rs.getString(2))
						.creationDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getTimestamp(3).getTime()))
						.lowestConfidence(rs.getFloat(4))
						.lowestSupport(rs.getInt(5))
						.numberOfCorrespondences(rs.getInt(6))
						.numberOfDomainObjects(rs.getInt(7))
						.numberOfRangeObjects(rs.getInt(8))
						.build();

				set.addMappingVersion(v);
			}
		} catch (SQLException e) {
			throw new RepositoryException("loadMappingVersionSet():"+e.getMessage());
		}
		
		return set;
	}
	
	/**
	 * The method extracts and associates a set of source versions from a given SQL query result set. 
	 * It returns the set of mapping versions.
	 * @param rs query result set
	 * @param set set of mapping versions to which the source versions should be associated
	 * @param sourceVersionSet set of source version that should be associated
	 * @return set of mapping versions
	 * @throws RepositoryException
	 */
	private MappingVersionSet extractMappingVersionSources(ResultSet rs, MappingVersionSet set, SourceVersionSet sourceVersionSet) throws RepositoryException {
		MappingVersion mv;
		SourceVersion sv;
		
		try {
			while (rs.next()) {
				if (!set.contains(rs.getInt(1))) continue;
				if (!sourceVersionSet.contains(rs.getInt(2))) continue;
				
				mv = set.getMappingVersion(rs.getInt(1));
				sv = sourceVersionSet.getSourceVersion(rs.getInt(2));
				
				if (rs.getInt(3)==1) // is domain source ?
					mv.addDomainSourceVersion(sv);
				else mv.addRangeSourceVersion(sv);
				
				set.addMappingVersion(mv);
			}
		} catch (SQLException e) {
			throw new RepositoryException("loadMappingVersionSources(): "+e.getMessage());
		}
		
		return set;
	}
}
