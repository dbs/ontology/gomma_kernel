package org.gomma.api.mapping;

import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.APIFactory;
import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.DataTypes;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeValue;
import org.gomma.model.attribute.AttributeValueFactory;
import org.gomma.model.attribute.AttributeValueFloat;
import org.gomma.model.attribute.AttributeValueInteger;
import org.gomma.model.attribute.AttributeValueSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;

/**
 * The class implements the mapping API. It is the default implementation for a relational database. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingAPI_RDBMS implements MappingAPI {

	/**
	 * The constant represents the MySQL statement for loading a set of mappings.
	 */
	private static final String LOAD_MAPPING_SET = 
		"select a.map_id, a.d_lds_id_fk,a.r_lds_id_fk,"+
			"a.name, a.is_inst_map, a.is_derived, "+
			"c.name, c.descr, d.name, d.descr, e.name, e.descr, b.name, b.descr "+
		"from gomma_maps a, gomma_map_types b, gomma_map_classes c, gomma_map_methods d, gomma_map_tools e "+
		"where b.map_type_id=a.map_type_id_fk "+
		  "and c.map_class_id=a.map_class_id_fk "+
		  "and d.map_method_id = a.map_method_id_fk "+
		  "and e.map_tool_id = a.map_tool_id_fk";
	
	/**
	 * The constant represents the MySQL statement for loading a set of mapping attribute values
	 */
	private static final String LOAD_MAPPING_ATTRIBUTE_VALUES = 
		"select map_id_fk, att_id, att_name, att_scope, data_type_id_fk, "+
			"att_value_id, att_value_f, att_value_i, att_value_s "+ 
		"from gomma_map_att_values a, gomma_attributes b "+
		"where a.att_id_fk=b.att_id "+
		  "and a.map_id_fk in ([mapIdList])";
	
	/**
	 * The constant represents the MySQL statement for inserting a mapping object.
	 */
	private static final String INSERT_MAPPING =
		"insert into gomma_maps (name,is_inst_map,is_derived,"+
			"d_lds_id_fk,r_lds_id_fk,"+
			"map_class_id_fk,map_method_id_fk,map_tool_id_fk,map_type_id_fk) "+
		"select ?,?,?,?,?,map_class_id,map_method_id,map_tool_id,map_type_id "+
		"from gomma_map_classes a, gomma_map_methods b, gomma_map_tools c, gomma_map_types d "+
		"where a.name=? "+
		  "and b.name=? "+
		  "and c.name=? "+
		  "and d.name=?";
	
	/**
	 * The constant represents the MySQL statement for inserting a mapping attribute value.
	 */
	private static final String INSERT_ATTRIBUTE_VALUE_STRING = 
		"insert into gomma_map_att_values (map_id_fk,att_id_fk,value_s) values (?,?,?)";
	
	/**
	 * The constant represents the MySQL statement for updating a mapping object.
	 */
	private static final String UPDATE_MAPPING = 
		"update gomma_maps "+
		"set name=?, is_inst_map=?, is_derived=?,"+
		"d_lds_id_fk=?, r_lds_id_fk=?,"+
		"map_class_id_fk=(select map_class_id from gomma_map_classes where name=?), "+
		"map_method_id_fk=(select map_method_id from gomma_map_methods where name=?), "+
		"map_tool_id_fk=(select map_tool_id from gomma_map_tools where name=?), "+
		"map_type_id_fk=(select map_type_id from gomma_map_types where name=?) "+
		"where map_id=?";
	
	/**
	 * The constant represents the MySQL statement for inserting a mapping integer attribute value.
	 */
	private static final String INSERT_ATTRIBUTE_VALUE_INTEGER = 
		"insert into gomma_map_att_values (map_id_fk,att_id_fk,value_s,value_i) values (?,?,?,?)";
	
	/**
	 * The constant represents the MySQL statement for inserting a mapping float attribute value.
	 */
	private static final String INSERT_ATTRIBUTE_VALUE_FLOAT =
		"insert into gomma_map_att_values (map_id_fk,att_id_fk,value_s,value_f) values (?,?,?,?)";
	
	/**
	 * The constant represents the MySQL statement for deleting a specified mapping object.
	 */
	private static final String DELETE_MAPPING =
		"delete from gomma_maps where map_id=[mapID]";
	
	/**
	 * The constant represents the MySQL statement for deleting all associated attribute values of a specified mapping.
	 */
	private static final String DELETE_MAPPING_ATTRIBUTE_VALUES = 
		"delete from gomma_map_att_values where map_id_fk=[mapID]";
	
	/**
	 * Constructor of the class.
	 */
	public MappingAPI_RDBMS() {}
	
	
	/**
	 * @see MappingAPI#getMappingSet()
	 */
	public MappingSet getMappingSet() throws RepositoryException {
		System.out.println("here");
		return this.loadMappingSet("");
	}
	
	/**
	 * @see MappingAPI#getMappingSet(String, QueryMatchOperations)
	 */
	public MappingSet getMappingSet(String name, QueryMatchOperations op) throws GommaException, RemoteException {
		String queryCond = " and a.name ";
		name = name.replaceAll("'","");
		
		switch (op) {
			case EXACT_MATCH: queryCond +="= '"+name+"'"; break;
			case STARTS_WITH: queryCond +="like '"+name+"%'"; break;
			case ENDS_WITH:   queryCond +="like '%"+name+"'"; break;
			case CONTAINS:    queryCond +="like '%"+name+"%'"; break;
		}
		
		return this.loadMappingSet(queryCond);
	}
	/**
	 * @see MappingAPI#getMappingSet(Source, Source)
	 */
	public MappingSet getMappingSet(Source domainSource, Source rangeSource) throws RepositoryException {
		return this.loadMappingSet(" and d_lds_id_fk="+domainSource.getID()+" and r_lds_id_fk="+rangeSource.getID());
	}
	
	/**
	 * The method returns a set of mappings meeting the specified conditions.
	 * @param queryCond query condition
	 * @return set of mappings
	 * @throws RepositoryException
	 */
	private MappingSet loadMappingSet(String queryCond) throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_MAPPING_SET+queryCond;
		ResultSet rs = null;
		
		// load core mapping set
		SourceSet sourceSet = APIFactory.getInstance().getSourceAPI().getSourceSet();
		rs = dbh.executeSelect(query);
		MappingSet set = this.extractMappingMetadata(rs,sourceSet);
		dbh.closeStatement(rs);
			
		if (set.size()==0) return set;
			
		// load mapping attributes (details)
		query = LOAD_MAPPING_ATTRIBUTE_VALUES.replaceAll("\\[mapIdList\\]",set.getSeparatedIDString(","));
		rs = dbh.executeSelect(query);
		set = this.extractMappingAttributes(rs,set);
		dbh.closeStatement(rs);
		
		return set;
	}

	

	/**
	 * @see MappingAPI#insertMapping(String, Source, Source, boolean, boolean, String, String, String, String)
	 */
	public int insertMapping(String name, Source domainSource, Source rangeSource, boolean isInstanceMapping, boolean isDerived,
			String mapClass, String mapMethod, String mapTool, String mapType) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_MAPPING;
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,name.replaceAll("'",""));
			pStmt.setInt(2,isInstanceMapping?1:0);
			pStmt.setInt(3,isDerived?1:0);
			pStmt.setInt(4,domainSource.getID());
			pStmt.setInt(5,rangeSource.getID());
			pStmt.setString(6,mapClass);
			pStmt.setString(7,mapMethod);
			pStmt.setString(8,mapTool);
			pStmt.setString(9,mapType);
			 
			pStmt.execute();
			affectedRows = pStmt.getUpdateCount();
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}

	
	/**
	 * @see MappingAPI#updateMapping(Mapping)
	 */
	public int updateMapping(Mapping m) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = UPDATE_MAPPING;
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			pStmt = dbh.prepareStatement(query);
		
			pStmt.setString(1,m.getName().replaceAll("'",""));
			pStmt.setInt(2,m.isInstanceMapping()?1:0);
			pStmt.setInt(3,m.isDerived()?1:0);
			pStmt.setInt(4,m.getDomainSource().getID());
			pStmt.setInt(5,m.getRangeSource().getID());
			pStmt.setString(6,m.getMappingClassName());
			pStmt.setString(7,m.getMappingMethodName());
			pStmt.setString(8,m.getMappingToolName());
			pStmt.setString(9,m.getMappingTypeName());
			pStmt.setInt(10, m.getID());
			
			pStmt.execute();
			
			affectedRows = pStmt.getUpdateCount();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	
	/**
	 * @see MappingAPI#insertMappingAttributeValues(Mapping, AttributeValueSet)
	 */
	public int insertMappingAttributeValues(Mapping m, AttributeValueSet set) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String querySValue = INSERT_ATTRIBUTE_VALUE_STRING,
			   queryIValue = INSERT_ATTRIBUTE_VALUE_INTEGER,
			   queryFValue = INSERT_ATTRIBUTE_VALUE_FLOAT;
		
		PreparedStatement pStmtSValue = null, pStmtIValue = null, pStmtFValue = null;
		int affectedRows = 0;
		
		try {
			pStmtSValue = dbh.prepareStatement(querySValue);
			pStmtIValue = dbh.prepareStatement(queryIValue);
			pStmtFValue = dbh.prepareStatement(queryFValue);
			
			for (AttributeValue av : set) {
				switch (av.getAttribute().getDataType()) {
					case INTEGER:
						pStmtIValue.setInt(1,m.getID());
						pStmtIValue.setInt(2,av.getAttribute().getID());
						pStmtIValue.setString(3,av.toString());
						pStmtIValue.setInt(4,((AttributeValueInteger)av).getValue());
						pStmtIValue.addBatch();
						break;
					case FLOAT:
						pStmtFValue.setInt(1,m.getID());
						pStmtFValue.setInt(2,av.getAttribute().getID());
						pStmtFValue.setString(3,av.toString());
						pStmtFValue.setFloat(4,((AttributeValueFloat)av).getValue());
						pStmtFValue.addBatch();
						break;
					default:
						pStmtSValue.setInt(1,m.getID());
						pStmtSValue.setInt(2,av.getAttribute().getID());
						pStmtSValue.setString(3,av.toString());
						pStmtSValue.addBatch();
						break;
				}
			}
			pStmtIValue.executeBatch();
			pStmtFValue.executeBatch();
			pStmtSValue.executeBatch();
			
			affectedRows = pStmtIValue.getUpdateCount() + pStmtIValue.getUpdateCount() + pStmtFValue.getUpdateCount();
		
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmtIValue!=null) dbh.closeStatement(pStmtIValue);
			if (pStmtFValue!=null) dbh.closeStatement(pStmtFValue);
			if (pStmtSValue!=null) dbh.closeStatement(pStmtSValue);
		}
		
		return affectedRows;
	}
	
	/**
	 * @see MappingAPI#deleteMapping(Mapping)
	 */
	public int deleteMapping(Mapping m) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MAPPING.replaceAll("\\[mapID\\]",Integer.toString(m.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * @see MappingAPI#deleteMappingAttributeValues(Mapping)
	 */
	public int deleteMappingAttributeValues(Mapping m) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MAPPING_ATTRIBUTE_VALUES.replaceAll("\\[mapID\\]",Integer.toString(m.getID()));
		
		return dbh.executeDml(query);
	}
	
	
	
	
//	/**
//	 * The method extends the given SQL query by conditions specified in the query parameter.
//	 * The conditions needs to be transformed such that the table attributes are used.
//	 * @param sqlQuery SQL query to be extended
//	 * @param query further query conditions which should be added to the sql query
//	 * @return extended SQL query
//	 */
//	private static String rewriteMappingQuery(String sqlQuery, String query) {
//		if (query.trim().length()==0) return sqlQuery;
//		// replace the mapping name attribute
//		query = query.replaceAll("\\[name\\]","a.name");
//		query = query.replaceAll("\\[mapName\\]","a.name");
//		query = query.replaceAll("\\[mappingName\\]","a.name");
//		// replace the "is instance mapping" attribute
//		query = query.replaceAll("\\[isInstanceMapping\\]( )*=( )*('|\")?(true|yes)('|\")?","is_inst_map=1");
//		query = query.replaceAll("\\[isInstanceMapping\\]( )*=( )*('|\")?(false|no)('|\")?","is_inst_map=0");
//		// replace domain and range lds
//		Pattern p;
//		Matcher m;
//		String matchPattern, matchStr, matchResult;
//		for (String lds : new String[]{"\\[domainLDS\\]","\\[rangeLDS\\]","\\[domainInstLDS\\]","\\[rangeInstLDS\\]"}) {
//			matchPattern=lds+"\\s*(=\\s*\\d+|\\s+in\\s+\\(((\\d),)*\\d+\\))";
//			p = Pattern.compile(matchPattern);
////			m = p.matcher(query);
//			while ((m=p.matcher(query)).find()) {
//				matchStr = query.substring(m.start(),m.end());
//				matchResult = "exists (select * from gomma_lds_versions z1, "+
//									  ((lds.equals("\\[domainLDS\\]")||lds.equals("\\[rangeLDS\\]"))?"gomma_map_src_versions z2 ":"gomma_map_inst_versions z2 ")+
//									  "where z2.map_id_fk=map_id "+
//									    "and z1.lds_version_id=z2.lds_version_id_fk "+
//									    "and z1.lds_id_fk"+
//									  (matchStr.contains("=")?"="+matchStr.split("=")[1]:matchStr.replaceAll(lds,""))+" "+
//									  "and is_domain_lds="+(lds.startsWith("\\[domain")?1:0)+") ";
//				query = query.replaceFirst(matchPattern,matchResult);
//			}
//		}
//		// replace further conditions on generic attribute 
//		p = Pattern.compile("\\[(\\p{Alpha})+\\]");
//		while ((m = p.matcher(query)).find()) {
//			matchStr =  query.substring(m.start(),m.end());
//			query = query.replaceFirst("\\[(\\p{Alpha})+\\]","attr_name ='"+matchStr+"' and attr_value ");
//		}
//		return sqlQuery+" and ("+query+")";
//	}
	
//	/**
//	 * The method extends the given SQl query such that relevant attributes are additionally 
//	 * queried by the specified keywords. 
//	 * @param sqlQuery originally SQl query
//	 * @param keywords string of delimited keywords
//	 * @return extended SQl query
//	 */
//	private static String rewriteMappingSearch(String sqlQuery, String keywords) {
//		if (keywords.trim().length()==0) return sqlQuery;
//		String[] keywordArray=extractKeywords(keywords);
//		StringBuffer queryExtension = new StringBuffer();
//		// checking mapping name
//		for (String kw : keywordArray)
//			queryExtension.append(LOWER_FUNCTION_NAME).append("(map_name) like '%")
//			              .append(kw.toLowerCase()).append("%' ")
//			              .append(" or ");
//		// checking mapping attributes
//		for (int i=0;i<keywordArray.length;i++) {
//			queryExtension.append(LOWER_FUNCTION_NAME).append("(attr_value) like '%")
//			              .append(keywordArray[i].toLowerCase()).append("%' ");
//			if (i+1<keywordArray.length) queryExtension.append(" or ");
//		}
//		return sqlQuery+" and ("+queryExtension.append(")").toString();
//	}
	

	
	
	
	
	
	/**
	 * The methods extracts mapping metadata from the given SQL result set.
	 * @param rs SQl result set
	 * @return set of mappings
	 * @see org.gomma.model.mapping.MappingSet
	 * @exception SQLException
	 */
	private MappingSet extractMappingMetadata(ResultSet rs, SourceSet sourceSet) throws RepositoryException {
		MappingSet set = new MappingSet();
		Mapping m;
		Source domainLDS, rangeLDS;
		
		try {
			while (rs.next()) {
				domainLDS = sourceSet.getSource(rs.getInt(2));
				rangeLDS  = sourceSet.getSource(rs.getInt(3));
				
				m = new Mapping.Builder(rs.getInt(1),domainLDS,rangeLDS)
					.name(rs.getString(4))
					.isDerived(rs.getInt(5)==1)
					.isInstanceMapping(rs.getInt(6)==1)
					.mappingClassName(rs.getString(7))
					.mappingClassDescription(rs.getString(8))
					.mappingMethodName(rs.getString(9))
					.mappingMethodDescription(rs.getString(10))
					.mappingToolName(rs.getString(11))
					.mappingToolDescription(rs.getString(12))
					.mappingTypeName(rs.getString(13))
					.mappingTypeDescription(rs.getString(14)).build();
				
				set.addMapping(m);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}

	/**
	 * The methods extracts generically stored attributes describing mappings from the 
	 * given SQL result set. 
	 * @param rs SQL result set
	 * @param maps set of mappings those description should be extended
	 * @return set of mappings
	 * @throws SQLException
	 */
	private MappingSet extractMappingAttributes(ResultSet rs, MappingSet set) throws RepositoryException {
		Mapping m;
		Attribute att = null;
		AttributeValue av = null;
		
		try {
			while (rs.next()) {
		
				if (!set.contains(rs.getInt(1))) continue;
			
				m = set.getMapping(rs.getInt(1));
			
				att = CacheManager.getInstance().getAttributeCache().getAttribute(
						rs.getInt(2),rs.getString(3),rs.getString(4), DataTypes.resolveDataType(rs.getInt(5)));
			
				switch (att.getDataType()) {
					case FLOAT:
						av = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								rs.getFloat(7));
						break;
					case INTEGER: 
						av = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								rs.getInt(8));
						break;
					case STRING: 
						av = AttributeValueFactory.getValue(
								rs.getInt(6), 
								att, 
								rs.getString(9));
						break;
					default: 
				}
			
				m.addAttrbuteValue(av);
				set.addMapping(m);
			}
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	


}
