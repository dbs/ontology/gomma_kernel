package org.gomma.api.util;

import org.gomma.util.SystemPropertyPool;

/**
 * The class is factory creating validation query for the currently 
 * used data source system (repository).
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class ValidationQueryFactory {

	/**
	 * The constructor initializes the class.
	 */
	private ValidationQueryFactory() {}
	
	/**
	 * The method returns the validation query implementation for the currently
	 * used data source system (repository).
	 * @return validation query implementation
	 */
	public static ValidationQuery getValidationQuery() {
		switch (SystemPropertyPool.getInstance().getDataSourceSystem()) {
			case MYSQL: return new ValidationQuery_MySQL();
			case FEVER: return new ValidationQuery_FEVER();
			default: return new ValidationQuery_MySQL();
		}
	}
}
