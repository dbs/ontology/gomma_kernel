/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * @author: Toralf Kirsten 
 * e-mail: tkirsten@izbi.uni-leipzig.de 
 * institution: Interdisciplinary Centre for Bioinformatics, University of Leipzig 
 * @version: 1.0 
 * @date: 2005/01/03
 *
 * changes: --
 */
package org.gomma.api.util;

/**
 * The class represents the connection data for relational database.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DatabaseConnectionData extends Object {
	/**
	 * represents the jdbc driver class name
	 */
	private String dbDriver;
	/**
	 * represents the database url to be used to establish
	 * the database connection
	 */
	private String dbUrl;
	/**
	 * represents the database user to be used
	 * to establish the database connection
	 */
	private String dbUser;
	/**
	 * represents the password of the database user to be used
	 * to establish the database connection
	 */
	private String	dbPw;

	/**
	 * constructor of the class
	 * @exception none
	 **/
	public DatabaseConnectionData() {
		this.reset();
	}
	/**
	 * constructor of the class
	 * @param driver class name of the jdbc driver
	 * @param url url to be used to establish a database connection
	 * @param user database user to be used to establish a database connection
	 * @param pw password of the database user to be used to establish a
	 * database connection
	 * @exception none
	 **/
	public DatabaseConnectionData(String driver, String url, String user, String pw) {
		this.dbDriver = driver;
		this.dbUrl = url;
		this.dbUser = user;
		this.dbPw = pw;
	}
	/**
	 * returns the class name of the jdbc driver
	 * @return driver class name of the jdbc driver
	 * @exception none
	 **/
	public String getDatabaseDriver() {
		return (this.dbDriver);
	}
	/**
	 * returns the url
	 * @return url url to be used to establish a database connection
	 * @exception none
	 **/
	public String getDatabaseUrl() {
		return (this.dbUrl);
	}
	/**
	 * returns the database user
	 * @return user database user to be used to establish a database connection
	 * @exception none
	 **/
	public String getDatabaseUser() {
		return (this.dbUser);
	}
	/**
	 * returns the password of the database user
	 * @return pw password of the database user to be used to establish a
	 * database connection
	 * @exception none
	 **/
	public String getDatabasePw() {
		return (this.dbPw);
	}
	/**
	 * resets the driver, the url, the database user and its password to default values
	 * driver = ""
	 * url = ""
	 * user = ""
	 * password = ""
	 * @exception none
	 **/
	public void reset() {
		this.dbDriver = this.dbUrl = this.dbUser = this.dbPw = "";
	}
	/**
	 * sets the class name of the jdbc driver
	 * @param driver class name of the jdbc driver
	 * @exception none
	 **/
	public void setDatabaseDriver(String driver) {
		this.dbDriver = driver;
	}
	/**
	 * sets the url
	 * @param url url to be used to establish a database connection
	 * @exception none
	 **/
	public void setDatabaseUrl(String url) {
		this.dbUrl = url;
	}
	/**
	 * sets the database user
	 * @param user database user to be used to establish a database connection
	 * @exception none
	 **/
	public void setDatabaseUser(String user) {
		this.dbUser = user;
	}
	/**
	 * sets the password of the database user
	 * @param pw password of the database user to be used to establish a
	 * database connection
	 * @exception none
	 **/
	public void setDatabasePw(String pw) {
		this.dbPw = pw;
	}
	/**
	 * prints out the objects; just for temporarily usage
	 * @exception none
	 **/
	public void print() {
		System.out.println("Database connection data");
		System.out.println("************************");
		System.out.println("Driver="+this.getDatabaseDriver());
		System.out.println("Url="+this.getDatabaseUrl());
		System.out.println("User="+this.getDatabaseUser());
		System.out.println("Password="+this.getDatabasePw());
	}
}
