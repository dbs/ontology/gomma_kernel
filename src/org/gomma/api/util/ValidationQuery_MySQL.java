package org.gomma.api.util;

/**
 * The class implements the validation query interface to provide 
 * a validation query for a relational MySQL database. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class ValidationQuery_MySQL implements ValidationQuery {

	/**
	 * @see ValidationQuery#getQuery()
	 */
	public String getQuery() {
		return "select 1";
	}
}
