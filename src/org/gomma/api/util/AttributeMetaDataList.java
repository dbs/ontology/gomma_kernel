
package org.gomma.api.util;

import java.util.ArrayList;

public class AttributeMetaDataList extends ArrayList<AttributeMetaData> {
	
	/**
	 * -----------------------------------------------------
	 *          i n t e r n a l   d a t a   e l e m e n t s
	 * -----------------------------------------------------
	 */
	/**
	 * version id
	 */
	public static final long serialVersionUID =1;
	
	/**
	 * constructor of the class
	 *
	 */
	public AttributeMetaDataList() {
		super();
	}
	/**
	 * adds the specified attribute and its corresponding metadata to the list
	 * @param schemaName name of the schema
	 * @param tableName name of the table
	 * @param attributeName name of the attribute
	 * @param dataType integer representation of the data type - see java.sql.Types
	 * @param typeName name of the data type
	 * @param position position of the attribute in the table 
	 */
	public void add(String schemaName, String tableName, String attributeName, 
			int dataType, String typeName, int position) {
		AttributeMetaData element = 
			new AttributeMetaData(schemaName,tableName,attributeName,dataType,typeName,position);
		this.add(element);
	}
	/**
	 * returns the attribute metadata from the list at the specified position
	 * @param i position in the list
	 * @return
	 */
	public AttributeMetaData getAttribute(int i) {
		return(super.get(i));
	}
}
