/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the OMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * @author:     Toralf Kirsten
 * e-mail:      tkirsten@izbi.uni-leipzig.de
 * institution: Interdisciplinary Centre for Bioinformatics,
 *              University of Leipzig
 * @version:    1.0
 * date:        2005/01/03
 *
 * changes:     --
 * 
 **/
package org.gomma.api.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.gomma.exceptions.RepositoryException;

/**
 * The class manages the database connection and executes queries.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class DatabaseHandler {

	/**
	 * -----------------------------------------------------
	 *          i n t e r n a l   d a t a   e l e m e n t s
	 * -----------------------------------------------------
	 */

	/**
	 * represents the database connection
	 */
	public DataSource dataSource = null;
	
	/**
	 * represents the database connection data, i.e. JDBC driver, URL etc.
	 */
	private DatabaseConnectionData dbConData = null;

	private volatile static DatabaseHandler singleton = null;
	
	
	public static DatabaseHandler getInstance() {
		if (singleton == null) {
			synchronized(DatabaseHandler.class) {
				if (singleton==null)
					singleton = new DatabaseHandler();
			}
		}
		return singleton;
	}
	
	public static void stop() {
		singleton = null;
	}
	
	
	/**
	 * constructor of the class
	 */
	private DatabaseHandler() {}
	
	
	/**
	 * -----------------------------------------------------
	 *          p u b l i c   m e t h o d s
	 * -----------------------------------------------------
	 */

	/**
	 * returns the database connection data
	 * @return DatabaseConnectionData
	 * @exception none
	 */
	public DatabaseConnectionData getDatabaseConnectionData() {
		return(this.dbConData);
	}
	
	public void setDatabaseConnectionData(DatabaseConnectionData data) {
		this.dbConData = data;
	}
	
	/**
	 * returns the database connection
	 * @return Connection
	 * @throws SQLException 
	 * @exception none
	 */
	public synchronized Connection getDatabaseConnection() throws SQLException {
		return(this.dataSource.getConnection());
	}
	/**
	 * creates a database connection to the specified database
	 * @exception SQLException
	 * @exception ClassNotFoundException
	 * @exception InstantiationException
	 * @exception IllegalAccessException
	 */
	public void createDatabaseConnection()
	throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Class.forName(this.dbConData.getDatabaseDriver()).newInstance();
		
		GenericObjectPool.Config config = new GenericObjectPool.Config();
		config.testOnBorrow = true;
		config.maxActive = -1;
		
		ObjectPool connectionPool = new GenericObjectPool(null,config);
		
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
				this.dbConData.getDatabaseUrl(),
				this.dbConData.getDatabaseUser(),
				this.dbConData.getDatabasePw());
		new PoolableConnectionFactory(
				connectionFactory,connectionPool,null,ValidationQueryFactory.getValidationQuery().getQuery(),false,true);
		
		this.dataSource = new PoolingDataSource(connectionPool);
	}
	/**
	 * closes a database connection to the specified database
	 */
	public void closeDatabaseConnection() {
		this.dataSource=null;
	}
	/**
	 * checks whether the database connection is timed out / closed or alive
	 * returns true if the connection is currently alive;
	 * returns false otherwise
	 * @return true/false
	 * @exception SQLException
	 */
	public synchronized boolean isDbConnectionsAlive() throws SQLException {	
		return this.dataSource!=null;
	}
	/**
	 * executes a DML database statement and returns the number of affected tupels
	 * @param dmlQuery DML SQL query to be executed
	 * @return number of affected tupels
	 * @exception RepositoryException
	 */
	public synchronized int executeDml(String dmlQuery) throws RepositoryException {
		this.validateDataSourceConnection();
		
		Connection dbCon = null;
		Statement stmt = null;
		int affectedRows=0;
		
		try {
			dbCon = this.dataSource.getConnection();
			stmt = dbCon.createStatement();
			affectedRows = stmt.executeUpdate(dmlQuery);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			try { stmt.close(); } catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			try { dbCon.close();} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
		}
		
		return(affectedRows);
	}
	/**
	 * executes series of DML statements and returns the affected rows for
	 * each executed DML statement
	 * @param dmlQueries series of DML statements (String[])
	 * @throws RepositoryException
	 * @return affected rows (int[])
	 */
	public synchronized int[] executeDml(String[] dmlQueries) throws RepositoryException {
		this.validateDataSourceConnection();
		
		Connection dbCon = null;
		Statement stmt = null;
		int[] affectedRows = new int[]{};
		
		try {
			dbCon = this.dataSource.getConnection();
			stmt = dbCon.createStatement();
			for (int i=0;i<dmlQueries.length;i++) 
				stmt.addBatch(dmlQueries[i]);
			affectedRows = stmt.executeBatch();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			try { stmt.close(); } catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			try { dbCon.close();} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
		}
		
		return(affectedRows);
	}
	
	/**
	 * executes a select statement and returns the result set (ResultSet);
	 * please close the result set and its statement in your method
	 * @param sqlQuery SQL query (select) to be executed
	 * @return result set (ResultSet)
	 * @exception RepositoryException
	 */
	public synchronized ResultSet executeSelect(String sqlQuery) throws RepositoryException {
		this.validateDataSourceConnection();
		Connection dbCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			dbCon = this.dataSource.getConnection();
			stmt = dbCon.createStatement();
			sqlQuery = sqlQuery.replaceAll("(?<=({0,2}'.{0,100}))(?<!(=.{0,10}))'(?=(.{0,100}'))(?!(\\s{1,3}[or|and]))", "''");
			rs = stmt.executeQuery(sqlQuery);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		return rs;
	}
	
	public synchronized void closeStatement(Statement stmt) throws RepositoryException {
		Connection dbCon = null; 
		
		try {
			dbCon = stmt.getConnection();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			try { stmt.close(); } catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			try { dbCon.close();} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
		}
	} 
	
	/**
	 * close the given result set and its corresponding statement
	 * @param rs result set to be closed (ResultSet)
	 * @throws SQLException
	 */
	public synchronized void closeStatement(ResultSet rs) throws RepositoryException {
		this.validateDataSourceConnection();
		
		Connection dbCon = null;
		Statement stmt = null;
		
		try {
			stmt = rs.getStatement();
			dbCon = stmt.getConnection();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			try { rs.close();} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			try { if (stmt!=null) stmt.close(); } catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			try { if (dbCon!=null && !dbCon.isClosed()) dbCon.close();} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
		}
	}
	/**
	 * prepares a statement object for the given SQL query
	 * @param sqlQuery query string to be used for preparation of the statement
	 * @return prepared statement object
	 * @throws SQLException
	 */
	public synchronized PreparedStatement prepareStatement(String sqlQuery) throws RepositoryException {
		this.validateDataSourceConnection();
		
		Connection dbCon = null;
		PreparedStatement pStmt = null;
		
		try {
			dbCon = this.dataSource.getConnection();
			pStmt = dbCon.prepareStatement(sqlQuery);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return pStmt;
	}
	
		
	/**
	 * -----------------------------------------------------
	 *          p r o t e c t e d   m e t h o d s
	 * -----------------------------------------------------
	 */
	
	
	protected synchronized void validateDataSourceConnection() throws RepositoryException {
		if (this.dataSource==null)
			throw new RepositoryException("The data source connection pool is currently not initialized.");
		return;
	}
}
