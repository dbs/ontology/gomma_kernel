package org.gomma.api.util;

import org.gomma.api.APIFactory;
import org.gomma.exceptions.RepositoryException;


/**
 * The class creates unique object identifiers. The identifiers are created 
 * independently from the database generated keys. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class ObjectIDGenerator {

	private int highValue;
	private int objID;
	private int sourceID;
	
	private volatile static ObjectIDGenerator singleton = null;
	
	private ObjectIDGenerator() throws RepositoryException {
		this.highValue = APIFactory.getInstance().getRepositoryAPI().getHighValue();
		this.sourceID  = APIFactory.getInstance().getRepositoryAPI().getSourceID();
		this.objID = 0;
	}
	
	public static ObjectIDGenerator getInstance() throws RepositoryException {
		if (singleton ==  null) {
			synchronized (ObjectIDGenerator.class) {
				if (singleton == null)
					singleton = new ObjectIDGenerator();
			}
		}
		return singleton;
	}
	
	public long generateNextID() {
		long generatedID = (long)this.highValue;
		generatedID = generatedID << 24;
		generatedID += (this.objID++) << 32;
		generatedID += this.sourceID;
		return generatedID;
	}
	
	public static void stop() {
		singleton = null;
	}
}
