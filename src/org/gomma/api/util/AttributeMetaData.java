package org.gomma.api.util;

public class AttributeMetaData extends Object {
	private String schemaName;
	private String tableName;
	private String attributeName;
	private int dataType;
	private String dataTypeName;
	private int pos;
	
	public AttributeMetaData() {
		reset();
	}
	public AttributeMetaData(String schemaName, String tableName, String attributeName,
			int dataType, String dataTypeName, int position) {
		this.schemaName=schemaName;
		this.tableName=tableName;
		this.attributeName=attributeName;
		this.dataType=dataType;
		this.dataTypeName=dataTypeName;
		this.pos=position;
	}
	public String getSchemaName() {
		return(schemaName);
	}
	public String getTableName() {
		return(tableName);
	}
	public String getAttributeName() {
		return(this.attributeName);
	}
	public int getDataType() {
		return(dataType);
	}
	public String getDataTypeName() {
		return(dataTypeName);
	}
	public int getPosition() {
		return(this.pos);
	}
	public void setSchemaName(String name) {
		this.schemaName=name;
	}
	public void setTableName(String name) {
		this.tableName=name;
	}
	public void setAttributeName(String name) {
		this.attributeName=name;
	}
	public void setDataType(int type) {
		this.dataType=type;
	}
	public void setDataTypeName(String name) {
		this.dataTypeName=name;
	}
	public void setPosition(int pos) {
		this.pos=pos;
	}
	public void reset() {
		schemaName="";
		tableName="";
		attributeName="";
		dataType=-1;
		dataTypeName="";
	}
}
