package org.gomma.api.util;

/**
 * The enumeration provides a set of data source system types 
 * that categorize the data source system of the repository.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum DataSourceSystemTypes {

	/**
	 * Relational data base system
	 */
	RDBMS("rdbms");
	
	
	/**
	 * The item represents the name of the data source system type.
	 */
	private String name;
	
	/**
	 * The constructor initializes the enumeration element.
	 * @param name internal name of the data source system type
	 */
	private DataSourceSystemTypes(String name) {
		this.name = name;
	}
	
	/**
	 * The method returns the name of the data source system type. 
	 * @return name of the data source system type
	 */
	public String getName() {
		return this.name;
	}
}
