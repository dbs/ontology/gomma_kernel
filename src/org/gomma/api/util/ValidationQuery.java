package org.gomma.api.util;

/**
 * The interface defines methods that are necessary to returns data 
 * source system specific validation query. This (dummy) query is used
 * to validate if a data source connection is already alive or not. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public interface ValidationQuery {

	/**
	 * The method returns the validation query specific for the 
	 * current data source system.
	 * @return validation query
	 */
	public String getQuery();
}
