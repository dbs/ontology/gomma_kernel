package org.gomma.api.util;

/**
 * The enumeration provides a list of query match operations
 * that can be used by each (!) repository system for rewriting 
 * query conditions as part of a query to be executed. For instance, 
 * query match operations are the exact match representing  "=" 
 * operator in a SQL where clause (attribute = value).   
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public enum QueryMatchOperations {

	/**
	 * The element represents the exact string match operation, e.g., "attribute=value" in SQL.
	 */
	EXACT_MATCH,
	
	/**
	 * The element represents the starts with string match operation, e.g., "attribute like value%" in SQL. 
	 */
	STARTS_WITH,
	
	/**
	 * The element represents the ends with string match operation, e.g, "attribute like %value" in SQL.
	 */
	ENDS_WITH,
	
	/**
	 * The element represents the contains string match operation, e.g., "attribute like %value%" in SQL.
	 */
	CONTAINS;
}
