package org.gomma.api.util;

import java.util.NoSuchElementException;

/**
 * The enumeration provides a set of data source systems 
 * that can be used to store the repository.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum DataSourceSystems {

	/**
	 * The element represents the MySQl relation database system.
	 */
	MYSQL("mysql",DataSourceSystemTypes.RDBMS),
	
	/**
	 * The element represents the FEVER data source system based on a relation database.
	 */
	FEVER("fever",DataSourceSystemTypes.RDBMS) ;
	
	/**
	 * The item represents the data source system name.
	 */
	private String systemName;
	
	/**
	 * The item represents the data source system type.
	 */
	private DataSourceSystemTypes type;
	
	/**
	 * The constructor initializes the enumeration.
	 * @param name element name
	 */
	private DataSourceSystems(String name, DataSourceSystemTypes type) {
		this.systemName = name;
		this.type = type;
	}
	
	/**
	 * The method returns the system name.
	 * @return system name
	 */
	public String getName() {
		return this.systemName;
	}
	
	/**
	 * The method returns the data source system type.
	 * @return system type
	 */
	public DataSourceSystemTypes getType() {
		return this.type;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString() { 
		return this.systemName; 
	}
	
	/**
	 * The method resolves and returns the data source system from the given string. 
	 * @param sourceSystem string from that the data source system can be recognized 
	 * @return data source system
	 * @throws NoSuchElementException
	 */
	public static DataSourceSystems resolveDataSourceSystem(String sourceSystem) throws NoSuchElementException {
		for (DataSourceSystems s : DataSourceSystems.values())
			if (sourceSystem.equalsIgnoreCase(s.getName())) return s;
		throw new NoSuchElementException("Could not extract the data source system from the ini file.");
	}
}
