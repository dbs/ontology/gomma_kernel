package org.gomma.api.work;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;

/**
 * The class implements the work package API. It is the implementation 
 * for the FEVER system using a relational database at the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class WorkPackageAPI_FEVER implements WorkPackageAPI {

	
	/**
	 * @see WorkPackageAPI#getWorkPackageSet()
	 */
	public WorkPackageSet getWorkPackageSet() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see WorkPackageAPI#getWorkPackageSet(String, QueryMatchOperations)
	 */
	public WorkPackageSet getWorkPackageSet(String name, QueryMatchOperations ops) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see WorkPackageAPI#insertWorkPackage(String, String)
	 */
	public int insertWorkPackage(String name, String description) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see WorkPackageAPI#updateWorkPackage(WorkPackage)
	 */
	public int updateWorkPackage(WorkPackage pack) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see WorkPackageAPI#deleteWorkPackage(WorkPackage)
	 */
	public int deleteWorkPackage(WorkPackage pack) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
