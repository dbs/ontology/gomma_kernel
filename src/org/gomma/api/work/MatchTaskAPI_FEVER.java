package org.gomma.api.work;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.MatchTaskSet;
import org.gomma.model.work.TaskStatus;
import org.gomma.model.work.WorkPackage;

/**
 * The class implements the match task API. It is the implementation 
 * for the FEVER system using a relational database at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MatchTaskAPI_FEVER implements MatchTaskAPI {

	

	/**
	 * @see MatchTaskAPI#getMatchTasks(WorkPackage)
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#getMatchTasks(WorkPackage, TaskStatus)
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p, TaskStatus s) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#insertMatchTask(String, WorkPackage, SourceVersion, SourceVersion, ObjSet, ObjSet, String)
	 */
	public int insertMatchTask(String name, WorkPackage pack,
			SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs,
			ObjSet rangeObjs, String taskConf) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#updateMatchTask(MatchTask)
	 */
	public int updateMatchTask(MatchTask task) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#deleteMatchTask(MatchTask)
	 */
	public int deleteMatchTask(MatchTask task) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#deleteMatchTaskObjects(MatchTask)
	 */
	public int deleteMatchTaskObjects(MatchTask task) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#deleteMatchTaskObjects(WorkPackage)
	 */
	public int deleteMatchTaskObjects(WorkPackage pack) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MatchTaskAPI#deleteMatchTasks(WorkPackage)
	 */
	public int deleteMatchTasks(WorkPackage pack) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
