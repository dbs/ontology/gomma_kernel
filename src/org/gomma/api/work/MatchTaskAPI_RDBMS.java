package org.gomma.api.work;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.MatchTaskSet;
import org.gomma.model.work.TaskStatus;
import org.gomma.model.work.WorkPackage;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the match task API. It is the default
 * implementation for a relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MatchTaskAPI_RDBMS implements MatchTaskAPI {

	/**
	 * The constant represents the MySQL statement to load the set of match tasks of a specified work package. 
	 */
	private static final String LOAD_MATCH_TASKS = 
		"select task_id,name,d_lds_version_id_fk,r_lds_version_id_fk,status_id_fk,task_conf,start_time,end_time,resource_url "+
		"from gomma_match_tasks a "+
		"where pack_id_fk=[packageID]";
	
	/**
	 * The constant represents the MySQL statement to load objects that should be included into the match tasks.
	 */
	private static final String LOAD_MATCH_TASK_OBJECTS = 
		"select obj_id_fk "+
		"from gomma_task_objs "+
		"where task_id_fk=[taskID] and is_domain_obj=[isDomainObj]";
	
	/**
	 * The constant represents the MySQL statement to store a match task into the repository. 
	 */
	private static final String INSERT_MATCH_TASK = 
		"insert into gomma_match_tasks (pack_id_fk,name,d_lds_version_id_fk,r_lds_version_id_fk,task_conf,status_id_fk) "+
		"values ([packageID],'[name]',[domainVersionID],[rangeVersionID],'[taskConfig]',1)";
	
	/**
	 * The constant represents the MySQL statement to store match task objects within the repository. 
	 */
	private static final String INSERT_MATCH_TASK_OBJECTS = 
		"insert into gomma_task_objs (task_id_fk,obj_id_fk,is_domain_obj) values (?,?,?)";
	
	/**
	 * The constant represents the MySQL statement to update the match task within the repository. 
	 */
	private static final String UPDATE_MATCH_TASK =
		"update gomma_match_tasks "+
		"set status_id_fk=[statusID],"+
		"start_time=?,end_time=?,resource_url=? where task_id=[taskID]";
	
	/**
	 * The constant represents the MySQL statement to delete a specified match task.
	 */
	private static final String DELETE_MATCH_TASK =
		"delete from gomma_match_tasks where task_id=[taskID]";
	
	/**
	 * The constant represents the MySQL statement to delete all match tasks of specified work package.
	 */
	private static final String DELETE_MATCH_TASKS = 
		"delete from gomma_match_tasks where pack_id_fk=[packageID]";
	
	/**
	 * The constant represents the MySQL statement to delete all match objects for a specified task.
	 */
	private static final String DELETE_MATCH_TASK_OBJECTS =
		"delete from gomma_task_objs where task_id_fk=[taskID]";
	
	/**
	 * The constant represents the MySQL statement to delete all match objects of a specified package. 
	 */
	private static final String DELETE_MATCH_TASKS_OBJECTS =
		"delete from gomma_task_objs where task_id_fk in ("+
			"select task_id from gomma_match_tasks where pack_id_fk=[packageID])";
	
	
	/**
	 * Constructor of the class.
	 */
	public MatchTaskAPI_RDBMS() {}
	
	/**
	 * @see MatchTaskAPI#getMatchTasks(WorkPackage)
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p) throws RepositoryException {
		return this.loadMatchTasks(p,"");
	}
	
	/**
	 * @see MatchTaskAPI#getMatchTasks(WorkPackage, TaskStatus)
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p, TaskStatus s) throws RepositoryException {
		return this.loadMatchTasks(p," and status_id_fk="+s.getID()+" limit 1");
	}
	
	/**
	 * The method returns a set of match tasks for the specified work package.
	 * @param pack work package for which the set of match tasks will be retrieved
	 * @param query additional query focusing on specific tasks 
	 * @return set of match tasks for the specified work package
	 * @throws RepositoryException
	 */
	private MatchTaskSet loadMatchTasks(WorkPackage p, String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = (LOAD_MATCH_TASKS+queryCond)
						.replaceAll("\\[packageID\\]",Integer.toString(p.getID()));
		
		try {
			ResultSet rs = dbh.executeSelect(query);
			MatchTaskSet tasks = this.extractMatchTasks(rs,p);
			dbh.closeStatement(rs);
			
			query = LOAD_MATCH_TASK_OBJECTS;
			for (MatchTask task : tasks.getCollection()) {
				
				try {
					rs = dbh.executeSelect(query.
							replaceAll("\\[taskID\\]",Integer.toString(task.getID()))
							.replaceAll("\\[isDomainObj\\]","1"));
					
					while (rs.next()) { task.addDomainObjectID(rs.getInt(1)); }
					dbh.closeStatement(rs);
					
					rs = dbh.executeSelect(query
							.replaceAll("\\[taskID\\]",Integer.toString(task.getID()))
							.replaceAll("\\[isDomainObj\\]","0"));
					
					while (rs.next()) { task.addRangeObjectID(rs.getInt(1)); }
					dbh.closeStatement(rs);
					
				} catch (SQLException e) { throw new RepositoryException(e.getMessage()); }
			}
			return tasks;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	/**
	 * @see MatchTaskAPI#insertMatchTask(String, WorkPackage, SourceVersion, SourceVersion, ObjSet, ObjSet, String)
	 */
	public int insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_MATCH_TASK
						.replaceAll("\\[packageID\\]",Integer.toString(pack.getID()))
						.replaceAll("\\[name\\]",name)
						.replaceAll("\\[domainVersionID\\]",Integer.toString(domainSV.getID()))
						.replaceAll("\\[rangeVersionID\\]",Integer.toString(rangeSV.getID()))
						.replaceAll("\\[taskConfig\\]",taskConf);
		Statement stmt = null;
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {
			stmt = dbh.getDatabaseConnection().createStatement();
			stmt.execute(query,Statement.RETURN_GENERATED_KEYS);
			affectedRows=stmt.getUpdateCount();
			
			int taskID=0;
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) taskID = rs.getInt(1);
			rs.close();
			
			if (taskID==0) throw new RepositoryException("Could not resolve the new identifier for the inserted match task.");
			
			query = INSERT_MATCH_TASK_OBJECTS;
			pStmt = dbh.prepareStatement(query);
			for (Obj domainObj : domainObjs) {
				pStmt.setInt(1,taskID);
				pStmt.setInt(2,domainObj.getID());
				pStmt.setInt(3,1);
				pStmt.addBatch();
			}
			for (Obj rangeObj : rangeObjs) {
				pStmt.setInt(1,taskID);
				pStmt.setInt(2,rangeObj.getID());
				pStmt.setInt(3,0);
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			affectedRows += pStmt.getUpdateCount();
			
			return affectedRows;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			try { dbh.closeStatement(stmt); } catch (Exception e) {}
			try { dbh.closeStatement(pStmt); } catch (Exception e) {}
		}
	}

	
	/**
	 * @see MatchTaskAPI#updateMatchTask(MatchTask)
	 */
	public int updateMatchTask(MatchTask t) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = UPDATE_MATCH_TASK
						.replaceAll("\\[statusID\\]",Integer.toString(t.getTaskStatus().getID()))
						.replaceAll("\\[taskID\\]",Integer.toString(t.getID()));
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setTimestamp(1,java.sql.Timestamp.valueOf(sdf.format(t.getStartTime().getTime())));
			pStmt.setTimestamp(2,java.sql.Timestamp.valueOf(sdf.format(t.getEndTime().getTime())));
			pStmt.setString(3,t.getResourceURL());
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * @see MatchTaskAPI#deleteMatchTask(MatchTask)
	 */
	public int deleteMatchTask(MatchTask t) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MATCH_TASK.replaceAll("\\[taskID\\]",Integer.toString(t.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * @see MatchTaskAPI#deleteMatchTasks(WorkPackage)
	 */
	public int deleteMatchTasks(WorkPackage p) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MATCH_TASKS.replaceAll("\\[packageID\\]",Integer.toString(p.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * @see MatchTaskAPI#deleteMatchTaskObjects(MatchTask)
	 */
	public int deleteMatchTaskObjects(MatchTask t) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MATCH_TASK_OBJECTS.replaceAll("\\[taskID\\]",Integer.toString(t.getID()));
		
		return dbh.executeDml(query);
	}
	
	/**
	 * @see MatchTaskAPI#deleteMatchTaskObjects(WorkPackage)
	 */
	public int deleteMatchTaskObjects(WorkPackage p) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_MATCH_TASKS_OBJECTS.replaceAll("\\[packageID\\]",Integer.toString(p.getID()));
		
		return dbh.executeDml(query);
	}
	
	
	
	
	
	
	/**
	 * The method extracts and returns a set of match tasks from the given query result set.
	 * @param rs query result set
	 * @param pack work package
	 * @return set of match tasks (metadata)
	 * @throws SQLException
	 */
	private MatchTaskSet extractMatchTasks(ResultSet rs, WorkPackage p) throws SQLException {
		MatchTaskSet set = new MatchTaskSet();
		MatchTask task=null;
		SimplifiedGregorianCalendar cal;
		
		while (rs.next()) {
			task = new MatchTask.Builder(rs.getInt(1),p)
						.name(rs.getString(2))
						.domainSourceVersionID(rs.getInt(3))
						.rangeSourceVersionID(rs.getInt(4))
						.taskStatus(TaskStatus.resolveTaskStatus(rs.getInt(5)))
						.taskConfiguration(rs.getString(6))
						.resourceURL(rs.getString(9)).build();
			// start time stamp
			if (task.getTaskStatus()==TaskStatus.IN_PROCESSING || task.getTaskStatus()==TaskStatus.PROCESSING_FINISHED) { 
				cal = CacheManager.getInstance().getDateCache().getCalendar(rs.getTimestamp(7).getTime());
				task.setStartTime(cal);
			}
			// end time stamp
			if (task.getTaskStatus()==TaskStatus.PROCESSING_FINISHED) {
				cal = CacheManager.getInstance().getDateCache().getCalendar(rs.getTimestamp(8).getTime());
				task.setStartTime(cal);
			}
					
			set.addMatchTask(task);
		}
		
		return set;
	}

}
