package org.gomma.api.work;

import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;

/**
 * The interface defines methods to manage work packages within the repository. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface WorkPackageAPI {

	/**
	 * The method returns the complete set of work package objects.
	 * @return set of defined work package objects
	 * @throws RepositoryException
	 */
	public WorkPackageSet getWorkPackageSet() throws RepositoryException;
	
	/**
	 * The method returns a set of work package objects meeting the specified name 
	 * and query operator.
	 * @param name name pattern of the work package
	 * @param ops query match operator
	 * @return set of work package objects
	 * @throws RepositoryException
	 */
	public WorkPackageSet getWorkPackageSet(String name, QueryMatchOperations ops) throws RepositoryException;
	
	/**
	 * The method insert a work package using the specified name and description and 
	 * returns the number of affected rows.
	 * @param name name of the work package
	 * @param description description of the work package
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int insertWorkPackage(String name, String description) throws RepositoryException;
	
	/**
	 * The method updates the specified work package and returns the number of affected rows.
	 * @param pack work package
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int updateWorkPackage(WorkPackage pack) throws RepositoryException;
	
	/**
	 * The method deletes the specified work package and returns the number of affected rows.
	 * @param pack work package that should be deleted
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteWorkPackage(WorkPackage pack) throws RepositoryException;
	
	
}
