package org.gomma.api.work;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;

/**
 * The class implements the work package API. It is the default
 * implementation for a relational database. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class WorkPackageAPI_RDBMS implements WorkPackageAPI {

	/**
	 * The constant represents the MySQL statement to load work packages from the repository.
	 */
	private static final String LOAD_WORK_PACKAGES = 
		"select pack_id,name,descr from gomma_work_packs";
	
	/**
	 * The constant represents the MySQL statement to store a work package into the repository.
	 */
	private static final String INSERT_WORK_PACKAGE = 
		"insert into gomma_work_packs (name,descr) values (?,?)";
	
	/**
	 * The constant represents the MySQL statement to update the work package within the repository.
	 */
	private static final String UPDATE_WORK_PACKAGE = 
		"update gomma_work_packs name=?,descr=? where pack_id=[packageID]";
	
	/**
	 * The constant represents the MySQL statement to delete a specified work package.
	 */
	private static final String DELETE_WORK_PACKAGE = 
		"delete from gomma_work_packs where pack_id=[packageID]";
	
	
	/**
	 * Constructor of the class.
	 */
	public WorkPackageAPI_RDBMS() {}
	
	
	/**
	 * @see WorkPackageAPI#getWorkPackageSet()
	 */
	public WorkPackageSet getWorkPackageSet() throws RepositoryException {
		return this.loadWorkPackages("");
	}
	
	/**
	 * @see WorkPackageAPI#getWorkPackageSet(String, QueryMatchOperations)
	 */
	public WorkPackageSet getWorkPackageSet(String name, QueryMatchOperations ops) throws RepositoryException {
		String queryCond = " where name ";
		
		switch (ops) {
			case EXACT_MATCH:  queryCond += "='" +name+"'"; break;
			case STARTS_WITH:  queryCond +="like '"+name+"%'"; break;
			case ENDS_WITH:    queryCond +="like '%"+name+"'"; break;
			case CONTAINS:     queryCond +="like '%"+name+"%'"; break;
		}
		
		return this.loadWorkPackages(queryCond);
	}
	
	/**
	 * The method returns a set of work packages meeting the specified query condition. 
	 * @param queryCond query condition
	 * @return set of work packages
	 * @throws RepositoryException
	 */
	private WorkPackageSet loadWorkPackages(String queryCond) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_WORK_PACKAGES+queryCond;
		
		ResultSet rs = dbh.executeSelect(query);
		WorkPackageSet set = this.extractWorkPackages(rs, new WorkPackageSet());
		dbh.closeStatement(rs);
			
		return set; 
	}
	
	/**
	 * @see WorkPackageAPI#insertWorkPackage(String, String)
	 */
	public int insertWorkPackage(String name, String description) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = INSERT_WORK_PACKAGE;
		PreparedStatement pStmt = null;
		int affectedRows=0;
		
		try {
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,name);
			pStmt.setString(2,description);
			affectedRows = pStmt.executeUpdate();
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * @see WorkPackageAPI#updateWorkPackage(WorkPackage)
	 */
	public int updateWorkPackage(WorkPackage pack) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = UPDATE_WORK_PACKAGE.replaceAll("\\[packageID\\]",Integer.toString(pack.getID()));
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		
		try {	
			pStmt = dbh.prepareStatement(query);
			pStmt.setString(1,pack.getName());
			pStmt.setString(2,pack.getWorkPackageDescription());
			affectedRows = pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		
		return affectedRows;
	}
	
	/**
	 * @see WorkPackageAPI#deleteWorkPackage(WorkPackage)
	 */
	public int deleteWorkPackage(WorkPackage pack) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = DELETE_WORK_PACKAGE.replaceAll("\\[packageID\\]",Integer.toString(pack.getID()));
							
		return dbh.executeDml(query);
	}
	
	
	/**
	 * The method extracts and returns work package metadata from the given query result set.
	 * @param rs query result set
	 * @return set of work package (metadata)
	 * @throws SQLException
	 */
	private WorkPackageSet extractWorkPackages(ResultSet rs, WorkPackageSet set) throws RepositoryException {
		WorkPackage p;
		
		try {
			while (rs.next()) {
				p = new WorkPackage.Builder(rs.getInt(1))
						.name(rs.getString(2))
						.description(rs.getString(3)).build();
				set.addWorkPackages(p);
			}
			
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}
