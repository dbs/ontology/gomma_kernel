package org.gomma.api.work;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.MatchTaskSet;
import org.gomma.model.work.TaskStatus;
import org.gomma.model.work.WorkPackage;

/**
 * The interface defines methods to manage match tasks within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MatchTaskAPI {

	/**
	 * The method returns the complete set of match tasks for the specified work package.
	 * @param pack work package for which the set of match tasks will be retrieved
	 * @return set of match tasks for the specified work package
	 * @throws RepositoryException
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p) throws RepositoryException;
	
	/**
	 * The method returns a set of match tasks for the specified work package and the defined task status.
	 * @param p work package
	 * @param s task status
	 * @return set of match tasks
	 * @throws RepositoryException
	 */
	public MatchTaskSet getMatchTasks(WorkPackage p, TaskStatus s) throws RepositoryException;
	
	/**
	 * The method creates and stores a match task object into the repository and returns
	 * the number of affected rows.
	 * @param name name of the match task
	 * @param pack associated work package
	 * @param domainSV domain source version
	 * @param rangeSV range source version
	 * @param domainObjs set of domain objects that should be included in the match task
	 * @param rangeObjs set of range objects that should be included in the match task
	 * @param taskConf specific task configuration
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int insertMatchTask(String name, WorkPackage pack, SourceVersion domainSV, SourceVersion rangeSV, ObjSet domainObjs, ObjSet rangeObjs, String taskConf) throws RepositoryException;
	
	/**
	 * The method updates the specified match task object within the repository
	 * and returns the number of affected rows. 
	 * (no object references) and returns the number of affected rows.
	 * @param task match task that should be updated
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int updateMatchTask(MatchTask task) throws RepositoryException;
	
	/**
	 * The method deletes the specified match task and returns the number of
	 * affected rows.
	 * @param task match task that should be deleted
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteMatchTask(MatchTask task) throws RepositoryException;
	
	/**
	 * The method deletes all associated match tasks for the specified work package.
	 * @param pack work package and returns the number of affected rows.
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteMatchTasks(WorkPackage pack) throws RepositoryException;
	
	/**
	 * The method deletes all object references for the specified match task
	 * and returns the number of affected rows.
	 * @param task match task
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteMatchTaskObjects(MatchTask task) throws RepositoryException;
	
	/**
	 * The method delete all object references for all match tasks of 
	 * the specified work package and returns the number of affected rows.
	 * @param pack work package
	 * @return number of affected rows
	 * @throws RepositoryException
	 */
	public int deleteMatchTaskObjects(WorkPackage pack) throws RepositoryException;
}
