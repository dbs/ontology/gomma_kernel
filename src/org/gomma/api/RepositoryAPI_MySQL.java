package org.gomma.api;

/**
 * The class implements the administrative repository interface 
 * for a MySQL relational database system. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class RepositoryAPI_MySQL extends RepositoryAPI_RDBMS {

	/**
	 * The constant defines the DDL file name and location to create the repository schema.
	 */
	private static final String CREATE_SCHEMA_FILENAMELOC = "org/gomma/api/config/create.sql";
	
	/**
	 * The constant defines the DDL file name and location to drop the repository schema.
	 */
	private static final String DROP_SCHEMA_FILENAMELOC   = "org/gomma/api/config/drop.sql";
	
	
	/**
	 * @see RepositoryAPI_RDBMS#getCreationSchemaScriptFileName()
	 */
	protected String getCreationSchemaScriptFileName() {
		return CREATE_SCHEMA_FILENAMELOC;
	}
	
	/**
	 * @see RepositoryAPI_RDBMS#getDropSchemaScriptFileName()
	 */
	protected String getDropSchemaScriptFileName() {
		return DROP_SCHEMA_FILENAMELOC;
	}
}
