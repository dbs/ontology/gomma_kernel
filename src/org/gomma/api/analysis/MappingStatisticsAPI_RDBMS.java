package org.gomma.api.analysis;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.APIFactory;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingStatistics;
import org.gomma.model.mapping.MappingStatisticsSet;

/**
 * The class implements the mapping statistics API interface. It is 
 * the default implementation for a relational database. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class MappingStatisticsAPI_RDBMS implements MappingStatisticsAPI {

	/**
	 * The constant represents the MySQL statement to load mapping statistics data. 
	 */
	private static final String LOAD_MAPPING_STATISTICS =
		"select map_id_fk, num_versions, avg_num_corrs, avg_d_objs, avg_r_objs "+
		"from gomma_map_stats ";
	
	/**
	 * The constant represents the MySQL statement to create and store mapping statistics data.
	 */
	private static final String INSERT_MAPPING_STATISTICS =
		"insert into gomma_map_stats (map_id_fk,num_versions,avg_num_corrs,avg_d_objs,avg_r_objs) "+
		"select map_id_fk, count(*), avg(num_corrs), avg(num_d_objs), avg(num_r_objs) "+
		"from gomma_map_versions "+
		"group by map_id_fk";
	
	/**
	 * The constant represents the MySQL statement to delete mapping statistics data.
	 */
	private static final String DELETE_MAPPING_STATISTICS =
		"delete from gomma_map_stats";
	
	/**
	 * Constructor of the class.
	 */
	public MappingStatisticsAPI_RDBMS() {}
	
	/**
	 * @see MappingStatisticsAPI#insertMappingStatistics();
	 */
	public int insertMappingStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(INSERT_MAPPING_STATISTICS);
	}
	
	/**
	 * @see MappingStatisticsAPI#deleteMappingStatistics()
	 */
	public int deleteMappingStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(DELETE_MAPPING_STATISTICS);
	}
	
	
	/**
	 * @see MappingStatisticsAPI#getMappingStatistics()
	 */
	public MappingStatisticsSet getMappingStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_MAPPING_STATISTICS);
		MappingStatisticsSet set = this.extractMappingStatistics(rs,
			APIFactory.getInstance().getMappingAPI().getMappingSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	/**
	 * The method extracts and returns the mappings statistics data from a given SQL query result set.
	 * @param rs SQL query result set
	 * @param mapSet set of mappings for which the mappings statistics will be extracted
	 * @return set of mapping statistics data
	 * @throws RepositoryException
	 */
	private MappingStatisticsSet extractMappingStatistics(ResultSet rs, MappingSet mapSet) throws RepositoryException {
		MappingStatisticsSet set = new MappingStatisticsSet();
		
		try {
			while (rs.next()) {
				if (!mapSet.contains(rs.getInt(1))) continue;
				
				set.addMappingStatistics(new MappingStatistics.Builder(mapSet.getMapping(rs.getInt(1)))
						.numberOfVersions(rs.getInt(2))
						.averageNumberOfCorrespondences(rs.getFloat(3))
						.averageNumberOfDomainObjects(rs.getFloat(4))
						.averageNumberOfRangeObjects(rs.getFloat(5)).build());
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return set;
	}
}
