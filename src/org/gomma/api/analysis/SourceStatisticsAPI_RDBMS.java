package org.gomma.api.analysis;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.gomma.api.APIFactory;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceStatistics;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatistics;
import org.gomma.model.source.SourceVersionStatisticsSet;

/**
 * The class implements the source statistics interface for managing 
 * source statistics data. It is the default implementation for a 
 * relational database.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class SourceStatisticsAPI_RDBMS implements SourceStatisticsAPI {

	/**
	 * The constant represents the MySQL statement to load the source statistics data.
	 */
	private static final String LOAD_SOURCE_STATISTICS = 
		"select a.lds_id_fk, num_versions, avg_num_concepts, avg_num_rels," +
			"d.version, b.num_objs, b.num_rels,"+ // first version
			"e.version, c.num_objs, c.num_rels "+ // last version
		"from gomma_lds_stats a, gomma_lds_version_stats b, gomma_lds_version_stats c, "+
			"gomma_lds_versions d, gomma_lds_versions e "+
		"where a.start_lds_version_id_fk = b.lds_version_id_fk "+
		  "and a.stop_lds_version_id_fk = c.lds_version_id_fk "+
		  "and a.start_lds_version_id_fk = d.lds_version_id "+
		  "and a.stop_lds_version_id_fk = e.lds_version_id";
	
	/**
	 * The constant represents the MySQL statement to create and store the source statistics data.
	 */
	private static final String INSERT_SOURCE_STATISTICS =
		"insert into gomma_lds_stats ("+
					"lds_id_fk,start_lds_version_id_fk,stop_lds_version_id_fk,"+
					"num_versions,avg_num_concepts,avg_num_rels) "+
		"select a.lds_id_fk, first.lds_version_id,last.lds_version_id,"+
					"count(*), avg(num_objs), avg(num_rels) "+
		"from gomma_lds_version_stats a,"+
		// find out the first version
			"(select lds_id_fk, lds_version_id from gomma_lds_versions c "+
				"where lds_date = (select min(lds_date) "+
								  "from gomma_lds_versions d "+
								  "where c.lds_id_fk=d.lds_id_fk)) first, "+
		// find out the last version
			"(select lds_id_fk, lds_version_id from gomma_lds_versions e "+
				"where lds_date = (select max(lds_date) "+
								  "from gomma_lds_versions f "+
								  "where e.lds_id_fk=f.lds_id_fk)) last "+
		"where a.lds_id_fk = first.lds_id_fk "+
		  "and a.lds_id_fk = last.lds_id_fk "+
		"group by a.lds_id_fk";
	
	/**
	 * The constant represents the MySQL statement to delete the source statistics data.
	 */
	private static final String DELETE_SOURCE_STATISTICS = 
		"delete from gomma_lds_stats";
	
	/**
	 * The constant represents the MySQL statement to load the source statistics data.
	 */
	private static final String LOAD_SOURCE_VERSION_STATISTICS =
		"select lds_version_id_fk, num_objs, num_rels "+
		"from gomma_lds_version_stats";
	
	/**
	 * The constant represents the MySQL statement to create and store the source version statistics data.
	 */
	private static final String INSERT_SOURCE_VERSION_STATISTICS =
		"insert into gomma_lds_version_stats (lds_id_fk, lds_version_id_fk, num_objs, num_rels) "+
		"select a.lds_id_fk, a.lds_version_id, b.cnt_obj, c.cnt_rel "+
		"from gomma_lds_versions a, "+
		// compute number of objects
			"(select lds_version_id, count(*) as cnt_obj "+
			"from gomma_lds_versions v1, gomma_obj_versions o1 "+
			"where v1.lds_id_fk = o1.lds_id_fk "+
			  "and v1.lds_date between o1.date_from and o1.date_to "+
			"group by lds_version_id "+
			"union all "+
			"select lds_version_id, 0 as cnt_obj "+
			"from gomma_lds_versions v2 "+
			"where not exists (select * from gomma_obj_versions o2 "+
							  "where v2.lds_id_fk = o2.lds_id_fk "+
							    "and v2.lds_date between o2.date_from and o2.date_to)) b, "+
		// compute number of relationships
			"(select lds_version_id, count(*) as cnt_rel "+
			"from gomma_lds_versions v1, gomma_lds_structure s1 "+
			"where v1.lds_id_fk = s1.lds_id_fk "+
			  "and v1.lds_date between s1.date_from and s1.date_to "+
			"group by lds_version_id "+
			"union all "+
			"select lds_version_id, 0 as cnt_rel "+
			"from gomma_lds_versions v2 "+
			"where not exists (select * from gomma_lds_structure s2 "+
							  "where v2.lds_id_fk = s2.lds_id_fk "+
							  "and v2.lds_date between s2.date_from and s2.date_to)) c "+
		"where a.lds_version_id = b.lds_version_id "+
		  "and a.lds_version_id = c.lds_version_id";
	
	/**
	 * The constant represents the MySQL statement to delete the source version statistics data.
	 */
	private static final String DELETE_SOURCE_VERSION_STATISTICS = 
		"delete from gomma_lds_version_stats";
	
	/**
	 * Constructor of the class.
	 */
	public SourceStatisticsAPI_RDBMS() {}

	/**
	 * @see SourceStatisticsAPI#getSourceStatisticsSet()
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_SOURCE_STATISTICS);
		SourceStatisticsSet set = this.extractSourceStatistics(rs,
			APIFactory.getInstance().getSourceAPI().getSourceSet());
		dbh.closeStatement(rs);
		
		return set;
	}

	/**
	 * @see SourceStatisticsAPI#insertSourceStatictics()
	 */
	public int insertSourceStatictics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(INSERT_SOURCE_STATISTICS);
	}
	
	/**
	 * @see SourceStatisticsAPI#deleteSourceStatistics()
	 */
	public int deleteSourceStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(DELETE_SOURCE_STATISTICS);
	}
	
	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatisticsSet()
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
	
		ResultSet rs = dbh.executeSelect(LOAD_SOURCE_VERSION_STATISTICS);
		SourceVersionStatisticsSet set = this.extractSourceVersionStatistics(rs,
			APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatisticsSet(Source)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_SOURCE_VERSION_STATISTICS+" where lds_id_fk="+s.getID());
		SourceVersionStatisticsSet set = this.extractSourceVersionStatistics(rs,
			APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatistics(SourceVersionSet)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(SourceVersionSet svSet) throws RepositoryException{
        DatabaseHandler dbh = DatabaseHandler.getInstance();
		
        
		ResultSet rs = dbh.executeSelect(LOAD_SOURCE_VERSION_STATISTICS+" where lds_version_id_fk in ( "+svSet.getSeparatedIDString(",")+" )");
		SourceVersionStatisticsSet set = this.extractSourceVersionStatistics(rs,
			APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet());
		dbh.closeStatement(rs);
		
		return set;
	}
	
	
	public int insertSourceVersionStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(INSERT_SOURCE_VERSION_STATISTICS);
	}
	
	/**
	 * @see SourceStatisticsAPI#deleteSourceVersionStatistics()
	 */
	public int deleteSourceVersionStatistics() throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		return dbh.executeDml(DELETE_SOURCE_VERSION_STATISTICS);
	}
	
	
	
	/**
	 * The method extracts and returns source statistics data from a given SQL query result set.
	 * @param rs query result set
	 * @param sourceSet set of sources for which the statistics data should be extracted
	 * @return set of source statistics data
	 * @throws RepositoryException
	 */
	private SourceStatisticsSet extractSourceStatistics(ResultSet rs, SourceSet sourceSet) throws RepositoryException {
		SourceStatisticsSet set = new SourceStatisticsSet();
		
		try {
			while (rs.next()) {
			
				if (!sourceSet.contains(rs.getInt(1))) continue;
				
				set.addSourceStatictics(new SourceStatistics.Builder(
						sourceSet.getSource(rs.getInt(1)))
						.numberOfVersions(rs.getInt(2))
						.averageNumberOfConcepts(rs.getFloat(3))
						.averageNumberOfRelations(rs.getFloat(4))
						.firstVersionName(rs.getString(5))
						.numberOfConceptsInFirstVersion(rs.getInt(6))
						.numberOfRelatonsFirstVersion(rs.getInt(7))
						.lastVersionName(rs.getString(8))
						.numberOfConceptsInLastVersion(rs.getInt(9))
						.numberOfRelatonsLastVersion(rs.getInt(10)).build());
			}
			
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	/**
	 * /**
	 * The method extracts and returns source version statistics data from a given SQL query result set.
	 * @param rs query result set
	 * @param versionSet set of source versions for which the statistics data should be extracted
	 * @return set of source version statistics data
	 * @throws RepositoryException
	 */
	private SourceVersionStatisticsSet extractSourceVersionStatistics(ResultSet rs, SourceVersionSet versionSet) throws RepositoryException {
		SourceVersionStatisticsSet set = new SourceVersionStatisticsSet();
		
		try {
			while (rs.next()) {
				if (!versionSet.contains(rs.getInt(1))) continue;
				
				set.addSourceVersionStatistics(new SourceVersionStatistics.Builder(
						versionSet.getSourceVersion(rs.getInt(1)))
						.numberOfObjects(rs.getInt(2))
						.numberOfRelations(rs.getInt(3)).build());
			}
			
			return set;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}
