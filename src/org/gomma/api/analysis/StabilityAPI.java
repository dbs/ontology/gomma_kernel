package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;


public interface StabilityAPI {
	
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisName(String analysisName)throws RepositoryException;
	
	public StabilityAnalysisSet getStabilityAnalysisSetWithMapName(String mapName)throws RepositoryException;
	
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisID(int id)throws RepositoryException;
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName)throws RepositoryException;
	
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName, SimplifiedGregorianCalendar versionBegin,SimplifiedGregorianCalendar versionEnd,int kMapVersions)throws RepositoryException;
	
	public  StabilityAnalysisSet getStabilityAnalysisStatistics()throws RepositoryException;
    
	public StabilityAnalysisSet getStabilityAnalysisSet(String mapName, int kMapVersions, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd )throws RepositoryException;
	
	public int insertStabilityAnalysis(String mapName, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd,int kMapVersions,String analysisName)throws RepositoryException;
	
	public int insertCorrespondenceStabs(CorrespondenceStabilitySet set)throws RepositoryException;
	
	public int insertCorrespondenceStab(CorrespondenceStability corrStab)throws RepositoryException;
	
	public int updateStabilityAnalysis(StabilityAnalysis sa)throws RepositoryException;
	
	public int deleteCorrespondenceStabs(String analysisName)throws RepositoryException;
	
	public int deleteStabilityAnalysis(String analysisName)throws RepositoryException;
	
	
	
}
