package org.gomma.api.analysis;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the stability API for generating, materializing
 * and retrieval of stability analysis data. It is the default implementation
 * of a relational database system.
 * @author Yiming Huang (leecoco@hotmail.com)
 * @version 1.0
 * @since JDK 1.5
 */
public class StabilityAPI_RDBMS implements StabilityAPI {
/*    private static final String LOAD_ANALYSIS_NAME="select a.name from gomma_stability_analysises a, "
    	    +"gomma_maps b where a.map_id_fk=b.map_id and b.name='[mapName]'";
*/   
    private static final String LOAD_CORRESPONDENCE_STABILITIES="select a.analysis_id_fk, "
    	    +"a.domain_obj_id_fk, a.range_obj_id_fk, c.accession, d.accession, b.name, "
    	    +"a.stabAvg, a.stabWM, a.last_confidence from "
    	    +"gomma_correspondence_stabilities a, gomma_stability_analysis b, gomma_obj_versions c, "
    	    +"gomma_obj_versions d, gomma_maps e where a.analysis_id_fk=b.analysis_id  "
    	    +"and a.domain_obj_id_fk=c.obj_id and a.range_obj_id_fk=d.obj_id and e.map_id=b.map_id_fk";
    
    private static final String LOAD_STABILITY_ANALYSIS="select a.analysis_id, a.name, b.map_id, b.name, a.num_versions, "
    	    +"a.creation_date, a.first_version_date, a.last_version_date from gomma_stability_analysis a,"
    	    +"gomma_maps b where a.map_id_fk=b.map_id";
    
    private static final String INSERT_STABILITY_ANALYSIS="insert into gomma_stability_analysis(name, map_id_fk,"
    	    +"num_versions, creation_date, first_version_date, last_version_date) "
    	    +"values(?,?,?,?,?,?)";
    
    private static final String INSERT_CORRESPONDENCE_STABILITIES="insert into gomma_correspondence_stabilities("
    	    +"domain_obj_id_fk, range_obj_id_fk, analysis_id_fk, last_confidence, stabAvg, stabWM) values(?,?,?,?,?,?)";
    
    private static final String UPDATE_STABILITY_ANALYSIS="update gomma_stability_analysis set name=? "
    	    +" where analysis_id=[analysisId]";
    
    private static final String DELETE_CORRESPONDENCE_STABILITIES="delete from gomma_correspondence_stabilities "
    	    +"where analysis_id_fk in(select analysis_id from gomma_stability_analysis where name="
    	    +"'[analysisName]')";
    
    private static final String DELETE_STABILITY_ANALYSIS="delete from gomma_stability_analysis where "
    	    +"name='[analysisName]'";
	
    /**
     * @see StabilityAPI#deleteCorrespondenceStabs(String)
     */
	public int deleteCorrespondenceStabs(String analysisName)
			throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String sqlQuery = DELETE_CORRESPONDENCE_STABILITIES.replaceAll("\\[analysisName\\]",analysisName);
		
		return dbh.executeDml(sqlQuery);
	}

	/**
	 * @see StabilityAPI#deleteStabilityAnalysis(String)
	 */
	public int deleteStabilityAnalysis(String analysisName)
			throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String sqlQuery = DELETE_STABILITY_ANALYSIS.replaceAll("\\[analysisName\\]",analysisName);
		
		return dbh.executeDml(sqlQuery);
	}
	
	/**
     * @see StabilityAPI#insertCorrespondenceStabs(CorrespondenceStabiltiySet)
     */
	public int insertCorrespondenceStabs(CorrespondenceStabilitySet set)
			throws RepositoryException {
        
        DatabaseHandler dbh = DatabaseHandler.getInstance();
        PreparedStatement pStmt = null;
        int affectedRows = 0;
		try {
			
			//insert
			pStmt = dbh.prepareStatement(INSERT_CORRESPONDENCE_STABILITIES);
			for(CorrespondenceStability corrStab:set.getCollection()){
			//System.out.println(corrStab.getStabAvgValue());
			//pStmt.setInt(1,mv.mapVersionID);
			pStmt.setInt(1,corrStab.getDomainObjID());
			pStmt.setInt(2,corrStab.getRangeObjID());
			pStmt.setInt(3, corrStab.getAnalysisID());
			pStmt.setFloat(4, corrStab.getLastConfidence());
			pStmt.setFloat(5,corrStab.getStabAvgValue());
			pStmt.setFloat(6,corrStab.getStabWMValue());
			pStmt.addBatch();
			}
			pStmt.executeBatch();
			affectedRows = pStmt.getUpdateCount();
			
						
		} catch (SQLException e) {
		 	throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}

	/**
     * @see StabilityAPI#insertCorrespondenceStab(CorrespondenceStabiltiy)
     */
	public int insertCorrespondenceStab(CorrespondenceStability corrStab)	throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
		int affectedRows = 0;
		try {
			
			//insert
		pStmt = dbh.prepareStatement(INSERT_CORRESPONDENCE_STABILITIES);
		
		//System.out.println(corrStab.getStabAvgValue());
		//pStmt.setInt(1,mv.mapVersionID);
			pStmt.setInt(1,corrStab.getDomainObjID());
			pStmt.setInt(2,corrStab.getRangeObjID());
			pStmt.setInt(3, corrStab.getAnalysisID());
			pStmt.setFloat(4, corrStab.getLastConfidence());
			pStmt.setFloat(5,corrStab.getStabAvgValue());
			pStmt.setFloat(6,corrStab.getStabWMValue());
			pStmt.execute();
			affectedRows = pStmt.getUpdateCount();
			
						
		} catch (SQLException e) {
		 	throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
		}

	
	/**
     * @see StabilityAPI#insertStabilityAnalysis(String, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar,int,String)
     */
	public int insertStabilityAnalysis(String mapName,
			SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd, int kMapVersions,
			String analysisName) throws RepositoryException {
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		PreparedStatement pStmt = null;
        int affectedRows = 0;
		try {
			
			ResultSet rs = dbh.executeSelect("SELECT map_id FROM gomma_maps WHERE name='"+mapName+"'");
			int mapID=0;
			while (rs.next()) {
		          mapID = rs.getInt(1);
		          break;
		        
		    }
			
			dbh.closeStatement(rs);
			
			//insert
			pStmt = dbh.prepareStatement(INSERT_STABILITY_ANALYSIS );
			//pStmt.setInt(1,mv.mapVersionID);
			pStmt.setString(1,analysisName);
			pStmt.setInt(2,mapID);
			pStmt.setInt(3, kMapVersions);//all selected Versions,not only previous versions
			pStmt.setDate(4, new Date(System.currentTimeMillis()));
			pStmt.setDate(5,new Date(versionBegin.getTime().getTime()));
			pStmt.setDate(6,new Date(versionEnd.getTime().getTime()));
			
			pStmt.execute();
			affectedRows = pStmt.getUpdateCount();
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} finally {
			if (pStmt!=null) dbh.closeStatement(pStmt);
		}
		return affectedRows;
	}
	
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisID(int id)throws RepositoryException{
		return this.loadAnalysisSet(" and a.analysis_id="+id);
	}

	/**
     * @see StabilityAPI#getStabilityAnalysisSetWithAnalysisName(String)
     */
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisName(String analysisName)throws RepositoryException{
		return this.loadAnalysisSet(" and a.name='"+analysisName+"'");
	}

	/**
     * @see StabilityAPI#getStabiltiyAnalysisSetWithMapName(String)
     */
	public StabilityAnalysisSet getStabilityAnalysisSetWithMapName(String mapName)
			throws RepositoryException {
		return this.loadAnalysisSet(" and b.name='"+mapName+"'");
	}

	/**
     * @see StabilityAPI#getCorrespondenceStabs(String)
     */
	public CorrespondenceStabilitySet getCorrespondenceStabs(
			String analysisName) throws RepositoryException {
		
		return this.loadCorrespondenceStabilitySet(" and b.name='"+analysisName+"'");
	}
    
	/**
     * @see StabilityAPI#getCorrespondenceStabs(String,SimplifiedGregorianCalendar,SimplifiedGregorianCalendar,int)
     */
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName,SimplifiedGregorianCalendar versionBegin, 
			SimplifiedGregorianCalendar versionEnd, int kMapVersions)throws RepositoryException{
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		return this.loadCorrespondenceStabilitySet(" and e.name='"+mapName+"' and b.num_versions="+kMapVersions+" and "
				+" b.last_version_date='"+df.format(versionEnd.getTime())+"'");
	}
	
	/**
     * @see StabilityAPI#getStabilityAnalysisSet(String,int,SimplifiedGregorianCalendar,SimplifiedGregorianCalendar)
     */
	public StabilityAnalysisSet getStabilityAnalysisSet(String mapName,
			int kMapVersions, SimplifiedGregorianCalendar versionBegin, SimplifiedGregorianCalendar versionEnd)
			throws RepositoryException {
		
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return this.loadAnalysisSet(" and b.name='"+mapName+"' and a.num_versions="+kMapVersions+" and "
    	    +" a.last_version_date='"+df.format(versionEnd.getTime())+"'");
	}
	
	/**
     * @see StabilityAPI#getStabilitiesAnalysisStatistic
     */
	public StabilityAnalysisSet getStabilityAnalysisStatistics()
			throws RepositoryException {
		return this.loadAnalysisSet("");
	}

	/**
     * @see StabilityAPI#updateStabilityAnalysis(StabilityAnalysis)
     */
	public int updateStabilityAnalysis(StabilityAnalysis sa	) throws RepositoryException {
		    PreparedStatement pStmt = null;
		    int affectedRows=0;
		    DatabaseHandler dbh=DatabaseHandler.getInstance();
		    String query = UPDATE_STABILITY_ANALYSIS.replaceAll("\\[analysisId\\]",Integer.toString(sa.getID()));
			try {
				
				//insert
				pStmt = dbh.prepareStatement(query);
				pStmt.setString(1,sa.getName());
				//pStmt.setDate(2,new Date(System.currentTimeMillis()));
				
				
				pStmt.execute();
				affectedRows = pStmt.getUpdateCount();
			} catch (SQLException e) {
			 	throw new RepositoryException(e.getMessage());
			} finally {
				if (pStmt!=null) dbh.closeStatement(pStmt);
			}
			return affectedRows;
	}
    
	private StabilityAnalysisSet loadAnalysisSet(String queryCond)throws RepositoryException{
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_STABILITY_ANALYSIS+queryCond;
		
		ResultSet rs = dbh.executeSelect(query);
		StabilityAnalysisSet set = this.extractStabilityAnalysisSet(rs);
		dbh.closeStatement(rs);
		
		return set;
	}
	
	private StabilityAnalysisSet extractStabilityAnalysisSet(ResultSet rs) throws RepositoryException {
		StabilityAnalysisSet set = new StabilityAnalysisSet();
		StabilityAnalysis sa;
		
		try {
			while (rs.next()) {
				sa = new StabilityAnalysis.Builder(rs.getString(2))
						.analysisId(rs.getInt(1))
						.mapId(rs.getInt(3))
						.mapName(rs.getString(4))
						.numberOfMappingVersions(rs.getInt(5))
						.creationDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(6).getTime()))
						.firstMappingVersionDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(7).getTime()))
						.lastMappingVersionDate(CacheManager.getInstance().getDateCache().getCalendar(rs.getDate(8).getTime()))
						.build();

				set.addStabilityAnalysis(sa);
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return set;
	}
	
	private CorrespondenceStabilitySet loadCorrespondenceStabilitySet(String queryCond)throws RepositoryException{
			
			DatabaseHandler dbh = DatabaseHandler.getInstance();
			String query = LOAD_CORRESPONDENCE_STABILITIES+queryCond;
			
			ResultSet rs = dbh.executeSelect(query);
			CorrespondenceStabilitySet set = this.extractCorrespondenceStabilitysSet(rs);
			dbh.closeStatement(rs);
			
			return set;
		}
	
	private CorrespondenceStabilitySet extractCorrespondenceStabilitysSet(ResultSet rs) throws RepositoryException {
		CorrespondenceStabilitySet set = new CorrespondenceStabilitySet();
		CorrespondenceStability corrStab;
		
		try {
			while (rs.next()) {
				 corrStab=new CorrespondenceStability.Builder(rs.getInt(2),rs.getInt(3),rs.getInt(1))
                 .domainAccession(rs.getString(4))
                 .rangeAccession(rs.getString(5))
                 .analysisName(rs.getString(6))
                 .stabAvgValue(rs.getFloat(7))
                 .stabWMValue(rs.getFloat(8))
                 .lastConfidence(rs.getFloat(9))
                 .build();
            set.addCorrespondenceStab(corrStab);
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		} catch (WrongSourceException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return set;
	}
}
