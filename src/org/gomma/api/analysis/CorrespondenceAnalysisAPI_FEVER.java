package org.gomma.api.analysis;

import java.util.List;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingVersion;

/**
 * The class implements methods for analyzing mapping correspondences.
 * The implementation is for the FEVER system using a relational database 
 * system at the back-end.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public final class CorrespondenceAnalysisAPI_FEVER implements CorrespondenceAnalysisAPI {

	/**
	 * @see CorrespondenceAnalysisAPI#getConfidenceFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getDomainObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getRangeObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getSupportFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

}
