package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.MappingStatisticsSet;

/**
 * The class implements the mapping statistics API interface for FEVER system 
 * using a relational database at the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class MappingStatisticsAPI_FEVER implements MappingStatisticsAPI {

	

	/**
	 * @see MappingStatisticsAPI#getMappingStatistics()
	 */
	public MappingStatisticsSet getMappingStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingStatisticsAPI#insertMappingStatistics()
	 */
	public int insertMappingStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see MappingStatisticsAPI#deleteMappingStatistics()
	 */
	public int deleteMappingStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
