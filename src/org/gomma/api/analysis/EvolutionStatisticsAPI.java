package org.gomma.api.analysis;

import java.util.List;
import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public interface EvolutionStatisticsAPI {

	public Map<Integer,Integer> getLDSVersionStats(SourceVersionSet set, int statType) throws RepositoryException;
	
	public Map<Integer,Integer> getLDSVersionStats(int ldsID, int type) throws RepositoryException;
		
	public float getAverageEvolutionStats(SourceVersionSet versions, int action, int period, SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException;
	
	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet versions, int action, SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException;
	
	public Map<Map<Integer,Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet versions,int action,int period,SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException;
	
	public void storeLDSVersionStats(int ldsID) throws RepositoryException;
	
	public void storeLDSVersionStats(SourceVersionSet versions) throws RepositoryException;
	
	public void storeEvolutionStats(SourceVersionSet v) throws RepositoryException, WrongSourceException, OperatorException;
	
	public void storeMapVersionEvolutionStats(MappingVersionSet v) throws RepositoryException, WrongSourceException,OperatorException;
	
	public int getNumberOfObjects(ObjSet set) throws RepositoryException;
	
	public Map<String,List<String>> getObsoleteMappings(List<String> accessions) throws RepositoryException;
	
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws RepositoryException;
	
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws RepositoryException;
}
