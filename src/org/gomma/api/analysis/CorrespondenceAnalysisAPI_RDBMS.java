package org.gomma.api.analysis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.RepositoryException;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingVersion;

/**
 * The class implements methods for analyzing mapping correspondences.
 * The implementation is for a default relational database system.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public class CorrespondenceAnalysisAPI_RDBMS implements CorrespondenceAnalysisAPI {

	private static final String LOAD_CONFIDENCE_FREQUENCIES = 
		"select round(confidence,1), count(*) "+
		"from gomma_obj_corrs "+
		"where map_version_id_fk= [versionID] "+
		"group by round(confidence,1)";
	
	private static final String LOAD_SUPPORT_FREQUENCIES = 
		"select support, count(*) "+
		"from gomma_obj_corrs "+
		"where map_version_id_fk= [versionID] "+
		"group by support";
	
	private static final String LOAD_DOMAIN_OBJECT_FREQUENCIES =
		"select cnt, count(*) "+
		"from ("+
			"select d_obj_id_fk, count(*) as cnt "+
			"from gomma_obj_corrs "+
			"where map_version_id_fk = [versionID] "+
			"group by d_obj_id_fk) t "+
		"group by cnt";
	
	private static final String LOAD_RANGE_OBJECT_FREQUENCIES =
		"select cnt, count(*) "+
		"from ("+
			"select r_obj_id_fk, count(*) as cnt "+
			"from gomma_obj_corrs "+
			"where map_version_id_fk = [versionID] "+
			"group by r_obj_id_fk) t "+
		"group by cnt";
	
	/**
	 * Constructor of the class.
	 */
	public CorrespondenceAnalysisAPI_RDBMS() {}
	
	/**
	 * @see CorrespondenceAnalysisAPI#getConfidenceFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v)	throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_CONFIDENCE_FREQUENCIES.replaceAll("\\[versionID\\]",Integer.toString(v.getID())));
		List<FrequencyStatisticsFloat> list = this.extractFloatFrequencies(rs);
		
		dbh.closeStatement(rs);
		return list;
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getSupportFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_SUPPORT_FREQUENCIES.replaceAll("\\[versionID\\]",Integer.toString(v.getID())));
		List<FrequencyStatisticsInteger> list = this.extractIntegerFrequencies(rs);
		
		dbh.closeStatement(rs);
		return list;
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getDomainObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_DOMAIN_OBJECT_FREQUENCIES.replaceAll("\\[versionID\\]",Integer.toString(v.getID())));
		List<FrequencyStatisticsInteger> list = this.extractIntegerFrequencies(rs);
		
		dbh.closeStatement(rs);
		return list;
	}

	/**
	 * @see CorrespondenceAnalysisAPI#getRangeObjectFrequencies(MappingVersion)
	 */
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		ResultSet rs = dbh.executeSelect(LOAD_RANGE_OBJECT_FREQUENCIES.replaceAll("\\[versionID\\]",Integer.toString(v.getID())));
		List<FrequencyStatisticsInteger> list = this.extractIntegerFrequencies(rs);
		
		dbh.closeStatement(rs);
		return list;
	}

	
	
	private List<FrequencyStatisticsFloat> extractFloatFrequencies(ResultSet rs) throws RepositoryException {
		
		List<FrequencyStatisticsFloat> list = new ArrayList<FrequencyStatisticsFloat>();
		
		try {
			
			while (rs.next()) {
				list.add(new FrequencyStatisticsFloat.Builder(rs.getFloat(1), rs.getInt(2)).build());
			}
			return list;
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	
	private List<FrequencyStatisticsInteger> extractIntegerFrequencies(ResultSet rs) throws RepositoryException {
		
		List<FrequencyStatisticsInteger> list = new ArrayList<FrequencyStatisticsInteger>();
		
		try {
			
			while (rs.next()) {
				list.add(new FrequencyStatisticsInteger.Builder(rs.getInt(1), rs.getInt(2)).build());
			}
			return list;
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
}
