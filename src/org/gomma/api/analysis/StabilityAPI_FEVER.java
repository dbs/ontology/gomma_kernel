package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.stability.CorrespondenceStability;
import org.gomma.model.stability.CorrespondenceStabilitySet;
import org.gomma.model.stability.StabilityAnalysis;
import org.gomma.model.stability.StabilityAnalysisSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the stability API for generating, materializing
 * and retrieval of stability analysis data. It is the implementation
 * for the FEVER system using a relational database at the back-end.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK 1.5
 */
public final class StabilityAPI_FEVER implements StabilityAPI {

	

	/**
	 * @see StabilityAPI#getStabilityAnalysisSet(String, int, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public StabilityAnalysisSet getStabilityAnalysisSet(String mapName,
			int kMapVersions, SimplifiedGregorianCalendar versionBegin,
			SimplifiedGregorianCalendar versionEnd) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#getStabilityAnalysisSetWithAnalysisID(int)
	 */
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisID(int id) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#getStabilityAnalysisSetWithAnalysisName(String)
	 */
	public StabilityAnalysisSet getStabilityAnalysisSetWithAnalysisName(String analysisName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#getStabilityAnalysisSetWithMapName(String)
	 */
	public StabilityAnalysisSet getStabilityAnalysisSetWithMapName(String mapName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#getStabilityAnalysisStatistics()
	 */
	public StabilityAnalysisSet getStabilityAnalysisStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#insertStabilityAnalysis(String, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, int, String)
	 */
	public int insertStabilityAnalysis(String mapName,
			SimplifiedGregorianCalendar versionBegin,
			SimplifiedGregorianCalendar versionEnd, int kMapVersions,
			String analysisName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#updateStabilityAnalysis(StabilityAnalysis)
	 */
	public int updateStabilityAnalysis(StabilityAnalysis sa) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see StabilityAPI#deleteStabilityAnalysis(String)
	 */
	public int deleteStabilityAnalysis(String analysisName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	
	/**
	 * @see StabilityAPI#getCorrespondenceStabs(String)
	 */
	public CorrespondenceStabilitySet getCorrespondenceStabs(String analysisName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#getCorrespondenceStabs(String, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar, int)
	 */
	public CorrespondenceStabilitySet getCorrespondenceStabs(String mapName,
			SimplifiedGregorianCalendar versionBegin,
			SimplifiedGregorianCalendar versionEnd, int kMapVersions)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see StabilityAPI#insertCorrespondenceStab(CorrespondenceStability)
	 */
	public int insertCorrespondenceStab(CorrespondenceStability corrStab) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#insertCorrespondenceStabs(CorrespondenceStabilitySet)
	 */
	public int insertCorrespondenceStabs(CorrespondenceStabilitySet set) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see StabilityAPI#deleteCorrespondenceStabs(String)
	 */
	public int deleteCorrespondenceStabs(String analysisName) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

}
