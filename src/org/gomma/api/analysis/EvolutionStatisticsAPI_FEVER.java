package org.gomma.api.analysis;

import java.util.List;
import java.util.Map;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the mapping statistics API interface for the FEVER 
 * system using a relational database at the back-end. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class EvolutionStatisticsAPI_FEVER implements EvolutionStatisticsAPI {

	/**
	 * @see EvolutionStatisticsAPI#getAllEvolutionStats(SourceVersionSet, int, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public Map<Integer, Integer> getAllEvolutionStats(
			SourceVersionSet versions, int action,
			SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getAverageEvolutionStats(SourceVersionSet, int, int, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public float getAverageEvolutionStats(SourceVersionSet versions,
			int action, int period, SimplifiedGregorianCalendar start,
			SimplifiedGregorianCalendar end) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getLDSVersionStats(SourceVersionSet, int)
	 */
	public Map<Integer, Integer> getLDSVersionStats(SourceVersionSet set, int statType) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getLDSVersionStats(int, int)
	 */
	public Map<Integer, Integer> getLDSVersionStats(int ldsID, int type) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getNumberOfObjects(ObjSet)
	 */
	public int getNumberOfObjects(ObjSet set) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getObsoleteMappings(List)
	 */
	public Map<String, List<String>> getObsoleteMappings(List<String> accessions) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getSourceVersionEvolutionList(Source)
	 */
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
	
	/**
	 * @see EvolutionStatisticsAPI#getMappingVersionEvolutionList(Mapping)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#storeEvolutionStats(SourceVersionSet)
	 */
	public void storeEvolutionStats(SourceVersionSet v) throws RepositoryException, WrongSourceException, OperatorException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#storeLDSVersionStats(int)
	 */
	public void storeLDSVersionStats(int ldsID) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#storeLDSVersionStats(SourceVersionSet)
	 */
	public void storeLDSVersionStats(SourceVersionSet versions) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#getAllEvolutionStatsWithAvgData(SourceVersionSet, int, int, SimplifiedGregorianCalendar, SimplifiedGregorianCalendar)
	 */
	public Map<Map<Integer, Integer>, Float> getAllEvolutionStatsWithAvgData(
			SourceVersionSet versions, int action, int period,
			SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end)
			throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see EvolutionStatisticsAPI#storeMapVersionEvolutionStats(MappingVersionSet)
	 */
	public void storeMapVersionEvolutionStats(MappingVersionSet v)
			throws RepositoryException, WrongSourceException, OperatorException {
		throw new RepositoryException("No implementation available");
		
	}

}
