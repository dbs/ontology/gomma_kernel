package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;

/**
 * The interface defines methods for managing source statistics data within the repository.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface SourceStatisticsAPI {

	/**
	 * The method creates and stores source statistics data within the repository.
	 * It return the number of sources for which the statistics data has been stored.
	 * The method takes source version statistics data as input, i.e., this kind of 
	 * statistics data needs to created / updated first in order to receive complete
	 * and up-to-date source statistics data.
	 * @return number of sources for which the statistics data has been created and stored
	 * @throws RepositoryException
	 */
	public int insertSourceStatictics() throws RepositoryException;
	
	/**
	 * The method deletes all source statistics data within the repository and returns
	 * the number of source for which the statistics data has been deleted.
	 * @return number of sources for which the statistics data has been deleted
	 * @throws RepositoryException
	 */
	public int deleteSourceStatistics() throws RepositoryException;
	
	/**
	 * The method returns the source version statistics data.
	 * @return source version statistics data
	 * @throws RepositoryException
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws RepositoryException;
	
	/**
	 * The method returns all source version statistics data from the repository.
	 * @return source version statistics data
	 * @throws RepositoryException
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws RepositoryException;
	
	/**
	 * The method returns the source version statistics data for the specified source 
	 * from the repository.
	 * @param s source for which the statistics data will be retrieved
	 * @return source version statistics data
	 * @throws RepositoryException
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws RepositoryException;
	
	/**
	 * The method returns the source version statistics data for the specified source versions 
	 * from the repository.
	 * @param svSet source versions for which the statistics data will be retrieved
	 * @return source version statistics data
	 * @throws RepositoryException
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(SourceVersionSet svSet) throws RepositoryException;
	
	/**
	 * The method creates and stores source version statistics data within the repository.
	 * It returns the number of source versions for which the statistics data has been stored.
	 * @return number of source versions for which the statistics data has been created and stored
	 * @throws RepositoryException
	 */
	public int insertSourceVersionStatistics() throws RepositoryException;
	
	/**
	 * The method deletes all source version statistics data within the repository and returns
	 * the number of source versions for which the statistics data has been deleted.
	 * @return number of source versions for which the statistics data has been deleted
	 * @throws RepositoryException
	 */
	public int deleteSourceVersionStatistics() throws RepositoryException;
}
