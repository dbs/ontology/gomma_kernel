package org.gomma.api.analysis;

import java.util.List;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.analysis.FrequencyStatisticsFloat;
import org.gomma.model.analysis.FrequencyStatisticsInteger;
import org.gomma.model.mapping.MappingVersion;

public interface CorrespondenceAnalysisAPI {

	public List<FrequencyStatisticsFloat> getConfidenceFrequencies(MappingVersion v) throws RepositoryException;
	
	public List<FrequencyStatisticsInteger> getSupportFrequencies(MappingVersion v) throws RepositoryException;
	
	public List<FrequencyStatisticsInteger> getDomainObjectFrequencies(MappingVersion v) throws RepositoryException;
	
	public List<FrequencyStatisticsInteger> getRangeObjectFrequencies(MappingVersion v) throws RepositoryException;
	
	
}
