package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.mapping.MappingStatisticsSet;

/**
 * The interface defines methods for managing mapping statistics data within the repository. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface MappingStatisticsAPI {

	/**
	 * The method creates and stores the mapping statistics within the repository. 
	 * It returns the number of mappings for which the statistics data has been stored. 
	 * @return number of mappings for which the statistics has been stored 
	 * @throws RepositoryException
	 */
	public int insertMappingStatistics() throws RepositoryException;
	
	/**
	 * The method deletes all stored mapping statistics within the repository
	 * and returns the number of mappings for which the statistics data has been deleted. 
	 * @return number of mappings for which the statistics data has been deleted
	 * @throws RepositoryException
	 */
	public int deleteMappingStatistics() throws RepositoryException;
	
	/**
	 * The method returns the mapping statistics data.
	 * @return mapping statistics data
	 * @throws RepositoryException
	 */
	public MappingStatisticsSet getMappingStatistics() throws RepositoryException;
}
