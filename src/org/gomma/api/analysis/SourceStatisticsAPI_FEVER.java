package org.gomma.api.analysis;

import org.gomma.exceptions.RepositoryException;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceStatisticsSet;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStatisticsSet;

/**
 * The class implements the source statistics interface for managing 
 * source statistics data. It is the implementation for the FEVER system 
 * using a relational database at the back-end.
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public final class SourceStatisticsAPI_FEVER implements SourceStatisticsAPI {

	

	/**
	 * @see SourceStatisticsAPI#getSourceStatisticsSet()
	 */
	public SourceStatisticsSet getSourceStatisticsSet() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatisticsSet()
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatisticsSet(Source)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(Source s) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#insertSourceStatictics()
	 */
	public int insertSourceStatictics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#insertSourceVersionStatistics()
	 */
	public int insertSourceVersionStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#deleteSourceStatistics()
	 */
	public int deleteSourceStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#deleteSourceVersionStatistics()
	 */
	public int deleteSourceVersionStatistics() throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}

	/**
	 * @see SourceStatisticsAPI#getSourceVersionStatisticsSet(SourceVersionSet)
	 */
	public SourceVersionStatisticsSet getSourceVersionStatisticsSet(
			SourceVersionSet svSet) throws RepositoryException {
		throw new RepositoryException("No implementation available");
	}
}
