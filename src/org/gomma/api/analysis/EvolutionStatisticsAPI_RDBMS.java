package org.gomma.api.analysis;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.gomma.api.APIFactory;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.RepositoryException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.MappingVersionEvolution;
import org.gomma.model.mapping.MappingVersionSet;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionEvolution;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.operator.EvolutionOperators;
import org.gomma.operator.StatisticsOperators;
import org.gomma.util.date.SimplifiedGregorianCalendar;

/**
 * The class implements the evolution statistics API interface. It is the default 
 * implementation for a default relational database. 
 * @author:  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version: 1.0
 * @since:   JDK1.5
 */
public class EvolutionStatisticsAPI_RDBMS implements EvolutionStatisticsAPI {

	private static final String LOAD_OBJECT_NUMBER = 
		"select count(*) from ("+
			"select distinct c.accession "+
			"from gomma_lds_versions a, gomma_lds b, gomma_obj_versions c "+
			"where a.lds_id_fk=b.lds_id and b.lds_id=c.lds_id_fk and "+
			"a.lds_date between c.date_from and date_to "+
			"and a.lds_version_id in ([ldsVersionIdList])) t";
	
	private static final String LOAD_LDS_EVOLUTION_STATS =
		"select from_lds_version_id_fk, to_lds_version_id_fk, "+
				"numberOfAddedObjects, numberOfDeletedObjects, numberOfFusedObjects, numberOfToObsoleteObjects "+
		"from gomma_evol_stats";
	
	private static final String LOAD_MAP_VERSION_EVOLUTION_STATS =
		"select from_map_version_id_fk, to_map_version_id_fk, "+
				"numberOfAddedCorrs, numberOfDeletedCorrs, numberOfSameCorrs, addedCorrsFraction,deletedCorrsFraction," +
				"numberOfAddedDomainObjs, numberOfDeletedDomainObjs, numberOfSameDomainObjs, addedDomainObjsFraction,deletedDomainObjsFraction," +
				"numberOfAddedRangeObjs, numberOfDeletedRangeObjs, numberOfSameRangeObjs, addedRangeObjsFraction,deletedRangeObjsFraction "+
		"from gomma_map_version_evol_stats";
	

	public EvolutionStatisticsAPI_RDBMS() {}
	
	
	
	public Map<Integer,Integer> getLDSVersionStats(SourceVersionSet set, int statType) throws RepositoryException {
		return this.getLDSVersionStats(set.getEarliestSourceVersion().getSource().getID(),statType);
	}
	
	public Map<Integer,Integer> getLDSVersionStats(int ldsID, int type) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = "select lds_version_id_fk, num_objs, num_rels "+
						"from gomma_lds_version_stats where lds_id_fk = '"+ldsID+"'";
		Map <Integer,Integer> result = new Int2IntOpenHashMap();
		ResultSet rs = null;
		
		try {
			rs = dbh.executeSelect(query);
			
			while (rs.next()) {
				int ldsVersionID = rs.getInt(1);
				if (type==StatisticsOperators.OBJECTS) {
					result.put(ldsVersionID,rs.getInt(2));
				} else if (type==StatisticsOperators.RELATIONSHIPS) {
					result.put(ldsVersionID,rs.getInt(3));
				}
			}
			dbh.closeStatement(rs);
			return result;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	
	
	/**
	 * This method generates statistics about the number of objects and relationships in a logical data
	 * source with ID ldsID
	 * 
	 * @param ldsID the ID of the logical data source
	 * @throws RepositoryException
	 */
	public void storeLDSVersionStats(int ldsID) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		try {
			String sqlQuery = "delete from gomma_lds_version_stats where lds_id_fk = '"+ldsID+"'";
			dbh.executeDml(sqlQuery);
			sqlQuery = "select lds_version_id, count(*) "+
						"from gomma_obj_versions a, gomma_lds_versions b "+
						"where a.lds_id_fk = b.lds_id_fk "+
						"and b.lds_date between a.date_from and a.date_to "+
						"and a.lds_id_fk = '"+ldsID+"' "+
						"group by lds_version_id";
			ResultSet rs= dbh.executeSelect(sqlQuery);
			
			Map<Integer,Integer> numberOfObjects = new Int2IntOpenHashMap();
			while (rs.next()) numberOfObjects.put(rs.getInt(1),rs.getInt(2));
			dbh.closeStatement(rs);
			
			sqlQuery = "select lds_version_id, count(*) "+
						"from gomma_lds_structure a, gomma_lds_versions b "+
						"where a.lds_id_fk = b.lds_id_fk "+
						"and b.lds_date between a.date_from and a.date_to "+
						"and a.lds_id_fk = '"+ldsID+"' "+
						"group by lds_version_id";
			rs= dbh.executeSelect(sqlQuery);

			Map<Integer,Integer> numberOfRelationships = new HashMap<Integer,Integer>();
			while (rs.next()) numberOfRelationships.put(rs.getInt(1),rs.getInt(2));
			dbh.closeStatement(rs);
			
			PreparedStatement pStmt = dbh.prepareStatement("insert into gomma_lds_version_stats (lds_id_fk,lds_version_id_fk,num_objs,num_rels) values (?,?,?,?)");
			for (int key : numberOfObjects.keySet()) {
				pStmt.setInt(1,ldsID);
				pStmt.setInt(2,key);
				pStmt.setInt(3,numberOfObjects.get(key));
				if (numberOfRelationships.get(key)==null) {
					pStmt.setInt(4,0);
				} else {
					pStmt.setInt(4,numberOfRelationships.get(key));
				}
				pStmt.addBatch();
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
			
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	public float getAverageEvolutionStats(SourceVersionSet set, int action, int period, SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		SourceVersion firstVersion = set.getEarliestSourceVersion();
		if (start==null||start.before(firstVersion.getCreationVersionDate())) 
			start = firstVersion.getCreationVersionDate();
		
		if (end==null||end.after(new GregorianCalendar())) 
			end = set.getLatestSourceVersion().getCreationVersionDate();
		
		List<Integer> matchingSourceVersionIDs = new Vector<Integer>();
		for (int id : set.getIDSet()) {
			SimplifiedGregorianCalendar sourceVersionDate = set.getSourceVersion(id).getCreationVersionDate();
			if ((sourceVersionDate.after(start)||sourceVersionDate.equals(start))&&(sourceVersionDate.before(end)||sourceVersionDate.equals(end))) {
				matchingSourceVersionIDs.add(id);
			}
		}
		if (matchingSourceVersionIDs.size()==0)
			return 0;
		
		String sqlQuery = "select to_lds_version_id_fk,numberOfAddedObjects,numberOfDeletedObjects,numberOfToObsoleteObjects,numberOfFusedObjects "+
							"from gomma_evol_stats "+
							"where lds_id_fk = '"+firstVersion.getSource().getID()+"' and to_lds_version_id_fk in (";
		for (int key : matchingSourceVersionIDs) {
			sqlQuery += key + ",";
		}
		sqlQuery += "-1);";
		
		TreeMap<Integer,Integer> results = new TreeMap<Integer,Integer>();
		ResultSet rs = null;
		try {
			rs = dbh.executeSelect(sqlQuery);
			
			while (rs.next()) {
				int sourceVersionKey = rs.getInt(1);
				int numberOfChangedObjects = 0;
				if (action==StatisticsOperators.ADD_ACTION) {
					numberOfChangedObjects = rs.getInt(2);
				} else if (action==StatisticsOperators.DEL_ACTION) {
					numberOfChangedObjects = rs.getInt(3);
				} else if (action==StatisticsOperators.TOOBS_ACTION) {
					numberOfChangedObjects = rs.getInt(4);
				} else if (action==StatisticsOperators.FUSE_ACTION) {
					numberOfChangedObjects = rs.getInt(5);
				}
				results.put(sourceVersionKey,numberOfChangedObjects);
			}
			dbh.closeStatement(rs);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		float observationPeriod = end.getTimeInMillis()-start.getTimeInMillis();
		
		float monthPeriod = 30f*24f*3600000f;
		float quartalPeriod = 90f*24f*3600000f;
		float halfyearPeriod = 180f*24f*3600000f;
		float yearPeriod = 365f*24f*3600000f;
		
		float numberOfChangesInObservationPeriod = 0;
		for (int key : results.keySet()) {
			numberOfChangesInObservationPeriod += results.get(key);
		}
		
		if (period==StatisticsOperators.MONTH_PERIOD) {
			return numberOfChangesInObservationPeriod/observationPeriod*monthPeriod;
		} else if (period==StatisticsOperators.QUARTAL_PERIOD) {
			return numberOfChangesInObservationPeriod/observationPeriod*quartalPeriod;
		} else if (period==StatisticsOperators.HALFYEAR_PERIOD) {
			return numberOfChangesInObservationPeriod/observationPeriod*halfyearPeriod;
		} else if (period==StatisticsOperators.YEAR_PERIOD) {
			return numberOfChangesInObservationPeriod/observationPeriod*yearPeriod;
		}
		return 0;
	}
	
	public Map<Integer,Integer> getAllEvolutionStats(SourceVersionSet set, int action, SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		SourceVersion firstVersion = set.getEarliestSourceVersion();
		if (start==null||start.before(firstVersion.getCreationVersionDate())) 
			start = firstVersion.getCreationVersionDate();
		
		if (end==null||end.after(new GregorianCalendar())) 
			end = set.getLatestSourceVersion().getCreationVersionDate();
		
		List<Integer> matchingSourceVersionIDs = new Vector<Integer>();
		for (int id : set.getIDSet()) {
			SimplifiedGregorianCalendar sourceVersionDate = set.getSourceVersion(id).getCreationVersionDate();
			if ((sourceVersionDate.after(start)||sourceVersionDate.equals(start))&&(sourceVersionDate.before(end)||sourceVersionDate.equals(end))) {
				matchingSourceVersionIDs.add(id);
			}
		}
		if (matchingSourceVersionIDs.size()==0)
			return null;
		
		String sqlQuery = "select to_lds_version_id_fk,numberOfAddedObjects,numberOfDeletedObjects,numberOfToObsoleteObjects,numberOfFusedObjects "+
							"from gomma_evol_stats "+
							"where lds_id_fk = '"+firstVersion.getSource().getID()+"' and to_lds_version_id_fk in (";
		for (int key : matchingSourceVersionIDs) {
			sqlQuery += key + ",";
		}
		sqlQuery += "-1);";
		
		Map<Integer,Integer> results = new Int2IntOpenHashMap();
		ResultSet rs = null;
		try {
			rs = dbh.executeSelect(sqlQuery);
			
			while (rs.next()) {
				int sourceVersionKey = rs.getInt(1);
				int numberOfChangedObjects = 0;
				if (action==StatisticsOperators.ADD_ACTION) {
					numberOfChangedObjects = rs.getInt(2);
				} else if (action==StatisticsOperators.DEL_ACTION) {
					numberOfChangedObjects = rs.getInt(3);
				} else if (action==StatisticsOperators.TOOBS_ACTION) {
					numberOfChangedObjects = rs.getInt(4);
				} else if (action==StatisticsOperators.FUSE_ACTION) {
					numberOfChangedObjects = rs.getInt(5);
				}
				results.put(sourceVersionKey,numberOfChangedObjects);
			}
			dbh.closeStatement(rs);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return results;
	}

	public Map<Map<Integer,Integer>,Float> getAllEvolutionStatsWithAvgData(SourceVersionSet set, int action,int period,SimplifiedGregorianCalendar start, SimplifiedGregorianCalendar end) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		
		SourceVersion firstVersion = set.getEarliestSourceVersion();
		if (start==null||start.before(firstVersion.getCreationVersionDate())) 
			start = firstVersion.getCreationVersionDate();
		
		if (end==null||end.after(new GregorianCalendar())) 
			end = set.getLatestSourceVersion().getCreationVersionDate();
		
		List<Integer> matchingSourceVersionIDs = new Vector<Integer>();
		for (int id : set.getIDSet()) {
			SimplifiedGregorianCalendar sourceVersionDate = set.getSourceVersion(id).getCreationVersionDate();
			if ((sourceVersionDate.after(start)||sourceVersionDate.equals(start))&&(sourceVersionDate.before(end)||sourceVersionDate.equals(end))) {
				matchingSourceVersionIDs.add(id);
			}
		}
		if (matchingSourceVersionIDs.size()==0)
			return null;
		
		String sqlQuery = "select to_lds_version_id_fk,numberOfAddedObjects,numberOfDeletedObjects,numberOfToObsoleteObjects,numberOfFusedObjects "+
							"from gomma_evol_stats "+
							"where lds_id_fk = '"+firstVersion.getSource().getID()+"' and to_lds_version_id_fk in (";
		for (int key : matchingSourceVersionIDs) {
			sqlQuery += key + ",";
		}
		sqlQuery += "-1);";
		
		Map<Integer,Integer> results = new Int2IntOpenHashMap();
		ResultSet rs = null;
		try {
			rs = dbh.executeSelect(sqlQuery);
			
			while (rs.next()) {
				int sourceVersionKey = rs.getInt(1);
				int numberOfChangedObjects = 0;
				if (action==StatisticsOperators.ADD_ACTION) {
					numberOfChangedObjects = rs.getInt(2);
				} else if (action==StatisticsOperators.DEL_ACTION) {
					numberOfChangedObjects = rs.getInt(3);
				} else if (action==StatisticsOperators.TOOBS_ACTION) {
					numberOfChangedObjects = rs.getInt(4);
				} else if (action==StatisticsOperators.FUSE_ACTION) {
					numberOfChangedObjects = rs.getInt(5);
				}
				results.put(sourceVersionKey,numberOfChangedObjects);
			}
			dbh.closeStatement(rs);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
        float observationPeriod = end.getTimeInMillis()-start.getTimeInMillis();
		
		float monthPeriod = 30f*24f*3600000f;
		float quartalPeriod = 90f*24f*3600000f;
		float halfyearPeriod = 180f*24f*3600000f;
		float yearPeriod = 365f*24f*3600000f;
		
		float numberOfChangesInObservationPeriod = 0;
		for (int key : results.keySet()) {
			numberOfChangesInObservationPeriod += results.get(key);
		}
		float avg=0F;
		if (period==StatisticsOperators.MONTH_PERIOD) {
			avg=numberOfChangesInObservationPeriod/observationPeriod*monthPeriod;
		} else if (period==StatisticsOperators.QUARTAL_PERIOD) {
			avg= numberOfChangesInObservationPeriod/observationPeriod*quartalPeriod;
		} else if (period==StatisticsOperators.HALFYEAR_PERIOD) {
			avg=numberOfChangesInObservationPeriod/observationPeriod*halfyearPeriod;
		} else if (period==StatisticsOperators.YEAR_PERIOD) {
			avg=numberOfChangesInObservationPeriod/observationPeriod*yearPeriod;
		}
		
		Map<Map<Integer,Integer>,Float> map=new HashMap<Map<Integer,Integer>,Float>();
		map.put(results, avg);
		return map;
	}
	
	
	/**
	 * TThis method generates statistics about the number of objects and relationships in a logical data
	 * source with consisting of a physical source and an object type.
	 * 
	 * @param pdsName name of the physical source
	 * @param objectType name of the object type
	 * @throws RepositoryException
	 */
	public void storeLDSVersionStats(SourceVersionSet set) throws RepositoryException {
		if (set.size()>0) {
			int oneVersionID = (Integer)set.getIDSet().toArray()[0];
			this.storeLDSVersionStats(set.getSourceVersion(oneVersionID).getSource().getID());
		}
	}
	
	public void storeEvolutionStats(SourceVersionSet set) throws RepositoryException, WrongSourceException, OperatorException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String cleanQuery = "delete from gomma_evol_stats where lds_id_fk =";
		String insertQuery = "insert into gomma_evol_stats "+
								"(lds_id_fk,from_lds_version_id_fk,to_lds_version_id_fk,"+
								"numberOfAddedObjects,numberOfDeletedObjects,numberOfToObsoleteObjects,numberOfFusedObjects) "+
							 "values (?,?,?,?,?,?,?)";
		EvolutionOperators evo_op = EvolutionOperators.getInstance();
		PreparedStatement pStmt = null;
		
		if (set.size()==0) return;
		
		try {
			SourceVersion oldVersion = set.getEarliestSourceVersion();
			SourceVersion latestVersion = set.getLatestSourceVersion();
			ObjSet oldVersionObjs = APIFactory.getInstance().getObjectAPI().getObjectSet(oldVersion);
			SourceVersion newVersion;
			ObjSet newVersionObjs;
			boolean isFinished = false;
			
			dbh.executeDml(cleanQuery+Integer.toString(oldVersion.getSource().getID()));
			pStmt = dbh.prepareStatement(insertQuery);
			
			while (!isFinished) {
				newVersion = set.getNextOlderSourceVersion(oldVersion.getCreationVersionDate());
				newVersionObjs = APIFactory.getInstance().getObjectAPI().getObjectSet(newVersion);
				
				int addedObjects = evo_op.addedObjects(oldVersionObjs,newVersionObjs).size();
				int deletedObjects = evo_op.deletedObjects(oldVersionObjs,newVersionObjs).size();
				int toObsoleteObjects = evo_op.toObsoleteObjects(oldVersionObjs,newVersionObjs).size();
				int fusedObjects = evo_op.fusedObjects(oldVersionObjs,newVersionObjs).size();
				
				pStmt.setInt(1,newVersion.getSource().getID());
				pStmt.setInt(2,oldVersion.getID());
				pStmt.setInt(3,newVersion.getID());
				pStmt.setInt(4,addedObjects);
				pStmt.setInt(5,deletedObjects);
				pStmt.setInt(6,toObsoleteObjects);
				pStmt.setInt(7,fusedObjects);
				
				pStmt.addBatch();
				
				oldVersion = newVersion;
				oldVersionObjs = newVersionObjs;
				isFinished = oldVersion.equals(latestVersion);
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	public void storeMapVersionEvolutionStats(MappingVersionSet set) throws RepositoryException, WrongSourceException, OperatorException {
		
		List<MappingVersion> l=set.sort();
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String cleanQuery = "delete from gomma_map_version_evol_stats where map_id_fk =";
		String insertQuery = "insert into gomma_map_version_evol_stats "+
								"(map_id_fk,from_map_version_id_fk,to_map_version_id_fk,"+
								"numberOfAddedCorrs,numberOfDeletedCorrs,numberOfSameCorrs,addedCorrsFraction,deletedCorrsFraction, " +
								"numberOfAddedDomainObjs,numberOfDeletedDomainObjs,numberOfSameDomainObjs,addedDomainObjsFraction,deletedDomainObjsFraction," +
								"numberOfAddedRangeObjs,numberOfDeletedRangeObjs,numberOfSameRangeObjs,addedRangeObjsFraction,deletedRangeObjsFraction ) "+
							 "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		EvolutionOperators evo_op = EvolutionOperators.getInstance();
		PreparedStatement pStmt = null;
		
		if (l.size()==0) return;
		dbh.executeDml(cleanQuery+Integer.toString(l.iterator().next().getMapping().getID()));
		pStmt = dbh.prepareStatement(insertQuery);
		try {
		   for(int i=0;i<l.size();i++){
			if(i+1==l.size())break;
			else{
				ObjCorrespondenceSet c1=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(l.get(i));
				ObjCorrespondenceSet c2=APIFactory.getInstance().getObjCorrespondenceAPI().getObjectCorrespondenceSet(l.get(i+1));
				
				int numberOfAddedCorrs=evo_op.addedCorrespondences(c1, c2).size();
				int numberOfDeletedCorrs=evo_op.deletedCorrespondences(c1, c2).size();
				int numberOfSameCorrs=evo_op.sameCorrespondences(c1, c2).size();
				float addedCorrFraction=(float)Math.round(evo_op.getAddedCorrespondenceFraction(c1, c2)*100)/100;
				float deletedCorrFraction=(float)Math.round(evo_op.getDeletedCorrespondenceFraction(c1, c2)*100)/100;
				int numberOfAddedDomainObjs=evo_op.addedObjects(c1, c2,true).size();
				int numberOfDeletedDomainObjs=evo_op.deletedObjects(c1,c2,true).size();
				int numberOfSameDomainObjs=evo_op.sameObjects(c1,c2,true).size();
				float addedDomainObjsFraction=(float)Math.round(evo_op.getAddedObjectFraction(c1,c2,true)*100)/100;
				float deletedDomainObjsFraction=(float)Math.round(evo_op.getDeletedObjectFraction(c1,c2,true)*100)/100;
				int numberOfAddedRangeObjs=evo_op.addedObjects(c1,c2,false).size();
				int numberOfDeletedRangeObjs=evo_op.deletedObjects(c1,c2,false).size();
				int numberOfSameRangeObjs=evo_op.sameObjects(c1,c2,false).size();
				float addedRangeObjsFraction=(float)Math.round(evo_op.getAddedObjectFraction(c1,c2,false)*100)/100;
				float deletedRangeObjsFraction=(float)Math.round(evo_op.getDeletedObjectFraction(c1,c2,false)*100)/100;
				
				pStmt.setInt(1,l.get(i).getMapping().getID());
				pStmt.setInt(2,l.get(i).getID());
				pStmt.setInt(3,l.get(i+1).getID());
				pStmt.setInt(4,numberOfAddedCorrs);
				pStmt.setInt(5,numberOfDeletedCorrs);
				pStmt.setInt(6, numberOfSameCorrs);
				pStmt.setFloat(7,addedCorrFraction);
				pStmt.setFloat(8,deletedCorrFraction);
				pStmt.setInt(9,numberOfAddedDomainObjs);
				pStmt.setInt(10,numberOfDeletedDomainObjs);
				pStmt.setInt(11, numberOfSameDomainObjs);
				pStmt.setFloat(12,addedDomainObjsFraction);
				pStmt.setFloat(13,deletedDomainObjsFraction);
				pStmt.setInt(14,numberOfAddedRangeObjs);
				pStmt.setInt(15,numberOfDeletedRangeObjs);
				pStmt.setInt(16, numberOfSameRangeObjs);
				pStmt.setFloat(17,addedRangeObjsFraction);
				pStmt.setFloat(18,deletedRangeObjsFraction);
				
				pStmt.addBatch();
					 
				}
			}
			pStmt.executeBatch();
			dbh.closeStatement(pStmt);
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	public int getNumberOfObjects(ObjSet set) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		String query = LOAD_OBJECT_NUMBER.replaceAll("\\[ldsVersionIdList\\]",set.getSourceVersionSet().getSeparatedIDString(","));
		ResultSet rs = null;
		
		try {
			int nObjs = Integer.MAX_VALUE;
			rs = dbh.executeSelect(query);
			if (rs.next()) nObjs=rs.getInt(1);
			dbh.closeStatement(rs);
			return nObjs;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage()); 
		} 
	}
	/**
	 * This method loads obsolete mappings for obsolete marked concepts.
	 * 
	 * @param accessions concepts for which mappings are retrieved
	 * @throws RepositoryException
	 */
	public Map<String,List<String>> getObsoleteMappings(List<String> accessions) throws RepositoryException {
		
		DatabaseHandler dbh = DatabaseHandler.getInstance();
		Map<String, List<String>> result = new Object2ObjectOpenHashMap<String, List<String>>();
		
		if (accessions.size()==0) return result;
		
		try {
			StringBuffer queryCondition = new StringBuffer("(");
			for (int i=0;i<accessions.size();i++) {
				queryCondition.append("'"+accessions.get(i)+"',");
			}
			queryCondition.append("'xy')");
			
			ResultSet rs = null;
			rs = dbh.executeSelect("Select from_accession, to_accession from gomma_obj_obsolete_mappings where from_accession IN "+queryCondition+" order by from_accession");
			
			while (rs.next()) {
				String currentFromAccession = rs.getString(1);
				String currentToAccession = rs.getString(2);
				List<String> resultEntry = result.get(currentFromAccession);
				if (resultEntry==null) {
					resultEntry = new Vector<String>();
				}
				if (!resultEntry.contains(currentToAccession)) {
					resultEntry.add(currentToAccession);
					result.put(currentFromAccession, resultEntry);
				}
			}
			return result;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
	}
	
	/**
	 * @see EvolutionStatisticsManager#getSourceVersionEvolutionList(Source)
	 */
	public List<SourceVersionEvolution> getSourceVersionEvolutionList(Source s) throws RepositoryException {
		return this.loadSourceVersionEvolutionList(" where lds_id_fk="+s.getID());
	}
	
	private List<SourceVersionEvolution> loadSourceVersionEvolutionList(String queryCond) throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		ResultSet rs = dbh.executeSelect(LOAD_LDS_EVOLUTION_STATS+queryCond);
		List<SourceVersionEvolution> list = this.extractList(rs, 
			APIFactory.getInstance().getSourceVersionAPI().getSourceVersionSet());
		dbh.closeStatement(rs);
		
		return list;
	}
	
	/**
	 * @see EvolutionStatisticsManager#getMappingVersionEvolutionList(Mapping)
	 */
	public List<MappingVersionEvolution> getMappingVersionEvolutionList(Mapping m) throws RepositoryException {
		return this.loadMappingVersionEvolutionList(" where map_id_fk="+m.getID(),m);
	}
	
	private List<MappingVersionEvolution> loadMappingVersionEvolutionList(String queryCond,Mapping m) throws RepositoryException {

		DatabaseHandler dbh = DatabaseHandler.getInstance();
		ResultSet rs = dbh.executeSelect(LOAD_MAP_VERSION_EVOLUTION_STATS+queryCond);
		List<MappingVersionEvolution> list = this.extractList(rs, 
			APIFactory.getInstance().getMappingVersionAPI().getMappingVersionSet(m));
		dbh.closeStatement(rs);
		
		return list;
	}
	
	private List<MappingVersionEvolution> extractList(ResultSet rs, MappingVersionSet set) throws RepositoryException {
		List<MappingVersionEvolution> list = new ArrayList<MappingVersionEvolution>();
		
		try {
			while (rs.next()) {
				if (!(set.contains(rs.getInt(1)) && set.contains(rs.getInt(2)))) continue;
				
				list.add(new MappingVersionEvolution.Builder(
						set.getMappingVersion(rs.getInt(1)),set.getMappingVersion(rs.getInt(2)))
						.numberOfAddedCorrs(rs.getInt(3))
						.numberOfDeletedCorrs(rs.getInt(4))
						.numberOfSameCorrs(rs.getInt(5))
						.addedCorrFraction(rs.getFloat(6))
						.deletedCorrFraction(rs.getFloat(7))
						.numberOfAddedDomainObjs(rs.getInt(8))
						.numberOfDeletedDomainObjs(rs.getInt(9))
						.numberOfSameDomainObjs(rs.getInt(10))
						.addedDomainObjsFraction(rs.getFloat(11))
						.deletedDomainObjsFraction(rs.getFloat(12))
						.numberOfAddedRangeObjs(rs.getInt(13))
						.numberOfDeletedRangeObjs(rs.getInt(14))
						.numberOfSameRangeObjs(rs.getInt(15))
						.addedRangeObjsFraction(rs.getFloat(16))
						.deletedRangeObjsFraction(rs.getFloat(17))
						.build());
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return list;
	}
	
	private List<SourceVersionEvolution> extractList(ResultSet rs, SourceVersionSet set) throws RepositoryException {
		List<SourceVersionEvolution> list = new ArrayList<SourceVersionEvolution>();
		
		try {
			while (rs.next()) {
				if (!(set.contains(rs.getInt(1)) && set.contains(rs.getInt(2)))) continue;
				
				list.add(new SourceVersionEvolution.Builder(
						set.getSourceVersion(rs.getInt(1)),set.getSourceVersion(rs.getInt(2)))
						.numberOfAddedObjects(rs.getInt(3))
						.numberOfDeletedObjects(rs.getInt(4))
						.numberOfFusedObjects(rs.getInt(5))
						.numberOfToObsoleteObjects(rs.getInt(6)).build());
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage());
		}
		
		return list;
	}
}
