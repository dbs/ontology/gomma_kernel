/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.worker;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;

import org.gomma.Gomma;
import org.gomma.GommaImpl;
import org.gomma.exceptions.GommaException;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.WorkPackage;
import org.gomma.model.work.WorkPackageSet;
import org.gomma.rmi.GommaAdminClient;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public class MatchWorkerHandler {

	private Gomma gommaService;
	private boolean isLocal;
	
	public MatchWorkerHandler(String confFileNameLoc) throws GommaException, RemoteException {
		this.gommaService = new GommaImpl();
		this.gommaService.initialize(confFileNameLoc);
		this.isLocal = true;
	}
	
	public MatchWorkerHandler(String hostName, int port, String serviceName) throws GommaException, RemoteException, MalformedURLException, NotBoundException {
		this.gommaService = new GommaAdminClient();
		GommaAdminClient.class.cast(this.gommaService).connect(hostName,port,serviceName);
		this.isLocal = false;
	}
	
	public void disconnect() throws GommaException, RemoteException {
		if (this.isLocal) this.gommaService.shutdown();
		else GommaAdminClient.class.cast(this.gommaService).disconnect();
	}
	
	public void work(String packageName) throws RemoteException, GommaException {
		DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		String uri = null;
		
		try {
			uri = java.net.InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) { uri = "unknown"; }
		
		
		AbstractMatchWorker worker;
		MatchTask task;
		boolean killMe=false;
		
		WorkPackageSet workPackages = this.gommaService.getWorkManager().getWorkPackageSet(packageName);
		
		if (workPackages.size()==0)
			throw new GommaException("Could not find any work package '"+packageName+"'.");
		
		if (workPackages.size()>1)
			throw new GommaException("There are multiple work packages using the name '"+packageName+"'.");
		
		WorkPackage workPackage = workPackages.getCollection().iterator().next();
		
		while (!killMe) {
			try {
				task = this.gommaService.getWorkManager().getNextMatchTask(workPackage,uri);
				if (task==null) {
					killMe=true;
					continue;
				}
				
				task.setStartTime(SimplifiedGregorianCalendar.getInstance());
				
				System.out.println("\n["+sdf.format(GregorianCalendar.getInstance().getTime())+
						"] Working on task "+task.getName()+" (id='"+task.getID()+"')");
				
				worker = WorkerFactory.getMatchWorker(this.gommaService,task);
				worker.execute();
				
				System.out.println("\n["+sdf.format(GregorianCalendar.getInstance().getTime())+
						"] Work on task "+task.getName()+" (id='"+task.getID()+"') finished.");
				
				task.setEndTime(SimplifiedGregorianCalendar.getInstance());
				this.gommaService.getWorkManager().updateMatchTaskStatus(worker.getMatchTask());
				
			} catch (NoSuchElementException e) {
				killMe=true;
			}
		}
	}
	
	public static String title() {
		return "MatchWorkerHandler\n"+
        		"IZBI/IMISE Leipzig, Version 0.8, 2008-2009\n"+
        		"Toralf Kirsten\n\n";
	}
	
	public static String usage() {
		return "MatchWorkerHandler -h=<hostName> -p=<port> -s=<serviceName> -w=<work package name>\n"+
        		"IZBI/IMISE Leipzig, Version 0.8, 2008-2009\n"+
        		"Toralf Kirsten\n\n";
	}
	
	public static void main(String[] args) {
		try {
			if (!(args!=null && (args.length==4 || args.length==2))) {
				System.out.println(MatchWorkerHandler.usage());
				return;
			}
			
			String hostName=null, serviceName=null, packageName=null, configFileName=null;
			int port=0;
			
			for (String arg : args) {
				if (arg.startsWith("-h=")) { hostName=arg.substring(3); continue; }
				else if (arg.startsWith("-p=")) { port=Integer.parseInt(arg.substring(3)); continue; }
				else if (arg.startsWith("-s=")) { serviceName=arg.substring(3); }
				else if (arg.startsWith("-w="))  { packageName=arg.substring(3); continue; }
				else if (arg.startsWith("-conf=")) { configFileName = arg.substring(6);}
				else System.out.println("Unknown parameter '"+arg+"'.\n");
			}
			
			if (hostName!=null && serviceName!=null && packageName!=null) {
				System.out.println(MatchWorkerHandler.title());
				long start = System.currentTimeMillis();
				
				MatchWorkerHandler worker = new MatchWorkerHandler(hostName,port,serviceName);
				worker.work(packageName);
				worker.disconnect();
				
				System.out.println("\n\nMatch work for package '"+
						packageName+"' finished in "+((System.currentTimeMillis()-start)/1000)+"s.");
				return;
			} else if (configFileName!=null && packageName!=null) {
				System.out.println(MatchWorkerHandler.title());
				long start = System.currentTimeMillis();
				
				MatchWorkerHandler worker = new MatchWorkerHandler(configFileName);
				worker.work(packageName);
				worker.disconnect();
				
				System.out.println("\n\nMatch work for package '"+
						packageName+"' finished in "+((System.currentTimeMillis()-start)/1000)+"s.");
				return;
			}
			System.out.println(MatchWorkerHandler.usage());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
