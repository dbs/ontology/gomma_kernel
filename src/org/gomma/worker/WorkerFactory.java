/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.worker;

import java.util.Properties;

import org.gomma.GommaManager;
import org.gomma.exceptions.GommaException;
import org.gomma.model.work.MatchTask;

public class WorkerFactory {

	public static AbstractMatchWorker getMatchWorker(GommaManager gommaServer, MatchTask task) throws GommaException {
		AbstractMatchWorker worker;
		Properties props = task.getTaskConfigurationProperties(";");
		String className = props.getProperty("className");
		
		try {
			
			worker = (AbstractMatchWorker)Class.forName(className).newInstance();
			worker.initialize(gommaServer,task);
			return worker;
			
		} catch (ClassNotFoundException e) {
			throw new GommaException(e.getMessage());
		} catch (InstantiationException e) {
			throw new GommaException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new GommaException(e.getMessage());
		}
	}
}
