/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma.worker;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gomma.GommaManager;
import org.gomma.api.util.QueryMatchOperations;
import org.gomma.exceptions.GommaException;
import org.gomma.io.FileTypes;
import org.gomma.model.IDObject;
import org.gomma.model.IDObjectSet;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.MappingSet;
import org.gomma.model.mapping.MappingVersion;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.model.work.MatchTask;
import org.gomma.model.work.TaskStatus;
import org.gomma.util.date.SimplifiedGregorianCalendar;

public abstract class AbstractMatchWorker implements Worker {

	private static final String NL  = "\n";
	private static final String TAB = "\t";
		
	protected GommaManager gommaServer;
	protected MatchTask matchTask;
	
	public AbstractMatchWorker() {}
	
	public void initialize(GommaManager server, MatchTask task) {
		this.gommaServer=server;
		this.matchTask=task;
	}
	
	public MatchTask getMatchTask() {
		return this.matchTask;
	}
	
	public void saveMappingVersion(String mapName, String mapVersionName, float minConf, int minSup, ObjCorrespondenceSet objCorrs) throws GommaException, RemoteException {
		MappingSet mapSet = this.gommaServer.getMappingManager().getMappingSet(mapName, QueryMatchOperations.EXACT_MATCH);
		Mapping m = null;
		
		if (mapSet.size()==0) {
			m = this.createMapping(mapName,objCorrs.getDomainLDSID(),objCorrs.getRangeLDSID());
		} else if (mapSet.size()>1)
			throw new GommaException("Too many mappings available using the same name");
		else m = mapSet.getMapping(mapName);
		
		this.gommaServer.getMappingManager().insertMappingVersion(m, mapVersionName, SimplifiedGregorianCalendar.getInstance(), minConf, minSup, objCorrs);
	}
	
	public void saveMappingInTSV(String fileName, ObjCorrespondenceSet objCorrs) throws IOException {
		StringBuffer buf = new StringBuffer();
		FileWriter writer = new FileWriter(fileName);
		
		for (ObjCorrespondence corr : objCorrs.getCollection()) {
			buf.append(corr.getDomainAccessionNumber()).append(TAB)
			   .append(corr.getRangeAccessionNumber()).append(TAB)
			   .append(corr.getConfidence()).append(TAB)
			   .append(corr.getSupport()).append(NL);
			if (buf.length()>20000) {
				writer.write(buf.toString());
				buf = new StringBuffer();
			}
		}
		writer.write(buf.toString());
		writer.flush();
		writer.close();
	}
	
	
	public void saveMappingInXML(String fileName, Mapping m, String name, float minConf, int minSup, ObjCorrespondenceSet objCorrs) throws GommaException, RemoteException {
		
		MappingVersion v = new MappingVersion.Builder(MappingVersion.DEFAULT_MAPPING_VERSION_ID, m)
								.name(name)
								.creationDate(SimplifiedGregorianCalendar.getInstance())
								.lowestConfidence(minConf)
								.lowestSupport(minSup)
								.numberOfCorrespondences(objCorrs.size())
								.numberOfDomainObjects(objCorrs.getDomainObjIds().size())
								.numberOfRangeObjects(objCorrs.getRangeObjIds().size())
								.build();

		SourceVersionSet set = this.gommaServer.getSourceManager().getSourceVersionSet();
		for (int versionID : objCorrs.getDomainSourceVersionIDSet())
			v.addDomainSourceVersion(set.getSourceVersion(versionID));
		for (int versionID : objCorrs.getRangeSourceVersionIDSet())
			v.addRangeSourceVersion(set.getSourceVersion(versionID));
		
		this.gommaServer.getMappingManager().exportMappingVersion(fileName,v,objCorrs,FileTypes.XML);
	}
	
	
	public void showCorrespondences(ObjCorrespondenceSet objCorrs) {
		StringBuffer buf = new StringBuffer();
		int i=1;
		for (ObjCorrespondence corr : objCorrs) {
			buf.append("(").append(i++).append("): ")
			   .append(corr.getDomainAccessionNumber()).append(TAB)
			   .append(corr.getRangeAccessionNumber()).append(TAB)
			   .append(corr.getConfidence()).append(TAB)
			   .append(corr.getSupport()).append(NL);
			if (buf.length()>20000) {
				System.out.println(buf);
				buf = new StringBuffer();
			}
		}
		
		System.out.println(NL+buf);
	}
	
	protected void setStatus(TaskStatus status) {
		this.matchTask.setTaskStatus(status);
	}
	
	protected MatchData loadData(String accSepPattern) throws RemoteException, GommaException {
		
		SourceVersionSet set = this.gommaServer.getSourceManager().getSourceVersionSet();
		IDObjectSet domainObjSet = new IDObjectSet(), rangeObjSet = new IDObjectSet();
		
		for (int id : this.matchTask.getDomainObjectIDs()) domainObjSet.addElement(new IDObject(id));
		for (int id : this.matchTask.getRangeObjectIDs()) rangeObjSet.addElement(new IDObject(id));
		
		MatchData data = new MatchData();
		data.setDomainSources(this.getSourceVersionStructures(
				set.getSourceVersion(this.matchTask.getDomainSourceVersionID()),
				domainObjSet,accSepPattern));
		data.setRangeSources(this.getSourceVersionStructures(
				set.getSourceVersion(this.matchTask.getRangeSourceVersionID()),
				rangeObjSet,accSepPattern));
		
		return data;
	}
	
	private SourceVersionStructure[] getSourceVersionStructures(SourceVersion s, IDObjectSet idObjSet, String accSepPattern) throws GommaException, RemoteException {
		
		ObjSet set = this.gommaServer.getSourceManager().getObjectSet(s,idObjSet);
		
		ObjSet partitionObjs;
		Map<String,ObjSet> partitionObjMap = new HashMap<String,ObjSet>();
		
		Pattern p = Pattern.compile(accSepPattern);
		Matcher m;
		String partitionName;
		
		for (Obj o : set) {
			if ((m = p.matcher(o.getAccessionNumber())).find()) {
				partitionName = o.getAccessionNumber().substring(m.start(),m.end());
				
				if (partitionObjMap.containsKey(partitionName)) partitionObjs = partitionObjMap.get(partitionName);
				else partitionObjs = new ObjSet();
				partitionObjs.addObj(o);
				
				partitionObjMap.put(partitionName,partitionObjs);
			}
		}
		
		SourceVersionStructure[] partitions = new SourceVersionStructure[partitionObjMap.size()];
		int i=0;
		for (String family : partitionObjMap.keySet()) {
			partitions[i++]=this.gommaServer.getSourceManager().getSourceStructurePortion(s,partitionObjMap.get(family));
		}
		
		return partitions;
	}
	
	protected int getMinimumSupport(ObjCorrespondenceSet set) {
		int min = Integer.MAX_VALUE;
		
		for (ObjCorrespondence c : set.getCollection())
			if (min>c.getSupport()) min = c.getSupport();
		
		return min;
	}
	
	protected float getMinimumConfidence(ObjCorrespondenceSet set) {
		float min = Float.MAX_VALUE;
		
		for (ObjCorrespondence c : set.getCollection())
			if (min>c.getConfidence()) min = c.getConfidence();
		
		return min;
	}
	
	protected abstract Mapping createMapping(String mapName, int domainLDSID, int rangeLDSID) throws GommaException, RemoteException;
	
	
	
	
	
	protected class MatchData extends Object implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4105794731678687417L;
		
		private SourceVersionStructure[] domain, range;
		
		public MatchData(){
			this.domain = new SourceVersionStructure[] {};
			this.range  = new SourceVersionStructure[] {};
		}
		
		public void setDomainSources(SourceVersionStructure[] array) {
			this.domain=array;
		}
		
		public void setRangeSources(SourceVersionStructure[] array) {
			this.range=array;
		}
		
		public SourceVersionStructure[] getDomainSources() {
			return this.domain;
		}
		
		public SourceVersionStructure[] getRangeSources() {
			return this.range;
		}
	}
}
