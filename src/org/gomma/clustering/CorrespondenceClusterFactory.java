package org.gomma.clustering;

import org.gomma.clustering.clusterer.CorrespondenceClusterer;
import org.gomma.clustering.clusterer.SourceGraphClusterer;
import org.gomma.exceptions.OperatorException;

/**
 * The class represents a factory for providing correspondence clusterer.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class CorrespondenceClusterFactory {

	/**
	 * Constructor of the class.
	 */
	private CorrespondenceClusterFactory() {}
	
	/**
	 * The method returns the correspondence clusterer of the specified type.
	 * @param type correspondence cluster type
	 * @return correspondence clusterer of the specified type
	 * @throws OperatorException
	 */
	public static CorrespondenceClusterer getCorrespondenceClusterer(CorrespondenceClusterTypes type) throws OperatorException {
		
		switch (type) {
			case SOURCE_GRAPH: return getSourceGraphClusterer();
			default: throw new OperatorException("Could not resolve the cluster type.");
		}
	}
	
	
	/**
	 * The method returns an instance of the source graph correspondence clusterer.
	 * @return instance of the source graph clusterer
	 */
	private static SourceGraphClusterer getSourceGraphClusterer() {
		return new SourceGraphClusterer();
	}
}
