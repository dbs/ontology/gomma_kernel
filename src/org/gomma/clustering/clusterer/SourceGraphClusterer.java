package org.gomma.clustering.clusterer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gomma.exceptions.OperatorException;
import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.operator.SetOperators;

/**
 * The class implements a source graph correspondence clustering. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public class SourceGraphClusterer extends AbstractCorresopondenceClusterer {
	
	/**
	 * The constructor initializes the class.
	 */
	public SourceGraphClusterer() {}


	/**
	 * @see CorrespondenceClusterer#cluster(ObjCorrespondenceSet)
	 */
	public List<ObjCorrespondenceSet> cluster(ObjCorrespondenceSet set) throws OperatorException {
		
		if (super.domainSource==null || super.rangeSource == null)
			throw new OperatorException("Could not execute clustering operation "+
					"since domain or range source version structure is not available."); 
		
		return reduce(map(set,super.domainSource,super.rangeSource),super.domainSource,super.rangeSource);
	}
	
	private List<ObjCorrespondenceSet> map(ObjCorrespondenceSet set, SourceVersionStructure domain, SourceVersionStructure range) throws OperatorException {
		List<ObjCorrespondenceSet> clusterList = new ArrayList<ObjCorrespondenceSet>(set.size());
		ObjCorrespondenceSet cluster;
		
		try {
			for (ObjCorrespondence c : set) {
				cluster = new ObjCorrespondenceSet();
				cluster.addCorrespondence(c);
			
				clusterList.add(cluster);
			}
		} catch (WrongSourceException e) {
			throw new OperatorException(e.getMessage());
		}
		
		return clusterList;
	}
	
	private List<ObjCorrespondenceSet> reduce(List<ObjCorrespondenceSet> mappedClusterList, SourceVersionStructure domain, SourceVersionStructure range) throws OperatorException {
		
		if (mappedClusterList.size()<2) return mappedClusterList;
		
		int maxNumberOfClusters = mappedClusterList.size();
		
		ObjCorrespondenceSet cluster1, cluster2;
		Set<Integer> removedClusters = new HashSet<Integer>(maxNumberOfClusters);
		boolean hasGloballyMerged, hasLocallyMerged;
		
		do {
			hasGloballyMerged = false;
			
			for (int i=0;i<mappedClusterList.size()-1;i++) {
				if (removedClusters.contains(i)) continue;
				hasLocallyMerged = false;
				cluster1 = mappedClusterList.get(i);
				
				for (int j=i+1;j<mappedClusterList.size();j++) {
					if (removedClusters.contains(j)) continue;
					cluster2 = mappedClusterList.get(j);
					
					for (ObjCorrespondence cc1 : cluster1) {
						
						for (ObjCorrespondence cc2 : cluster2) {
							
							if (super.clusterFunction.isLinked(domain, range, cc1, cc2)) {
								try {
									cluster1 = SetOperators.union(cluster1, cluster2);
								} catch (WrongSourceException e) {
									throw new OperatorException(e.getMessage());
								}
								mappedClusterList.set(i, cluster1);
								removedClusters.add(j);
								hasGloballyMerged=hasLocallyMerged=true;
								break;
							}
						}
						if (hasLocallyMerged) break;
					}
					if (hasLocallyMerged) break;
				}
			}
			
		} while (hasGloballyMerged);
		
		
		List<ObjCorrespondenceSet> reducedClusterList = new ArrayList<ObjCorrespondenceSet>(maxNumberOfClusters - removedClusters.size());
		for (int i=0; i<mappedClusterList.size();i++) 
			if (removedClusters.contains(i)) continue;
			else reducedClusterList.add(mappedClusterList.get(i));
		
		return reducedClusterList;
	}
	
	
	
	// Map version mit pre-processing, in dem eine erste cluster iteration vorgenommen wird, um die anzahl an cluster moeglichst gering zu halten
//	private List<ObjCorrespondenceSet> map(ObjCorrespondenceSet set, SourceVersionStructure domain, SourceVersionStructure range) throws OperatorException {
//		List<ObjCorrespondenceSet> clusterList = new ArrayList<ObjCorrespondenceSet>();
//		boolean isAdded=false;
//		
//		for (ObjCorrespondence c : set) {
//			isAdded= false;
//			
//			for (ObjCorrespondenceSet cluster : clusterList) {
//				
//				for (ObjCorrespondence cc : cluster) {
//					
//					if (super.clusterFunction.isLinked(domain, range, c, cc)) {
//						
//						cluster.addCorrespondence(c);
//						isAdded=true;
//						break;
//					}
//				}
//				if (isAdded) break;
//			}
//			
//			if (isAdded) continue;
//			
//			ObjCorrespondenceSet cluster = new ObjCorrespondenceSet();
//			cluster.addCorrespondence(c);
//			
//			clusterList.add(cluster);
//		}
//		
//		return clusterList;
//	}
	
	
	
	
//	private List<ObjCorrespondenceSet> reduce(List<ObjCorrespondenceSet> mappedClusterList, SourceVersionStructure domain, SourceVersionStructure range) throws OperatorException {
//
//		int maxNumberOfClusters = mappedClusterList.size();
//		
//		if (mappedClusterList.size()<2) return mappedClusterList;
//		
//		List<ObjCorrespondenceSet> reducedClusterList = new ArrayList<ObjCorrespondenceSet>(maxNumberOfClusters);
//		ObjCorrespondenceSet cluster1, cluster2;
//		Set<Integer> removedClusters = new HashSet<Integer>(maxNumberOfClusters);
//		boolean hasClusterReduced;
//		
//		do {
//			hasClusterReduced = false;
//		
//			for (int i=0; i< mappedClusterList.size(); i++) {
//				if (removedClusters.contains(i)) continue;
//				cluster1 = mappedClusterList.get(i);
//			
//				for (int j=i+1; j<mappedClusterList.size(); j++) {
//					if (removedClusters.contains(j)) continue;
//					cluster2 = mappedClusterList.get(j);
//				
//					for (ObjCorrespondence cc1 : cluster1) {
//					
//						for (ObjCorrespondence cc2 : cluster2) {
//						
//							if (super.clusterFunction.isLinked(domain, range, cc1, cc2)) {
//							
//								reducedClusterList.add(SetOperators.union(cluster1, cluster2));
//								removedClusters.add(i);
//								removedClusters.add(j);
//								hasClusterReduced=true;
//								break;
//							}
//						}
//						if (hasClusterReduced) break;
//					}
//					if (!hasClusterReduced && !reducedClusterList.contains(cluster2)) reducedClusterList.add(cluster2);
//				}
//				if (!hasClusterReduced && !reducedClusterList.contains(cluster1)) reducedClusterList.add(cluster1);
//			}
//			
//			maxNumberOfClusters = reducedClusterList.size();
//			mappedClusterList = reducedClusterList;
//			removedClusters = new HashSet<Integer>(maxNumberOfClusters);
//			
//			System.gc();
//			
//		} while (hasClusterReduced);
//		
//		
//		return reducedClusterList;
//	}
	
}
