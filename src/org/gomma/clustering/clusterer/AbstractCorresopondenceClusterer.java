package org.gomma.clustering.clusterer;

import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.CorrespondenceClusterFunction;


/**
 * The abstract class implements general method of the interface.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public abstract class AbstractCorresopondenceClusterer implements CorrespondenceClusterer {

	/**
	 * The item represents the source graph of the mapping domain. 
	 */
	protected SourceVersionStructure domainSource = null;
	
	/**
	 * The item represents the source graph of the mapping range. 
	 */
	protected SourceVersionStructure rangeSource = null;
	
	/**
	 * The item represents the clustering function.
	 */
	protected CorrespondenceClusterFunction clusterFunction = null;
	
	
	
	/**
	 * Constructor of the class.
	 */
	protected AbstractCorresopondenceClusterer() {}
	
	
	
	/**
	 * @see CorrespondenceClusterer#setClusterFunction(CorrespondenceClusterFunction)
	 */
	public void setClusterFunction(CorrespondenceClusterFunction clusterFunc) {
		this.clusterFunction = clusterFunc;
	}
	
	/**
	 * @see CorrespondenceClusterer#setDomainSourceVersionStructure(SourceVersionStructure)
	 */
	public void setDomainSourceVersionStructure(SourceVersionStructure domainSource) {
		this.domainSource = domainSource;
	}

	/**
	 * @see CorrespondenceClusterer#setRangeSourceVersionStructure(SourceVersionStructure)
	 */
	public void setRangeSourceVersionStructure(SourceVersionStructure rangeSource) {
		this.rangeSource = rangeSource;
	}
}
