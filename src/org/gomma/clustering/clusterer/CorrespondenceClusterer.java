package org.gomma.clustering.clusterer;

import java.util.List;

import org.gomma.exceptions.OperatorException;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.CorrespondenceClusterFunction;

/**
 * The interface defines a set of methods to generate clusters of object correspondences
 * and to set necessary input data.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public interface CorrespondenceClusterer {

	/**
	 * The method sets the clustering function.
	 * @param clusterFunc clustering function
	 */
	public void setClusterFunction(CorrespondenceClusterFunction clusterFunc);
	
	/**
	 * The method sets the source version structure for the domain of the mapping version 
	 * for that the correspondence set should be clustered. 
	 * @param domainSource domain source version structure
	 */
	public void setDomainSourceVersionStructure(SourceVersionStructure domainSource);
	
	/**
	 * The method sets the source version structure for the range of the mapping version 
	 * for that the correspondence set should be clustered.
	 * @param rangeSource range source version structure
	 */
	public void setRangeSourceVersionStructure(SourceVersionStructure rangeSource);
	
	/**
	 * The method returns a set of correspondence sets: Each of acts as correspondence 
	 * cluster in which correspondence which are somehow similar are grouped together.
	 * @param set set of object correspondences
	 * @return correspondence cluster list
	 * @throws OperatorException
	 */
	public List<ObjCorrespondenceSet> cluster(ObjCorrespondenceSet set) throws OperatorException;
}
