package org.gomma.clustering;

/**
 * The enumeration provides a set of clustering types 
 * that can be used to group correspondences. 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public enum CorrespondenceClusterTypes {

	/**
	 * The element represents the source-graph-based clustering type for grouping
	 * object correspondences. This clustering type groups correspondences whose 
	 * participating objects are linked in the domain and/or range source graphs.
	 */
	SOURCE_GRAPH
	
}
