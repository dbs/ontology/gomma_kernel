/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public Licence. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2007/11/10
 * 
 * changes:
 * 
 * 2008/24/03 -- add import methods
 * 
 **/

package org.gomma;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Properties;

import org.gomma.api.APIFactory;
import org.gomma.api.cache.CacheManager;
import org.gomma.api.util.DatabaseConnectionData;
import org.gomma.exceptions.GommaException;
import org.gomma.io.PropertyIniReader;
import org.gomma.manager.ManagerModuleFactory;
import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;
import org.gomma.rmi.Server;
import org.gomma.util.SystemPropertyPool;
import org.gomma.util.sysinit.DataStoreFinalizerFactory;
import org.gomma.util.sysinit.DataStoreInitializerFactory;
import org.gomma.util.sysinit.DataStoreInitializerRDBMS;

/**
 * The class implements the GOMMA interface. All GOMMA functions are accessible 
 * by this interface implementation.
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @author: Michael Hartung (hartung@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public class GommaImpl implements Gomma {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5574650524998148620L;

	/**
	 * The item represents the standard name of the configuration file.
	 */
	private static final String CONFIG_FILE_NAME_LOCATION = "gomma-conf.ini";
	
	
	private boolean isInitialized;
;
	
	/**
	 * The constructor initializes the class.
	 * @throws GommaException
	 * @throws RemoteException
	 */
	public GommaImpl() {
		super();
	}
	
	/**
	 * @see Server#initialize()
	 */
	public synchronized void initialize() throws GommaException, RemoteException {
		this.initialize(CONFIG_FILE_NAME_LOCATION);
	}

	public synchronized void initializeUsingParameters(String driver,String dbUrl,String dbUser,String pw) throws RemoteException, GommaException {
		this.initialize(driver,dbUrl,dbUser,pw);
	}	
	
	/**
	 * @see Server#initializeStandalone()
	 */
	public synchronized void initializeStandalone() throws GommaException, RemoteException {
		this.isInitialized = true;
		System.out.println("GOMMA startup complete!");
	}
	
	/**
	 * @see Server#initialize(String)
	 */
	public synchronized void initialize(String confFileNameLoc) throws GommaException, RemoteException {
		try {
			PropertyIniReader iniReader = new PropertyIniReader(confFileNameLoc);
			Properties repositoryProps = iniReader.getSection("repository");
			
			DataStoreInitializerFactory.getInitializer(repositoryProps).initialize();
			this.isInitialized = true;
			
			System.out.println("GOMMA startup complete!");
		} catch (IOException e) {
			throw new GommaException(e.getMessage());
		}
	}
	/**
	 * @see Server#initialize(DatabaseConnectionData)
	 */
	public synchronized void initialize(String driver,String dbUrl,String dbUser,String pw) throws GommaException, RemoteException {
		Properties repositoryProps = new Properties();
		
		repositoryProps.put(DataStoreInitializerRDBMS.DB_DRIVER, driver);
		repositoryProps.put(DataStoreInitializerRDBMS.DB_URL, dbUrl);
		repositoryProps.put(DataStoreInitializerRDBMS.DB_USER, dbUser);
		repositoryProps.put(DataStoreInitializerRDBMS.DB_PASSWORD, pw);
		
		DataStoreInitializerFactory.getInitializer(repositoryProps).initialize();
		this.isInitialized = true;		
		System.out.println("GOMMA startup complete!");
	}
	/**
	 * @see Server#shutdown()
	 */
	public synchronized void shutdown() throws GommaException, RemoteException {
		
		CacheManager.getInstance().shutdown();
		CacheManager.stop();
		
		APIFactory.getInstance().shutdown();
		APIFactory.stop();
		
		DataStoreFinalizerFactory.getFinalizer().stop();
		
		ManagerModuleFactory.getInstance().shutdown();
		ManagerModuleFactory.stop();
		
		SystemPropertyPool.getInstance().shutdown();
		SystemPropertyPool.stop();
		
		this.isInitialized = false;
		
		System.out.println("GOMMA shutdown complete!");
	}
	
	/**
	 * @see Server#isInitialized()
	 */
	public boolean isInitialized() throws GommaException, RemoteException {
		return this.isInitialized;
	}
	
	/**
	 * @see Server
	 */
	public synchronized void installRepository() throws GommaException, RemoteException {
		this.checkInitialization();
	
		APIFactory.getInstance().getRepositoryAPI().createSchema();
	}
	
	/**
	 * @see Server
	 */
	public synchronized void deinstallRepository() throws GommaException, RemoteException {
		this.checkInitialization();
	
		APIFactory.getInstance().getRepositoryAPI().dropSchema();
	}
	
	/**
	 * @see GommaManager#getSourceManager()
	 */
	public SourceManager getSourceManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getSourceManager();
	}

	/**
	 * @see GommaManager#getMappingManager()
	 */
	public MappingManager getMappingManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getMappingManager();
	}
	
	/**
	 * @see GommaManager#getMappingOperationManager()
	 */
	public MappingOperationManager getMappingOperationManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getMappingOperationManager();
	}
	
	/**
	 * @see GommaManager#getSourceEvolutionManager()
	 */
	public SourceEvolutionManager getSourceEvolutionManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getSourceEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getMappingObjectEvolutionManager()
	 */
	public MappingObjectEvolutionManager getMappingObjectEvolutionManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getMappingObjectEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getMappingCorrespondenceEvolutionManager()
	 */
	public MappingCorrespondenceEvolutionManager getMappingCorrespondenceEvolutionManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getMappingCorrespondenceEvolutionManager();
	}
	
	/**
	 * @see GommaManager#getEvolutionStatisticsManager()
	 */
	public EvolutionStatisticsManager getEvolutionStatisticsManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getEvolutionStatisticsManager();
	}

	/**
	 * @see GommaManager#getStatisticsManager()
	 */
	public StatisticsManager getStatisticsManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getStatisticsManager();
	}
	
	/**
	 * @see GommaManager#getMatchManager()
	 */
	public MatchManager getMatchManager() throws RemoteException, GommaException {
		return ManagerModuleFactory.getInstance().getMatchManager();
	}
	
	/**
	 * @see GommaManager#getWorkManager()
	 */
	public WorkManager getWorkManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getWorkManager();
	}
	
	/**
	 * @see GommaManager#getStabilityManager()
	 * @author Yiming Huang (Diploma Student)
	 */
	public StabilityManager getStabilityManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getStabilityManager();
	}
	
	/**
	 * @see GommaManager#getDistributedMatchManager()
	 */
	public DistributedMatchManager getDistributedMatchManager() throws RemoteException, GommaException {
		this.checkInitialization();
		return ManagerModuleFactory.getInstance().getDistributedMatchManager();
	}
	
	/**
	 * The method returns true if the kernel is initialized; otherwise it returns false.
	 * @return true/false
	 * @throws GommaException
	 * @throws RemoteException
	 */
	protected void checkInitialization() throws GommaException, RemoteException {
		if (!this.isInitialized()) 
			throw new GommaException("Could not resolve respository connection.");
	}
}
