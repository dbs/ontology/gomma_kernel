/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2009/02/16
 * 
 * changes:
 * 
 * 
 **/
package org.gomma;

import java.rmi.Remote;

import org.gomma.manager.analysis.EvolutionStatisticsManager;
import org.gomma.manager.analysis.MappingCorrespondenceEvolutionManager;
import org.gomma.manager.analysis.MappingObjectEvolutionManager;
import org.gomma.manager.analysis.SourceEvolutionManager;
import org.gomma.manager.analysis.StatisticsManager;
import org.gomma.manager.distributedmatching.DistributedMatchManager;
import org.gomma.manager.mapping.MappingManager;
import org.gomma.manager.mapping.MappingOperationManager;
import org.gomma.manager.mapping.MatchManager;
import org.gomma.manager.source.SourceManager;
import org.gomma.manager.stability.StabilityManager;
import org.gomma.manager.work.WorkManager;
import org.gomma.rmi.Server;

/**
 * The interface represents the GOMMA service interface defining 
 * methods that are necessary to implement a RMI or WEB service. 
 * @author: Toralf Kirsten (tkirsten@izbi.uni-leipzig.de) 
 * @version: 1.0
 * @since:   JDK1.5
 */
public interface GommaService extends Remote,
	Server,
	
	SourceManager, 
	
	MappingManager, 
	MappingOperationManager,
	
	SourceEvolutionManager, 
	MappingObjectEvolutionManager, 
	MappingCorrespondenceEvolutionManager,
	
	EvolutionStatisticsManager,
	StatisticsManager,
	
	StabilityManager,
	
	MatchManager, 
	
	DistributedMatchManager,
	
	WorkManager {}
