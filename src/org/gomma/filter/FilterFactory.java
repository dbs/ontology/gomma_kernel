/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.filter;

import org.gomma.filter.filter.ConfidenceThresholdDeltaFilter;
import org.gomma.filter.filter.ConfidenceThresholdFilter;
import org.gomma.filter.filter.DistinctSymmetryFilter;
import org.gomma.filter.filter.Filter;
import org.gomma.filter.filter.MaxDeltaFilterBothDirections;
import org.gomma.filter.filter.MaxDeltaFilterOneDirection;
import org.gomma.filter.filter.MaxNFilterBothDirections;
import org.gomma.filter.filter.MaxNFilterOneDirection;
import org.gomma.filter.filter.MaxTotalConfidenceFilter;
import org.gomma.filter.filter.NoFilter;
import org.gomma.filter.filter.NonReflexiveRelationshipFilter;
import org.gomma.filter.filter.BestConfidenceFilter;
import org.gomma.filter.filter.SmoothingConfidenceFilter;
import org.gomma.filter.filter.StableMarriageFilter;
import org.gomma.filter.filter.TopXOnHighestConfidenceFilter;
import org.gomma.filter.filter.TopXRankedConfidenceFilter;
import org.gomma.util.math.SmoothFunctions;

/**
 * The class acts as factory to instantiate filter classes.
 * @author  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since   JDK1.5
 */
public class FilterFactory {

	/**
	 * Constructor of the class.
	 */
	private FilterFactory() {}
	
	/**
	 * The method returns the filter for the specified filter type.
	 * @param type filter type
	 * @param t threshold
	 * @param n TODO
	 * @return filter of specified type
	 */
	public static Filter getFilter(FilterTypes type, float t,  boolean isDomain, int n) {
		switch (type) {
			case NON_REFLEXIVE_RELATIONSHIPS:              return getNonReflexiveRelationshipFilter();
			case DISTINCT_SYMMETRIC:                       return getDistinctSymmetryFilter();
			case FILTER_BEST_BY_CONFIDENCE:                return getBestConfidenceFilter();
			case CONFIDENCE_THRESHOLD:                     return getConfidenceThresholdFilter(t);
			case CONFIDENCE_DELTA_THRESHOLD:               return getConfidenceThresholdDeltaFilter(t);
			case TOP_X_PERCENT_RANKED_BY_CONFIDENCE:       return getTopXRankedConfidenceFilter(t);
			case TOP_X_PERCENT_BASED_ON_HIGHEST_CONFIDENCE:return getTopXOnHighestConfidenceFilter(t);
			case STABLE_MARRIAGE_BY_CONFIDENCE:            return getStableMarriageFilter();
			case SIGMOID_SMOOTHING_CONFIDENCE:             return getSigmoidSmoothingFilter();
			case TRIGONOMETRIC_SMOOTHING_CONFIDENCE:       return getTrigonometricSmoothingFilter();
			case TOTAL_CONFIDENCE_MAXIMUM:                 return getMaxTotalConfidenceFilter();
			case MAX_DELTA_BOTH_DIR:                	   return getMaxDeltaFilterBothDirections(t);
			case MAX_DELTA_ONE_DIR:                	  	   return getMaxDeltaFilterOneDirection(t, isDomain);
			case MAX_N_BOTH_DIR:                	  	   return getMaxNFilterBothDirections(n);
			case MAX_N_ONE_DIR:                	  	   	   return getMaxNFilterOneDirection(n, isDomain);
			
			default: return getNoFilter();
		}
	}

	/**
	 * The method returns the no filter.
	 * @return no filter
	 * @see NoFilter
	 */
	public static NoFilter getNoFilter() {
		return new NoFilter();
	}
	
	/**
	 * The method returns the non reflexive relationship filter
	 * @return non reflexive relationship filter
	 * @see NonReflexiveRelationshipFilter
	 */
	public static NonReflexiveRelationshipFilter getNonReflexiveRelationshipFilter() {
		return new NonReflexiveRelationshipFilter();
	}
	
	/**
	 * The method returns the distinct symmetry filter.
	 * @return distinct symmetry filter
	 * @see DistinctSymmetryFilter
	 */
	public static DistinctSymmetryFilter getDistinctSymmetryFilter() {
		return new DistinctSymmetryFilter();
	}
	
	/**
	 * The method returns the top equivalence filter.
	 * @return top equivalence filter
	 * @see BestConfidenceFilter
	 */
	public static BestConfidenceFilter getBestConfidenceFilter() {
		return new BestConfidenceFilter();
	}
	
	/**
	 * The method returns the confidence filter using the predefined threshold value.
	 * @param threshold confidence threshold
	 * @return ConfidenceThresholdFilter
	 */
	public static ConfidenceThresholdFilter getConfidenceThresholdFilter(float threshold) {
		return new ConfidenceThresholdFilter(threshold);
	}
	
	/**
	 * The method returns the confidence threshold delta filter using the predefined
	 * threshold delta value.
	 * @param delta threshold delta
	 * @return ConfidenceThresholdDeltaFilter
	 */
	public static ConfidenceThresholdDeltaFilter getConfidenceThresholdDeltaFilter(float delta) {
		return new ConfidenceThresholdDeltaFilter(delta);
	}
	
	/**
	 * The method returns the top x ranked confidence filter using the predefined
	 * x (percent) value.
	 * @param xPercent top x (in percent) of the correspondence list
	 * @return TopXRankedConfidenceFilter
	 */
	public static TopXRankedConfidenceFilter getTopXRankedConfidenceFilter(float xPercent) {
		return new TopXRankedConfidenceFilter(xPercent);
	}
	
	/**
	 * The method returns the top x of highest confidence filter using the predefined
	 * x (percent) value.
	 * @param xPercent top x (percent) of the highest confidence value.
	 * @return TopXOnHighestConfidenceFilter
	 */
	public static TopXOnHighestConfidenceFilter getTopXOnHighestConfidenceFilter(float xPercent) {
		return new TopXOnHighestConfidenceFilter(xPercent);
	}
	
	/**
	 * The method returns the stable marriage filter based on match confidences.
	 * @return StableMarriageFilter
	 */
	public static StableMarriageFilter getStableMarriageFilter() {
		return new StableMarriageFilter();
	}
	
	/**
	 * The method returns a confidence value threshold filter based on a sigmoid smoothing function. 
	 * @return SmoothingConfidenceFilter
	 */
	public static SmoothingConfidenceFilter getSigmoidSmoothingFilter() {
		return new SmoothingConfidenceFilter(SmoothFunctions.SIGMOID_SMOOTHING);
	}
	
	/**
	 * The method returns a confidence value threshold filter based on a sigmoid smoothing function and a predefined threshold. 
	 * @param threshold threshold reducing the correspondences to be included in the result
	 * @return SmoothingConfidenceFilter
	 */
	public static SmoothingConfidenceFilter getSigmoidSmoothingFilter(float threshold) {
		return new SmoothingConfidenceFilter(SmoothFunctions.SIGMOID_SMOOTHING,threshold);
	}
	
	/**
	 * The method returns a confidence value threshold filter based on a trigonometric smoothing function. 
	 * @return SmoothingConfidenceFilter
	 */
	public static SmoothingConfidenceFilter getTrigonometricSmoothingFilter() {
		return new SmoothingConfidenceFilter(SmoothFunctions.TRIGONOMETRIC_SMOOTHING);
	}
	
	/**
	 * The method returns a confidence value threshold filter based on a trigonometric smoothing function and a predefined threshold. 
	 * @param threshold threshold reducing the correspondences to be included in the result
	 * @return SmoothingConfidenceFilter
	 */
	public static SmoothingConfidenceFilter getTrigonometricSmoothingFilter(float threshold) {
		return new SmoothingConfidenceFilter(SmoothFunctions.TRIGONOMETRIC_SMOOTHING,threshold);
	}
	
	/**
	 * The method returns the maximum total confidence filter returning 1:1 correspondences
	 * with the highest total of confidence values.
	 * @return MaxTotalConfidenceFilter
	 */
	public static MaxTotalConfidenceFilter getMaxTotalConfidenceFilter() {
		return new MaxTotalConfidenceFilter();
	}
	
	
	/**
	 * The method returns for each concept the correspondences with the maximal similarity value and those 
	 * within a small delta distance to the maximal value. Considered from domain OR range direction.
	 * @return MaxTotalConfidenceFilter
	 */
	public static MaxDeltaFilterOneDirection getMaxDeltaFilterOneDirection(float thresholdDelta, boolean isDomain) {
		return new MaxDeltaFilterOneDirection(thresholdDelta, isDomain);
	}
	/**
	 * The method returns for each concept the correspondences with the maximal similarity value and those 
	 * within a small delta distance to the maximal value. Executed for both: first from domain and than from range direction. 
	 * @return MaxTotalConfidenceFilter
	 */
	public static MaxDeltaFilterBothDirections getMaxDeltaFilterBothDirections(float thresholdDelta) {
		return new MaxDeltaFilterBothDirections(thresholdDelta);
	}
	
	/**
	 * The method returns for each concept its top n correspondences. Considered from domain OR range direction.
	 * @return MaxTotalConfidenceFilter
	 */
	public static Filter getMaxNFilterOneDirection(int n, boolean isDomain) {
		return new MaxNFilterOneDirection(n, isDomain);
	}
	/**
	 * The method returns for each concept its top n correspondences. Executed for both: first from domain and than from range direction.
	 * @return MaxTotalConfidenceFilter
	 */
	public static Filter getMaxNFilterBothDirections(int n) {
		return new MaxNFilterBothDirections(n);
	}

}
