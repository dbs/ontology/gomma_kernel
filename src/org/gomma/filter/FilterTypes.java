/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.filter;

import java.util.NoSuchElementException;

/**
 * The enumeration contains different correspondence filters.
 * @author  Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since   JDK1.5
 */
public enum FilterTypes {

	/**
	 * The enumeration element represents the filler filter, i.e., no 
	 * correspondences will filtered out. 
	 */
	NO_FILTER(0) ,
	
	/**
	 * The enumeration element represents the non reflexive relationship, i.e., 
	 * all correspondences (x,x) will be filtered out and, thus,
	 * are not included in the result set.
	 */
	NON_REFLEXIVE_RELATIONSHIPS(1),
	
	/**
	 * The enumeration element represents the distinct symmetric filter, i.e.,
	 * all correspondences (x,y) for which a correspondence (y,x) 
	 * exists are filtered out, such that only (x,y) is in the result set 
	 */
	DISTINCT_SYMMETRIC(2),
	
	/**
	 * The enumeration element represents the top equivalence filter that includes
	 * 1:1 correspondences (x,y) of (x,Y) and (X,y) with the highest 
	 * confidence for x and y,respectively. At the end, there is at most
	 * one correspondence for each x of X and one correspondence for each y of Y.
	 */
	FILTER_BEST_BY_CONFIDENCE(3),
	/**
	 * The enumeration element represents the confidence threshold filter that includes
	 * correspondences for that the confidence exceeds a given threshold (sim(x,y)>=t).
	 */
	CONFIDENCE_THRESHOLD(4),
	/**
	 * The enumeration element represents the confidence delta threshold filter. It 
	 * ranks the given correspondences w.r.t. their confidence and and includes a 
	 * correspondence when the confidence delta to the predecessor is lower or equal 
	 * a given threshold.
	 */
	CONFIDENCE_DELTA_THRESHOLD(5),
	/**
	 * The enumeration element represents the top x percent filter that includes 
	 * correspondences of the first x percent of the correspondence list ranked by 
	 * their confidence values.
	 */
	TOP_X_PERCENT_RANKED_BY_CONFIDENCE(6),
	/**
	 * The enumeration element represents the top x percent filter that includes 
	 * correspondences for that the confidence exceeds x percent of the 
	 * maximum confidence value.  
	 */
	TOP_X_PERCENT_BASED_ON_HIGHEST_CONFIDENCE(7),
	/**
	 * The enumeration element represents the stable marriage filter that includes
	 * 1:1 correspondences (x,y) such that there is no correspondence (x',y) and 
	 * (x,y') with sim(x',y)>sim(x,y)<sim(x,y'). 
	 */
	STABLE_MARRIAGE_BY_CONFIDENCE(8),
	/**
	 * The enumeration element represents a threshold filter to that a sigmoid 
	 * smoothing function is applied before. The function strengthens and weakens
	 * the confidence with the goal of a better distinction between 'good' and
	 * 'bad' correspondences.
	 */
	SIGMOID_SMOOTHING_CONFIDENCE(9),
	/**
	 * The enumeration element represents a threshold filter to that a trigonometric 
	 * smoothing function is applied before. The function strengthens and weakens
	 * the confidence with the goal of a better distinction between 'good' and
	 * 'bad' correspondences.
	 */
	TRIGONOMETRIC_SMOOTHING_CONFIDENCE(10),
	/**
	 * The enumeration element represents the maximum total confidence filter
	 * returning a mapping of 1:1 correspondences (x,y) such that there is no 
	 * other mapping with correspondences (x',y') with 
	 * sum(sim(x,y)) < sum(sim(x',y')).
	 */
	TOTAL_CONFIDENCE_MAXIMUM(11), 
	/**
	 * The enumeration element represents the maximum delta confidence filter
	 * for both directions (domain AND range) returning a mapping of correspondences  
	 * where each concept of domain/range can only "be part of" the correspondence 
	 * with the maximum confidence accepting a delta variation of the confidence.
	 */	
	MAX_DELTA_BOTH_DIR(12),
	/**
	 * The enumeration element represents the maximum delta confidence filter
	 * for one direction (domain OR range) returning a mapping of correspondences  
	 * where each concept of domain/range can only "be part of" the correspondence 
	 * with the maximum confidence accepting a delta variation of the confidence.
	 */	
	MAX_DELTA_ONE_DIR(13), 
	/**
	 * The enumeration element represents the maximum N filter
	 * for both directions (domain AND range) returning a mapping of correspondences  
	 * where each concept of domain/range can only "be part of" the correspondences 
	 * with the highest confidence value (max/top n similarities).
	 */	
	MAX_N_BOTH_DIR(14), 
	/**
	 * The enumeration element represents the maximum N filter
	 * for one direction (domain OR range) returning a mapping of correspondences  
	 * where each concept of domain/range can only "be part of" the correspondences 
	 * with the highest confidence value (max/top n similarities).
	 */	
	MAX_N_ONE_DIR(15);
	
	private int filterTypeID;
	
	private FilterTypes(int id) {
		this.filterTypeID = id;
	}
	public int getID() {
		return this.filterTypeID;
	}
	
	public static FilterTypes resolveFilterType(int typeID) throws NoSuchElementException {
		for (FilterTypes type : FilterTypes.values())
			if (type.getID()==typeID) return type;
		throw new NoSuchElementException("Could not find any filter type for the specified type identifier '"+typeID+"'.");
	}
}
