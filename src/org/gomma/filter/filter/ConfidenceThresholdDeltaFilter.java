/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/24
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents a filter that reduces the correspondence 
 * set of such correspondences (x,y) which confidence is lower 
 * than a given threshold.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class ConfidenceThresholdDeltaFilter implements Filter {
	
	/**
	 * The item represents the threshold delta used to compare the 
	 * confidence of object correspondences. 
	 */
	private float thresholdDelta;
	
	/**
	 * The constructor initializes the class.
	 * @param thresholdDelta confidence threshold delta
	 */
	public ConfidenceThresholdDeltaFilter(float thresholdDelta) {
		this.thresholdDelta=thresholdDelta;
	}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		if (corrs.size()<2) return corrs;
		
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		float delta=0F;
		
		ObjCorrespondence[] sortedCorrespondenceArray = corrs.getAscendingSimilarityOrderedArray();
		result.addCorrespondence(sortedCorrespondenceArray[sortedCorrespondenceArray.length-1]);
		
		for (int i=sortedCorrespondenceArray.length-2;i>=0;i--) {
			delta = Math.abs(sortedCorrespondenceArray[i+1].getConfidence() - sortedCorrespondenceArray[i].getConfidence()); 
			if (delta>this.thresholdDelta) break;
			result.addCorrespondence(sortedCorrespondenceArray[i]);
		}

		return result;
	}

}
