/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents a filter that reduces the correspondence 
 * set of such correspondences (x,y) that are available as both (x,y)
 * and (y,x). The result set thereafter id distinct, i.e., for each
 * correspondence (x,y) there doesn't exists a correspondence (y,x).
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class DistinctSymmetryFilter implements Filter {

	/**
	 * The constructor initializes the class.
	 */
	public DistinctSymmetryFilter() {}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		for (ObjCorrespondence corr : corrs.getCollection()) {
			if (result.containsCorrespondence(corr.getDomainObjID(),corr.getRangeObjID()) ||
					result.containsCorrespondence(corr.getRangeObjID(),corr.getRangeObjID())) continue;
			result.addCorrespondence(corr);
		}
		
		return result;
	}
}
