/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/12/08
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.util.NoSuchElementException;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents the maximum total confidence filter returning
 * a mapping of 1:1 correspondences (x,y) such that there is no other
 * mapping of correspondences (x',y') with sum(sim(x,y)) <= sum(sim(x',y')) 
 * 
 * See also J. Euzenat, P. Shvaiko: Ontology Matching, Springer, 2008, pp. 147
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class MaxTotalConfidenceFilter implements Filter {

	private float maxTotalConfidence;
	private ObjCorrespondenceSet result, tmpCorrespondences;
	
	/**
	 * The constructor initializes the class.
	 */
	public MaxTotalConfidenceFilter() {}
	
	/**
	 * @See Filter#filter(ObjCorrespondences)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet objCorrs) throws WrongSourceException {
		
		this.maxTotalConfidence=0F;
		this.result = new ObjCorrespondenceSet();
		
		Set<Integer> domainObjIDs = objCorrs.getDomainObjIds(), 
					 rangeObjIDs  = objCorrs.getRangeObjIds(),
					 freeDomainObjIDs = new IntOpenHashSet(domainObjIDs),
					 freeRangeObjIDs  = new IntOpenHashSet(rangeObjIDs);
		int candidateObjID=-1;
		float totalConfidence = 0F, confidence;
		boolean isDomain = domainObjIDs.size()<=rangeObjIDs.size();
		
		for (int objID : isDomain?domainObjIDs:rangeObjIDs) {
			this.tmpCorrespondences = new ObjCorrespondenceSet();
			
			if (isDomain) freeDomainObjIDs.remove(objID);
			else freeRangeObjIDs.remove(objID);
			
			ObjCandidates initialCandidateSet = this.searchObjCandidates(objCorrs,objID,isDomain?freeRangeObjIDs:freeDomainObjIDs,isDomain);
			totalConfidence = initialCandidateSet.getConfidence();
			
			for (int candidateID : initialCandidateSet.getCandidates()) {
				
				if (isDomain) freeRangeObjIDs.remove(candidateID);
				else freeDomainObjIDs.remove(candidateID);
				
				confidence = this.probeCandidates(objCorrs,freeDomainObjIDs,freeRangeObjIDs,totalConfidence);
				
				if (isDomain) freeRangeObjIDs.add(candidateID);
				else freeDomainObjIDs.add(candidateID);
				
				if (totalConfidence>confidence) continue;
				
				candidateObjID = candidateID;
				totalConfidence += confidence;
			}
			
			if (isDomain) this.tmpCorrespondences.addCorrespondence(objCorrs.getCorrespondence(objID,candidateObjID));
			else this.tmpCorrespondences.addCorrespondence(objCorrs.getCorrespondence(candidateObjID,objID));
			
			if (this.maxTotalConfidence < totalConfidence) {
				this.maxTotalConfidence = totalConfidence;
				this.result = this.tmpCorrespondences;
			}
		}
	
		return result;
	}
	
	private float probeCandidates(ObjCorrespondenceSet map, Set<Integer> freeDomainObjIDs, Set<Integer> freeRangeObjIDs, float totalConfidence) throws WrongSourceException, NoSuchElementException {
		if (freeDomainObjIDs.size()==0 || freeRangeObjIDs.size()==0) return 0F;
		
		int candidateIDWithMaxConfidence=-1;
		float maxConfidence=0F, conf;
		ObjCandidates candidateSet = searchCandidates(map,freeDomainObjIDs,freeRangeObjIDs);
		
		totalConfidence += candidateSet.getConfidence();
		if (candidateSet.isPrimaryDomainObj()) freeDomainObjIDs.remove(candidateSet.getObjID()); 
		else freeRangeObjIDs.remove(candidateSet.getObjID());
		
		for (int candidateID : candidateSet.getCandidates()) {
			if (candidateSet.isPrimaryDomainObj()) freeRangeObjIDs.remove(candidateID); 
			else freeDomainObjIDs.remove(candidateID);
			
			conf = probeCandidates(map,freeDomainObjIDs,freeRangeObjIDs,totalConfidence);
			
			if (candidateSet.isPrimaryDomainObj()) freeRangeObjIDs.add(candidateID); 
			else freeDomainObjIDs.add(candidateID);
			
			if (maxConfidence>totalConfidence+conf) continue;
			
			maxConfidence=totalConfidence+conf;
			candidateIDWithMaxConfidence=candidateID;
		}
		
		if (candidateSet.isPrimaryDomainObj()) {
			freeRangeObjIDs.remove(candidateIDWithMaxConfidence);
			this.tmpCorrespondences.addCorrespondence(map.getCorrespondence(candidateSet.getObjID(),candidateIDWithMaxConfidence));
		} else {
			freeDomainObjIDs.remove(candidateIDWithMaxConfidence);
			this.tmpCorrespondences.addCorrespondence(map.getCorrespondence(candidateIDWithMaxConfidence,candidateSet.getObjID()));
		}
		
		return totalConfidence;
	}
	
	private ObjCandidates searchCandidates(ObjCorrespondenceSet map, Set<Integer> freeDomainObjIDs, Set<Integer> freeRangeObjIDs) throws WrongSourceException {
		boolean isDomain =freeDomainObjIDs.size()<=freeRangeObjIDs.size(); 
		ObjCandidates candidateSet = null, set;
		
		for (int objID : isDomain?freeDomainObjIDs:freeRangeObjIDs) {
			set = searchObjCandidates(map,objID,isDomain?freeRangeObjIDs:freeDomainObjIDs,isDomain);
			if (candidateSet==null) candidateSet=set;
			else if (candidateSet.getConfidence()<set.getConfidence()) candidateSet=set;
		}
		
		return candidateSet;
	}
	
	private ObjCandidates searchObjCandidates(ObjCorrespondenceSet map, int objID, Set<Integer> candidateObjIDs, boolean isDomain) throws WrongSourceException {
		ObjCandidates candidateSet = new ObjCandidates(objID,isDomain);
		
		for (ObjCorrespondence objCorr : isDomain?map.filterByDomainObj(objID).getCollection():map.filterByRangeObj(objID).getCollection()) {
			if (!candidateObjIDs.contains(isDomain?objCorr.getRangeObjID():objCorr.getDomainObjID())) continue;
			if (candidateSet.getConfidence()>objCorr.getConfidence()) continue;
			if (candidateSet.getConfidence()<objCorr.getConfidence()) {
				candidateSet.clearCandidates();
				candidateSet.setConfidence(objCorr.getConfidence());
			}
			candidateSet.addCandidate(isDomain?objCorr.getRangeObjID():objCorr.getDomainObjID());
		}
		
		return candidateSet;
	}
	
	
	
	private class ObjCandidates {
		
		private int objID;
		private Set<Integer> candidateSet;
		private boolean isPrimaryDomainObj;
		private float confidence;
		
		public ObjCandidates(int objID, boolean isPrimaryDomainObj) {
			this.objID=objID;
			this.candidateSet = new IntOpenHashSet();
			this.confidence = 0F;
			this.isPrimaryDomainObj=isPrimaryDomainObj;
		}
		
		public int getObjID() {
			return this.objID;
		}
		
		public Set<Integer> getCandidates() {
			return this.candidateSet;
		}
		
		public void addCandidate(int candidate) {
			this.candidateSet.add(candidate);
		}
		
		public void clearCandidates() {
			this.candidateSet.clear();
		}
		
		public float getConfidence() {
			return this.confidence;
		}
		
		public void setConfidence(float confidence) {
			this.confidence=confidence;
		}
		
		public boolean isPrimaryDomainObj() {
			return this.isPrimaryDomainObj;
		}
	}
	
	
//	private float extractCorrespondence(ObjCorrespondences objCorrs, HashSet<Integer> freeDomainObjIDs, HashSet<Integer> freeRangeObjIDs, float totalConf) throws WrongSourceException, NoSuchElementException {
//		if (freeDomainObjIDs.size()==0 || freeRangeObjIDs.size()==0) return totalConf;
//		
//		if (freeDomainObjIDs.size()<=freeRangeObjIDs.size()) {
//			totalConf += this.getMaxCorrespondencesConfidence(objCorrs,
//								this.getFreeObjWithHighestConfidence(objCorrs,freeDomainObjIDs,freeRangeObjIDs,true),
//								freeRangeObjIDs,true); 
//		} else {
//			totalConf += this.getMaxCorrespondencesConfidence(objCorrs,
//								this.getFreeObjWithHighestConfidence(objCorrs,freeDomainObjIDs,freeRangeObjIDs,false),
//								freeDomainObjIDs,false);
//		}
//		
//		return totalConf;
//	}
//	
//	private float getMaxCorrespondencesConfidence(ObjCorrespondences objCorrs, int objID, HashSet<Integer> freeCandidateObjIDs, boolean isDomain) throws WrongSourceException, NoSuchElementException {
//		HashSet<Integer> candidateObjIDs = new HashSet<Integer>();
//		float maxConf=0F, conf;
//		
//		for (int candidateObjID : freeCandidateObjIDs) {
//			conf = isDomain?objCorrs.getCorrespondence(objID,candidateObjID).confidence:
//				objCorrs.getCorrespondence(candidateObjID,objID).confidence;
//			if (maxConf>conf) continue;
//			else if (maxConf<conf) candidateObjIDs.clear();
//			candidateObjIDs.add(candidateObjID);
//		}
//		
//		for (int candidateObjID : candidateObjIDs) {
//			this.tmpCorrespondences.addCorrespondence(isDomain?
//					objCorrs.getCorrespondence(objID,candidateObjID):
//					objCorrs.getCorrespondence(candidateObjID,objID));
//			freeCandidateObjIDs.remove(candidateObjID);
//		}
//		
//		return maxConf;
//	}
//	
//	private int getFreeObjWithHighestConfidence(ObjCorrespondences objCorrs, HashSet<Integer> freeDomainObjIDs, HashSet<Integer> freeRangeObjIDs, boolean isDomain) {
//		HashSet<Integer> candidateObjIDs = new HashSet<Integer>();
//		float maxConf=0F, conf;
//		
//		if (isDomain) {
//			for (int domainObjID : freeDomainObjIDs) {
//				for (int rangeObjID : freeRangeObjIDs) {
//					conf = objCorrs.getCorrespondence(domainObjID,rangeObjID).confidence;
//					if (maxConf>conf) continue;
//					else if (maxConf<conf) candidateObjIDs.clear();
//					candidateObjIDs.add(domainObjID);
//				}
//			}
//		}
//		
//	}
}
