/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/24
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents a filter that reduces the correspondence 
 * set of such correspondences (x,y) which confidence is lower 
 * than a given threshold.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class MaxNFilterBothDirections implements Filter {
	
	/**
	 * The item represents the threshold n used to get the top n correspondences
	 */
	private int n;	
	/**
	 * The constructor initializes the class.
	 * @param threshold n: get the n top correspondeneces
	 */
	public MaxNFilterBothDirections(int n) {
		this.n=n;
	}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		
		ObjCorrespondenceSet tmpResult = new ObjCorrespondenceSet();
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
		Set<Integer> domainObjects,rangeObjects;
		
		domainObjects = corrs.getDomainObjIds();
		for (Integer singleObject : domainObjects){
			ObjCorrespondenceSet corrSetForOneID = corrs.getCorrespondenceSet(singleObject, true);
			if(corrSetForOneID.size()<=n){
				tmpResult.addCorrespondences(corrSetForOneID);				
			}else{				
				ObjCorrespondence[] orderedSimilarities = corrSetForOneID.getDescendingSimilarityOrderedArray();
				int cnt = 0;
				for(ObjCorrespondence corr: orderedSimilarities){
					cnt++;
					if(cnt<=n){
						tmpResult.addCorrespondence(corr);				
					}else{
						break; //correct
					}
				}	
			}					
		}
		rangeObjects = tmpResult.getRangeObjIds();		
		for (Integer singleObject : rangeObjects){
			ObjCorrespondenceSet corrSetForOneID = corrs.getCorrespondenceSet(singleObject, false);
			if(corrSetForOneID.size()<=n){
				result.addCorrespondences(corrSetForOneID);				
			}else{				
				ObjCorrespondence[] orderedSimilarities = corrSetForOneID.getDescendingSimilarityOrderedArray();
				int cnt = 0;
				for(ObjCorrespondence corr: orderedSimilarities){
					cnt++;
					if(cnt<=n){
						result.addCorrespondence(corr);				
					}else{
						break; //correct
					}
				}	
			}					
		}
		return result;
	}
}
