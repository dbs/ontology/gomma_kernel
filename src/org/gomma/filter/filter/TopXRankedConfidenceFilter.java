/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/24
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents a filter that reduces the correspondence 
 * set of such correspondences (x,y) that do not belong to the top
 * x percent ranked by the confidence.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class TopXRankedConfidenceFilter implements Filter {

	/**
	 * The item represents the x percent used to compare the 
	 * confidence of object correspondences. 
	 */
	private float xPercent;
	
	/**
	 * The constructor initializes the class.
	 * @param xPercent percent value ranging from 0 to 100
	 */
	public TopXRankedConfidenceFilter(float xPercent) {
		this.xPercent=xPercent;
	}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		int topN= Math.round(corrs.size() * this.xPercent / 100);
		
		ObjCorrespondence[] sortedCorrespondenceArray = corrs.getAscendingSimilarityOrderedArray();
		
		for (int i=sortedCorrespondenceArray.length-1;topN-i>=0;i--) 
			result.addCorrespondence(sortedCorrespondenceArray[i]);
		
		return result;
	}
}
