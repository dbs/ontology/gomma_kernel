/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/10/24
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents a filter that reduces the correspondence 
 * set of such correspondences (x,y) which confidence is lower 
 * than a given threshold.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class MaxDeltaFilterBothDirections implements Filter {
	
	/**
	 * The item represents the threshold delta used to compare the 
	 * confidence of object correspondences. 
	 */
	private float thresholdDelta;	
	/**
	 * The constructor initializes the class.
	 * @param thresholdDelta confidence threshold delta
	 */
	public MaxDeltaFilterBothDirections(float thresholdDelta) {
		this.thresholdDelta=thresholdDelta;
	}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		
		ObjCorrespondenceSet tmpResult = new ObjCorrespondenceSet();
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
			
		Set<Integer> domainObjects,rangeObjects;
		
		
		domainObjects = corrs.getDomainObjIds();			
		for (Integer singleObject : domainObjects){
			ObjCorrespondenceSet corrSetForOneID = corrs.getCorrespondenceSet(singleObject,true);
			float confHigh	= corrSetForOneID.getHighestConfidence();
			float confNew	= confHigh - thresholdDelta;
		
			for(ObjCorrespondence corr: corrSetForOneID){				
				if(corr.getConfidence()>=confNew){
					tmpResult.addCorrespondence(corr);
					//if(corr.getDomainAccessionNumber().equals("MA_0002440")){
					//	System.out.println(corr.getDomainAccessionNumber() +" - "+corr.getRangeAccessionNumber() +" - "+corr.getConfidence());
					//}
				}
			}			
		}		
		rangeObjects = tmpResult.getRangeObjIds();
		for (Integer singleObject : rangeObjects){
			ObjCorrespondenceSet corrSetForOneID = tmpResult.getCorrespondenceSet(singleObject,false);
			float confHigh	= corrSetForOneID.getHighestConfidence();
			float confNew	= confHigh - thresholdDelta;
		
			for(ObjCorrespondence corr: corrSetForOneID){				
				if(corr.getConfidence()>=confNew){
					result.addCorrespondence(corr);
					//if(corr.getDomainAccessionNumber().equals("MA_0002440")){
					//	System.out.println(corr.getDomainAccessionNumber() +" - "+corr.getRangeAccessionNumber() +" - "+corr.getConfidence());
					//}				
				}
			}			
		}
		//System.out.println("size "+result.size());
		
		return result;
	}

}
