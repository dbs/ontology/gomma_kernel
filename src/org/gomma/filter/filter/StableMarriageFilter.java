/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/12/05
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.util.math.CantorDecoder;

/**
 * The class represents the stable marriage filter. The filter reduces 
 * the input mapping to a 1:1 mapping consisting of correspondences
 * (x,y) such that there is no correspondence (x',y) and 
 * (x,y') with sim(x',y)>sim(x,y)<sim(x,y'). 
 * 
 * See also FilterBest algorithm in S.Melnik: Generic Model Management, Springer, 2004, pp. 142
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class StableMarriageFilter implements Filter {

	private Map<Integer,Set<Integer>> cMap;
	private Set<Long> rejectSet;
	
	/**
	 * The constructor initializes the class.
	 */
	public StableMarriageFilter() {}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet objCorrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		this.cMap = new HashMap<Integer,Set<Integer>>();
		this.rejectSet = new HashSet<Long>();
		
		for (ObjCorrespondence cor : objCorrs.getCollection())
			probeCandidate(cor.getDomainObjID(),cor.getRangeObjID(),false,objCorrs);
		
		this.cMap = new HashMap<Integer,Set<Integer>>();
		
		for (ObjCorrespondence cor : objCorrs.getCollection())
			probeCandidate(cor.getRangeObjID(),cor.getDomainObjID(),true,objCorrs);
		
		for (ObjCorrespondence cor : objCorrs.getCollection())
			if (!this.rejectSet.contains(CantorDecoder.code(cor.getDomainObjID(),cor.getRangeObjID())))
				result.addCorrespondence(cor);
		
		return result;
	}
	
	private void probeCandidate(int objID, int candidateObjID, boolean isInverse, ObjCorrespondenceSet objCorrs) {
		Set<Integer> cList;
		
		if (this.cMap.containsKey(objID)) cList = this.cMap.get(objID);
		else cList = new IntOpenHashSet();
		
		if (cList.isEmpty()) cList.add(candidateObjID);
		else {
			ObjCorrespondence objCor = isInverse?
					objCorrs.getCorrespondence(candidateObjID,objID):
					objCorrs.getCorrespondence(objID,candidateObjID),
					cmpCor;
			int cmp=0;
			for (int id :cList) {
				cmpCor = isInverse?objCorrs.getCorrespondence(id,objID):objCorrs.getCorrespondence(objID,id);
				cmp = objCor.getConfidence()==cmpCor.getConfidence()?0:(objCor.getConfidence()<cmpCor.getConfidence()?-1:1);
			}
			if (cmp==-1) 
				this.rejectSet.add(isInverse?
						CantorDecoder.code(candidateObjID,objID):
						CantorDecoder.code(objID,candidateObjID));
			else if (cmp==0) cList.add(candidateObjID);
			else {
				for (int id : cList) 
					this.rejectSet.add(isInverse?
							CantorDecoder.code(id,objID):
							CantorDecoder.code(objID,id));
				cList = new HashSet<Integer>();
				cList.add(candidateObjID);
			}
		}
		
		this.cMap.put(objID,cList);
	}
}
