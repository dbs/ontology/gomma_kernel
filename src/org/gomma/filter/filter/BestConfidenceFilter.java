/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/09/04
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import java.util.Set;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;

/**
 * The class represents top equivalence filter. The result 
 * set contains correspondences (x,y) of (x,Y) and (X,y) 
 * with the highest similarity for x and y,respectively.
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class BestConfidenceFilter implements Filter {

	/**
	 * The constructor initializes the class.
	 */
	public BestConfidenceFilter() {}
	
	/**
	 * @see Filter#filter(ObjCorrespondenceSet)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet corrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		ObjCorrespondence corr;
		Set<Integer> domainObjIds = corrs.getDomainObjIds(), 
					 rangeObjIds  = corrs.getRangeObjIds(); 
		ObjCorrespondence[] sortedCorrespondenceArray = corrs.getAscendingSimilarityOrderedArray();
		
		for (int i=sortedCorrespondenceArray.length-1; i>=0 && domainObjIds.size()>0 && rangeObjIds.size()>0; i--) {
			corr = sortedCorrespondenceArray[i];
			
			if (!domainObjIds.contains(corr.getDomainObjID())) continue;
			if (!rangeObjIds.contains(corr.getRangeObjID())) continue;
			
			result.addCorrespondence(corr);
			domainObjIds.remove(corr.getDomainObjID());
			rangeObjIds.remove(corr.getRangeObjID());
		}
		return result;
	}
}
