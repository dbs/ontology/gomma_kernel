/**
 *   G O M M A - Generic Ontology Mapping Management and Analysis
 * 
 * This code may be freely distributed and modified under the
 * terms of the GNU Lesser General Public License. This should
 * be distributed with the code. If you do not have a copy,
 * see:
 *
 *      http://www.gnu.org/copyleft/lesser.html
 *
 * Copyright for this code is held jointly by the individual
 * authors. These should be listed in @author doc comments.
 *
 * For more information on the GOMMA project and its aims,
 * or to join the project, visit the web pages
 * at:
 *
 *      http://www.izbi.de/
 *
 * and
 *
 *      http://dbs.uni-leipzig.de
 *
 * Interdisciplinary Centre for Bioinformatics, University of Leipzig
 *
 * creation date: 2008/12/08
 * 
 * changes: --
 * 
 **/
package org.gomma.filter.filter;

import org.gomma.exceptions.WrongSourceException;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.util.math.SmoothFunctions;

/**
 * The class represents a confidence threshold filter based on a 
 * specified smooth function operating on confidence values. 
 * 
 * See also J. Euzenat, P. Shvaiko: Ontology Matching, Springer, 2008, pp. 146
 * 
 * @author Toralf Kirsten (tkirsten@izbi.uni-leipzig.de)
 * @version 1.0
 * @since JDK1.5
 */
public final class SmoothingConfidenceFilter implements Filter {

	private SmoothFunctions smoothFunction;
	private float threshold;
	
	/**
	 * The constructor initializes the class.
	 * @param func smooth function
	 */
	public SmoothingConfidenceFilter(SmoothFunctions func) {
		this.smoothFunction=func;
		this.threshold=0F;
	}
	
	/**
	 * The constructor initializes the class.
	 * @param func smooth function
	 * @param threshold confidence threshold
	 */
	public SmoothingConfidenceFilter(SmoothFunctions func, float threshold) {
		this.smoothFunction=func;
		this.threshold=threshold;
	}
	
	/**
	 * @See Filter#filter(ObjCorrespondences)
	 */
	public ObjCorrespondenceSet filter(ObjCorrespondenceSet objCorrs) throws WrongSourceException {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		
		for (ObjCorrespondence objCorr : objCorrs.getCollection()) {
			objCorr.resetConfidence(this.smoothFunction.smooth(objCorr.getConfidence()));
			
			if (objCorr.getConfidence()>=this.threshold) 
				result.addCorrespondence(objCorr);
		}
		
		return result;
	}
}
