#!/bin/bash
#
# Name              : load-config.sh
# Short Description : loads and sets the local configuration
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 26.12.2008
#-------------------------------------------



# set the host name where you install the gomma service; default host is localhost
HOST_NAME='localhost'

# set the port to which the clients connect to;default port is 1099
PORT=1099

# set the service name which is used by clients to identify the service and access it
SERVICE_NAME=GommaService

URI=${HOST_NAME}:${PORT}/${SERVICE_NAME}

# sets Java home where the Java JRE is located
JAVA_HOME=/opt/jdk1.6.0_14/bin

# set the application home directory where all programs, scripts and libraries are located
APPL_HOME=/opt/gomma

# set the library path where all necessary libraries are located
LIB=${APPL_HOME}/lib

# set the jar path where all java programs are located
JAR=${APPL_HOME}/jar

# set the log path into which all log files should be written
LOG=${APPL_HOME}/log

# set the classpath including all module and kernel libraries as well as  all necessary external libraries
CP=`echo $LIB/* | sed -e "s/ /:/g"`