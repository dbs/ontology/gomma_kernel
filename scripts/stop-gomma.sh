#!/bin/bash
#
# Name              : stop-server.sh
# Short Description : stops/shutdown the Gomma server and service
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 26.12.2008
#-------------------------------------------

# set the environment
source ./load-config.sh

# set the log file name
LOG_FILE=${LOG}/gomma_server.log

# stop and shutdown the GOMMA server
${JAVA_HOME}/java -Xmx512m -Xms512m -classpath ${CP} org.gomma.rmi.GommaAdminClient stop -h=${HOST_NAME} -p=${PORT} -s=${SERVICE_NAME} 1>>${LOG_FILE} 2>>${LOG_FILE} &

sleep 5s

# create the server process
$JAVA_HOME/java -Xmx1024m -Xms1024m -classpath $CP org.gomma.rmi.GommaServer stop -h=$HOST_NAME -p=$PORT -s=$SERVICE_NAME 1>> $LOG_FILE 2>> $LOG_FILE &

# end of the script
