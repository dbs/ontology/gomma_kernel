REM Name              : start-server.bat
REM Short Description : starts up the Gomma server
REM Documentation     : N/A
REM Author            : TK, IZBI Leipzig
REM Arguments         : no arguments
REM Return Code       : 0 if successful
REM                   : 1 if failed
REM-------------------------------------------
REM History           : V 1.0, 26.12.2008
REM-------------------------------------------

REM set the environment
call load-config.bat

REM set the GOMMA configuration file name
set CONFIG_FILE=%APPL_HOME%\gomma-conf.ini

REM set the log file name
set LOG_FILE=%LOG%\gomma_server.log

REM start up the GOMMA server
"%JAVA_HOME%\java" -Xmx512m -Xms512m -classpath %CP% org.gomma.rmi.GommaAdminClient start -h=%HOST_NAME% -p=%PORT% -s=%SERVICE_NAME% -c=%CONFIG_FILE% >>%LOG_FILE%
