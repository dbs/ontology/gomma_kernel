:: Name              : load-config.bat
:: Short Description : loads and sets the local configuration
:: Documentation     : N/A
:: Author            : TK, IZBI Leipzig
:: Arguments         : no arguments
:: Return Code       : 0 if successful
::                   : 1 if failed
::-------------------------------------------
:: History           : V 1.0, 26.12.2008
::-------------------------------------------



:: set the host name where you install the gomma service; default host is localhost
set HOST_NAME=localhost

:: set the port to which the clients connect to;default port is 1099
set PORT=1099

:: set the service name which is used by clients to identify the service and access it
set SERVICE_NAME=GommaService

set URI=%HOST_NAME%:%PORT%/%SERVICE_NAME%

:: sets Java home where the Java JRE is located
set JAVA_HOME=C:\Program Files\Java\jdk1.6.0_14\bin

:: set the application home directory where all programs, scripts and libraries are located
set APPL_HOME=E:\coco\workspace\Gomma\release

:: set the library path where all necessary libraries are located
set LIB=%APPL_HOME%\lib

:: set the jar path where all java programs are located
set JAR=%APPL_HOME%\jar

:: set the log path into which all log files should be written
set LOG=%APPL_HOME%\log

:: set the classpath including all necessary external libraries
set CP=%LIB%\log4j-1.2.9.jar;%LIB%\mysql-connector-java-5.1.7-bin.jar;%LIB%\simmetrics_jar_v1_6_2_d07_02_07.jar
set CP=%CP%;%LIB%\commons-pool-1.4.jar;%LIB%\commons-dbcp-1.2.2.jar
set CP=%CP%;%LIB%\fastutil-5.1.5.jar

:: set the classpath including all module and kernel libraries
set CP=%CP%;%JAR%\gomma-kernel.jar
