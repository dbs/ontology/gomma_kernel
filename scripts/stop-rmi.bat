REM
REM Name              : stop-rmi.sh
REM Short Description : stops the Gomma RMI service
REM Documentation     : N/A
REM Author            : TK, IZBI Leipzig
REM Arguments         : no arguments
REM Return Code       : 0 if successful
REM                   : 1 if failed
REM -------------------------------------------
REM History           : V 1.0, 26.12.2008
REM -------------------------------------------

REM set the environment
call load-config.bat

REM set the log file name
set LOG_FILE=%LOG%\rmi.log

REM create the server process
"%JAVA_HOME%\java" -Xmx1024m -Xms1024m -classpath %CP% org.gomma.rmi.GommaServer stop -h=%HOST_NAME% -p=%PORT% -s=%SERVICE_NAME% >> %LOG_FILE%

REM end of the script
