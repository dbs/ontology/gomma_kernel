#!/bin/bash
#
# Name              : start-gomma.sh
# Short Description : starts the Gomma RMI service and server
# Documentation     : N/A
# Author            : TK, IZBI Leipzig
# Arguments         : no arguments
# Return Code       : 0 if successful
#                   : 1 if failed
#-------------------------------------------
# History           : V 1.0, 26.12.2008
#-------------------------------------------

# set the environment
source ./load-config.sh

#set the log file name
LOG_FILE=${LOG}/gomma.log

# set the GOMMA configuration file name
CONFIG_FILE=${APPL_HOME}/gomma-conf.ini.local

# create the server process
$JAVA_HOME/java -Xmx1024m -Xms1024m -classpath $CP org.gomma.rmi.GommaServer start -h=$HOST_NAME -p=$PORT -s=$SERVICE_NAME 1>>$LOG_FILE 2>>$LOG_FILE &

sleep 5s

# start up the GOMMA server
${JAVA_HOME}/java -Xmx512m -Xms512m -classpath ${CP} org.gomma.rmi.GommaAdminClient start -h=${HOST_NAME} -p=${PORT} -s=${SERVICE_NAME} -c=${CONFIG_FILE} 1>>${LOG_FILE} 2>>${LOG_FILE} &
